package fr.univtours.gpod.Controller;

import fr.univtours.gpod.persistance.Manager.PersistenceManager;
import fr.univtours.gpod.persistance.entities.AireStockage;
import fr.univtours.gpod.persistance.entities.SousStructure;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.io.File;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created with IntelliJ IDEA.
 * User: geoffroy.vibrac
 * Date: 27/03/13
 * Time: 12:28
 */
public class ImportControllerTest {

    @Before
    public void setUp() throws Exception {
        PersistenceManager.setEntityName("gpodLR2");

    }

    @After
    public void tearDown() throws Exception {
        PersistenceManager.setEntityName("gpodLR");
    }

    @Test
    public void testImportFile() throws Exception {
        System.out.println(PersistenceManager.getEntityName());
        HttpServletRequest stubRequest = mock(HttpServletRequest.class);
        HttpServletResponse stubResponse = mock(HttpServletResponse.class);
        HttpSession stubSession = mock(HttpSession.class);
        when(stubSession.getAttribute("idUtilisateur")).thenReturn(1L);
        when(stubSession.getAttribute("absolutePath")).thenReturn("c://usr//");
        SousStructure ss = mock(SousStructure.class);
        when(ss.getIdSousStructure()).thenReturn(1);
        AireStockage as = mock(AireStockage.class);
        when(as.getIdAire()).thenReturn(1);

        ImportController ic = new ImportController(stubRequest, stubResponse, stubSession);
        System.out.println(ic.importFile("RICM J1122 CARBONE 9.xlsx", ss, as));
    }

    //@Test
    public void testAddData() throws Exception {
        SousStructure ss = mock(SousStructure.class);
        when(ss.getIdSousStructure()).thenReturn(1);
        AireStockage as = mock(AireStockage.class);
        when(as.getIdAire()).thenReturn(1);
        HttpServletRequest stubRequest = mock(HttpServletRequest.class);
        HttpServletResponse stubResponse = mock(HttpServletResponse.class);
        HttpSession stubSession = mock(HttpSession.class);
        when(stubSession.getAttribute("idUtilisateur")).thenReturn(1L);
        when(stubSession.getAttribute("absolutePath")).thenReturn("c://usr//");

        ImportController ic = new ImportController(stubRequest, stubResponse, stubSession);
        ic.addData(ss, as);
    }

}
