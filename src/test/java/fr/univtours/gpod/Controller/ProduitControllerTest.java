package fr.univtours.gpod.Controller;

import fr.univtours.gpod.persistance.Manager.PersistenceManager;
import fr.univtours.gpod.persistance.entities.AireStockage;
import fr.univtours.gpod.persistance.entities.SousStructure;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created with IntelliJ IDEA.
 * User: geoffroy.vibrac
 * Date: 06/05/13
 * Time: 15:22
 */
public class ProduitControllerTest {
    @Before
    public void setUp() throws Exception {
        PersistenceManager.setEntityName("gpodLR2");

    }

    @After
    public void tearDown() throws Exception {
        PersistenceManager.setEntityName("gpodLR");
    }

    //@Test
    public void testGetXLSList() throws Exception {
        System.out.println(PersistenceManager.getEntityName());
        HttpServletRequest stubRequest = mock(HttpServletRequest.class);
        HttpServletResponse stubResponse = mock(HttpServletResponse.class);
        HttpSession stubSession = mock(HttpSession.class);
        when(stubSession.getAttribute("idUtilisateur")).thenReturn(1L);


        ProduitController pc = new ProduitController(stubRequest, stubResponse, stubSession);
        pc.getXLSList();
    }
}
