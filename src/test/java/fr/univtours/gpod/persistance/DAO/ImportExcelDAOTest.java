package fr.univtours.gpod.persistance.DAO;

import fr.univtours.gpod.persistance.Manager.PersistenceManager;
import fr.univtours.gpod.persistance.entities.ImportExcel;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: geoffroy.vibrac
 * Date: 15/05/13
 * Time: 15:50
 */
public class ImportExcelDAOTest {

    @Before
    public void setUp() throws Exception {
        PersistenceManager.setEntityName("gpodLR2");

    }

    @After
    public void tearDown() throws Exception {
        PersistenceManager.setEntityName("gpodLR");
    }

    //@Test
    public void testDestroyAllNew() throws Exception {
        ImportExcel.getDao().destroyAllNew();
    }

    //@Test
    public void getImport() throws Exception {
        List<ImportExcel> list =  ImportExcel.getDao().getImport(true, true, -1,-1);
        System.out.println(list.size());
    }
}
