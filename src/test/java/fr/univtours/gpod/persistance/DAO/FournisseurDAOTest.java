package fr.univtours.gpod.persistance.DAO;

import fr.univtours.gpod.persistance.Manager.PersistenceManager;
import fr.univtours.gpod.persistance.entities.Fournisseur;
import fr.univtours.gpod.persistance.entities.ImportExcel;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: geoffroy.vibrac
 * Date: 17/05/13
 * Time: 13:53
 */
public class FournisseurDAOTest {

    @Before
    public void setUp() throws Exception {
        PersistenceManager.setEntityName("gpodLR2");

    }

    @After
    public void tearDown() throws Exception {
        PersistenceManager.setEntityName("gpodLR");
    }


    //@Test
    public void testFindFournisseurEntities() throws Exception {
         FournisseurDAO fournisseurDao = new    FournisseurDAO();
         List<Fournisseur> fs = fournisseurDao.findFournisseurEntities(true, "Carlo Erba", "nom", 0, 0);

        System.out.println(fs.size());
    }
}
