/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2012/01/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/

Ext.onReady(function(){

	Ext.QuickTips.init();
	
    var doDisable = function(btn) {     
        btn.disable();
    }

    var doEnable = function(btn) {     
        btn.enabled();
    }
    
	var login = new Ext.FormPanel({
		renderTo:'loginpanel',
		title:"Identification",
		url: 'public/Identify',
		frame:true,
		waitMsgTarget: true, // si on l'enleve, �a affiche une jauge de progression
		width:300,
		items:[{fieldLabel:'Identifiant',
                        id:'identifiant',
				xtype:'textfield',
		       name:'identifiant',
		       blankText:'Entrer un identifiant',
		       msgTarget:'side',
		       allowBlank: false},{
		    	   fieldLabel:'Mot de passe',
		    	   inputType:'password',
		    	   xtype:'textfield',
		    	   //msgTarget:'side',
			       name:'mdp',
			       blankText:'Entrer un mot de passe',
			       allowBlank: false}
		       ],
		buttons:[{
			id:'valider',
			text:'valider',
			method:'POST',
			waitMsg:'Saving Data...',
			handler:function(btn){
				if (login.getForm().isValid()){
					btn.disable();
					login.getForm().submit({waitTitle:'Connexion au serveur', waitMsg:'Identification en cours...',
						success:function(f,action){
						var result = Ext.util.JSON.decode(action.response.responseText);
                                                window.location = result.redirect;
					},
						failure:function(f,action){
							btn.enable();
							var result = Ext.util.JSON.decode(action.response.responseText);
							Ext.Msg.alert('Erreur d\'identification', result.errors.reason);
						}
					});
				}
			}
	
		}],
                keys: [
                    { key: [Ext.EventObject.ENTER],
                        handler:function(){
				if (login.getForm().isValid()){
					var btn = Ext.getCmp('valider')
                                        btn.disable();
					login.getForm().submit({waitTitle:'Connexion au serveur', waitMsg:'Identification en cours...',
						success:function(f,action){
						var result = Ext.util.JSON.decode(action.response.responseText);
                                                window.location = result.redirect;
					},
						failure:function(f,action){
							btn.enable();
							var result = Ext.util.JSON.decode(action.response.responseText);
							Ext.Msg.alert('Erreur d\'identification', result.errors.reason);
						}
					});
				}
			}
                    }
                ]
		

		
		
	});
	
	
	login.render(document.body);
	Ext.getCmp("identifiant").focus();
	
	
	
	
});