/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2012/01/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/

var myTimer;

Ext.QuickTips.init();

    Ext.form.Field.prototype.msgTarget = 'side';

    var bd = Ext.getBody();


 // ---------------------------------------
 // GESTION DES DROITS
   Ext.Ajax.request({
        params:{
                controller:'LivraisonSimple',
                action:'getRight'
            },
         url:'FrontController',
         success:function(response, options){
 
         },
         failure:function(response, options) {  
            Ext.getCmp("btn-enreg").hide();
            Ext.getCmp("btn-new").hide();
         }
    }); 
 // ---------------------------------------   


// ----------------------------------------------------------
// ----------------------------------------------------------
// TABLEAU - GRID 
// acces au données pour le tableau
    var proxy = new Ext.data.HttpProxy({
        url: 'FrontController',
        method: 'POST'
    });
    
    var rec = Ext.data.Record.create([
            {name: 'idLivraison'},
            {name: 'CAS'},
            {name: 'idProduitConditionnement'},
            {name: 'nomProduit'},
            {name: 'dateLivraison'},
            {name: 'idSousStructure'},
            {name: 'nomSousStructure'},
            {name: 'quantite'},
            {name: 'write'}
    ]);

    var reader = new Ext.data.JsonReader({
 	   root : "data",
 	   id : "idLivraison"
    }, rec);


    var ds = new Ext.data.JsonStore({
        proxy: proxy,
        fields: ['idLivraison','CAS','idProduitConditionnement', 'nomProduit', 'dateLivraison','idSousStructure','nomSousStructure','quantite','supp','write'],
        root:'data',
        totalProperty: 'totalCount',
        reader: reader,
        nocache: true,
        autoLoad: false,
        baseParams:{action:'getListItems', controller:'LivraisonSimple'},
        listeners:{
            load:function(){   
                if(typeof(myMask) !== 'undefined')
                    myMask.hide();
            }
        }
    });
    ds.load({params:{start:0, limit:10, period:1}});


    



    var colModel = new Ext.grid.ColumnModel([
        {id:'idLivraison',header: "idLivraison", hidden:true, dataIndex: 'idLivraison'},
        {id:'CAS',header: "CAS", width: 100, sortable: true, locked:false, dataIndex: 'CAS'},
        {header: "idProduitConditionnement", width: 100, sortable: true, dataIndex: 'idProduitConditionnement', hidden:true},
        {id:'nomProduit',header: "Produit", width: 320, sortable: true, dataIndex: 'nomProduit'},
        {header: "idSousStructure", width: 200, sortable: true, dataIndex: 'idSousStructure', hidden:true},
        {header: "Equipe/Département", width: 200, sortable: true, dataIndex: 'nomSousStructure'},
        {header: "Date entrée", width: 100, sortable: true, dataIndex: 'dateLivraison'},
        {header: "quantite", width: 50, sortable: true, dataIndex: 'quantite'},
        {header: "Supp", width: 50, sortable: false, xtype:'actioncolumn',
        items: [{
                    getClass: function(v, meta, rec) {          // Or return a class from a function
                        if (rec.get('supp') == 'true' && rec.get('write') == 'true'){
                            this.items[0].tooltip = 'Supprimer cette ligne';
                            return 'del-col';
                        }
                    },
                    handler: function(grid, rowIndex, colIndex) {
                        var rec = ds.getAt(rowIndex);
                        deleteligne(rec.get('idLivraison'));
                    }
                }]}
    ]);

    var PagingBar = new pagingBarWithPeriodButton({
        pageSize: 10,
        store: ds,
        displayInfo: true,
        action:'getListItems', 
        controller:'LivraisonSimple',
        displayMsg: 'Entrées directes {0} - {1} of {2}',
        emptyMsg: 'Aucune entrée'
    });   
    
    PagingBar.insertButton(11, periodButtons);
    PagingBar.insertButton(13, excelButton);    
    
// FIN TABLEAU - GRID
// ----------------------------------------------------------   
    
// ---------------------------------------------------------- 
//  Toolbar Tableau
  var formTbar = new Ext.Toolbar({
     height: 27,
     items: [{text: 'Nouvelle entrée',
                iconCls: 'icon-add',
                id:'btn-new',
                handler : reset
               },'->',{
                id:'editXls',
                iconCls:'icon-excel',
                value: '',
                handler:function(){
                    var value = includePanel.getForm().findField('idLivraison').getValue();
                    if (value>0){
                        printAction("LivraisonSimple","getXLSList", value);
                    }else{
                        Ext.Msg.alert('Export impossible','Veuillez selectionner un item à éditer');
                        return;
                    }
                }            
            },{
                id:'edit',
                iconCls:'icon-print',
                value: '',
                handler:function(){
                    var value = includePanel.getForm().findField('idLivraison').getValue();
                    if (value>0){
                        printAction("LivraisonSimple","getPDF", value);
                    }else{
                        Ext.Msg.alert('Export impossible','Veuillez selectionner un item à éditer');
                        return;
                    }
                }            
            }]
    });
    
     var formBbar = new Ext.Toolbar({
     height: 27,
     id:'bottom-bar',
     cls:'x-bbar',
     id:'bottom-bar',
     items: [                   '->', 
               {
                   id:'btn-enreg',
                text: 'Enregistrer',
                iconCls: 'icon-save',
                handler : save
                }]

    });  
// ---------------------------------------------------------- 




//// ---------------------------------------------------------------
/// LIVRAISON


    var recliv = Ext.data.Record.create([
            {name: 'idArticle'},
            {name: 'CAS'},
            {name: 'reference'},
            {name: 'idProduitConditionnement'},
            {name: 'nom'},
            {name: 'idAire'},
            {name: 'nomAire'},
            {name: 'identifiantEtiquette'}
    ]);

    var readerliv = new Ext.data.JsonReader({
 	   root : "data",
 	   id : "idArticle"
    }, recliv);

    var writerliv = new Ext.data.JsonWriter({
           encode: true,
           writeAllFields: false
       });

    var dsliv = new Ext.data.JsonStore({
        proxy: proxy,
        fields: ['idArticle','CAS','reference','idProduitConditionnement', 'nom', 'identifiantEtiquette', 'idAire', 'refrigere'],
        root:'data',
        autoSave:false,
        totalProperty: 'totalCount',
        reader: reader,
        writer:writerliv,
        nocache: true,
        autoLoad: false,
        baseParams:{action:'getListArticles', controller:'Livraison'}
    });
    
    var idAireFavorite = 0;

        Ext.Ajax.request({
            url: 'FrontController',
            method: 'GET',
            success: function(response, conn, option){
                var ajaxResponse = Ext.util.JSON.decode(response.responseText);
                idAireFavorite = ajaxResponse.idAireStockage;
            },
            params: {action:'getAireStockageFavorite', controller:'Utilisateur'}
        });

    // ComboBox Aire dans la colonne
    var cbaire = new Ext.data.JsonStore({
        proxy: proxy,
        fields: ['idAire', 'nomTypeAire', 'nomLong','refrigere'],
        root:'data',
        totalProperty: 'totalCount',
        nocache: true,
        autoLoad: true,
        baseParams:{action:'getListItems', controller:'AireStockage'}
    });

 // ----------------------------------------------------------

   var comboRenderer = function(combo){
        return function(value){
            combo.store.clearFilter();
            var record = combo.findRecord(combo.valueField || combo.displayField, value);
            return record ? record.get(combo.displayField) : combo.valueNotFoundText;
        }
    }


    var combo = new Ext.form.ComboBox({
                typeAhead: true,
                name:'nom',
                store: cbaire,
                root:'data',
                triggerAction: 'all',
                listClass: 'x-combo-list-small',
                allowBlank: false,
                hiddenName : 'idAire',
                valueField:'idAire',
                displayField:'nomLong',
                mode: 'local',
                listeners:{
                     // Test si l'aire est refrigérée
                     'select': function(combo, record, index){
                         // si l'aire est rédrigérée, pas de test, sinon on test si le produit l'est
                         if (record.get('refrigere')=="false"){
                            // Je récupère dans le DOM l'index de la ligne sélectionnée grace au style CSS x-grid3-row-selected
                            var gridIndex = Ext.get(this.el.dom.parentElement.parentElement.previousSibling.previousSibling).query(".x-grid3-row-selected")[0].rowIndex;
                            // je récupère la propriété refrigérée du produit dans la grid
                            var produitRefrigere = Ext.getCmp('gridEnStock').getStore().getAt(gridIndex).get('refrigere');
                            // si le produit a besoin d'être refrigéré, on affiche un message
                            if (produitRefrigere=="true"){
                                Ext.Msg.alert('Aire non réfrigérée','Attention, le produit nécessite \n\ une aire réfrigérée, or l\'aire choisie ne l\'est pas.');
                            }
                         }                         
                     }
                }                
                
    });


    var cmliv = new Ext.grid.ColumnModel({
        // specify any defaults for each column
        defaults: {
            sortable: true // columns are not sortable by default
        },
        columns: [{
            id: 'idArticle',
            header: 'id',
            dataIndex: 'idArticle',
            width: 40
        },{
            id: 'CAS',
            header: 'CAS',
            dataIndex: 'CAS',
            width: 70
        },{
            id: 'reference',
            header: 'Ref. Fournisseur18LI;!',
            dataIndex: 'reference',
            width: 80
        }, {
            id: 'idProduitConditionnement',
            dataIndex: 'idProduitConditionnement',
            hidden:true
        }, {
            id: 'nom',
            header: 'Nom produit',
            dataIndex: 'nom',
            width: 200
        },{
            id: 'idAire',
            header: 'Aire de Stockage',
            dataIndex: 'idAire',
            renderer:comboRenderer(combo),
            width: 430,
            editor: combo
        },{
            id: 'identifiantEtiquette',
            header: 'Code Etiquette',
            dataIndex: 'identifiantEtiquette',
            width: 120, 
            editor: new Ext.form.TextField({
                        xtype:'textfield',
                        maxLength:50
                    }),css:'background-image:url(lib/extjs/resources/images/default/grid/dirty.gif);background-repeat:no-repeat;'
        }, {
            id: 'refrigere',
            dataIndex: 'refrigere',
            hidden:true
        },
        {header: "scan", width: 40, sortable: false, xtype:'actioncolumn',
        items: [{
                    getClass: function(v, meta, rec) {          // Or return a class from a function
                            this.items[0].tooltip = 'Scanner une étiquette';
                            return 'rfid-col';
                    },
                    handler: function(grid, rowIndex, colIndex) {
                            //window.open ("popupnfc.html?row="+rowIndex, "Scanner une étiquette", "height=220, width=250, top=100, left=100, toolbar=no, menubar=yes, location=no, resizable=yes, scrollbars=no, status=no");                             
                             Ext.DomHelper.overwrite('iframediv', {
                                    tag: 'iframe',
                                    id:'appletIFrame',
                                    frameBorder: 0,
                                    width: 0,
                                    height: 0,
                                    css: 'display:none;visibility:hidden;height:0px;',
                                    //css: 'height:50px;',
                                    src: '../applet/popupnfc.html?row='+rowIndex+"&origine=LivraisonSimple"
                                });
                                appletWin.show();
                                appletWin.el.mask('Chargement, veuillez patienter...', 'x-mask-loading');
                                console.log('toto');
                            
                            
                        }
                }]} ]
    });


    function disableFields(){
        Ext.getCmp("StockForm").getForm().findField('nomProduit').setReadOnly(true);
        Ext.getCmp("StockForm").getForm().findField('nomProduit').addClass("x-item-disabled");
        Ext.getCmp("StockForm").getForm().findField('nomSousStructure').setReadOnly(true);
        Ext.getCmp("StockForm").getForm().findField('nomSousStructure').addClass("x-item-disabled");
        Ext.getCmp("StockForm").getForm().findField('dateLivraison').setReadOnly(true);
        Ext.getCmp("StockForm").getForm().findField('dateLivraison').addClass("x-item-disabled");
        Ext.getCmp("StockForm").getForm().findField('quantite').setReadOnly(true);
        Ext.getCmp("StockForm").getForm().findField('quantite').addClass("x-item-disabled");
        Ext.getCmp("btn_searchProduit").disable();
        Ext.getCmp("btn_searchSousStructure").disable();
        Ext.getCmp("btn_allAires").disable();
        Ext.getCmp("btn_selAire").disable();
        //Ext.getCmp("btn-enreg").disable()
        //Ext.getCmp("gridEnStock").disable();
    }

    function enableFields(){
        Ext.getCmp("StockForm").getForm().findField('nomProduit').setReadOnly(false);
        Ext.getCmp("StockForm").getForm().findField('nomProduit').removeClass("x-item-disabled");
        Ext.getCmp("StockForm").getForm().findField('nomSousStructure').setReadOnly(false);
        Ext.getCmp("StockForm").getForm().findField('nomSousStructure').removeClass("x-item-disabled");
        Ext.getCmp("StockForm").getForm().findField('dateLivraison').setReadOnly(false);
        Ext.getCmp("StockForm").getForm().findField('dateLivraison').removeClass("x-item-disabled");
        Ext.getCmp("StockForm").getForm().findField('quantite').setReadOnly(false);
        Ext.getCmp("StockForm").getForm().findField('quantite').removeClass("x-item-disabled");
        Ext.getCmp("btn_searchProduit").enable();
        Ext.getCmp("btn_searchSousStructure").enable();
        Ext.getCmp("btn_allAires").enable();
        Ext.getCmp("btn_selAire").enable();
        Ext.getCmp("btn-enreg").enable();
         Ext.getCmp("gridEnStock").enable();
    }

    function restartForm(){
        Ext.getCmp("gridEnStock").getStore().removeAll();
        enableFields();
        Ext.getCmp("StockForm").getForm().findField('dateLivraison').setValue(new Date());
        
    }

    
    var includePanel = new Ext.FormPanel({
        id: 'StockForm',
        controller:'LivraisonSimple',
        url: 'FrontController',
        frame: true,    
        labelAlign: 'left',
        waitMsgTarget: true,
        width: 1021,
        layout: 'border',    // Specifies that the items will now be arranged in columns
        height:645,
        baseParams:{action:'saveItem', controller:'LivraisonSimple'},
        items: [{
            xtype: 'grid',
            id:'gridP',      	
            margins: '5 0 5 0',
            region: 'center',
            height:287,
            bbar: PagingBar , 
            enableColumnHide:false,
            loadMask:{msg: 'Chargement...'},
            ds: ds,
            cm: colModel,
            sm: new Ext.grid.RowSelectionModel({
                singleSelect: true,
                listeners: {
                    rowselect: function(sm, row, rec) {

                        disableFields();

                        Ext.getCmp("StockForm").getForm().loadRecord(rec);

                        Ext.getCmp("gridEnStock").getStore().removeAll();
                       // Ext.getCmp("gridEnStock").getGridEl().mask('chargement...', 'x-mask-loading');
                        Ext.getCmp("gridEnStock").stopEditing();
                        dsliv.load({

                            params:{idLivraison:rec.get('idLivraison')},
                            callback: function(records,o,s){
                                Ext.getCmp("gridEnStock").getGridEl().unmask();
                                Ext.getCmp("gridEnStock").getColumnModel().setEditable(4, false);
                                Ext.getCmp("gridEnStock").getColumnModel().setEditable(5, false);
                            }
                            })

                    }}}),
            autoExpandColumn: 'nomProduit',
             title:'Entrées directes',
            border: true
        },{


            xtype:'panel',
            region:'south',
            id:'southPanel',
            height:335,
            layout: 'border',
            items:[{





            id:'formPart',
            displayFloatingMsg:true,
            split:true,        	
            margins: '0 0 0 0',        	
            columnWidth: 0.9,
            autoScroll:true,    
            region: 'center',
            xtype: 'fieldset',
            loadMask:{msg: 'Chargement...'},
            labelWidth: 190,
            cls:'x-fieldset-with-tbar',            
            tbar:formTbar,            
            bbar:formBbar,    
            defaults: {border:false},    // Default config options for child items
            defaultType: 'textfield',
            labelSeparator:'',
            //autoHeight: true,
            bodyStyle: Ext.isIE ? 'padding:0 0 5px 15px;' : 'padding:5px 15px;',
            border: true,
            items: [
                    {
                      fieldLabel: 'idLivraison',
                      name: 'idLivraison',
                      hidden:true
                    },{
                      fieldLabel: 'aireRefrigere',
                      name: 'aireRefrigere',
                      hidden:true
                    },
                    {
                      xtype: 'compositefield',
                      fieldLabel: 'Produit *',
                      width:324,
                      items: [
                        {
                            xtype: 'textfield',
                            name: 'nomProduit',
                            allowBlank: false,
                            readOnly:true,
                            blankText:'Le champ est requis',
                            width:200
                         },
                         {
                          id:'btn_searchProduit',
                          xtype:'button',
                          text:'Selectionner un Produit',
                          windowName:'addProduit',
                          handler:function(){
                              var mySearchWin = eval(this.windowName);
                              mySearchWin.show();
                          }
                         }
                    ]},
                    {
                      xtype: 'compositefield',
                      fieldLabel: 'Equipe/Département *',
                      width:593,
                      items: [
                        {
                            xtype: 'textfield',
                            name: 'nomSousStructure',
                            allowBlank: false,
                            readOnly:true,
                            blankText:'Le champ est requis',
                            width:400
                         },
                         {
                          xtype:'button',
                          id:'btn_searchSousStructure',
                          text:'Selectionner une Equipe/Département',
                          windowName:'searchSousStructure',
                          handler:function(){
                              var mySearchWin = eval(this.windowName);
                              mySearchWin.show();
                          }
                         }
                    ]},
                {
                xtype: 'datefield',
                fieldLabel: 'Date d\'entrée *',
                name: 'dateLivraison',
                startDay:1,
                allowBlank: false,
                value:new Date(),
                format:'d/m/Y',
                blankText:'Le champ est requis',                
                width:100
            },{
                            xtype:'numberfield',
                            name: 'quantite',
                            hidden:true

                        }
//                        , {
//                  xtype: 'compositefield',
//                  fieldLabel: 'Quantite *',
//                  width:152,
//                  items: [{
//              xtype:'button',
//              text:'Générer les lignes',
//              //iconCls:'icon-delete',
//              id:'genereLigne',
//              handler:function(){
//
//                   theGrid = Ext.getCmp("gridEnStock");
//                   dsliv.removeAll();
//                   itemsNumber = parseInt(includePanel.getForm().findField('quantite').getValue());
//                   idProduitConditionnement = parseInt(includePanel.getForm().findField('idProduitConditionnement').getValue());
//
//                  if (isNaN(itemsNumber) ||itemsNumber<=0 || isNaN(idProduitConditionnement) ||idProduitConditionnement<=0){
//                       Ext.Msg.alert("erreur", "Veuillez saisir un <b>produit</b> et une <b>quantite</b>");
//                  }else{
//
//                          Ext.getCmp("gridEnStock").stopEditing();
//
//                          for (i=0;i<itemsNumber;i++){
//
//                              dsliv.add(new recliv(
//                                    {
//                                       idArticle:i+1,
//                                       CAS: includePanel.getForm().findField('CAS').getValue(),
//                                       reference:includePanel.getForm().findField('referenceFournisseur').getValue(),
//                                       idProduitConditionnement:includePanel.getForm().findField('idProduitConditionnement').getValue(),
//                                       nom:includePanel.getForm().findField('nomProduit').getValue(),
//                                       identifiantEtiquette:''
//
//                                    }
//                                ));
//                          }
//
//                          Ext.getCmp("gridEnStock").startEditing(0, 1);
//                  }
//
//              }
//             }
//            ]}
        ,{
                    xtype:'panel',
                    baseCls:'x-plain',
                    layout:'hbox',
                    width:900,
                    defaults:{
                        margins:'0 5 0 0',
                        pressed: false
                    },

                    items: [{cls:'x-form-item',   xtype:'label', text:'Aire de Stockage *', width:190},
                        {
                        xtype:'button',
                        id:'btn_allAires',
                        fieldLabel: 'Aire de stockage',
                        text:'Selectionner votre aire de stockage favorite',
                        handler:function(){
                            theGrid = Ext.getCmp("gridEnStock");
                            if (theGrid.getStore().getCount() == 0) {
                                Ext.Msg.alert("erreur", "Générez d'abord les lignes");
                            }else{

                                theGrid = Ext.getCmp("gridEnStock");
                                theStore = theGrid.getStore();
                                var problemeRefrigere=false;
                                for(i=0;i<theStore.getCount();i++){
                                    var rec = theStore.getAt(i);
//                                if (selRecord.data.refrigere=="false"){
//                                    var produitRefrigere = rec.get('refrigere');
//                                    if (produitRefrigere=="true"){
//                                        problemeRefrigere = true;
//                                    }
//                                }
                                    rec.set('idAire', idAireFavorite);
                                    rec.commit();
                                    theStore.commitChanges();
                                }
                                if (problemeRefrigere==true){
                                    Ext.Msg.alert('Aire non réfrigérée','Attention, des produits nécessitent \n\ une aire réfrigérée, or l\'aire choisie ne l\'est pas.');
                                }
                            }
                        }},{

                        xtype:'button',
                        id:'btn_selAire',
                        fieldLabel: 'Aire de stockage',
                        text:'Selectionner une aire de stockage',
                        windowName:'searchAireStockage',
                          handler:function(){
                              theGrid = Ext.getCmp("gridEnStock");
                              if (theGrid.getStore().getCount() == 0) {
                                    Ext.Msg.alert("erreur", "Générez d'abord les lignes");
                              }else{

                                    var mySearchWin = eval(this.windowName);
                                    mySearchWin.show();
                              }

                          }




                    }
                    ]














             }
             ,{
              fieldLabel: 'idProduitConditionnement',
              name: 'idProduitConditionnement',
              hidden:true
             },{
              fieldLabel: 'idSousStructure',
              name: 'idSousStructure',
              hidden:true
             },
            {
              fieldLabel: 'produitRefrigere',
              name: 'produitRefrigere',
              hidden:true
            }, {
              fieldLabel: 'referenceFournisseur',
              name: 'referenceFournisseur',
              hidden:true
            },
            {
              fieldLabel: 'CAS',
              name: 'CAS',
              hidden:true
            },{
              name: 'articlesLivraison',
              hidden:true
            }
            ]
            


            },{
                xtype: 'editorgrid',
                id:'gridEnStock',
                margins: '0 0 0 0',
                region: 'south',
                clicksToEdit: 1,
                height:160,
                split:true,
                enableColumnHide:false,
                loadMask:{msg: 'Chargement...'},
                sm: new Ext.grid.RowSelectionModel({}),
                border: true,
                ds: dsliv,
                cm: cmliv
           }   

    ]







        }],
        saveItemEnd:function(){
                        var erreurAireStockage = false;
                        var grid = Ext.getCmp('gridEnStock');
                        // si pas de ligne : on annule
                        if (grid.getStore().getCount()==0){
                             Ext.Msg.alert('Erreur', "Vous devez générer des lignes d'articles");
                                return;
                        }

                        // CONVERSION DE LA GRILLE DES LIVRAION SON JSON
                        grid.stopEditing();
                        var recordsToSend = grid.getStore().getRange();
                        var farray = [];
                        Ext.each(recordsToSend, function(aRecord) {
                            if (aRecord.get('idAire')==undefined){
                                Ext.Msg.alert('Erreur', "Vous devez saisir une aire de stockage pour chaque article");
                                erreurAireStockage = true;
                                return;
                            }
                            farray.push(aRecord.data);
                        });

                        if (erreurAireStockage == true){
                            return;
                        }

                        var JSONToSend = Ext.encode(farray);
                        includePanel.getForm().findField('articlesLivraison').setValue(JSONToSend);
                        // FIN conversion




                        includePanel.getForm().submit({waitTitle:'Connexion au serveur', waitMsg:'Enregistrement en cours...',
                        success:function(f,action){
                           ds.reload();
                           includePanel.getForm().reset();
                           restartForm();
                            Ext.msg.show(this.title ? this.title : 'Op&eacute;ration r&eacute;ussie:', 'Enregistrement effectu&eacute; !');
                },
                        failure:function(f,action){
				var result = Ext.util.JSON.decode(action.response.responseText);
				Ext.Msg.alert('Erreur', result.errors.reason);
                        }
                });

        },
        saveItem:function(){
            if (includePanel.getForm().isValid()){

                var prodR = includePanel.getForm().findField('produitRefrigere').getValue();
                var aireR = includePanel.getForm().findField('aireRefrigere').getValue();

                if (prodR=="true" && aireR=="false") {
                    Ext.MessageBox.confirm('Aire non réfrigérée','Attention, le produit nécessite \n\
                        une aire réfrigérée, or l\'aire choisie ne l\'est pas. Voulez vous l\'ajouter quand même ? ',function(btn) {
                           if(btn == 'no') {
                               return false;
                           }else{
                               this.saveItemEnd();
                           }
                    },this);
                    return false;
                }else{
                    this.saveItemEnd();
                }


            }
        },
        resetToNull: function() {
           var form = this.getForm();
           if (form.isDirty()){
            Ext.MessageBox.show({
               buttons: Ext.Msg.OKCANCEL,
               title:'Etes-vous sur ?',
               msg:'Les données non enregistrées vont être effacées. Etes-vous sur ?',
               fn:function(btn) {
                   if(btn == 'ok') {
                       Ext.getCmp("StockForm").resetForm();
                        Ext.getCmp("gridP").getSelectionModel().clearSelections();
                        restartForm();
                   }else{
                       return;
                   }
            }});
            return false;
            }
    },
    resetForm:function(){
        var form = this.getForm();
        form.items.each(function(f){			
       if(f.getXType() == 'xcheckbox')
            f.originalValue = false;
        else
            f.originalValue = '';
        }, form);
        form.reset();
    },   
    deleteItem: function(id) {
        if(id == null)
            var this_id = this.getForm().findField(this.gridP).getValue();
        else
            var this_id = id;
        if(this_id != '') {
            Ext.MessageBox.confirm('Etes-vous sur ?','Voulez-vous effacer cet enregistrement, le stock va être modifié&nbsp;?',function(btn) {
                if(btn == 'yes') {
                        Ext.getCmp("gridP").loadMask.show();   
                        var formPart = Ext.getCmp("formPart");
                        formPart.el.mask('Chargement...', 'x-mask-loading');                     
                        var myFormParams = {
                            controller:this.controller,
                            action:'deleteItem',
                            id:this_id,
                            output:'json'
                        };
                        // adding extra params if necessary!
                        if (this.extraParams) {
                            for (key in this.extraParams) {
                                myFormParams[key] = this.extraParams[key]; 
                            }
                        }
                        Ext.Ajax.request({
                            params:myFormParams,
                            url:'FrontController',
                             success:function(response, options){
                                if (response.responseText) {						
                                    if (response.responseText.substr(0,1) == '{') {
			    	        var ajaxResponse = Ext.util.JSON.decode(response.responseText);
				     }
	                        }
                                if(ajaxResponse && ajaxResponse.success) {
                                    includePanel.getForm().reset();
                                    ds.reload();
                                    restartForm();
                                    Ext.msg.show(this.title ? this.title : 'Op&eacute;ration r&eacute;ussie:', 'Enregistrement effac&eacute; !');

                                }
                                formPart.el.unmask(); 
                            },
                            failure:function(response, options) {  
				var result = Ext.util.JSON.decode(response.responseText);
				Ext.Msg.alert('Erreur', result.errors.reason); 
                                ds.reload();
                                formPart.el.unmask(); 
                            }
                        }); 
                } else {
                  //do nothing
                }
            },this);
            return false;           
        } else {
            Ext.Msg.alert('Erreur','Vous devez s&eacute;lectionner un enregistrement');
        }
    }
    
    });
    

function reset(){

    includePanel.resetToNull();

}

function deleteligne(id){
    includePanel.deleteItem(id);
}

function save(){
    includePanel.saveItem();
}

var mapwin;

function closeImgWindow(){
	if(mapwin){
		mapwin.hide();
		mapwin= null;
	}
}

function openImgWindow(imageName, imageId){

		if(!mapwin){
			mapwin = new Ext.Window({
				layout: 'fit',
				closeAction: 'hide',
				width:164,
				height:222,
                                x:10,
                                cls:'whitebck',
				items: {
					xtype:'box',
					showAnimDuration:1
					,anchor:''
                                        ,cls:'whitebck'
					,isFormField:true
					,fieldLabel:'Image'
					,autoEl:{
                                            tag:'div', children:[{
                                                     tag:'img'
                                                    ,src:imageName+'.jpg'
                                            }]
					}
					
				}
			});					
		}        
        mapwin.show();
}


function genereLignes(){
       theGrid = Ext.getCmp("gridEnStock");
       dsliv.removeAll();
       itemsNumber = parseInt(includePanel.getForm().findField('quantite').getValue());
       idProduitConditionnement = parseInt(includePanel.getForm().findField('idProduitConditionnement').getValue());

      if (isNaN(itemsNumber) ||itemsNumber<=0 || isNaN(idProduitConditionnement) ||idProduitConditionnement<=0){
           Ext.Msg.alert("erreur", "Veuillez saisir un <b>produit</b> et une <b>quantite</b>");
      }else{

              Ext.getCmp("gridEnStock").stopEditing();

              for (i=0;i<itemsNumber;i++){

                  dsliv.add(new recliv(
                        {
                           idArticle:i+1,
                           CAS: includePanel.getForm().findField('CAS').getValue(),
                           reference:includePanel.getForm().findField('referenceFournisseur').getValue(),
                           idProduitConditionnement:includePanel.getForm().findField('idProduitConditionnement').getValue(),
                           nom:includePanel.getForm().findField('nomProduit').getValue(),
                           refrigere:includePanel.getForm().findField('produitRefrigere').getValue(),
                           identifiantEtiquette:''

                        }
                    ));
              }

              Ext.getCmp("gridEnStock").startEditing(0, 1);
      }

  }