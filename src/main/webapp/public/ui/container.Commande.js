/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2012/01/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/

var myTimer;

Ext.QuickTips.init();

    Ext.form.Field.prototype.msgTarget = 'side';

    var bd = Ext.getBody();


   
        
 // ---------------------------------------
 // GESTION DES DROITS
   Ext.Ajax.request({
        params:{
                controller:'Commande',
                action:'getRight'
            },
         url:'FrontController',
         success:function(response, options){
 
         },
         failure:function(response, options) {  
            Ext.getCmp("btn-enreg").hide();
            Ext.getCmp("btn-new").hide();
            Ext.getCmp("btn-enregSIFAC").hide();
            Ext.getCmp("btn-enregLivraison").hide();
            Ext.getCmp("btn-enregArticle").hide();


         }
    });

var proxy = new Ext.data.HttpProxy({
    url: 'FrontController',
    method: 'POST'
});


 // ---------------------------------------   
Ext.getUrlParam = function(param) {
    var params = Ext.urlDecode(location.search.substring(1));
    return param ? params[param] : params;
};
var idProParam = Ext.getUrlParam("idProduitConditionnement");
var idSsParam = Ext.getUrlParam("idSousStructure");

if (idProParam!==undefined && idSsParam !== undefined){

    // |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
    // Récupération de la sous structure
    var paramSousStructure = Ext.data.Record.create([
        {name: 'idSousStructure'},
        {name: 'code'},
        {name: 'nom'},
        {name: 'adresse1'},
        {name: 'ville'}
    ]);

    var paramReaderSousStructure = new Ext.data.JsonReader({
        root : "data",
        id : "idSousStructure"
    }, paramSousStructure);

    dsParamSousStructure = new Ext.data.JsonStore({
        proxy: proxy,
        fields: ['idSousStructure', 'code', 'nom', 'adresse1', 'ville'],
        root:'data',
        totalProperty: 'totalCount',
        reader: paramReaderSousStructure,
        nocache: true,
        baseParams:{action:'getListItems', controller:'SousStructure'}
    });
    dsParamSousStructure.load({params:{id:idSsParam},callback: function(records,o,s){
        if (records.length>0){
            includePanel.getForm().findField('nomSousStructure').setValue(records[0].get('nom'));
            includePanel.getForm().findField('idSousStructure').setValue(records[0].get('idSousStructure'));
        }
        }});
    // |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

    // |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
    // récupération du produit
    var paramSPRoduit = Ext.data.Record.create([
        {name: 'idProduitConditionnement'},
        {name: 'idProduit'},
        {name: 'substance'},
        {name: 'nom'},
        {name: 'nomFournisseur'},
        {name: 'idFournisseur'},
        {name: 'referenceFournisseur'},
        {name: 'contenance'},
        {name: 'refrigere'}

    ]);

    var paramReaderSPRoduit = new Ext.data.JsonReader({
        root : "data",
        id : "idProduitConditionnement"
    }, paramSPRoduit);


    var dsparamSPRoduit = new Ext.data.GroupingStore({
        proxy: proxy,
        fields: ['idProduitConditionnement', 'idProduit','substance', 'nom', 'nomFournisseur', 'idFournisseur', 'referenceFournisseur', 'contenance','refrigere'],
        root:'data',
        totalProperty: 'totalCount',
        reader: paramReaderSPRoduit,
        nocache: true,
        autoLoad: false,
        groupField:'substance',
        baseParams:{action:'getListProduitAvecConditionnement', controller:'Produit'}

    });
    dsparamSPRoduit.load({params:{idProduitConditionnement:idProParam},callback: function(records,o,s){
        if (records.length>0){

            var newRecord = new Ext.data.Record({
                idCommandeLigne:'0',
                idProduitConditionnement:records[0].get('idProduitConditionnement'),
                CAS: records[0].get('substance'),
                reference:records[0].get('referenceFournisseur'),
                idFournisseur:records[0].get('idFournisseur'),
                nom:records[0].get('nom'),
                quantiteLigne:1,
                contenance:records[0].get('contenance'),
                quantiteRecue:'0'

            });
            Ext.getCmp("basketGrid").getStore().add(newRecord);
        }
    }});


    // |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
}


// ----------------------------------------------------------
// TABLEAU - GRID 
// acces au données pour le tableau

    
    var rec = Ext.data.Record.create([
            {name: 'idCommande'},
            {name: 'dateCommande'},
            {name: 'idSousStructure'},
            {name: 'nomSousStructure'},
            {name: 'statut'},
            {name: 'idstatut'},
            {name: 'numBonDeCommandeInterne'},
            {name: 'dateCommandeInterne'},
            {name: 'supp'},
            {name: 'write'}
    ]);


    var reader = new Ext.data.JsonReader({
 	   root : "data",
 	   id : "idCommande"
    }, rec);


    var ds = new Ext.data.JsonStore({
        proxy: proxy,
        fields: ['idCommande', 'dateCommande','idSousStructure','nomSousStructure', 'quantiteRecue',
            'statut','idstatut','numBonDeCommandeInterne', 'dateCommandeInterne','supp','write'],
        root:'data',
        totalProperty: 'totalCount',
        reader: reader,
        nocache: true,
        autoLoad: false,
        baseParams:{action:'getListItems', controller:'Commande'}
        ,listeners:{
            load:function(){ 
                if(typeof(myMask) !== 'undefined')
                    myMask.hide();   
            }
        }
    });
    ds.load({params:{start:0, limit:10, period:1}});


    var idAireFavorite = 0;

    Ext.Ajax.request({
        url: 'FrontController',
        method: 'GET',
        success: function(response, conn, option){
            var ajaxResponse = Ext.util.JSON.decode(response.responseText);
            idAireFavorite = ajaxResponse.idAireStockage;
        },
        params: {action:'getAireStockageFavorite', controller:'Utilisateur'}
    });

    var colModel = new Ext.grid.ColumnModel([
        {id:'idCommande',header: "id", width: 40, dataIndex: 'idCommande'},
        {header: "Numero Cmde", width: 90, sortable: true, dataIndex: 'numBonDeCommandeInterne'},
        {header: "idSousStructure", width: 200, sortable: true, dataIndex: 'idSousStructure', hidden:true},
        {header: "Equipe/Département", width: 250, sortable: true, dataIndex: 'nomSousStructure', id:'nomSousStructure'},
        {header: 'Qté reçue', dataIndex: 'quantiteRecue', width: 130, 
            renderer: function(percentValue){
                var fontColor = 'red';
                if (percentValue==100){
                    fontColor = 'green';
                }else if (percentValue>=50){
                    fontColor = 'orange';
                }
                return "<span class='livraison-complete-bar' style='text-align:center;background:"+fontColor+";width:"+percentValue+"%'> "+percentValue+"% </span>";
         }},
        {header: "Date commande", width: 90, sortable: true, dataIndex: 'dateCommande'},
        {header: "Statut", width: 150, sortable: true, dataIndex: 'statut'},
        {header: "idstatut", width: 50, sortable: true, dataIndex: 'idstatut', hidden:true},
        {header: "DroitSuppression", width: 50, sortable: true, dataIndex: 'supp', hidden:true},
        {header: "dateCommandeInterne", width: 50, sortable: true, dataIndex: 'dateCommandeInterne', hidden:true},
        {header: "Annule", width: 50, sortable: false, xtype:'actioncolumn',
        items: [{
                    getClass: function(v, meta, rec) {
                        if (rec.get('supp') == 'true' && rec.get('write') == 'true'){
                            this.items[0].tooltip = 'Annuler la commande';
                            return 'del-col';
                        }
                    },
                    handler: function(grid, rowIndex, colIndex) {
                        var rec = ds.getAt(rowIndex);
                        annuleCommande(rec.get('idCommande'));
                    }
                }]}
    ]);


    var PagingBar = new pagingBarWithPeriodButton({
        pageSize: 10,
        store: ds,
        displayInfo: true,
        action:'getListItems', 
        controller:'Commande',
        displayMsg: 'Commandes {0} - {1} sur {2}',
        emptyMsg: 'Aucune commande'
    });   
    
    PagingBar.insertButton(11, periodButtons);
    PagingBar.insertButton(13, excelButton); 
   
    
// FIN TABLEAU - GRID
// ----------------------------------------------------------   
    
// ----------------------------------------------------------
//  Toolbar Tableau
     var exportBar = new Ext.Toolbar({
     cls: 'x-panel-header',
     height: 25,
     items: [  '->',{
                id:'editXLSArticle',
                iconCls:'icon-excel',
                value: '',
                handler:function(){
                    var value = includePanel.getForm().findField('idCommande').getValue();
                    if (value>0){
                        printAction("Commande","getArticleXLS", value);
                    }else{
                        Ext.Msg.alert('Export impossible','Veuillez selectionner un item à éditer');
                        return;
                    }
                }            
            }]

    });
// ---------------------------------------------------------- 

// ---------------------------------------------------------- 
//  Toolbar Tableau
    var formTbar = new Ext.Toolbar({
     height: 27,
     items: [{text: 'Nouvelle commande',
                iconCls: 'icon-add',
                id:'btn-new',
                handler : reset
               },'-',
             {
                xtype: 'displayfield',
                value: ' Etat de la commande : '
            } ,' ' ,       
            {
                xtype: 'displayfield',
                name: 'statut',
                id:'statut',
                value: 'Nouvelle commande',
                cls:'bold tallSize'
            } ,' ' 
               ,'->',{
                id:'editXLS',
                iconCls:'icon-excel',
                value: '',
                handler:function(){
                    var value = includePanel.getForm().findField('idCommande').getValue();
                    if (value>0){
                        printAction("Commande","getXLSList", value);
                    }else{
                        Ext.Msg.alert('Export impossible','Veuillez selectionner un item à éditer');
                        return;
                    }
                }            
            },{
                id:'edit',
                iconCls:'icon-print',
                value: '',
                handler:function(){
                    var value = includePanel.getForm().findField('idCommande').getValue();
                    if (value>0){
                        printAction("Commande","getPDF", value);
                    }else{
                        Ext.Msg.alert('Export impossible','Veuillez selectionner un item à éditer');
                        return;
                    }
                }            
            }

]
    });
    
        
// ---------------------------------------------------------- 

function disableGeneralsFields(){
        Ext.getCmp("CommandeForm").getForm().findField('nomSousStructure').setReadOnly(true);
        Ext.getCmp("CommandeForm").getForm().findField('nomSousStructure').addClass("x-item-disabled");
        Ext.getCmp("CommandeForm").getForm().findField('idSousStructure').setReadOnly(true);
        Ext.getCmp("CommandeForm").getForm().findField('idSousStructure').addClass("x-item-disabled");
        Ext.getCmp("CommandeForm").getForm().findField('dateCommande').setReadOnly(true);
        Ext.getCmp("CommandeForm").getForm().findField('dateCommande').addClass("x-item-disabled");
        Ext.getCmp("CommandeForm").getForm().findField('cloture').disable();
        Ext.getCmp("addProduit").disable();
        Ext.getCmp("basketGrid").disable();
        Ext.getCmp("searchSousStructure").disable();
        Ext.getCmp("btn-enreg").disable();

}

function disableCommandeInterneFields(){
    Ext.getCmp("CommandeForm").getForm().findField('dateCommandeInterne').setReadOnly(true);
    Ext.getCmp("CommandeForm").getForm().findField('dateCommandeInterne').addClass("x-item-disabled");
    Ext.getCmp("CommandeForm").getForm().findField('numBonDeCommandeInterne').setReadOnly(true);
    Ext.getCmp("CommandeForm").getForm().findField('numBonDeCommandeInterne').addClass("x-item-disabled");
    Ext.getCmp("CommandeForm").getForm().findField('commandeEnvoyee').disable();
    Ext.getCmp("btn-enregSIFAC").disable();
}

function disableLivraisonFields(){
    Ext.getCmp("CommandeForm").getForm().findField('dateLivraison').setReadOnly(true);
    Ext.getCmp("CommandeForm").getForm().findField('dateLivraison').addClass("x-item-disabled");
    Ext.getCmp("CommandeForm").getForm().findField('numeroBonLivraison').setReadOnly(true);
    Ext.getCmp("CommandeForm").getForm().findField('numeroBonLivraison').addClass("x-item-disabled");
    Ext.getCmp("gridLivraison").getColumnModel().setEditable(4, false);
    Ext.getCmp("gridLivraison").getColumnModel().setEditable(5, false);
    Ext.getCmp("btn-enregLivraison").disable();
}

function cleanLivraisonFields(){
    Ext.getCmp("CommandeForm").getForm().findField('numeroBonLivraison').setValue("");
    Ext.getCmp("CommandeForm").getForm().findField('dateLivraison').setValue(new Date());
    Ext.getCmp("gridLivraison").getStore().removeAll();
}

function disableAllFields(){
    disableGeneralsFields();
    disableCommandeInterneFields();
    disableLivraisonFields();
    Ext.getCmp("btn-enreg").disable();

}

function enableGeneralsFields(){
    Ext.getCmp("CommandeForm").getForm().findField('nomSousStructure').setReadOnly(false);
    Ext.getCmp("CommandeForm").getForm().findField('nomSousStructure').removeClass("x-item-disabled");
    Ext.getCmp("CommandeForm").getForm().findField('idSousStructure').setReadOnly(false);
    Ext.getCmp("CommandeForm").getForm().findField('idSousStructure').removeClass("x-item-disabled");
    Ext.getCmp("CommandeForm").getForm().findField('dateCommande').setReadOnly(false);
    Ext.getCmp("CommandeForm").getForm().findField('dateCommande').removeClass("x-item-disabled");
    Ext.getCmp("CommandeForm").getForm().findField('cloture').enable();   
    Ext.getCmp("searchSousStructure").enable();
    Ext.getCmp("addProduit").enable();
    Ext.getCmp("basketGrid").enable();    
    Ext.getCmp("btn-enreg").enable();
}

function enableCommandeInterneFields(){
    Ext.getCmp("CommandeForm").getForm().findField('dateCommandeInterne').setReadOnly(false);
    Ext.getCmp("CommandeForm").getForm().findField('dateCommandeInterne').removeClass("x-item-disabled");
    Ext.getCmp("CommandeForm").getForm().findField('numBonDeCommandeInterne').setReadOnly(false);
    Ext.getCmp("CommandeForm").getForm().findField('numBonDeCommandeInterne').removeClass("x-item-disabled");
    Ext.getCmp("CommandeForm").getForm().findField('commandeEnvoyee').enable();        
    Ext.getCmp("btn-enregSIFAC").enable();
}

function enableLivraisonFields(){
    Ext.getCmp("CommandeForm").getForm().findField('dateLivraison').setReadOnly(false);
    Ext.getCmp("CommandeForm").getForm().findField('dateLivraison').removeClass("x-item-disabled");
    Ext.getCmp("CommandeForm").getForm().findField('numeroBonLivraison').setReadOnly(false);
    Ext.getCmp("CommandeForm").getForm().findField('numeroBonLivraison').removeClass("x-item-disabled");
    Ext.getCmp("gridLivraison").getColumnModel().setEditable(4, true);
    Ext.getCmp("gridLivraison").getColumnModel().setEditable(5, true);
    Ext.getCmp("btn-enregLivraison").enable();
}

function enableAllFields(){
    enableGeneralsFields();
    enableCommandeInterneFields();
    enableLivraisonFields();
    

}

    
    
    var includePanel = new Ext.FormPanel({
        id: 'CommandeForm',
        controller:'Commande',
        url: 'FrontController',
        frame: true,    
        labelAlign: 'left',
        waitMsgTarget: true,
        width: 1021,
        layout: 'border',    // Specifies that the items will now be arranged in columns
        height:645,
        baseParams:{action:'saveItem', controller:'Commande'},
        items: [{
            xtype: 'grid',
            id:'gridP',     	
            margins: '5 0 5 0',
            region: 'north',
            height:287,
            bbar: PagingBar , 
            enableColumnHide:false,
            loadMask:{msg: 'Chargement...'},
            ds: ds,
            cm: colModel,
            sm: new Ext.grid.RowSelectionModel({
                singleSelect: true,
                listeners: {
                    rowselect: function(sm, row, rec) {

                        Ext.getCmp("gridLivraison").getStore().removeAll();

                        var formPart = Ext.getCmp("formPart");
                        formPart.el.mask('Chargement...', 'x-mask-loading');
                        Ext.getCmp("CommandeForm").getForm().loadRecord(rec);
                        
                        // Chargement des détails de la commande
                        var basketGrid = Ext.getCmp("basketGrid");
                        var basketGridStore = basketGrid.getStore();                                
                        basketGrid.stopEditing();
                        basketGridStore.load({params:{idCommande:rec.get('idCommande')},
                            callback:function(records,o,s){
                                if (!s)
                                    Ext.getCmp("basketGrid").getStore().removeAll();
                                                         
                        }});                                                              
                        // Fin chargement                        
                        

                        switch(rec.get('idstatut')){
                            // la commande est ouverte, on peut ajouter/supprimer des articles
                            case "1":
                                Ext.getCmp("formPart").setActiveTab("TabCommande");
                                // on netoye les champs livraison
                                cleanLivraisonFields();
                                // on desactive l'onglet reception et sifac
                                Ext.getCmp("TabLivraison").disable();         
                                Ext.getCmp("TabSifac").disable();  
                                Ext.getCmp("TabArticle").disable();  
                                                               
                                // si supp=true=> la commande est faite par l'utilisateur
                                // il peut donc la supprimer et modifier les informations sur la commande
                                // sinon, on désactive les champs des infos générales mais pas ceux sifac
                                if (rec.get('supp')=='true'){
                                    enableAllFields();
                                }else{
                                    disableGeneralsFields();
                                }
                                Ext.getCmp('statut').setValue(rec.get('statut'));
                                break;                                
                            
                            // la commande est en attente saisie du bon de commande sifac
                            case "2":
                                Ext.getCmp("TabSifac").enable();
                                Ext.getCmp("formPart").setActiveTab("TabSifac");
                                Ext.getCmp("TabArticle").disable();  
                                // on netoye les champs livraison
                                cleanLivraisonFields();
                                // on desactive l'onglet reception
                                Ext.getCmp("TabLivraison").disable();
                                // si supp=true=> la commande est faite par l'utilisateur
                                // il peut donc la supprimer et modifier les informations sur la commande
                                // sinon, on désactive les champs des infos générales mais pas ceux sifac
                                enableCommandeInterneFields();
                                disableGeneralsFields();

                                Ext.getCmp('statut').setValue(rec.get('statut'));
                                break;
                            // la commande a été réalisé et envoyé : on attend la réception
                            case "3":
                                Ext.getCmp("TabSifac").enable();
                                // on active l'onglet livraison
                                Ext.getCmp("TabLivraison").enable();
                                Ext.getCmp("formPart").setActiveTab("TabLivraison");
                                Ext.getCmp("TabArticle").disable();  
                                // on netoye les champs livraison
                                cleanLivraisonFields();
                                // on ne peut plus modifier les informations générales
                                disableGeneralsFields();
                                disableCommandeInterneFields();
                                enableLivraisonFields();
                                Ext.getCmp('statut').setValue(rec.get('statut'));
                                
                                Ext.getCmp("gridLivraison").getGridEl().mask('chargement...', 'x-mask-loading');
                                Ext.getCmp("gridLivraison").stopEditing();
                                Ext.getCmp("gridLivraison").getStore().load({

                                    params:{idCommande:rec.get('idCommande')},
                                    callback: function(records,o,s){
                                        Ext.getCmp("gridLivraison").getGridEl().unmask();
                                        Ext.getCmp("gridLivraison").getColumnModel().setEditable(4, false);
                                        Ext.getCmp("gridLivraison").getColumnModel().setEditable(5, false);
                                    }
                                    })
                                    
                                Ext.getCmp("gridArticle").getGridEl().mask('chargement...', 'x-mask-loading');
                                Ext.getCmp("gridArticle").stopEditing();
                                Ext.getCmp("gridArticle").getStore().load({

                                    params:{idCommande:rec.get('idCommande')},
                                    callback: function(records,o,s){
                                        Ext.getCmp("gridArticle").getGridEl().unmask();
                                        Ext.getCmp("gridArticle").getColumnModel().setEditable(5, false);
                                        if (Ext.getCmp("gridArticle").getStore().getTotalCount()>0){
                                            Ext.getCmp("TabArticle").enable(); 
                                        }
                                    }
                                    })                                   
                                
                                
                                break;
                            // la commande est livrée : on ne peut plus rien changer
                            case "4": // LIVREE
                                
                                Ext.getCmp("TabSifac").enable();
                                Ext.getCmp("TabLivraison").enable();
                                Ext.getCmp("TabArticle").enable();  
                                Ext.getCmp("formPart").setActiveTab("TabArticle");
                                cleanLivraisonFields();
                                disableAllFields();

                                Ext.getCmp('statut').setValue(rec.get('statut'));
                                
                                Ext.getCmp("gridArticle").getGridEl().mask('chargement...', 'x-mask-loading');
                                Ext.getCmp("gridArticle").stopEditing();
                                Ext.getCmp("gridArticle").getStore().load({

                                    params:{idCommande:rec.get('idCommande')},
                                    callback: function(records,o,s){
                                        Ext.getCmp("gridArticle").getGridEl().unmask();
                                        Ext.getCmp("gridArticle").getColumnModel().setEditable(5, false);
                                    }
                                    })
                                
                                break;
                            case "5": // ANNULEE
                                // on netoye les champs livraison
                                cleanLivraisonFields();
                                Ext.getCmp("TabLivraison").disable();
                                disableAllFields();
                                Ext.getCmp('statut').setValue(rec.get('statut'));
                                break;
                        }
                        formPart.el.unmask();
                    }
                }
            }),
            autoExpandColumn: 'nomSousStructure',
            title:'Commandes',
            border: true
        },{
        xtype:'tabpanel',
        id:'formPart',
        activeTab: 0,
        width:600,
        region: 'center',
        height:332,
        plain:true,
        deferredRender:false,
        items:[{

//------------> ONGLET 1
            id:'TabCommande',
            title:'Commande',
            layout:'form',
            displayFloatingMsg:true,
            loadMask:{msg: 'Chargement...'},  
            bbar:new Ext.Toolbar({height:27,id:'bottom-bar',cls:'x-bbar',items:['->', {id:'btn-enreg', text:'Enregistrer',iconCls:'icon-save',handler:save}]}),
            tbar: formTbar,
            margins: '0 5 5 5',
            baseCls:'x-panel-mc',
            cls:'x-panel-mc-with-tbar',
            labelWidth: 190,
            defaults: {border:false},
            defaultType: 'textfield',
            labelSeparator:'',
            bodyStyle: Ext.isIE ? 'padding:0 0 5px 15px;' : 'padding:10px 15px;',

            items: [
                    {
                      xtype: 'compositefield',
                      fieldLabel: 'Equipe/Département *',
                      name:'equipeComposite',
                      width:493,
                      items: [
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Equipe/Département',
                            name: 'nomSousStructure',
                            allowBlank: false,
                            readOnly:true,
                            blankText:'Le champ est requis',
                            width:300
                         },
                         {
                          xtype:'button',
                          text:'Selectionner une Equipe/Département',
                          windowName:'searchSousStructure',
                          id:'searchSousStructure',
                          handler:function(){
                              var mySearchWin = eval(this.windowName);
                              mySearchWin.show();
                          }
                         }
                    ]},
                {
                xtype: 'datefield',
                fieldLabel: 'Date de commande *',
                name: 'dateCommande',
                startDay:1,
                allowBlank: false,
                value:new Date(),
                format:'d/m/Y',
                blankText:'Le champ est requis',                
                width:100
            },{
                        xtype:'checkbox',
                        fieldLabel: 'Cloturer',
                        name: 'cloture',
                        inputValue:'true',
                        boxLabel:'Cloturer la commande (Modifications ulterieures impossibles et email envoyé au secrétartiat)',
                        checked: false
             },
                {
                      xtype: 'compositefield',
                      fieldLabel: 'Produit *',
                      name:'produitComposite',
                      width:424,
                      items: [
                        {
                            xtype: 'displayfield',
                            hidden:true
                         },
                         {
                          xtype:'button',
                          text:'Ajouter un Produit',
                          windowName:'addProduit',
                          id: 'addProduit',
                          handler:function(){
                              if (includePanel.getForm().findField('idSousStructure').getValue()==null || includePanel.getForm().findField('idSousStructure').getValue()==0){
                                  Ext.Msg.alert('Erreur', 'Veuillez choisir une équipe ou un département');
                                  return;
                              }
                              var mySearchWin = eval(this.windowName);
                              mySearchWin.show();
                          }
                         }
                    ]},                
            {
              name: 'idSousStructure',
              hidden:true
             },
            {
              name: 'commandeLigne',
              hidden:true
            },
             {
              name: 'idStatut',
              id:'idStatut',
              hidden:true
            }
            ,
            {
              name: 'idCommande',
              hidden:true
            },{
                xtype: 'editorgrid',
                id:'basketGrid',
                margins: '0 5 5 5',
                region: 'south',
                clicksToEdit: 1,
                height:137,
                enableColumnHide:false,
                loadMask:{msg: 'Chargement...'},
                sm: new Ext.grid.RowSelectionModel({}),
                border: true,
                ds: new Ext.data.JsonStore({
                        proxy: proxy,
                        fields: ['idCommandeLigne','idProduitConditionnement','CAS','reference','idFournisseur','nom', 'quantiteLigne', 'contenance','quantiteRecue'],
                        root:'data',
                        autoSave:false,
                        totalProperty: 'totalCount',
                        reader: new Ext.data.JsonReader({root : "data",id : "idCommandeLigne"}),
                        nocache: true,
                        autoLoad: false,
                        baseParams:{action:'getJsonCommandeLigne', controller:'Commande'}
                    }),
                cm:     new Ext.grid.ColumnModel({columns: [
                    {dataIndex:'idCommandeLigne', hidden:true},
                    {dataIndex:'idProduitConditionnement',hidden:true},
                    {header: 'CAS', dataIndex: 'CAS', width: 100, sortable:true},
                    {header: 'Reference', dataIndex: 'reference', width: 200},
                    {dataIndex:'idFournisseur',hidden:true},
                    {header: 'Nom', dataIndex: 'nom', width: 300},
                    {header: 'Qté', dataIndex: 'quantiteLigne', width: 50, editor: new Ext.form.NumberField({
                        xtype:'numberfield',
                        allowDecimals:false,
                        css:'background-image:url(lib/extjs/resources/images/default/grid/dirty.gif);background-repeat:no-repeat;'
                    })                    
                    },
                    {header: 'Contenance', dataIndex: 'contenance', width: 100},
                    {header: 'Qté reçue', dataIndex: 'quantiteRecue', width: 150, 
                        renderer: function(percentValue){
                            var fontColor = 'red';
                            if (percentValue==100){
                                fontColor = 'green';
                            }else if (percentValue>=50){
                                fontColor = 'orange';
                            }
                            return "<span class='livraison-complete-bar' style='text-align:center;background:"+fontColor+";width:"+percentValue+"%'> "+percentValue+"% </span>";
                     }}, {header: "Supp", width: 50, sortable: false, xtype:'actioncolumn',
                    items: [{
                                getClass: function(v, meta, rec) {
                                        this.items[0].tooltip = 'Supprimer ce produit';
                                        return 'del-col';
                                },
                                handler: function(grid, rowIndex, colIndex) {
                                    
                                    Ext.MessageBox.confirm('Etes-vous sur ?','Voulez-vous supprimer cet article de la commande &nbsp;?',function(btn) {
                                        if(btn == 'yes') {
                                            var rec = Ext.getCmp('basketGrid').getStore().getAt(rowIndex);
                                            // si idCommandeLigne==0 c'est que la ligne n'a pas été insérée, on ne supprime que dans le store local
                                            if (rec.get('idCommandeLigne')==0){
                                                Ext.getCmp('basketGrid').getStore().removeAt(rowIndex);
                                            // sinon il faut supprimer dans la base et recharger les lignes
                                            }else{

                                                    Ext.Ajax.request({
                                                    method:'POST',
                                                    url: 'FrontController',
                                                    params: {action:'supprimeCommandeLigne', controller:'Commande', 
                                                        idCommandeLigne:rec.get('idCommandeLigne')},
                                                 callback: function(option,success,response){
                                                     if (success && response.responseText!=''){
                                                         Ext.getCmp('basketGrid').getStore().reload();
                                                     }else{
                                                         var reason = 'Raison inconnue';
                                                         if (response.responseText!=''){
                                                            var result = Ext.util.JSON.decode(response.responseText);
                                                            reason = result.errors.reason;
                                                         }
                                                         Ext.Msg.alert('Erreur', 'Impossible de supprimer le produit : ' + reason);
                                                    }
                                                }});

                                            }
                                        }
                                    },this);
                                }
                            }]}
                    
                    ]}),
                writer:new Ext.data.JsonWriter({encode: true, writeAllFields: false})
                }

            ]
        },{
//------------> FIN ONGLET 1
////------------> ONGLET 2
            id:'TabSifac',
            title:'Bon de Commande',
            disabled:true,
            layout:'form',
            displayFloatingMsg:true,
            loadMask:{msg: 'Chargement...'},  
            bbar:new Ext.Toolbar({height:27,id:'bottom-bar-2',cls:'x-bbar',items:['->', {id:'btn-enregSIFAC', text:'Enregistrer',iconCls:'icon-save',handler:save}]}),
            margins: '0 5 5 5',
            baseCls:'x-panel-mc',
            cls:'x-panel-mc-with-tbar',
            labelWidth: 190,
            defaults: {border:false},
            defaultType: 'textfield',
            labelSeparator:'',
            bodyStyle: Ext.isIE ? 'padding:0 0 5px 15px;' : 'padding:10px 15px;',

            items: [

   
            {
                xtype: 'textfield',
                fieldLabel: 'Numero bon de commande',
                name: 'numBonDeCommandeInterne',
                maxLength:50,
                width:200,
                plugins:new Ext.ux.plugin.triggerfieldTooltip(),
                tooltip: {
                    text: 'A l\'enregistrement, si un numero de commande est saisi, la commande passera a l\'état \'en attente de livraison\''
                }
             },
                        {
                            xtype: 'datefield',
                            fieldLabel: 'Date de saisie',
                            name: 'dateCommandeInterne',
                            startDay:1,
                            allowBlank: true,
                            format:'d/m/Y',
                            blankText:'Le champ est requis',
                            width:100
                            }
                ,{
                        xtype:'checkbox',
                        fieldLabel: 'Commande Envoyée',
                        name: 'commandeEnvoyee',
                        inputValue:'true',
                        boxLabel:'(Modifications ulterieures impossibles et email envoyé au commanditaire)',
                        checked: false
             }
            ]
        }
//------------> FIN ONGLET 2
//------------> ONGLET 3

            ,{
            id:'TabLivraison',
            disabled:true,
            title:'Livraison',
            layout:'border',
            displayFloatingMsg:true,
            loadMask:{msg: 'Chargement...'},
            bbar:new Ext.Toolbar({height:27,id:'bottom-bar-3',cls:'x-bbar',items:['->', {id:'btn-enregLivraison', text:'Enregistrer',iconCls:'icon-save',handler:saveLiv}]}),
            //tbar :new Ext.Toolbar({height:27,items:[{text: ' ', iconCls: 'icon-excel',handler : exportExcel}]}),                         
            autoScroll:true,
            margins: '0 5 5 5',
            baseCls:'x-panel-mc',
            cls:'x-panel-mc-with-tbar',
            labelWidth: 190,
            defaults: {border:false},
            defaultType: 'textfield',
            labelSeparator:'',

            items: [



            {
                                displayFloatingMsg:true,
                                split:true,
                                margins: '0 0 0 0',
                                columnWidth: 0.9,
                                region: 'center',
                                width:600,
                                xtype: 'fieldset',
                                loadMask:{msg: 'Chargement...'},
                                labelWidth: 190,
                                defaults: {width: 140, border:false},    // Default config options for child items
                                defaultType: 'textfield',
                                labelSeparator:'',
                                bodyStyle: Ext.isIE ? 'padding:0 0 0px 15px;' : 'padding:0px 15px;',

                                items: [{
                                    fieldLabel: 'Numero bon de livraison *',
                                    name: 'numeroBonLivraison',
                                    //allowBlank: false,
                                    blankText:'Le champ est requis'
                                },
                                {
                                xtype: 'datefield',
                                fieldLabel: 'Date de livraison *',
                                name: 'dateLivraison',
                                startDay:1,
                                //allowBlank: false,
                                value:new Date(),
                                format:'d/m/Y',
                                blankText:'Le champ est requis',                
                                width:100
                            }
                             ,{
                                  name: 'livraisonLigne',
                                  hidden:true
                                },{
                                  name: 'stockLigne',
                                  hidden:true
                                },{
                                  name: 'ArticleList',
                                  hidden:true
                                }
                            ]


            },{
                xtype: 'editorgrid',
                id:'gridLivraison',
                margins: '0 5 5 5',
                region: 'south',
                clicksToEdit: 1,
                height:180,
                enableColumnHide:false,
                loadMask:{msg: 'Chargement...'},
                sm: new Ext.grid.RowSelectionModel({}),
                border: true,
                ds: new Ext.data.JsonStore({
                        proxy: proxy,
                        fields: ['idCommandeLigne','idProduitConditionnement','CAS','reference','nom', 'quantiteLigne', 'contenance','refrigere','quantiteResteALivrer','supp','write'],
                        root:'data',
                        autoSave:false,
                        totalProperty: 'totalCount',
                        reader: new Ext.data.JsonReader({root : "data",id : "idCommandeLigne"}),
                        nocache: true,
                        autoLoad: false,
                        baseParams:{action:'getJsonCommandeLigne', afficheQteZero:false, controller:'Commande'}
                    }),
                cm:     new Ext.grid.ColumnModel({columns: [
                    {dataIndex:'idCommandeLigne', hidden:true},
                    {dataIndex:'idProduitConditionnement',hidden:true},
                    {header: 'CAS', dataIndex: 'CAS', width: 100, sortable:true},
                    {header: 'Reference', dataIndex: 'reference', width: 190},
                    {header: 'Nom', dataIndex: 'nom', width: 280},
                    {header: 'Qté commandée', dataIndex: 'quantiteLigne', width: 80},
                    {header: 'Contenance', dataIndex: 'contenance', width: 100},
                    {header: 'refrigere', dataIndex: 'refrigere',hidden:true},
                    {header: 'Reste à livrer', dataIndex: 'quantiteResteALivrer', width: 90},
                    {header: 'Qté Livrée', dataIndex: 'quantiteLivraison', sortable:false, width: 90, editor: new Ext.form.NumberField({
                        xtype:'numberfield',
                        allowDecimals:false
                    }),css:'background-image:url(lib/extjs/resources/images/default/grid/dirty.gif);background-repeat:no-repeat;'},
                    {header: "Annule", width: 40, sortable: false, xtype:'actioncolumn',
                    items: [{
                                getClass: function(v, meta, rec) {
                                    if (rec.get('supp') == 'true' && rec.get('write') == 'true'){
                                        this.items[0].tooltip = 'Annuler la ligne';
                                        return 'del-col';
                                    }
                                },
                                handler: function(grid, rowIndex, colIndex) {
                                    
                                    Ext.MessageBox.confirm('Etes-vous sur ?','Voulez-vous supprimer cet article de la commande &nbsp;?',function(btn) {
                                        if(btn == 'yes') {
                                            var rec = Ext.getCmp('gridLivraison').getStore().getAt(rowIndex);
                                            // si idCommandeLigne==0 c'est pas normal
                                            if (rec.get('idCommandeLigne')==0){
                                                Ext.Msg.alert('Erreur', 'Impossible de supprimer le produit : ' + reason);
                                            }else{

                                                    Ext.Ajax.request({
                                                    method:'POST',
                                                    url: 'FrontController',
                                                    params: {action:'supprimeCommandeLigne', controller:'Commande', 
                                                        idCommandeLigne:rec.get('idCommandeLigne')},
                                                 callback: function(option,success,response){
                                                     if (success && response.responseText!=''){
                                                         Ext.getCmp('gridLivraison').getStore().reload();
                                                         Ext.msg.show(this.title ? this.title : 'Op&eacute;ration r&eacute;ussie:', 'Enregistrement effac&eacute; !');
                                                     }else{
                                                         var reason = 'Raison inconnue';
                                                         if (response.responseText!=''){
                                                            var result = Ext.util.JSON.decode(response.responseText);
                                                            reason = result.errors.reason;
                                                         }
                                                         Ext.Msg.alert('Erreur', 'Impossible de supprimer le produit : ' + reason);
                                                    }
                                                }});

                                            }
                                        }
                                    },this);
                                }
                            }]}            
        
                ]}),
                writer:new Ext.data.JsonWriter({encode: true, writeAllFields: false})
                }         
          ]}


//------------> FIN ONGLET 3
//------------> DEBUT ONGLET 4
,{
            id:'TabArticle',
            disabled:true,
            title:'Articles',
            layout:'border',
            displayFloatingMsg:true,
            loadMask:{msg: 'Chargement...'},
            bbar:new Ext.Toolbar({height:27,id:'bottom-bar-4',cls:'x-bbar',items:['->', {id:'btn-enregArticle', text:'Enregistrer',iconCls:'icon-save',handler:saveArt}]}),
            //tbar :new Ext.Toolbar({height:27,items:[{text: ' ', iconCls: 'icon-excel',handler : exportExcel}]}),                         
            autoScroll:true,
            margins: '0 5 5 5',
            baseCls:'x-panel-mc',
            cls:'x-panel-mc-with-tbar',
            labelWidth: 190,
            defaults: {border:false},
            defaultType: 'textfield',
            labelSeparator:'',
            tbar:exportBar,
            items: [



            {
  
                xtype: 'editorgrid',
                id:'gridArticle',
                margins: '0 5 5 5',
                region: 'center',
                clicksToEdit: 1,
                height:180,
                enableColumnHide:false,
                loadMask:{msg: 'Chargement...'},
                sm: new Ext.grid.RowSelectionModel({}),
                border: true,
                ds: new Ext.data.JsonStore({
                        proxy: proxy,
                        fields: ['idArticle','CAS','reference','nom', 'nomAire','contenance','identifiantEtiquette','scan'],
                        root:'data',
                        autoSave:false,
                        totalProperty: 'totalCount',
                        reader: new Ext.data.JsonReader({root : "data",id : "idArticle"}),
                        nocache: true,
                        autoLoad: false,
                        baseParams:{action:'getListItems', controller:'Article'}
                    }),
                cm:     new Ext.grid.ColumnModel({columns: [
                    {dataIndex:'idArticle', hidden:true},                           
                    {header: 'CAS', dataIndex: 'CAS', width: 70, sortable:true},
                    {header: 'Reference', dataIndex: 'reference', width: 100, sortable:true},                                
                    {header: 'Nom', dataIndex: 'nom', width: 250, sortable:true},
                    {header: 'Local', dataIndex: 'nomAire', width: 280, sortable:true},
                    {header: 'Contenance', dataIndex: 'contenance', width: 100, sortable:true},
                    {header: 'Etiquette', dataIndex: 'identifiantEtiquette', width: 100, editor: new Ext.form.TextField({
                        xtype:'textfield',
                        maxLength:50
                    }),css:'background-image:url(lib/extjs/resources/images/default/grid/dirty.gif);background-repeat:no-repeat;'}
                    ,{header: "scan", dataIndex:'scan', width: 40, sortable: false, xtype:'actioncolumn',
                    items: [{
                    getClass: function(v, meta, rec) {          // Or return a class from a function
                            this.items[0].tooltip = 'Scanner une étiquette';
                            return 'rfid-col';
                    },
                        handler: function(grid, rowIndex, colIndex) {
                            //window.open ("popupnfc.html?row="+rowIndex, "Scanner une étiquette", "height=220, width=250, top=100, left=100, toolbar=no, menubar=yes, location=no, resizable=yes, scrollbars=no, status=no");                             
                             Ext.DomHelper.overwrite('iframediv', {
                                    tag: 'iframe',
                                    id:'appletIFrame',
                                    frameBorder: 0,
                                    width: 0,
                                    height: 0,
                                    css: 'display:none;visibility:hidden;height:0px;',
                                    //css: 'height:50px;',
                                    src: '../applet/popupnfc.html?row='+rowIndex+"&origine=Commande"
                                });
                                appletWin.show();
                                appletWin.el.mask('Chargement, veuillez patienter...', 'x-mask-loading');
                                console.log('toto');
                            
                            
                        }
                    }]}            
        
                   ]}),
                writer:new Ext.data.JsonWriter({encode: true, writeAllFields: false})
                }         
          ]}

        ]
            
        }],
        saveArt:function(){
        
                        var gridArticle = Ext.getCmp('gridArticle');
                        // si pas de ligne : on annule
                        if (gridArticle.getStore().getCount()==0){
                             Ext.Msg.alert('Erreur', "Il n'y a pas d\'Article");
                                return;
                        }
                        
                        // CONVERSION DE LA GRILLE DES LIVRAION SON JSON
                        gridArticle.stopEditing();
                        var recordsToSend = gridArticle.getStore().getRange();
                        var farray = [];
                        Ext.each(recordsToSend, function(aRecord) {   
                            var tab = {idArticle:aRecord.get('idArticle'),'identifiantEtiquette':aRecord.get('identifiantEtiquette')};
                            farray.push(tab);
                        });                 
                        var JSONToSend = Ext.encode(farray)
                        includePanel.getForm().findField('ArticleList').setValue(JSONToSend);
                        Ext.Ajax.request({
                            params:{
                                controller:'Article',                            
                                action:'saveItem',
                                articleList:JSONToSend
                            },
                            url:'FrontController',
                             success:function(response, options){
                                if (response.responseText) {						
                                    if (response.responseText.substr(0,1) == '{') {
			    	        var ajaxResponse = Ext.util.JSON.decode(response.responseText);
                                        if (ajaxResponse.success==false){
                                            Ext.Msg.alert('Erreur', ajaxResponse.errors.reason);
                                        }else{
                                            Ext.msg.show(this.title ? this.title : 'Op&eacute;ration r&eacute;ussie:', 'Enregistrement modifi&eacute; !');  
                                        }
				     }
	                        }
                            },
                            failure:function(response, options) {  
				var result = Ext.util.JSON.decode(response.responseText);
				
                            }
                        }); 
                       
                        
        
        },
        saveLiv:function(){

                    var erreurQte = false;
                    var qteTotaleLivree=0;

                     // si un bon de livraison et une date de livrasion sont saisis
                    if (includePanel.getForm().findField('numeroBonLivraison').getValue()!=""
                        && includePanel.getForm().findField('dateLivraison').getValue()!=""){

                        
                        var gridLivraison = Ext.getCmp('gridLivraison');
                        // si pas de ligne : on annule
                        if (gridLivraison.getStore().getCount()==0){
                             Ext.Msg.alert('Erreur', "Il n'y a pas de ligne de livraison");
                                return;
                        }

                        // CONVERSION DE LA GRILLE DES LIVRAION SON JSON
                        gridLivraison.stopEditing();
                        var recordsToSend = gridLivraison.getStore().getRange();
                        var farray = [];
                        Ext.each(recordsToSend, function(aRecord) {
                            if (isNaN(aRecord.get('quantiteLivraison')) || aRecord.get('quantiteLivraison')>aRecord.get('quantiteResteALivrer')){
                                erreurQte = true;
                                return;
                            }
                            qteTotaleLivree+=aRecord.get('quantiteLivraison');                            
                            farray.push(aRecord.data);
                        });

                        if (erreurQte == true){
                            Ext.Msg.alert('Erreur', "Vous devez saisir une quantité livrée inférieure ou égale à la quantité restant à livrer");                            
                            return;
                        }
                        
                        if (qteTotaleLivree==0){
                            Ext.Msg.alert('Erreur', "Il n'y a aucun article livré");                            
                            return;             
                        }

                        var JSONToSend = Ext.encode(farray);  
                        includePanel.getForm().findField('livraisonLigne').setValue(JSONToSend);
                        // FIN conversion
                        
                        // Ouverture de la fenetre pour entrer les produits dans le stock
                        windowEntreeEnStock.show()

                        


                    }else{
                        Ext.Msg.alert('Erreur', "Veuillez saisir un numéro de BL et une date de livraison");                            
                        return;  
                    }


        
        
        },
        saveItem:function(){
            if (!includePanel.getForm().isValid()){
                    return;
            }
                              
            
                // -----------------------------------------------------------------------------
                // Traitement des Produit Commandés
                    var basketGrid = Ext.getCmp("basketGrid");
                    var basketGridStore = basketGrid.getStore();
                    var erreurQte = false;
                    
                    if (basketGridStore.getCount()==0){
                         Ext.Msg.alert('Erreur', "Vous devez choisir au moins un Produit");
                            return;
                    }
                    // CONVERSION DE LA GRILLE DES LIVRAION SON JSON
                    basketGrid.stopEditing();
                    var productToSend = basketGridStore.getRange();
                    var prodArray = [];
                    Ext.each(productToSend, function(aRecord) {
                        if (aRecord.get('quantiteLigne')==undefined || isNaN(aRecord.get('quantiteLigne')) || aRecord.get('quantiteLigne') < 0){
                            Ext.Msg.alert('Erreur', "Vous devez saisir une quantite pour chaque produit");
                            erreurQte = true;
                            return;
                        }
                        prodArray.push(aRecord.data);
                    });

                    if (erreurQte == true){
                        return;
                    }

                    var JSONProdToSend = Ext.encode(prodArray);
                    includePanel.getForm().findField('commandeLigne').setValue(JSONProdToSend);
                // FIN conversion                    
                // -----------------------------------------------------------------------------


                    // si un bon de commande est saisi et la date de saisie est vide, on met la date du jour
                    if (includePanel.getForm().findField('numBonDeCommandeInterne').getValue()!="" &&
                        includePanel.getForm().findField('dateCommandeInterne').getValue()==""){
                            includePanel.getForm().findField('dateCommandeInterne').setValue(new Date());
                    }





                includePanel.getForm().submit({waitTitle:'Connexion au serveur', waitMsg:'Enregistrement en cours...',
                        success:function(f,action){
                           ds.reload();
                           includePanel.getForm().reset();
                            Ext.getCmp("statut").setValue('nouvelle commande');
                            cleanLivraisonFields();
                            enableAllFields();
                            Ext.getCmp("formPart").setActiveTab("TabCommande");
                            Ext.getCmp("TabLivraison").disable();
                            Ext.getCmp("TabSifac").disable();
                             Ext.getCmp("TabArticle").disable();
                            Ext.getCmp("gridEnStock").getStore().removeAll();
                             Ext.getCmp("basketGrid").getStore().removeAll();
                            Ext.msg.show(this.title ? this.title : 'Op&eacute;ration r&eacute;ussie:', 'Enregistrement effectu&eacute; !');
                },
                        failure:function(f,action){
				var result = Ext.util.JSON.decode(action.response.responseText);
				Ext.Msg.alert('Erreur', result.errors.reason);
                        }
                });

        },
        saveItemOld:function(){
            if (includePanel.getForm().isValid()){
                    this.saveItemEnd();
            }
        },
        resetToNull: function() {
           var form = this.getForm();
           if (form.isDirty()){
            Ext.MessageBox.show({
               buttons: Ext.Msg.OKCANCEL,
               title:'Etes-vous sur ?',
               msg:'Les données non enregistrées vont être effacées. Etes-vous sur ?',
               fn:function(btn) {
                   if(btn == 'ok') {
                        Ext.getCmp("CommandeForm").resetForm();
                        Ext.getCmp("gridP").getSelectionModel().clearSelections();
                        Ext.getCmp("statut").setValue('nouvelle commande');
                        Ext.getCmp("formPart").setActiveTab("TabCommande");
                        Ext.getCmp("basketGrid").getStore().removeAll();
                        Ext.getCmp("CommandeForm").getForm().findField('dateCommande').setValue(new Date());
                        Ext.getCmp("TabLivraison").disable();
                        Ext.getCmp("TabSifac").disable();
                        Ext.getCmp("TabArticle").disable();
                        cleanLivraisonFields();
                        enableAllFields();                       
                   }else{
                       return;
                   }
            }});
            return false;
            }

    },
    resetForm:function(){
        var form = this.getForm();
        form.items.each(function(f){			
       if(f.getXType() == 'xcheckbox')
            f.originalValue = false;
        else
            f.originalValue = '';
        }, form);
        form.reset();
    },   
    annuleCommande: function(id) {
        if(id == null)
            var this_id = this.getForm().findField(this.gridP).getValue();
        else
            var this_id = id;
        if(this_id != '') {
            Ext.MessageBox.confirm('Etes-vous sur ?','Voulez-vous annuler cette commande &nbsp;?',function(btn) {
                if(btn == 'yes') {
                        Ext.getCmp("gridP").loadMask.show();   
                        var formPart = Ext.getCmp("formPart");
                        formPart.el.mask('Chargement...', 'x-mask-loading');                     
                        var myFormParams = {
                            controller:this.controller,
                            action:'annuleCommande',
                            id:this_id,
                            output:'json'
                        };
                        // adding extra params if necessary!
                        if (this.extraParams) {
                            for (key in this.extraParams) {
                                myFormParams[key] = this.extraParams[key]; 
                            }
                        }
                        Ext.Ajax.request({
                            params:myFormParams,
                            url:'FrontController',
                             success:function(response, options){
                                if (response.responseText) {						
                                    if (response.responseText.substr(0,1) == '{') {
			    	        var ajaxResponse = Ext.util.JSON.decode(response.responseText);
				     }
	                        }
                                if(ajaxResponse && ajaxResponse.success) {
                                    includePanel.getForm().reset();
                                    Ext.getCmp("statut").setValue('nouvelle commande');
                                    enableGeneralsFields();
                                    ds.reload();
                                    Ext.msg.show(this.title ? this.title : 'Op&eacute;ration r&eacute;ussie:', 'Enregistrement effac&eacute; !');

                                }
                                formPart.el.unmask(); 
                            },
                            failure:function(response, options) {  
				var result = Ext.util.JSON.decode(response.responseText);
				Ext.Msg.alert('Erreur', result.errors.reason); 
                                ds.reload();
                                formPart.el.unmask(); 
                            }
                        }); 
                } else {
                  //do nothing
                }
            },this);
            return false;           
        } else {
            Ext.Msg.alert('Erreur','Vous devez s&eacute;lectionner un enregistrement');
        }
    }
    
    });
    

function reset(){

    includePanel.resetToNull();

}

function annuleCommande(id){
    includePanel.annuleCommande(id);
}

function saveLiv(){
    includePanel.saveLiv();
}

function saveArt(){
    includePanel.saveArt();
}


function save(){
    includePanel.saveItem();
}

var mapwin;

function closeImgWindow(){
	if(mapwin){
		mapwin.hide();
		mapwin= null;
	}
}




function openImgWindow(imageName, imageId){

		if(!mapwin){
			mapwin = new Ext.Window({
				layout: 'fit',
				closeAction: 'hide',
				width:164,
				height:222,
                                x:10,
                                cls:'whitebck',
				items: {
					xtype:'box',
					showAnimDuration:1
					,anchor:''
                                        ,cls:'whitebck'
					,isFormField:true
					,fieldLabel:'Image'
					,autoEl:{
                                            tag:'div', children:[{
                                                     tag:'img'
                                                    ,src:imageName+'.jpg'
                                            }]
					}
					
				}
			});					
		}        
        mapwin.show();
}


//function printCodeBarre(){
//            if (includePanel.getForm().findField('idCommande').getValue()==""){
//                Ext.Msg.alert('Impression impossible','Veuillez selectionner une commande à imprimer');
//                return;
//            }
//
//        Ext.MessageBox.show( {
//            msg:'Création du document en cours',
//            progressText:'Veuillez patienter...',
//            width:300,
//            wait:true,
//            modal:true,
//            waitConfig:{interval:200},
//            style:'background:transparent url(images/download.gif) no-repeat top left;height:46px;'
//            }
//        );
//        var x = function()
//        	{
//
//
//
//                    Ext.Ajax.request({
//                        method:'POST',
//                        url: 'FrontController',
//                        params: {action:'getLabelsBarCode', controller:'Commande', idCommande:includePanel.getForm().findField('idCommande').getValue()
//
//
//                                 },
//                        failure: function(r, o) {
//                            Ext.MessageBox.hide();
//                        },
//                        success: function(response, o) {
//
//                            results = Ext.decode(response.responseText);
//                            try {
//                                Ext.destroy(Ext.get('downloadIframe'));
//                            }
//                            catch(e) {}
//                            Ext.DomHelper.append(document.body, {
//                                tag: 'iframe',
//                                id:'downloadIframe',
//                                frameBorder: 0,
//                                width: 0,
//                                height: 0,
//                                css: 'display:none;visibility:hidden;height:0px;',
//                                src: 'DownloadFile?fileName='+results.fileName
//                            });
//
//                            Ext.MessageBox.hide();
//                        }
//                    });
//        }
//        x.defer(1000);
//}
