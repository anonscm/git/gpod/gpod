/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2012/01/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/

Ext.onReady(function(){
	var simple = new Ext.FormPanel({ // instance du nouveau FormPanel
		labelWidth: 75, // largeur par d�faut des label
		url:'save-form.php',// url du script PHP d'enregistrement (principe du formulaire)
		frame:true, //true = utilsation de 9 �l�ment pour le Template, false = utilis� une bordure de 1 pixel
		//title: 'Login', //Titre du panel
		bodyStyle:'padding:5px 5px 0',// Style padding de la balise body!
		width: 350,//largeur du panel
		defaults: {width: 230}, // largeur des champs
		defaultType: 'textfield', // type par d�faut des champs
		items:[
		       {fieldLabel: 'Nom',
				name: 'nom',
				allowBlank: false,
				vtype:'name'},
				{
					xtype: 'datefield',
					fieldLabel: 'Date',
					name: 'chdate'
					
				},
				{
					xtype: 'textfield',
					fieldLabel: 'Email',
					name: 'chemail',
					vtype:'email'
				}
		      ]
	});

	{
		Ext.form.VTypes.nameVal  = /^([A-Za-z]{1})[A-Za-z\-]/;
		Ext.form.VTypes.nameMask = /[A-Za-z\- ]/;
		Ext.form.VTypes.nameText = 'Nom invalide, veuillez v�rifier votre saisie.';
		Ext.form.VTypes.name     = function(v){
			return Ext.form.VTypes.nameVal.test(v);
		};
	}
	
	simple.render(document.body); // ajout du panel au document
	
	//une variable qui va contenir notre fen�tre
	var win = new Ext.Window({
		// le layout de notre fen�tre
		layout:'fit',
		//titre de la fen�tre
		title:'Login',
		//largeur de la fen�tre
		width:500,
		//hauteur de la fen�tre
		height:300,
		// Composition de la fen�tre
		items: simple,
		//les boutons de la fen�tre (bouton valider et bouton quitter)
		buttons: [{
			text:'Valider',
			//le bouton valider est d�sactiv� pas de validation possible (valider = submit!!)
			disabled:true
		},{
			text: 'Quitter',
			// handler = r�ponse sur �v�nement clic!
			handler: function(){
				//action a ex�cut� en cas de clic sur le bouton Quitter
				//ici on ferme la fen�tre.
			win.hide();
			}
		}]
	});
	//on affiche la fen�tre.
	win.show();
	
});