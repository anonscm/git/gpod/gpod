/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2012/01/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/

Ext.onReady(function(){
 


     var proxySPRoduit = new Ext.data.HttpProxy({
        url: 'FrontController',
        method: 'POST'
    });

    var recSPRoduit = Ext.data.Record.create([
            {name: 'idProduitConditionnement'},
            {name: 'idProduit'},
            {name: 'substance'},
            {name: 'nom'},
            {name: 'nomFournisseur'},
            {name: 'referenceFournisseur'},
            {name: 'contenance'},
            {name: 'refrigere'}

    ]);

    var readerSPRoduit = new Ext.data.JsonReader({
 	   root : "data",
 	   id : "idProduitConditionnement"
    }, recSPRoduit);


    var dsSPRoduit = new Ext.data.GroupingStore({
        proxy: proxySPRoduit,
        fields: ['idProduitConditionnement', 'idProduit','substance', 'nom', 'nomFournisseur', 'referenceFournisseur', 'contenance','refrigere'],
        root:'data',
        totalProperty: 'totalCount',
        reader: readerSPRoduit,
        nocache: true,
        autoLoad: false,
         groupField:'substance',
        baseParams:{action:'getListProduitAvecConditionnement', controller:'Produit'}
       
    });



    var cmSPRoduit = new Ext.grid.ColumnModel([
        {id:'idProduitConditionnement',header: "idProduitConditionnement", dataIndex: 'idProduitConditionnement', hidden:true},
        {header: "idProduit", dataIndex: 'idProduit', hidden:true},
        {header: "substance", width: 80, sortable: true, locked:false, dataIndex: 'substance'},
        {id:'nom',header: "Nom", width: 100, sortable: true, dataIndex: 'nom'},
        {header: "Fournisseur", width: 200, sortable: true, dataIndex: 'nomFournisseur'},
        {header: "reference fournisseur", width: 100, sortable: true, dataIndex: 'referenceFournisseur'},
        {header: "contenance", width: 100, sortable: true, dataIndex: 'contenance'},
        {header: "Refrigere", width: 20, sortable: true, dataIndex: 'refrigere'}
    ]);


    var winPagingBar = new Ext.Toolbar();

        searchProduit = new Ext.Window({
                layout:'border',
                width:1000,
                height:400,
                plain: true,
                modal:true,
                closable:false,
                title:'Choississez un Produit',

                items: [ {
                    id:'formPartAire',
            displayFloatingMsg:true,
            margins: '0 0 0 0',
            columnWidth: 0.9,
            region: 'north',
            xtype: 'fieldset',
            loadMask:{msg: 'Chargement...'},
            labelWidth: 190,
            height:0,
            defaults: {border:false},    // Default config options for child items
            defaultType: 'textfield',
            border:false,
            items: [

                    ]},{

                             xtype: 'grid',
                            id:'gridPAire',
                            margins: '5 5 5 5',
                            region: 'center',
                            autoScroll:true,
                            enableColumnHide:false,
                            loadMask:{msg: 'Chargement...'},
                            ds: dsSPRoduit,
                            cm: cmSPRoduit,
                            tbar: winPagingBar ,
                            view: new Ext.grid.GroupingView({
                                        forceFit:true,
                                        groupTextTpl: '{text} ({[values.rs.length]} {[values.rs.length > 1 ? "Items" : "Item"]})'
                                    }),
                            sm: new Ext.grid.RowSelectionModel({
                                singleSelect: true,
                                listeners: {
                                    rowselect: function(sm, row, rec) {

                                        selRecord = rec;

                                    }
                                }
                            }),
                            autoExpandColumn: 'nom',
                            border: true,
                            plugins:[new Ext.ux.grid.Search({
                                 mode:'remote'
                                ,iconCls:'icon-zoom'
                                ,autoFocus:true
                                ,minLength:2
                                ,position:'top'
                                ,align:'left'
                                ,width:500
                                ,shortcutKey:113
                                ,disableIndexes:['idProduit', 'idProduitConditionnement','substance', 'contenance', 'nomFournisseur','referenceFournisseur']
                            })]
                         }
              ],

                buttons: [{
                    text:'Annuler',
                    handler: function(){
                        searchProduit.hide();
                    }
                },{
                    text: 'Selectionner',
                    handler: function(){


                        if (selRecord == null){
                            searchProduit.hide();
                        }else{

                            includePanel.getForm().findField('idProduitConditionnement').setValue(selRecord.data.idProduitConditionnement);
                            includePanel.getForm().findField('nomProduit').setValue(selRecord.data.nom);
                            if (includePanel.getForm().findField('produitRefrigere'))
                            includePanel.getForm().findField('produitRefrigere').setValue(selRecord.data.refrigere);
                            if (includePanel.getForm().findField('CAS'))
                            includePanel.getForm().findField('CAS').setValue(selRecord.data.substance);
                            if (includePanel.getForm().findField('referenceFournisseur'))
                            includePanel.getForm().findField('referenceFournisseur').setValue(selRecord.data.referenceFournisseur);
                            searchProduit.hide();

                        }
                    }
                }]
            });

});