/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2012/01/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/

Ext.onReady(function(){

     var recLdap = Ext.data.Record.create([
            {name: 'uid'},
            {name: 'prenom'},
            {name: 'email'},
            {name: 'nom'},
            {name: 'composante'},
            {name: 'codeComposante'}
    ]);

    var readerAire = new Ext.data.JsonReader({
 	   root : "data",
 	   id : "uid"
    }, recLdap);


     var dsLdap = new Ext.data.JsonStore({
        proxy: proxy,
        fields: ['uid', 'prenom', 'email', 'nom','composante','codeComposante'],
        root:'data',
        totalProperty: 'totalCount',
        reader: readerAire,
        nocache: true,
        autoLoad: false,
        baseParams:{action:'getListFromLdap', controller:'Utilisateur'}
    });

    var cmLdap = new Ext.grid.ColumnModel([
        {id:'uid',header: "uid", width: 80, sortable: true, locked:false, dataIndex: 'uid'},
        {header: "prenom", width: 100, sortable: true, dataIndex: 'prenom'},
        {id:'nom',header: "nom", width: 100, sortable: true, dataIndex: 'nom'},
        {header: "email", width: 300, sortable: true, dataIndex: 'email'},
        {header: "composante", width: 300, sortable: true, dataIndex: 'composante'},
        {header: "codeComposante", width: 0, sortable: true, dataIndex: 'codeComposante', hidden:true}
    ]);



        var selRecord = null;
// ----------------------------------------------------------


        searchLdap = new Ext.Window({
                layout:'border',
                width:1000,
                height:400,
                plain: true,
                modal:true,
                closable:false,
                title:'Choississez un utilisateur',

                items: [ {
                    id:'formPartLdap',
            displayFloatingMsg:true,
            margins: '0 5 0 5',
            columnWidth: 0.9,
            region: 'north',
            xtype: 'fieldset',
            loadMask:{msg: 'Chargement...'},
            labelWidth: 190,
            height:160,
            defaults: {border:false},    // Default config options for child items
            defaultType: 'textfield',
            border:false,
            buttons: [{
                    text:'Rechercher',
                    handler: function(){

                         var type =   Ext.getCmp("type").items.get(0).getGroupValue();
                         var uid = Ext.getCmp("uid").getValue();
                         var name = Ext.getCmp("name").getValue();
                         var firstName = Ext.getCmp("firstName").getValue();


                         dsLdap.load({params:{
                                 uid:uid,
                                 type:type,
                                 name:name,
                                 firstName:firstName
                                  }});
                    }
                }
                ],
            items: [
                    {
                        id:'type',
                        name:'type',
                        xtype: 'radiogroup',
                        fieldLabel: 'Rechercher des ',
                        columns: [100, 100],
                        vertical: true,
                        items: [
                            {boxLabel: 'Personnels', name: 'cb-custwidth', inputValue: 1, checked: true},
                            {boxLabel: 'Etudiants', name: 'cb-custwidth', inputValue: 2}
                        ]
                    },
                    {
                        id:'name',
                        allowBlank: true,
                        fieldLabel: 'Nom (commence par)',
                        name: 'name',
                        width:200
                    },
                    {
                        id:'firstName',
                        allowBlank: true,
                        fieldLabel: 'Prénom (commence par)',
                        name: 'firstName',
                        width:200
                       },
                    {
                        id:'uid',
                        allowBlank: true,
                        fieldLabel: 'Nom de connexion',
                        name: 'uid',
                        width:200
                       }

                    ]},{

                             xtype: 'grid',
                            id:'gridPAire',
                            margins: '5 0 5 5',
                            region: 'center',
                            //tbar:gridTbar,
                            //height:150,
                            //minHeight:150,
                            autoScroll:true,
                            //bbar: PagingBar ,
                            enableColumnHide:false,
                            loadMask:{msg: 'Chargement...'},
                            ds: dsLdap,
                            cm: cmLdap,
                            sm: new Ext.grid.RowSelectionModel({
                                singleSelect: true,
                                listeners: {
                                    rowselect: function(sm, row, rec) {

                                        selRecord = rec;

                                    }
                                }
                            }),
                            autoExpandColumn: 'nom',
                            border: true





                                }




                ],

                buttons: [{
                    text:'Annuler',
                    handler: function(){
                        searchLdap.hide();
                    }
                },{
                    text: 'Selectionner',
                    handler: function(){


                        if (selRecord == null){
                            searchLdap.hide();
                        }else{


                            includePanel.getForm().findField('nom').setValue(selRecord.data.nom);
                            includePanel.getForm().findField('prenom').setValue(selRecord.data.prenom);
                            includePanel.getForm().findField('email').setValue(selRecord.data.email);
                            includePanel.getForm().findField('login').setValue(selRecord.data.uid);
                            includePanel.getForm().findField('login').setReadOnly(true);
                        includePanel.getForm().findField('password').allowBlank = true;
                        includePanel.getForm().findField('passwordConfirm').allowBlank = true;
                        includePanel.getForm().findField('password').validate();
                        includePanel.getForm().findField('passwordConfirm').validate();
                        includePanel.getForm().findField('password').disable();
                         includePanel.getForm().findField('passwordConfirm').disable();


                            searchLdap.hide();

                        }
                    }
                }]
            });



});