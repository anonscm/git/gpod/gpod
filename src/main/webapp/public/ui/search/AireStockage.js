/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2012/01/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/

Ext.onReady(function(){
 
     var recAire = Ext.data.Record.create([
            {name: 'idAire'},
            {name: 'code'},
            {name: 'nom'},
            {name: 'adresse1'},
            {name: 'ville'},
            {name: 'refrigere'}
    ]);

    var readerAire = new Ext.data.JsonReader({
 	   root : "data",
 	   id : "idAire"
    }, recAire);
 
 
     var dsAire = new Ext.data.JsonStore({
        proxy: proxy,
        fields: ['idAire', 'code', 'nom', 'adresse1', 'ville', 'refrigere'],
        root:'data',
        totalProperty: 'totalCount',
        reader: readerAire,
        nocache: true,
        autoLoad: true,
        baseParams:{action:'getListItems', controller:'AireStockage'}
    });

    var cmAire = new Ext.grid.ColumnModel([
        {id:'idAire',header: "idAire", width: 80, sortable: true, locked:false, dataIndex: 'idAire', hidden:true},
        {header: "code", width: 100, sortable: true, dataIndex: 'code'},
        {id:'nom',header: "nom", width: 100, sortable: true, dataIndex: 'nom'},
        {header: "adresse1", width: 200, sortable: true, dataIndex: 'adresse1'},
        {header: "ville", width: 200, sortable: true, dataIndex: 'ville'},
        {header: "refrigere", width: 200, sortable: true, dataIndex: 'refrigere'}
    ]);
 
 // ---------------------------------------------------------- 
// STORES

    // ComboBox site
    var cbsite = new Ext.data.JsonStore({
        id:cbsite,
        proxy: proxy,
        fields: ['idSite', 'nom'],
        root:'data',
        totalProperty: 'totalCount',
        nocache: true,
        autoLoad: false,
        baseParams:{action:'getListItems', controller:'Site'}
    });

    
    // ComboBox Batiment
    var cbbatiment = new Ext.data.JsonStore({
        id:cbbatiment,
        proxy: proxy,
        fields: ['idBatiment', 'nom', 'adresse1', 'adresse2', 'codePostal', 'ville', 'longitude','latitude'],
        root:'data',
        totalProperty: 'totalCount',
        nocache: true,
        autoLoad: false,
        baseParams:{action:'getListItems', controller:'Batiment'}
    });
    
    var cbsalle = new Ext.data.JsonStore({
        id:cbsalle,
        proxy: proxy,
        fields: ['idSalleStockage', 'nom'],
        root:'data',
        totalProperty: 'totalCount',
        nocache: true,
        autoLoad: false,
        baseParams:{action:'getListItems', controller:'SalleStockage'}
    });
    
      
        var selRecord = null;
// ----------------------------------------------------------  
 

        searchAireStockage = new Ext.Window({
                layout:'border',
                width:1000,
                height:430,
                plain: true,
                modal:true,
                closable:false,
                title:'Choississez une aire de stockage',

                items: [ {
                    id:'formPartAire',
            displayFloatingMsg:true,     	
            margins: '0 5 0 5',        	
            columnWidth: 0.9,
            region: 'north',
            xtype: 'fieldset',
            loadMask:{msg: 'Chargement...'},
            labelWidth: 190,
            height:100,
            defaults: {border:false},    // Default config options for child items
            defaultType: 'textfield',
            border:false,
            items: [
                    
                    {
                        id:'idSite',
                        xtype:'combo',
                        allowBlank: false,
                        blankText:'Le champ est requis',
                        fieldLabel: 'Site',
                        name: 'nomSite',
                        width:200,
                        store: cbsite,
                        root:'data',
                        hiddenName : 'idSite',
                        valueField:'idSite',
                        displayField:'nom',
                        typeAhead: true,
                        loadingText: 'Recherche...',
                        mode: 'local',
                        triggerAction: 'all',
                        emptyText:'Selectionner un element...',
                        selectOnFocus:true,
                        listeners:{
                             'select': function() {
                                var idsite = this.getValue();
                                cbbatiment.load({params:{idSite:idsite},callback:function(records,o,s){
                                    if (!s){
                                        cbbatiment.removeAll();
                                        Ext.getCmp('idBatiment').setValue('');
                                        cbsalle.removeAll();
                                        Ext.getCmp('idSalleStockage').setValue('');
                                        dsAire.removeAll();                                        
                                    }else{
                                        if (cbbatiment.getCount()==0){
                                            cbbatiment.removeAll();
                                            Ext.getCmp('idBatiment').setValue('');
                                            cbsalle.removeAll();
                                            Ext.getCmp('idSalleStockage').setValue('');
                                            dsAire.removeAll();                                            
                                        }else{
                                            var idBatiment0 = cbbatiment.getAt(0).get('idBatiment');
                                            Ext.getCmp('idBatiment').setValue(idBatiment0);
                                            cbsalle.load({params:{idBatiment:idBatiment0},callback:function(records,o,s){
                                                if (!s){
                                                    cbsalle.removeAll();
                                                    Ext.getCmp('idSalleStockage').setValue('');
                                                    dsAire.removeAll();  
                                                }else{
                                                    if (cbsalle.getCount()==0){
                                                        cbsalle.removeAll();
                                                        Ext.getCmp('idSalleStockage').setValue('');
                                                        dsAire.removeAll();                                                          
                                                    }else{
                                                        var idSalleStockage0 = cbsalle.getAt(0).get('idSalleStockage');
                                                        Ext.getCmp('idSalleStockage').setValue(idSalleStockage0);
                                                        dsAire.load({params:{idSalleStockage:idSalleStockage0},callback:function(records,o,s){
                                                            if (!s){
                                                                dsAire.removeAll();
                                                            }
                                                            }});                                            
                                                    }
                                                }
                                                }});                                              
                                        }
                                    }
                                    }});
                             }
                        }
                    },
                    {
                        xtype:'combo',
                        id:'idBatiment',
                        allowBlank: false,
                        blankText:'Le champ est requis',
                        fieldLabel: 'Batiment',
                        name: 'nomBatiment',
                        width:300,
                        store: cbbatiment,
                        root:'data',
                        hiddenName : 'idBatiment',
                        valueField:'idBatiment',
                        displayField:'nom',
                        typeAhead: true,
                        loadingText: 'Recherche...',
                        mode: 'local',
                        triggerAction: 'all',
                        emptyText:'Selectionner un element...',
                        selectOnFocus:true,
                        listeners:{
                             'select': function() {
                                var idBatiment = this.getValue();
                                cbsalle.load({params:{idBatiment:idBatiment},callback:function(records,o,s){
                                    if (!s){
                                        cbsalle.removeAll();
                                        Ext.getCmp('idSalleStockage').setValue('');
                                        dsAire.removeAll();
                                    }else{
                                        if (cbsalle.getCount()==0){
                                            cbsalle.removeAll();
                                            Ext.getCmp('idSalleStockage').setValue('');
                                            dsAire.removeAll();                                            
                                        }else{
                                            var idSalleStockage0 = cbsalle.getAt(0).get('idSalleStockage');
                                            Ext.getCmp('idSalleStockage').setValue(idSalleStockage0);
                                            dsAire.load({params:{idSalleStockage:idSalleStockage0},callback:function(records,o,s){
                                                if (!s){
                                                    dsAire.removeAll();
                                                }
                                                }});                                            
                                        }
                                    }
                                    }});

                             }
                         }
                       },
                    {
                        id:'idSalleStockage',
                        xtype:'combo',
                        allowBlank: false,
                        blankText:'Le champ est requis',
                        fieldLabel: 'Salle',
                        name: 'nomSalle',
                        width:300,
                        store: cbsalle,
                        root:'data',
                        hiddenName : 'idSalleStockage',
                        valueField:'idSalleStockage',
                        displayField:'nom',
                        typeAhead: true,
                        loadingText: 'Recherche...',
                        mode: 'local',
                        triggerAction: 'all',
                        emptyText:'Selectionner un element...',
                        selectOnFocus:true,
                        listeners:{
                             'select': function() {
                                var idSalleStockage = this.getValue();
                                dsAire.load({params:{idSalleStockage:idSalleStockage},callback:function(records,o,s){
                                    if (!s){
                                        dsAire.removeAll();
                                    }
                                }});

                             }
                         }
                       }]},{
                            
                             xtype: 'grid',
                            id:'gridPAire',    	
                            margins: '5 0 5 5',
                            region: 'center',
                            //tbar:gridTbar,
                            //height:150,
                            //minHeight:150,
                            autoScroll:true,                               
                            //bbar: PagingBar , 
                            enableColumnHide:false,
                            loadMask:{msg: 'Chargement...'},
                            ds: dsAire,
                            cm: cmAire,
                            sm: new Ext.grid.RowSelectionModel({
                                singleSelect: true,
                                listeners: {
                                    rowselect: function(sm, row, rec) {

                                        selRecord = rec;

                                    }
                                }
                            }),
                            autoExpandColumn: 'nom',
                            border: true
        
                            
                            
                            
                            
                                }
            

        
    
                ],

                buttons: [{
                    text:'Annuler',
                    handler: function(){
                        searchAireStockage.hide();
                    }
                },{
                    text: 'Selectionner',
                    handler: function(){
                        
                        
                        if (selRecord == null){
                            searchAireStockage.hide();
                        }else{
                        
                            if (includePanel.getForm().findField('idAire'))
                                includePanel.getForm().findField('idAire').setValue(selRecord.data.idAire);
                            if (includePanel.getForm().findField('NomAireStockage'))
                                includePanel.getForm().findField('NomAireStockage').setValue(selRecord.data.nom);
                            if (includePanel.getForm().findField('aireRefrigere'))
                                includePanel.getForm().findField('aireRefrigere').setValue(selRecord.data.refrigere);

                            if (Ext.getCmp("gridEnStock")){
                                theGrid = Ext.getCmp("gridEnStock");
                                theStore = theGrid.getStore();
                                var problemeRefrigere=false;
                                for(i=0;i<theStore.getCount();i++){
                                     var rec = theStore.getAt(i);
                                     if (selRecord.data.refrigere=="false"){
                                            var produitRefrigere = rec.get('refrigere');
                                            if (produitRefrigere=="true"){
                                                problemeRefrigere = true;
                                            }
                                     }
                                     rec.set('idAire', selRecord.data.idAire);
                                     rec.commit();
                                }
                                if (problemeRefrigere==true){
                                    Ext.Msg.alert('Aire non réfrigérée','Attention, des produits nécessitent \n\ une aire réfrigérée, or l\'aire choisie ne l\'est pas.');
                                }
                            }            
                            searchAireStockage.hide();
                            
                        }
                    }
                }]
            
            
                        ,listeners: {
                        show: function(){
                                    cbsite.load({callback:function(records,o,s){
                                            if (!s){
                                                cbsite.removeAll();
                                                Ext.getCmp('idSite').setValue('');
                                                cbbatiment.removeAll();
                                                Ext.getCmp('idBatiment').setValue('');
                                                cbsalle.removeAll();
                                                Ext.getCmp('idSalleStockage').setValue('');
                                                dsAire.removeAll();                                                    
                                            }else{
                                                if (cbsite.getCount()==0){
                                                    cbbatiment.removeAll();
                                                    Ext.getCmp('idBatiment').setValue('');
                                                    cbsalle.removeAll();
                                                    Ext.getCmp('idSalleStockage').setValue('');
                                                    dsAire.removeAll();    
                                                }else{
                                                    var idSite0 = cbsite.getAt(0).get('idSite')
                                                    Ext.getCmp('idSite').setValue(idSite0);
                                                    cbbatiment.load({params:{idSite:idSite0},callback:function(records,o,s){
                                                        if (!s){
                                                            cbbatiment.removeAll();
                                                            Ext.getCmp('idBatiment').setValue('');
                                                            cbsalle.removeAll();
                                                            Ext.getCmp('idSalleStockage').setValue('');
                                                            dsAire.removeAll();                                                              
                                                        }else{
                                                            if (cbbatiment.getCount()==0){
                                                                cbbatiment.removeAll();
                                                                Ext.getCmp('idBatiment').setValue('');
                                                                cbsalle.removeAll();
                                                                Ext.getCmp('idSalleStockage').setValue('');
                                                                dsAire.removeAll();                                                                     
                                                            }else{
                                                                var idBatiment0 = cbbatiment.getAt(0).get('idBatiment');
                                                                Ext.getCmp('idBatiment').setValue(idBatiment0);
                                                                cbsalle.load({params:{idBatiment:idBatiment0},callback:function(records,o,s){
                                                                    if (!s){
                                                                        cbsalle.removeAll();
                                                                        Ext.getCmp('idSalleStockage').setValue('');
                                                                        dsAire.removeAll();                                                                            
                                                                    }else{
                                                                        if (cbsalle.getCount()==0){
                                                                            cbsalle.removeAll();
                                                                            Ext.getCmp('idSalleStockage').setValue('');
                                                                            dsAire.removeAll();                                                                             
                                                                        }else{
                                                                            var idSalleStockage0 = cbsalle.getAt(0).get('idSalleStockage');
                                                                            Ext.getCmp('idSalleStockage').setValue(idSalleStockage0);
                                                                            dsAire.load({params:{idSalleStockage:idSalleStockage0},callback:function(records,o,s){
                                                                            if (!s){
                                                                                dsAire.removeAll();
                                                                            }
                                                                            }});                                            
                                                                        }
                                                                    }
                                                                    }});                                
                                                            }
                                                        }
                                                        }});                    
                                                }
                                            }
                                    }});
                
                          }
                    }
            
            });
            
           

});