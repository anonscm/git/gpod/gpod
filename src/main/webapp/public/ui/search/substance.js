/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2012/01/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/

Ext.onReady(function(){
 
 

 
 
     var winrec = Ext.data.Record.create([
            {name: 'CAS'},
            {name: 'nom'},
            {name: 'formule'},
            {name: 'description'},
            {name: 'pictogrammes'}
    ]);

    var winreader = new Ext.data.JsonReader({
 	   root : "data",
 	   id : "CAS"
    }, winrec);
    
     var winds = new Ext.data.JsonStore({
        proxy: proxy,
        fields: ['CAS', 'nom', 'formule', 'description', 'pictogrammes'],
        root:'data',
        totalProperty: 'totalCount',
        reader: winreader,
        nocache: true,
        autoLoad: false,
        baseParams:{action:'getListItems', controller:'Substance'}
    });

 
    var selRecord = null;
 
    var winPagingBar = new Ext.Toolbar({
        store: winds,
        action:'getListItems', 
        controller:'Substance'
    });  

 
     var wincolModel = new Ext.grid.ColumnModel([
        {id:'CAS',header: "CAS", width: 80, sortable: true, locked:false, dataIndex: 'CAS'},
        {id:'nom',header: "Nom", width: 100, sortable: true, dataIndex: 'nom'},
        {header: "Formule", width: 50, sortable: true, dataIndex: 'formule'},
        {id:'description', header: "Description", width: 200, sortable: true, dataIndex: 'description', hidden:false},
        {id:'pictogrammes',header: "pictogrammes", width: 200, sortable: true, dataIndex: 'pictogrammes', hidden:true}
    ]);
 
        searchSubstance = new Ext.Window({
                layout:'fit',
                width:700,
                height:400,
                closeAction:'hide',
                plain: true,

                items: [
            
            {
            xtype: 'grid',
            id:'windowgridP',
            split:true,        	
            margins: '5 0 5 5',
            //tbar:windowgridTbar,
            height:291,
            minHeight:291,
            tbar: winPagingBar , 
            enableColumnHide:false,
            loadMask:{msg: 'Chargement...'},
            ds: winds,
            cm: wincolModel,
            sm: new Ext.grid.RowSelectionModel({
                singleSelect: true,
                listeners: {
                    rowselect: function(sm, row, rec) {

                        selRecord = rec;
                       
                        
                    }
                }
            }),
            autoExpandColumn: 'description',
             title:'Substances',
            border: true,
            plugins:[new Ext.ux.grid.Search({
                 mode:'remote'
                ,iconCls:'icon-zoom'
                ,autoFocus:true
                ,minLength:2
                ,position:'top'
                ,align:'left'
                ,width:500
                ,shortcutKey:113
                ,disableIndexes:['pictogrammes']
            })]
            }
        
    
                ],

                buttons: [{
                    text:'Annuler',
                    handler: function(){
                        searchSubstance.hide();
                    }
                },{
                    text: 'Selectionner',
                    handler: function(){
                        
                        
                        if (selRecord == null){
                            searchSubstance.hide();
                        }else{
                        
                            includePanel.getForm().reset();   
                        
                            includePanel.getForm().findField('nomSubstance').setValue(Encoder.htmlDecode(selRecord.data.nom));
                            includePanel.getForm().findField('substance').setValue(selRecord.data.CAS);
                           


                            var checkboxGroup = includePanel.getForm().findField('pictogrammes').items;
                            var picto = selRecord.data.pictogrammes;
                            var arrayPicto=picto.split(',');
                            
                            for(x=1;x<checkboxGroup.length; x++){
                                if (arrayPicto.indexOf(checkboxGroup.items[x].inputValue) > -1){
                                    checkboxGroup.items[x].setValue(true);
                                }
                            }
                                                                            
                            searchSubstance.hide();
                            
                        }
                    }
                }]
            });

});