/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2012/01/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/

Ext.onReady(function(){
 
     var recSousStructure = Ext.data.Record.create([
            {name: 'idSousStructure'},
            {name: 'code'},
            {name: 'nom'},
            {name: 'adresse1'},
            {name: 'ville'}
    ]);

    var readerSousStructure = new Ext.data.JsonReader({
 	   root : "data",
 	   id : "idSousStructure"
    }, recSousStructure);
 
    sousStructureObj = new Object();
    sousStructureObj.action = 'getListSousStructureUser';
    sousStructureObj.controller = 'Utilisateur';

    dsSousStructure = new Ext.data.JsonStore({
        proxy: proxy,
        fields: ['idSousStructure', 'code', 'nom', 'adresse1', 'ville'],
        root:'data',
        totalProperty: 'totalCount',
        reader: readerSousStructure,
        nocache: true
    });
//    dsSousStructure.action = 'getListSousStructureUser';
//    dsSousStructure.controller = 'Utilisateur';
    dsSousStructure.load({params:{action:'getListSousStructureUser', controller:'Utilisateur'}});

    var cmSousStructure = new Ext.grid.ColumnModel([
        {id:'idSousStructure',header: "idSousStructure", width: 80, sortable: true, locked:false, dataIndex: 'idSousStructure', hidden:true},
        {header: "code", width: 100, sortable: true, dataIndex: 'code'},
        {id:'nom',header: "nom", width: 100, sortable: true, dataIndex: 'nom'},
        {header: "adresse1", width: 200, sortable: true, dataIndex: 'adresse1'},
        {header: "ville", width: 200, sortable: true, dataIndex: 'ville'}
    ]);
 
 // ---------------------------------------------------------- 
// STORES

    // ComboBox site
    var cbStructure = new Ext.data.JsonStore({
        id:cbStructure,
        proxy: proxy,
        fields: ['idStructure', 'nom'],
        root:'data',
        totalProperty: 'totalCount',
        nocache: true,
        autoLoad: true,
        baseParams:{action:'getListItems', controller:'Structure'}
    });
        
      
        var selRecord = null;
// ----------------------------------------------------------  
 

        searchSousStructure = new Ext.Window({
                layout:'border',
                width:1000,
                height:400,
                plain: true,
                modal:true,
                closable:false,
                title:'Choississez une aire de stockage',

                items: [ {
                    id:'formPartAire',
            displayFloatingMsg:true,     	
            margins: '0 5 0 5',        	
            columnWidth: 0.9,
            region: 'north',
            xtype: 'fieldset',
            loadMask:{msg: 'Chargement...'},
            labelWidth: 190,
            height:70,
            defaults: {border:false},    // Default config options for child items
            defaultType: 'textfield',
            border:false,
            items: [
                    
                    {
                        xtype:'combo',
                        allowBlank: true,
                        fieldLabel: 'Laboratoire/UFR',
                        name: 'nomStructure',
                        width:400,
                        store: cbStructure,
                        root:'data',
                        hiddenName : 'idStructure',
                        valueField:'idStructure',
                        displayField:'nom',
                        typeAhead: true,
                        loadingText: 'Recherche...',
                        mode: 'local',
                        triggerAction: 'all',
                        emptyText:'Selectionner un element...',
                        selectOnFocus:true,
                        listeners:{
                             'select': function() {
                                var idStructure = this.getValue();
                                dsSousStructure.load({params:{idStructure:idStructure, action:sousStructureObj.action, controller:sousStructureObj.controller}});
                             }
                        }
                    
                       }]},{
                            
                             xtype: 'grid',
                            id:'gridPAire',    	
                            margins: '5 0 5 5',
                            region: 'center',
                            //tbar:gridTbar,
                            //height:150,
                            //minHeight:150,
                            autoScroll:true,                               
                            enableColumnHide:false,
                            loadMask:{msg: 'Chargement...'},
                            ds: dsSousStructure,
                            cm: cmSousStructure,
                            sm: new Ext.grid.RowSelectionModel({
                                singleSelect: true,
                                listeners: {
                                    rowselect: function(sm, row, rec) {

                                        selRecord = rec;

                                    }
                                }
                            }),
                            autoExpandColumn: 'nom',
                            border: true
        
                            
                            
                            
                            
                                }
            

        
    
                ],

                buttons: [{
                    text:'Annuler',
                    handler: function(){
                        searchSousStructure.hide();
                    }
                },{
                    text: 'Selectionner',
                    handler: function(){
                        
                        
                        if (selRecord == null){
                            searchSousStructure.hide();
                        }else{
                        
                            try{
                                includePanel.getForm().findField('idSousStructure').setValue(selRecord.data.idSousStructure);
                            
                                // nomSousStructureMove quand on est dans le formulaire de CONSOMMATION
                                // nomSousStructure quand on est ailleur.
                                if (includePanel.getForm().findField('nomSousStructureMove')){
                                    includePanel.getForm().findField('nomSousStructureMove').setValue(selRecord.data.nom);
                                }else{
                                    includePanel.getForm().findField('nomSousStructure').setValue(selRecord.data.nom);
                                }
                            }catch (e){
                                fp.getForm().findField('nomSousStructure').setValue(selRecord.data.nom);
                                fp.getForm().findField('idSousStructure').setValue(selRecord.data.idSousStructure);


                            }
                            searchSousStructure.hide();
                            
                        }
                    }
                }]
            });
            
           

});