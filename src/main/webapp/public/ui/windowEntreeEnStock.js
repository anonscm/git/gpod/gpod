/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2012/01/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/

Ext.onReady(function(){
 


     var proxyEES = new Ext.data.HttpProxy({
        url: 'FrontController',
        method: 'POST'
    });

    var recliv = Ext.data.Record.create([
            {name: 'idArticle'},
            {name: 'idCommandeLigne'},
            {name: 'CAS'},
            {name: 'reference'},
            {name: 'refrigere'},
            {name: 'idProduitConditionnement'},
            {name: 'nom'},
            {name: 'idAire'},
            {name: 'nomAire'},
            {name: 'identifiantEtiquette'}
    ]);
    
   // ComboBox Labo dans la colonne
    var cbLABO = new Ext.data.JsonStore({
        proxy: proxy,
        fields: ['idAire', 'nomTypeAire', 'nomLong','refrigere'],
        root:'data',
        totalProperty: 'totalCount',
        nocache: true,
        autoLoad: true,
        baseParams:{action:'getListItems', controller:'AireStockage'}
    });




 // ----------------------------------------------------------

   var comboRenderer = function(combo){
        return function(value){
            combo.store.clearFilter();
            var record = combo.findRecord(combo.valueField || combo.displayField, value);
            return record ? record.get(combo.displayField) : combo.valueNotFoundText;
        }
    }


    var combo = new Ext.form.ComboBox({
                typeAhead: true,
                name:'nom',
                store: cbLABO,
                root:'data',
                triggerAction: 'all',
                listClass: 'x-combo-list-small',
                allowBlank: false,
                hiddenName : 'idAire',
                valueField:'idAire',
                displayField:'nomLong',
                mode: 'local',
                listeners:{
                     // Test si l'aire est refrigérée
                     'select': function(combo, record, index){
                         // si l'aire est rédrigérée, pas de test, sinon on test si le produit l'est
                         if (record.get('refrigere')=="false"){
                            // Je récupère dans le DOM l'index de la ligne sélectionnée grace au style CSS x-grid3-row-selected
                            var gridIndex = Ext.get(this.el.dom.parentElement.parentElement.previousSibling.previousSibling).query(".x-grid3-row-selected")[0].rowIndex;
                            // je récupère la propriété refrigérée du produit dans la grid
                            var produitRefrigere = gridEnStock.getStore().getAt(gridIndex).get('refrigere');
                            // si le produit a besoin d'être refrigéré, on affiche un message
                            if (produitRefrigere=="true"){
                                Ext.Msg.alert('Aire non réfrigérée','Attention, le produit nécessite \n\ une aire réfrigérée, or l\'aire choisie ne l\'est pas.');
                            }
                         }                         
                     }
                }
    });



        windowEntreeEnStock = new Ext.Window({
                layout:'border',
                width:1000,
                height:400,
                plain: true,
                modal:true,
                closable:false,
                title:'Choississez un Produit',

                items: [ {
                    id:'formPartAire',
                    displayFloatingMsg:true,
                    margins: '0 0 0 0',
                    columnWidth: 0.9,
                    region: 'north',
                    xtype: 'fieldset',
                    loadMask:{msg: 'Chargement...'},
                    labelWidth: 190,
                    height:50,
                    defaults: {border:false},    // Default config options for child items
                    defaultType: 'textfield',
                    border:false,
                    items: [{
                            xtype: 'displayfield',
                            fieldLabel: '',
                            name: '',
                            value: ''
                            },{
                              fieldLabel: 'aireRefrigere',
                              name: 'aireRefrigere',
                              hidden:true
                            },
                        {
                            xtype:'panel',
                            baseCls:'x-plain',
                            layout:'hbox',
                            width:900,
                            defaults:{
                                margins:'0 5 0 0',
                                pressed: false
                            },

                            items: [{cls:'x-form-item',   xtype:'label', text:'Aire de Stockage *', width:190},
                                {
                                    xtype:'button',
                                    id:'btn_allAires',
                                    fieldLabel: 'Aire de stockage',
                                    text:'Selectionner votre aire de stockage favorite',
                                    handler:function(){
                                        gridEnStock = Ext.getCmp("gridEnStock");
                                        if (gridEnStock.getStore().getCount() == 0) {
                                            Ext.Msg.alert("erreur", "Générez d'abord les lignes");
                                        }else{

                                            gridEnStock = Ext.getCmp("gridEnStock");
                                            theStore = gridEnStock.getStore();
                                            var problemeRefrigere=false;
                                            for(i=0;i<theStore.getCount();i++){
                                                var rec = theStore.getAt(i);
                                                rec.set('idAire', idAireFavorite);
                                                rec.commit();
                                                theStore.commitChanges();
                                            }
                                            if (problemeRefrigere==true){
                                                Ext.Msg.alert('Aire non réfrigérée','Attention, des produits nécessitent \n\ une aire réfrigérée, or l\'aire choisie ne l\'est pas.');
                                            }
                                        }
                                    }},{

                                    xtype:'button',
                                    id:'btn_selAire',
                                    fieldLabel: 'Aire de stockage',
                                    text:'Selectionner une aire de stockage',
                                    windowName:'searchAireStockage',
                                    handler:function(){
                                        gridEnStock = Ext.getCmp("gridEnStock");
                                        if (gridEnStock.getStore().getCount() == 0) {
                                            Ext.Msg.alert("erreur", "Aucun article à entrer en stock");
                                        }else{

                                            var mySearchWin = eval(this.windowName);
                                            mySearchWin.show();
                                        }

                                    }




                                }
                            ]





                        }
/*                            {
                              xtype:'button',
                              id:'btn_allAires',
                              fieldLabel: 'Aire de stockage',
                              text:'Selectionner une même aire de stockage pour tous les articles',
                              windowName:'searchAireStockage',
                              handler:function(){
                                  gridEnStock = Ext.getCmp("gridEnStock");
                                  if (gridEnStock.getStore().getCount() == 0) {
                                        Ext.Msg.alert("erreur", "Aucun article à entrer en stock");
                                  }else{

                                        var mySearchWin = eval(this.windowName);
                                        mySearchWin.show();
                                  }

                              }
                             }*/

                    ]},{

                            xtype: 'editorgrid',
                            id:'gridEnStock',
                            margins: '0 5 5 5',
                            region: 'center',
                            clicksToEdit: 1,
                            height:137,
                            enableColumnHide:false,
                            loadMask:{msg: 'Chargement...'},
                            sm: new Ext.grid.RowSelectionModel({}),
                            border: true,
                            ds: new Ext.data.JsonStore({
                                    proxy: proxyEES,
                                    fields: ['idArticle','idCommandeLigne','CAS','reference','idProduitConditionnement','nom', 'idAire','nomAire','contenance','identifiantEtiquette'],
                                    root:'data',
                                    autoSave:false,
                                    totalProperty: 'totalCount',
                                    reader: new Ext.data.JsonReader({root : "data",id : "idArticle"}),
                                    nocache: true,
                                    autoLoad: false,
                                    baseParams:{action:'getJsonCommandeLigne', controller:'Commande'}
                                }),
                            cm:     new Ext.grid.ColumnModel({columns: [
                                {dataIndex:'idArticle', hidden:true},
                                {dataIndex:'idCommandeLigne', hidden:true},                            
                                {header: 'CAS', dataIndex: 'CAS', width: 70, sortable:true},
                                {header: 'Reference', dataIndex: 'reference', width: 100},
                                {dataIndex:'idProduitConditionnement', hidden:true},  
                                {header: 'refrigere', dataIndex: 'refrigere',hidden:true},
                                {header: 'Nom', dataIndex: 'nom', width: 260},
                                {
                                    id: 'idAire',
                                    header: 'Local de Stockage',
                                    dataIndex: 'idAire',
                                    renderer:comboRenderer(combo),
                                    width: 280,
                                    editor: combo
                                },                               
                                {header: 'Contenance', dataIndex: 'contenance', width: 100},
                                {header: 'Etiquette', dataIndex: 'identifiantEtiquette', width: 100, editor: new Ext.form.TextField({
                                    xtype:'textfield',
                                    maxLength:50
                                    }),css:'background-image:url(lib/extjs/resources/images/default/grid/dirty.gif);background-repeat:no-repeat;'}
                                ,{header: "scan", dataIndex:'scan', width: 40, sortable: false, xtype:'actioncolumn',
                                items: [{
                                getClass: function(v, meta, rec) {          // Or return a class from a function
                                        this.items[0].tooltip = 'Scanner une étiquette';
                                        return 'rfid-col';
                                },
                                    handler: function(grid, rowIndex, colIndex) {
                                        Ext.DomHelper.overwrite('iframediv', {
                                                tag: 'iframe',
                                                id:'appletIFrame',
                                                frameBorder: 0,
                                                width: 0,
                                                height: 0,
                                                css: 'display:none;visibility:hidden;height:0px;',
                                                //css: 'height:50px;',
                                                src: '../applet/popupnfc.html?row='+rowIndex+"&origine=EntreeEnStock"
                                            });
                                            appletWin.show();
                                            appletWin.el.mask('Chargement, veuillez patienter...', 'x-mask-loading');


                                    }
                    }]}
                        ]}),
                            writer:new Ext.data.JsonWriter({encode: true, writeAllFields: false})
                         }
              ],

                buttons: [{
                    text:'Annuler',
                    handler: function(){
                        Ext.getCmp("gridEnStock").getStore().removeAll();
                        windowEntreeEnStock.hide();
                    }
                },{
                    text: 'Entrer en stock',
                    handler: function(){

                        var gridEnStock = Ext.getCmp('gridEnStock');
                        var erreurAire = false;
                        // CONVERSION DE LA GRILLE DES LIVRAION SON JSON
                        gridEnStock.stopEditing();
                        var recordsToSend = gridEnStock.getStore().getRange();
                        var farray = [];
                        Ext.each(recordsToSend, function(aRecord) {
                            if (isNaN(aRecord.get('idAire')) || aRecord.get('idAire')<=0){
                                erreurAire = true;
                                return;
                            }                           
                            farray.push(aRecord.data);
                        });

                        if (erreurAire == true){
                            Ext.Msg.alert('Erreur', "Vous devez choisir un local de stockage");                            
                            return;
                        }
                       

                        var JSONToSend = Ext.encode(farray);    
                        includePanel.getForm().findField('stockLigne').setValue(JSONToSend);
                        
                        if (includePanel.getForm().findField('stockLigne').getValue()=='' ||
                            includePanel.getForm().findField('livraisonLigne').getValue()=='')
                                return;
                        
                        
                        includePanel.getForm().submit({waitTitle:'Connexion au serveur', waitMsg:'Enregistrement en cours...',
                                success:function(f,action){
                                   ds.reload();
                                   includePanel.getForm().reset();
                                   windowEntreeEnStock.hide();
                                    Ext.getCmp("statut").setValue('nouvelle commande');
                                    cleanLivraisonFields();
                                    enableAllFields();
                                    
                                    Ext.getCmp("gridEnStock").getStore().removeAll();                                    
                                    Ext.getCmp("formPart").setActiveTab("TabCommande");
                                    Ext.getCmp("TabLivraison").disable();
                                    Ext.getCmp("TabSifac").disable();
                                     Ext.getCmp("basketGrid").getStore().removeAll();
                                    Ext.msg.show(this.title ? this.title : 'Op&eacute;ration r&eacute;ussie:', 'Enregistrement effectu&eacute; !');
                        },
                                failure:function(f,action){
                                        var result = Ext.util.JSON.decode(action.response.responseText);
                                        Ext.Msg.alert('Erreur', result.errors.reason);
                                }
                        });
                        
                        
                 

                    }
                }]
            });
            
            windowEntreeEnStock.on('activate', function(){ 

                
                     gridEnStock = Ext.getCmp("gridEnStock");
                     if (gridEnStock.getStore().getCount() > 0)
                         return;                 
                   
                    
                      gridLivraison = Ext.getCmp("gridLivraison");
                      gridEnStock.stopEditing();
                      gridLivraison.stopEditing();

                      recordsToSend = gridLivraison.getStore().getRange();
                      farray = [];
                      j=0;
                      Ext.each(recordsToSend, function(aRecord) {
                            var quantiteLivraison = aRecord.get('quantiteLivraison');
                            for (i=0;i<quantiteLivraison;i++){                                                   
                                gridEnStock.getStore().add(new recliv(
                                    {
                                       idArticle:j++,
                                       idCommandeLigne:aRecord.get('idCommandeLigne'),
                                       CAS: aRecord.get('CAS'),
                                       reference:aRecord.get('reference'),
                                       idProduitConditionnement:aRecord.get('idProduitConditionnement'),
                                       nom:aRecord.get('nom'),
                                       refrigere:aRecord.get('refrigere'),
                                       contenance:aRecord.get('contenance'),
                                       identifiantEtiquette:''

                                    }
                               )); 
                            }                                                
                       });

                      gridEnStock.startEditing(0, 1);                    
            });
            

});