/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2012/01/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/

Ext.QuickTips.init();

    Ext.form.Field.prototype.msgTarget = 'side';

    var bd = Ext.getBody();
    
   Ext.apply(Ext.form.VTypes, {
   password: function(value, field)
   {
      if (field.initialPasswordField)
      {
         var pwd = Ext.getCmp(field.initialPasswordField);
         this.passwordText = 'La confirmation ne correspond pas au mot de passe.';
         return (value == pwd.getValue());
      }
 
      this.passwordText = 'Le mot de passe doit comporter au moins 5 caract&egrave;res';
 
      //var hasSpecial = value.match(/[0-9!@#\$%\^&\*\(\)\-_=\+]+/i);
      var hasLength = (value.length >= 5);
 
      //return (hasSpecial && hasLength);
      return (hasLength);
   },
 
   passwordText: 'Le mot de passe doit comporter au moins 5 caract&egrave;res' //, or a valid special character (!@#$%^&*()-_=+)'
});
    
// ----------------------------------------------------------
// ----------------------------------------------------------
// TABLEAU - GRID 
// acces au données pour le tableau
    var proxy = new Ext.data.HttpProxy({
        url: 'FrontController',
        method: 'POST'
    });
    
    var rec = Ext.data.Record.create([
            {name: 'idUtilisateur'},
            {name: 'nom'},
            {name: 'prenom'},
            {name: 'login'},
            {name: 'email'},
            {name: 'idUtilisateurProfil'},
            {name: 'nomProfil'}
    ]);

    var reader = new Ext.data.JsonReader({
 	   root : "data",
 	   id : "idUtilisateur"
    }, rec);


    var ds = new Ext.data.JsonStore({
        proxy: proxy,
        fields: ['idUtilisateur', 'nom', 'prenom', 'login', 'email', 'idUtilisateurProfil', 'nomProfil'],
        root:'data',
        totalProperty: 'totalCount',
        reader: reader,
        nocache: true,
        autoLoad: false,
        baseParams:{action:'getListItems', controller:'Utilisateur'},
        listeners:{
            load:function(){
                if(typeof(myMask) !== 'undefined')
                    myMask.hide();
            }
        }
    });
    ds.load({params:{start:0, limit:10}});


    var colModel = new Ext.grid.ColumnModel([
        {id:'idUtilisateur',header: "id", width: 80, sortable: true, locked:false, dataIndex: 'idUtilisateur', hidden:true},
        {id:'nom',header: "Nom", width: 100, sortable: true, dataIndex: 'nom'},
        {header: "Prenom", width: 100, sortable: true, dataIndex: 'prenom'},
        {header: "Login", width: 200, sortable: true, dataIndex: 'login'},
        {header: "Email", width: 100, sortable: true, dataIndex: 'email'},
        {header: "idUtilisateurProfil", width: 200, sortable: true, dataIndex: 'idUtilisateurProfil', hidden:true},
        {header: "Profil", width: 200, sortable: true, dataIndex: 'nomProfil'},
        {header: "Supp", width: 40, sortable: false, xtype:'actioncolumn',
        items: [{
                    getClass: function(v, meta, rec) {          // Or return a class from a function
                            this.items[0].tooltip = 'Supprimer cette ligne';
                            return 'del-col';
                    },
                    handler: function(grid, rowIndex, colIndex) {
                        var rec = ds.getAt(rowIndex);
                        deleteligne(rec.get('idUtilisateur'));
                    }
                }]}        
    ]);

    var PagingBar = new Ext.PagingToolbar({
        pageSize: 10,
        store: ds,
        displayInfo: true,
        action:'getListItems', 
        controller:'Utilisateur',
        displayMsg: 'Utilisateurs {0} - {1} of {2}',
        emptyMsg: 'Aucun Utilisateur'
    });
    
    PagingBar.insertButton(11, excelButton);
// FIN TABLEAU - GRID
// ----------------------------------------------------------   
    

// ---------------------------------------------------------- 
// ---------------------------------------------------------- 
//  Toolbar Tableau
    var formTbar = new Ext.Toolbar({
     height: 27,
     items: [{text: 'Nouvel utilisateur',
                iconCls: 'icon-add',
                handler : reset
               },'-',
               {
                  iconCls:'icon-ldap',
                  text:'Rechercher dans l\'annuaire',
                  windowName:'searchLdap',
                  handler:function(){

                      var mySearchWin = eval(this.windowName);
                      mySearchWin.show();
                  }
                },'->',               
               {
                id:'edit-xls',
                iconCls:'icon-excel',
                value: '',
                handler:function(){
                    var value = includePanel.getForm().findField('idUtilisateur').getValue();
                    if (value>0){
                        printAction("Utilisateur","getXLSList", value);
                    }else{
                        Ext.Msg.alert('Export impossible','Veuillez selectionner un item à éditer');
                        return;
                    }
                }            
            },               
               {
                id:'edit',
                iconCls:'icon-print',
                value: '',
                handler:function(){
                    var value = includePanel.getForm().findField('idUtilisateur').getValue();
                    if (value>0){
                        printAction("Utilisateur","getPDF", value);
                    }else{
                        Ext.Msg.alert('Export impossible','Veuillez selectionner un item à éditer');
                        return;
                    }
                }            
            }]
    });
    
     var formBbar = new Ext.Toolbar({
     height: 27,
     id:'bottom-bar',
     cls:'x-bbar',
     items: [                   '->', 
               {id:'btn-enreg',
                text: 'Enregistrer',
                iconCls: 'icon-save',
                handler : save
                }]

    });    
// ---------------------------------------------------------- 
    

// ---------------------------------------------------------- 
// STORES

    // ComboBox Profil
    var cb = new Ext.data.JsonStore({
        proxy: proxy,
        fields: ['idUtilisateurProfil', 'nomProfil', 'code'],
        root:'data',
        totalProperty: 'totalCount',
        nocache: true,
        autoLoad: true,
        baseParams:{action:'getListItems', controller:'UtilisateurProfil'}
    });
    
    //Select equipes autorisés
    var multiAuth = new Ext.data.JsonStore({
        proxy: proxy,
        fields: ['idSousStructure', 'nom'],
        root:'data',
        totalProperty: 'totalCount',
        nocache: true,
        autoLoad: false,
        baseParams:{action:'getListSousStructureUser', controller:'Utilisateur'}
    });
    
    // select equipes disponibles
    var multiDisp = new Ext.data.JsonStore({
        proxy: proxy,
        fields: ['idSousStructure', 'nom'],
        root:'data',
        totalProperty: 'totalCount',
        nocache: true,
        autoLoad: true,
        baseParams:{action:'getListItems', controller:'SousStructure', idUtilisateur:0}
    });
// FIN STORES
// ---------------------------------------------------------- 
    
    
    
    var includePanel = new Ext.FormPanel({
        id: 'UtilisateursForm',
         controller:'Utilisateur',
        url: 'FrontController',
        frame: true,    
        labelAlign: 'left',
        waitMsgTarget: true,
        width: 1021,
        layout: 'border',    // Specifies that the items will now be arranged in columns
        height:645,
        baseParams:{action:'saveItem', controller:'Utilisateur'},
        items: [{
            xtype: 'grid',
            id:'gridP', 	
            margins: '5 0 5 0',
            region: 'north',
            height:287,
            bbar: PagingBar , 
            enableColumnHide:false,
            loadMask:{msg: 'Chargement...'},
            ds: ds,
            cm: colModel,
            sm: new Ext.grid.RowSelectionModel({
                singleSelect: true,
                listeners: {
                    rowselect: function(sm, row, rec) {
                        var formPart = Ext.getCmp("formPart");
                        var this_id = rec.data.idUtilisateur;
                        formPart.el.mask('Chargement...', 'x-mask-loading');
                        multiAuth.load({params:{idUtilisateur:this_id}});
                        multiDisp.load({params:{idUtilisateur:this_id}});
                        Ext.getCmp("UtilisateursForm").getForm().loadRecord(rec);  
                        formPart.el.unmask();
                        Ext.getCmp("password").allowBlank = true;
                        Ext.getCmp("passwordConfirm").allowBlank = true;
                        Ext.getCmp("password").validate();
                        Ext.getCmp("passwordConfirm").validate();

                    }
                }
            }),
            autoExpandColumn: 'nom',
             title:'Utilisateurs',
            border: true,
            
            plugins:[new Ext.ux.grid.Search({
                 mode:'remote'
                ,iconCls:'icon-zoom'
                ,autoFocus:true
                ,minLength:2
                ,position:'bottom'
                ,align:'left'
                ,width:200
                ,shortcutKey:113
                ,disableIndexes:['idUtilisateur','idUtilisateurProfil','nomProfil']
            })]
        },{
            id:'formPart',
            displayFloatingMsg:true,      	
            margins: '0 0 0 0',
            columnWidth: 0.9, 
            region: 'center',
            xtype: 'fieldset',
            cls:'x-fieldset-with-tbar',   
            bbar:formBbar,
            tbar: formTbar,             
            loadMask:{msg: 'Chargement...'},
            labelWidth: 190,          
            height:342,
            defaults: {border:false},    // Default config options for child items
            defaultType: 'textfield',
            labelSeparator:'',
            bodyStyle: Ext.isIE ? 'padding:0 0 5px 15px;' : 'padding:10px 15px;',
            border: true,
            items: [{
                fieldLabel: 'idUtilisateur',
                name: 'idUtilisateur',
                hidden:true
            }

            ,{
                    xtype: 'compositefield',
                    fieldLabel: 'Nom / Prenom',
                    width:505,
                    items: [
                    {   
                         xtype: 'textfield',
                        fieldLabel: 'Nom',
                        name: 'nom',
                        allowBlank: false,
                        blankText:'Le champ est requis',                
                        width:250,
                        maxLength:45,
                        maxLengthText:'Longueur maxi : 45'                  
                    },{   
                        xtype: 'textfield',                        
                        fieldLabel: 'Prenom',
                        name: 'prenom',
                        blankText:'Le champ est requis',                
                        width:250,
                        maxLength:45,
                        maxLengthText:'Longueur maxi : 45' 
                    }]
            }
            ,{   
                fieldLabel: 'Email',
                name: 'email',
                vtype:'email',
                allowBlank: false,
                blankText:'Le champ est requis',                
                width:250,
                maxLength:45,
                maxLengthText:'Longueur maxi : 45' 
            },{            	
            	fieldLabel: 'Login',
                name: 'login',
                allowBlank: false,
                blankText:'Le champ est requis',
                width:100,
                maxLength:45,
                maxLengthText:'Longueur maxi : 45' 
            },{
                id: 'password',
                fieldLabel: 'Mot de passe',
                vtype:'password',
                allowBlank: false,
                inputType: 'password',
                blankText:'Le champ est requis',
                name: 'password',
                width:100,
                maxLength:45,
                maxLengthText:'Longueur maxi : 45' 
            },{
                id: 'passwordConfirm',
                fieldLabel: 'Confirmation',
                inputType: 'password',
                allowBlank: false,
                blankText:'Le champ est requis',
                vtype:'password',
                name: 'passwordconfirm',
                initialPasswordField:'password',
                width:100,
                maxLength:45,
                maxLengthText:'Longueur maxi : 45' 
            },
            {
                xtype:'combo',
                allowBlank: false,
                blankText:'Le champ est requis',
                fieldLabel: 'Profil',
                name: 'nomProfil',
                width:200,
                store: cb,
                root:'data',
                hiddenName : 'idUtilisateurProfil',
                valueField:'idUtilisateurProfil',
                displayField:'nomProfil',
                typeAhead: true,
                loadingText: 'Recherche...',                
                mode: 'local',
                triggerAction: 'all',
                emptyText:'Selectionner un element...',
                selectOnFocus:true
            },{
            xtype: 'itemselector',
            name: 'idSousStructures',
            imagePath: 'lib/multiselect/',
            fieldLabel: 'Equipe/Département',
            drawUpIcon:false,
            drawDownIcon:false,
            drawLeftIcon:true,
            drawRightIcon:true,
            drawTopIcon:false,
            drawBotIcon:false,    
            width:700,
           // height:120,
            multiselects: [{
                width: 250,
                height: 95,
                searchBar:false,
                store: multiDisp,
                displayField: 'nom',
                valueField: 'idSousStructure',
                legend:'Autres Equipes/Départements'
            },{
                width: 250,
                height: 95,
                store: multiAuth,
                displayField: 'nom',
                valueField: 'idSousStructure',
                legend:'Equipes/Départements de l\'utilisateur'
            }]
            }


            ]
            
        }],
        saveItem:function(){
                if (includePanel.getForm().isValid()){
                includePanel.getForm().submit({waitTitle:'Connexion au serveur', waitMsg:'Enregistrement en cours...',
                        success:function(f,action){
                           ds.reload();
                           includePanel.getForm().reset();
                            Ext.getCmp("password").allowBlank = false;
                            Ext.getCmp("passwordConfirm").allowBlank = false;                           
                            Ext.msg.show(this.title ? this.title : 'Op&eacute;ration r&eacute;ussie:', 'Enregistrement effectu&eacute; !');
                },
                        failure:function(f,action){
				var result = Ext.util.JSON.decode(action.response.responseText);
				Ext.Msg.alert('Erreur', result.errors.reason);   
                        }
                });
        }
        },
        resetToNull: function() {
           var form = this.getForm();
           if (form.isDirty()){
            Ext.MessageBox.show({
               buttons: Ext.Msg.OKCANCEL,
               title:'Etes-vous sur ?',
               msg:'Les données non enregistrées vont être effacées. Etes-vous sur ?',
               fn:function(btn) {
                   if(btn == 'ok') {
                       Ext.getCmp("UtilisateursForm").resetForm();
                       Ext.getCmp("gridP").getSelectionModel().clearSelections();
                   }else{
                       return;
                   }
            }});
            return false;
            }

    },
    resetForm:function(){
        var form = this.getForm();
        form.items.each(function(f){			
       if(f.getXType() == 'xcheckbox')
            f.originalValue = false;
        else
            f.originalValue = '';
        }, form);
        form.reset();
        Ext.getCmp("password").allowBlank = false;
        Ext.getCmp("passwordConfirm").allowBlank = false;     
    },   
    deleteItem: function(id) {
        if(id == null)
            var this_id = this.getForm().findField(this.gridP).getValue();
        else
            var this_id = id;
        if(this_id != '') {
            Ext.MessageBox.confirm('Etes-vous sur ?','Voulez-vous effacer cet enregistrement&nbsp;?',function(btn) {
                if(btn == 'yes') {
                        Ext.getCmp("gridP").loadMask.show();   
                        var formPart = Ext.getCmp("formPart");
                        formPart.el.mask('Chargement...', 'x-mask-loading');                     
                        var myFormParams = {
                            controller:this.controller,
                            action:'deleteItem',
                            id:this_id,
                            output:'json'
                        };
                        // adding extra params if necessary!
                        if (this.extraParams) {
                            for (key in this.extraParams) {
                                myFormParams[key] = this.extraParams[key]; 
                            }
                        }
                        Ext.Ajax.request({
                            params:myFormParams,
                            url:'FrontController',
                             success:function(response, options){
                                if (response.responseText) {						
                                    if (response.responseText.substr(0,1) == '{') {
			    	        var ajaxResponse = Ext.util.JSON.decode(response.responseText);
				     }
	                        }
                                if(ajaxResponse && ajaxResponse.success) {
                                    includePanel.getForm().reset();
                                    ds.reload();
                                    Ext.msg.show(this.title ? this.title : 'Op&eacute;ration r&eacute;ussie:', 'Enregistrement effac&eacute; !');

                                }
                                formPart.el.unmask(); 
                            },
                            failure:function(response, options) {  
				var result = Ext.util.JSON.decode(response.responseText);
				Ext.Msg.alert('Erreur', result.errors.reason); 
                                ds.reload();
                                formPart.el.unmask(); 
                            }
                        }); 
                } else {
                  //do nothing
                }
            },this);
            return false;           
        } else {
            Ext.Msg.alert('Erreur','Vous devez s&eacute;lectionner un enregistrement');
        }
    }
    
    });
    

function reset(){

    includePanel.resetToNull();

}

function deleteligne(id){
    includePanel.deleteItem(id);
}

function save(){
    includePanel.saveItem();
}