/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2012/01/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/

Ext.onReady(function(){
 



    var recSPRoduit = new Ext.data.Record.create([{name: 'idProduitConditionnement'},{name: 'idProduit'},{name: 'substance'},{name: 'nom'},{name: 'nomFournisseur'},
            {name: 'referenceFournisseur'},{name: 'idFournisseur'},{name: 'contenance'},{name: 'refrigere'}]);
    


    var cbFournisseur = new Ext.data.JsonStore({
        id:cbFournisseur,
        proxy: proxy,
        fields: ['idFournisseur', 'nom'],
        root:'data',
        totalProperty: 'totalCount',
        nocache: true,
        autoLoad: true,
        baseParams:{action:'getListItems', controller:'Fournisseur'}
    });


        addProduit = new Ext.Window({
                layout:'border',
                width:1000,
                height:500,
                plain: true,
                modal:true,
                closable:false,
                title:'Ajouter un Produit à la commande',

                items: [ {
            id:'panelProduit',
            displayFloatingMsg:true,
            margins: '0 0 0 0',
            columnWidth: 0.9,
            region: 'north',
            xtype: 'fieldset',
            loadMask:{msg: 'Chargement...'},
            labelWidth: 250,
            height:165,
            defaults: {border:false},    // Default config options for child items
            defaultType: 'textfield',
            border:false,
            items: [{
                xtype:'numberfield',
            	fieldLabel: 'Quantite *',
                name: 'quantite',   
                id:'quantite',
                allowDecimals:false,
                width:50,
                blankText:'Le champ est requis'

            }, 
            {
                xtype:'label',
                cls:'form-separator',
                text: 'Rechercher un produit'
            },                    {
                        id:'idFournisseur',
                        xtype:'combo',
                        fieldLabel: 'Fournisseur',
                        name: 'nomFournisseur',
                        width:200,
                        store: cbFournisseur,
                        root:'data',
                        hiddenName : 'idFournisseur',
                        valueField:'idFournisseur',
                        displayField:'nom',
                        typeAhead: true,
                        loadingText: 'Recherche...',
                        mode: 'local',
                        triggerAction: 'all',
                        emptyText:'Selectionner un element...',
                        selectOnFocus:true
                    },{   
                            xtype: 'textfield',
                            fieldLabel: 'CAS',
                            name: 'CAS',
                            id:'CAS',
                            width:100               
                        },{   
                             xtype: 'textfield',
                            fieldLabel: 'Nom',
                            name: 'nom/Description',       
                            id:'nom',
                            width:300        
                        },{
                          xtype:'button',
                          text:'Rechercher le produit',
                          handler:function(){

                                
                                var CAS =addProduit.getComponent('panelProduit').getComponent('CAS').getValue();
                                var nom = addProduit.getComponent('panelProduit').getComponent('nom').getValue();
                                var idFournisseur = addProduit.getComponent('panelProduit').getComponent('idFournisseur').getValue();
                              
                                Ext.getCmp("gridAddProduit").loadMask.show();   
                                
                                Ext.getCmp('gridAddProduit').getStore().load({params:{idFournisseur:idFournisseur,
                                    nom:nom,CAS:CAS},
                                callback:function(){
                                    Ext.getCmp("gridP").loadMask.hide();
                                }});
                              
                                
                              
                          } 
                         },

                    ]},
                         {

                            xtype: 'grid',
                            id:'gridAddProduit',
                            margins: '5 5 5 5',
                            region: 'center',
                            autoScroll:true,
                            enableColumnHide:false,
                            loadMask:{msg: 'Chargement...'},
                            ds: new Ext.data.GroupingStore({
                                proxy: new Ext.data.HttpProxy({
                                            url: 'FrontController',
                                            method: 'POST'
                                        }),
                                fields: ['idProduitConditionnement', 'idProduit','substance', 'nom', 'nomFournisseur', 'referenceFournisseur', 'idFournisseur', 'contenance','refrigere'],
                                root:'data',
                                totalProperty: 'totalCount',
                                reader: new Ext.data.JsonReader({root : "data", id : "idProduitConditionnement"}, recSPRoduit),
                                nocache: true,
                                autoLoad: false,
                                 groupField:'substance',
                                baseParams:{action:'getListProduitAvecConditionnement', controller:'Produit'}       
                            }),
                            cm: new Ext.grid.ColumnModel([
                                {id:'idProduitConditionnement',header: "idProduitConditionnement", dataIndex: 'idProduitConditionnement', hidden:true},
                                {header: "idProduit", dataIndex: 'idProduit', hidden:true},
                                {header: "substance", width: 80, sortable: true, locked:false, dataIndex: 'substance'},
                                {id:'nom',header: "Nom", width: 100, sortable: true, dataIndex: 'nom'},
                                {header: "Fournisseur", width: 200, sortable: true, dataIndex: 'nomFournisseur'},
                                {header: "reference fournisseur", width: 100, sortable: true, dataIndex: 'referenceFournisseur'},
                                {id:'idFournisseur',header: "idFournisseur", dataIndex: 'idFournisseur', hidden:true},
                                {header: "contenance", width: 80, sortable: true, dataIndex: 'contenance'},
                                {header: "Refrigere", width: 40, sortable: true, dataIndex: 'refrigere'}
                            ]),
                            //tbar: new Ext.Toolbar(),
                            view: new Ext.grid.GroupingView({
                                        forceFit:true,
                                        groupTextTpl: '{text} ({[values.rs.length]} {[values.rs.length > 1 ? "Items" : "Item"]})'
                                    }),
                            sm: new Ext.grid.RowSelectionModel({
                                singleSelect: true
//                                ,listeners: {
//                                    rowselect: function(sm, row, rec) {
//
//                                        selRecord = rec;
//                                        
//                                    }
//                                }
                            })                          ,
                            autoExpandColumn: 'nom',
                            border: true
//                            ,plugins:[new Ext.ux.grid.Search({
//                                 mode:'remote'
//                                ,iconCls:'icon-zoom'
//                                ,autoFocus:true
//                                ,minLength:2
//                                ,position:'top'
//                                ,align:'left'
//                                ,width:500
//                                ,shortcutKey:113
//                                ,disableIndexes:['idProduit', 'idProduitConditionnement','substance', 'contenance', 'nomFournisseur','referenceFournisseur']
//                            })]
                         }
              ],
               


                buttons: [{
                    text:'Annuler',
                    handler: function(){
                        addProduit.getComponent('panelProduit').getComponent('quantite').reset();
                        Ext.getCmp('gridAddProduit').getStore().removeAll();
                        addProduit.getComponent('panelProduit').getComponent('CAS').reset();
                        addProduit.getComponent('panelProduit').getComponent('nom').reset();
                        addProduit.getComponent('panelProduit').getComponent('idFournisseur').reset();
                        addProduit.hide();
                    }
                },{
                    text: 'Selectionner',
                    handler: function(){

                        
                        
                       var grid = addProduit.getComponent('gridAddProduit').getSelectionModel() ;
                       var qte = addProduit.getComponent('panelProduit').getComponent('quantite').getValue();

                        if (!grid.hasSelection() || qte== '' || qte==0){
                            Ext.Msg.alert("erreur", "Veuillez saisir une quantité et choisir un article");
                            return;
                        }
                        
                        // Interrogation de la base pour savoir si il y a déjà de ce produit en stock

                        Ext.Ajax.request({
                            params: {
                                controller:'Stock',
                                idSousStructure:includePanel.getForm().findField('idSousStructure').getValue(),
                                idProduitConditionnement:grid.getSelections()[0].data.idProduitConditionnement,
                                action:'getQteItems'
                            },
                            form:'aform',
                            url:'FrontController',
                            // En retour de l'interrogation, on ajoute le produit dans tous les cas (erreur, réponse incohérente, etc..)
                            // SAuf si on reçoit une quantite > 1 et que l'utilisateur répond NON
                            callback :function(op, su, response){
                                if (response.responseText) {						
                                    if (response.responseText.substr(0,1) == '{') {
			    	        var ajaxResponse = Ext.util.JSON.decode(response.responseText);
				     }else{
                                        addProduitToGrid(grid, qte);
                                     }
	                        }else{
                                    addProduitToGrid(grid, qte);
                                }
                                if(ajaxResponse && ajaxResponse.quantite>1) {
                                      Ext.MessageBox.confirm('Etes-vous sur ?','Il reste '+ajaxResponse.quantite + ' unité(s) de cet article en stock. L\'ajouter quand même&nbsp;?',function(btn) {
                                        if(btn == 'yes') {
                                            addProduitToGrid(grid, qte);
                                        }
                                      });
                                }else{
                                    addProduitToGrid(grid, qte);
                                }
                            }
                        });                         
                        


                                   

                    }
                }]
            ,listeners: {
                        show: function(){
                    var param='';
                            Ext.getCmp('idFournisseur').getStore().load({params:{action:'getListItems', controller:'Fournisseur'}});
                    /*
                    // CE CODE SERT A IMPOSER LE FOURNISSEUR QUAND UN PRODUIT EST DEJA AJOUTER
                    // supprimé dans la mesure ou les grossistes peuvent livrer des produits de plusieurs fournisseurs (entendre
                    // plutot fabriquants)
                    if (Ext.getCmp("basketGrid").getStore().getCount()==0){
                        Ext.getCmp('idFournisseur').getStore().load({params:{action:'getListItems', controller:'Fournisseur'}});
                    }else{
                        try
                        {
                            var idFournisseurBasket = Ext.getCmp("basketGrid").getStore().getAt(0).get('idFournisseur');                        
                            if (!isNaN(idFournisseurBasket) && idFournisseurBasket > 0){
                                Ext.getCmp('idFournisseur').getStore().load({params:{action:'getListItems', controller:'Fournisseur',query:idFournisseurBasket,
                                fields:'[idFournisseur]'},callback:function(r, option, success){
                                    if (success)
                                        Ext.getCmp('idFournisseur').setValue(idFournisseurBasket);

                                }})
                            }
                        }catch(err){}
                    }
                    */
                    return true;
                
            }
                    }
            
            });

});


function addProduitToGrid(grid, qte){

                        // Si c'est une livraison classique, on ajoute le produit à un tableau
                        // si on est sur une livraison simple, pas de tableau, mais seulement des formulaires
                        try{
                            var basketGrid = Ext.getCmp("basketGrid");
                            var basketGridStore = basketGrid.getStore();

                                var selRecord = grid.getSelections()[0];
                                var newRecord = new Ext.data.Record({
                                    idCommandeLigne:'0',
                                                           idProduitConditionnement:selRecord.data.idProduitConditionnement,
                                                           CAS: selRecord.data.substance,
                                                           reference:selRecord.data.referenceFournisseur,
                                                           idFournisseur:selRecord.data.idFournisseur,
                                                           nom:selRecord.data.nom,
                                                           quantiteLigne:qte,
                                                           contenance:selRecord.data.contenance,
                                                           quantiteRecue:'0'

                                                    });
                                          // si il n'y a pas déjà des lignes j'ajoute la ligne
                                          if (basketGridStore.getCount() == 0) {
                                            basketGridStore.add(newRecord);
                                          //si il y a deja des ligne, je cherche si il y a une ligne avec cet article
                                          }else{
                                              var index = basketGridStore.find('idProduitConditionnement', selRecord.data.idProduitConditionnement);
                                              if (index>-1){
                                                  var existingRec = basketGridStore.getAt(index);
                                                  existingRec.data.quantiteLigne = parseInt(qte) + parseInt(existingRec.data.quantiteLigne);
                                                  existingRec.commit();
                                              }else{
                                                  basketGridStore.add(newRecord);
                                              }
                                          }
                            
                                addProduit.getComponent('panelProduit').getComponent('quantite').reset();
                                addProduit.getComponent('panelProduit').getComponent('CAS').reset();
                                addProduit.getComponent('panelProduit').getComponent('nom').reset();
                                addProduit.getComponent('panelProduit').getComponent('idFournisseur').reset();                            
                                Ext.getCmp('gridAddProduit').getStore().removeAll();
                                addProduit.hide();

                            
                        }catch(err){
                            var selRecord = grid.getSelections()[0];
                            
                            includePanel.getForm().findField('quantite').setValue(qte);
                            includePanel.getForm().findField('idProduitConditionnement').setValue(selRecord.data.idProduitConditionnement);
                            includePanel.getForm().findField('nomProduit').setValue(selRecord.data.nom);
                            if (includePanel.getForm().findField('produitRefrigere'))
                            includePanel.getForm().findField('produitRefrigere').setValue(selRecord.data.refrigere);
                            if (includePanel.getForm().findField('CAS'))
                            includePanel.getForm().findField('CAS').setValue(selRecord.data.substance);
                            if (includePanel.getForm().findField('referenceFournisseur'))
                            includePanel.getForm().findField('referenceFournisseur').setValue(selRecord.data.referenceFournisseur);
                            addProduit.getComponent('panelProduit').getComponent('quantite').reset();
                            addProduit.getComponent('panelProduit').getComponent('CAS').reset();
                            addProduit.getComponent('panelProduit').getComponent('nom').reset();
                            addProduit.getComponent('panelProduit').getComponent('idFournisseur').reset();                            
                            Ext.getCmp('gridAddProduit').getStore().removeAll();
                            genereLignes();
                            addProduit.hide();

                        }

}