/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2012/01/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/

var myTimer;

Ext.QuickTips.init();

Ext.getUrlParam = function(param) {
   var params = Ext.urlDecode(location.search.substring(1));
   return param ? params[param] : params;
};

    Ext.form.Field.prototype.msgTarget = 'side';

    var bd = Ext.getBody();
        
// ----------------------------------------------------------
// ----------------------------------------------------------
// TABLEAU - GRID 
// acces au données pour le tableau
    var proxy = new Ext.data.HttpProxy({
        url: 'FrontController',
        method: 'POST'
    });
    
    var rec = Ext.data.Record.create([
            {name: 'idArticle'},
            {name: 'idProduitConditionnement'},
            {name: 'identifiantEtiquette'},
            {name: 'CAS'},
            {name: 'nomProduit'},
            {name: 'idAire'},
            {name: 'nomAire'},
            {name: 'idSousStructure'},
            {name: 'nomSousStructure'},
            {name: 'dateOuverture'},
            {name: 'contenance'},
            {name: 'uniteMesure'},
            {name: 'uniteMesureLong'},
            {name: 'quantite'}
    ]);

    var reader = new Ext.data.JsonReader({
 	   root : "data",
 	   id : "idArticle"
    }, rec);


    var dsDon = new Ext.data.JsonStore({
        proxy: proxy,
        fields: ['idDon', 'idUtilisateur', 'idArticle', 'dateFinDon', 'dateDon','accepte', 'idSousStructure', 'nomSousStructure','idAire','nomAire'],
        root:'data',
        totalProperty: 'totalCount',
        reader: new Ext.data.JsonReader({root : "data",id : "idArticle"}, Ext.data.Record.create([{name: 'idDon'},{name: 'idUtilisateur'},{name: 'idArticle'},{name: 'dateFinDon'},
            {name: 'dateDon'},{name: 'accepte'},{name: 'idSousStructure'},{name: 'nomSousStructure'},{name: 'idAire'},{name: 'nomAire'}])),
        nocache: true,
        autoLoad: false,
        baseParams:{action:'getListItems', controller:'ChimieDon'}
    });

    dsDon.on('load', function(thisO, records, options){
        if (records.length > 0){
            includePanel.getForm().findField('idDon').setValue( records[0].get("idDon") );
            includePanel.getForm().findField('donOuiNon').setValue( true );
            includePanel.getForm().findField('dateFinDon').setValue( records[0].get("dateFinDon"));

            if (records[0].get("idSousStructure") > 0){
                includePanel.getForm().findField('idSousStructureDon').setValue( records[0].get("idSousStructure") );
                includePanel.getForm().findField('nomSousStructureDon').setValue( records[0].get("nomSousStructure") );
                includePanel.getForm().findField('idAireStockageDon').setValue( records[0].get("idAire") );
                includePanel.getForm().findField('nomAireStockageDon').setValue( records[0].get("nomAire") );
                Ext.getCmp("checkboxAccepte").enable();
            }

        }

    });


    var idProParam = Ext.getUrlParam("idPro");
    var idArticleParam = Ext.getUrlParam("idArticle");
    var idAireParam = Ext.getUrlParam("idAire");
    var idSsParam = Ext.getUrlParam("idSs");

    var ds = new Ext.data.JsonStore({
        proxy: proxy,
        fields: ['idArticle', 'idProduitConditionnement', 'identifiantEtiquette', 'CAS', 'nomProduit','idAire',
            'nomAire', 'idSousStructure', 'nomSousStructure', 'dateOuverture','contenance','uniteMesure','uniteMesureLong','quantite'],
        root:'data',
        totalProperty: 'totalCount',
        reader: reader,
        nocache: true,
        autoLoad: false,
        baseParams:{action:'getListItems', controller:'Stock'}
    });
    ds.load({params:{conso:true, idProduitConditionnement:idProParam, idArticle:idArticleParam,idAire:idAireParam,idSousStructure:idSsParam},
    callback: function(records,o,s){
            if (ds.getCount()==1){
                Ext.getCmp("gridP").getSelectionModel().selectFirstRow();
            }
            if(typeof(myMask) !== 'undefined')
                myMask.hide();
        }});


    var colModel = new Ext.grid.ColumnModel([
        {dataIndex: 'idArticle', hidden:true},
        {dataIndex: 'idProduitConditionnement', hidden:true},
        {dataIndex: 'idAire', hidden:true},
        {dataIndex: 'idSousStructure', hidden:true},
        {header: "Code Etiquette", width: 110, sortable: true, locked:false, dataIndex: 'identifiantEtiquette'},
        {header: "CAS", width: 70, sortable: true, locked:false, dataIndex: 'CAS'},
        {id:'nomProduit',header: "Produit", width: 200, sortable: true, dataIndex: 'nomProduit'},
        {header: "Local de stockage", width: 200, sortable: true, locked:false, dataIndex: 'nomAire'},
        {header: "Equipe/departement", width: 200, sortable: true, locked:false, dataIndex: 'nomSousStructure'},
        {header: "Date d'ouverture", width: 100, sortable: true, locked:false, dataIndex: 'dateOuverture'},
        {header: "Quantite", width: 60, sortable: true, locked:false, renderer: function(v, params, record){
                    return Ext.util.Format.number(record.data.quantite * record.data.contenance, '0.00') + ' ' + record.data.uniteMesure;
                }}


    ]);

    var PagingBar = new Ext.PagingToolbar({
        pageSize: 9,
        store: ds,
        displayInfo: true,
        action:'getListItems', 
        controller:'Article',
        displayMsg: 'Articles {0} - {1} of {2}',
        emptyMsg: 'Aucun Article'
    });   
// FIN TABLEAU - GRID
// ----------------------------------------------------------   


var idAireFavorite = 0;
var libelleAireFavorite = "";

Ext.Ajax.request({
    url: 'FrontController',
    method: 'GET',
    success: function(response, conn, option){
        var ajaxResponse = Ext.util.JSON.decode(response.responseText);
        idAireFavorite = ajaxResponse.idAireStockage;
        libelleAireFavorite = ajaxResponse.libelleAireFavorite;
    },
    params: {action:'getAireStockageFavorite', controller:'Utilisateur'}
});

// ---------------------------------------------------------- 
// ---------------------------------------------------------- 
//  Toolbar Tableau
     var gridTbar = new Ext.Toolbar({
     cls: 'x-panel-header',
     height: 25,
     items: [
//                {text: 'Ajouter une substance',
//                iconCls: 'icon-add',
//                handler : reset
//               },
//               {text: 'Supprimer une substance',
//                iconCls: 'icon-delete',
//                handler : deleteligne
//               },
                 '->', 
               {text: 'Enregistrer',
                iconCls: 'icon-save',
                handler : save
                }]

    });
    
     var formBbar = new Ext.Toolbar({
     height: 27,
     //cls:'x-bbar',
     items: [                   '->', 
               {id:'btn-enreg',
                text: 'Enregistrer',
                iconCls: 'icon-save',
                handler : save
                }]

    });       
    
// ---------------------------------------------------------- 
    

// ---------------------------------------------------------- 
// STORES

function disableCommandeInterneFields(){
    Ext.getCmp("ConsommationsForm").getForm().findField('contenuRestant').disable();
    Ext.getCmp("ConsommationsForm").getForm().findField('dateConso').disable();
    Ext.getCmp("ConsommationsForm").getForm().findField('commentaires').disable();
    Ext.getCmp("ConsommationsForm").getForm().findField('dateOuverture').disable();
    Ext.getCmp("ConsommationsForm").getForm().findField('qtyLabel').disable();
    Ext.getCmp("ConsommationsForm").getForm().findField('newQty').disable();
    Ext.getCmp("ConsommationsForm").getForm().findField('nomSousStructureMove').disable();
    Ext.getCmp("ConsommationsForm").getForm().findField('affectationUtilisateur').disable();
    Ext.getCmp("btnSearchSousStructure").disable();
    Ext.getCmp("ConsommationsForm").getForm().findField('NomAireStockage').disable();
    Ext.getCmp("btnSearchAireStockage").disable();    
   
}

function enableCommandeInterneFields(){
    Ext.getCmp("ConsommationsForm").getForm().findField('contenuRestant').enable();
    Ext.getCmp("ConsommationsForm").getForm().findField('dateConso').enable();
    Ext.getCmp("ConsommationsForm").getForm().findField('commentaires').enable();
    Ext.getCmp("ConsommationsForm").getForm().findField('qtyLabel').enable();
    Ext.getCmp("ConsommationsForm").getForm().findField('newQty').enable();
    Ext.getCmp("ConsommationsForm").getForm().findField('nomSousStructureMove').enable();
    Ext.getCmp("ConsommationsForm").getForm().findField('affectationUtilisateur').enable();
    Ext.getCmp("btnSearchSousStructure").enable();
    Ext.getCmp("ConsommationsForm").getForm().findField('NomAireStockage').enable();
    Ext.getCmp("btnSearchAireStockage").enable();       
}




// FIN STORES
// ---------------------------------------------------------- 
    
    
    var myslider = new Ext.form.SliderField( {

                            xtype: 'sliderfield',
                            value: 0,
                            width:300,
                            name: 'contenuRestant',
                            tipText: function(thumb){
                                return String(thumb.value) + '%';
                            },
                        disabled:true
                            
                        });
    myslider.slider.on('change', function(obj, newv, oldv) {
            if (newv>includePanel.getForm().findField('quantite').getValue()*100){
                includePanel.getForm().findField('contenuRestant').setValue(includePanel.getForm().findField('quantite').getValue()*100);
            }
        });


    var emptySlider = new Ext.Button({
              xtype:'button',
              iconCls:'icon-delete',
              name:'emptySlider',
              id:'emptySlider',
              tooltip: {
                    title:'Aide',
                    text: '<b>Suppression du stock</b>\n La quantité est mise à zero et l\'article supprimé du stock'
              },
              handler:function(){
                  includePanel.getForm().findField('contenuRestant').setValue(0);
              }

    });
    
    
    var qtyField = new Ext.form.NumberField({
       name:'newQty',
       disabled:true,
       width:60
       
    });
    
    var labelField = new Ext.form.DisplayField({
       value:'   ou Qté ',
       name:'qtyLabel',
       disabled:true,
       width:40
       
    });

    var qteLabelField = new Ext.form.DisplayField({
        value:'',
        name:'qteLabelField',
        width:100

    });


var compositeSlider = new Ext.form.CompositeField({
        name:'compositeF',
        fieldLabel: 'Quantite restante dans le contenant *',
        width     : 600,
        items:[myslider, emptySlider,labelField,qtyField, qteLabelField]
    });

// ---------------------------------------------------------- 
//  Toolbar Tableau
    var formTbar = new Ext.Toolbar({
     height: 27,
     items: ['->',{
                id:'editXLS',
                iconCls:'icon-excel',
                value: '',
                handler:function(){
                    var value = includePanel.getForm().findField('idArticle').getValue();
                    if (value>0){
                        printAction("Consommation","getXLSList", value);
                    }else{
                        Ext.Msg.alert('Export impossible','Veuillez selectionner un item à éditer');
                        return;
                    }
                }            
            },{
                id:'edit',
                iconCls:'icon-print',
                value: '',
                handler:function(){
                    var value = includePanel.getForm().findField('idArticle').getValue();
                    if (value>0){
                        printAction("Consommation","getPDF", value);
                    }else{
                        Ext.Msg.alert('Export impossible','Veuillez selectionner un item à éditer');
                        return;
                    }
                }            
            }

]
    });





    var includePanel = new Ext.FormPanel({
        id: 'ConsommationsForm',
         controller:'Consommation',
        url: 'FrontController',
        frame: true,    
        labelAlign: 'left',
        waitMsgTarget: true,
        width: 1021,
        layout: 'border',    // Specifies that the items will now be arranged in columns
        height:645,
        baseParams:{action:'saveItem', controller:'Consommation'},
        items: [{
            xtype: 'grid',
            id:'gridP',
            split:true,        	
            margins: '2 0 2 2',
            region: 'north',
            //tbar:gridTbar,
            height:125,
            minHeight:200, 
            enableColumnHide:false,
            loadMask:{msg: 'Chargement...'},
            ds: ds,
            cm: colModel,
            sm: new Ext.grid.RowSelectionModel({
                singleSelect: true,
                listeners: {
                    rowselect: function(sm, row, rec) {
                        
                        Ext.getCmp("tabPart").setActiveTab("TabConso");
                        Ext.getCmp("gridHistorique").getStore().removeAll();
                        
                        var formPart = Ext.getCmp("formPart");
                        formPart.el.mask('Chargement...', 'x-mask-loading');

                        includePanel.getForm().reset(); 
                        includePanel.getForm().loadRecord(rec);
                        var qte = Ext.util.Format.number(rec.get("quantite") * rec.get("contenance"), '0.00');
                        var qteS = rec.get("quantite") +' x ' + rec.get("contenance") + ' = <b>' + qte + ' ' + rec.get("uniteMesure") + '</b>';
                        includePanel.getForm().findField('quantiteP').setValue( qteS );
                        includePanel.getForm().findField('contenuRestant').setValue( rec.get("quantite") * 100 );
                        includePanel.getForm().findField('qteLabelField').setValue( rec.get("uniteMesure") + " (maxi "+qte+")" );

                        // Si pas de date d'ouverture : on met la date du jour et on autorise en saisie
                        // sinon, on ne peut pas modifier ce champs
                        if (rec.get("dateOuverture")==""){
                            includePanel.getForm().findField('dateOuverture').setValue( new Date() );
                            Ext.getCmp("ConsommationsForm").getForm().findField('dateOuverture').enable();
                        }else{
                             Ext.getCmp("ConsommationsForm").getForm().findField('dateOuverture').disable();
                        }
                        
                        Ext.getCmp("gridHistorique").getStore().load({
                                    params:{idArticle:rec.get('idArticle'),idSousStructure:rec.get('idSousStructure')},
                                    callback: function(records,o,s){
                                        if (records.length>0){
                                            var affectationUtilisateur = records[0].get('affectationUtilisateur');
                                            includePanel.getForm().findField('affectationUtilisateurDis').setValue(affectationUtilisateur);
                                        }
                                    }
                        });

                        dsDon.load({params:{idArticle:rec.get('idArticle'), accepte:false}});

                        enableCommandeInterneFields();
                        
                        formPart.el.unmask();
                    }
                }
            }),
            autoExpandColumn: 'nomProduit',
             title:'Consommations',
            border: true
        },{
            id:'formPart',
            displayFloatingMsg:true,
            split:true,        	
            margins: '0 0 0 0',
            columnWidth: 0.9,
            autoScroll:false,    
            region: 'center',
            xtype: 'fieldset',
            loadMask:{msg: 'Chargement...'},
            labelWidth: 280,
            height:542,
            //title:'Utilisateurs',
            defaults: {border:false},    // Default config options for child items
            defaultType: 'textfield',
            labelSeparator:'',
            //autoHeight: true,
            bodyStyle: Ext.isIE ? 'padding:0 0 0px 15px;' : 'padding:0px 0px;',
            border: false,
            items: [{   
                name: 'idArticle',
                allowBlank: false,
                hidden:true
            },{   
               name: 'idSousStructure',
               allowBlank: false,
               hidden:true
            },{   
               name: 'idAire',
               allowBlank: false,
               hidden:true
            },{
                name: 'quantite',
                allowBlank: false,
                hidden:true
            },{
                name: 'contenance',
                allowBlank: false,
                hidden:true
            },{
                title: 'Article Selectionné',
                xtype:'fieldset',
                allowBlank: false,
                hidden:false,
                labelWidth: 120,
                labelSeparator:'',
                border:true,
                margins: '0 0 0 0',
                items:[
                        {
                            name: 'identifiantEtiquette',
                            fieldLabel:'Code Etiquette',
                            xtype:'displayfield',
                            readonly:true
                        },                   {
                            name: 'nomProduit',
                            fieldLabel:'produit',
                            xtype:'displayfield',
                            readonly:true
                        },                   {
                            name: 'quantiteP',
                            fieldLabel:'Quantite',
                            xtype:'displayfield',
                            readonly:true
                        },                   {
                            name: 'nomAire',
                            fieldLabel:'Local de stockage',
                            xtype:'displayfield',
                            readonly:true
                        },                   {
                            name: 'nomSousStructure',
                            fieldLabel:'Equipe/Département',
                            xtype:'displayfield',
                            readonly:true
                        },{
                            name: 'affectationUtilisateurDis',
                            fieldLabel:'Affecté à',
                            xtype:'displayfield',
                            readonly:true
                        }

                ]
            }]},{

        xtype:'tabpanel',
        id:'tabPart',
        activeTab: 0,
        region: 'south',
        height:300,
        plain:true,
        //deferredRender:false,
        items:[{

//------------> ONGLET 1
            id:'TabConso',
            title:'Saisie des consommations',
            layout:'form',
            displayFloatingMsg:true,
            loadMask:{msg: 'Chargement...'},  
            bbar:new Ext.Toolbar({height:27,id:'bottom-bar',cls:'x-bbar',items:['->', {id:'btn-enreg', text:'Enregistrer',iconCls:'icon-save',handler:save}]}),
            tbar: formTbar,
            margins: '0 5 5 5',
            baseCls:'x-panel-mc',
            cls:'x-panel-mc-with-tbar',
            labelWidth: 220,
            defaults: {border:false},
            defaultType: 'textfield',
            labelSeparator:'',
            bodyStyle: Ext.isIE ? 'padding:0 0 5px 15px;' : 'padding:10px 15px;',

            items: [


                    compositeSlider,
                    {
                        xtype: 'datefield',
                        fieldLabel: 'Date d\'ouverture du contenant *',
                        name: 'dateOuverture',
                        startDay:1,
                        allowBlank: false,
                        value:new Date(),
                        format:'d/m/Y',
                        blankText:'Le champ est requis',
                        width:100,
                        disabled:true
                    },
                    {
                        xtype: 'datefield',
                        fieldLabel: 'Date de consommation *',
                        name: 'dateConso',
                        startDay:1,
                        allowBlank: false,
                        value:new Date(),
                        format:'d/m/Y',
                        blankText:'Le champ est requis',
                        width:100,
                        disabled:true
                    },{
                        xtype:'textfield',
                        fieldLabel: 'Commentaires',
                        name: 'commentaires',
                        allowBlank: true,
                        width:300,
                        disabled:true
                    },{
                        xtype:'label',
                        fieldLabel: 'Déplacer / Changer d\'affectation :',
                        name: 'deplacer',
                        labelStyle:'font-weight:bold;'
                    }
                  ,
                    
                        

                        {
                          xtype: 'compositefield',
                          fieldLabel: 'Equipe/Département',
                          name:'equipeComposite',
                          items: [
                            {
                                xtype: 'textfield',
                                name: 'nomSousStructureMove',
                                readOnly:true,
                                 disabled:true,
                                width:300
                             },
                             {
                              xtype:'button',
                              text:'Selectionner une Equipe/Département',
                              windowName:'searchSousStructure',
                              id:'btnSearchSousStructure',
                              disabled:true,
                              handler:function(){
                                  var mySearchWin = eval(this.windowName);
                                  sousStructureObj.action = 'getListItems';
                                  sousStructureObj.controller = 'SousStructure';                           
                                  dsSousStructure.load({params:{action:'getListItems', controller:'SousStructure'}});
                                  mySearchWin.show();
                              }
                             }
                        ]},
                        {
                          xtype: 'compositefield',
                          fieldLabel: 'Local de stockage',
                          name:'localStockage',
                          items: [
                            {
                                xtype: 'textfield',
                                name: 'NomAireStockage',
                                readOnly:true,
                                disabled:true,
                                width:300
                             },
                             {
                              xtype:'button',
                              text:'Selectionner un local de stockage',
                              windowName:'searchAireStockage',
                              id:'btnSearchAireStockage',
                              disabled:true,
                              handler:function(){
                                  var mySearchWin = eval(this.windowName);
                                  mySearchWin.show();
                              }
                             },
                              {
                                  xtype:'button',
                                  id:'btn_allAires',
                                  fieldLabel: 'Aire de stockage',
                                  text:'Selectionner votre aire de stockage favorite',
                                  handler:function(){
                                      includePanel.getForm().findField('idAire').setValue(idAireFavorite);
                                      includePanel.getForm().findField('NomAireStockage').setValue(libelleAireFavorite);

                                      console.log(idAireFavorite);

                                  }}
                        ]},{
                        xtype:'textfield',
                        fieldLabel: 'Affecter à une personne',
                        name: 'affectationUtilisateur',
                        id:'affectationUtilisateur',
                        allowBlank: true,
                        width:255,
                        disabled:true
                    }
                    
                        
                    
                    
                    
                    
         ]},{

//------------> ONGLET 2
            id:'TabHistorique',
            title:'Historique',
            layout:'form',
            displayFloatingMsg:true,
            loadMask:{msg: 'Chargement...'},  
            margins: '0 5 5 5',
            baseCls:'x-panel-mc',
            cls:'x-panel-mc-with-tbar',
            labelWidth: 220,
            defaults: {border:false},
            defaultType: 'textfield',
            labelSeparator:'',
            bodyStyle: Ext.isIE ? 'padding:0 0 5px 15px;' : 'padding:10px 15px;',

            items: [
                
            {
  
                xtype: 'editorgrid',
                id:'gridHistorique',
                margins: '0 5 5 5',
                region: 'center',
                clicksToEdit: 1,
                height:180,
                enableColumnHide:false,
                loadMask:{msg: 'Chargement...'},
                sm: new Ext.grid.RowSelectionModel({}),
                border: true,
                ds: new Ext.data.JsonStore({
                        proxy: proxy,
                        fields: ['idHistoriqueConsommation','dateConsommation','idUtilisateur','nomUtilisateur', 'idSousStructure','nomSousStructure','idAire','nomAire','quantiteRestante','commentaire','affectationUtilisateur'],
                        root:'data',
                        autoSave:false,
                        totalProperty: 'totalCount',
                        reader: new Ext.data.JsonReader({root : "data",id : "idHistoriqueConsommation"}),
                        nocache: true,
                        autoLoad: false,
                        baseParams:{action:'getListHistorique', controller:'Consommation'}
                    }),
                cm:     new Ext.grid.ColumnModel({columns: [
                    {dataIndex:'idHistoriqueConsommation', hidden:true},       
                    {header: "Date Conso", width: 90, sortable: true, dataIndex: 'dateConsommation'},                    
                    {header: 'idUtilisateur', dataIndex: 'idUtilisateur', width: 70, hidden:true},
                    {header: 'Utilisateur', dataIndex: 'nomUtilisateur', width: 100, sortable:true},                                
                    {header: 'idSousStructure', dataIndex: 'idSousStructure', hidden:true},
                    {header: 'Structure', dataIndex: 'nomSousStructure', width: 200, sortable:true},
                    {header: 'idAire', dataIndex: 'idAire', hidden:true},
                    {header: "Aire de Stockage", dataIndex:'nomAire', width: 200, sortable: true}, 
                    {header: "Qté Restante", dataIndex:'quantiteRestante', width: 40, sortable: true},
                    {header: "Commentaire", dataIndex:'commentaire', width: 160, sortable: true},
                    {header: "Affectation", dataIndex:'affectationUtilisateur', width: 160, sortable: true} 
        
                   ]})
                }                  
                
            ]},
//------------> ONGLET 3
            {
                id:'TabDon',
                title:'Don',
                layout:'form',
                displayFloatingMsg:true,
                loadMask:{msg: 'Chargement...'},
                margins: '0 5 5 5',
                baseCls:'x-panel-mc',
                bbar:new Ext.Toolbar({height:27,id:'bottom-bar2',cls:'x-bbar',items:['->', {id:'btn-enreg', text:'Enregistrer',iconCls:'icon-save',handler:gereDon}]}),
                cls:'x-panel-mc-with-tbar',
                labelWidth: 220,
                defaults: {border:false},
                defaultType: 'textfield',
                labelSeparator:'',
                bodyStyle: Ext.isIE ? 'padding:0 0 5px 15px;' : 'padding:10px 15px;',

                items: [

                    {
                        name: 'idDon',
                        hidden:true
                    },
                    {
                        xtype:'checkbox',
                        fieldLabel: 'Don',
                        name: 'donOuiNon',
                        inputValue:'true',
                        boxLabel:'Proposer cet article au don avant destruction',
                        checked: false
                    },
                    {
                        xtype: 'datefield',
                        fieldLabel: 'Date de validité',
                        name: 'dateFinDon',
                        startDay:1,
                        value:new Date(),
                        format:'d/m/Y',
                        blankText:'Le champ est requis',
                        width:100
                    }
                    ,
                    {
                        xtype:'label',
                        fieldLabel: ' '
                    },
                    {
                        xtype:'label',
                        fieldLabel: 'Proposition de récupération'
                    },
                    {
                        name: 'idSousStructureDon',
                        hidden:true
                    } ,
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Equipe/Département',
                        name: 'nomSousStructureDon',
                        readOnly:true,
                        width:300
                    },
                    {
                        name: 'idAireStockageDon',
                        hidden:true
                    },
                                         {
                        xtype: 'textfield',
                        fieldLabel: 'Aire de stockage',
                        name: 'nomAireStockageDon',
                        readOnly:true,
                        width:300
                    },
                    {
                        xtype: 'checkboxgroup',
                        id: 'checkboxAccepte',
                        fieldLabel: 'Accepter proposition',
                        disabled:true,
                        defaultType: 'radio',
                        columns: [100, 100],
                        vertical: true,
                        cls: 'x-check-group-alt',
                        items: [
                            {boxLabel: 'Oui', name: 'accepteDon', inputValue: 'true'},
                            {boxLabel: 'Non', name: 'accepteDon', inputValue: 'false'}
                        ]
                    }


                ]




            }
 


]}
            
            
        ],
        saveItem:function(){
            
            var qteMax = includePanel.getForm().findField('quantite').getValue()*includePanel.getForm().findField('contenance').getValue();
            var newQty = includePanel.getForm().findField('newQty').getValue();
            if (newQty > qteMax){
                Ext.Msg.alert('Erreur', "La nouvelle quantité doit être inferieur ou égale à l'ancienne quantité");  
            }else{
            
                if (includePanel.getForm().isValid()){
                    includePanel.getForm().submit({waitTitle:'Connexion au serveur', waitMsg:'Enregistrement en cours...',
                            success:function(f,action){
                               ds.reload();
                               includePanel.getForm().reset();   
                               Ext.getCmp("gridHistorique").getStore().removeAll();
                               disableCommandeInterneFields();
                                Ext.msg.show(this.title ? this.title : 'Op&eacute;ration r&eacute;ussie:', 'Enregistrement effectu&eacute; !');

                                // Ajout de l'avertissement si le produit n'est plus disponible
                                if (action.result.qteRestante<0.3){
                                    Ext.MessageBox.show({
                                        buttons: Ext.Msg.OKCANCEL,
                                        title:'Quantite Faible',
                                        msg:'Il ne reste plus qu\'une faible quantite de ce produit, voulez vous générer une commande ?',
                                        fn:function(btn) {
                                            if(btn == 'ok') {
                                                var uri = "FrontController?controller=Commande&action=getHtml&idProduitConditionnement="+action.result.idProduitConditionnement+"&idSousStructure="+action.result.idSousStructure;
                                                window.location = uri;

                                            }else{
                                                return;
                                            }
                                        }});
                                }


                    },
                            failure:function(f,action){
                                    var result = Ext.util.JSON.decode(action.response.responseText);
                                    Ext.Msg.alert('Erreur', result.errors.reason);   
                            }
                    });                    
                }               
            }
        },
        resetToNull: function() {
           var form = this.getForm();
           if (form.isDirty()){
            Ext.MessageBox.show({
               buttons: Ext.Msg.OKCANCEL,
               title:'Etes-vous sur ?',
               msg:'Les données non enregistrées vont être effacées. Etes-vous sur ?',
               fn:function(btn) {
                   if(btn == 'ok') {
                       Ext.getCmp("ConsommationsForm").resetForm();
                       Ext.getCmp("gridP").getSelectionModel().clearSelections();
                        Ext.getCmp("gridHistorique").getStore().removeAll();
                   }else{
                       return;
                   }
            }});
            return false;
            }

    },
    resetForm:function(){
        var form = this.getForm();
        form.items.each(function(f){			
       if(f.getXType() == 'xcheckbox')
            f.originalValue = false;
        else
            f.originalValue = '';
        }, form);
        form.reset();
    },   
    deleteItem: function(id) {
        if(id == null)
            var this_id = this.getForm().findField(this.gridP).getValue();
        else
            var this_id = id;
        if(this_id != '') {
            Ext.MessageBox.confirm('Etes-vous sur ?','Voulez-vous effacer cet enregistrement&nbsp;?',function(btn) {
                if(btn == 'yes') {
                        Ext.getCmp("gridP").loadMask.show();   
                        var formPart = Ext.getCmp("formPart");
                        formPart.el.mask('Chargement...', 'x-mask-loading');                     
                        var myFormParams = {
                            controller:this.controller,
                            action:'deleteItem',
                            id:this_id,
                            output:'json'
                        };
                        // adding extra params if necessary!
                        if (this.extraParams) {
                            for (key in this.extraParams) {
                                myFormParams[key] = this.extraParams[key]; 
                            }
                        }
                        Ext.Ajax.request({
                            params:myFormParams,
                            url:'FrontController',
                             success:function(response, options){
                                if (response.responseText) {						
                                    if (response.responseText.substr(0,1) == '{') {
			    	        var ajaxResponse = Ext.util.JSON.decode(response.responseText);
				     }
	                        }
                                if(ajaxResponse && ajaxResponse.success) {
                                    includePanel.getForm().reset();
                                    ds.reload();
                                    Ext.msg.show(this.title ? this.title : 'Op&eacute;ration r&eacute;ussie:', 'Enregistrement effac&eacute; !');

                                }
                                formPart.el.unmask(); 
                            },
                            failure:function(response, options) {  
				var result = Ext.util.JSON.decode(response.responseText);
				Ext.Msg.alert('Erreur', result.errors.reason); 
                                ds.reload();
                                formPart.el.unmask(); 
                            }
                        }); 
                } else {
                  //do nothing
                }
            },this);
            return false;           
        } else {
            Ext.Msg.alert('Erreur','Vous devez s&eacute;lectionner un enregistrement');
        }
    }
    
    });



function gereDon(){

    var idDon = includePanel.getForm().findField('idDon').getValue();

    var idArticle = includePanel.getForm().findField('idArticle').getValue();
    var donOuiNon = includePanel.getForm().findField('donOuiNon').getValue();
    var dateFinDon = includePanel.getForm().findField('dateFinDon').getValue();
    var accepteDon = includePanel.getForm().findField('accepteDon').getValue();
    var idAireStockageDon = includePanel.getForm().findField('idAireStockageDon').getValue();
    var idSousStructureDon = includePanel.getForm().findField('idSousStructureDon').getValue();


    // si on propose un don  date oui non obligatoire
    if (donOuiNon){
        if (dateFinDon==""){
            alert("Veuiller saisir la date de fin de validité du don");
            return;
        }

    }

    if (idArticle > 0) {

        // action par défaut : on enregistre le don
        var action = "saveItem";

        Ext.Ajax.request({
            url: 'FrontController',
            //success: someFn,
            //failure: otherFn,
            params: {
                controller:'ChimieDon',
                action:action,
                idDon:idDon,
                donOuiNon:donOuiNon,
                accepteDon: accepteDon,
                dateFinDon:dateFinDon,
                idArticle:idArticle
            },
            success: function(response, conn, option){
                Ext.getCmp("checkboxAccepte").disable();
                Ext.getCmp("btn-enreg").disable();

                Ext.msg.show('Op&eacute;ration r&eacute;ussie:', 'Enregistrement effectu&eacute; !');

            },
            failure: function(response, conn, option){
                Ext.msg.alert('Erreur', 'Enregistrement impossible !');
            }
        });

    }

}

function reset(){

    includePanel.resetToNull();

}

function deleteligne(){
    includePanel.deleteItem();
}

function save(){
    includePanel.saveItem();
}

var mapwin;

function closeImgWindow(){
	if(mapwin){
		mapwin.hide();
		mapwin= null;
	}
}

function openImgWindow(imageName, imageId){

		if(!mapwin){
			mapwin = new Ext.Window({
				layout: 'fit',
				closeAction: 'hide',
				width:164,
				height:222,
                                x:10,
                                cls:'whitebck',
				items: {
					xtype:'box',
					showAnimDuration:1
					,anchor:''
                                        ,cls:'whitebck'
					,isFormField:true
					,fieldLabel:'Image'
					,autoEl:{
                                            tag:'div', children:[{
                                                     tag:'img'
                                                    ,src:imageName+'.jpg'
                                            }]
					}
					
				}
			});					
		}        
        mapwin.show();
}