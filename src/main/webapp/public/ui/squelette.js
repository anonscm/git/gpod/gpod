/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2012/01/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/

function quit(){
    window.location='Quit';
}

var mainPanelTbar = new Ext.Toolbar({
 cls: 'x-panel-header',
 height: 25,
 items: [
  '<span style="color:#15428B; font-weight:bold">Gestion des produits dangereux</span>'
  , '->','<span style="color:#000000";>Utilisateur : <b>'+B_nameUser+'</b>, dernière connexion le <b>'+B_lastCx+'</b> </span>','-',
  {
   text: 'Deconnexion',
   iconCls: 'icon-close',
   handler: quit
 },' '
]
});

Ext.MessageBox.buttonText.ok = "Oui";
Ext.MessageBox.buttonText.cancel = "Annuler";
Ext.MessageBox.buttonText.yes = "Oui";
Ext.MessageBox.buttonText.no = "Non";

Ext.onReady(function(){
    
    // --------------------------------------------------
    // GESTION DU TIMEOUT
        // si une requete POST retourne un json avec action=reload
        // on recharge la page
        Ext.Ajax.on('requestcomplete', function(a, b, c){
            try{
                var ajResp = Ext.util.JSON.decode(b.responseText);
                if (!ajResp.success && ajResp.action=='reload'){
                    window.location.replace("Authentification?deconnect=true");
                }
            }catch(e){
                
            }
            
        });
       // si on le parametre deconnect=true on affiche un message pour avertir l'utilisateur
       var params = Ext.urlDecode(location.search.substring(1));
       var deco = params['deconnect'];
       if (deco){
           Ext.Msg.alert('Deconnexion','Suite à temps d\'inactivité sur l\'application, vous avez été déconnecté et reconnecté.');
       }
    // FIN GESTION TIMEOUT
    // --------------------------------------------------

	
	var myPan = new Ext.Panel({
           tbar:mainPanelTbar,
           renderTo:'centerPanel',
           width:1024,
           height:700,
           layout:'border',
            layoutConfig: {
                padding:'5',
                pack:'center',
                align:'middle'
            }    ,       
           autoScroll:true,
           items: [{
                    region:'north',
                    split:false,
                    layout:'anchor',
                    margins: '0 0 0 0',
                    height:27,
                    items:tb
                }, {
                    id:'mainWindow',
                    region:'center',
                    margins: '0 0 0 0',
                    layout: 'absolute',
                    items:includePanel
                   }]
	});

});