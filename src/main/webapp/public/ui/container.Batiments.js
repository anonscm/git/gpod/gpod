/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2012/01/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/


Ext.QuickTips.init();

    Ext.form.Field.prototype.msgTarget = 'side';

    var bd = Ext.getBody();
    
    var proxy = new Ext.data.HttpProxy({
        url: 'FrontController',
        method: 'POST'
    });

    var recB = Ext.data.Record.create([
            {name: 'idBatiment'},
            {name: 'idSite'},
            {name: 'nomSite'},
            {name: 'code'},
            {name: 'nom'},
            {name: 'adresse1'},
            {name: 'adresse2'},
            {name: 'codePostal'},
            {name: 'ville'},
            {name: 'telephone'},
            {name: 'fax'},
            {name: 'longitude'},
            {name: 'latitude'}
    ]);


    var dsBatiment = new Ext.data.JsonStore({
        proxy: proxy,
        fields: ['idBatiment', 'idSite', 'nomSite',  'code', 'nom', 'adresse1', 'adresse2', 'codePostal', 'ville', 'telephone','fax','longitude','latitude'],
        root:'data',
        totalProperty: 'totalCount',
        reader: new Ext.data.JsonReader({root : "data",id : "idBatiment"}, recB),
        nocache: true,
        autoLoad: false,
        baseParams:{action:'getListItems', controller:'Batiment'}
        ,listeners:{
            load:function(){
                if(typeof(myMask) !== 'undefined')
                    myMask.hide();
            }
        }
    });
    dsBatiment.load({params:{start:0, limit:10}});
   
   
    var PagingBar = new Ext.PagingToolbar({
        pageSize: 10,
        store: dsBatiment,
        displayInfo: true,
        action:'getListItems', 
        controller:'Batiment',
        displayMsg: 'Batiments {0} - {1} of {2}',
        emptyMsg: 'Aucun Batiment'
    });      
    PagingBar.insertButton(11, excelButton);
   
// ----------------------------------------------------------
// ----------------------------------------------------------
// TABLEAU - GRID 
// acces au données pour le tableau

 

    var recS = Ext.data.Record.create([{name: 'idSalleStockage'},{name: 'idBatiment'},{name: 'nomBatiment'},{name: 'nom'},{name: 'code'},{name: 'description'}]);

    var dsSalleStockage = new Ext.data.JsonStore({
        proxy: proxy,
        fields: ['idSalleStockage', 'idBatiment', 'nomBatiment', 'nom', 'code', 'description'],
        root:'data',
        totalProperty: 'totalCount',
        reader: new Ext.data.JsonReader({root : "data",id : "idSalleStockage"}, recS),
        nocache: true,
        autoLoad: false,
        baseParams:{action:'getListItems', controller:'SalleStockage'}
        ,listeners:{
            load:function(){
                if(typeof(myMask) !== 'undefined')
                    myMask.hide();
            }
        }
    });


// FIN TABLEAU - GRID
// ----------------------------------------------------------   
       
    

// ---------------------------------------------------------- 
//  Toolbar Tableau
  var formTbar = new Ext.Toolbar({
     height: 27,
     items: [{text: 'Nouveau Batiment',
                iconCls: 'icon-add',
                handler : reset
               },'->',{
                id:'edit',
                iconCls:'icon-excel',
                value: '',
                handler:function(){
                    var value = includePanel.getForm().findField('idBatiment').getValue();
                    if (value>0){
                        printAction("Batiment","getXLSList", value);
                    }else{
                        Ext.Msg.alert('Export impossible','Veuillez selectionner un item à éditer');
                        return;
                    }
                }            
            },{
                id:'editPDF',
                iconCls:'icon-print',
                value: '',
                handler:function(){
                    var value = includePanel.getForm().findField('idBatiment').getValue();
                    if (value>0){
                        printAction("Batiment","getPDF", value);
                    }else{
                        Ext.Msg.alert('Export impossible','Veuillez selectionner un item à éditer');
                        return;
                    }
                }            
            }]
    });
    
     var formBbar = new Ext.Toolbar({
     height: 27,
     cls:'x-bbar',
     id:'bottom-bar',
     items: [                   '->', 
               {id:'btn-enreg',
                text: 'Enregistrer',
                iconCls: 'icon-save',
                handler : save
                }]

    });   
// ---------------------------------------------------------- 
       

// ---------------------------------------------------------- 
// STORES

    // ComboBox site
    var cbsite = new Ext.data.JsonStore({
        id:cbsite,
        proxy: proxy,
        fields: ['idSite', 'nom'],
        root:'data',
        totalProperty: 'totalCount',
        nocache: true,
        autoLoad: true,
        baseParams:{action:'getListItems', controller:'Site'}
    });
    
        // ComboBox TypeAire
    var cbtypeaire = new Ext.data.JsonStore({
        proxy: proxy,
        fields: ['idTypeAire', 'nom'],
        root:'data',
        totalProperty: 'totalCount',
        nocache: true,
        autoLoad: true,
        baseParams:{action:'getListItems', controller:'TypeAire'}
    });

    var recA = Ext.data.Record.create([{name: 'idAire'},{name: 'nom'},{name: 'idTypeAire'},{name: 'nomTypeAire'},{name: 'refrigere'},{name: 'description'}]);

    var dsAire = new Ext.data.JsonStore({
        proxy: proxy,
        fields: ['idAire','nom','idTypeAire', 'nomTypeAire','code', 'refrigere', 'description'],
        root:'data',
        autoSave:false,
        totalProperty: 'totalCount',
        reader: new Ext.data.JsonReader({root : "data",id : "idAire"}, recA),
        writer:new Ext.data.JsonWriter({encode: true,writeAllFields: false}),
        nocache: true,
        autoLoad: false,
        baseParams:{action:'getListItems', controller:'AireStockage'}
    });

    var cmAire = new Ext.grid.ColumnModel({
        // specify any defaults for each column
        defaults: {
            sortable: true // columns are not sortable by default
        },
        columns: [{
            id: 'idAire',
            dataIndex: 'idAire',
            hidden:true
        },{
            id: 'nom',
            header: 'Nom',
            dataIndex: 'nom',
            width: 140
        },{
            id: 'idTypeAire',
            dataIndex: 'idTypeAire',
            hidden:true
        },{
            id: 'nomTypeAire',
            header: 'Type',
            dataIndex: 'nomTypeAire',
            width: 110
        },{
            id: 'code',
            header: 'Code',
            dataIndex: 'code',
            width: 100
        },{
            id: 'refrigere',
            header: 'ref',
            dataIndex: 'refrigere',
            width: 40
        },{
            id: 'description',
            header: 'Commentaire',
            dataIndex: 'description',
            width: 155
        },        
            {header: "Supp", width: 37, sortable: false, xtype:'actioncolumn',
            items: [{
                    getClass: function(v, meta, rec) {          // Or return a class from a function
                            this.items[0].tooltip = 'Supprimer cette ligne';
                            return 'del-col';
                    },
                    handler: function(grid, rowIndex, colIndex) {
                        var rec = dsAire.getAt(rowIndex);
                        deleteligne(rec.get('idAire'),"AireStockage");                        
                    }
                }]} 
        ]
    });

// ---------------------------------------------------------- 
    
    var includePanel = new Ext.FormPanel({
        id: 'BatimentForm',
        controller:'Batiment',
        url: 'FrontController',
        frame: true,    
        labelAlign: 'left',
        waitMsgTarget: true,
        width: 1021,
        layout: 'border',    // Specifies that the items will now be arranged in columns
        height:645,
        baseParams:{action:'saveItem', controller:'Batiment'},
        items: [{
            xtype: 'grid',
            id:'gridBatiment',     	
            margins: '5 0 5 0',
            region: 'north',
            height:287,
            bbar: PagingBar , 
            enableColumnHide:false,
            loadMask:{msg: 'Chargement...'},
            ds: dsBatiment,
            cm: new Ext.grid.ColumnModel([
                {id:'idBatiment',header: "idBatiment", width: 80, sortable: true, locked:false, dataIndex: 'idBatiment', hidden:true},
                {header: "idSite", width: 100, sortable: true, dataIndex: 'idSite', hidden:true},
                {header: "Site", width: 100, sortable: true, dataIndex: 'nomSite'},
                {id:'code',header: "Code", width: 100, sortable: true, dataIndex: 'code'},
                {id:'nom',header: "Nom", width: 100, sortable: true, dataIndex: 'nom'},
                {header: "Adresse", width: 100, sortable: true, dataIndex: 'adresse1'},
                {header: "adresse2", width: 200, sortable: true, dataIndex: 'adresse2', hidden:true},
                {header: "codepostal", width: 100, sortable: true, dataIndex: 'codePostal', hidden:true},
                {header: "ville", width: 200, sortable: true, dataIndex: 'ville'},
                {header: "telephone", width: 200, sortable: true, dataIndex: 'telephone', hidden:true},
                {header: "fax", width: 200, sortable: true, dataIndex: 'fax', hidden:true},
                {header: "longitude", width: 200, sortable: true, dataIndex: 'longitude', hidden:true},
                {header: "latitude", width: 200, sortable: true, dataIndex: 'latitude', hidden:true},
                {header: "Supp", width: 40, sortable: false, xtype:'actioncolumn',
                items: [{
                            getClass: function(v, meta, rec) {          // Or return a class from a function
                                    this.items[0].tooltip = 'Supprimer cette ligne';
                                    return 'del-col';
                            },
                            handler: function(grid, rowIndex, colIndex) {
                                var rec = dsBatiment.getAt(rowIndex);
                                deleteligne(rec.get('idBatiment'));
                            }
                        }]} 
            ]),
            sm: new Ext.grid.RowSelectionModel({
                singleSelect: true,
                listeners: {
                    rowselect: function(sm, row, rec) {
                        var formPart = Ext.getCmp("formPart");
                        var this_id = rec.data.idUtilisateur;
                        formPart.el.mask('Chargement...', 'x-mask-loading');
                        Ext.getCmp("BatimentForm").getForm().loadRecord(rec); 
                        
                        
                        
                        
                        //var point = new GPoint(1,1);
                        var marker = [{
                            lat: rec.data.latitude,
                            lng: rec.data.longitude,
                            marker: {title: rec.data.nom, clear:true},
                            setCenter:true
                        }];
                        
                        includePanel.getForm().findField('longlat').setValue(rec.data.longitude + '/' +rec.data.latitude); 
                        Ext.getCmp("myGmap").clearMarkers();
                        Ext.getCmp("myGmap").addMarkers(marker); 
                        dsAire.removeAll();
                        Ext.getCmp("gridSalle").getSelectionModel().clearSelections();  
                        includePanel.getForm().findField('idSalleStockage').reset();    
                        includePanel.getForm().findField('idAireStockage').reset();                                               
                        includePanel.getForm().findField('nomSalle').reset();
                        includePanel.getForm().findField('codeSalle').reset();
                        includePanel.getForm().findField('descriptionSalle').reset();                          
                        Ext.getCmp("TabAireStockage").disable();
                        Ext.getCmp("tabPart").setActiveTab("TabBatiment");
                        dsSalleStockage.removeAll();
                        dsSalleStockage.load({
                            params:{idBatiment:rec.get('idBatiment')}
                        })
                        Ext.getCmp("TabSalleStockage").enable();
                        
                        formPart.el.unmask();
                    }
                }
            })
            ,autoExpandColumn: 'nom',
             title:'Batiments',
            border: true,
            
            plugins:[new Ext.ux.grid.Search({
                 mode:'remote'
                ,iconCls:'icon-zoom'
                ,autoFocus:true
                ,minLength:2
                ,position:'bottom'
                ,align:'left'
                ,width:200
                ,shortcutKey:113
                ,disableIndexes:['idBatiment', 'telephone','fax','longitude','latitude', 'nomSite', 'idSite']
            })]
        },
        
        
        
        {
            xtype:'panel',
            layout:'border',
            region: 'center',
            autoScroll:true,
            id:'formPart',         
            defaults: {frame:true},
            border: true,
            items:[
         {
            xtype:'tabpanel',
            id:'tabPart',
            activeTab: 0,       
            columnWidth: 0.9,
            width:625,
            plain:true,
            deferredRender:false,                
            region: 'west',
            autoScroll:true,            
            defaults: {frame:true},
            border: true,
        
            items:[{

//------------> ONGLET 1
            id:'TabBatiment',
            title:'Batiment',
            layout:'form',
            displayFloatingMsg:true,
            loadMask:{msg: 'Chargement...'},  
            bbar:formBbar,
            tbar: formTbar,
            margins: '0 5 5 5',
            baseCls:'x-panel-mc',
            cls:'x-panel-mc-with-tbar',
            labelWidth: 190,
            height:300,
            defaults: {border:false},
            defaultType: 'textfield',
            labelSeparator:'',
            bodyStyle: Ext.isIE ? 'padding:0 0 5px 15px;' : 'padding:10px 15px;',

            items: [{
                                    fieldLabel: 'idBatiment',
                                    name: 'idBatiment',
                                    hidden:true
                                },{   
                                    fieldLabel: 'Nom *',
                                    name: 'nom',
                                    allowBlank: false,
                                    blankText:'Le champ est requis',                
                                    width:200,
                                    maxLength:45,
                                    maxLengthText:'Longueur maxi : 45'
                                },
                                {
                                    xtype:'combo',
                                    allowBlank: false,
                                    blankText:'Le champ est requis',
                                    fieldLabel: 'Site *',
                                    name: 'nomSite',
                                    width:200,
                                    store: cbsite,
                                    root:'data',
                                    hiddenName : 'idSite',
                                    valueField:'idSite',
                                    displayField:'nom',
                                    typeAhead: true,
                                    loadingText: 'Recherche...',
                                    mode: 'local',
                                    triggerAction: 'all',
                                    emptyText:'Selectionner un element...',
                                    selectOnFocus:true
                                }
                                                                
                                ,{   
                                    id:'code',  
                                    fieldLabel: 'Code/Numero',
                                    name: 'code',        
                                    width:100,
                                    maxLength:25,
                                    maxLengthText:'Longueur maxi : 25'                
                                }
                                ,{   
                                    id:'adresse1',  
                                    fieldLabel: 'Adresse',
                                    name: 'adresse1',        
                                    width:300,
                                    maxLength:255,
                                    maxLengthText:'Longueur maxi : 255'                
                                },{   
                                    fieldLabel: 'Complement',
                                    name: 'adresse2',
                                    width:300,
                                    maxLength:255,
                                    maxLengthText:'Longueur maxi : 255'                        
                                },{            	
                                    xtype:'numberfield',
                                    fieldLabel: 'Code Postal',
                                    name: 'codePostal',
                                    allowDecimals:false,
                                    width:50,
                                    maxLength:6,
                                    maxLengthText:'Longueur maxi : 6'
                                },{
                                    fieldLabel: 'Ville',
                                    name: 'ville',
                                    width:300,
                                    maxLength:255,
                                    maxLengthText:'Longueur maxi : 255'                        
                                }
                                ,{
                                    fieldLabel: 'Long/Lat',
                                    //xtype:'numberfield',
                                    name: 'longlat',
                                    width:300,
                                    disabled:true
                                }
                                ,{
                                    xtype:'numberfield',
                                    fieldLabel: 'Longitude',
                                    name: 'longitude',
                                    decimalPrecision:15,
                                    //decimalSeparator:',',
                                    width:100,
                                    hidden:true
                                },{
                                    xtype:'numberfield',
                                    fieldLabel: 'Latitude',
                                    name: 'latitude',
                                    decimalPrecision:15,
                                    //decimalSeparator:',',
                                    width:100,
                                    hidden:true
                                }
             ]
        }                           
//------------> FIN ONGLET 1

            , {

//------------> ONGLET 2
            id:'TabSalleStockage',
            title:'Salle de Stockage',
            layout:'form',
            disabled:true,
            displayFloatingMsg:true,
            loadMask:{msg: 'Chargement...'},  
            margins: '0 5 5 5',
            baseCls:'x-panel-mc',
            cls:'x-panel-mc-with-tbar',
            labelWidth: 190,
            defaults: {border:false},
            defaultType: 'textfield',
            labelSeparator:'',
            bodyStyle: Ext.isIE ? 'padding:0 0 5px 15px;' : 'padding:10px 15px;',

            items: [{
                xtype:'panel',
                layout:'form',
                region: 'center',
                autoScroll:true,        
                margins: '0 5 5 5',
                baseCls:'x-panel-mc',
                cls:'x-panel-mc-with-tbar',
                labelWidth: 190,
                defaults: {border:false},
                defaultType: 'textfield',
                labelSeparator:'',
                bodyStyle: Ext.isIE ? 'padding:0 0 5px 15px;' : 'padding:10px 15px;',
                items:[
                
                
                
                                {
                                    fieldLabel: 'idSalleStockage',
                                    name: 'idSalleStockage',
                                    hidden:true
                                },{   
                                    fieldLabel: 'Nom *',
                                    name: 'nomSalle',
                                    blankText:'Le champ est requis',                
                                    width:200,
                                    maxLength:45,
                                    maxLengthText:'Longueur maxi : 45'
                                }
                                
                                                              
                                ,{   
                                    id:'codeSalle',  
                                    fieldLabel: 'Code/Numero',
                                    name: 'codeSalle',        
                                    width:100,
                                    maxLength:25,
                                    maxLengthText:'Longueur maxi : 25'                
                                },{
                                    xtype: 'compositefield',
                                    fieldLabel: 'Description',
                                    items: [{     
                                      xtype:'textfield',
                                      fieldLabel: 'Description',
                                      name: 'descriptionSalle',
                                      allowBlank: true,
                                      width:230
                                    }  
                                    ,
                                        {
                                              xtype:'button',
                                              text:'Ajouter/Modifier',
                                              id: 'addSalle',
                                              handler:function(){
                                                  
                                        var idSalleStockage  = includePanel.getForm().findField('idSalleStockage').getValue();  
                                        var idBatiment  = includePanel.getForm().findField('idBatiment').getValue();     
                                        var nom = includePanel.getForm().findField('nomSalle').getValue();
                                        var code = includePanel.getForm().findField('codeSalle').getValue();
                                        var description = includePanel.getForm().findField('descriptionSalle').getValue();

                                        
                                        if (nom !="" && idBatiment > 0){
                                            Ext.getCmp("TabSalleStockage").el.mask('Ajout de la salle...', 'x-mask-loading');    
                                        
                                            Ext.Ajax.request({
                                                method:'POST',
                                                url:'FrontController',
                                                form: 'some-form',
                                                params: {controller:'SalleStockage', action: 'saveItem', idBatiment:idBatiment,idSalleStockage:idSalleStockage,
                                                nom:nom, code:code, description:description},
                                                success: function(response, options){
                                                        if (response.responseText) {						
                                                            if (response.responseText.substr(0,1) == '{') {
                                                                var ajaxResponse = Ext.util.JSON.decode(response.responseText);
                                                             }
                                                        }
                                                        if(ajaxResponse && ajaxResponse.success) {
                                                            Ext.msg.show(this.title ? this.title : 'Op&eacute;ration r&eacute;ussie:', 'Enregistrement effectu&eacute; !');

                                                        }
                                                        dsSalleStockage.removeAll();
                                                        dsSalleStockage.load({
                                                            params:{idBatiment:idBatiment}
                                                        })                               
                                                        includePanel.getForm().findField('idAireStockage').reset(); 
                                                        includePanel.getForm().findField('idSalleStockage').reset(); 
                                                        includePanel.getForm().findField('nomSalle').reset();
                                                        includePanel.getForm().findField('codeSalle').reset();
                                                        includePanel.getForm().findField('descriptionSalle').reset();   
                                                         Ext.getCmp("TabSalleStockage").el.unmask(); 
                                                    },
                                                    failure:function(response, options) {  
                                                        var result = Ext.util.JSON.decode(response.responseText);
                                                        Ext.Msg.alert('Erreur', result.errors.reason); 
                                                        includePanel.getForm().findField('idAireStockage').reset();  
                                                        includePanel.getForm().findField('idSalleStockage').reset(); 
                                                        includePanel.getForm().findField('nomSalle').reset();
                                                        includePanel.getForm().findField('codeSalle').reset();
                                                        includePanel.getForm().findField('descriptionSalle').reset();    
                                                         Ext.getCmp("TabSalleStockage").el.unmask(); 
                                                    }
                                                });
                                            }else{
                                                 Ext.Msg.alert('Erreur', "Veuillez saisir un nom"); 
                                            }

                                      }
                                 },{
                                                xtype:'button',
                                              text:'Effacer',
                                              id: 'resetSalle',
                                              handler:function(){
                                                    Ext.getCmp("gridSalle").getSelectionModel().clearSelections();  
                                                    includePanel.getForm().findField('idSalleStockage').reset();    
                                                    includePanel.getForm().findField('idAireStockage').reset();                                               
                                                    includePanel.getForm().findField('nomSalle').reset();
                                                    includePanel.getForm().findField('codeSalle').reset();
                                                    includePanel.getForm().findField('descriptionSalle').reset();       
                                                    Ext.getCmp("TabAireStockage").disable();
                                              } 
                         
                                 }
                            ]}   
                ]}
                    ,{
                        xtype: 'editorgrid',
                        id:'gridSalle',
                        margins: '0 5 5 5',
                        region: 'south',
                        clicksToEdit: 1,
                        height:133,
                        enableColumnHide:false,
                        loadMask:{msg: 'Chargement...'},
                        sm: new Ext.grid.RowSelectionModel({
                            singleSelect: true,
                            listeners: {
                                rowselect: function(sm, row, rec) {
                                    var formPart = Ext.getCmp("formPart");
                                    formPart.el.mask('Chargement...', 'x-mask-loading');
                                    includePanel.getForm().findField('idSalleStockage').setValue(rec.data.idSalleStockage);
                                    includePanel.getForm().findField('nomSalle').setValue(rec.data.nom);
                                    includePanel.getForm().findField('codeSalle').setValue(rec.data.code);
                                    includePanel.getForm().findField('descriptionSalle').setValue(rec.data.description);
                                    dsAire.removeAll();
                                    dsAire.load({
                                        params:{idSalleStockage:rec.get('idSalleStockage')}
                                    })
                                    Ext.getCmp("TabAireStockage").enable();                                    
                                   formPart.el.unmask();
                                }
                            }
                        }),
                        border: true,
                        ds: dsSalleStockage,
                        cm: new Ext.grid.ColumnModel([
                            {id:'idSalleStockage',header: "idSalleStockage", width: 80, sortable: true, locked:false, dataIndex: 'idSalleStockage', hidden:true},
                            {id:'nom',header: "Nom", width: 200, sortable: true, dataIndex: 'nom'},
                            {header: "Code", width: 100, sortable: true, dataIndex: 'code'},
                            {header: "Description", width: 235, sortable: true, dataIndex: 'description'},
                            {header: "Supp", width: 35, sortable: false, xtype:'actioncolumn',
                            items: [{
                                        getClass: function(v, meta, rec) {          // Or return a class from a function
                                                this.items[0].tooltip = 'Supprimer cette ligne';
                                                return 'del-col';
                                        },
                                        handler: function(grid, rowIndex, colIndex) {
                                            var rec = dsSalleStockage.getAt(rowIndex);
                                            deleteligne(rec.get('idSalleStockage'),"SalleStockage");                                       
                                        }
                                    }]} 
                        ])
                        //,tbar:exportBar
                   } 
             ]
        }                           
//------------> FIN ONGLET 2
//------------> ONGLET 3

            ,      
            
            {
            id:'TabAireStockage',
            disabled:true,
            title:'Armoire/Local',
            layout:'border',
            displayFloatingMsg:true,
            loadMask:{msg: 'Chargement...'},
            autoScroll:true,


            items: [


{
            xtype:'panel',
            layout:'form',
            region: 'center',
            autoScroll:true,        
            margins: '0 5 5 5',
            baseCls:'x-panel-mc',
            cls:'x-panel-mc-with-tbar',
            labelWidth: 190,
            defaults: {border:false},
            defaultType: 'textfield',
            labelSeparator:'',
            bodyStyle: Ext.isIE ? 'padding:0 0 5px 15px;' : 'padding:10px 15px;',
            items:[
         
{
                                fieldLabel: 'idAireStockage',
                                    name: 'idAireStockage',
                                    hidden:true
                                },
                                {   
                                    fieldLabel: 'Nom *',
                                    name: 'nomAire',             
                                    width:200,
                                    maxLength:45,
                                    maxLengthText:'Longueur maxi : 45'
                                }
                                ,
                                {
                                    xtype:'combo',
                                    blankText:'Le champ est requis',
                                    fieldLabel: 'Type de local *',
                                    name: 'nomTypeAire',
                                    width:200,
                                    store: cbtypeaire,
                                    root:'data',
                                    hiddenName : 'idTypeAire',
                                    valueField:'idTypeAire',
                                    id:'idTypeAire',
                                    displayField:'nom',
                                    typeAhead: true,
                                    loadingText: 'Recherche...',
                                    mode: 'local',
                                    triggerAction: 'all',
                                    emptyText:'Selectionner un element...',
                                    selectOnFocus:true
                                }                                
                                ,{    
                                    fieldLabel: 'Code/Numero',
                                    name: 'codeAire',        
                                    width:100,
                                    maxLength:25,
                                    maxLengthText:'Longueur maxi : 25'                
                                }
                                ,{
                                    xtype:'checkbox',
                                    fieldLabel: 'Refrigéré',
                                    name: 'refrigere',
                                    inputValue:'true'                                   
                                },
                                {
                                    xtype: 'compositefield',
                                    fieldLabel: 'Commentaires',
                                    items: [{     
                                              xtype:'textfield',
                                              fieldLabel: 'Description',
                                              name: 'description',
                                              allowBlank: true,
                                              width:230
                                            },{
                                              xtype:'button',
                                              text:'Ajouter/Modifier',
                                              windowName:'searchProduit',
                                              id: 'searchProduit',
                                              handler:function(){
                                          
                                        var idAireStockage  = includePanel.getForm().findField('idAireStockage').getValue();                                          
                                        var idSalleStockage = includePanel.getForm().findField('idSalleStockage').getValue(); 
                                        var idTypeAire = Ext.getCmp("idTypeAire").value;
                                        var refrigere = includePanel.getForm().findField('refrigere').getValue();
                                        var nom = includePanel.getForm().findField('nomAire').getValue();
                                        var code = includePanel.getForm().findField('codeAire').getValue();
                                        var description = includePanel.getForm().findField('description').getValue();

                                        
                                        if (nom !="" && idTypeAire > 0){
                                            Ext.getCmp("TabAireStockage").el.mask('Ajout du local...', 'x-mask-loading');    
                                        
                                            Ext.Ajax.request({
                                                method:'POST',
                                                url:'FrontController',
                                                form: 'some-form',
                                                params: {controller:'AireStockage', action: 'saveItem', idAire:idAireStockage,idSalleStockage:idSalleStockage,
                                                idTypeAire:idTypeAire,refrigere:refrigere, nom:nom, code:code, description:description},
                                                success: function(response, options){
                                                        if (response.responseText) {						
                                                            if (response.responseText.substr(0,1) == '{') {
                                                                var ajaxResponse = Ext.util.JSON.decode(response.responseText);
                                                             }
                                                        }
                                                        if(ajaxResponse && ajaxResponse.success) {
                                                            Ext.msg.show(this.title ? this.title : 'Op&eacute;ration r&eacute;ussie:', 'Enregistrement effectuc&eacute; !');

                                                        }
                                                        dsAire.removeAll();
                                                        dsAire.load({
                                                            params:{idSalleStockage:idSalleStockage}
                                                        })                               
                                                        includePanel.getForm().findField('idAireStockage').reset();
                                                        Ext.getCmp("idTypeAire").reset();                                                        
                                                        includePanel.getForm().findField('refrigere').reset();
                                                        includePanel.getForm().findField('nomAire').reset();
                                                        includePanel.getForm().findField('codeAire').reset();
                                                        includePanel.getForm().findField('description').reset();
                                                         Ext.getCmp("TabAireStockage").el.unmask(); 
                                                    },
                                                    failure:function(response, options) {  
                                                        var result = Ext.util.JSON.decode(response.responseText);
                                                        Ext.Msg.alert('Erreur', result.errors.reason); 
                                                        includePanel.getForm().findField('idAireStockage').reset();
                                                        Ext.getCmp("idTypeAire").reset();                                                        
                                                        includePanel.getForm().findField('refrigere').reset();
                                                        includePanel.getForm().findField('nomAire').reset();
                                                        includePanel.getForm().findField('codeAire').reset();
                                                        includePanel.getForm().findField('description').reset();
                                                         Ext.getCmp("TabAireStockage").el.unmask(); 
                                                    }
                                                });
                                            }else{
                                                 Ext.Msg.alert('Erreur', "Veuillez saisir un nom et un type"); 
                                            }

                                      }
                                 },{
                                                xtype:'button',
                                              text:'Effacer',
                                              id: 'reset',
                                              handler:function(){
                                                    
                                                    includePanel.getForm().findField('idAireStockage').reset();
                                                    Ext.getCmp("idTypeAire").reset();    
                                                    Ext.getCmp("gridP2").getSelectionModel().clearSelections();                                                    
                                                    includePanel.getForm().findField('refrigere').reset();
                                                    includePanel.getForm().findField('nomAire').reset();
                                                    includePanel.getForm().findField('codeAire').reset();
                                                    includePanel.getForm().findField('description').reset();                                    
                                              } 
                         
                                 }
                            ]}                               
          ]}
            ,{
                xtype: 'editorgrid',
                id:'gridP2',
                margins: '0 5 5 5',
                region: 'south',
                clicksToEdit: 1,
                height:133,
                enableColumnHide:false,
                loadMask:{msg: 'Chargement...'},
                sm: new Ext.grid.RowSelectionModel({
                    singleSelect: true,
                    listeners: {
                        rowselect: function(sm, row, rec) {
                            var formPart = Ext.getCmp("formPart");
                            formPart.el.mask('Chargement...', 'x-mask-loading');
                            includePanel.getForm().findField('idAireStockage').setValue(rec.data.idAire);
                            Ext.getCmp("idTypeAire").setValue(rec.data.idTypeAire, true);
                            includePanel.getForm().findField('refrigere').setValue(rec.data.refrigere);
                            includePanel.getForm().findField('nomAire').setValue(rec.data.nom);
                            includePanel.getForm().findField('codeAire').setValue(rec.data.code);
                            includePanel.getForm().findField('description').setValue(rec.data.description);
                           formPart.el.unmask();
                        }
                    }
                }),
                border: true,
                ds: dsAire,
                cm: cmAire
                //,tbar:exportBar
           } 
            ]},
      
      
      


//------------> FIN ONGLET 3
        
    


]}    
                            
                            
                            
                            ,
                                {
                                xtype:'panel',
                                layout:'border',
                                region: 'center',
                                items:[{
                                    labelSeparator: '',
                                    region: 'south',
                                    fieldLabel:' ',
                                    xtype:'button',
                                    name:'getLatLong',
                                    height:35,
                                    text: 'Rechercher les coordonn&eacute;es g&eacute;ographiques',
                                    tooltip:'Si l\'adresse est localisée, déplaçez ou cliquez sur le marqueur sur le plan pour ajouter les coordonnées',
                                    iconCls: 'icon-add',
                                    handler : function() {
                                        var adresse1 = includePanel.getForm().getValues()['adresse1'];
                                        var adresse2 = includePanel.getForm().getValues()['adresse2'];
                                        var codepostal = includePanel.getForm().getValues()['codePostal'];
                                        var ville = includePanel.getForm().getValues()['ville']; 
                                        if (adresse2!=''){
                                            adresse2 = adresse2 + ',';
                                        }
                                       
                                        Ext.getCmp("myGmap").geoCodeLookup(adresse1+','+adresse2+codepostal+','+ville+', France', {title: '',draggable:true, center:true}, true)
                                    }
                                  },
                                  {
                                    xtype: 'gmappanel',
                                    
                                    id:'myGmap',
                                    region: 'center',   
                                    zoomLevel: 17,
                                    gmapType: google.maps.MapTypeId.ROADMAP,
                                    displayGeoErrors: true,
                                    minGeoAccuracy: 'GEOMETRIC_CENTER',
                                    mapConfOpts: ['enableScrollWheelZoom','enableDoubleClickZoom','enableDragging'],
                                    mapControls: ['GSmallMapControl','GMapTypeControl','NonExistantControl'],
                                    setCenter: {
                                        geoCodeAddr: '3 rue des tanneurs, tours, france',
                                        marker: {title: '',draggable:true, clear:true}
                                    }}
                
                                ]}
            
            ]}
            
            
            
    ]
    ,
        saveItem:function(){
                if (includePanel.getForm().isValid()){
                includePanel.getForm().submit({waitTitle:'Connexion au serveur', waitMsg:'Enregistrement en cours...',
                        success:function(f){
                           dsBatiment.reload();
                           includePanel.getForm().reset();                      
                            Ext.msg.show(this.title ? this.title : 'Op&eacute;ration r&eacute;ussie:', 'Enregistrement effectu&eacute; !');
                },
                        failure:function(f,action){
				var result = Ext.util.JSON.decode(action.response.responseText);
				Ext.Msg.alert('Erreur', result.errors.reason);   
                        }
                });
        }
        },
        resetToNull: function() {
           var form = this.getForm();
           if (form.isDirty()){
            Ext.MessageBox.show({
               buttons: Ext.Msg.OKCANCEL,
               title:'Etes-vous sur ?',
               msg:'Les données non enregistrées vont être effacées. Etes-vous sur ?',
               fn:function(btn) {
                   if(btn == 'ok') {
                       Ext.getCmp("BatimentForm").resetForm();
                       Ext.getCmp("gridBatiment").getSelectionModel().clearSelections();
                       Ext.getCmp("gridSalle").getStore().removeAll();
                       Ext.getCmp("gridP2").getStore().removeAll();
                       Ext.getCmp("TabAireStockage").disable();
                       Ext.getCmp("TabSalleStockage").disable();
                       Ext.getCmp("tabPart").setActiveTab("TabBatiment");                      
                   }else{
                       return;
                   }
            }});
            return false;
            }

    },
    resetForm:function(){
        var form = this.getForm();
        form.items.each(function(f){			
       if(f.getXType() == 'xcheckbox')
            f.originalValue = false;
        else
            f.originalValue = '';
        }, form);
        form.reset();  
    },   
    deleteItem: function(id, cont) {
        if (cont==null){
            cont=this.controller
        }
        if(id == null)
            var this_id = this.getForm().findField(this.gridP).getValue();
        else
            var this_id = id;
        if(this_id != '') {
            Ext.MessageBox.confirm('Etes-vous sur ?','Voulez-vous effacer cet enregistrement&nbsp;?',function(btn) {
                if(btn == 'yes') {
                        Ext.getCmp("gridBatiment").loadMask.show();   
                        var formPart = Ext.getCmp("tabPart");
                        formPart.el.mask('Chargement...', 'x-mask-loading');                     
                        var myFormParams = {
                            controller:cont,
                            action:'deleteItem',
                            id:this_id,
                            output:'json'
                        };
                        // adding extra params if necessary!
                        if (this.extraParams) {
                            for (key in this.extraParams) {
                                myFormParams[key] = this.extraParams[key]; 
                            }
                        }
                        Ext.Ajax.request({
                            params:myFormParams,
                            url:'FrontController',
                             success:function(response){
                                if (response.responseText) {						
                                    if (response.responseText.substr(0,1) == '{') {
			    	        var ajaxResponse = Ext.util.JSON.decode(response.responseText);
				     }
	                        }
                                if(ajaxResponse && ajaxResponse.success) {  
                                    if (cont=='SalleStockage'){
                                        dsSalleStockage.reload();
                                        includePanel.getForm().findField('idAireStockage').reset();                                               
                                        includePanel.getForm().findField('nomSalle').reset();
                                        includePanel.getForm().findField('codeSalle').reset();
                                        includePanel.getForm().findField('descriptionSalle').reset();
                                        Ext.getCmp("TabAireStockage").disable();        
                                        Ext.getCmp("gridBatiment").loadMask.hide();
                                        
                                        
                                    }else if (cont=='AireStockage'){
                                        dsAire.reload();
                                        includePanel.getForm().findField('idAireStockage').reset();
                                        Ext.getCmp("idTypeAire").reset();                                                        
                                        includePanel.getForm().findField('refrigere').reset();
                                        includePanel.getForm().findField('nomAire').reset();
                                        includePanel.getForm().findField('codeAire').reset();
                                        includePanel.getForm().findField('description').reset();                                           
                                        Ext.getCmp("gridBatiment").loadMask.hide();
                                    }else{
                                        includePanel.getForm().reset();                                        
                                        dsBatiment.reload();
                                    }
                                    Ext.msg.show(this.title ? this.title : 'Op&eacute;ration r&eacute;ussie:', 'Enregistrement effac&eacute; !');

                                }
                                formPart.el.unmask(); 
                            },
                            failure:function(response) {
				var result = Ext.util.JSON.decode(response.responseText);
				Ext.Msg.alert('Erreur', result.errors.reason); 
                                Ext.getCmp("gridP2").loadMask.hide();   
                                Ext.getCmp("gridSalle").loadMask.hide();                                 
                                Ext.getCmp("gridBatiment").loadMask.hide();
                                formPart.el.unmask(); 
                            }
                        }); 
                } else {
                  //do nothing
                }
            },this);
            return false;           
        } else {
            Ext.Msg.alert('Erreur','Vous devez s&eacute;lectionner un enregistrement');
        }
    }
    
    });
    

function reset(){

    includePanel.resetToNull();

}

function deleteligne(id, contr){
    includePanel.deleteItem(id, contr);
}

function save(){
    includePanel.saveItem();
}


