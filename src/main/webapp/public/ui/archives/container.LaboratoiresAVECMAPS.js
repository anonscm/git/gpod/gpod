
Ext.QuickTips.init();

    Ext.form.Field.prototype.msgTarget = 'side';

    var bd = Ext.getBody();
    
// ----------------------------------------------------------
// ----------------------------------------------------------
// TABLEAU - GRID 
// acces au données pour le tableau
    var proxy = new Ext.data.HttpProxy({
        url: 'FrontController',
        method: 'POST'
    });
    
    var rec = Ext.data.Record.create([
            {name: 'idStructure'},
            {name: 'code'},
            {name: 'nom'},
            {name: 'adresse1'},
            {name: 'adresse2'},
            {name: 'codePostal'},
            {name: 'ville'},
            {name: 'telephone'},
            {name: 'fax'},
            {name: 'responsable'},
            {name: 'longitude'},
            {name: 'latitude'}
    ]);

    var reader = new Ext.data.JsonReader({
 	   root : "data",
 	   id : "idStructure"
    }, rec);


    var ds = new Ext.data.JsonStore({
        proxy: proxy,
        fields: ['idStructure', 'code', 'nom', 'adresse1', 'adresse2', 'codePostal', 'ville', 'telephone','fax','responsable','longitude','latitude'],
        root:'data',
        totalProperty: 'totalCount',
        reader: reader,
        nocache: true,
        autoLoad: false,
        baseParams:{action:'getListItems', controller:'Laboratoire'}
    });
    ds.load({params:{start:0, limit:9}});


    var colModel = new Ext.grid.ColumnModel([
        {id:'idStructure',header: "idStructure", width: 80, sortable: true, locked:false, dataIndex: 'idStructure', hidden:true},
        {id:'code',header: "Code", width: 100, sortable: true, dataIndex: 'code'},
        {id:'nom',header: "Nom", width: 100, sortable: true, dataIndex: 'nom'},
        {header: "Adresse", width: 100, sortable: true, dataIndex: 'adresse1'},
        {header: "adresse2", width: 200, sortable: true, dataIndex: 'adresse2', hidden:true},
        {header: "codepostal", width: 100, sortable: true, dataIndex: 'codePostal', hidden:true},
        {header: "ville", width: 200, sortable: true, dataIndex: 'ville'},
        {header: "telephone", width: 200, sortable: true, dataIndex: 'telephone', hidden:true},
        {header: "fax", width: 200, sortable: true, dataIndex: 'fax', hidden:true},
        {header: "Responsable", width: 200, sortable: true, dataIndex: 'responsable'},
        {header: "longitude", width: 200, sortable: true, dataIndex: 'longitude', hidden:true},
        {header: "latitude", width: 200, sortable: true, dataIndex: 'latitude', hidden:true}
    ]);

    var PagingBar = new Ext.PagingToolbar({
        pageSize: 9,
        store: ds,
        displayInfo: true,
        action:'getListItems', 
        controller:'Laboratoire',
        displayMsg: 'Unité de recherche {0} - {1} of {2}',
        emptyMsg: 'Aucune Unité de recherche'
    });   
// FIN TABLEAU - GRID
// ----------------------------------------------------------   
       
    

// ---------------------------------------------------------- 
// ---------------------------------------------------------- 
//  Toolbar Tableau
     var gridTbar = new Ext.Toolbar({
     cls: 'x-panel-header',
     height: 25,
     items: [  {text: 'Ajouter une unité',
                iconCls: 'icon-add',
                handler : reset
               },
               {text: 'Supprimer une unité',
                iconCls: 'icon-delete',
                handler : deleteligne
               },            
                 '->', 
               {text: 'Enregistrer',
                iconCls: 'icon-save',
                handler : save
                }]

    });
// ---------------------------------------------------------- 
       
    
    
    var includePanel = new Ext.FormPanel({
        id: 'LaboratoiresForm',
         controller:'Laboratoire',
        url: 'FrontController',
        frame: true,    
        labelAlign: 'left',
        waitMsgTarget: true,
        width: 1021,
        layout: 'border',    // Specifies that the items will now be arranged in columns
        height:645,
        baseParams:{action:'saveItem', controller:'Laboratoire'},
        items: [{
            xtype: 'grid',
            id:'gridP',
            split:true,        	
            margins: '5 0 5 5',
            region: 'north',
            tbar:gridTbar,
            height:291,
            minHeight:191,
            bbar: PagingBar , 
            enableColumnHide:false,
            loadMask:{msg: 'Chargement...'},
            ds: ds,
            cm: colModel,
            sm: new Ext.grid.RowSelectionModel({
                singleSelect: true,
                listeners: {
                    rowselect: function(sm, row, rec) {
                        var formPart = Ext.getCmp("formPart");
                        var this_id = rec.data.idUtilisateur;
                        formPart.el.mask('Chargement...', 'x-mask-loading');
                        Ext.getCmp("LaboratoiresForm").getForm().loadRecord(rec);  
                        
                        
                        //var point = new GPoint(1,1);
                        var marker = [{
                            lat: rec.data.latitude,
                            lng: rec.data.longitude,
                            marker: {title: rec.data.nom, clear:true},
                            setCenter:true
                        }];
                        
                        includePanel.getForm().findField('longlat').setValue(rec.data.longitude + '/' +rec.data.latitude); 
                        
                        Ext.getCmp("myGmap").addMarkers(marker);  
                        
                        formPart.el.unmask();
                    }
                }
            }),
            autoExpandColumn: 'nom',
             title:'Unités de recherche',
            border: true,
            
            plugins:[new Ext.ux.grid.Search({
                 mode:'remote'
                ,iconCls:'icon-zoom'
                ,autoFocus:true
                ,minLength:2
                ,position:'bottom'
                ,align:'left'
                ,width:200
                ,shortcutKey:113
                ,disableIndexes:['idStructure', 'telephone','fax','longitude','latitude']
            })]
        },
        
          {
            xtype:'panel',
            layout:'border',
            region: 'center',
            autoScroll:true,
            id:'formPart',
            defaults: {frame:true},
            border: true,
            items:[{
                                
                                displayFloatingMsg:true,
                                split:true,        	
                                margins: '0 0 0 0',        	
                                columnWidth: 0.9,
                                region: 'west',    
                                width:600,
                                xtype: 'fieldset',
                                loadMask:{msg: 'Chargement...'},
                                labelWidth: 190,
                                //height:342,
                                //title:'Laboratoires',
                                defaults: {width: 140, border:false},    // Default config options for child items
                                defaultType: 'textfield',
                                //autoHeight: true,
                                bodyStyle: Ext.isIE ? 'padding:0 0 5px 15px;' : 'padding:10px 15px;',
                                
                                items: [{
                                    fieldLabel: 'idStructure',
                                    name: 'idStructure',
                                    hidden:true
                                },{   
                                    fieldLabel: 'Nom',
                                    name: 'nom',
                                    allowBlank: false,
                                    blankText:'Le champ est requis',                
                                    width:300,
                                    maxLength:45,
                                    maxLengthText:'Longueur maxi : 45'
                                },{   
                                    id:'code',  
                                    fieldLabel: 'Code',
                                    name: 'code',        
                                    width:100,
                                    maxLength:25,
                                    maxLengthText:'Longueur maxi : 25'                
                                }
                                ,{   
                                    id:'adresse1',  
                                    fieldLabel: 'Adresse',
                                    name: 'adresse1',        
                                    width:300,
                                    maxLength:255,
                                    maxLengthText:'Longueur maxi : 255'                
                                },{   
                                    fieldLabel: 'Complement',
                                    name: 'adresse2',
                                    width:300,
                                    maxLength:255,
                                    maxLengthText:'Longueur maxi : 255'                        
                                },{            	
                                    xtype:'numberfield',
                                    fieldLabel: 'Code Postal',
                                    name: 'codePostal',
                                    allowDecimals:false,
                                    width:50,
                                    maxLength:6,
                                    maxLengthText:'Longueur maxi : 6'
                                },{
                                    fieldLabel: 'Ville',
                                    name: 'ville',
                                    width:300,
                                    maxLength:255,
                                    maxLengthText:'Longueur maxi : 255'                        
                                },{
                                    fieldLabel: 'Telephone',
                                    name: 'telephone',
                                    width:100,
                                    maxLength:12,
                                    maxLengthText:'Longueur maxi : 12'                        
                                },{
                                    fieldLabel: 'Fax',
                                    name: 'fax',
                                    width:100,
                                    maxLength:12,
                                    maxLengthText:'Longueur maxi : 12'                  
                                },{
                                    fieldLabel: 'Responsable',
                                    name: 'responsable',
                                    width:300,
                                    maxLength:255,
                                    maxLengthText:'Longueur maxi : 255'                  
                                },{
                                    fieldLabel: 'Long/Lat',
                                    //xtype:'numberfield',
                                    name: 'longlat',
                                    width:300,
                                    disabled:true
                                }
                                ,{
                                    xtype:'numberfield',
                                    fieldLabel: 'Longitude',
                                    name: 'longitude',
                                    decimalPrecision:15,
                                    //decimalSeparator:',',
                                    width:100,
                                    hidden:true
                                },{
                                    xtype:'numberfield',
                                    fieldLabel: 'Latitude',
                                    name: 'latitude',
                                    decimalPrecision:15,
                                    //decimalSeparator:',',
                                    width:100,
                                    hidden:true
                                }
                                ]},
                                {
                                xtype:'panel',
                                layout:'border',
                                region: 'center',
                                autoScroll:true,
                                defaults: {frame:true},
                                items:[{
                                    labelSeparator: '',
                                    region: 'south',
                                    fieldLabel:' ',
                                    xtype:'button',
                                    name:'getLatLong',
                                    height:35,
                                    text: 'Rechercher les coordonn&eacute;es g&eacute;ographiques',
                                    tooltip:'Si l\'adresse est localisée, déplaçez ou cliquez sur le marqueur sur le plan pour ajouter les coordonnées',
                                    iconCls: 'icon-add',
                                    handler : function() {
                                        var adresse1 = includePanel.getForm().getValues()['adresse1'];
                                        var adresse2 = includePanel.getForm().getValues()['adresse2'];
                                        var codepostal = includePanel.getForm().getValues()['codePostal'];
                                        var ville = includePanel.getForm().getValues()['ville']; 
                                        if (adresse2!=''){
                                            adresse2 = adresse2 + ',';
                                        }

                                        Ext.getCmp("myGmap").geoCodeLookup(adresse1+','+adresse2+codepostal+','+ville+', France', {title: '',draggable:true, center:true}, true)
                                    }
                                  },
                                  {
                                    xtype: 'gmappanel',
                                    id:'myGmap',
                                    gmapType: google.maps.MapTypeId.ROADMAP,
                                    displayGeoErrors: true,
                                    minGeoAccuracy: 'GEOMETRIC_CENTER',                                    
                                    region: 'center',   
                                    zoomLevel: 17,
                                    gmapType: 'map',
                                    mapConfOpts: ['enableScrollWheelZoom','enableDoubleClickZoom','enableDragging'],
                                    mapControls: ['GSmallMapControl','GMapTypeControl','NonExistantControl'],
                                    setCenter: {
                                        geoCodeAddr: '3 rue des tanneurs, tours, france',
                                        marker: {title: '',draggable:true, clear:true}
                                    }}
                
                                ]}
            
            ]}
            
            
            
    ]
    ,
        saveItem:function(){
                if (includePanel.getForm().isValid()){
                includePanel.getForm().submit({waitTitle:'Connexion au serveur', waitMsg:'Enregistrement en cours...',
                        success:function(f,action){
                           ds.reload();
                           includePanel.getForm().reset();                      
                            Ext.msg.show(this.title ? this.title : 'Op&eacute;ration r&eacute;ussie:', 'Enregistrement effectu&eacute; !');
                },
                        failure:function(f,action){
				var result = Ext.util.JSON.decode(action.response.responseText);
				Ext.Msg.alert('Erreur', result.errors.reason);   
                        }
                });
        }
        },
        resetToNull: function() {
           var form = this.getForm();
           if (form.isDirty()){
            Ext.MessageBox.confirm('Etes-vous sur ?','Voulez vous enregistrer les modifications ?',function(btn) {
                   if(btn == 'no') {
                       this.resetForm();
                   }else{
                       this.saveItem();
                   }
            },this);
            return false;
            }

    },
    resetForm:function(){
        var form = this.getForm();
        form.items.each(function(f){			
       if(f.getXType() == 'xcheckbox')
            f.originalValue = false;
        else
            f.originalValue = '';
        }, form);
        form.reset();  
    },   
    deleteItem: function(id) {
        if(id == null)
            var this_id = this.getForm().findField(this.gridP).getValue();
        else
            var this_id = id;
        if(this_id != '') {
            Ext.MessageBox.confirm('Etes-vous sur ?','Voulez-vous effacer cet enregistrement&nbsp;?',function(btn) {
                if(btn == 'yes') {
                        Ext.getCmp("gridP").loadMask.show();   
                        var formPart = Ext.getCmp("formPart");
                        formPart.el.mask('Chargement...', 'x-mask-loading');                     
                        var myFormParams = {
                            controller:this.controller,
                            action:'deleteItem',
                            id:this_id,
                            output:'json'
                        };
                        // adding extra params if necessary!
                        if (this.extraParams) {
                            for (key in this.extraParams) {
                                myFormParams[key] = this.extraParams[key]; 
                            }
                        }
                        Ext.Ajax.request({
                            params:myFormParams,
                            url:'FrontController',
                             success:function(response, options){
                                if (response.responseText) {						
                                    if (response.responseText.substr(0,1) == '{') {
			    	        var ajaxResponse = Ext.util.JSON.decode(response.responseText);
				     }
	                        }
                                if(ajaxResponse && ajaxResponse.success) {
                                    includePanel.getForm().reset();
                                    ds.reload();
                                    Ext.msg.show(this.title ? this.title : 'Op&eacute;ration r&eacute;ussie:', 'Enregistrement effac&eacute; !');

                                }
                                formPart.el.unmask(); 
                            },
                            failure:function(response, options) {  
				var result = Ext.util.JSON.decode(response.responseText);
				Ext.Msg.alert('Erreur', result.errors.reason); 
                                ds.reload();
                                formPart.el.unmask(); 
                            }
                        }); 
                } else {
                  //do nothing
                }
            },this);
            return false;           
        } else {
            Ext.Msg.alert('Erreur','Vous devez s&eacute;lectionner un enregistrement');
        }
    }
    
    });
    

function reset(){

    includePanel.resetToNull();

}

function deleteligne(){
    includePanel.deleteItem();
}

function save(){
    includePanel.saveItem();
}


