	//Ext.namespace('Ext.produitDangereux');

    var tb = new Ext.Toolbar();

    
    function onButtonClick(btn){
        var controlleurName = btn.id.substr(3);
        var uri = "FrontController?controller="+controlleurName+"&action=getHtml";
        if (btn.params != null ){
            if (btn.params.type != null){
                uri = uri + "&type="+btn.params.type;
            }
        }
        window.location = uri;
        
	
    }
    
       
    
    var menuProd = new Ext.menu.Menu({
        id: 'produit',
        items: [
            {
            id:'btnSubstance',
            text: 'Substances',
            iconCls:'icon-substance',
            handler: onButtonClick
            }
            ,            
            {
            id:'btnProduit',
            text: 'Catalogue fiche produit',
            iconCls:'icon-produit',
            handler: onButtonClick
            }          ,
            {
                id:'btnImport',
                text: 'Importer à partir d\'un fichier XLS',
                iconCls:'icon-produit',
                handler: function(){
                    //windowImport.show();
                    window.open('importExcel.html','imp','toolbar=0,location=0,directories=0,status=0,scrollbars=1,resizable=1,copyhistory=0,menuBar=0,width=1050,height=500 left=10 top=10');
                }
            }
        ]
    });
 
    var menuCommande = new Ext.menu.Menu({
        id: 'commande',
        items: [
            {
                id:'btnCommande',
                text: 'Commande',
                handler: onButtonClick
            },
            {
                id:'btnFournisseur',
                text: 'Fournisseurs',
                handler: onButtonClick
            }
        ]
    });


    var menuDepartement = new Ext.menu.Menu({
        id: 'departements',
        items: [
            {
                id:'btnDepartement',
                text: 'Département d\'enseignement',
                handler: onButtonClick
            }
        ]
    });

   
    var menuLabo = new Ext.menu.Menu({
        id: 'laboratoires',
        items: [
            {
                id:'btnLaboratoire',
                text: 'Unités de recherche',
                handler: onButtonClick
            },
             {
                id:'btnEquipe',
                text: 'Equipes',
                handler: onButtonClick
            }
        ]
    });
    
   
 
    var menuAdmin = new Ext.menu.Menu({
        id: 'admin',
        items: [
            {
            id:'btnUtilisateur',
            text: 'Utilisateurs',
            handler: onButtonClick
            },
             {
            id:'btnBatiment',
            text: 'Locaux de stockage',
            handler: onButtonClick
            },            
            {
            text: 'Laboratoires',
            menu:menuLabo
            },
            {
            text: 'Départements',
            menu:menuDepartement
            }]
    });



    var menuEdition = new Ext.menu.Menu({
        id: 'edition',
        items: [
            {
            text: 'Nomenclature',
        	menu:menuAdmin
            },
            {
            text: 'Produits'
            }]
    });
    
    var menustockdetail = new Ext.menu.Menu({
        id: 'stockDetail',
        items: [
            {
                id:'btnStockListe',
                text: 'Visualisation en liste',
                handler: onButtonClick
            },
            {
                id:'btnStockMap',
                text: 'Visualisation géographique',
                handler: onButtonClick
            }]
    });

    var menuStock = new Ext.menu.Menu({
        id: 'stock',
        items: [
            {                
                text: 'Visualisation du Stock',
                menu:menustockdetail
            },
            {
                id:'btnLivraisonSimple',
                text: 'Entrée directe dans le Stock',
                handler: onButtonClick
            },
            {
                id:'btnChimieDon',
                text: 'Liste des produits à donner',
                handler: onButtonClick
            }]
    });

    var menuHelp = new Ext.menu.Menu({
        id: 'menuHelp',     
        items: [
            {                
                text: 'Aide',
                handler: function(){
                    window.open('http://gpodwiki.univ-tours.fr');
                }
            },
            {
                id:'btnPreferences',
                text: 'Mes préférences',
                handler: function(){
                    windowPreferences.show();
                }
            },
            {
                id:'btnAPropos',
                text: 'A propos de Gpod',
                handler: function(){
                    windowApropos.show();
                }
            },
            {
                id:'btnFeedBack',
                text: 'Remonter un bug / Envoyer un commentaire',
                handler: function(){
                    windowFeedBack.show();
                }
            }]
    });


    
    
    tb.add({
    	text: 'Produits',  
    	menu:menuProd
    	
    }
    ,{
    	text: 'Commandes',
        menu:menuCommande
    }
    ,{
    	text: 'Stock',
    	menu:menuStock
    }
    ,'-'
    ,{
    	text: 'Administration',
    	menu:menuAdmin
    },{
    	text: '?',
    	menu:menuHelp
    }

);
    
    
