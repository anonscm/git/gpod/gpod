	//Ext.namespace('Ext.produitDangereux');

    var tb = new Ext.Toolbar();

    
    function onButtonClick(btn){
        var controlleurName = btn.id.substr(3);
        var uri = "FrontController?controller="+controlleurName+"&action=getHtml";
        if (btn.params != null ){
            if (btn.params.type != null){
                uri = uri + "&type="+btn.params.type;
            }
        }
        window.location = uri;
        
	
    }

    var menuHelp = new Ext.menu.Menu({
        id: 'menuHelp',     
        items: [
            {                
                text: 'Aide',
                handler: function(){
                    window.open('http://gpodwiki.univ-tours.fr');
                }
            },
            {
                id:'btnPreferences',
                text: 'Mes préférences',
                handler: function(){
                    windowPreferences.show();
                }
            },
            {
                id:'btnAPropos',
                text: 'A propos de Gpod',
                handler: function(){
                    windowApropos.show();
                }
            },
            {
                id:'btnFeedBack',
                text: 'Remonter un bug / Envoyer un commentaire',
                handler: function(){
                    windowFeedBack.show();
                }
            }]
    });

    var menuProd = new Ext.menu.Menu({
        id: 'produit',
        items: [
            {
            id:'btnSubstance',
            text: 'Substances',
            iconCls:'icon-substance',
            handler: onButtonClick
            }
            ,            
            {
            id:'btnProduit',
            text: 'Catalogue fiche produit',
            iconCls:'icon-produit',
            handler: onButtonClick
            }
        ]
    });
 
  
    var menustockdetail = new Ext.menu.Menu({
        id: 'stockDetail',
        items: [
            {
                id:'btnStockListe',
                text: 'Visualisation en liste',
                handler: onButtonClick
            },
            {
                id:'btnStockMap',
                text: 'Visualisation géographique',
                handler: onButtonClick
            }]
    });

    var menuStock = new Ext.menu.Menu({
        id: 'stock',
        items: [
            {                
                text: 'Visualisation du Stock',
                menu:menustockdetail
            },
            {
                id:'btnChimieDon',
                text: 'Liste des produits à donner',
                handler: onButtonClick
            }
        ]
    });




    
    
    tb.add({
    	text: 'Produits',  
    	menu:menuProd
    	
    }
    ,{
    	text: 'Stock',
    	menu:menuStock
    },{
    	text: '?',
    	menu:menuHelp
    }


);
    
    
