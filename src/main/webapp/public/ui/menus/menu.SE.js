	//Ext.namespace('Ext.produitDangereux');

    var tb = new Ext.Toolbar();

    
    function onButtonClick(btn){
        var controlleurName = btn.id.substr(3);
        var uri = "FrontController?controller="+controlleurName+"&action=getHtml";
        if (btn.params != null ){
            if (btn.params.type != null){
                uri = uri + "&type="+btn.params.type;
            }
        }
        window.location = uri;
        
	
    }
    
    var menuHelp = new Ext.menu.Menu({
        id: 'menuHelp',     
        items: [
            {                
                text: 'Aide',
                handler: function(){
                    window.open('http://gpodwiki.univ-tours.fr');
                }
            },
            {
                id:'btnPreferences',
                text: 'Mes préférences',
                handler: function(){
                    windowPreferences.show();
                }
            },
            {
                id:'btnAPropos',
                text: 'A propos de Gpod',
                handler: function(){
                    windowApropos.show();
                }
            },
            {
                id:'btnFeedBack',
                text: 'Remonter un bug / Envoyer un commentaire',
                handler: function(){
                    windowFeedBack.show();
                }
            }]
    });    
    
    var menuCommande = new Ext.menu.Menu({
        id: 'commande',
        items: [
            {
                id:'btnCommande',
                text: 'Commande',
                handler: onButtonClick
            },'-',
            {
                id:'btnFournisseur',
                text: 'Fournisseurs',
                handler: onButtonClick
            }
        ]
    });

    tb.add({
    	text: 'Commandes',
        menu:menuCommande
    },{
    	text: '?',
    	menu:menuHelp
    }

);
    
    
