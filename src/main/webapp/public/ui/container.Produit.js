/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2012/01/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/

var myTimer;
var dataArray;

function IsNumeric(input)
{
   return (input - 0) == input && input.length > 0;
}

Ext.QuickTips.init();

    Ext.form.Field.prototype.msgTarget = 'side';

    var bd = Ext.getBody();
        
 // ---------------------------------------
 // GESTION DES DROITS
   Ext.Ajax.request({
        params:{
                controller:'Produit',
                action:'getRight'
            },
         url:'FrontController',
         success:function(response, options){
 
         },
         failure:function(response, options) {  
                    Ext.getCmp("btn-enreg").hide();
                    Ext.getCmp("btn-new").hide();
                    Ext.getCmp("btn-deltag").hide();
                    Ext.getCmp("btn-addtag").hide();
                    buildTopToolbar.hide();
         }
    }); 
 // --------------------------------------- 
 
// ----------------------------------------------------------
// TABLEAU - GRID 
// acces au données pour le tableau
    var proxy = new Ext.data.HttpProxy({
        url: 'FrontController',
        method: 'POST'
    });
    
    var rec = Ext.data.Record.create([
            {name: 'idProduit'},
            {name: 'substance'},
            {name: 'nom'},
            {name: 'description'},            
            {name: 'nomFournisseur'},
            {name: 'write'}

    ]);

    var reader = new Ext.data.JsonReader({
 	   root : "data",
 	   id : "substance"
    }, rec);


    var ds = new Ext.data.JsonStore({
        proxy: proxy,
        fields: ['idProduit','substance', 'nom', 'description', 'nomFournisseur','write'],
        root:'data',
        totalProperty: 'totalCount',
        reader: reader,
        nocache: true,
        autoLoad: false,
        baseParams:{action:'getListItems', controller:'Produit'}
        ,listeners:{
            load:function(){
                if(typeof(myMask) !== 'undefined')
                    myMask.hide();             
            }
        }
    });
    ds.load({params:{start:0, limit:10}});


    var colModel = new Ext.grid.ColumnModel([
        {id:'idProduit',header: "idProduit", width: 80, sortable: true, locked:false, dataIndex: 'idProduit', hidden:true},
        {id:'substance',header: "substance", width: 80, sortable: true, locked:false, dataIndex: 'substance'},
        {id:'nom',header: "Nom", width: 100, sortable: true, dataIndex: 'nom'},
        {header: "description", width: 100, sortable: true, dataIndex: 'description'},
        {header: "Fournisseur", width: 200, sortable: true, dataIndex: 'nomFournisseur'},
        {header: "Supp", width: 40, sortable: false, xtype:'actioncolumn',
        items: [{
                    getClass: function(v, meta, rec) {          // Or return a class from a function
                        if (rec.get('write') == 'true'){
                            this.items[0].tooltip = 'Supprimer cette ligne';
                            return 'del-col';
                        }
                    },
                    handler: function(grid, rowIndex, colIndex) {
                        var rec = ds.getAt(rowIndex);
                        deleteligne(rec.get('idProduit'));
                    }
                }]}
    ]);

    var PagingBar = new Ext.PagingToolbar({
        pageSize: 10,
        store: ds,
        displayInfo: true,
        action:'getListItems', 
        controller:'Produit',
        displayMsg: 'Produits {0} - {1} of {2}',
        emptyMsg: 'Aucun Produit'
    });   
    
    PagingBar.insertButton(11, excelButton);
// FIN TABLEAU - GRID
// ----------------------------------------------------------   
    

// ---------------------------------------------------------- 
// ---------------------------------------------------------- 
//  Toolbar Tableau
     var formTbar = new Ext.Toolbar({
     height: 27,
     items: [  {text: 'Nouvelle fiche produit',
             id:'btn-new',
                iconCls: 'icon-add',
                handler : reset
               }
//               // TODO : recherche détaillé               
//               ,{text: 'Recherche détaillée',
//                iconCls: 'icon-zoom',
//                windowName:'searchProduit',
//                          handler:function(){
//
//                              var mySearchWin = eval(this.windowName);
//                              mySearchWin.show();
//                          } 
//                 }
                 ,'->',{
                id:'editXls',
                iconCls:'icon-excel',
                value: '',
                handler:function(){
                    var value = includePanel.getForm().findField('idProduit').getValue();
                    if (value>0){
                        printAction("Produit","getXLSList", value);
                    }else{
                        Ext.Msg.alert('Export impossible','Veuillez selectionner un item à éditer');
                        return;
                    }
                }            
            },{
                id:'edit',
                iconCls:'icon-print',
                value: '',
                handler:function(){
                    var value = includePanel.getForm().findField('idProduit').getValue();
                    if (value>0){
                        printAction("Produit","getPDF", value);
                    }else{
                        Ext.Msg.alert('Export impossible','Veuillez selectionner un item à éditer');
                        return;
                    }
                }            
            }
               ]

    });
    
     var formBbar = new Ext.Toolbar({
     height: 27,
     id:'bottom-bar',
     cls:'x-bbar',
     items: [                   '->', 
               {id:'btn-enreg',
                text: 'Enregistrer',
                iconCls: 'icon-save',
                handler : save
                }]

    });      
// ---------------------------------------------------------- 
    

// ---------------------------------------------------------- 
// STORES

    var cbfournisseur = new Ext.data.JsonStore({
        proxy: proxy,
        fields: ['idFournisseur', 'nom'],
        root:'data',
        totalProperty: 'totalCount',
        nocache: true,
        autoLoad: true,
        baseParams:{action:'getListItems', controller:'Fournisseur'}
    });
    
    var cbsubstance = new Ext.data.JsonStore({
        proxy: proxy,
        fields: ['substance', 'nom', 'pictogrammes'],
        root:'data',
        totalProperty: 'totalCount',
        nocache: true,
        autoLoad: true,
        baseParams:{action:'getListItems', controller:'Substance'}
    });    
    
    var cbunitemesure = new Ext.data.JsonStore({
        proxy: proxy,
        fields: ['idUniteMesure', 'libelleLong', 'abreviation'],
        root:'data',
        totalProperty: 'totalCount',
        nocache: true,
        autoLoad: true,
        baseParams:{action:'getListItems', controller:'UniteMesure'}
    });   
    
    
     var cbcontenant = new Ext.data.JsonStore({
        proxy: proxy,
        fields: ['idContenant', 'libelle'],
        root:'data',
        totalProperty: 'totalCount',
        nocache: true,
        autoLoad: true,
        baseParams:{action:'getListItems', controller:'Contenant'}
    });     

     var cbetatphysique = new Ext.data.JsonStore({
        proxy: proxy,
        fields: ['idEtatPhysique', 'libelle'],
        root:'data',
        totalProperty: 'totalCount',
        nocache: true,
        autoLoad: true,
        baseParams:{action:'getListItems', controller:'EtatPhysique'}
    });     
   
    // select mention
    var multiMentionDispo = new Ext.data.JsonStore({
        proxy: proxy,
        fields: ['idMention', 'code', 'libelle'],
        root:'data',
        totalProperty: 'totalCount',
        nocache: true,
        autoLoad: true,
        baseParams:{action:'getListItems', controller:'MentionDanger', idProduit:0}
    });
    
    
    
    //Select mention associé
    var multiMentionAssoc = new Ext.data.JsonStore({
        proxy: proxy,
        fields: ['idMention', 'code', 'libelle'],
        root:'data',
        totalProperty: 'totalCount',
        nocache: true,
        autoLoad: false,
        baseParams:{action:'getListMention', controller:'Produit'}
    });  
    
    
        // select tag dispo
    var multiTagDispo = new Ext.data.JsonStore({
        proxy: proxy,
        fields: ['idtag', 'libelle'],
        root:'data',
        totalProperty: 'totalCount',
        nocache: true,
        autoLoad: true,
        baseParams:{action:'getListItems', controller:'Tag', idProduit:0}
    });
    
        // select tag dispo
    var multiTagAssoc = new Ext.data.JsonStore({
        proxy: proxy,
        fields: ['idtag', 'libelle'],
        root:'data',
        totalProperty: 'totalCount',
        nocache: true,
        autoLoad: false,
        baseParams:{action:'getListTag', controller:'Produit', idProduit:0}
    });    




// FIN STORES
// ---------------------------------------------------------- 
    
// --------------------------------------------------
// TABLEAU CONDITIONNEMENT


    var recliv = Ext.data.Record.create([
            {name: 'ligneNum'},
            {name: 'idProduitConditionnement'},
            {name: 'reference'},
            {name: 'contenance'},
            {name: 'nomProduit'},
            {name: 'idUniteMesure'},
            {name: 'idContenant'}
    ]);

    var readerliv = new Ext.data.JsonReader({
 	   root : "data",
 	   id : "idProduitConditionnement"
    }, recliv);

    var writerliv = new Ext.data.JsonWriter({
           encode: true,
           writeAllFields: false
       });

    var dsliv = new Ext.data.JsonStore({
        proxy: proxy,
        fields: ['ligneNum','idProduitConditionnement','reference','contenance', 'idUniteMesure', 'idContenant'],
        root:'data',
        autoSave:false,
        totalProperty: 'totalCount',
        reader: reader,
        writer:writerliv,
        nocache: true,
        autoLoad: false,
        baseParams:{action:'getListConditionnement', controller:'Produit'}
    });


     var buildTopToolbar = new Ext.Toolbar({
     cls: 'x-panel-header',
     height: 25,
     items: [{ text: 'Ajouter un condionnement',
            iconCls: 'icon-add',
            handler: function(){
               var i = dsliv.getCount()+0;
               var u = new  recliv({
                       ligneNum:i+1,
                       idProduitConditionnement:'0',
                       reference: '',
                       contenance:'',
                       idUniteMesure:'',
                       idContenant:''
                });
                 Ext.getCmp("gridP2").stopEditing();
                 dsliv.add(u);
                 Ext.getCmp("gridP2").startEditing(0, 1)
            },
            scope: this
        }, '-', {
            text: 'Supprimer',
            iconCls: 'icon-delete',
            handler:  function(btn, ev) {

                 Ext.MessageBox.confirm('Etes-vous sur ?','Vous êtes sur le point de supprimer un conditionnement, Etes vous sur ?',function(btn) {
                       if(btn == 'yes') {
                            var index = Ext.getCmp("gridP2").getSelectionModel().getSelectedCell();
                            if (!index) {
                                return false;
                            }
                            var rec = dsliv.getAt(index[0]);
                            dsliv.remove(rec);
                       }
                },this);
            },
            scope: this
        }, '-']

    });

   var comboRenderer = function(combo){
        return function(value){
            combo.store.clearFilter();
            var record = combo.findRecord(combo.valueField || combo.displayField, value);
            return record ? record.get(combo.displayField) : combo.valueNotFoundText;
        }
    }


    var comboContenant = new Ext.form.ComboBox({
                typeAhead: true,
                name:'nom',
                store: cbcontenant,
                root:'data',
                triggerAction: 'all',
                listClass: 'x-combo-list-small',
                allowBlank: false,
                hiddenName : 'idContenant',
                valueField:'idContenant',
                displayField:'libelle',
                emptyText:'Selectionner un element...',
                mode: 'local'
    });

    var comboUniteMesure = new Ext.form.ComboBox({
                typeAhead: true,
                name:'nom',
                store: cbunitemesure,
                root:'data',
                triggerAction: 'all',
                listClass: 'x-combo-list-small',
                allowBlank: false,
                emptyText:'Selectionner un element...',
                hiddenName : 'idUniteMesure',
                valueField:'idUniteMesure',
                displayField:'libelleLong',
                mode: 'local',
                tpl:new Ext.XTemplate(
                     '<tpl for="."><div class="x-combo-list-item">{libelleLong} ({abreviation})</div></tpl>'
                 )
    });

    var cmliv = new Ext.grid.ColumnModel({
        // specify any defaults for each column
        defaults: {
            sortable: true // columns are not sortable by default
        },
        columns: [{
            id: 'ligneNum',
            header: 'Ligne',
            dataIndex: 'ligneNum',
            width: 40
        },{
            id: 'idProduitConditionnement',
            header: 'Id',
            dataIndex: 'idProduitConditionnement',
            width: 40,
            hidden:true
        },{
            id: 'reference',
            header: 'Référence fournisseur',
            dataIndex: 'reference',
            width: 175,
            editor: {
                xtype: 'textfield',
                allowBlank: true,
                maxLength:50
            }
        },{
            id: 'contenance',
            header: 'Contenance',
            dataIndex: 'contenance',
            editor: {
                xtype:'numberfield',
                decimalPrecision:8,
                decimalSeparator:'.'
            }
        },{
            id: 'idUniteMesure',
            header: 'Unité de mesure',
            dataIndex: 'idUniteMesure',
            renderer:comboRenderer(comboUniteMesure),
            width: 330,
            editor: comboUniteMesure
        },{
            id: 'idContenant',
            header: 'Contenant',
            dataIndex: 'idContenant',
            renderer:comboRenderer(comboContenant),
            width: 330,
            editor: comboContenant
        }]
    });
    
    function cleanTableauConditionnement(){
        dsliv.removeAll();
    }
    
// FIN TABLEAU
// -----------------------------------------------


    var vk = new Ext.ux.plugins.VirtualKeyboard();


    var readPicto= new Ext.data.JsonReader({root : "data",id : "id"},
    Ext.data.Record.create([{name: 'boxLabel'},{name: 'inputValue'},{name: 'name'},{name: 'id'}]));

   
    var includePanel = new Ext.FormPanel({
        id: 'ProduitForm',
        controller:'Produit',
        url: 'FrontController',
        frame: true,    
        labelAlign: 'left',
        waitMsgTarget: true,
        width: 1021,
        layout: 'border',    // Specifies that the items will now be arranged in columns
        height:645,
        baseParams:{action:'saveItem', controller:'Produit'},
        items: [{
                
//------------> LE TABLEAU
             
            xtype: 'grid',
            id:'gridP', 	
            margins: '5 0 5 5',
            region: 'north',
            height:287,
            bbar: PagingBar , 
            enableColumnHide:false,
            loadMask:{msg: 'Chargement...'},
            ds: ds,
            cm: colModel,
            sm: new Ext.grid.RowSelectionModel({
                singleSelect: true,
                listeners: {
                    rowselect: function(sm, row, rec) {
                        var formPart = Ext.getCmp("formPart");
                        formPart.el.mask('Chargement...', 'x-mask-loading');                        
                        includePanel.getForm().reset();
                        cleanTableauConditionnement();
                        var idProduit = rec.get("idProduit");
                        
                        
                        Ext.getCmp("ProduitForm").getForm().doAction("load", 
                    
                    {            
                            method:'POST',
                            url:'FrontController',
                            form: 'some-form',
                            params: { controller:'Produit', action: 'getProduit', idProduit:idProduit},
                            success: function(form, action){
                                
                            
                        // pour transformer les caractères html en caractères 
                        includePanel.getForm().findField('nomSubstance').setValue(Encoder.htmlDecode(includePanel.getForm().findField('nomSubstance').getValue()));
                        includePanel.getForm().findField('description').setValue(Encoder.htmlDecode(includePanel.getForm().findField('description').getValue()));
                        includePanel.getForm().findField('nom').setValue(Encoder.htmlDecode(includePanel.getForm().findField('nom').getValue()));  

                                    formPart.el.unmask();
                                },
                                failure:function(form, action) {  
                                    var result = Ext.util.JSON.decode(action.response.responseText);
                                    Ext.Msg.alert('Erreur', action.failureType); 
                                    formPart.el.unmask();
                                }
                            }        
            
                        )

                        multiMentionDispo.load({params:{idProduit:idProduit}});
                        multiMentionAssoc.load({params:{idProduit:idProduit}});
                        multiTagDispo.load({params:{idProduit:idProduit}});
                        multiTagAssoc.load({params:{idProduit:idProduit}});
                        dsliv.load({params:{idProduit:idProduit}});
                     
                     
                        var tab = Ext.getCmp('formPart').getActiveTab();
                        var idx = Ext.getCmp('formPart').items.indexOf(tab);
                        if (idx==5){
                             //Ext.getCmp('formPart').setActiveTab(0);
                             drawCanvas(Ext.getCmp('formPart').getActiveTab(), rec.get('substance'));
                        }
                        
                      

                        //Ext.getCmp("ProduitForm").getForm().loadRecord(rec);
                                               
                        
                        
                    }
                }
            }),
            autoExpandColumn: 'nom',
             title:'Catalogue fiche produit',
            border: true,
            
            plugins:[new Ext.ux.grid.Search({
                 mode:'remote'
                ,iconCls:'icon-zoom'
                ,autoFocus:true
                ,minLength:2
                ,position:'bottom'
                ,align:'left'
                ,width:200
                ,shortcutKey:113
                ,disableIndexes:['idProduit', 'substance', 'nomFournisseur','referenceFournisseur']
            })]
        },{
            
            
        xtype:'tabpanel',
        id:'formPart',
        activeTab: 0,
        width:600,
        region: 'center',
        height:342,
        plain:true,
        deferredRender:false,
        defaults:{autoScroll: true},
        items:[{
            
            //------------> ONGLET 1
            id:'tab1',
            cls:'x-panel-mc-with-tbar',         
            tbar:formTbar,            
            bbar:formBbar,               
            title:'Informations générales',
            layout:'form',
            displayFloatingMsg:true,
            loadMask:{msg: 'Chargement...'},
            autoScroll:true,   
            margins: '0 5 5 5',   
            baseCls:'x-panel-mc',
            labelWidth: 190,
            defaults: {border:false},
            defaultType: 'textfield',
            labelSeparator:'',
            bodyStyle: Ext.isIE ? 'padding:0 0 5px 15px;' : 'padding:10px 15px;',
            
            items: [
                            {
                                    fieldLabel: 'idProduit',
                                    name: 'idProduit',
                                    hidden:true
                                },
                
                    {xtype: 'compositefield',
                    fieldLabel: 'Substance *',
                    width:348,
                    items: [
                        {   
                            xtype: 'textfield',
                            fieldLabel: 'Substance',
                            name: 'nomSubstance',
                            allowBlank: false,
                            readOnly:true,                            
                            blankText:'Le champ est requis',                
                            width:200               
                        }
                        ,
                         {
                          xtype:'button',
                          text:'Selectionner une substance',
                          windowName:'searchSubstance',
                          handler:function(){

                              var mySearchWin = eval(this.windowName);
                              mySearchWin.show();
                          } 
                         }
                    ]}
               
            ,{   
                fieldLabel: 'CAS *',
                name: 'substance',
                blankText:'Le champ est requis',                
                width:100,
                allowBlank: false,
                maxLength:15,
                readOnly:true,
                maxLengthText:'Longueur maxi : 15' 
            },{   
                fieldLabel: 'Nom du produit *',
                name: 'nom',
                allowBlank: false,
                blankText:'Le champ est requis',                
                width:300,
                maxLength:255,
                maxLengthText:'Longueur maxi : 255' ,
                keyboardConfig: {
                        language: 'Greek',
                        showIcon: true
                },
                plugins: vk                 
            },
            {
                xtype:'combo',
                allowBlank: false,
                blankText:'Le champ est requis',
                fieldLabel: 'Fournisseur *',
                name: 'idFournisseur',
                width:300,
                store: cbfournisseur,
                root:'data',
                hiddenName : 'idFournisseur',
                valueField:'idFournisseur',
                displayField:'nom',
                typeAhead: true,
                loadingText: 'Recherche...',
                mode: 'local',
                triggerAction: 'all',
                emptyText:'Selectionner un element...',
                selectOnFocus:true
            },{     
                xtype:'textarea',
            	fieldLabel: 'Description',
                name: 'description',
                allowBlank: true,
                width:300,
                keyboardConfig: {
                        language: 'Greek',
                        showIcon: true
                },
                plugins: vk
            }

            ]
            // ----------------> Fin ONGLET 1
            // ----------------> DEBUT ONGLET 2
            },
            {
              
                title: 'Autres infos',
                layout:'form',
                displayFloatingMsg:true,
                loadMask:{msg: 'Chargement...'},
                autoScroll:true,   
                margins: '0 5 5 5',   
                baseCls:'x-panel-mc',
                labelWidth: 190,
                defaults: {border:false},
                defaultType: 'textfield',
                labelSeparator:'',
                bodyStyle: Ext.isIE ? 'padding:0 0 5px 15px;' : 'padding:10px 15px;',

                items: [                                
                        {
                        xtype:'numberfield',
                        decimalPrecision:8,
                        decimalSeparator:',',
                        fieldLabel: 'Pureté',
                        name: 'purete',
                        allowBlank: true,
                        width:50
                    },{
                        xtype:'numberfield',
                        decimalPrecision:8,
                        decimalSeparator:',',
                        fieldLabel: 'Densité à 20°C',
                        name: 'densite',
                        allowBlank: true,
                        width:50
                    },{
                        xtype:'numberfield',
                        decimalPrecision:8,
                        decimalSeparator:',',
                        fieldLabel: 'Point de fusion (°C)',
                        name: 'pointDeFusion',
                        allowBlank: true,
                        width:50
                    },{
                        xtype:'numberfield',
                        decimalPrecision:8,
                        decimalSeparator:',',
                        fieldLabel: 'Point d\'ébullition (°C)',
                        name: 'pointdEbullition',
                        allowBlank: true,
                        width:50
                    },{
                        xtype:'numberfield',
                        decimalPrecision:8,
                        decimalSeparator:',',
                        fieldLabel: 'Masse molaire (g/mol)',
                        name: 'masseMolaire',
                        allowBlank: true,
                        width:50
                    },                               
                    {
                        xtype:'combo',
                        allowBlank: true,
                        blankText:'Le champ est requis',
                        fieldLabel: 'Etat physique',
                        name: 'idEtatPhysique',
                        width:200,
                        store: cbetatphysique,
                        root:'data',
                        hiddenName : 'idEtatPhysique',
                        valueField:'idEtatPhysique',
                        displayField:'libelle',
                        typeAhead: true,
                        loadingText: 'Recherche...',
                        mode: 'local',
                        triggerAction: 'all',
                        emptyText:'Selectionner un element...',
                        selectOnFocus:true
                    }
                    ,{
                        xtype:'checkbox',
                        fieldLabel: 'Refrigéré',
                        name: 'refrigere',
                        inputValue:'true'
                     }
            ]
            
// ----------------> Fin ONGLET 2           
            
            
            },
            {

                title: 'Conditionnement',
                layout:'form',
                displayFloatingMsg:true,
                loadMask:{msg: 'Chargement...'},
                autoScroll:true,
                margins: '0 5 5 5',
                baseCls:'x-panel-mc',
                labelWidth: 190,
                defaults: {border:false},
                defaultType: 'textfield',
                labelSeparator:'',
                bodyStyle: Ext.isIE ? 'padding:0 0 5px 15px;' : 'padding:10px 15px;',

                items: [
              {
                        xtype: 'editorgrid',
                        id:'gridP2',
                        split:true,
                        margins: '0 5 5 5',
                        region: 'south',
                        clicksToEdit: 1,
                        height:163,
                        minHeight:163,
                        enableColumnHide:false,
                        border: true,
                        tbar:buildTopToolbar,
//                        sm: new Ext.grid.RowSelectionModel({
//                                singleSelect: true
//                        }),
                        ds: dsliv,
                        cm: cmliv
                   },{
                                  name: 'conditionnement',
                                  hidden:true
                                }
            ]

// ----------------> Fin ONGLET 3
// ----------------> debut ONGLET 4

            },
            {
                title: 'Danger', 
                id:'dangerFormPart',
                layout:'form',
                displayFloatingMsg:true,
                loadMask:{msg: 'Chargement...'},
                autoScroll:true,   
                margins: '0 5 5 5',   
                baseCls:'x-panel-mc',
                labelWidth: 190,
                defaults: {border:false},
                defaultType: 'textfield',
            labelSeparator:'',
                bodyStyle: Ext.isIE ? 'padding:0 0 5px 15px;' : 'padding:10px 15px;',
                items: [  
                    
                 
                    
                    {
                        xtype:'remotecheckboxgroup',
                        fieldLabel: 'Pictogrammes',
                        itemCls: 'x-check-group-alt',
                        columns: 10,
                        width:700,
                        name:'pictogrammes',
                        url: 'FrontController',
                        baseParams:{action:'getListItems', controller:'Pictogramme',path:'ui/images/picto/'},
                        reader:readPicto,
                        cbRenderer:function(){},
                        cbHandler:function(){},                        
                        items:[{boxLabel:'Loading'}]
                    }  
                    ,{
                        xtype: 'itemselector',
                        name: 'idmentions',
                        imagePath: 'lib/multiselect/',
                        fieldLabel: 'Mentions de danger',
                        drawUpIcon:false,
                        drawDownIcon:false,
                        drawLeftIcon:true,
                        drawRightIcon:true,
                        drawTopIcon:false,
                        drawBotIcon:false,    
                        width:700,
                        //height:120,
                        multiselects: [{
                            id:'multiMentionDispo',
                            name:'multiMentionDispo',
                            width: 300,
                            height: 120,
                            store: multiMentionDispo,
                            displayField: 'libelle',
                            valueField: 'idMention',      
                            legend:'Mentions',
                            searchBar:true,
                            tpl:new Ext.XTemplate('<tpl for="."><div ext:qtip="{libelle}">{code} : {libelle}</div></tpl>')                            
                        },{
                            width: 300,
                            name:'multiMentionAssoc',
                            height: 120,
                            store: multiMentionAssoc,
                            displayField: 'libelle',
                            valueField: 'idMention',                
                            legend:'Mentions associées',
                            tpl:new Ext.XTemplate('<tpl for="."><div ext:qtip="{libelle}">{code} : {libelle}</div></tpl>')                            
                        }]
                        },
                        {xtype: 'compositefield',
                            labelWidth: 120,
                            items: [
                                {
                                    fieldLabel: 'Url fiche de sécurité',
                                    xtype:'textfield',
                                    emptyText:'http://...',
                                    name: 'fds',
                                    width:500,
                                    maxLength:255,
                                    maxLengthText:'Longueur maxi : 255' 
                                },
                                {
                                      xtype:'button',
                                      tooltip :'Voir la fiche dans une nouvelle fenetre',
                                      iconCls: 'icon-voir',
                                      handler:function(){
                                          var url = includePanel.getForm().findField('fds').getValue();
                                          if (url!=''){
                                              window.open(url);
                                          }
                                      }
                                }
                            ]
                        }
                      
                ]                    
            },
// ----------------> Fin ONGLET 4
// ----------------> debut ONGLET 5            
            {
                title: 'Tags',
                id:'tagtabs',                
                layout:'form',
                displayFloatingMsg:true,
                loadMask:{msg: 'Chargement...'},
                autoScroll:true,   
                margins: '0 5 5 5',   
                baseCls:'x-panel-mc',
                labelWidth: 190,
                defaults: {border:false},
                defaultType: 'textfield',
                labelSeparator:'',
                bodyStyle: Ext.isIE ? 'padding:0 0 5px 15px;' : 'padding:10px 15px;',

                items: [   
                    
                       {
                        xtype: 'itemselector',
                        name: 'idtags',
                        imagePath: 'lib/multiselect/',
                        fieldLabel: 'Tag',
                        drawUpIcon:false,
                        drawDownIcon:false,
                        drawLeftIcon:true,
                        drawRightIcon:true,
                        drawTopIcon:false,
                        drawBotIcon:false,    
                        width:700,
                        //height:120,
                        multiselects: [{
                            id:'multiTagDispo',
                            name:'multiTagDispo',
                            width: 300,
                            height: 220,
                            store: multiTagDispo,
                            displayField: 'libelle',
                            valueField: 'idtag',      
                            legend:'Tag',
                            searchBar:true                      
                        },{
                            id:'multiTagAssoc',
                            name:'multiTagAssoc',                            
                            width: 300,
                            height: 220,
                            store: multiTagAssoc,
                            displayField: 'libelle',
                            valueField: 'idtag',                
                            legend:'Tag associés'                      
                        }]
                        },
                        {xtype: 'compositefield',
                            labelWidth: 120,
                            items: [
                                {
                                    fieldLabel: 'Ajouter un tag',
                                    xtype:'textfield',
                                    name: 'newTag',
                                    width:245,
                                    maxLength:255,
                                    maxLengthText:'Longueur maxi : 255' 
                                },
                                {
                                      xtype:'button',
                                      id:'btn-addtag',
                                      iconCls: 'icon-add',
                                      tooltip :'Ajouter le tag',
                                      handler:function(){
                                          var myTag = includePanel.getForm().findField('newTag').getValue();
                                        if (myTag !=""){
                                            Ext.getCmp("tagtabs").el.mask('Ajout du tag...', 'x-mask-loading');    
                                        
                                            Ext.Ajax.request({
                                                method:'POST',
                                                url:'FrontController',
                                                form: 'some-form',
                                                params: {controller:'Tag', action: 'addItem', tagLibelle:myTag},
                                                success: function(response, options){
                                                        if (response.responseText) {						
                                                            if (response.responseText.substr(0,1) == '{') {
                                                                var ajaxResponse = Ext.util.JSON.decode(response.responseText);
                                                             }
                                                        }
                                                        if(ajaxResponse && ajaxResponse.success) {
                                                            multiTagDispo.reload();
                                                            Ext.msg.show(this.title ? this.title : 'Op&eacute;ration r&eacute;ussie:', 'Enregistrement effac&eacute; !');

                                                        }
                                                        includePanel.getForm().findField('newTag').setValue('');
                                                         Ext.getCmp("tagtabs").el.unmask(); 
                                                    },
                                                    failure:function(response, options) {  
                                                        var result = Ext.util.JSON.decode(response.responseText);
                                                        Ext.Msg.alert('Erreur', result.errors.reason); 
                                                        multiTagDispo.reload();
                                                        includePanel.getForm().findField('newTag').setValue('');
                                                         Ext.getCmp("tagtabs").el.unmask(); 
                                                    }
                                                });
                                            }

                                      }
                                },
                                {
                                      xtype:'button',
                                      id:'btn-deltag',
                                      iconCls: 'icon-delete',
                                      tooltip :'Supprimer le tag selectionné',
                                      handler:function(){
                                          var selectionsArray = Ext.getCmp("multiTagdispo").view.getSelectedIndexes();
                                          if (selectionsArray.length > 1){
                                              Ext.Msg.alert('Erreur', 'Veuillez selectionner un seul tag à supprimer'); 
                                              return;
                                          }
                                          var record = Ext.getCmp("multiTagdispo").view.store.getAt(selectionsArray[0]);
                                          if (record != null){
                                              var idtag= record.get('idtag'); 

                                              if (IsNumeric(idtag) && idtag > 0){

                                                 Ext.Ajax.request({
                                                    method:'POST',
                                                    url:'FrontController',
                                                    form: 'some-form',
                                                    params: {controller:'Tag', action: 'deleteItem', idtag:idtag},
                                                    success: function(response, options){
                                                            if (response.responseText) {						
                                                                if (response.responseText.substr(0,1) == '{') {
                                                                    var ajaxResponse = Ext.util.JSON.decode(response.responseText);
                                                                 }
                                                            }
                                                            if(ajaxResponse && ajaxResponse.success) {
                                                                multiTagdispo.reload();
                                                                Ext.msg.show(this.title ? this.title : 'Op&eacute;ration r&eacute;ussie:', 'Enregistrement effac&eacute; !');

                                                            }
                                                            includePanel.getForm().findField('newTag').setValue('');
                                                             Ext.getCmp("tagtabs").el.unmask(); 
                                                        },
                                                        failure:function(response, options) {  
                                                            var result = Ext.util.JSON.decode(response.responseText);
                                                            Ext.Msg.alert('Erreur', result.errors.reason); 
                                                            multiTagdispo.reload();
                                                            includePanel.getForm().findField('newTag').setValue('');
                                                             Ext.getCmp("tagtabs").el.unmask(); 
                                                        }
                                                    });
                                                }
                                          }
                                      }
                                }
                            ]
                        }                 
      
                    

                ]
            } 
// ----------------> Fin ONGLET 5
// ----------------> debut ONGLET 6     
,{
                title: 'Molecule', 
                id:'MoleculeFormPart',
                layout:'form',
                displayFloatingMsg:true,
                loadMask:{msg: 'Chargement...'},
                autoScroll:true,   
                margins: '0 5 5 5',   
                baseCls:'x-panel-mc',
                labelWidth: 190,
                defaults: {border:false},
                defaultType: 'textfield',
                labelSeparator:'',
                bodyStyle: Ext.isIE ? 'padding:0 0 5px 15px;' : 'padding:10px 15px;',
                items: [  {
                                xtype:'box',
                                anchor:'',
                                autoEl:{
                                    tag:'div',
                                    style:'margin:1px 0 1px 1px',
                                    children:[{
                                        tag:'canvas',
                                         id:'transformer'
                                      }]
                            } 
                           }
                ],
                listeners: {
                    activate:function(){
                        drawCanvas();
                    }
                }
                
            }
            ]
        
        }

        ],
        saveItem:function(){
                if (includePanel.getForm().isValid()){

                        var erreurConditionnement = false;

                        var grid = Ext.getCmp('gridP2');
                        // si pas de ligne : on annule
                        if (grid.getStore().getCount()==0){
                             Ext.Msg.alert('Erreur', "Vous devez saisir au moins un conditionnement");
                                return;
                        }

                        // CONVERSION DE LA GRILLE DES LIVRAION SON JSON
                        grid.stopEditing();
                        var recordsToSend = grid.getStore().getRange();
                        var farray = [];
                        Ext.each(recordsToSend, function(aRecord) {   
                            //if (aRecord.get('reference')=="" || aRecord.get('contenance')=="" ||
                            // Suppression de l'obligation de saisir une reference fournisseur
                            if (aRecord.get('contenance')=="" ||
                            aRecord.get('idUniteMesure')=="" || aRecord.get('idContenant')==""){
                                Ext.Msg.alert('Erreur', "Vous devez saisir toutes les informations du conditionnement");
                                erreurConditionnement = true;
                                return;
                            }
                            farray.push(aRecord.data);
                        });

                        if (erreurConditionnement == true){
                            return;
                        }

                        var JSONToSend = Ext.encode(farray);
                        includePanel.getForm().findField('conditionnement').setValue(JSONToSend);
                        // FIN conversion


                // pour transformer les caractères html en caractères 
                includePanel.getForm().findField('description').setValue(Encoder.htmlEncode(includePanel.getForm().findField('description').getValue()));
                includePanel.getForm().findField('nom').setValue(Encoder.htmlEncode(includePanel.getForm().findField('nom').getValue()));   
                
                includePanel.getForm().submit({waitTitle:'Connexion au serveur', waitMsg:'Enregistrement en cours...',
                        success:function(f,action){
                           ds.reload();
                           includePanel.getForm().reset();
                           cleanTableauConditionnement();
                           Ext.getCmp("gridP").getSelectionModel().clearSelections();
                           Ext.getCmp("formPart").setActiveTab("tab1");
                           Ext.msg.show(this.title ? this.title : 'Op&eacute;ration r&eacute;ussie:', 'Enregistrement effectu&eacute; !');
                },
                        failure:function(f,action){
				var result = Ext.util.JSON.decode(action.response.responseText);
				Ext.Msg.alert('Erreur', result.errors.reason);   
                        }
                });
        }
        },
        resetToNull: function() {
           var form = this.getForm();
           if (form.isDirty()){
            Ext.MessageBox.show({
               buttons: Ext.Msg.OKCANCEL,
               title:'Etes-vous sur ?',
               msg:'Les données non enregistrées vont être effacées. Etes-vous sur ?',
               fn:function(btn) {
                   if(btn == 'ok') {
                       Ext.getCmp("ProduitForm").resetForm();
                       cleanTableauConditionnement();
                       Ext.getCmp("gridP").getSelectionModel().clearSelections();
                       Ext.getCmp("formPart").setActiveTab("tab1");
                       drawCanvas();
                   }else{
                       return;

                   }
            }});
            return false;
            }
            Ext.getCmp("formPart").setActiveTab("tab1");
    },
    resetForm:function(){
        var form = this.getForm();
        form.items.each(function(f){			
       if(f.getXType() == 'xcheckbox')
            f.originalValue = false;
        else
            f.originalValue = '';
        }, form);
        form.reset();
    },   
    deleteItem: function(id) {
        if(id == null)
            var this_id = this.getForm().findField(this.gridP).getValue();
        else
            var this_id = id;
        if(this_id != '') {
            Ext.MessageBox.confirm('Etes-vous sur ?','Voulez-vous effacer cet enregistrement&nbsp;?',function(btn) {
                if(btn == 'yes') {
                        Ext.getCmp("gridP").loadMask.show();   
                        var formPart = Ext.getCmp("formPart");
                        formPart.el.mask('Chargement...', 'x-mask-loading');                     
                        var myFormParams = {
                            controller:this.controller,
                            action:'deleteItem',
                            id:this_id,
                            output:'json'
                        };
                        // adding extra params if necessary!
                        if (this.extraParams) {
                            for (key in this.extraParams) {
                                myFormParams[key] = this.extraParams[key]; 
                            }
                        }
                        Ext.Ajax.request({
                            params:myFormParams,
                            url:'FrontController',
                             success:function(response, options){
                                if (response.responseText) {						
                                    if (response.responseText.substr(0,1) == '{') {
			    	        var ajaxResponse = Ext.util.JSON.decode(response.responseText);
				     }
	                        }
                                if(ajaxResponse && ajaxResponse.success) {
                                    includePanel.getForm().reset();
                                    cleanTableauConditionnement();
                                    Ext.getCmp("gridP").getSelectionModel().clearSelections();
                                    Ext.getCmp("formPart").setActiveTab("tab1");
                                    ds.reload();
                                    Ext.msg.show(this.title ? this.title : 'Op&eacute;ration r&eacute;ussie:', 'Enregistrement effac&eacute; !');

                                }
                                formPart.el.unmask(); 
                            },
                            failure:function(response, options) {  
				var result = Ext.util.JSON.decode(response.responseText);
				Ext.Msg.alert('Erreur', result.errors.reason); 
                                ds.reload();
                                formPart.el.unmask(); 
                            }
                        }); 
                } else {
                  //do nothing
                }
            },this);
            return false;           
        } else {
            Ext.Msg.alert('Erreur','Vous devez s&eacute;lectionner un enregistrement');
        }
    }
    
    });
    




function reset(){

    includePanel.resetToNull();

}

function deleteligne(id){
    includePanel.deleteItem(id);
}

function save(){
    includePanel.saveItem();
}

var mapwin;

function closeImgWindow(){
	if(mapwin){
		mapwin.hide();
		mapwin= null;
	}
}

function openImgWindow(imageName, imageId){

		if(!mapwin){
			mapwin = new Ext.Window({
				layout: 'fit',
				closeAction: 'hide',
				width:164,
				height:222,
                                x:10,
                                cls:'whitebck',
				items: {
					xtype:'box',
					showAnimDuration:1
					,anchor:''
                                        ,cls:'whitebck'
					,isFormField:true
					,fieldLabel:'Image'
					,autoEl:{
                                            tag:'div', children:[{
                                                     tag:'img'
                                                    ,src:imageName+'.jpg'
                                            }]
					}
					
				}
			});					
		}        
        mapwin.show();
}


function drawCanvas(tabPanel, CASSelection){
    
        var CAS = "";
        if (CASSelection!=undefined){ 
            CAS = CASSelection;
        }else{
            CAS = includePanel.getForm().findField('substance').getValue();
        }
        
        if (CAS != ''){

            Ext.Ajax.request({
                method:'POST',
                url:'FrontController',
                params: { controller:'Produit', action: 'getMolecule', CAS:CAS},
            success: function(response, options){
                
                    var cls = response.responseText;
                    if (cls !=''){
                        
                        if (cls.substr(0,1) == '{') {
                            var canvas = document.getElementById('transformer');
                            canvas.width = canvas.width + 1;
                        }else{    
                            // initialize component and set visual specifications
                            var transformer = new ChemDoodle.TransformCanvas('transformer', 300, 270, true);
                            // gradient between JMol colors for atom types when drawing bonds
                            transformer.specs.bonds_useJMOLColors = true;
                            // make bonds thicker
                            transformer.specs.bonds_width_2D = 3;
                            // don't draw atoms
                            transformer.specs.atoms_display = false;
                            // change the background color to black
                            transformer.specs.backgroundColor = 'black';
                            // clear overlaps to show z-depth
                            transformer.specs.bonds_clearOverlaps_2D = true;

                            var sub = ChemDoodle.readMOL(cls);
                            transformer.loadMolecule(sub);


                       }
                    }                        
                    
            },
                failure: function(response, opts) {
                    console.log('server-side failure ' + url);
                }
            });
        }else{
            var canvas = document.getElementById('transformer');
            canvas.width = canvas.width + 1;
        }

            
        
}