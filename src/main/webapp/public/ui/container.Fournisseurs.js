/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2012/01/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/

Ext.QuickTips.init();

    Ext.form.Field.prototype.msgTarget = 'side';

    var bd = Ext.getBody();
    
 // ---------------------------------------
 // GESTION DES DROITS
   Ext.Ajax.request({
        params:{
                controller:'Fournisseur',
                action:'getRight'
            },
         url:'FrontController',
         success:function(response, options){
 
         },
         failure:function(response, options) {  
                Ext.getCmp("btn-enreg").hide();
                Ext.getCmp("btn-new").hide();
         }
    }); 
 // --------------------------------------- 

// ----------------------------------------------------------
// TABLEAU - GRID 
// acces au données pour le tableau
    var proxy = new Ext.data.HttpProxy({
        url: 'FrontController',
        method: 'POST'
    });
    
    var rec = Ext.data.Record.create([
            {name: 'idFournisseur'},
            {name: 'referenceSIFAC'},
            {name: 'nom'},
            {name: 'adresse1'},
            {name: 'adresse2'},
            {name: 'codePostal'},
            {name: 'ville'},
            {name: 'telephone'},
            {name: 'fax'},
            {name: 'nomContact'},
            {name: 'write'}
    ]);

    var reader = new Ext.data.JsonReader({
 	   root : "data",
 	   id : "idFournisseur"
    }, rec);


    var ds = new Ext.data.JsonStore({
        proxy: proxy,
        fields: ['idFournisseur', 'referenceSIFAC','nom', 'adresse1', 'adresse2', 'codePostal', 'ville', 'telephone','fax','nomContact','write'],
        root:'data',
        totalProperty: 'totalCount',
        reader: reader,
        nocache: true,
        autoLoad: false,
        baseParams:{action:'getListItems', controller:'Fournisseur'}
        ,listeners:{
            load:function(){ 
                if(typeof(myMask) !== 'undefined')
                    myMask.hide();
            }
        }
    });
    ds.load({params:{start:0, limit:10}});


    var colModel = new Ext.grid.ColumnModel([
        {id:'idFournisseur',header: "idFournisseur", width: 80, sortable: true, locked:false, dataIndex: 'idFournisseur', hidden:true},
        {id:'referenceSIFAC',header: "SIFAC", width: 100, sortable: true, dataIndex: 'referenceSIFAC'},
        {id:'nom',header: "Nom", width: 100, sortable: true, dataIndex: 'nom'},
        {header: "Adresse", width: 100, sortable: true, dataIndex: 'adresse1'},
        {header: "adresse2", width: 200, sortable: true, dataIndex: 'adresse2', hidden:true},
        {header: "codepostal", width: 100, sortable: true, dataIndex: 'codePostal', hidden:true},
        {header: "ville", width: 200, sortable: true, dataIndex: 'ville'},
        {header: "telephone", width: 200, sortable: true, dataIndex: 'telephone', hidden:true},
        {header: "fax", width: 200, sortable: true, dataIndex: 'fax', hidden:true},
        {header: "nomContact", width: 200, sortable: true, dataIndex: 'nomContact'},
        {header: "Supp", width: 40, sortable: false, xtype:'actioncolumn',
        items: [{
                    getClass: function(v, meta, rec) {          // Or return a class from a function
                        if (rec.get('write') == 'true'){
                            this.items[0].tooltip = 'Supprimer cette ligne';
                            return 'del-col';
                        }
                    },
                    handler: function(grid, rowIndex, colIndex) {
                        var rec = ds.getAt(rowIndex);
                        deleteligne(rec.get('idFournisseur'));
                    }
                }]}
    ]);

    var PagingBar = new Ext.PagingToolbar({
        pageSize: 10,
        store: ds,
        displayInfo: true,
        action:'getListItems', 
        controller:'Fournisseur',
        displayMsg: 'Fournisseurs {0} - {1} of {2}',
        emptyMsg: 'Aucun Fournisseur'
    });   
    
    PagingBar.insertButton(11, excelButton);
// FIN TABLEAU - GRID
// ----------------------------------------------------------   
       
    

// ---------------------------------------------------------- 
// ---------------------------------------------------------- 
//  Toolbar Tableau
   var formTbar = new Ext.Toolbar({
     height: 27,
     items: [{text: 'Nouveau fournisseur',
                iconCls: 'icon-add',
                id:'btn-new',
                handler : reset
               },'->',{
                id:'editXls',
                iconCls:'icon-excel',
                value: '',
                handler:function(){
                    var value = includePanel.getForm().findField('idFournisseur').getValue();
                    if (value>0){
                        printAction("Fournisseur","getXLSList", value);
                    }else{
                        Ext.Msg.alert('Export impossible','Veuillez selectionner un item à éditer');
                        return;
                    }
                }            
            },{
                id:'edit',
                iconCls:'icon-print',
                value: '',
                handler:function(){
                    var value = includePanel.getForm().findField('idFournisseur').getValue();
                    if (value>0){
                        printAction("Fournisseur","getPDF", value);
                    }else{
                        Ext.Msg.alert('Export impossible','Veuillez selectionner un item à éditer');
                        return;
                    }
                }            
            }]
    });
    
     var formBbar = new Ext.Toolbar({
     height: 27,
     id:'bottom-bar',
     cls:'x-bbar',
     items: [                   '->', 
               {id:'btn-enreg',
                text: 'Enregistrer',
                iconCls: 'icon-save',
                handler : save
                }]

    });   
// ---------------------------------------------------------- 
       
    
    
    var includePanel = new Ext.FormPanel({
        id: 'FournisseurForm',
         controller:'Fournisseur',
        url: 'FrontController',
        frame: true,    
        labelAlign: 'left',
        waitMsgTarget: true,
        width: 1021,
        layout: 'border',    // Specifies that the items will now be arranged in columns
        height:645,
        baseParams:{action:'saveItem', controller:'Fournisseur'},
        items: [{
            xtype: 'grid',
            id:'gridP',            
            margins: '5 0 5 0',
            region: 'north',
            height:287,
            bbar: PagingBar , 
            enableColumnHide:false,
            loadMask:{msg: 'Chargement...'},
            ds: ds,
            cm: colModel,
            sm: new Ext.grid.RowSelectionModel({
                singleSelect: true,
                listeners: {
                    rowselect: function(sm, row, rec) {
                        var formPart = Ext.getCmp("formPart");
                        var this_id = rec.data.idUtilisateur;
                        formPart.el.mask('Chargement...', 'x-mask-loading');
                        Ext.getCmp("FournisseurForm").getForm().loadRecord(rec);  
                        
                       
                        formPart.el.unmask();
                    }
                }
            }),
            autoExpandColumn: 'nom',
             title:'Fournisseurs',
            border: true,
            
            plugins:[new Ext.ux.grid.Search({
                 mode:'remote'
                ,iconCls:'icon-zoom'
                ,autoFocus:true
                ,minLength:2
                ,position:'bottom'
                ,align:'left'
                ,width:200
                ,shortcutKey:113
                ,disableIndexes:['idFournisseur', 'telephone','fax']
            })]
        },
        
          {
            xtype:'panel',
            layout:'border',
            region: 'center',
            autoScroll:false,
            id:'formPart',
            cls:'x-fieldset-with-tbar',            
            tbar:formTbar,            
            bbar:formBbar,                  
            defaults: {frame:true},
            border: true,
            items:[{
                                
                                displayFloatingMsg:true,
                                split:true,        	
                                margins: '0 0 0 0',        	
                                columnWidth: 0.9,
                                autoScroll:true,
                                region: 'center',    
                                xtype: 'fieldset',
                                loadMask:{msg: 'Chargement...'},
                                labelWidth: 190,
                                defaults: {width: 140, border:false},    // Default config options for child items
                                defaultType: 'textfield',
                                labelSeparator:'',
                                //autoHeight: true,
                                bodyStyle: Ext.isIE ? 'padding:0 0 0px 15px;' : 'padding:0px 15px;',
                                
                                items: [{
                                    fieldLabel: 'idFournisseur',
                                    name: 'idFournisseur',
                                    hidden:true
                                },{   
                                    fieldLabel: 'Reference SIFAC',
                                    name: 'referenceSIFAC',              
                                    width:300,
                                    maxLength:45,
                                    maxLengthText:'Longueur maxi : 45'
                                }
                                ,{   
                                    fieldLabel: 'Nom *',
                                    name: 'nom',
                                    allowBlank: false,
                                    blankText:'Le champ est requis',                
                                    width:300,
                                    maxLength:45,
                                    maxLengthText:'Longueur maxi : 45'
                                },{   
                                    id:'adresse1',  
                                    fieldLabel: 'Adresse',
                                    name: 'adresse1',        
                                    width:300,
                                    maxLength:255,
                                    maxLengthText:'Longueur maxi : 255'                
                                },{   
                                    fieldLabel: 'Complement',
                                    name: 'adresse2',
                                    width:300,
                                    maxLength:255,
                                    maxLengthText:'Longueur maxi : 255'                        
                                },{            	
                                    xtype:'numberfield',
                                    fieldLabel: 'Code Postal',
                                    name: 'codePostal',
                                    allowDecimals:false,
                                    width:50,
                                    maxLength:6,
                                    maxLengthText:'Longueur maxi : 6'
                                },{
                                    fieldLabel: 'Ville',
                                    name: 'ville',
                                    width:300,
                                    maxLength:255,
                                    maxLengthText:'Longueur maxi : 255'                        
                                },{
                                    fieldLabel: 'Telephone',
                                    name: 'telephone',
                                    width:100,
                                    maxLength:12,
                                    maxLengthText:'Longueur maxi : 12'                        
                                },{
                                    fieldLabel: 'Fax',
                                    name: 'fax',
                                    width:100,
                                    maxLength:12,
                                    maxLengthText:'Longueur maxi : 12'                  
                                },{
                                    fieldLabel: 'Contact',
                                    name: 'nomContact',
                                    width:300,
                                    maxLength:255,
                                    maxLengthText:'Longueur maxi : 255'                  
                                }


                                ]

                        }
            
            ]}
            
            
            
    ]
    ,
        saveItem:function(){
                if (includePanel.getForm().isValid()){
                includePanel.getForm().submit({waitTitle:'Connexion au serveur', waitMsg:'Enregistrement en cours...',
                        success:function(f,action){
                           ds.reload();
                           includePanel.getForm().reset();                      
                            Ext.msg.show(this.title ? this.title : 'Op&eacute;ration r&eacute;ussie:', 'Enregistrement effectu&eacute; !');
                },
                        failure:function(f,action){
				var result = Ext.util.JSON.decode(action.response.responseText);
				Ext.Msg.alert('Erreur', result.errors.reason);   
                        }
                });
        }
        },
        resetToNull: function() {
           var form = this.getForm();
           if (form.isDirty()){
            Ext.MessageBox.show({
               buttons: Ext.Msg.OKCANCEL,
               title:'Etes-vous sur ?',
               msg:'Les données non enregistrées vont être effacées. Etes-vous sur ?',
               fn:function(btn) {
                   if(btn == 'ok') {
                       Ext.getCmp("FournisseurForm").resetForm();
                       Ext.getCmp("gridP").getSelectionModel().clearSelections();
                   }else{
                       return;
                   }
            }});
            return false;
            }

    },
    resetForm:function(){
        var form = this.getForm();
        form.items.each(function(f){			
       if(f.getXType() == 'xcheckbox')
            f.originalValue = false;
        else
            f.originalValue = '';
        }, form);
        form.reset();  
    },   
    deleteItem: function(id) {
        if(id == null)
            var this_id = this.getForm().findField(this.gridP).getValue();
        else
            var this_id = id;
        if(this_id != '') {
            Ext.MessageBox.confirm('Etes-vous sur ?','Voulez-vous effacer cet enregistrement&nbsp;?',function(btn) {
                if(btn == 'yes') {
                        Ext.getCmp("gridP").loadMask.show();   
                        var formPart = Ext.getCmp("formPart");
                        formPart.el.mask('Chargement...', 'x-mask-loading');                     
                        var myFormParams = {
                            controller:this.controller,
                            action:'deleteItem',
                            id:this_id,
                            output:'json'
                        };
                        // adding extra params if necessary!
                        if (this.extraParams) {
                            for (key in this.extraParams) {
                                myFormParams[key] = this.extraParams[key]; 
                            }
                        }
                        Ext.Ajax.request({
                            params:myFormParams,
                            url:'FrontController',
                             success:function(response, options){
                                if (response.responseText) {						
                                    if (response.responseText.substr(0,1) == '{') {
			    	        var ajaxResponse = Ext.util.JSON.decode(response.responseText);
				     }
	                        }
                                if(ajaxResponse && ajaxResponse.success) {
                                    includePanel.getForm().reset();
                                    ds.reload();
                                    Ext.msg.show(this.title ? this.title : 'Op&eacute;ration r&eacute;ussie:', 'Enregistrement effac&eacute; !');

                                }
                                formPart.el.unmask(); 
                            },
                            failure:function(response, options) {  
				var result = Ext.util.JSON.decode(response.responseText);
				Ext.Msg.alert('Erreur', result.errors.reason); 
                                ds.reload();
                                formPart.el.unmask(); 
                            }
                        }); 
                } else {
                  //do nothing
                }
            },this);
            return false;           
        } else {
            Ext.Msg.alert('Erreur','Vous devez s&eacute;lectionner un enregistrement');
        }
    }
    
    });
    

function reset(){

    includePanel.resetToNull();

}

function deleteligne(id){
    includePanel.deleteItem(id);
}

function save(){
    includePanel.saveItem();
}


