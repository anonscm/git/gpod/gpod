/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2012/01/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/

var myTimer;

Ext.QuickTips.init();

    Ext.form.Field.prototype.msgTarget = 'side';

    var bd = Ext.getBody();


    var proxy = new Ext.data.HttpProxy({
        url: 'FrontController',
        method: 'POST',
        timeout: 300000
    });







// ---------------------------------------------------------- 
// STORES


    var ds = new Ext.data.JsonStore({
        proxy: proxy,
        fields: ['CAS','idProduit', 'nomProduit', 'idAire', 'nomAire', 'quantite', 'aireLat', 'aireLng'],
        root:'data',
        totalProperty: 'totalCount',
        nocache: true,
        autoLoad: false,
        baseParams:{action:'getListItems', controller:'StockMap'}
    });



    // ComboBox site
    var cbsite = new Ext.data.JsonStore({
        id:cbsite,
        proxy: proxy,
        fields: ['idSite', 'nom'],
        root:'data',
        totalProperty: 'totalCount',
        nocache: true,
        autoLoad: true,
        baseParams:{action:'getListItems', controller:'Site'},
        listeners : {
        load : function() {
            var rec = new this.recordType({idSite:0, nom:'0 - Tous les Sites'});
            rec.commit();
            this.add(rec);
            this.sort('nom');
            Ext.getCmp('idSitec').setValue(0);
        }}
    });

    // ComboBox Batiment
    var cbbatiment = new Ext.data.JsonStore({
        id:cbbatiment,
        proxy: proxy,
        fields: ['idBatiment', 'nom', 'adresse1', 'adresse2', 'codePostal', 'ville', 'longitude','latitude'],
        root:'data',
        totalProperty: 'totalCount',
        nocache: true,
        autoLoad: true,
        baseParams:{action:'getListItems', controller:'Batiment'},
        listeners : {
        load : function() {
            var rec = new this.recordType({idBatiment:0, nom:'0 - Tous les Batiments'});
            rec.commit();
            this.add(rec);
            this.sort('nom');
            Ext.getCmp('idBatimentc').setValue(0);
        }}
    });
    cbbatiment.load({params:{idSite:-1}});
    
    var cbsalle = new Ext.data.JsonStore({
        id:cbsalle,
        proxy: proxy,
        fields: ['idSalleStockage', 'code', 'nom', 'adresse1', 'ville'],
        root:'data',
        totalProperty: 'totalCount',
        nocache: true,
        autoLoad: false,
        baseParams:{action:'getListItems', controller:'SalleStockage'}
        ,listeners : {
        load : function() {
            var rec = new this.recordType({idSalleStockage:0, nom:'0 - Toutes les Salles de Stockage'});
            rec.commit();
            this.add(rec);
            this.sort('nom');
            Ext.getCmp('idSallec').setValue(0);
            if(typeof(myMask) !== 'undefined')
                myMask.hide();
            
        }}
    });
    cbsalle.load({params:{idBatiment:-1}});        


    // ComboBox Aire
    var cbaire = new Ext.data.JsonStore({
        id:cbaire,
        proxy: proxy,
        fields: ['idAire', 'code', 'nom', 'refrigere'],
        root:'data',
        totalProperty: 'totalCount',
        nocache: true,
        autoLoad: false,
        baseParams:{action:'getListItems', controller:'AireStockage'}
        ,listeners : {
        load : function() {
            var rec = new this.recordType({idAire:0, nom:'0 - Toutes les aires de Stockage'});
            rec.commit();
            this.add(rec);
            this.sort('nom');
            Ext.getCmp('idAirec').setValue(0);
            if(typeof(myMask) !== 'undefined')
                myMask.hide();
            
        }}
    });
    cbaire.load({params:{idSalleStockage:-1}});    


    var signeData = [
        ["="],
        [">"],
        [">="],
        ["<"],
        ["<="],
    ];

    var cbsigne = new Ext.data.ArrayStore({
        fields: [
           {name: 'nomSigne'},
        ]
    });

    cbsigne.loadData(signeData);

    var readPicto= new Ext.data.JsonReader({root : "data",id : "id"},
    Ext.data.Record.create([{name: 'boxLabel'},{name: 'inputValue'},{name: 'name'},{name: 'id'}]));


// STORES
// ----------------------------------------------------------




    var includePanel = new Ext.FormPanel({
        id: 'StockForm',
        controller:'LivraisonSimple',
        url: 'FrontController',
        frame: true,
        labelAlign: 'left',
        waitMsgTarget: true,
        width: 1021,
        //layout: 'border',    // Specifies that the items will now be arranged in columns
        height:645,
        baseParams:{action:'saveItem', controller:'LivraisonSimple'},
        items: [{
            id:'formPart',
            displayFloatingMsg:true,
            title:'Visualisation du Stock',
            split:true,
            margins: '0 5 5 5',
            columnWidth: 0.9,
            autoScroll:true,
            height:300,
            xtype: 'fieldset',
            loadMask:{msg: 'Chargement...'},
            labelWidth: 190,
            autoHeight:true,
            collapsible: true,
            defaults: {border:false},    // Default config options for child items
            defaultType: 'textfield',
            labelSeparator:'',
            bodyStyle: Ext.isIE ? 'padding:0 0 0px 15px;' : 'padding:0px 15px;',
            border: true,
            listeners: {
                    collapse: function(panel) {
                        Ext.getCmp("panelMap").setHeight(597);
                    },
                    expand: function(panel) {
                        Ext.getCmp("panelMap").setHeight(260);
                    }
             },
             buttons: [{
                    text:'Annuler',
                    handler: function(){
                        reset();
                        ds.removeAll();
                        includePanel.getForm().findField('regroupeArticle').setValue(true);
                    }
                },{
                    text:'Rechercher',
                    handler: function(){

                        Ext.getCmp("panelMap").el.mask('Recherche du stock', 'x-mask-loading');

                        var idSite = includePanel.getForm().findField('idSite').getValue();
                        var idBatiment = includePanel.getForm().findField('idBatiment').getValue();
                        var idAire = includePanel.getForm().findField('idAire').getValue();
                        var idSalleStockage = includePanel.getForm().findField('idSalleStockage').getValue();
                        var idProduitConditionnement = includePanel.getForm().findField('idProduitConditionnement').getValue();
                        var nomSigne = includePanel.getForm().findField('nomSigne').getValue();
                        var idSousStructure = includePanel.getForm().findField('idSousStructure').getValue();
                        var valeurStock = includePanel.getForm().findField('valeurStock').getValue();
                        var pictogrammes = includePanel.getForm().findField('pictogrammes').getValue();
                        var etiquette = includePanel.getForm().findField('etiquette').getValue();
                        var CAS = includePanel.getForm().findField('CAS').getValue();
                        var autre = includePanel.getForm().findField('autre').getValue();
                        var pictoString ="";
                        for (var i=0;i<pictogrammes.length;i++){
                            pictoString = pictoString + ',' + pictogrammes[i].getRawValue();
                        }
                        if (pictogrammes.length>0){
                            pictoString = pictoString.substring(1);
                        }
                        //var regroupeArticle = includePanel.getForm().findField('regroupeArticle').getValue();


                        ds.removeAll();
                        Ext.getCmp("formPart").collapse();

                         ds.load({params:{
                                 idSite:idSite,
                                 idBatiment:idBatiment,
                                 idSalleStockage:idSalleStockage,
                                 idAire:idAire,
                                 idProduitConditionnement:idProduitConditionnement,
                                 idSousStructure:idSousStructure,
                                 nomSigne:nomSigne,
                                 valeurStock:valeurStock,
                                 pictogrammes:pictoString,
                                 autre:autre,
                                 cas:CAS,
                                 //regroupeArticle:regroupeArticle,
                                 etiquette:etiquette},
                                callback: function(records, options, success ){
                                       if (success){
                                           Ext.getCmp("myGmap").clearMarkers();
                                           Ext.getCmp("myGmap").addJsonMarkers(records, true);
                                       }
                                       Ext.getCmp("panelMap").el.unmask();
                                  }});
                        //

                    }
                }
                ],
            items: [
                    {
                        id:'idSitec',
                        xtype:'combo',
                        allowBlank: true,
                        blankText:'Le champ est requis',
                        fieldLabel: 'Site',
                        name: 'nomSite',
                        width:200,
                        store: cbsite,
                        root:'data',
                        hiddenName : 'idSite',
                        valueField:'idSite',
                        displayField:'nom',
                        typeAhead: true,
                        loadingText: 'Recherche...',
                        mode: 'local',
                        triggerAction: 'all',
                        emptyText:'Selectionner un element...',
                        selectOnFocus:true,
                        listeners:{
                             'select': function() {

                                var idsite = this.getValue();
                                if (idsite == 0 || idsite==''){
                                    cbbatiment.load({params:{idSite:-1}});
                                    cbaire.load({params:{idBatiment:-1}});
                                }else{
                                    cbbatiment.load({params:{idSite:idsite}});
                                }
                             }
                        }
                    },
                    {
                        id:'idBatimentc',
                        xtype:'combo',
                        allowBlank: true,
                        blankText:'Le champ est requis',
                        fieldLabel: 'Batiment',
                        name: 'nomBatiment',
                        width:300,
                        store: cbbatiment,
                        root:'data',
                        hiddenName : 'idBatiment',
                        valueField:'idBatiment',
                        displayField:'nom',
                        typeAhead: true,
                        loadingText: 'Recherche...',
                        mode: 'local',
                        triggerAction: 'all',
                        emptyText:'Selectionner un element...',
                        selectOnFocus:true,
                        listeners:{
                             'select': function() {
                                var idBatiment = this.getValue();
                                if (idBatiment == 0 || idBatiment==''){
                                    cbsalle.load({params:{idBatiment:-1}});
                                }else{
                                    cbsalle.load({params:{idBatiment:idBatiment}});
                                }

                             }
                         }
                       },
                    {
                        id:'idSallec',
                        xtype:'combo',
                        fieldLabel: 'Salle de Stockage',
                        name: 'nomSalleStockage',
                        width:300,
                        store: cbsalle,
                        root:'data',
                        hiddenName : 'idSalleStockage',
                        valueField:'idSalleStockage',
                        displayField:'nom',
                        typeAhead: true,
                        loadingText: 'Recherche...',
                        mode: 'local',
                        triggerAction: 'all',
                        emptyText:'Selectionner un element...',
                        selectOnFocus:true,
                        listeners:{
                             'select': function() {
                                var idSalle = this.getValue();
                                if (idSalle == 0 || idSalle==''){
                                    cbaire.load({params:{idSalleStockage:-1}});
                                }else{
                                    cbaire.load({params:{idSalleStockage:idSalle}});
                                }

                             }
                         }                        
                       },
                    {
                        id:'idAirec',
                        xtype:'combo',
                        fieldLabel: 'Local de Stockage',
                        name: 'nomAire',
                        width:300,
                        store: cbaire,
                        root:'data',
                        hiddenName : 'idAire',
                        valueField:'idAire',
                        displayField:'nom',
                        typeAhead: true,
                        loadingText: 'Recherche...',
                        mode: 'local',
                        triggerAction: 'all',
                        emptyText:'Selectionner un element...',
                        selectOnFocus:true
                       }
                       ,
                    {
                      xtype: 'compositefield',
                      fieldLabel: 'Produit (Recherche 1 produit)',
                      width:324,
                      items: [
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Produit',
                            name: 'nomProduit',
                            readOnly:true,
                            width:200
                         },
                         {
                          xtype:'button',
                          text:'Selectionner un Produit',
                          windowName:'searchProduit',
                          handler:function(){
                              var mySearchWin = eval(this.windowName);
                              mySearchWin.show();
                          }
                         }
                    ]},{
                    xtype: 'textfield',
                    fieldLabel: 'Produit (Libelle/Description)',
                    name:'autre',
                    id:'autre',
                    width:200
                },
                    {
                      xtype: 'compositefield',
                      fieldLabel: 'Equipe/Département',
                      name:'equipeComposite',
                      width:493,
                      items: [
                        {
                            xtype: 'textfield',
                            name: 'nomSousStructure',
                            readOnly:true,
                            width:300
                         },
                         {
                          xtype:'button',
                          text:'Selectionner une Equipe/Département',
                          windowName:'searchSousStructure',
                          id:'searchSousStructure',
                          handler:function(){
                              var mySearchWin = eval(this.windowName);
                              mySearchWin.show();
                          }
                         }
                    ]},{
                      xtype: 'compositefield',
                      fieldLabel: 'Quantité en stock',
                      width:324,
                      items: [
                    {
                        xtype:'combo',
                        name: 'nomSigne',
                        width:50,
                        store: cbsigne,
                        root:'data',
                        hiddenName : 'nomSigne',
                        valueField:'nomSigne',
                        displayField:'nomSigne',
                        mode: 'local',
                        triggerAction: 'all',
                        value:'>',
                        selectOnFocus:true
                       },
                        {
                            xtype:'numberfield',
                            decimalPrecision:15,
                            name: 'valeurStock',
                            decimalSeparator:',',
                                width:50,
                            value:'0'
                         }
                    ]},{
                        xtype:'remotecheckboxgroup',
                        fieldLabel: 'Classes de risque',
                        itemCls: 'x-check-group-alt',
                        columns: 10,
                        width:600,
                        name:'pictogrammes',
                        url: 'FrontController',
                        baseParams:{action:'getListItems', controller:'Pictogramme',path:'ui/images/picto/'},
                        reader:readPicto,
                        cbRenderer:function(){},
                        cbHandler:function(){},
                        items:[{boxLabel:'Loading'}]
                      }
//                      ,{
//                        xtype:'checkbox',
//                        fieldLabel:'Regroupement',
//                        boxLabel: 'Regrouper les articles identiques présent dans un même lieu de stockage',
//                        name: 'regroupeArticle',
//                        inputValue:'true',
//                        checked:true
//                     }
                     ,{
                      xtype: 'compositefield',
                      fieldLabel: 'Etiquette',
                      name:'EtiquetteComposite',
                      width:493,
                      items: [
                            {
                            xtype: 'textfield',
                            fieldLabel:'Etiquette',
                            name: 'etiquette',
                            id:'etiquette',
                            width:200
                        },
                         {
                          xtype:'button',
                          iconCls:'icon-rfid',
                          width:30,
                          id:'EtiquetteScan',
                          handler:function(grid, rowIndex, colIndex) {  
                                Ext.DomHelper.overwrite('iframediv', {
                                        tag: 'iframe',
                                        id:'appletIFrame',
                                        frameBorder: 0,
                                        width: 0,
                                        height: 0,
                                        css: 'display:none;visibility:hidden;height:0px;',
                                        src: '../applet/popupnfc.html?row=0&origine=Stock'
                                    });
                                    appletWin.show();
                                    appletWin.el.mask('Chargement, veuillez patienter...', 'x-mask-loading');


                            }
                         }
                    ]
                    },
                {
                    xtype: 'textfield',
                    fieldLabel:'CAS',
                    name: 'CAS',
                    id:'CAS',
                    width:75
                },{
                              name: 'idProduitConditionnement',
                              hidden:true
                     },{
                              name: 'idSousStructure',
                              hidden:true
                     }
            ]

        },
            {

            id:'panelMap',
            xtype:'panel',
            layout:'border',
            region: 'south',
            autoScroll:false,
            height:210,
            defaults: {frame:true},
            items:[
              {
                xtype: 'gmappanel',
                id:'myGmap',
                gmapType: google.maps.MapTypeId.ROADMAP,
                displayGeoErrors: true,
                minGeoAccuracy: 'GEOMETRIC_CENTER',
                region: 'center',
                zoomLevel: 17,
                mapConfOpts: ['enableScrollWheelZoom','enableDoubleClickZoom','enableDragging'],
                mapControls: ['GSmallMapControl','GMapTypeControl','NonExistantControl'],
                setCenter: {
                    geoCodeAddr: '3 rue des tanneurs, tours, france'
                    ,marker: {visible:false}
                }
               }

            ]}
],
        saveItem:function(){
                if (includePanel.getForm().isValid()){
                includePanel.getForm().submit({waitTitle:'Connexion au serveur', waitMsg:'Enregistrement en cours...',
                        success:function(f,action){
                           ds.reload();
                           includePanel.getForm().reset();
                            Ext.msg.show(this.title ? this.title : 'Op&eacute;ration r&eacute;ussie:', 'Enregistrement effectu&eacute; !');
                },
                        failure:function(f,action){
				var result = Ext.util.JSON.decode(action.response.responseText);
				Ext.Msg.alert('Erreur', result.errors.reason);
                        }
                });
        }
        },
        resetToNull: function() {
           var form = this.getForm();
        this.resetForm();

    },
    resetForm:function(){
        var form = this.getForm();
        form.items.each(function(f){
       if(f.getXType() == 'xcheckbox')
            f.originalValue = false;
        else
            f.originalValue = '';
        }, form);
        form.reset();
    },
    deleteItem: function(id) {
        if(id == null)
            var this_id = this.getForm().findField(this.gridP).getValue();
        else
            var this_id = id;
        if(this_id != '') {
            Ext.MessageBox.confirm('Etes-vous sur ?','Voulez-vous effacer cet enregistrement, le stock va être modifié&nbsp;?',function(btn) {
                if(btn == 'yes') {
                        Ext.getCmp("gridP").loadMask.show();
                        var formPart = Ext.getCmp("formPart");
                        formPart.el.mask('Chargement...', 'x-mask-loading');
                        var myFormParams = {
                            controller:this.controller,
                            action:'deleteItem',
                            id:this_id,
                            output:'json'
                        };
                        // adding extra params if necessary!
                        if (this.extraParams) {
                            for (key in this.extraParams) {
                                myFormParams[key] = this.extraParams[key];
                            }
                        }
                        Ext.Ajax.request({
                            params:myFormParams,
                            url:'FrontController',
                             success:function(response, options){
                                if (response.responseText) {
                                    if (response.responseText.substr(0,1) == '{') {
			    	        var ajaxResponse = Ext.util.JSON.decode(response.responseText);
				     }
	                        }
                                if(ajaxResponse && ajaxResponse.success) {
                                    includePanel.getForm().reset();
                                    ds.reload();
                                    Ext.msg.show(this.title ? this.title : 'Op&eacute;ration r&eacute;ussie:', 'Enregistrement effac&eacute; !');

                                }
                                formPart.el.unmask();
                            },
                            failure:function(response, options) {
				var result = Ext.util.JSON.decode(response.responseText);
				Ext.Msg.alert('Erreur', result.errors.reason);
                                ds.reload();
                                formPart.el.unmask();
                            }
                        });
                } else {
                  //do nothing
                }
            },this);
            return false;
        } else {
            Ext.Msg.alert('Erreur','Vous devez s&eacute;lectionner un enregistrement');
        }
    }

    });


function reset(){

    includePanel.resetToNull();

}

function deleteligne(id){
    includePanel.deleteItem(id);
}

function save(){
    includePanel.saveItem();
}

var mapwin;

function closeImgWindow(){
	if(mapwin){
		mapwin.hide();
		mapwin= null;
	}
}

function openImgWindow(imageName, imageId){

		if(!mapwin){
			mapwin = new Ext.Window({
				layout: 'fit',
				closeAction: 'hide',
				width:164,
				height:222,
                                x:10,
                                cls:'whitebck',
				items: {
					xtype:'box',
					showAnimDuration:1
					,anchor:''
                                        ,cls:'whitebck'
					,isFormField:true
					,fieldLabel:'Image'
					,autoEl:{
                                            tag:'div', children:[{
                                                     tag:'img'
                                                    ,src:imageName+'.jpg'
                                            }]
					}

				}
			});
		}
        mapwin.show();
}