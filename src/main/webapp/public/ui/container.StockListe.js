/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2012/01/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/

var myTimer;

Ext.QuickTips.init();

    Ext.form.Field.prototype.msgTarget = 'side';

    var bd = Ext.getBody();

// ----------------------------------------------------------
// ----------------------------------------------------------
// TABLEAU - GRID
// acces au données pour le tableau
    var proxy = new Ext.data.HttpProxy({
        url: 'FrontController',
        method: 'POST',
        timeout: 300000
    });

    var rec = Ext.data.Record.create([
            {name: 'CAS'},
            {name: 'idArticle'},
            {name: 'idProduitConditionnement'},
            {name: 'identifiantEtiquette'},
            {name: 'nomProduit'},
            {name: 'idSousStructure'},
            {name: 'nomSousStructure'},
            {name: 'idAire'},
            {name: 'nomAire'},
            {name: 'contenance', type: 'float'},
            {name: 'uniteMesure'},
            {name: 'uniteMesureLong'},
            {name: 'quantite', type: 'float'},
            {name: 'write'}
    ]);

    var reader = new Ext.data.JsonReader({
 	   root : "data"
    }, rec);


    var ds = new Ext.data.GroupingStore({
        proxy: proxy,
        fields: ['CAS','idArticle','idProduitConditionnement', 'identifiantEtiquette', 'nomProduit', 
            'idSousStructure','nomSousStructure', 'idAire', 'nomAire', 'contenance','uniteMesure','uniteMesureLong', 'quantite', 'write'],
        root:'data',
        totalProperty: 'totalCount',
        reader: reader,
        nocache: true,
        autoLoad: false,
        groupField:'nomProduit',
        remoteGroup:true,
        baseParams:{action:'getListItems', controller:'StockListe'},
        listeners : {
            load : function(store,records,options) {
                if(typeof(Storage)!==undefined){
                    var jsonData = Ext.encode(Ext.pluck(store.data.items, 'data'));
                    sessionStorage.listElement = jsonData;

                    if (Ext.getCmp("StockForm") != undefined){
                        sessionStorage.idSite = Ext.getCmp("StockForm").getForm().findField('idSite').getValue();
                        sessionStorage.idBatiment = Ext.getCmp("StockForm").getForm().findField('idBatiment').getValue();
                        sessionStorage.idSalleStockage = Ext.getCmp("StockForm").getForm().findField('idSalleStockage').getValue();
                        sessionStorage.idAire = Ext.getCmp("StockForm").getForm().findField('idAire').getValue();
                        sessionStorage.idProduitConditionnement = Ext.getCmp("StockForm").getForm().findField('idProduitConditionnement').getValue();
                        sessionStorage.nomSigne = Ext.getCmp("StockForm").getForm().findField('nomSigne').getValue();
                        sessionStorage.idSousStructure = Ext.getCmp("StockForm").getForm().findField('idSousStructure').getValue();
                        sessionStorage.valeurStock = Ext.getCmp("StockForm").getForm().findField('valeurStock').getValue();
                        var pictogrammes = Ext.getCmp("StockForm").getForm().findField('pictogrammes').getValue();
                        sessionStorage.etiquette = Ext.getCmp("StockForm").getForm().findField('etiquette').getValue();
                        sessionStorage.CAS = Ext.getCmp("StockForm").getForm().findField('CAS').getValue();
                        sessionStorage.autre = Ext.getCmp("StockForm").getForm().findField('autre').getValue();
                        var pictoString ="";
                        for (var i=0;i<pictogrammes.length;i++){
                            pictoString = pictoString + ',' + pictogrammes[i].getRawValue();
                        }
                        if (pictogrammes.length>0){
                            sessionStorage.pictoString = pictoString.substring(1);
                        }
                    }
                }
            }
        }
    });


if(typeof(Storage)!==undefined && sessionStorage.listElement !==undefined && sessionStorage.listElement !=="undefined"){
    records = sessionStorage.listElement;
    if (records.length>0){
        ds.removeAll();
        records = "{'totalCount':'1','data':" + records + "}";
        var json = eval("(" + records + ")");
        ds.loadData(json);
    }
}

    var colModel = new Ext.grid.ColumnModel([
        {id:'CAS',header: "CAS", width: 70, sortable: true, locked:false, dataIndex: 'CAS'},
        {dataIndex: 'idArticle', hidden:true},
        {header: "idProduitConditionnement", width: 80, sortable: true, locked:false, dataIndex: 'idProduitConditionnement', hidden:true},
        {id:'etiquette',header: "Code Etiquette", width: 100, sortable: true, locked:false, dataIndex: 'identifiantEtiquette', hidden:true},
        {id:'nomProduit',header: "Nom", width: 100, sortable: true, dataIndex: 'nomProduit'},
        {header: "idSousStructure", dataIndex: 'idSousStructure', hidden:true},
        {header: "Equipe/departement", width: 300, sortable: true, dataIndex: 'nomSousStructure'},
        {header: "idAire", dataIndex: 'idAire', hidden:true},
        {header: "Local de stockage", width: 180, sortable: true, dataIndex: 'nomAire'},
        {header: "Qte unitaire", summaryType: 'sum', width: 53, sortable: true, dataIndex: 'quantite'},
        {header: "Contenance", width: 57, sortable: true, dataIndex: 'contenance'},
        {header: "Total", width: 55, sortable: true, renderer: function(v, params, record){
                    return Ext.util.Format.number(record.data.quantite * record.data.contenance, '0.00');
                }},
        {header: "Unite", width: 40, sortable: true, dataIndex: 'uniteMesure'},
        {header: "Conso", width: 40, sortable: false, xtype:'actioncolumn',
        items: [{
            getClass: function(v, meta, rec) { 
                if (rec.get('write') == 'true'){
                    this.items[0].tooltip = 'Gérer les consommations de ce produit';
                    return 'conso-col';
                }
            },
            handler: function(grid, rowIndex, colIndex) {
                var rec = ds.getAt(rowIndex);
                var uri = "FrontController?controller=Consommation&action=getHtml&idArticle="+rec.get("idArticle")+"&idPro="+rec.get("idProduitConditionnement")+"&idAire="+rec.get("idAire")+"&idSs="+rec.get("idSousStructure");
                window.location.assign(uri);
            }
        }]}

    ]);



//    var PagingBar = new Ext.PagingToolbar({
//        pageSize: 9,
//        store: ds,
//        displayInfo: true,
//        action:'getListItems',
//        controller:'StockListe',
//        displayMsg: 'Stocks {0} - {1} of {2}',
//        emptyMsg: 'Aucun stock'
//    });
// FIN TABLEAU - GRID
// ----------------------------------------------------------


// ----------------------------------------------------------
//  Toolbar Tableau
     var exportBar = new Ext.Toolbar({
     cls: 'x-panel-header',
     height: 25,
     items: [  '->',
                { 
                    text: ' ',
                    iconCls: 'icon-excel',
                    handler : exportExcel
                }]

    });
// ---------------------------------------------------------- 


// ---------------------------------------------------------- 
// STORES

    // ComboBox site
    var cbsite = new Ext.data.JsonStore({
        id:cbsite,
        proxy: proxy,
        fields: ['idSite', 'nom'],
        root:'data',
        totalProperty: 'totalCount',
        nocache: true,
        autoLoad: true,
        baseParams:{action:'getListItems', controller:'Site'},
        listeners : {
        load : function() {
            var rec = new this.recordType({idSite:0, nom:'0 - Tous les Sites'});
            rec.commit();
            this.add(rec);
            this.sort('nom');
            Ext.getCmp('idSitec').setValue(0);
        }}
    });

    // ComboBox Batiment
    var cbbatiment = new Ext.data.JsonStore({
        id:cbbatiment,
        proxy: proxy,
        fields: ['idBatiment', 'nom', 'adresse1', 'adresse2', 'codePostal', 'ville', 'longitude','latitude'],
        root:'data',
        totalProperty: 'totalCount',
        nocache: true,
        autoLoad: true,
        baseParams:{action:'getListItems', controller:'Batiment'},
        listeners : {
        load : function() {
            var rec = new this.recordType({idBatiment:0, nom:'0 - Tous les Batiments'});
            rec.commit();
            this.add(rec);
            this.sort('nom');
            Ext.getCmp('idBatimentc').setValue(0);
        }}
    });
    cbbatiment.load({params:{idSite:-1}});
    
    var cbsalle = new Ext.data.JsonStore({
        id:cbsalle,
        proxy: proxy,
        fields: ['idSalleStockage', 'code', 'nom', 'adresse1', 'ville'],
        root:'data',
        totalProperty: 'totalCount',
        nocache: true,
        autoLoad: false,
        baseParams:{action:'getListItems', controller:'SalleStockage'}
        ,listeners : {
        load : function() {
            var rec = new this.recordType({idSalleStockage:0, nom:'0 - Toutes les Salles de Stockage'});
            rec.commit();
            this.add(rec);
            this.sort('nom');
            Ext.getCmp('idSallec').setValue(0);
            if(typeof(myMask) !== 'undefined')
                myMask.hide();
            
        }}
    });
    cbsalle.load({params:{idBatiment:-1}});    
    
    // ComboBox Aire
    var cbaire = new Ext.data.JsonStore({
        id:cbaire,
        proxy: proxy,
        fields: ['idAire', 'code', 'nom', 'refrigere'],
        root:'data',
        totalProperty: 'totalCount',
        nocache: true,
        autoLoad: false,
        baseParams:{action:'getListItems', controller:'AireStockage'}
        ,listeners : {
        load : function() {
            var rec = new this.recordType({idAire:0, nom:'0 - Toutes les aires de Stockage'});
            rec.commit();
            this.add(rec);
            this.sort('nom');
            Ext.getCmp('idAirec').setValue(0);
            if(typeof(myMask) !== 'undefined')
                myMask.hide();
            
        }}
    });
    cbaire.load({params:{idSalleStockage:-1}});    
    
    

    var signeData = [
        ["="],
        [">"],
        [">="],
        ["<"],
        ["<="],
    ];

    var cbsigne = new Ext.data.ArrayStore({
        fields: [
           {name: 'nomSigne'},
        ]
    });

    cbsigne.loadData(signeData);


// STORES
// ----------------------------------------------------------

    var readPicto= new Ext.data.JsonReader({root : "data",id : "id"},
    Ext.data.Record.create([{name: 'boxLabel'},{name: 'inputValue'},{name: 'name'},{name: 'id'}]));


var summary = new Ext.ux.grid.GroupSummary();

    var includePanel = new Ext.FormPanel({
        id: 'StockForm',
        controller:'LivraisonSimple',
        url: 'FrontController',
        frame: true,
        labelAlign: 'left',
        waitMsgTarget: true,
        width: 1021,
        //layout: 'border',
        height:645,
        baseParams:{action:'saveItem', controller:'LivraisonSimple'},
        items: [{
            id:'formPart',
            displayFloatingMsg:true,
            title:'Visualisation du Stock',
            split:true,
            margins: '0 5 5 5',
            columnWidth: 0.9,
            autoScroll:true,
            height:300,
            xtype: 'fieldset',
            loadMask:{msg: 'Chargement...'},            
            labelWidth: 190,
            autoHeight:true,
            collapsible: true,
            defaults: {border:false},    // Default config options for child items
            defaultType: 'textfield',
            labelSeparator:'',
            bodyStyle: Ext.isIE ? 'padding:0 0 0px 15px;' : 'padding:0px 15px;',
            border: true,
             listeners: {
                    collapse: function(panel) {
                        Ext.getCmp("gridP").setHeight(597);
                    },
                    expand: function(panel) {
                        Ext.getCmp("gridP").setHeight(285);
                    }
             },
             buttons: [{
                    text:'annuler',
                    handler: function(){
                        reset();
                        sessionStorage.listElement = undefined;
                        sessionStorage.idSite = undefined;
                        sessionStorage.idBatiment = undefined;
                        sessionStorage.idSalleStockage = undefined;
                        sessionStorage.idAire = undefined;
                        sessionStorage.idProduitConditionnement = undefined;
                        sessionStorage.nomSigne = undefined;
                        sessionStorage.idSousStructure = undefined;
                        sessionStorage.valeurStock = undefined;
                        sessionStorage.etiquette = undefined;
                        sessionStorage.CAS = undefined;
                        sessionStorage.autre = undefined;
                        sessionStorage.pictoString = undefined;
                        ds.removeAll();
                        //includePanel.getForm().findField('regroupeArticle').setValue(true);
                    }
                },{
                    text:'Rechercher',
                    handler: function(){
                        var idSite = includePanel.getForm().findField('idSite').getValue();
                        var idBatiment = includePanel.getForm().findField('idBatiment').getValue();
                        var idSalleStockage = includePanel.getForm().findField('idSalleStockage').getValue();
                        var idAire = includePanel.getForm().findField('idAire').getValue();
                        var idProduitConditionnement = includePanel.getForm().findField('idProduitConditionnement').getValue();
                        var nomSigne = includePanel.getForm().findField('nomSigne').getValue();
                        var idSousStructure = includePanel.getForm().findField('idSousStructure').getValue();
                        var valeurStock = includePanel.getForm().findField('valeurStock').getValue();
                        var pictogrammes = includePanel.getForm().findField('pictogrammes').getValue();
                        var etiquette = includePanel.getForm().findField('etiquette').getValue();
                        var CAS = includePanel.getForm().findField('CAS').getValue();
                        var autre = includePanel.getForm().findField('autre').getValue();
                        var pictoString ="";
                        for (var i=0;i<pictogrammes.length;i++){
                            pictoString = pictoString + ',' + pictogrammes[i].getRawValue();
                        }
                        if (pictogrammes.length>0){
                            pictoString = pictoString.substring(1);
                        }
                        //var regroupeArticle = includePanel.getForm().findField('regroupeArticle').getValue();

                        // si on ne fait pas le regroupement, on affiche la colonne etiquette
                        ds.removeAll();
//                        if (regroupeArticle==false){
//                            Ext.getCmp("gridP").getColumnModel().setHidden(2, false);
//                        }else{
//                            Ext.getCmp("gridP").getColumnModel().setHidden(2, true);
//                        }

                         ds.load({params:{
                                 idSite:idSite,
                                 idBatiment:idBatiment,
                                 idSalleStockage:idSalleStockage,
                                 idAire:idAire,
                                 idProduitConditionnement:idProduitConditionnement,
                                 idSousStructure:idSousStructure,
                                 nomSigne:nomSigne,
                                 valeurStock:valeurStock,
                                 pictogrammes:pictoString,
                                 //regroupeArticle:regroupeArticle,
                                 etiquette:etiquette,
                                 autre:autre,
                                 cas:CAS
                                  }});
                        //Ext.getCmp("formPart").collapse();

                    }
                }
                ],
            items: [
                    {
                        id:'idSitec',
                        xtype:'combo',
                        allowBlank: true,
                        blankText:'Le champ est requis',
                        fieldLabel: 'Site',
                        name: 'nomSite',
                        width:200,
                        store: cbsite,
                        root:'data',
                        hiddenName : 'idSite',
                        valueField:'idSite',
                        displayField:'nom',
                        typeAhead: true,
                        loadingText: 'Recherche...',
                        mode: 'local',
                        triggerAction: 'all',
                        emptyText:'Selectionner un element...',
                        selectOnFocus:true,
                        listeners:{
                             'select': function() {
                                 
                                var idsite = this.getValue();
                                if (idsite == 0 || idsite==''){
                                    cbbatiment.load({params:{idSite:-1}});
                                    cbaire.load({params:{idBatiment:-1}});
                                }else{
                                    cbbatiment.load({params:{idSite:idsite}});
                                }
                             }
                        }
                    },
                    {
                        id:'idBatimentc',
                        xtype:'combo',
                        allowBlank: true,
                        blankText:'Le champ est requis',
                        fieldLabel: 'Batiment',
                        name: 'nomBatiment',
                        width:300,
                        store: cbbatiment,
                        root:'data',
                        hiddenName : 'idBatiment',
                        valueField:'idBatiment',
                        displayField:'nom',
                        typeAhead: true,
                        loadingText: 'Recherche...',
                        mode: 'local',
                        triggerAction: 'all',
                        emptyText:'Selectionner un element...',
                        selectOnFocus:true,
                        listeners:{
                             'select': function() {
                                var idBatiment = this.getValue();
                                if (idBatiment == 0 || idBatiment==''){
                                    cbsalle.load({params:{idBatiment:-1}});
                                }else{
                                    cbsalle.load({params:{idBatiment:idBatiment}});
                                }

                             }
                         }
                       },
                    {
                        id:'idSallec',
                        xtype:'combo',
                        fieldLabel: 'Salle de Stockage',
                        name: 'nomSalleStockage',
                        width:300,
                        store: cbsalle,
                        root:'data',
                        hiddenName : 'idSalleStockage',
                        valueField:'idSalleStockage',
                        displayField:'nom',
                        typeAhead: true,
                        loadingText: 'Recherche...',
                        mode: 'local',
                        triggerAction: 'all',
                        emptyText:'Selectionner un element...',
                        selectOnFocus:true,
                        listeners:{
                             'select': function() {
                                var idSalle = this.getValue();
                                if (idSalle == 0 || idSalle==''){
                                    cbaire.load({params:{idSalleStockage:-1}});
                                }else{
                                    cbaire.load({params:{idSalleStockage:idSalle}});
                                }

                             }
                         }                        
                       },
                    {
                        id:'idAirec',
                        xtype:'combo',
                        fieldLabel: 'Aire de Stockage',
                        name: 'nomAire',
                        width:300,
                        store: cbaire,
                        root:'data',
                        hiddenName : 'idAire',
                        valueField:'idAire',
                        displayField:'nom',
                        typeAhead: true,
                        loadingText: 'Recherche...',
                        mode: 'local',
                        triggerAction: 'all',
                        emptyText:'Selectionner un element...',
                        selectOnFocus:true
                       }
                       ,
                    {
                      xtype: 'compositefield',
                      fieldLabel: 'Produit (Recherche 1 produit)',
                      width:324,
                      items: [
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Produit',
                            name: 'nomProduit',
                            readOnly:true,
                            width:200
                         },
                         {
                          xtype:'button',
                          text:'Selectionner un Produit',
                          windowName:'searchProduit',
                          handler:function(){
                              var mySearchWin = eval(this.windowName);
                              mySearchWin.show();
                          }
                         }
                    ]},{
                    xtype: 'textfield',
                    fieldLabel: 'Produit (Libelle/Description)',
                    name:'autre',
                    id:'autre',
                    width:200
                },
                    {
                      xtype: 'compositefield',
                      fieldLabel: 'Equipe/Département',
                      name:'equipeComposite',
                      width:493,
                      items: [
                        {
                            xtype: 'textfield',
                            name: 'nomSousStructure',
                            readOnly:true,
                            width:300
                         },
                         {
                          xtype:'button',
                          text:'Selectionner une Equipe/Département',
                          windowName:'searchSousStructure',
                          id:'searchSousStructure',
                          handler:function(){
                              var mySearchWin = eval(this.windowName);
                              mySearchWin.show();
                          }
                         }
                    ]},{
                      xtype: 'compositefield',
                      fieldLabel: 'Quantité en stock',
                      width:324,
                      items: [
                    {
                        xtype:'combo',
                        name: 'nomSigne',
                        width:50,
                        store: cbsigne,
                        root:'data',
                        hiddenName : 'nomSigne',
                        valueField:'nomSigne',
                        displayField:'nomSigne',
                        mode: 'local',
                        triggerAction: 'all',
                        value:'>',
                        selectOnFocus:true
                       },
                        {
                            xtype:'numberfield',
                            decimalPrecision:15,
                            name: 'valeurStock',
                            decimalSeparator:',',
                            value:'0',
                            width:50
                         }
                    ]},{
                        xtype:'remotecheckboxgroup',
                        fieldLabel: 'Classes de risque',
                        itemCls: 'x-check-group-alt',
                        columns: 10,
                        width:600,
                        name:'pictogrammes',
                        url: 'FrontController',
                        baseParams:{action:'getListItems', controller:'Pictogramme',path:'ui/images/picto/'},
                        reader:readPicto,
                        cbRenderer:function(){},
                        cbHandler:function(){},
                        items:[{boxLabel:'Loading'}]
//                      },{
//                        xtype:'checkbox',
//                        fieldLabel:'Regroupement',
//                        boxLabel: 'Regrouper les articles identiques présent dans un même lieu de stockage',
//                        name: 'regroupeArticle',
//                        inputValue:'true',
//                        checked:true
                     },{
                      xtype: 'compositefield',
                      fieldLabel: 'Etiquette',
                      name:'EtiquetteComposite',
                      width:493,
                      items: [
                            {
                            xtype: 'textfield',
                            fieldLabel:'Etiquette',
                            name: 'etiquette',
                            id:'etiquette',
                            width:200
                        },
                         {
                          xtype:'button',
                          iconCls:'icon-rfid',
                          width:30,
                          id:'EtiquetteScan',
                          handler:function(grid, rowIndex, colIndex) {  
                                Ext.DomHelper.overwrite('iframediv', {
                                        tag: 'iframe',
                                        id:'appletIFrame',
                                        frameBorder: 0,
                                        width: 0,
                                        height: 0,
                                        css: 'display:none;visibility:hidden;height:0px;',
                                        src: '../applet/popupnfc.html?row=0&origine=Stock'
                                    });
                                    appletWin.show();
                                    appletWin.el.mask('Chargement, veuillez patienter...', 'x-mask-loading');


                            }
                         }
                    ]
                },
                    {
                    xtype: 'textfield',
                    fieldLabel:'CAS',
                    name: 'CAS',
                    id:'CAS',
                    width:75
                    }
                    ,{
                              name: 'idProduitConditionnement',
                              hidden:true
                     },{
                              name: 'idSousStructure',
                              hidden:true
                     }
            ]

        },
        {
            xtype: 'grid',
            id:'gridP',
            margins: '5 0 5 5',
            region: 'south',
            plugins: summary,
            height:210,
            view: new Ext.grid.GroupingView({
                        forceFit:true,
                        showGroupName: false,
                        enableNoGroups: false,
			enableGroupingMenu: false,
                        hideGroupedColumn: true,
                        groupTextTpl: '{text} ({[values.rs.length]} {[values.rs.length > 1 ? "Items" : "Item"]})'
                    }),
            enableColumnHide:false,
            loadMask:{msg: 'Chargement...'},
            ds: ds,
            tbar:exportBar,
            cm: colModel,
            sm: new Ext.grid.RowSelectionModel({
                singleSelect: true
            }),
            autoExpandColumn: 'nomProduit',
            border: true
        }
],
        saveItem:function(){
                if (includePanel.getForm().isValid()){
                includePanel.getForm().submit({waitTitle:'Connexion au serveur', waitMsg:'Enregistrement en cours...',
                        success:function(f,action){
                           ds.reload();
                           includePanel.getForm().reset();
                            Ext.msg.show(this.title ? this.title : 'Op&eacute;ration r&eacute;ussie:', 'Enregistrement effectu&eacute; !');
                },
                        failure:function(f,action){
				var result = Ext.util.JSON.decode(action.response.responseText);
				Ext.Msg.alert('Erreur', result.errors.reason);
                        }
                });
        }
        },
        resetToNull: function() {
           var form = this.getForm();
        this.resetForm();
        Ext.getCmp("gridP").getSelectionModel().clearSelections();

    },
    resetForm:function(){
        var form = this.getForm();
        form.items.each(function(f){
       if(f.getXType() == 'xcheckbox')
            f.originalValue = false;
        else
            f.originalValue = '';
        }, form);
        form.reset();
    },
    deleteItem: function(id) {
        if(id == null)
            var this_id = this.getForm().findField(this.gridP).getValue();
        else
            var this_id = id;
        if(this_id != '') {
            Ext.MessageBox.confirm('Etes-vous sur ?','Voulez-vous effacer cet enregistrement, le stock va être modifié&nbsp;?',function(btn) {
                if(btn == 'yes') {
                        Ext.getCmp("gridP").loadMask.show();
                        var formPart = Ext.getCmp("formPart");
                        formPart.el.mask('Chargement...', 'x-mask-loading');
                        var myFormParams = {
                            controller:this.controller,
                            action:'deleteItem',
                            id:this_id,
                            output:'json'
                        };
                        // adding extra params if necessary!
                        if (this.extraParams) {
                            for (key in this.extraParams) {
                                myFormParams[key] = this.extraParams[key];
                            }
                        }
                        Ext.Ajax.request({
                            params:myFormParams,
                            url:'FrontController',
                             success:function(response, options){
                                if (response.responseText) {
                                    if (response.responseText.substr(0,1) == '{') {
			    	        var ajaxResponse = Ext.util.JSON.decode(response.responseText);
				     }
	                        }
                                if(ajaxResponse && ajaxResponse.success) {
                                    includePanel.getForm().reset();
                                    ds.reload();
                                    Ext.msg.show(this.title ? this.title : 'Op&eacute;ration r&eacute;ussie:', 'Enregistrement effac&eacute; !');

                                }
                                formPart.el.unmask();
                            },
                            failure:function(response, options) {
				var result = Ext.util.JSON.decode(response.responseText);
				Ext.Msg.alert('Erreur', result.errors.reason);
                                ds.reload();
                                formPart.el.unmask();
                            }
                        });
                } else {
                  //do nothing
                }
            },this);
            return false;
        } else {
            Ext.Msg.alert('Erreur','Vous devez s&eacute;lectionner un enregistrement');
        }
    }

    });



function reset(){

    includePanel.resetToNull();

}

function deleteligne(id){
    includePanel.deleteItem(id);
}

function save(){
    includePanel.saveItem();
}

function exportExcel(){
        Ext.MessageBox.show( {
            msg:'Création du document en cours',
            progressText:'Veuillez patienter...',
            width:300,
            wait:true,
            modal:true,
            waitConfig:{interval:200},
            style:'background:transparent url(images/download.gif) no-repeat top left;height:46px;'
            }
        );
        var x = function()
        	{
                if (sessionStorage.idSite !== undefined && sessionStorage.idSite !== "undefined"  && sessionStorage.idSite.length>0 && sessionStorage.idSite>0){
                    var idSite =  sessionStorage.idSite;
                }else{
                    var idSite = includePanel.getForm().findField('idSite').getValue();
                }

                if (sessionStorage.idBatiment !== undefined && sessionStorage.idBatiment !== "undefined"  && sessionStorage.idBatiment.length>0 && sessionStorage.idBatiment>0){
                    var idBatiment =  sessionStorage.idBatiment;
                }else{
                    var idBatiment = includePanel.getForm().findField('idBatiment').getValue();
                }

                if (sessionStorage.idAire !== undefined && sessionStorage.idAire !== "undefined"  && sessionStorage.idAire.length>0 && sessionStorage.idAire>0){
                    var idAire =  sessionStorage.idAire;
                }else{
                    var idAire = includePanel.getForm().findField('idAire').getValue();
                }

                if (sessionStorage.idProduitConditionnement !== undefined && sessionStorage.idProduitConditionnement !== "undefined"  && sessionStorage.idProduitConditionnement.length>0 && sessionStorage.idProduitConditionnement>0){
                    var idProduitConditionnement =  sessionStorage.idProduitConditionnement;
                }else{
                    var idProduitConditionnement = includePanel.getForm().findField('idProduitConditionnement').getValue();
                }

                if (sessionStorage.nomSigne !== undefined && sessionStorage.nomSigne !== "undefined"  && sessionStorage.nomSigne.length>0){
                    var nomSigne =  sessionStorage.nomSigne;
                }else{
                    var nomSigne = includePanel.getForm().findField('nomSigne').getValue();
                }

                if (sessionStorage.idSousStructure !== undefined && sessionStorage.idSousStructure !== "undefined"  && sessionStorage.idSousStructure.length>0 && sessionStorage.idSousStructure>0){
                    var idSousStructure =  sessionStorage.idSousStructure;
                }else{
                    var idSousStructure = includePanel.getForm().findField('idSousStructure').getValue();
                }

                if (sessionStorage.valeurStock !== undefined && sessionStorage.valeurStock !== "undefined"  && sessionStorage.valeurStock.length>0){
                    var valeurStock =  sessionStorage.valeurStock;
                }else{
                    var valeurStock = includePanel.getForm().findField('valeurStock').getValue();
                }

                if (sessionStorage.pictoString !== undefined && sessionStorage.pictoString !== "undefined"  && sessionStorage.pictoString.length>0){
                    var pictoString =  sessionStorage.pictoString;
                }else{
                    var pictogrammes = includePanel.getForm().findField('pictogrammes').getValue();
                    for (var i=0;i<pictogrammes.length;i++){
                        pictoString = pictoString + ',' + pictogrammes[i].getRawValue();
                    }
                    if (pictogrammes.length>0){
                        pictoString = pictoString.substring(1);
                    }
                }

                if (sessionStorage.etiquette !== undefined && sessionStorage.etiquette !== "undefined"  && sessionStorage.etiquette.length>0){
                    var etiquette =  sessionStorage.etiquette;
                }else{
                    var etiquette = includePanel.getForm().findField('etiquette').getValue();
                }

                if (sessionStorage.CAS !== undefined && sessionStorage.CAS !== "undefined"  && sessionStorage.CAS.length>0){
                    var CAS =  sessionStorage.CAS;
                }else{
                    var CAS = includePanel.getForm().findField('CAS').getValue();
                }

                if (sessionStorage.autre !== undefined && sessionStorage.autre !== "undefined"  && sessionStorage.autre.length>0){
                    var autre =  sessionStorage.autre;
                }else{
                    var autre = includePanel.getForm().findField('autre').getValue();
                }

                    Ext.Ajax.request({
                        method:'POST',
                        url: 'FrontController',
                        params: { action:'getXLS', controller:'StockListe', idSite:idSite,
                                 idBatiment:idBatiment,
                                 idAire:idAire,
                                 idProduitConditionnement:idProduitConditionnement,
                                 idSousStructure:idSousStructure,
                                 nomSigne:nomSigne,
                                 valeurStock:valeurStock,
                                 pictogrammes:pictoString,
                                 //regroupeArticle:regroupeArticle,
                                 etiquette:etiquette,
                                autre:autre,
                                cas:CAS
                                 },
                        failure: function(r, o) {
                            Ext.MessageBox.hide();
                        },
                        success: function(response, o) {

                            results = Ext.decode(response.responseText);
                            try {
                                Ext.destroy(Ext.get('downloadIframe'));
                            }
                            catch(e) {}
                            Ext.DomHelper.append(document.body, {
                                tag: 'iframe',
                                id:'downloadIframe',
                                frameBorder: 0,
                                width: 0,
                                height: 0,
                                css: 'display:none;visibility:hidden;height:0px;',
                                src: 'DownloadFile?fileName='+results.fileName
                            });
                            
                            Ext.MessageBox.hide();
                        }
                    });
        }
        x.defer(1000);
}


var mapwin;

function closeImgWindow(){
	if(mapwin){
		mapwin.hide();
		mapwin= null;
	}
}

function openImgWindow(imageName, imageId){

		if(!mapwin){
			mapwin = new Ext.Window({
				layout: 'fit',
				closeAction: 'hide',
				width:164,
				height:222,
                                x:10,
                                cls:'whitebck',
				items: {
					xtype:'box',
					showAnimDuration:1
					,anchor:''
                                        ,cls:'whitebck'
					,isFormField:true
					,fieldLabel:'Image'
					,autoEl:{
                                            tag:'div', children:[{
                                                     tag:'img'
                                                    ,src:imageName+'.jpg'
                                            }]
					}

				}
			});
		}
        mapwin.show();
}


