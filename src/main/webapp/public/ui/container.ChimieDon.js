/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2012/01/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/

var myTimer;



Ext.QuickTips.init();

    Ext.form.Field.prototype.msgTarget = 'side';

    var bd = Ext.getBody();
        
 // ---------------------------------------
 // GESTION DES DROITS
   Ext.Ajax.request({
        params:{
                controller:'ChimieDon',
                action:'getRight'
            },
         url:'FrontController',
         success:function(response, options){
 
         },
         failure:function(response, options) {  
                Ext.getCmp("btn-enreg").hide();
                Ext.getCmp("btn-new").hide();
         }
    }); 
 // --------------------------------------- 


var idAireFavorite = 0;
var libelleAireFavorite = "";

Ext.Ajax.request({
    url: 'FrontController',
    method: 'GET',
    success: function(response, conn, option){
        var ajaxResponse = Ext.util.JSON.decode(response.responseText);
        idAireFavorite = ajaxResponse.idAireStockage;
        libelleAireFavorite = ajaxResponse.libelleAireFavorite;
    },
    params: {action:'getAireStockageFavorite', controller:'Utilisateur'}
});

// ----------------------------------------------------------
// TABLEAU - GRID 
// acces au données pour le tableau
    var proxy = new Ext.data.HttpProxy({
        url: 'FrontController',
        method: 'POST'
    });
    


    var ds = new Ext.data.JsonStore({
        proxy: proxy,
        fields: ['idDon', 'idUtilisateur', 'emplacement', 'proprietaire', 'idArticle', 'articleNom', 'articleQte', 'dateFinDon',
            'dateDon','accepte', 'idSousStructure', 'nomSousStructure','idAire','nomAire'],
        root:'data',
        totalProperty: 'totalCount',
        reader: new Ext.data.JsonReader({root : "data",id : "idArticle"}, Ext.data.Record.create([{name: 'idDon'},{name: 'idUtilisateur'},{name: 'emplacement'},{name: 'proprietaire'},{name: 'idArticle'},
            {name: 'articleNom'},{name: 'articleQte'}, {name: 'dateFinDon'},
            {name: 'dateDon'},{name: 'accepte'},{name: 'idSousStructure'},{name: 'nomSousStructure'},{name: 'idAire'},{name: 'nomAire'}])),
        nocache: true,
        autoLoad: false,
        baseParams:{action:'getListItems', controller:'ChimieDon'}
        ,listeners:{
            load:function(){
                if(typeof(myMask) !== 'undefined')
                    myMask.hide();
            }
        }
    });
   ds.load({params:{start:0, limit:10}});


    var colModel = new Ext.grid.ColumnModel([
        {id:'idDon',header: "idDon", width: 80, sortable: true, locked:false, dataIndex: 'idDon', hidden:true},
        {id:'idUtilisateur',header: "idUtilisateur", width: 80, sortable: true, dataIndex: 'idUtilisateur', hidden:true},
        {id:'idArticle',header: "idArticle", width: 100, sortable: true, dataIndex: 'idArticle', hidden:true},
        {id:'emplacement',header: "Stockage", width: 350, sortable: true, dataIndex: 'emplacement'},
        {id:'proprietaire',header: "Structure", width: 200, sortable: true, dataIndex: 'proprietaire'},
        {id:'articleNom',header: "Article", width: 150, sortable: true, dataIndex: 'articleNom'},
        {id:'articleQte',header: "Qte", width: 50, sortable: true, dataIndex: 'articleQte'},
        {header: "dateFinDon", header: "date Fin", width: 80, sortable: true, dataIndex: 'dateFinDon'}
    ]);

    var PagingBar = new Ext.PagingToolbar({
        pageSize: 10,
        store: ds,
        displayInfo: true,
        action:'getListItems', 
        controller:'ChimieDon',
        displayMsg: 'Dons {0} - {1} sur {2}',
        emptyMsg: 'Aucun Don'
    });   


    
// FIN TABLEAU - GRID
// ----------------------------------------------------------   
    

// ---------------------------------------------------------- 
// ---------------------------------------------------------- 
//  Toolbar Tableau

     var formBbar = new Ext.Toolbar({
     height: 27,
     cls:'x-bbar',
         id:'bottom-bar',
     items: [                   '->', 
               {id:'btn-enreg',
                text: 'Enregistrer',
                iconCls: 'icon-save',
                handler : save
                }]

    });   
// ---------------------------------------------------------- 
    



    var includePanel = new Ext.FormPanel({
        id: 'ChimieDonForm',
        controller:'ChimieDon',
        url: 'FrontController',
        frame: true,    
        labelAlign: 'left',
        waitMsgTarget: true,
        width: 1021,
        layout: 'border',    // Specifies that the items will now be arranged in columns
        height:645,
        baseParams:{action:'saveItem', controller:'ChimieDon'},
        items: [{
            xtype: 'grid',
            id:'gridP',    	
            margins: '5 0 5 0',
            region: 'north',
            height:287,
            bbar: PagingBar , 
            enableColumnHide:false,
            loadMask:{msg: 'Chargement...'},
            ds: ds,
            cm: colModel,
            sm: new Ext.grid.RowSelectionModel({
                singleSelect: true,
                listeners: {
                    rowselect: function(sm, row, rec) {
                        var formPart = Ext.getCmp("formPart");
                        includePanel.getForm().reset();
                        formPart.el.mask('Chargement...', 'x-mask-loading');
                        Ext.getCmp("ChimieDonForm").getForm().loadRecord(rec);


                        formPart.el.unmask();
                    }
                }
            }),
            autoExpandColumn: 'emplacement',
             title:'Dons',
            border: true,
            
            plugins:[new Ext.ux.grid.Search({
                 mode:'remote'
                ,iconCls:'icon-zoom'
                ,autoFocus:true
                ,minLength:2
                ,position:'bottom'
                ,align:'left'
                ,width:200
                ,shortcutKey:113
            })]
        },{
            id:'formPart',
            displayFloatingMsg:true,     	
            margins: '0 0 0 0',        	
            columnWidth: 0.9,
            region: 'center',
            xtype: 'fieldset',
            cls:'x-fieldset-with-tbar',
            bbar:formBbar,                        
            loadMask:{msg: 'Chargement...'},
            labelWidth: 190,
            height:342,
            defaults: {border:false},    // Default config options for child items
            defaultType: 'textfield',
            labelSeparator:'',
            bodyStyle: Ext.isIE ? 'padding:0 0 5px 15px;' : 'padding:10px 15px;',
            border: true,
            items: [{   
                name: 'idDon',
                hidden:true
            },
            {
                xtype: 'textfield',
                name: 'proposition',
                hidden:true,
                value:'true'
            },
            {
                xtype: 'compositefield',
                fieldLabel: 'Equipe/Département *',
                width:593,
                items: [
                    {
                        xtype: 'textfield',
                        name: 'idSousStructure',
                        id:'idSousStructureID',
                        hidden:true
                    },
                {
                    xtype: 'textfield',
                    name: 'nomSousStructure',
                    allowBlank: false,
                    readOnly:true,
                    blankText:'Le champ est requis',
                    width:400
                },
                {
                    xtype:'button',
                    id:'btn_searchSousStructure',
                    text:'Selectionner une Equipe/Département',
                    windowName:'searchSousStructure',
                    handler:function(){
                        var mySearchWin = eval(this.windowName);
                        mySearchWin.show();
                    }
                }
            ]}
                ,{
                    xtype:'panel',
                    baseCls:'x-plain',
                    layout:'hbox',
                    width:900,
                    defaults:{
                        margins:'0 5 0 0',
                        pressed: false
                    },

                    items: [{cls:'x-form-item',   xtype:'label', text:'Aire de Stockage *', width:190},
                        {
                            xtype: 'textfield',
                            name: 'idAire',
                            id:'idAireID',
                            hidden:true
                        },{
                            xtype: 'textfield',
                            name: 'NomAireStockage',
                            id:'NomAireStockageID',
                            allowBlank: false,
                            readOnly:true,
                            width:300
                        },
                        {
                            xtype:'button',
                            id:'btn_allAires',
                            fieldLabel: 'Aire de stockage',
                            text:'Selectionner votre aire de stockage favorite',
                            handler:function(){
                                Ext.getCmp('idAireID').setValue(idAireFavorite);
                                Ext.getCmp('NomAireStockageID').setValue(libelleAireFavorite);

                            }},{

                            xtype:'button',
                            id:'btn_selAire',
                            fieldLabel: 'Aire de stockage',
                            text:'Selectionner une aire de stockage',
                            windowName:'searchAireStockage',
                            handler:function(){
                                    var mySearchWin = eval(this.windowName);
                                    mySearchWin.show();
                            }




                        }
                    ]

                }
            ]
            
        }],
        saveItem:function(){
                if (includePanel.getForm().isValid()){

                
                includePanel.getForm().submit({waitTitle:'Connexion au serveur', waitMsg:'Enregistrement en cours...',
                        success:function(f,action){
                           ds.reload();
                           includePanel.getForm().reset();                         
                            Ext.msg.show(this.title ? this.title : 'Op&eacute;ration r&eacute;ussie:', 'Enregistrement effectu&eacute; !');
                },
                        failure:function(f,action){
				var result = Ext.util.JSON.decode(action.response.responseText);
				Ext.Msg.alert('Erreur', result.errors.reason);   
                        }
                });
        }
        },
        resetToNull: function() {
           var form = this.getForm();
           if (form.isDirty()){
            Ext.MessageBox.show({
               buttons: Ext.Msg.OKCANCEL,
               title:'Etes-vous sur ?',
               msg:'Les données non enregistrées vont être effacées. Etes-vous sur ?',
               fn:function(btn) {
                   if(btn == 'ok') {
                       Ext.getCmp("SubstancesForm").resetForm();
                       Ext.getCmp("gridP").getSelectionModel().clearSelections();
                   }else{
                       return;
                   }
            }});
            return false;
            }

    },
    resetForm:function(){
        var form = this.getForm();
        form.items.each(function(f){			
       if(f.getXType() == 'xcheckbox')
            f.originalValue = false;
        else
            f.originalValue = '';
        }, form);
        form.reset();
    },   
    deleteItem: function(id) {
        if(id == null)
            var this_id = this.getForm().findField(this.gridP).getValue();
        else
            var this_id = id;
        if(this_id != '') {
            Ext.MessageBox.confirm('Etes-vous sur ?','Voulez-vous effacer cet enregistrement&nbsp;?',function(btn) {
                if(btn == 'yes') {
                        Ext.getCmp("gridP").loadMask.show();   
                        var formPart = Ext.getCmp("formPart");
                        formPart.el.mask('Chargement...', 'x-mask-loading');                     
                        var myFormParams = {
                            controller:this.controller,
                            action:'deleteItem',
                            id:this_id,
                            output:'json'
                        };
                        includePanel.getForm().findField('CASorigine').setValue('');
                        // adding extra params if necessary!
                        if (this.extraParams) {
                            for (key in this.extraParams) {
                                myFormParams[key] = this.extraParams[key]; 
                            }
                        }
                        Ext.Ajax.request({
                            params:myFormParams,
                            url:'FrontController',
                             success:function(response, options){
                                if (response.responseText) {						
                                    if (response.responseText.substr(0,1) == '{') {
			    	        var ajaxResponse = Ext.util.JSON.decode(response.responseText);
				     }
	                        }
                                if(ajaxResponse && ajaxResponse.success) {
                                    includePanel.getForm().reset();
                                    ds.reload();
                                    Ext.msg.show(this.title ? this.title : 'Op&eacute;ration r&eacute;ussie:', 'Enregistrement effac&eacute; !');

                                }
                                formPart.el.unmask(); 
                            },
                            failure:function(response, options) {  
				var result = Ext.util.JSON.decode(response.responseText);
				Ext.Msg.alert('Erreur', result.errors.reason); 
                                ds.reload();
                                formPart.el.unmask(); 
                            }
                        }); 
                } else {
                  //do nothing
                }
            },this);
            return false;           
        } else {
            Ext.Msg.alert('Erreur','Vous devez s&eacute;lectionner un enregistrement');
        }
    }
    
    });
    

function reset(){

    includePanel.resetToNull();

}

function deleteligne(id){
    includePanel.deleteItem(id);
}

function save(){
    includePanel.saveItem();
}

var mapwin;

function closeImgWindow(){
	if(mapwin){
		mapwin.hide();
		mapwin= null;
	}
}

function openImgWindow(imageName, imageId){

		if(!mapwin){
			mapwin = new Ext.Window({
				layout: 'fit',
				closeAction: 'hide',
				width:164,
				height:222,
                                x:10,
                                cls:'whitebck',
				items: {
					xtype:'box',
					showAnimDuration:1
					,anchor:''
                                        ,cls:'whitebck'
					,isFormField:true
					,fieldLabel:'Image'
					,autoEl:{
                                            tag:'div', children:[{
                                                     tag:'img'
                                                    ,src:imageName+'.jpg'
                                            }]
					}
					
				}
			});					
		}        
        mapwin.show();
}