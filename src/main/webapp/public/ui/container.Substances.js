/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2012/01/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/

var myTimer;

Ext.QuickTips.init();

    Ext.form.Field.prototype.msgTarget = 'side';

    var bd = Ext.getBody();
        
 // ---------------------------------------
 // GESTION DES DROITS
   Ext.Ajax.request({
        params:{
                controller:'Substance',
                action:'getRight'
            },
         url:'FrontController',
         success:function(response, options){
 
         },
         failure:function(response, options) {  
                Ext.getCmp("btn-enreg").hide();
                Ext.getCmp("btn-new").hide();
         }
    }); 
 // --------------------------------------- 


// ----------------------------------------------------------
// TABLEAU - GRID 
// acces au données pour le tableau
    var proxy = new Ext.data.HttpProxy({
        url: 'FrontController',
        method: 'POST'
    });
    
    var rec = Ext.data.Record.create([
            {name: 'CAS'},
            {name: 'EC'},
            {name: 'nom'},
            {name: 'formule'},
            {name: 'description'},
            {name: 'pictogrammes'},
            {name: 'write'}
    ]);

    var reader = new Ext.data.JsonReader({
 	   root : "data",
 	   id : "CAS"
    }, rec);


    var ds = new Ext.data.JsonStore({
        proxy: proxy,
        fields: ['CAS', 'EC', 'nom', 'formule', 'description', 'pictogrammes','write'],
        root:'data',
        totalProperty: 'totalCount',
        reader: reader,
        nocache: true,
        autoLoad: false,
        baseParams:{action:'getListItems', controller:'Substance'}
        ,listeners:{
            load:function(){      
                if(typeof(myMask) !== 'undefined')
                    myMask.hide();
            }
        }
    });
    ds.load({params:{start:0, limit:10}});


    var colModel = new Ext.grid.ColumnModel([
        {id:'CAS',header: "CAS", width: 80, sortable: true, locked:false, dataIndex: 'CAS'},
        {id:'EC',header: "EC", width: 80, sortable: true, dataIndex: 'EC'},
        {id:'nom',header: "Nom", width: 100, sortable: true, dataIndex: 'nom'},
        {header: "Formule", width: 100, sortable: true, dataIndex: 'formule'},
        {header: "Description", width: 200, sortable: true, dataIndex: 'description', hidden:true},
        {id:'pictogrammes',header: "pictogrammes", width: 200, sortable: true, dataIndex: 'pictogrammes', hidden:true},
        {header: "Supp", width: 40, sortable: false, xtype:'actioncolumn',
        items: [{
                    getClass: function(v, meta, rec) {          // Or return a class from a function
                        if (rec.get('write') == 'true'){
                            this.items[0].tooltip = 'Supprimer cette ligne';
                            return 'del-col';
                        }
                    },
                    handler: function(grid, rowIndex, colIndex) {
                        var rec = ds.getAt(rowIndex);
                        deleteligne(rec.get('CAS'));
                    }
                }]}
    ]);

    var PagingBar = new Ext.PagingToolbar({
        pageSize: 10,
        store: ds,
        displayInfo: true,
        action:'getListItems', 
        controller:'Substance',
        displayMsg: 'Substances {0} - {1} sur {2}',
        emptyMsg: 'Aucune Substance'
    });   
    
     PagingBar.insertButton(11, excelButton);

    
// FIN TABLEAU - GRID
// ----------------------------------------------------------   
    

// ---------------------------------------------------------- 
// ---------------------------------------------------------- 
//  Toolbar Tableau
    var formTbar = new Ext.Toolbar({
     height: 27,
     items: [{text: 'Nouvelle substance',
                id:'btn-new',
                iconCls: 'icon-add',
                handler : reset
               },'->',{
                id:'editxls',
                iconCls:'icon-excel',
                value: '',
                handler:function(){
                    var value = includePanel.getForm().findField('CAS').getValue();
                    if (value!=''){
                        printAction("Substance","getXLSList", value);
                    }else{
                        Ext.Msg.alert('Export impossible','Veuillez selectionner un item à éditer');
                        return;
                    }
                }            
            },{
                id:'edit',
                iconCls:'icon-print',
                value: '',
                handler:function(){
                    var value = includePanel.getForm().findField('CAS').getValue();
                    if (value!=''){
                        printAction("Substance","getPDF", value);
                    }else{
                        Ext.Msg.alert('Export impossible','Veuillez selectionner un item à éditer');
                        return;
                    }
                }            
            }]
    });
    
     var formBbar = new Ext.Toolbar({
     height: 27,
     cls:'x-bbar',
         id:'bottom-bar',
     items: [                   '->', 
               {id:'btn-enreg',
                text: 'Enregistrer',
                iconCls: 'icon-save',
                handler : save
                }]

    });   
// ---------------------------------------------------------- 
    

// ---------------------------------------------------------- 
// STORES
   

    var checkboxesstore = new Ext.data.JsonStore({
        //url: 'json.txt',
        proxy: proxy,
        root: 'data',
        baseParams:{action:'getListItems', controller:'Pictogramme',path:'ui/images/picto/'},
        fields: ['boxLabel','inputValue', 'name', 'id']
    });
    checkboxesstore.load({callback: function(records,o,s){

	var formPart = Ext.getCmp('formPart');
 
        // Turn array of records into array usable by CheckboxGroup items
        dataArray = new Array();
        Ext.each(records, function(rec)
        {
           dataArray.push({boxLabel: rec.get('boxLabel'), inputValue: rec.get('inputValue'), name: rec.get('name'), id: rec.get('id')});
        });

        var myCheckboxGroup = new Ext.form.CheckboxGroup({
                id:'myGroup',
                fieldLabel: 'Pictogrammes',
                itemCls: 'x-check-group-alt',
                // Put all controls in a single column with width 100%'
                columns: 10,
                width:500,
                items: dataArray
        });	


       //formPart.add(myCheckboxGroup);
       //formPart.doLayout();
		 
    }});

    var readPicto= new Ext.data.JsonReader({root : "data",id : "id"},
    Ext.data.Record.create([{name: 'boxLabel'},{name: 'inputValue'},{name: 'name'},{name: 'id'}]));


// FIN STORES
// ---------------------------------------------------------- 
    
    var vk = new Ext.ux.plugins.VirtualKeyboard();
    
    var includePanel = new Ext.FormPanel({
        id: 'SubstancesForm',
        controller:'Substance',
        url: 'FrontController',
        frame: true,    
        labelAlign: 'left',
        waitMsgTarget: true,
        width: 1021,
        layout: 'border',    // Specifies that the items will now be arranged in columns
        height:645,
        baseParams:{action:'saveItem', controller:'Substance'},
        items: [{
            xtype: 'grid',
            id:'gridP',    	
            margins: '5 0 5 0',
            region: 'north',
            height:287,
            bbar: PagingBar , 
            enableColumnHide:false,
            loadMask:{msg: 'Chargement...'},
            ds: ds,
            cm: colModel,
            sm: new Ext.grid.RowSelectionModel({
                singleSelect: true,
                listeners: {
                    rowselect: function(sm, row, rec) {
                        var formPart = Ext.getCmp("formPart");
                        includePanel.getForm().reset();
                        formPart.el.mask('Chargement...', 'x-mask-loading');
                        Ext.getCmp("SubstancesForm").getForm().loadRecord(rec);
                        includePanel.getForm().findField('CASorigine').setValue(rec.get('CAS'));
                        
                        // pour transformer les caractères html en caractères 
                        includePanel.getForm().findField('description').setValue(Encoder.htmlDecode(rec.get('description')));
                        includePanel.getForm().findField('nom').setValue(Encoder.htmlDecode(rec.get('nom')));
                        
                        formPart.el.unmask();
                    }
                }
            }),
            autoExpandColumn: 'nom',
             title:'Substances',
            border: true,
            
            plugins:[new Ext.ux.grid.Search({
                 mode:'remote'
                ,iconCls:'icon-zoom'
                ,autoFocus:true
                ,minLength:2
                ,position:'bottom'
                ,align:'left'
                ,width:200
                ,shortcutKey:113
                ,disableIndexes:['pictogrammes']
            })]
        },{
            id:'formPart',
            displayFloatingMsg:true,     	
            margins: '0 0 0 0',        	
            columnWidth: 0.9,
            region: 'center',
            xtype: 'fieldset',
            cls:'x-fieldset-with-tbar',            
            tbar:formTbar,            
            bbar:formBbar,                        
            loadMask:{msg: 'Chargement...'},
            labelWidth: 190,
            height:342,
            defaults: {border:false},    // Default config options for child items
            defaultType: 'textfield',
            labelSeparator:'',
            bodyStyle: Ext.isIE ? 'padding:0 0 5px 15px;' : 'padding:10px 15px;',
            border: true,
            items: [{   
                name: 'CASorigine',
                hidden:true
            },
            {xtype: 'compositefield',
                    fieldLabel: 'CAS *',
                    width:348,
                    items: [
                        {   
                            xtype: 'textfield',
                            fieldLabel: 'CAS',
                            name: 'CAS',
                            allowBlank: false,               
                            maxLength:15,
                            blankText:'Le champ est requis',   
                            maxLengthText:'Longueur maxi : 15',
                            width:100               
                        }
                        ,
                         {
                          xtype:'button',
                          text:'Recherche ESIS',
                          
                       
                          handler:function(){

                                var lCAS = includePanel.getForm().findField('CAS').getValue();
                                if (lCAS == ""){
                                    Ext.Msg.alert('Erreur', "Veuillez saisir un CAS"); 
                                }else{
                              
                              
                                            Ext.getCmp("gridP").loadMask.show();   
                                            var formPart = Ext.getCmp("formPart");
                                            formPart.el.mask('Chargement...', 'x-mask-loading');                     
                                            var myFormParams = {
                                                action:'ESISsearch',
                                                controller:'Substance',
                                                CAS:includePanel.getForm().findField('CAS').getValue(),
                                                output:'json'
                                            };
                                            Ext.Ajax.request({
                                                params:myFormParams,
                                                url:'FrontController',
                                                 success:function(response, options){
                                                    if (response.responseText) {						
                                                        if (response.responseText.substr(0,1) == '{') {
                                                            var ajaxResponse = Ext.util.JSON.decode(response.responseText);
                                                         }
                                                    }
                                                    if(ajaxResponse && ajaxResponse.totalCount==1) {
                                                        includePanel.getForm().reset();
                                                        Ext.getCmp("gridP").getSelectionModel().clearSelections();
                                                        ds.reload();
                                                        includePanel.getForm().findField('CAS').setValue(ajaxResponse.data[0].CAS);
                                                        includePanel.getForm().findField('EC').setValue(ajaxResponse.data[0].EC);
                                                        includePanel.getForm().findField('nom').setValue(Encoder.htmlDecode(ajaxResponse.data[0].nom));
                                                        includePanel.getForm().findField('formule').setValue(ajaxResponse.data[0].formule);
                                                        includePanel.getForm().findField('description').setValue(Encoder.htmlDecode(ajaxResponse.data[0].description));
                                                        includePanel.getForm().findField('pictogrammes').setValue(ajaxResponse.data[0].pictogrammes);
                                                    }else{
                                                        includePanel.getForm().reset();
                                                        Ext.Msg.alert('Erreur', "Impossible de trouver cette substance"); 
                                                    }
                                                    formPart.el.unmask(); 
                                                    Ext.getCmp("gridP").loadMask.hide();
                                                },
                                                failure:function(response, options) {  
                                                    Ext.Msg.alert('Erreur', "Impossible de trouver cette substance"); 
                                                    ds.reload();
                                                    formPart.el.unmask(); 
                                                    Ext.getCmp("gridP").loadMask.hide();
                                                }
                                            }); 
                              
                                }
                              
                          } 
                         }
            ]},{   
                fieldLabel: 'EC',
                name: 'EC',            
                width:100,
                maxLength:10,
                maxLengthText:'Longueur maxi : 10' 
            },{   
                fieldLabel: 'Nom *',
                name: 'nom',
                allowBlank: false,
                blankText:'Le champ est requis',                
                width:300,
                maxLength:255,
                maxLengthText:'Longueur maxi : 255',
                keyboardConfig: {
                        language: 'Greek',
                        showIcon: true
                },
                plugins: vk
            },{            	
            	fieldLabel: 'Formule',
                name: 'formule',
                allowBlank: true,
                width:300,
                maxLength:45,
                maxLengthText:'Longueur maxi : 45' 
            },{     
                xtype:'textarea',
            	fieldLabel: 'Description',
                name: 'description',
                allowBlank: true,
                width:300,
                keyboardConfig: {
                        language: 'Greek',
                        showIcon: true
                },
                plugins: vk
            },{
                xtype:'remotecheckboxgroup',
                fieldLabel: 'Pictogrammes',
                itemCls: 'x-check-group-alt',
                columns: 10,
                width:600,
                name:'pictogrammes',
                url: 'FrontController',
                baseParams:{action:'getListItems', controller:'Pictogramme',path:'ui/images/picto/'},
                reader:readPicto,
                cbRenderer:function(){},
                cbHandler:function(){},                        
                items:[{boxLabel:'Loading'}]
              }  
            ]
            
        }],
        saveItem:function(){
                if (includePanel.getForm().isValid()){
                    
                // pour transformer les caractères html en caractères 
                includePanel.getForm().findField('description').setValue(Encoder.htmlEncode(includePanel.getForm().findField('description').getValue()));
                includePanel.getForm().findField('nom').setValue(Encoder.htmlEncode(includePanel.getForm().findField('nom').getValue()));                
                
                includePanel.getForm().submit({waitTitle:'Connexion au serveur', waitMsg:'Enregistrement en cours...',
                        success:function(f,action){
                           ds.reload();
                           includePanel.getForm().reset();                         
                            Ext.msg.show(this.title ? this.title : 'Op&eacute;ration r&eacute;ussie:', 'Enregistrement effectu&eacute; !');
                },
                        failure:function(f,action){
				var result = Ext.util.JSON.decode(action.response.responseText);
				Ext.Msg.alert('Erreur', result.errors.reason);   
                        }
                });
        }
        },
        resetToNull: function() {
           var form = this.getForm();
           if (form.isDirty()){
            Ext.MessageBox.show({
               buttons: Ext.Msg.OKCANCEL,
               title:'Etes-vous sur ?',
               msg:'Les données non enregistrées vont être effacées. Etes-vous sur ?',
               fn:function(btn) {
                   if(btn == 'ok') {
                       Ext.getCmp("SubstancesForm").resetForm();
                       Ext.getCmp("gridP").getSelectionModel().clearSelections();
                   }else{
                       return;
                   }
            }});
            return false;
            }

    },
    resetForm:function(){
        var form = this.getForm();
        form.items.each(function(f){			
       if(f.getXType() == 'xcheckbox')
            f.originalValue = false;
        else
            f.originalValue = '';
        }, form);
        form.reset();
    },   
    deleteItem: function(id) {
        if(id == null)
            var this_id = this.getForm().findField(this.gridP).getValue();
        else
            var this_id = id;
        if(this_id != '') {
            Ext.MessageBox.confirm('Etes-vous sur ?','Voulez-vous effacer cet enregistrement&nbsp;?',function(btn) {
                if(btn == 'yes') {
                        Ext.getCmp("gridP").loadMask.show();   
                        var formPart = Ext.getCmp("formPart");
                        formPart.el.mask('Chargement...', 'x-mask-loading');                     
                        var myFormParams = {
                            controller:this.controller,
                            action:'deleteItem',
                            id:this_id,
                            output:'json'
                        };
                        includePanel.getForm().findField('CASorigine').setValue('');
                        // adding extra params if necessary!
                        if (this.extraParams) {
                            for (key in this.extraParams) {
                                myFormParams[key] = this.extraParams[key]; 
                            }
                        }
                        Ext.Ajax.request({
                            params:myFormParams,
                            url:'FrontController',
                             success:function(response, options){
                                if (response.responseText) {						
                                    if (response.responseText.substr(0,1) == '{') {
			    	        var ajaxResponse = Ext.util.JSON.decode(response.responseText);
				     }
	                        }
                                if(ajaxResponse && ajaxResponse.success) {
                                    includePanel.getForm().reset();
                                    ds.reload();
                                    Ext.msg.show(this.title ? this.title : 'Op&eacute;ration r&eacute;ussie:', 'Enregistrement effac&eacute; !');

                                }
                                formPart.el.unmask(); 
                            },
                            failure:function(response, options) {  
				var result = Ext.util.JSON.decode(response.responseText);
				Ext.Msg.alert('Erreur', result.errors.reason); 
                                ds.reload();
                                formPart.el.unmask(); 
                            }
                        }); 
                } else {
                  //do nothing
                }
            },this);
            return false;           
        } else {
            Ext.Msg.alert('Erreur','Vous devez s&eacute;lectionner un enregistrement');
        }
    }
    
    });
    

function reset(){

    includePanel.resetToNull();

}

function deleteligne(id){
    includePanel.deleteItem(id);
}

function save(){
    includePanel.saveItem();
}

var mapwin;

function closeImgWindow(){
	if(mapwin){
		mapwin.hide();
		mapwin= null;
	}
}

function openImgWindow(imageName, imageId){

		if(!mapwin){
			mapwin = new Ext.Window({
				layout: 'fit',
				closeAction: 'hide',
				width:164,
				height:222,
                                x:10,
                                cls:'whitebck',
				items: {
					xtype:'box',
					showAnimDuration:1
					,anchor:''
                                        ,cls:'whitebck'
					,isFormField:true
					,fieldLabel:'Image'
					,autoEl:{
                                            tag:'div', children:[{
                                                     tag:'img'
                                                    ,src:imageName+'.jpg'
                                            }]
					}
					
				}
			});					
		}        
        mapwin.show();
}