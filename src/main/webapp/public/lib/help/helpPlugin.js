Ext.namespace('Ext.ux.plugin');

Ext.ux.plugin.triggerfieldTooltip = function(config){
    Ext.apply(this, config);
};

    var proxys = new Ext.data.HttpProxy({
        url: 'FrontController',
        method: 'POST'
    });




Ext.extend(Ext.ux.plugin.triggerfieldTooltip, Ext.util.Observable,{
    init: function(component){
        this.component = component;
        this.component.on('render', this.onRender, this);
    },

    //private
    onRender: function(){

        // je récupere le pere du DIV
        var father = this.component.getEl().dom.parentNode;
        // j'instancie un nouvel Element Ext représentant le parent du DIV
        var tt = new Ext.Element(father);

        // je créé un Span avec un espace
        var p = tt.createChild({
            tag:'span',
            html:'&nbsp;'
        });

        // je créé une image dans le span
        var img = p.createChild({
            tag:'img',
            src:'ui/images/information.png',
            cls:'aideContextuelle'

        });

        // je créé un tooltip sur l'icone
        if(this.component.tooltip){
            this.component.tooltip.title='Aide';
                Ext.QuickTips.register(Ext.apply({
                      target: img.id
                }, this.component.tooltip));
        }


    }
});  