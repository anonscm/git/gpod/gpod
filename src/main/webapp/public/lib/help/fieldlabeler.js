Ext.namespace('Ext.ux.plugin');

Ext.ux.plugin.triggerfieldTooltip = function(config){
    Ext.apply(this, config);
};

Ext.extend(Ext.ux.plugin.triggerfieldTooltip, Ext.util.Observable,{
    init: function(component){
        this.component = component;
        this.component.on('render', this.onRender, this);
    },

    //private
    onRender: function(){

        var elId = this.component.getEl().id;
        var father = this.component.getEl().dom.parentNode;
        var tt = new Ext.Element(father);

        var p = tt.createChild({
            tag:'span',
            html:'&nbsp;'
        });

        var img = p.createChild({
            tag:'img',
            src:'ui/images/information.png',
            cls:'aideContextuelle'
            
        });
            

        if(this.component.tooltip){
                Ext.QuickTips.register(Ext.apply({
                      target: img.id
                }, this.component.tooltip));
        }
    }
});  