/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2012/01/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/
var pagingBarWithPeriodButton = Ext.extend(Ext.PagingToolbar, {
  moveFirst: function() {
    ds.load({params:{start:0, limit:10, period:periodChoosen}});
  },
  moveNext: function() {
    var newCusor =   this.cursor + this.pageSize;  
    ds.load({params:{start:newCusor, limit:10, period:periodChoosen}});
  },  
  movePrevious:function() {
    var newCusor =   this.cursor - this.pageSize;  
    ds.load({params:{start:newCusor, limit:10, period:periodChoosen}});
  },
  moveLast: function() {
    var nbItem = this.store.getTotalCount();
    var newCusor =   (Math.floor(nbItem / this.pageSize))*10;  
    ds.load({params:{start:newCusor, limit:10, period:periodChoosen}});
  },  
  doRefresh:function() {
    ds.load({params:{start:this.cursor, limit:10, period:periodChoosen}});
  },
  changePage :function(page) {
    ds.load({params:{start:page, limit:10, period:periodChoosen}});
  }

});
    
var periodChoosen = 1;
var periodButtons = 
[
        "-"
        ,{
            xtype:'buttongroup',
            items: [{
                id:'pagb1',
                pressed: true,
                enableToggle:true,
                text: '1 mois',
                toggleGroup:'period',
                handler:function(){
                    if (this.pressed){
                        periodChoosen=1;
                        ds.load({params:{start:0, limit:10, period:1}});
                    }
                }
                
            },{
                id:'pagb2',
                enableToggle:true,
                toggleGroup:'period',
                text: '6 mois',
                handler:function(){
                    if (this.pressed){
                        periodChoosen=6;
                        ds.load({params:{start:0, limit:10, period:6}});
                    }
                }
            },{
                id:'pagb3',
                enableToggle:true,
                toggleGroup:'period',
                text: '1 an',
                handler:function(){
                    if (this.pressed){
                        periodChoosen=12;
                        ds.load({params:{start:0, limit:10, period:12}});
                    }
                }
            },{
                id:'pagb4',
                enableToggle:true,
                toggleGroup:'period',
                text: 'toutes' ,
                handler:function(){
                    if (this.pressed){
                        periodChoosen=-1;
                        ds.load({params:{start:0, limit:10, period:-1}});
                    }
                }               
            }]
        }];