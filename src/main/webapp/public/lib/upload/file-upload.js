/*!
 * Ext JS Library 3.3.1
 * Copyright(c) 2006-2010 Sencha Inc.
 * licensing@sencha.com
 * http://www.sencha.com/license
 */

Ext.onReady(function(){

    Ext.QuickTips.init();


    var proxyAire = new Ext.data.HttpProxy({
        url: 'FrontController',
        method: 'POST'
    });

    var cbairePref = new Ext.data.JsonStore({
        id:cbairePref,
        proxy: proxyAire,
        fields: [
            {name: 'idAire'},
            {name: 'nomLong', type: 'string'}
        ],
        root:'data',
        totalProperty: 'totalCount',
        nocache: true,
        autoLoad: true,
        sortInfo: {
            field: 'nomLong',
            direction: 'ASC' // or 'DESC' (case sensitive for local sorting)
        },
        baseParams:{action:'getListItems', controller:'AireStockage'}
        ,listeners:{
            load:function(){
                Ext.Ajax.request({
                    url: 'FrontController',
                    method: 'GET',
                    success: function(response, conn, option){
                        var ajaxResponse = Ext.util.JSON.decode(response.responseText);
                        var idAireF = ajaxResponse.idAireStockage;


                        var record = Ext.getCmp('idAire').getStore().getAt(idAireF);
                        Ext.getCmp('idAire').setValue(Ext.getCmp('idAire').getStore().indexOf(record));
                    },
                    params: {action:'getAireStockageFavorite', controller:'Utilisateur'}
                });
            }
        }
    });

    var msg = function(title, msg){
        Ext.Msg.show({
            title: title,
            msg: msg,
            minWidth: 200,
            modal: true,
            icon: Ext.Msg.INFO,
            buttons: Ext.Msg.OK
        });
    };

    fp = new Ext.FormPanel({
        renderTo: 'fi-form',
        fileUpload: true,
        width: 1050,
        frame: true,
        title: 'File Upload Form',
        //autoHeight: true,
        height: 500,
        bodyStyle: 'padding: 10px 10px 0 10px;',
        labelWidth: 150,
        defaults: {
            anchor: '95%',
            allowBlank: false,
            msgTarget: 'side'
        },
        items: [{
            xtype: 'compositefield',
            fieldLabel: 'Equipe/Département *',
            name:'equipeComposite',
            width:493,
            items: [
                {
                    xtype: 'textfield',
                    fieldLabel: 'Equipe/Département',
                    name: 'nomSousStructure',
                    allowBlank: false,
                    readOnly:true,
                    blankText:'Le champ est requis',
                    width:300
                },
                {
                    xtype:'button',
                    text:'Selectionner une Equipe/Département',
                    windowName:'searchSousStructure',
                    id:'searchSousStructure',
                    handler:function(){
                        var mySearchWin = eval(this.windowName);
                        mySearchWin.show();
                    }
                }
            ]},
            {
                xtype: 'textfield',
                name: 'idSousStructure',
                hidden:true
            },
            {
            id:'idAire',
            xtype:'combo',
            fieldLabel: 'Aire de Stockage *',
            name: 'nomAire',
            width:600,
            store: cbairePref,
            root:'data',
            hiddenName : 'idAire',
            valueField:'idAire',
            displayField:'nomLong',
            typeAhead: true,
            allowBlank: false,
            loadingText: 'Recherche...',
            mode: 'local',
            triggerAction: 'all',
            emptyText:'Selectionner un element...',
            selectOnFocus:true
        },{
            xtype: 'fileuploadfield',
            id: 'form-file',
            emptyText: 'Selectionner un fichier excel',
            fieldLabel: 'Fichier Excel *',
            name: 'excel-path',
            buttonText: '',
            buttonCfg: {
                iconCls: 'upload-icon'
            }
        }],
        buttons: [{
            text: 'Save',
            handler: function(){
                if(fp.getForm().isValid()){
	                fp.getForm().submit({
	                    url: 'FileTransfert',
	                    waitMsg: 'Chargement du fichier...',
	                    success: function(fp, o){
	                        msg('Success', 'Le fichier "'+o.result.file+'" a été traité');
	                    },
                        failure:function(f,action){
                            var result = Ext.util.JSON.decode(action.response.responseText);
                            Ext.Msg.alert('Erreur', result.errors.reason);
                        }
	                });
                }
            }
        },{
            text: 'Reset',
            handler: function(){
                fp.getForm().reset();
            }
        }]
    });

});