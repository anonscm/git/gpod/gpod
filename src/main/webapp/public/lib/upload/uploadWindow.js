//,{    
//                            xtype: 'compositefield',
//                            labelWidth: 120,
//                            items: [
//
//                                {
//                                    id: 'fds',
//                                    fieldLabel: 'Upload fiche de sécurité',
//                                    xtype:'textfield',
//                                    emptyText:'Selectionner une fiche de sécurité',
//                                    name: 'fds',
//                                    width:493,
//                                    maxLength:255,
//                                    maxLengthText:'Longueur maxi : 255' 
//                                },
//                                {
//                                    xtype: 'fileuploadfield',
//                                    width:2,
//                                    id: 'form-file',
//                                    buttonCfg: {
//                                        iconCls: 'icon-upload'
//                                    },
//                                    buttonText: '',
//                                    buttonOnly: true,
//                                            listeners: {
//                                                'fileselected': function(fb, v){
//                                                    var el = Ext.getCmp('fds').getEl();
//                                                    includePanel.getForm().findField('fds').setValue(v);
//                                                    el.highlight();
//                                                    
//                                                }
//                                            }
//                                },
//                                {
//                                      xtype:'button',
//                                      tooltip :'Voir la fiche dans une nouvelle fenetre',
//                                      iconCls: 'icon-add',
//                                      handler: function() {
//                                            var win = new Ext.Window({
//                                                    title: 'Select PDF file to upload',
//                                                    layout:'fit',
//                                                    width:400,
//                                                    height:130,
//                                                    closable: true,
//                                                    resizable: true,
//                                                    plain: false,
//                                                    border: false,
//                                                    modal: true,
//                                                    bodyStyle: 'padding: 3px;',
//
//                                                    // create the upload form and render to the main div
//                                                    items: logForm = new N2MarketingCommunicator.UploadFile({
//                                                            renderTo: 'centerPanel'
//                                                    })
//                                            });
//                                            win.show();
//                                    
//
//                                      }
//                                },
//                                {
//                                      xtype:'button',
//                                      tooltip :'Voir la fiche dans une nouvelle fenetre',
//                                      iconCls: 'icon-delete'
//                                }
//                            ]
//                        }

Ext.namespace('N2MarketingCommunicator');


N2MarketingCommunicator.UploadFile = Ext.extend(Ext.form.FormPanel, {
    // Properties
    id: 'TMC_uploadFile',
    fileUpload: true,
    width: 320,
    border: false,
    frame: true,    
    autoheight:true,
    bodyStyle: 'padding: 10px 10px 0 10px;',
    labelWidth: 60,
    defaults: {
        anchor: '95%',
        msgTarget: 'side'
    },
	
	// Overriden methods
    initComponent: function() {
        // Stuff in this. are CONFIGS (read-write)
       	this.items = [{
            xtype: 'fileuploadfield',
            id: 'Filename',
            emptyText: 'Select a PDF file',
            fieldLabel: 'Filename',
            name: 'Filename',
            buttonCfg: {
                text: '',
                iconCls: 'icon-upload'
            }
        }],
        
        this.buttons = [{
            text: 'Close',
            handler: this.close,
            id: 'btnEdit_close',
            scope: this
        },{
            text: 'Upload',
            id: 'btnEdit_upload',
            handler: this.handleUpload,
            scope: this
        }],
        
        N2MarketingCommunicator.UploadFile.superclass.initComponent.call(this);
        
        // event Names are ALWAYS lowercase
        this.addEvents('close');
        
        this.onFailure = function(frm,act){
			if (act.result.error){
				Ext.Msg.alert('Error: ',act.result.error);
			}else{
				console.log(act.result);
				console.log(act.failureType);	
			}
        };
        
        // Stuff in this. are PROPERTIES (read-only)
    },
    
    // Custom Methods
    close: function() {
        //this.hide();
        this.ownerCt.close();
    },
    
    handleUpload: function(){
        if(this.getForm().isValid()){
            this.getForm().submit({
                url: 'FrontController',
                waitMsg: 'Uploading your Document...',
                success: function(fp, o){
                    Ext.MessageBox.alert('Success','Successfully uploaded "' + o.result.fileName + '".');
                    // update the field in the parent form
                    var oFilename = Ext.getCmp('edit_fileName');
                    oFilename.setValue(o.result.fileName);
                },
				failure: function(fp, o){							 
					Ext.MessageBox.alert('Warning','Error uploading file: ' + o.result.error);    
				} 
            });
        }
    }
});

Ext.reg('uploadFile', N2MarketingCommunicator.UploadFile);