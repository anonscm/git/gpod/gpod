/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2012/01/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/

gPodVersion = "1.0.5";



var windowApropos = new Ext.Window({
    width:'460',
    height:'160',
    closeAction:'hide',
    html:"<div><img src='ui/images/logo_gPOD_1.png' width=118px height=89px style='float:left; margin:10px 10px 0 10px'></div>\n\
          <p style='font-size:16px'>&nbsp;<b>G</b>estion des <b>P</b>r<b>O</b>duits <b>D</b>angereux - gPod</p></br>\n\
          &nbsp;Université François Rabelais de Tours. 2012 - 2013\n\
          </br></br>\n\
          &nbsp;Contact : <a href='mailto:gpod@univ-tours.fr'>Geoffroy VIBRAC</a></br></br>\n\
          <p style='font-size:14px'>&nbsp;Version : " + gPodVersion + "</p>"
});


var windowFeedBack = new Ext.Window({
    width:'520',
    height:340,
    closeAction:'hide',
    layout:'border',
    items:[
        {
        id:'formFeedBack',
        xtype:'fieldset',    
        region:'center',     
        displayFloatingMsg:true,      	
        margins: '0 0 0 0',
        columnWidth: 0.9,     
        loadMask:{msg: 'Chargement...'},
        labelWidth: 100,          
        defaults: {border:false},    // Default config options for child items
        defaultType: 'textfield',
        labelSeparator:'',
        bodyStyle: Ext.isIE ? 'padding:0 0 5px 15px;' : 'padding:10px 15px;',
        border: true,     
        cls:'x-fieldset-with-tbar', 
        bbar:new Ext.Toolbar({
            height: 27,
            cls:'x-bbar',
            items: [ 
                '->','-',
                {
                    text:'Envoyer',
                    handler:function(){
                        Ext.getCmp('formFeedBack').el.mask('Chargement...', 'x-mask-loading');
                        
                        Ext.Ajax.request({
                            method:'POST',
                            url:'FrontController',
                            params: { controller:'Mail', action: 'sendFeedBack', 
                                subject:Ext.getCmp('feedBackSubject').getValue(), msg:Ext.getCmp('feedBackMsg').getValue(),
                                version:Ext.getCmp('feedBackVersion').getValue()},
                        
                        success: function(response, options){
                                Ext.getCmp('formFeedBack').el.unmask();
                                if (response.responseText) {
                                    if (response.responseText.substr(0,1) == '{') {
			    	        var ajaxResponse = Ext.util.JSON.decode(response.responseText);
				     }
	                        }
                                if(ajaxResponse && ajaxResponse.success) {
                                    Ext.getCmp('feedBackSubject').reset();
                                    Ext.getCmp('feedBackMsg').reset();
                                    windowFeedBack.hide();
                                    Ext.msg.show('Op&eacute;ration r&eacute;ussie:', 'Message envoy&eacute; !');     
                                }else{
                                    Ext.Msg.alert('Erreur', result.errors.reason);
                                }
                        },
                            failure: function(response, opts) {
                                Ext.getCmp('formFeedBack').el.unmask();
                                Ext.Msg.alert('Erreur', 'Impossible d\'envoyer le message');
                                console.log('server-side failure ' + url);
                            }
                        });
                        
                        
                        
                    }
                    
                }
            ]
        }),
        
        items:[
          {
              xtype:'displayfield',
              id:'feedBackVersion',
              name:'version',
              fieldLabel:'Version',
              width:300,
              value:gPodVersion
          },{
              id:'feedBackSubject',
              name:'sujet',
              fieldLabel:'Sujet',
              width:300
          },{
              id:'feedBackMsg',
              name:'message',
              fieldLabel:'Bug / Commentaire',
              xtype:'textarea',
              width:300,
              height:200
          }    
            
            
        ]}        
    ]
});


var proxyAire = new Ext.data.HttpProxy({
    url: 'FrontController',
    method: 'POST'
});

var cbairePref = new Ext.data.JsonStore({
        id:cbairePref,
        proxy: proxyAire,
        fields: [
           {name: 'idAire'},
           {name: 'nomLong', type: 'string'}
        ],
        root:'data',
        totalProperty: 'totalCount',
        nocache: true,
        autoLoad: false,
        sortInfo: {
            field: 'nomLong',
            direction: 'ASC' // or 'DESC' (case sensitive for local sorting)
        },
        baseParams:{action:'getListItems', controller:'AireStockage'}
        ,listeners:{
            load:function(){
                    Ext.Ajax.request({
                        url: 'FrontController',
                        method: 'GET',
                        success: function(response, conn, option){
                            var ajaxResponse = Ext.util.JSON.decode(response.responseText);
                            var idAireF = ajaxResponse.idAireStockage;

                            
                            var record = Ext.getCmp('idAirePref').getStore().getAt(idAireF);
                            Ext.getCmp('idAirePref').setValue(Ext.getCmp('idAirePref').getStore().indexOf(record));
                        },
                        params: {action:'getAireStockageFavorite', controller:'Utilisateur'}
                    });
            }
        }
    });

var windowPreferences = new Ext.Window({
    width:'900',
    height:340,
    closeAction:'hide',
    layout:'border',
    items:[
        {
        id:'windowPreferences',
        xtype:'fieldset',    
        region:'center',     
        displayFloatingMsg:true,      	
        margins: '0 0 0 0',
        columnWidth: 0.9,     
        loadMask:{msg: 'Chargement...'},
        labelWidth: 150,          
        defaults: {border:false},    // Default config options for child items
        defaultType: 'textfield',
        labelSeparator:'',
        bodyStyle: Ext.isIE ? 'padding:0 0 5px 15px;' : 'padding:10px 15px;',
        border: true,     
        cls:'x-fieldset-with-tbar', 
        bbar:new Ext.Toolbar({
            height: 27,
            cls:'x-bbar',
            items: [ 
                '->','-',
                {
                    text:'Enregistrer',
                    handler:function(){
                        Ext.getCmp('windowPreferences').el.mask('Chargement...', 'x-mask-loading');
                        
                        Ext.Ajax.request({
                            method:'POST',
                            url:'FrontController',
                            params: { controller:'Utilisateur', action: 'savePref', 
                                idAire:Ext.getCmp('idAirePref').getValue()},
                        
                        success: function(response, options){
                                Ext.getCmp('windowPreferences').el.unmask();
                                if (response.responseText) {
                                    if (response.responseText.substr(0,1) == '{') {
			    	        var ajaxResponse = Ext.util.JSON.decode(response.responseText);
				     }
	                        }
                                if(ajaxResponse && ajaxResponse.success) {
                                    Ext.getCmp('idAirePref').reset();
                                    windowPreferences.hide();
                                    Ext.msg.show('Op&eacute;ration r&eacute;ussie:', 'Préférences enregisr&eacute;es !');     
                                }else{
                                    Ext.Msg.alert('Erreur', result.errors.reason);
                                }
                        },
                            failure: function(response, opts) {
                                Ext.getCmp('formFeedBack').el.unmask();
                                Ext.Msg.alert('Erreur', 'Impossible d\'enregistrer vos préférences');
                                console.log('server-side failure ' + url);
                            }
                        });
                        
                        
                        
                    }
                    
                }
            ]
        }),
        
        items:[
            {
                        id:'idAirePref',
                        xtype:'combo',
                        fieldLabel: 'Aire de Stockage Favorite',
                        name: 'nomAirePref',
                        width:600,
                        store: cbairePref,
                        root:'data',
                        hiddenName : 'idAire',
                        valueField:'idAire',
                        displayField:'nomLong',
                        typeAhead: true,
                        loadingText: 'Recherche...',
                        mode: 'local',
                        triggerAction: 'all',
                        emptyText:'Selectionner un element...',
                        selectOnFocus:true
                       }    
            
            
        ]}
]            ,listeners: {
                show: function(){
                    cbairePref.load();
                    return true;

         }
        }
});


