/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2012/01/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/

package fr.univtours.front;

import fr.univtours.gpod.utils.Utilities;

import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Cette Servlet est l'interface entre le code client et le code serveur
 * Chaque appel GET et POST du javascript (sauf clickey, authentification, Quit et Download) passe par cette servlet
 * Utilisation de l'introspection pour appeler les "ActionController" correspondant à l'appel
 * @author geoffroy.vibrac
 */
public class FrontController extends HttpServlet {

    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response, String method)
    throws ServletException, IOException {

        // On récupère la session mais on ne la créé pas ! Si elle n'existe pas, il y a une erreur (voir try/catch en dessous)
        HttpSession session = request.getSession(false);

        // si pas d'auth et que session n'est pas nul (a cause du CAS) => On annule la session
        if (!Utilities.isAuth(session) && session != null){
            session.invalidate();
            session = null;
        }

    
        // récupérer du chemin des controlleur dans le web.xml
        String controllersPath = getServletConfig().getInitParameter("controllersPath"); 
        // utilisé pour afficher certaines images
        String absolutePath = getServletContext().getRealPath("");


        // action contient le nom de la methode à executée récupérer de l'interface client
        String action = request.getParameter("action");
        // controller contient le nom du controller qui va traiter l'action
        String controller = request.getParameter("controller");

        // Sert à gérer les exceptions en cas de session inexistante (timeout, accès direct, etc...)
        try{
            // on ajoute absolutePath dans la session pour être utilisé dans les controlleurs
            session.setAttribute("absolutePath", absolutePath);
        // session = null => on gère l'exception
        }catch(NullPointerException ex){
            // si c'est un get, c'est un chargement de page => on force le redirect
            if ("GET".equals(method)){
                // avec action = null dans le block ci dessous, excute un sendRedirect
                action = null;
            // si c'est un POST, ça vient d'une requete ajax, un rechargement ne sert à rien => on envoye un json qui forcera le redirect géré dans le javascript
            }else{
                PrintWriter out = response.getWriter();
                out.println("{success:false, action:'reload'}");
                out.close();
                return;
            }
        }
        
       
    
        
        // Si action ==null ou "" (pas de Methode à executer)
        // ou controlleur==nullou "" (ne porte sur aucun objet=
        // => On fait un redirect vers index.html qui va rappeler la page d'authentification et revenir à la page d'accueil
        if (action == null || controller == null || "".equals(action) || "".equals(controller)){
            response.sendRedirect("../index.html");
        }else{
            // sert à gérer les problème d'introspection
            try {

                // Classes des Attributs du constructeur des Controlleurs qui vont gérer les requêtes du client
                Class ReqRespClass[] = new Class[3];
                ReqRespClass[0] = HttpServletRequest.class;
                ReqRespClass[1] = HttpServletResponse.class;           
                ReqRespClass[2] = HttpSession.class;       

                // Objbjets des attributs du constructeur de ces Controlleurs
                Object ReqResp[] = new Object[3];
                ReqResp[0] = request;
                ReqResp[1] = response;
                ReqResp[2] = session;

                // Construction de la Classe avec reflexivité
                Class c = Class.forName(controllersPath + controller+"Controller");
                // On affecte à cons le constructeur Controller(HttpServletRequest, HttpServletResponse, HttpSession)
                Constructor cons = c.getConstructor(ReqRespClass);
                // Instanciation de l'objet avec le constructeur
                Object o = cons.newInstance(ReqResp);

                // on affecte à m la methode qui doit être executer et passer dans le paramètre action
                Method m = c.getMethod(action);
                // Invocation de la méthode
                String result = (String) m.invoke(o);

                // le résultat sous forme texte (html, json) va être écrit dans un  PrintWriter
                // on récupère le PrintWriter de response
                PrintWriter out = response.getWriter();
                // on ecrit le resultat dans le printWriter
                out.println(result);
                // Fermeture du flux 
                out.close();
                

            } catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException | ClassNotFoundException ex) {
                Logger.getLogger(FrontController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        

        


        
        
        
        
        
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response, "GET");
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response, "POST");
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
