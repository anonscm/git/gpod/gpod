package fr.univtours.front;

import fr.univtours.gpod.Controller.ImportController;
import fr.univtours.gpod.persistance.DAO.AireStockageDAO;
import fr.univtours.gpod.persistance.DAO.SousStructureDAO;
import fr.univtours.gpod.persistance.entities.AireStockage;
import fr.univtours.gpod.persistance.entities.SousStructure;
import fr.univtours.gpod.utils.Utilities;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: geoffroy.vibrac
 * Date: 24/01/13
 * Time: 14:22
 * To change this template use File | Settings | File Templates.
 */
public class FileTransfert extends HttpServlet {




    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {

        // On récupère la session mais on ne la créé pas ! Si elle n'existe pas, il y a une erreur (voir try/catch en dessous)
        HttpSession session = request.getSession(false);
        if (Utilities.isAuth(session)){

            response.setContentType("text/html;charset=iso-8859-1");

            String FILES_PATH = this.getServletContext().getRealPath("files");
            System.out.println(FILES_PATH);
            Date laDate = new Date();
            long keyFile = laDate.getTime();

            FileItemFactory factory = new DiskFileItemFactory();
            ServletFileUpload upload = new ServletFileUpload(factory);

            PrintWriter out = response.getWriter();
            try {
                List<FileItem> fields = upload.parseRequest(request);
                // je créé 2 iterator car je dois parcourir les champs 2 fois
                // 1ère fois pour avoir les checkbox
                // Seconde fois pour avoir les fichiers
                Iterator<FileItem> it = fields.iterator();
                Iterator<FileItem> fileIt = fields.iterator();
                if (!it.hasNext()) {
                    return;
                }

                AireStockage aireStockage = null;
                SousStructure sousStructure = null;

                // on parcours à la recherche des checkbox
                List<String> checkboxes = new ArrayList<String>();
                while (it.hasNext()) {
                    FileItem formItem = it.next();
                    boolean isNotFileFormField = formItem.isFormField();
                    if (isNotFileFormField) {
                        if ("idAire".equals(formItem.getFieldName())){
                            int idAire=0;
                            try{
                                idAire = Integer.parseInt(formItem.getString());
                                aireStockage = new AireStockageDAO().findAireStockage(idAire);
                            }catch(NumberFormatException e){
                            }
                        }
                        if ("idSousStructure".equals(formItem.getFieldName())){
                            int idSousStructure=0;
                            try{
                                idSousStructure = Integer.parseInt(formItem.getString());
                                sousStructure = new SousStructureDAO().findSousStructure(idSousStructure);

                            }catch(NumberFormatException e){
                            }
                        }
                    }
                }
                if (!fileIt.hasNext()) {
                    return;
                }
                while (fileIt.hasNext()) {
                    FileItem fileItem = fileIt.next();
                    boolean isFormField = fileItem.isFormField();
                    if (!isFormField) {

                        String fName = fileItem.getName();
                        String fullPathFile = FILES_PATH + "/" + keyFile +"_"+fName;
                        File uploadedFile = new File(fullPathFile);

                        String retour = "";
                        try {
                            fileItem.write(uploadedFile);
                            ImportController ic = new ImportController(request, response, session);
                            retour = ic.importFile(fullPathFile, sousStructure, aireStockage);

                            if ("".equals(retour)){
                                out.println("{success:true, file:'"+uploadedFile.getName()+"'}");
                            }else{
                                out.println(retour);
                            }
                        } catch (Exception e) {
                            out.println("{success:false, errors:{reason:'Une erreur s\\\'est produite'}}");
                            System.out.println(e.getMessage());
                        }
                    }
                }
            } catch (FileUploadException e) {
                out.println("{success:false, errors:{reason:'Une erreur s\\\'est produite'}}");
                System.out.println(e.getMessage());
            }
            out.close();
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        final String dir = System.getProperty("user.dir");
        response.setContentType("text/html");
        PrintWriter pw = response.getWriter();
        pw.println("<html>");
        pw.println("<head><title>Hello World</title></title>");
        pw.println("<body>");
        pw.println(this.getServletContext().getRealPath("files"));
        pw.println("</body></html>");
    }
}
