/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package fr.univtours.gpod.json;

import fr.univtours.gpod.exceptions.DuplicateIdentifiantEtiquetteException;
import fr.univtours.gpod.exceptions.IllegalFournisseurException;
import fr.univtours.gpod.persistance.DAO.ArticleDAO;
import fr.univtours.gpod.persistance.DAO.ProduitConditionnementDAO;
import fr.univtours.gpod.persistance.entities.Article;
import fr.univtours.gpod.persistance.entities.Commande;
import fr.univtours.gpod.persistance.entities.CommandeLigne;
import fr.univtours.gpod.persistance.entities.LivraisonLigne;
import fr.univtours.gpod.persistance.entities.ProduitConditionnement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author geoffroy.vibrac
 */
public class JsonConversionUtilities {

    public static  List<ProduitConditionnement> jsonProduitConditionnemenArrayToList(String stringProduitConditionnements) {
        List<ProduitConditionnement> produitConditionnements = new ArrayList<ProduitConditionnement>();
        if (stringProduitConditionnements.length()>0){
            try {
                JSONArray arrayProduitConditionnements = new JSONArray(stringProduitConditionnements);
                for (int i = 0; i < arrayProduitConditionnements.length(); i++) {
                    JSONObject obj = arrayProduitConditionnements.getJSONObject(i);
                    try {
                        if (obj != null) {
                            int idProduitConditionnement = 0;
                            String reference = "";
                            double contenance = 0;
                            int idUniteMesure = 0;
                            int idContenant = 0;
                                idProduitConditionnement = Integer.parseInt((String) obj.get("idProduitConditionnement"));
                                reference = (String) obj.get("reference");
                                // En fonction de ce qui est saisie, contenance est parfois un Double, parfois un Integer et parfois une String
                                if (obj.get("contenance") instanceof Integer){
                                    contenance = ((Integer)obj.get("contenance")).doubleValue();
                                }else if (obj.get("contenance") instanceof Double){
                                     contenance = (Double)obj.get("contenance");
                                }else if (obj.get("contenance") instanceof String){
                                     contenance = Double.parseDouble((String)obj.get("contenance"));
                                }
                                idUniteMesure = Integer.parseInt((String) obj.get("idUniteMesure"));
                                idContenant = Integer.parseInt((String) obj.get("idContenant"));
                            ProduitConditionnement pc = new ProduitConditionnement(idProduitConditionnement, reference, contenance, idContenant, idUniteMesure, 0);
                            if (pc != null) {
                                produitConditionnements.add(pc);
                            }
                        }
                    } catch (ClassCastException ex) {
                        Logger.getLogger(JsonConversionUtilities.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            } catch (JSONException ex) {
                Logger.getLogger(JsonConversionUtilities.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return produitConditionnements;

    }

   public static  List<Article> jsonArticleArrayToList(String stringArticles, int idSousStructure) throws DuplicateIdentifiantEtiquetteException {
       List<Article> articleList = new ArrayList<Article>();
       if (stringArticles.length()>0){
           try {
                JSONArray articleArray = new JSONArray(stringArticles);
                ArticleDAO daoA = new ArticleDAO();     
                List<String> identifiantEtiquetteList = new ArrayList<>();
                for (int i = 0; i < articleArray.length(); i++) {
                    Article article = null;
                    JSONObject obj = articleArray.getJSONObject(i);
                    if (obj != null) {
                        int lividProduitConditionnement = 0;
                        int idAire = 0;
                        int idArticle = 0;
                        
                        try{
                            lividProduitConditionnement = Integer.parseInt((String) obj.get("idProduitConditionnement"));
                            idAire = Integer.parseInt((String) obj.get("idAire"));                    
                        }catch(JSONException ex){                            
                        }catch (ClassCastException ex) {
                            Logger.getLogger(JsonConversionUtilities.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        
                        try {
                            idArticle = Integer.parseInt((String) obj.get("idArticle"));
                        } catch (ClassCastException ex) {
                        }
                        String identifiantEtiquette = (String) obj.get("identifiantEtiquette");
                        if (idArticle>0){
                            article = daoA.findArticle(idArticle);
                            // test de l'unicité de identifiantEtiquette
                            // si identifiantEtiquette est rempli
                            if (!"".equals(identifiantEtiquette) && !"000000000000000000000000".equals(identifiantEtiquette)){
                                // On cherche un article avec le même identifiant etiquette
                                Article a2 = daoA.findArticleFromIdentifiantEtiquette(identifiantEtiquette);
                                // si il y a un article avec cet etiquette et que ce n'est pas cet article qu'on modifie
                                // => On retourne une erreur
                                if ((a2!=null && !a2.equals(article)) || identifiantEtiquetteList.contains(identifiantEtiquette)){                        
                                    throw new DuplicateIdentifiantEtiquetteException("Une étiquette est déjà utilisée pour un autre article");
                                }                            
                            } 
                            identifiantEtiquetteList.add(identifiantEtiquette);
                            article.setIdentifiantEtiquette(identifiantEtiquette);
                            
                        }else{
                            if (!"".equals(identifiantEtiquette)){                           
                                // On cherche un article avec le même identifiant etiquette
                                Article a2 = daoA.findArticleFromIdentifiantEtiquette(identifiantEtiquette);
                                // si il y a un article avec cet etiquette et que ce n'est pas cet article qu'on modifie
                                // => On retourne une erreur
                                if ((a2!=null && !a2.equals(article)) || identifiantEtiquetteList.contains(identifiantEtiquette)){                                      
                                    throw new DuplicateIdentifiantEtiquetteException("Une étiquette est déjà utilisée pour un autre article");
                                }                            
                            }  
                            identifiantEtiquetteList.add(identifiantEtiquette);
                            article = new Article(lividProduitConditionnement, idSousStructure, idAire, 0, identifiantEtiquette);
                        }
                        if (article != null) {
                            articleList.add(article);
                        }
                    }
                }
            } catch (JSONException ex) {
                Logger.getLogger(JsonConversionUtilities.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return articleList;
    }

   public static  List<CommandeLigne> jsonCommandeLigneArrayToList(String stringCommandeLigne) throws IllegalFournisseurException{
       List<CommandeLigne> commandeLigneList = new ArrayList<CommandeLigne>();
       if (stringCommandeLigne.length()>0){
           try {
                JSONArray commandeLigneArray = new JSONArray(stringCommandeLigne);
                int idFournisseurPrecedent=-1;
                for (int i = 0; i < commandeLigneArray.length(); i++) {
                    JSONObject obj = commandeLigneArray.getJSONObject(i);
                    if (obj != null) {
                        CommandeLigne cL = null;                        
                        ProduitConditionnement pc = null;
                        // récupération de idCommande
                        Object idObj = obj.get("idCommandeLigne");
                        int idCommandeLigne = 0;
                        if (idObj instanceof String){
                             idCommandeLigne = Integer.parseInt((String)idObj);
                        }else if (idObj instanceof Integer){
                             idCommandeLigne = (Integer)idObj;
                        } 
                        
                        ProduitConditionnementDAO dao = new ProduitConditionnementDAO();                       
                        int idProduitConditionnement = Integer.parseInt((String)obj.get("idProduitConditionnement"));
                        if (idProduitConditionnement>0){
                            pc = dao.findProduitConditionnement(idProduitConditionnement);
                        }                        
                        
                        Object qteObj = obj.get("quantiteLigne");
                        int quantite = 0;
                        if (qteObj instanceof String){
                             quantite = Integer.parseInt((String)qteObj);
                        }else if (qteObj instanceof Integer){
                             quantite = (Integer)qteObj;
                        } 

/*
                        // la vérif par fournisseur est désactivée
                        if (pc !=null){
                             // il faut s'assurer qu'on est toujours le même fournisseur pour tous les produits
                             // commandés.
                             // On initialise idFournisseur lors de la 1ère ligne
                             if (idFournisseurPrecedent == -1){
                                 idFournisseurPrecedent = pc.getProduit().getFournisseur().getIdFournisseur();
                             }
                             if (idFournisseurPrecedent!=pc.getProduit().getFournisseur().getIdFournisseur()){
                                 throw new IllegalFournisseurException("Par commande, vous ne pouvez choisir que des produits du même fournisseur");
                             }

                         }*/
                        try{
                             cL = new CommandeLigne(idCommandeLigne, pc,quantite);
                        }catch(Exception ex){
                             Logger.getLogger(JsonConversionUtilities.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        if (cL != null) {
                            commandeLigneList.add(cL);
                        }
                    }
                }
            } catch (JSONException ex) {
                Logger.getLogger(JsonConversionUtilities.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassCastException ex) {
                Logger.getLogger(JsonConversionUtilities.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return commandeLigneList;
    }
   
   
   public static  List<LivraisonLigne> jsonLivraisonLigneArrayToList(String stringLivraisonLigne) {
       List<LivraisonLigne> livraisonLigneList = new ArrayList<LivraisonLigne>();
       if (stringLivraisonLigne.length()>0){
           try {
                JSONArray livraisonLigneArray = new JSONArray(stringLivraisonLigne);
                for (int i = 0; i < livraisonLigneArray.length(); i++) {
                    JSONObject obj = livraisonLigneArray.getJSONObject(i);
                    if (obj != null) {
                        LivraisonLigne cL = null;                        
                        ProduitConditionnement pc = null;
                        
                        ProduitConditionnementDAO dao = new ProduitConditionnementDAO();   
                        int idProduitConditionnement = 0;
                        try{
                            idProduitConditionnement = Integer.parseInt((String)obj.get("idProduitConditionnement"));
                        } catch (ClassCastException ex) {
                            Logger.getLogger(JsonConversionUtilities.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        if (idProduitConditionnement>0){
                            pc = dao.findProduitConditionnement(idProduitConditionnement);
                        }   
                        
                        Object qteObj = obj.get("quantiteLivraison");
                        int quantite = 0;
                        if (qteObj instanceof String){
                             quantite = Integer.parseInt((String)qteObj);
                        }else if (qteObj instanceof Integer){
                             quantite = (Integer)qteObj;
                        } 
                        try{
                             cL = new LivraisonLigne(quantite, true, pc);
                        }catch(Exception ex){
                             Logger.getLogger(JsonConversionUtilities.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        if (cL != null) {
                            livraisonLigneList.add(cL);
                        }
                    }
                }
            } catch (JSONException ex) {
                Logger.getLogger(JsonConversionUtilities.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassCastException ex) {
                Logger.getLogger(JsonConversionUtilities.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return livraisonLigneList;
    }
}
