/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2012/01/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/

package fr.univtours.gpod.HTML;

/**
 *
 * @author geoffroy.vibrac
 */
public class Header {

    private String title;
    private CSS css;
    private Js js;
    private String charset;
    private ICO ico;
    
    
    public Header(){
        this.title = "sans titre";
        this.charset= "iso-8859-1";
        this.css = new CSS();
        this.js = new Js();
        this.ico = new ICO();
    }
    
    
    
    
    public void setCharset(String charset) {
        this.charset = charset;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCharset() {
        return charset;
    }

    public CSS getCss() {
        return css;
    }

    public Js getJs() {
        return js;
    }

    public String getTitle() {
        return title;
    }

    public ICO getIco() {
        return ico;
    }

    public void setIco(ICO ico) {
        this.ico = ico;
    }

    public String getCode(){
       String out=""; 
       out =  "<head>\n";
       out += "<title>" + this.getTitle() + "</title>\n";
       out += "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=" + this.getCharset() + "\">\n";
       out += "<meta http-equiv=\"X-UA-Compatible\" content=\"chrome=1\">\n";
       out += this.getCss().getCode() + "\n";
       out += this.getIco().getCode() + "\n";
       out += this.getJs().getBlocksLignes() + "\n";
       out += this.getJs().getCode() + "\n";
       out += "</head>\n";
       return out;
  }    
    
    
    
}
