/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2012/01/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/


package fr.univtours.gpod.persistance.entities;

import fr.univtours.gpod.persistance.DAO.ArticleDAO;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author geoffroy.vibrac
 */
@Entity
@Table(name = "clickeyscan")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ClickeyScan.findAll", query = "SELECT c FROM ClickeyScan c"),
    @NamedQuery(name = "ClickeyScan.findByMultiCriteria", query = "SELECT c FROM ClickeyScan c WHERE c.uIDClickey = :uIDClickey AND "
        + "c.article = :article AND c.timeStamp = :timeStamp"),
    @NamedQuery(name = "ClickeyScan.findIfTagExist", query = "SELECT c FROM ClickeyScan c WHERE c.dejaTraite=FALSE AND c.article = :article AND c.utilisateur = :utilisateur"),
    @NamedQuery(name = "ClickeyScan.findByIdClickeyScan", query = "SELECT c FROM ClickeyScan c WHERE c.idClickeyScan = :idClickeyScan"),
    @NamedQuery(name = "ClickeyScan.findByUIDClickey", query = "SELECT c FROM ClickeyScan c WHERE c.uIDClickey = :uIDClickey"),
    @NamedQuery(name = "ClickeyScan.findByTimeStamp", query = "SELECT c FROM ClickeyScan c WHERE c.timeStamp = :timeStamp"),
    @NamedQuery(name = "ClickeyScan.findByDateSynchro", query = "SELECT c FROM ClickeyScan c WHERE c.dateSynchro = :dateSynchro"),
    @NamedQuery(name = "ClickeyScan.findByDejaTraite", query = "SELECT c FROM ClickeyScan c WHERE c.dejaTraite = :dejaTraite")})
public class ClickeyScan implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @NotNull
    @Column(name = "idClickeyScan")
    private Integer idClickeyScan;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "UIDClickey")
    private String uIDClickey;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "TimeStamp")
    private String timeStamp;
    @Basic(optional = false)
    @NotNull
    @Column(name = "DateSynchro")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateSynchro;
    @Basic(optional = false)
    @NotNull
    @Column(name = "DejaTraite")
    private boolean dejaTraite;

    
    @JoinColumn(name = "UIDTag", referencedColumnName = "identifiantEtiquette")
    @ManyToOne(optional = false)
    private Article article;    
    
    @JoinColumn(name = "idUtilisateur", referencedColumnName = "idUtilisateur")
    @ManyToOne(optional = false)
    private Utilisateur utilisateur;

    public ClickeyScan() {
    }

    public ClickeyScan(Integer idClickeyScan) {
        this.idClickeyScan = idClickeyScan;
    }

    public ClickeyScan(String uIDClickey, String uIDTag, String timeStamp, Date dateSynchro, Utilisateur utilisateur,  boolean dejaTraite) {
        
        ArticleDAO dao = new ArticleDAO();
        this.article = dao.findArticleFromIdentifiantEtiquette(uIDTag);
        
        this.uIDClickey = uIDClickey;
        this.timeStamp = timeStamp;
        this.dateSynchro = dateSynchro;
        this.dejaTraite = dejaTraite;
        this.utilisateur = utilisateur;
    }

    public Integer getIdClickeyScan() {
        return idClickeyScan;
    }

    public void setIdClickeyScan(Integer idClickeyScan) {
        this.idClickeyScan = idClickeyScan;
    }

    public String getUIDClickey() {
        return uIDClickey;
    }

    public void setUIDClickey(String uIDClickey) {
        this.uIDClickey = uIDClickey;
    }

    public Article getArticle() {
        return article;
    }

    public void setArticle(Article article) {
        this.article = article;
    }


    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public Date getDateSynchro() {
        return dateSynchro;
    }

    public void setDateSynchro(Date dateSynchro) {
        this.dateSynchro = dateSynchro;
    }

    public boolean getDejaTraite() {
        return dejaTraite;
    }

    public void setDejaTraite(boolean dejaTraite) {
        this.dejaTraite = dejaTraite;
    }

    public Utilisateur getUtilisateur() {
        return utilisateur;
    }

    public void setUtilisateur(Utilisateur utilisateur) {
        this.utilisateur = utilisateur;
    }
    
    
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idClickeyScan != null ? idClickeyScan.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof ClickeyScan)) {
            return false;
        }
        ClickeyScan other = (ClickeyScan) object;
        if ((this.idClickeyScan == null && other.idClickeyScan != null) || (this.idClickeyScan != null && !this.idClickeyScan.equals(other.idClickeyScan))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "fr.univtours.gpod.persistance.entities.ClickeyScan[ idClickeyScan=" + idClickeyScan + " ]";
    }
    
}
