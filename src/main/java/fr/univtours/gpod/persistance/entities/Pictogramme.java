/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2012/01/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/


package fr.univtours.gpod.persistance.entities;

import fr.univtours.gpod.utils.Utilities;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author geoffroy.vibrac
 */
@Entity
@Table(name = "pictogramme")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Pictogramme.findAll", query = "SELECT p FROM Pictogramme p"),
    @NamedQuery(name = "Pictogramme.findByIdPictogramme", query = "SELECT p FROM Pictogramme p WHERE p.idPictogramme = :idPictogramme"),
    @NamedQuery(name = "Pictogramme.findByNom", query = "SELECT p FROM Pictogramme p WHERE p.nom = :nom"),
    @NamedQuery(name = "Pictogramme.findByDescription", query = "SELECT p FROM Pictogramme p WHERE p.description = :description"),
    @NamedQuery(name = "Pictogramme.findByIcone", query = "SELECT p FROM Pictogramme p WHERE p.icone = :icone"),
    @NamedQuery(name = "Pictogramme.findByCode", query = "SELECT p FROM Pictogramme p WHERE p.code = :code")})   
public class Pictogramme implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idPictogramme;
    @Size(max = 45)
    @Column(name = "nom")
    private String nom;
    @Size(max = 45)
    @Column(name = "description")
    private String description;
    @Size(max = 45)
    @Column(name = "icone")
    private String icone;
    @Size(max = 10)
    @Column(name = "code")
    private String code;    
    @Column(name = "sgh")
    private boolean sgh;      
//    
//    @ManyToMany(mappedBy = "pictogrammes")
//    @JoinTable(name="substancepictogramme",
//    joinColumns= @JoinColumn(name = "idPictogramme"),
//    inverseJoinColumns= @JoinColumn(name="CAS"))
//    private List<Substance> substances;
    
    public Pictogramme() {
    }

    public Pictogramme(Integer idPictogramme) {
        this.idPictogramme = idPictogramme;
    }

    public Pictogramme(Integer idPictogramme, String nom, String description, String icone, String code, boolean sgh) {
        this.idPictogramme = idPictogramme;
        this.nom = nom;
        this.description = description;
        this.icone = icone;
        this.code = code;
        this.sgh = sgh;
    }
    
    
    public Integer getIdPictogramme() {
        return idPictogramme;
    }

    public void setIdPictogramme(Integer idPictogramme) {
        this.idPictogramme = idPictogramme;
    }

    public String getNom() {
        return Utilities.addSlashes(nom);
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIcone() {
        return icone;
    }

    public void setIcone(String icone) {
        this.icone = icone;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public boolean isSgh() {
        return sgh;
    }

    public void setSgh(boolean sgh) {
        this.sgh = sgh;
    }

    public static String ListToStringIdWithComma(List<Pictogramme> pictogrammes){
        String valRetour = "";
        if (pictogrammes.size()>0){
            for (Pictogramme p:pictogrammes){
                valRetour += "," + p.idPictogramme;
            }
            valRetour = valRetour.substring(1);
        }
        return valRetour;
    }
    
    public Map<String, String> getDataForVerticalTable(){
        Map<String, String> editedData = new LinkedHashMap<String, String>();    
        editedData.put("Code :", this.getCode());
        editedData.put("Nom :", this.getNom());   
        editedData.put("Description :", this.getDescription()); 

        return editedData;        
    }
    
    public static List<String[]> getDataForHorizontalTableImg(List<Pictogramme> pictogrammes){
        List <String[]> editedData = new ArrayList<String[]>();
        if (pictogrammes!=null){
            editedData.add(new String[]{""});

            for (Pictogramme pic : pictogrammes){
                editedData.add(new String[]{pic.getIcone()});
            }
        }
        return editedData;
    }       
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPictogramme != null ? idPictogramme.hashCode() : 0);
        return hash;
    }
    

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Pictogramme)) {
            return false;
        }
        Pictogramme other = (Pictogramme) object;
        if ((this.idPictogramme == null && other.idPictogramme != null) || (this.idPictogramme != null && !this.idPictogramme.equals(other.idPictogramme))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return idPictogramme + ";" + nom + ";" + description + ";" + icone + ";" + code+ ";" + sgh;
    }

}
