/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2012/01/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/


package fr.univtours.gpod.persistance.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author geoffroy.vibrac
 */
@Entity
@Table(name = "typeaire")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "typeaire.findAll", query = "SELECT t FROM TypeAire t"),
    @NamedQuery(name = "typeaire.findByIdTypeAire", query = "SELECT t FROM TypeAire t WHERE t.idTypeAire = :idTypeAire"),
    @NamedQuery(name = "typeaire.findByNom", query = "SELECT t FROM TypeAire t WHERE t.nom = :nom")})
public class TypeAire implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @NotNull
    @Column(name = "idTypeAire")
    private Integer idTypeAire;
    @Size(max = 45)
    @Column(name = "nom")
    private String nom;
    @OneToMany(mappedBy = "typeAire")
    private List<AireStockage> aires;

        
        
    public TypeAire() {
    }

    public Integer getIdTypeAire() {
        return idTypeAire;
    }

    public void setIdTypeAire(Integer idTypeAire) {
        this.idTypeAire = idTypeAire;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @XmlTransient
    public List<AireStockage> getAires() {
        return aires;
    }

    public void setAires(List<AireStockage> aires) {
        this.aires = aires;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTypeAire != null ? idTypeAire.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof TypeAire)) {
            return false;
        }
        TypeAire other = (TypeAire) object;
        if ((this.idTypeAire == null && other.idTypeAire != null) || (this.idTypeAire != null && !this.idTypeAire.equals(other.idTypeAire))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "fr.univtours.Gpod.persistance.TypeAire[ idTypeAire=" + idTypeAire + " ]";
    }

}
