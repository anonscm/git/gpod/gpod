/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2012/01/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/

package fr.univtours.gpod.persistance.entities;

import fr.univtours.gpod.persistance.DAO.ContenantDAO;
import fr.univtours.gpod.persistance.DAO.ProduitDAO;
import fr.univtours.gpod.persistance.DAO.UniteMesureDAO;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author geoffroy.vibrac
 */
@Entity
@Table(name = "produitconditionnement")
@NamedQueries({
    @NamedQuery(name = "ProduitConditionnement.findAll", query = "SELECT p FROM ProduitConditionnement p"),
    @NamedQuery(name = "ProduitConditionnement.findByIdProduitConditionnement", query = "SELECT p FROM ProduitConditionnement p WHERE p.idProduitConditionnement = :idProduitConditionnement"),
    @NamedQuery(name = "ProduitConditionnement.findByReferenceFournisseur", query = "SELECT p FROM ProduitConditionnement p WHERE p.referenceFournisseur = :referenceFournisseur"),
    @NamedQuery(name = "ProduitConditionnement.findByContenance", query = "SELECT p FROM ProduitConditionnement p WHERE p.contenance = :contenance")})
@XmlRootElement
public class ProduitConditionnement implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idProduitConditionnement")
    private Integer idProduitConditionnement;

    @Basic(optional = false)
    @Column(name = "referenceFournisseur")

    private String referenceFournisseur;
    @Basic(optional = false)
    @Column(name = "contenance")
    private double contenance;

    @JoinColumn(name = "idContenant", referencedColumnName = "idContenant")
    @ManyToOne
    private Contenant contenant;

    @JoinColumn(name = "idUniteMesure", referencedColumnName = "idUniteMesure")
    @ManyToOne
    private UniteMesure uniteMesure;

    @JoinColumn(name = "idProduit", referencedColumnName = "idProduit")
    @ManyToOne
    private Produit produit;

    
    @Basic(optional = false)
    @NotNull
    @Column(name = "deleted")
    private boolean deleted;    

    public ProduitConditionnement() {
    }

    public ProduitConditionnement(String referenceFournisseur, double contenance, Contenant contenant, UniteMesure uniteMesure, Produit produit) {
        this.referenceFournisseur = referenceFournisseur;
        this.contenance = contenance;
        this.contenant = contenant;
        this.uniteMesure = uniteMesure;
        this.produit = produit;
        this.deleted = false;
    }

    public ProduitConditionnement(int idProduitConditionnement, String referenceFournisseur, double contenance, int idContenant, int idUniteMesure, int idProduit) {
        this.referenceFournisseur = referenceFournisseur;
        this.contenance = contenance;

        if (idProduitConditionnement!=0){
            this.idProduitConditionnement = idProduitConditionnement;
        }
        
        ContenantDAO cdao = new ContenantDAO();
        Contenant contenant = cdao.findContenant(idContenant);
        this.contenant = contenant;

        UniteMesureDAO umdao = new UniteMesureDAO();
        UniteMesure uniteMesure = umdao.findUniteMesure(idUniteMesure);
        this.uniteMesure = uniteMesure;
        
        ProduitDAO pdao = new ProduitDAO();
        Produit produit = pdao.findProduit(idProduit);
        this.produit = produit;

        this.deleted = false;
    }



    public Integer getIdProduitConditionnement() {
        return idProduitConditionnement;
    }

    public String getReferenceFournisseur() {
        return referenceFournisseur;
    }

    public Contenant getContenant() {
        return contenant;
    }

    public void setContenant(Contenant contenant) {
        this.contenant = contenant;
    }

    public Produit getProduit() {
        return produit;
    }

    public void setProduit(Produit produit) {
        this.produit = produit;
    }

    

    public void setReferenceFournisseur(String referenceFournisseur) {
        this.referenceFournisseur = referenceFournisseur;
    }

    public double getContenance() {
        return contenance;
    }

    public void setContenance(double contenance) {
        this.contenance = contenance;
    }


    public UniteMesure getUniteMesure() {
        return uniteMesure;
    }

    public void setUniteMesure(UniteMesure idUniteMesure) {
        this.uniteMesure = idUniteMesure;
    }


    public Map<String, String> getDataForVerticalTable(){
        Map<String, String> editedData = new LinkedHashMap<String, String>();    
        String cont =  this.getContenance() + " " + this.getUniteMesure().getLibelleLong();
        editedData.put("Reference Fournisseur :", this.getReferenceFournisseur());
        editedData.put("Contenant :", this.getContenant().getLibelle());
        editedData.put("Contenance :", cont);   

        return editedData;        
    }
    
    public static List<String[]> getDataForHorizontalTable(List<ProduitConditionnement> produitConditionnements){
        List <String[]> editedData = new ArrayList<String[]>();
        if (produitConditionnements!=null){
            editedData.add(new String[]{"Reference Fournisseur","Contenant", "Contenance"});

            for (ProduitConditionnement pc : produitConditionnements){
                String cont =  pc.getContenance() + " " + pc.getUniteMesure().getLibelleLong();
                editedData.add(new String[]{pc.getReferenceFournisseur(), pc.getContenant().getLibelle(),cont});
            }
        }
        return editedData;
    }      

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idProduitConditionnement != null ? idProduitConditionnement.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof ProduitConditionnement)) {
            return false;
        }
        ProduitConditionnement other = (ProduitConditionnement) object;
        if ((this.idProduitConditionnement == null && other.idProduitConditionnement != null) || (this.idProduitConditionnement != null && !this.idProduitConditionnement.equals(other.idProduitConditionnement))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "fr.univtours.Gpod.persistance.entities.Produitconditionnement[idProduitConditionnement=" + idProduitConditionnement + "]";
    }

    public boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }


}
