/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2012/01/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/


package fr.univtours.gpod.persistance.entities;

import fr.univtours.gpod.utils.Utilities;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.joda.time.DateTime;

/**
 *
 * @author geoffroy.vibrac
 */
@Entity
@Table(name = "commande")
@NamedQueries({
    @NamedQuery(name = "Commande.findAll", query = "SELECT c FROM Commande c"),
    @NamedQuery(name = "Commande.findByIdCommande", query = "SELECT c FROM Commande c WHERE c.idCommande = :idCommande"),
    @NamedQuery(name = "Commande.findByDateCommande", query = "SELECT c FROM Commande c WHERE c.dateCommande = :dateCommande")})
@XmlRootElement
public class Commande implements Serializable {
    @Column(name =     "dateCommande")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCommande;
    @Column(name =     "dateCommandeInterne")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCommandeInterne;
    
   
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idCommande")
    private Integer idCommande;
    @Size(max = 50)
    @Column(name = "numBonDeCommandeInterne")
    private String numBonDeCommandeInterne;

    @JoinColumn(name = "idStatut", referencedColumnName = "idStatut")
    @ManyToOne(optional = false)
    private Statut statut;

    @JoinColumn(name = "idUtilisateur", referencedColumnName = "idUtilisateur")
    @ManyToOne(optional = false)
    private Utilisateur utilisateur;

    @JoinColumn(name = "idSousStructure", referencedColumnName = "idSousStructure")
    @ManyToOne(optional = false)
    private SousStructure sousStructure;

    @OneToMany(mappedBy = "commande")
    private List<Livraison> livraisonList;
    
//    @OneToMany(cascade= CascadeType.ALL)
//    @JoinTable(name = "CommandeCommandeLigne",
//        joinColumns = @JoinColumn(name = "idCommande"),
//        inverseJoinColumns = @JoinColumn(name = "idCommandeLigne") )
//    private List<CommandeLigne> commandeLigneList;    
    
    @OneToMany(mappedBy = "commande", fetch=FetchType.EAGER)
    @JoinColumn(name = "idCommande", referencedColumnName = "idCommande")
    private List<CommandeLigne> commandeLigneList;     

    public Commande() {
    }

    public Commande(Date dateCommande, Statut statut, Utilisateur utilisateur, SousStructure sousStructure, List<CommandeLigne> commandeLigneList) {
        this.dateCommande = dateCommande;
        this.statut = statut;
        this.utilisateur = utilisateur;
        this.sousStructure = sousStructure;
        this.commandeLigneList = commandeLigneList;
    }

    public Commande(DateTime dateCommande, Statut statut, Utilisateur utilisateur, SousStructure sousStructure, List<CommandeLigne> commandeLigneList) {
        if (dateCommande!=null){
            this.dateCommande = dateCommande.toDate();
        }
        this.statut = statut;
        this.utilisateur = utilisateur;
        this.sousStructure = sousStructure;
        this.commandeLigneList = commandeLigneList;

    }


 
    public Integer getIdCommande() {
        return idCommande;
    }

    public void setIdCommande(Integer idCommande) {
        this.idCommande = idCommande;
    }

    public void setDateCommande(DateTime dateCommande) {
        if (dateCommande!=null)
            this.dateCommande = dateCommande.toDate();
    }

    public void setDateCommandeInterne(DateTime dateCommandeInterne) {
        if (dateCommandeInterne!=null)
            this.dateCommandeInterne = dateCommandeInterne.toDate();
    }

    public Statut getStatut() {
        return statut;
    }

    public void setStatut(Statut statut) {
        this.statut = statut;
    }

    public Utilisateur getUtilisateur() {
        return utilisateur;
    }

    public void setUtilisateur(Utilisateur utilisateur) {
        this.utilisateur = utilisateur;
    }

    public SousStructure getSousStructure() {
        return sousStructure;
    }

    public void setSousStructure(SousStructure sousStructure) {
        this.sousStructure = sousStructure;
    }

    // Pour qu'une commande soit "supprimable"
    // il faut qu'elle ai été faite par l'utilisateur + qu'elle soit encore à l'état 1
    public boolean isDeletable(Utilisateur user){
        return this.getUtilisateur().equals(user) && this.getStatut().getIdStatut()<=3 && this.getLivraisonList().size()==0; 
    }

    public String getNumBonDeCommandeInterne() {
        return numBonDeCommandeInterne;
    }

    public void setNumBonDeCommandeInterne(String numBonDeCommandeInterne) {
        this.numBonDeCommandeInterne = numBonDeCommandeInterne;
    }

    public List<Livraison> getLivraisonList() {
        return livraisonList;
    }

    public void setLivraisonList(List<Livraison> livraisonList) {
        this.livraisonList = livraisonList;
    }
    
//    public int getQuantiteRecue(){
//        int quantiteRecue=0;
//        if (this.getCommandeLigneList()!=null && this.getCommandeLigneList().size()>0){
//            for (CommandeLigne ll : getCommandeLigneList()){
//                quantiteRecue += ll.getQuantiteRecue();
//            }
//        }
//        return quantiteRecue;
//    }
//    
//    public int getQuantiteCommandee(){
//        int quantiteCommandee=0;
//        if (this.getCommandeLigneList()!=null && this.getCommandeLigneList().size()>0){
//            for (CommandeLigne ll : getCommandeLigneList()){
//                quantiteCommandee += ll.getQuantite();
//            }
//        }
//        return quantiteCommandee;
//    }
//      
//    
//    public int getPourcentageRecu(){
//        return (this.getQuantiteRecue()*100)/this.getQuantiteCommandee();
//    }
//    
//    public int getQuantiteResteAlivrer(){
//        return this.getQuantiteCommandee() - this.getQuantiteRecue();
//    }

    
    
    public Map<String, String> getDataForVerticalTable(){
        Map<String, String> editedData = new LinkedHashMap<String, String>();        
        editedData.put("Date de commande :", Utilities.javaDateToString(this.dateCommande));
        editedData.put("Date de commande Sifac :",  Utilities.javaDateToString(this.dateCommandeInterne));          
        editedData.put("Numero bon de commande : ", this.numBonDeCommandeInterne);
        editedData.put("Statut commande :", this.statut.getLibelle());
        editedData.put("Commanditaire :", this.utilisateur.getNom());
        editedData.put("Structure : ", this.sousStructure.getNom());        
   
        
        return editedData;        
    }
    
    public static List<String[]> getDataForHorizontalTable(List<Commande> commandes){
        List <String[]> editedData = new ArrayList<String[]>();
        if (commandes!=null){
            editedData.add(new String[]{"Date de commande","Date de commande Sifac", "Numero bon de commande", "Statut commande", "Commanditaire", "Structure"});
            for (Commande commande : commandes){
                String dateCommande = Utilities.javaDateToString(commande.getDateCommande());
                String dateCommandeInt = Utilities.javaDateToString(commande.getDateCommandeInterne());
                editedData.add(new String[]{dateCommande,
                                            dateCommandeInt,
                                            commande.getNumBonDeCommandeInterne(),
                                            commande.getStatut().getLibelle(),
                                            commande.getUtilisateur().getNom(),
                                            commande.getSousStructure().getNom()                
                });
            }
        }
        return editedData;
    }  
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCommande != null ? idCommande.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Commande)) {
            return false;
        }
        Commande other = (Commande) object;
        if ((this.idCommande == null && other.idCommande != null) || (this.idCommande != null && !this.idCommande.equals(other.idCommande))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "fr.univtours.Gpod.persistance.entities.Commande[idCommande=" + idCommande + "]";
    }

    public Date getDateCommande() {
        return dateCommande;
    }

    public void setDateCommande(Date dateCommande) {
        this.dateCommande = dateCommande;
    }

    public Date getDateCommandeInterne() {
        return dateCommandeInterne;
    }

    public void setDateCommandeInterne(Date dateCommandeInterne) {
        this.dateCommandeInterne = dateCommandeInterne;
    }

    @XmlTransient
    public List<CommandeLigne> getCommandeLigneList() {
        return commandeLigneList;
    }

    public void setCommandeLigneList(List<CommandeLigne> commandeLigneList) {
        this.commandeLigneList = commandeLigneList;
    }

}
