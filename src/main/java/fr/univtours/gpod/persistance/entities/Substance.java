/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2012/01/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/


package fr.univtours.gpod.persistance.entities;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.apache.commons.lang.StringEscapeUtils;

/**
 *
 * @author geoffroy.vibrac
 */
@Entity
@Table(name = "substance")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Substance.findAll", query = "SELECT s FROM Substance s"),
    @NamedQuery(name = "Substance.findByCas", query = "SELECT s FROM Substance s WHERE s.cas = :cas"),
    @NamedQuery(name = "Substance.findByEC", query = "SELECT s FROM Substance s WHERE s.ec = :ec"),
    @NamedQuery(name = "Substance.findByNom", query = "SELECT s FROM Substance s WHERE s.nom = :nom"),
    @NamedQuery(name = "Substance.findByFormule", query = "SELECT s FROM Substance s WHERE s.formule = :formule")})
public class Substance implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "CAS")
    private String cas;
    @Size(max = 255)
    @Column(name = "nom")
    private String nom;
    @Size(max = 45)
    @Column(name = "formule")
    private String formule;
    @Lob
    @Size(max = 65535)
    @Column(name = "description")
    private String description;
    @Size(min = 1, max = 10)
    @Column(name = "EC")
    private String ec;    
    @Lob
    @Size(max = 65535)
    @Column(name = "sdf")
    private String sdf;

    @OneToMany
    @JoinTable(name="substancepictogramme",
    joinColumns= @JoinColumn(name = "CAS"),
    inverseJoinColumns= @JoinColumn(name="idPictogramme"))
    private List<Pictogramme> pictogrammes;
    

    
    
    public Substance() {
    }

    public Substance(String cas, String nom, String formule, String description, String ec) {
        this.cas = cas;
        this.nom = nom;
        this.formule = formule;
        this.description = description;
        this.ec = ec;
    }
    
    public Substance(String cas, String nom, String formule, String description, String ec, List<Pictogramme> pictogrammes) {
        this(cas, nom, formule, description, ec);
        this.pictogrammes = pictogrammes;
    }
    

    public String getCas() {
        return cas;
    }

    public void setCas(String cas) {
        this.cas = cas;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getFormule() {
        return formule;
    }

    public void setFormule(String formule) {
        this.formule = formule;
    }

    public String getDescription() {
        return description;
    }
    
    public String getEc() {
        return ec;
    }

    public void setEc(String ec) {
        this.ec = ec;
    }    

    public void setDescription(String description) {
        this.description = description;
    }
    
    public void setPictogramme(List<Pictogramme> pictogrammes){
        this.pictogrammes = pictogrammes;
    }

    public String getSdf() {
        return sdf;
    }

    public void setSdf(String sdf) {
        this.sdf = sdf;
    }

    
    
    
    public String getListPictogramme(){
        String returnVal="";
        if (this.pictogrammes != null && pictogrammes.size()>0){
            for (Pictogramme p : pictogrammes){
                returnVal += "," + p.getIdPictogramme();
            }
            return returnVal.substring(1);
        }
        return returnVal;
    }
    
    @XmlTransient    
    public List<Pictogramme> getPictogrammes() {
        return pictogrammes;
    }    
    
    
    
        public Map<String, String> getDataForVerticalTable(){
        Map<String, String> editedData = new LinkedHashMap<>();        
        editedData.put("CAS :", this.getCas());
        editedData.put("EC :", this.getEc()); 
        editedData.put("Formule :", this.getFormule());
        editedData.put("Description :", StringEscapeUtils.unescapeHtml(this.getDescription()));  

        return editedData;        
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cas != null ? cas.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Substance)) {
            return false;
        }
        Substance other = (Substance) object;
        if ((this.cas == null && other.cas != null) || (this.cas != null && !this.cas.equals(other.cas))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "fr.univtours.Gpod.persistance.entities.Substance[ cas=" + cas + " ]";
    }

}
