/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2012/01/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/

package fr.univtours.gpod.persistance.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.apache.commons.lang.StringEscapeUtils;

/**
 *
 * @author geoffroy.vibrac
 */
@Entity
@Table(name = "produit")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Produit.findAll", query = "SELECT p FROM Produit p"),
    @NamedQuery(name = "Produit.findByIdProduit", query = "SELECT p FROM Produit p WHERE p.idProduit = :idProduit"),
    @NamedQuery(name = "Produit.findByNom", query = "SELECT p FROM Produit p WHERE p.nom = :nom"),
    @NamedQuery(name = "Produit.findByPurete", query = "SELECT p FROM Produit p WHERE p.purete = :purete")})
public class Produit implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @NotNull
    @Column(name = "idProduit")
    private Integer idProduit;
    @Size(max = 255)
    @Column(name = "nom")
    private String nom;
    @Column(name = "purete")
    private Double purete;
    @Column(name = "densite")
    private Double densite;
    @Column(name = "pointDeFusion")
    private Double pointDeFusion;    
    @Column(name = "pointdEbullition")
    private Double pointdEbullition;        
    @Column(name = "masseMolaire")
    private Double masseMolaire;        
    @JoinColumn(name = "CAS", referencedColumnName = "CAS")
    @ManyToOne(optional = false)
    private Substance substance;
    @JoinColumn(name = "idFournisseur", referencedColumnName = "idFournisseur")
    @ManyToOne
    private Fournisseur fournisseur;
    @JoinColumn(name = "idEtatPhysique", referencedColumnName = "idEtatPhysique")
    @ManyToOne
    private EtatPhysique etatPhysique;

    @Lob
    @Size(max = 65535)
    @Column(name = "description")
    private String description;
    @Size(max = 255)
    @Column(name = "ficheSecurite")
    private String ficheSecurite;
    @Column(name = "refrigere")
    private Boolean refrigere;
    
    @OneToMany
    @JoinTable(name="produitpictogramme",
    joinColumns= @JoinColumn(name = "idProduit"),
    inverseJoinColumns= @JoinColumn(name="idPictogramme"))
    private List<Pictogramme> pictogrammes;    
  
    @OneToMany
    @JoinTable(name="produitmention",
    joinColumns= @JoinColumn(name = "idProduit"),
    inverseJoinColumns= @JoinColumn(name="idmention"))
    private List<MentionDanger> mentions;   
 
    @OneToMany
    @JoinTable(name="produittag",
    joinColumns= @JoinColumn(name = "idProduit"),
    inverseJoinColumns= @JoinColumn(name="idtag"))
    private List<Tag> tags; 

    @OneToMany(mappedBy = "produit")
    private List<ProduitConditionnement> conditionnements;
    
    public Produit() {
    }

    public Produit(String nom) {
        this.nom = nom;
    }

    public Integer getIdProduit() {
        return idProduit;
    }

    public void setIdProduit(Integer idProduit) {
        this.idProduit = idProduit;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFicheSecurite() {
        return ficheSecurite;
    }

    public void setFicheSecurite(String ficheSecurite) {
        this.ficheSecurite = ficheSecurite;
    }
    
    
    

    public Double getPurete() {
        return purete;
    }

    public void setPurete(Double purete) {
        this.purete = purete;
    }
 
    
    public Substance getSubstance() {
        return substance;
    }

    public void setSubstance(Substance cas) {
        this.substance = cas;
    }



    public Fournisseur getFournisseur() {
        return fournisseur;
    }

    public void setFournisseur(Fournisseur idFournisseur) {
        this.fournisseur = idFournisseur;
    }

    public EtatPhysique getEtatPhysique() {
        return etatPhysique;
    }

    public void setEtatPhysique(EtatPhysique idEtatPhysique) {
        this.etatPhysique = idEtatPhysique;
    }

    public Double getDensite() {
        return densite;
    }

    public void setDensite(Double densite) {
        this.densite = densite;
    }

    public Double getPointDeFusion() {
        return pointDeFusion;
    }

    public void setPointDeFusion(Double pointDeFusion) {
        this.pointDeFusion = pointDeFusion;
    }
    
    public Double getPointdEbullition() {
        return pointdEbullition;
    }

    public void setPointdEbullition(Double pointdEbullition) {
        this.pointdEbullition = pointdEbullition;
    }
    
    public Double getMasseMolaire() {
        return masseMolaire;
    }

    public void setMasseMolaire(Double masseMolaire) {
        this.masseMolaire = masseMolaire;
    }    
    
    
    @XmlTransient
    public List<MentionDanger> getMentions() {
        return mentions;
    }

    public void setMentions(List<MentionDanger> mentions) {
        this.mentions = mentions;
    }

    @XmlTransient    
    public List<Pictogramme> getPictogrammes() {
        return pictogrammes;
    }
    
    public String getListPictogrammes(){
        String resultat = "";
        if (pictogrammes!=null){
            for (Pictogramme p : pictogrammes){
                resultat += "," + String.valueOf(p.getIdPictogramme());
            }
            if (resultat.length() > 0){
                resultat = resultat.substring(1);
            }
            
            
        }
        return resultat;
        
        
    }
    

    public void setPictogrammes(List<Pictogramme> pictogrammes) {
        this.pictogrammes = pictogrammes;
    }

    @XmlTransient    
    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    public Boolean isRefrigere() {
        return refrigere;
    }

    public void setRefrigere(Boolean refrigere) {
        this.refrigere = refrigere;
    }

    @XmlTransient    
    public List<ProduitConditionnement> getConditionnements() {
        return conditionnements;
    }

    public void setConditionnements(List<ProduitConditionnement> conditionnements) {
        this.conditionnements = conditionnements;
    }


    public Map<String, String> getDataForVerticalTable(){
        Map<String, String> editedData = new LinkedHashMap<>();        
        editedData.put("CAS :", this.getSubstance().getCas());
        editedData.put("Substance :", StringEscapeUtils.unescapeHtml(this.getSubstance().getNom()));
        editedData.put("Fournisseur :", this.getFournisseur().getNom());
        editedData.put("Description :", StringEscapeUtils.unescapeHtml(this.getDescription()));
        editedData.put("Pureté :", this.getPurete() + " %");
        editedData.put("Densite :", this.getDensite() + "");
        editedData.put("Point de fusion :", this.getPointDeFusion() + " °C");
        editedData.put("Point d'ébullition :", this.getPointdEbullition() + " °C");
        editedData.put("Masse Molaire :", this.getMasseMolaire() + " g/mol");
        if (this.getEtatPhysique()!=null){
            editedData.put("Etat Physique : ", this.getEtatPhysique().getLibelle());
        }
        editedData.put("Refrigeration : ", this.isRefrigere()?"Oui":"Non");
        editedData.put("Fiche sécurité", this.getFicheSecurite());     

        return editedData;        
    }
    
    public static List<String[]> getDataForHorizontalTable(List<Produit> produits){
        List <String[]> editedData = new ArrayList<String[]>();
        if (produits!=null){
            editedData.add(new String[]{"CAS","Substance", "Fournisseur", "Description", "Pureté", "Densite", "Point de fusion", "Point d'ébullition", "Masse Molaire", "Etat Physique", "Refrigiration", "Fiche sécurité"});
            for (Produit produit : produits){
                editedData.add(new String[]{produit.getSubstance().getCas(),
                                            StringEscapeUtils.unescapeHtml(produit.getSubstance().getNom()),
                                            produit.getFournisseur().getNom(),
                                            StringEscapeUtils.unescapeHtml(produit.getDescription()),
                                            produit.getPurete() + " %",
                                            produit.getDensite()+"",
                                            produit.getPointDeFusion()+ " °C",
                                            produit.getPointdEbullition()+ " °C",
                                            produit.getMasseMolaire()+ " g/mol",
                                            produit.getEtatPhysique().getLibelle(),
                                            produit.isRefrigere()?"Oui":"Non",
                                            produit.getFicheSecurite()                
                });
            }
        }
        return editedData;
    }    
    
    
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idProduit != null ? idProduit.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Produit)) {
            return false;
        }
        Produit other = (Produit) object;
        if ((this.idProduit == null && other.idProduit != null) || (this.idProduit != null && !this.idProduit.equals(other.idProduit))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "fr.univtours.Gpod.persistance.entities.Produit[ idProduit=" + idProduit + " ]";
    }

}
