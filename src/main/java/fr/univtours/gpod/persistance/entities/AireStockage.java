/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2012/01/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/

package fr.univtours.gpod.persistance.entities;

import fr.univtours.gpod.utils.Utilities;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author geoffroy.vibrac
 */
@Entity
@Table(name = "airestockage")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AireStockage.findAll", query = "SELECT p FROM AireStockage p"),
    @NamedQuery(name = "AireStockage.findByIdAire", query = "SELECT p FROM AireStockage p WHERE p.idAire = :idAire"),
    @NamedQuery(name = "AireStockage.findBySalleStockage", query = "SELECT p FROM AireStockage p WHERE p.salleStockage = :salleStockage"),
    @NamedQuery(name = "AireStockage.findByNom", query = "SELECT p FROM AireStockage p WHERE p.nom = :nom"),
    @NamedQuery(name = "AireStockage.findByCode", query = "SELECT p FROM AireStockage p WHERE p.code = :code")})
public class AireStockage implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @NotNull
    @Column(name = "idAire")
    private Integer idAire;
    @JoinColumn(name = "idTypeAire", referencedColumnName = "idTypeAire")
    @ManyToOne(optional = false)
    private TypeAire typeAire; 
    
    @JoinColumn(name = "idSalleStockage", referencedColumnName = "idSalleStockage")
    @ManyToOne(optional = false)
    private SalleStockage salleStockage;   
    
    @Size(max = 255)
    @Column(name = "nom")
    private String nom;
    @Size(min = 1, max = 25)
    @Column(name = "code")
    private String code;
    @Lob
    @Size(max = 65535)
    @Column(name = "description")
    private String description;
    @Column(name = "refrigere")
    private Boolean refrigere;

    public AireStockage() {
    }

    public AireStockage(TypeAire typeAire, SalleStockage salleStockage, String nom, String code, String description, boolean refrigere) {
        this.typeAire = typeAire; 
        this.salleStockage = salleStockage; 
        this.nom = nom;
        this.code = code;
        this.description = description;
        this.refrigere = refrigere;
    }

    public Integer getIdAire() {
        return idAire;
    }

    public TypeAire getTypeAire() {
        return typeAire;
    }

    public void setTypeAire(TypeAire typeAire) {
        this.typeAire = typeAire;
    }

    public SalleStockage getSalleStockage() {
        return salleStockage;
    }

    public void setSalleStockage(SalleStockage salleStockage) {
        this.salleStockage = salleStockage;
    }
  
    
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Boolean isRefrigere() {
        return refrigere;
    }

    public void setRefrigere(Boolean refrigere) {
        this.refrigere = refrigere;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    

    // permet de retourner une liste d'idAire séparée par des virgule à partir d'une liste d'aire
    // utile pour les requetes sql dans les IN (a,b,c,d)
    public static String getIdAireFromListAire(List<AireStockage> listAires){
        String stringWithComma = "";
        if (listAires.size() > 0){
            for(AireStockage as : listAires){
                stringWithComma += ","+as.idAire;
            }
            stringWithComma = stringWithComma.substring(1);
        }
        return stringWithComma;
    }

    public Map<String, String> getDataForVerticalTable(){
        Map<String, String> editedData = new LinkedHashMap<String, String>();    
        editedData.put("Nom :", this.getNom());       
        editedData.put("Code :", this.getCode());
        editedData.put("Type :",  this.getTypeAire().getNom());          
        editedData.put("Salle de Stockage : ", this.getSalleStockage().getNom());
        editedData.put("Description : ", this.getDescription());
        editedData.put("Refrigéré :", Utilities.booleanToOuiNon(this.isRefrigere()));       
   
        
        return editedData;        
    }
    
    public static List<String[]> getDataForHorizontalTable(List<AireStockage> aires){
        List <String[]> editedData = new ArrayList<String[]>();
        if (aires!=null){
            editedData.add(new String[]{"Nom","Code", "Type", "Salle de Stockage", "Description", "Refrigéré"});
            for (AireStockage aire : aires){
                editedData.add(new String[]{aire.getNom(),
                                            aire.getCode(),
                                            aire.getTypeAire().getNom(),
                                            aire.getSalleStockage().getNom(),
                                            aire.getDescription(),
                                            Utilities.booleanToOuiNon(aire.isRefrigere())                
                });
            }
        }
        return editedData;
    }     
    
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idAire != null ? idAire.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof AireStockage)) {
            return false;
        }
        AireStockage other = (AireStockage) object;
        if ((this.idAire == null && other.idAire != null) || (this.idAire != null && !this.idAire.equals(other.idAire))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "fr.univtours.Gpod.persistance.entities.Aire[ idAire=" + idAire + " ]";
    }

}
