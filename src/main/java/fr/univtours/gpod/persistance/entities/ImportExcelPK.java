package fr.univtours.gpod.persistance.entities;

import javax.persistence.Embeddable;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Created with IntelliJ IDEA.
 * User: geoffroy.vibrac
 * Date: 03/04/13
 * Time: 15:02
 */
@Embeddable
public class ImportExcelPK implements Serializable {

    private String cas;
    private String nom;
    private String fourniseur;
    private double quantite;
    private String uniteMesure;
    private double quantiteUnitaire;
    private String formuleBrut;
    private Timestamp dateImport;

    public ImportExcelPK() {
    }

    public ImportExcelPK(String cas, Timestamp dateImport, String formuleBrut, String fourniseur, String nom, double quantite, double quantiteUnitaire, String uniteMesure) {
        this.cas = cas;
        this.dateImport = dateImport;
        this.formuleBrut = formuleBrut;
        this.fourniseur = fourniseur;
        this.nom = nom;
        this.quantite = quantite;
        this.quantiteUnitaire = quantiteUnitaire;
        this.uniteMesure = uniteMesure;
    }

    public String getCas() {
        return cas;
    }

    public void setCas(String cas) {
        this.cas = cas;
    }

    public Timestamp getDateImport() {
        return dateImport;
    }

    public void setDateImport(Timestamp dateImport) {
        this.dateImport = dateImport;
    }

    public String getFormuleBrut() {
        return formuleBrut;
    }

    public void setFormuleBrut(String formuleBrut) {
        this.formuleBrut = formuleBrut;
    }

    public String getFourniseur() {
        return fourniseur;
    }

    public void setFourniseur(String fourniseur) {
        this.fourniseur = fourniseur;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public double getQuantite() {
        return quantite;
    }

    public void setQuantite(double quantite) {
        this.quantite = quantite;
    }

    public double getQuantiteUnitaire() {
        return quantiteUnitaire;
    }

    public void setQuantiteUnitaire(double quantiteUnitaire) {
        this.quantiteUnitaire = quantiteUnitaire;
    }

    public String getUniteMesure() {
        return uniteMesure;
    }

    public void setUniteMesure(String uniteMesure) {
        this.uniteMesure = uniteMesure;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ImportExcelPK that = (ImportExcelPK) o;

        if (Double.compare(that.quantite, quantite) != 0) return false;
        if (Double.compare(that.quantiteUnitaire, quantiteUnitaire) != 0) return false;
        if (cas != null ? !cas.equals(that.cas) : that.cas != null) return false;
        if (dateImport != null ? !dateImport.equals(that.dateImport) : that.dateImport != null) return false;
        if (formuleBrut != null ? !formuleBrut.equals(that.formuleBrut) : that.formuleBrut != null) return false;
        if (fourniseur != null ? !fourniseur.equals(that.fourniseur) : that.fourniseur != null) return false;
        if (nom != null ? !nom.equals(that.nom) : that.nom != null) return false;
        if (uniteMesure != null ? !uniteMesure.equals(that.uniteMesure) : that.uniteMesure != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = cas != null ? cas.hashCode() : 0;
        result = 31 * result + (nom != null ? nom.hashCode() : 0);
        result = 31 * result + (fourniseur != null ? fourniseur.hashCode() : 0);
        temp = quantite != +0.0d ? Double.doubleToLongBits(quantite) : 0L;
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (uniteMesure != null ? uniteMesure.hashCode() : 0);
        temp = quantiteUnitaire != +0.0d ? Double.doubleToLongBits(quantiteUnitaire) : 0L;
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (formuleBrut != null ? formuleBrut.hashCode() : 0);
        result = 31 * result + (dateImport != null ? dateImport.hashCode() : 0);
        return result;
    }


}

