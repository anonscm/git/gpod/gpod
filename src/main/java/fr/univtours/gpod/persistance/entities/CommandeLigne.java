/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2012/01/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/


package fr.univtours.gpod.persistance.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author geoffroy.vibrac
 */
@Entity
@Table(name = "commandeligne")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CommandeLigne.findAll", query = "SELECT c FROM CommandeLigne c"),
    @NamedQuery(name = "CommandeLigne.findByIdCommandeLigne", query = "SELECT c FROM CommandeLigne c WHERE c.idCommandeLigne = :idCommandeLigne"),
    @NamedQuery(name = "CommandeLigne.findByQuantite", query = "SELECT c FROM CommandeLigne c WHERE c.quantite = :quantite")})

public class CommandeLigne implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @NotNull
    @Column(name = "idCommandeLigne")
    private Integer idCommandeLigne;
    @Basic(optional = false)
    @NotNull
    @Column(name = "quantite")
    private int quantite;
    
    @JoinColumn(name = "idProduitConditionnement", referencedColumnName = "idProduitConditionnement")
    @ManyToOne(optional = false)
    private ProduitConditionnement produitConditionnement;
    
    @ManyToOne(optional = false)
    @JoinColumn(name = "idCommande", referencedColumnName = "idCommande")    
    private Commande commande;       
//    @JoinColumn(name = "idCommande", referencedColumnName = "idCommande")
//    @ManyToOne
//    private Commande commande;
//    @ManyToOne
//    @JoinTable(name = "CommandeCommandeLigne",
//        joinColumns = @JoinColumn(name = "idCommandeLigne"),
//        inverseJoinColumns = @JoinColumn(name = "idCommande") )
//    private Commande commande;  
    
    public CommandeLigne() {
    }

    public CommandeLigne(ProduitConditionnement produitConditionnement, int quantite) {
        this.produitConditionnement = produitConditionnement;
        this.quantite = quantite;
    }
    
    public CommandeLigne(int idCommandeLigne, ProduitConditionnement produitConditionnement, int quantite) {
        this.idCommandeLigne = idCommandeLigne;
        this.produitConditionnement = produitConditionnement;
        this.quantite = quantite;
    }

    public Integer getIdCommandeLigne() {
        return idCommandeLigne;
    }

    public void setIdCommandeLigne(Integer idCommandeLigne) {
        this.idCommandeLigne = idCommandeLigne;
    }

    public int getQuantite() {
        return quantite;
    }

    public void setQuantite(int quantite) {
        this.quantite = quantite;
    }

    public ProduitConditionnement getProduitConditionnement() {
        return produitConditionnement;
    }

    public void setProduitConditionnement(ProduitConditionnement idProduitConditionnement) {
        this.produitConditionnement = idProduitConditionnement;
    }
    
    

    public Commande getCommande() {
        return commande;
    }

    public void setCommande(Commande commande) {
        this.commande = commande;
    }
    
    // Pour qu'une Ligne de commande soit "supprimable"
    // il faut qu'elle ai été faite par l'utilisateur et que la commande soit à l'étape 3 (attente de livraison)
    public boolean isDeletable(Utilisateur user){
        return this.getCommande().getUtilisateur().equals(user) && this.getCommande().getStatut().getIdStatut()==3; 
    }    


//    public int getQuantiteRecue(){
//        int quantiteRecue=0;
//        if (this.getLivraisonLigneList()!=null && this.getLivraisonLigneList().size()>0){
//            for (LivraisonLigne ll : getLivraisonLigneList()){
//                quantiteRecue += ll.getQuantite();
//            }
//        }
//        return quantiteRecue;
//    }
//    
//    public int getPourcentageRecu(){
//        return (this.getQuantiteRecue()*100)/this.getQuantite();
//    }
//    
//    public int getQuantiteResteAlivrer(){
//        return this.getQuantite() - this.getQuantiteRecue();
//    }
    
    


    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCommandeLigne != null ? idCommandeLigne.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof CommandeLigne)) {
            return false;
        }
        CommandeLigne other = (CommandeLigne) object;
        if ((this.idCommandeLigne == null && other.idCommandeLigne != null) || (this.idCommandeLigne != null && !this.idCommandeLigne.equals(other.idCommandeLigne))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "fr.univtours.gpod.persistance.entities.CommandeLigne[ idCommandeLigne=" + idCommandeLigne + " ]";
    }
    
}
