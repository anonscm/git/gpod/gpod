/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2012/01/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/


package fr.univtours.gpod.persistance.entities;

import fr.univtours.gpod.utils.Utilities;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

/**
 *
 * @author geoffroy.vibrac
 */
@Entity
@Table(name = "historiqueconsomation")
@NamedQueries({
    @NamedQuery(name = "HistoriqueConsomation.findAll", query = "SELECT h FROM HistoriqueConsomation h"),
    @NamedQuery(name = "HistoriqueConsomation.findByIdHistoriqueConsommation", query = "SELECT h FROM HistoriqueConsomation h WHERE h.idHistoriqueConsommation = :idHistoriqueConsommation"),
    @NamedQuery(name = "HistoriqueConsomation.findByDateConsommation", query = "SELECT h FROM HistoriqueConsomation h WHERE h.dateConsommation = :dateConsommation"),
    @NamedQuery(name = "HistoriqueConsomation.findByQuantiteRestante", query = "SELECT h FROM HistoriqueConsomation h WHERE h.quantiteRestante = :quantiteRestante")})
public class HistoriqueConsomation implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idHistoriqueConsommation")
    private Integer idHistoriqueConsommation;
    @Column(name = "dateConsommation")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateConsommation;
    @Column(name = "quantiteRestante")
    private Double quantiteRestante;
    @Lob
    @Column(name = "commentaire")
    private String commentaire;

    @JoinColumn(name = "idArticle", referencedColumnName = "idArticle")
    @ManyToOne(optional = false)
    private Article article;

    @JoinColumn(name = "idUtilisateur", referencedColumnName = "idUtilisateur")
    @ManyToOne(optional = false)
    private Utilisateur utilisateur;
    
    @Size(max = 255)
    @Column(name = "affectationutilisateur")
    private String affectationUtilisateur;    

    @JoinColumn(name = "idSousStructure", referencedColumnName = "idSousStructure")
    @ManyToOne(optional = false)
    private SousStructure sousStructure;

    @JoinColumn(name = "idAire", referencedColumnName = "idAire")
    @ManyToOne(optional = false)
    private AireStockage aireStockage;
    
    
    public HistoriqueConsomation() {
    }

    public HistoriqueConsomation(Integer idHistoriqueConsommation) {
        this.idHistoriqueConsommation = idHistoriqueConsommation;
    }

    public Integer getIdHistoriqueConsommation() {
        return idHistoriqueConsommation;
    }

    public void setIdHistoriqueConsommation(Integer idHistoriqueConsommation) {
        this.idHistoriqueConsommation = idHistoriqueConsommation;
    }

    public Date getDateConsommation() {
        return dateConsommation;
    }

    public void setDateConsommation(Date dateConsommation) {
        this.dateConsommation = dateConsommation;
    }

    public Double getQuantiteRestante() {
        return quantiteRestante;
    }

    public void setQuantiteRestante(Double quantiteRestante) {
        this.quantiteRestante = quantiteRestante;
    }

    public String getCommentaire() {
        return commentaire;
    }

    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

    public Article getArticle() {
        return article;
    }

    public void setArticle(Article article) {
        this.article = article;
    }

    public Utilisateur getUtilisateur() {
        return utilisateur;
    }

    public void setUtilisateur(Utilisateur utilisateur) {
        this.utilisateur = utilisateur;
    }

    public String getAffectationUtilisateur() {
        return affectationUtilisateur;
    }

    public void setAffectationUtilisateur(String affectationUtilisateur) {
        this.affectationUtilisateur = affectationUtilisateur;
    }

    public AireStockage getAireStockage() {
        return aireStockage;
    }

    public void setAireStockage(AireStockage aireStockage) {
        this.aireStockage = aireStockage;
    }

    public SousStructure getSousStructure() {
        return sousStructure;
    }

    public void setSousStructure(SousStructure sousStructure) {
        this.sousStructure = sousStructure;
    }
    
    
    public static List<String[]> getDataForHorizontalTable(List<HistoriqueConsomation> hcList){
        List <String[]> editedData = new ArrayList<String[]>();
        if (hcList!=null){
            editedData.add(new String[]{"Date consommation","Utilisateur", "Aire de Stockage", "Qté restante", "Commentaire", "Affectation"});
            for (HistoriqueConsomation hc : hcList){
                String dateConso = Utilities.javaDateToString(hc.getDateConsommation());
                editedData.add(new String[]{dateConso,
                                            hc.getUtilisateur().getPrenom() + " " + hc.getUtilisateur().getNom(),
                                            hc.getAireStockage().getSalleStockage().getNom() + " > " + hc.getAireStockage().getNom(),
                                            hc.getQuantiteRestante().toString(),
                                            hc.getCommentaire(),
                                            hc.getAffectationUtilisateur()              
                });
            }
        }
        return editedData;
    }     

    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idHistoriqueConsommation != null ? idHistoriqueConsommation.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof HistoriqueConsomation)) {
            return false;
        }
        HistoriqueConsomation other = (HistoriqueConsomation) object;
        if ((this.idHistoriqueConsommation == null && other.idHistoriqueConsommation != null) || (this.idHistoriqueConsommation != null && !this.idHistoriqueConsommation.equals(other.idHistoriqueConsommation))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "fr.univtours.Gpod.persistance.entities.Historiqueconsomation[idHistoriqueConsommation=" + idHistoriqueConsommation + "]";
    }

}
