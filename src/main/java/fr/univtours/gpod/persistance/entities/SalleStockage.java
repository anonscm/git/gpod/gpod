/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2012/01/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/


package fr.univtours.gpod.persistance.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author geoffroy.vibrac
 */
@Entity
@Table(name = "sallestockage")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SalleStockage.findAll", query = "SELECT p FROM SalleStockage p"),
    @NamedQuery(name = "SalleStockage.findByIdAire", query = "SELECT p FROM SalleStockage p WHERE p.idSalleStockage = :idSalleStockage"),
    @NamedQuery(name = "SalleStockage.findByBatiment", query = "SELECT p FROM SalleStockage p WHERE p.batiment = :batiment"),
    @NamedQuery(name = "SalleStockage.findByNom", query = "SELECT p FROM SalleStockage p WHERE p.nom = :nom"),
    @NamedQuery(name = "SalleStockage.findByCode", query = "SELECT p FROM SalleStockage p WHERE p.code = :code")})
public class SalleStockage implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @NotNull
    @Column(name = "idSalleStockage")
    private Integer idSalleStockage;
    
    @JoinColumn(name = "idBatiment", referencedColumnName = "idBatiment")
    @ManyToOne(optional = false)
    private Batiment batiment;   
    
    @Size(max = 255)
    @Column(name = "nom")
    private String nom;
    @Size(min = 1, max = 25)
    @Column(name = "code")
    private String code;
    @Lob
    @Size(max = 65535)
    @Column(name = "description")
    private String description;
    
    @OneToMany(mappedBy = "salleStockage")
    private List<AireStockage> aires;    


    public SalleStockage() {
    }

    public SalleStockage(Batiment batiment, String nom, String code, String description) {
        this.batiment = batiment; 
        this.nom = nom;
        this.code = code;
        this.description = description;

    }

    public Integer getIdSalle() {
        return idSalleStockage;
    }


    public Batiment getBatiment() {
        return batiment;
    }

    public void setBatiment(Batiment batiment) {
        this.batiment = batiment;
    }
  
    
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
 
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    

    @XmlTransient
    public List<AireStockage> getAires() {
        return aires;
    }

    
    public void setAires(List<AireStockage> aires) {
        this.aires = aires;
    }    

    // permet de retourner une liste d'idSalleStockage séparée par des virgule à partir d'une liste de salle
    // utile pour les requetes sql dans les IN (a,b,c,d)
    public static String getIdSalleFromListSalle(List<SalleStockage> listSalles){
        String stringWithComma = "";
        if (listSalles.size() > 0){
            for(SalleStockage as : listSalles){
                stringWithComma += ","+as.idSalleStockage;
            }
            stringWithComma = stringWithComma.substring(1);
        }
        return stringWithComma;
    }

    public Map<String, String> getDataForVerticalTable(){
        Map<String, String> editedData = new LinkedHashMap<String, String>();    
        editedData.put("Nom :", this.getNom());       
        editedData.put("Code :", this.getCode());
        editedData.put("Batiment : ", this.getBatiment().getNom());
        editedData.put("Description :", this.getDescription());
  
   
        
        return editedData;        
    }
    
    public static List<String[]> getDataForHorizontalTable(List<SalleStockage> salles){
        List <String[]> editedData = new ArrayList<String[]>();
        if (salles!=null){
            editedData.add(new String[]{"Nom","Code", "Batiment", "Description"});
            for (SalleStockage salle : salles){
                editedData.add(new String[]{salle.getNom(),
                                            salle.getCode(),
                                            salle.getBatiment().getNom(),
                                            salle.getDescription()             
                });
            }
        }
        return editedData;
    }     
    
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idSalleStockage != null ? idSalleStockage.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof SalleStockage)) {
            return false;
        }
        SalleStockage other = (SalleStockage) object;
        if ((this.idSalleStockage == null && other.idSalleStockage != null) || (this.idSalleStockage != null && !this.idSalleStockage.equals(other.idSalleStockage))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "fr.univtours.Gpod.persistance.entities.SalleStockage[ idSalleStockage=" + idSalleStockage + " ]";
    }

}
