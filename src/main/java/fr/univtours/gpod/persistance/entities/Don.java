package fr.univtours.gpod.persistance.entities;

import javax.persistence.*;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: geoffroy.vibrac
 * Date: 28/02/14
 * Time: 09:45
 */
@Entity
@Table(name = "don")
public class Don {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idDon")
    private int idDon;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "dateFinDon")
    private Date dateFinDon;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "dateDon")
    private Date dateDon;
    @Column(name = "accepte")
    private boolean accepte;

    @JoinColumn(name = "idUtilisateurOffrant", referencedColumnName = "idUtilisateur")
    @ManyToOne(optional = false)
    private Utilisateur utilisateurOffrant;

    @JoinColumn(name = "idUtilisateurRecevant", referencedColumnName = "idUtilisateur")
    @ManyToOne(optional = false)
    private Utilisateur utilisateurRecevant;

    @JoinColumn(name = "idSousStructure", referencedColumnName = "idSousStructure")
    @ManyToOne(optional = false)
    private SousStructure sousStructure;

    @JoinColumn(name = "idAire", referencedColumnName = "idAire")
    @ManyToOne(optional = false)
    private AireStockage aireStockage;

    @JoinColumn(name = "idArticle", referencedColumnName = "idArticle")
    @ManyToOne(optional = false)
    private Article article;

    public int getIdDon() {
        return idDon;
    }

    public SousStructure getSousStructure() {
        return sousStructure;
    }

    public void setSousStructure(SousStructure sousStructure) {
        this.sousStructure = sousStructure;
    }


    public AireStockage getAireStockage() {
        return aireStockage;
    }

    public void setAireStockage(AireStockage aireStockage) {
        this.aireStockage = aireStockage;
    }

    public Article getArticle() {
        return article;
    }

    public void setArticle(Article article) {
        this.article = article;
    }

    public Utilisateur getUtilisateurOffrant() {
        return utilisateurOffrant;
    }

    public void setUtilisateurOffrant(Utilisateur utilisateurOffrant) {
        this.utilisateurOffrant = utilisateurOffrant;
    }

    public Utilisateur getUtilisateurRecevant() {
        return utilisateurRecevant;
    }

    public void setUtilisateurRecevant(Utilisateur utilisateurRecevant) {
        this.utilisateurRecevant = utilisateurRecevant;
    }

    @javax.persistence.Column(name = "dateFinDon", nullable = false, insertable = true, updatable = true, length = 19, precision = 0)
    @Basic
    public Date getDateFinDon() {
        return dateFinDon;
    }

    public void setDateFinDon(Date dateFinDon) {
        this.dateFinDon = dateFinDon;
    }

    @javax.persistence.Column(name = "dateDon", nullable = false, insertable = true, updatable = true, length = 19, precision = 0)
    @Basic
    public Date getDateDon() {
        return dateDon;
    }

    public void setDateDon(Date dateDon) {
        this.dateDon = dateDon;
    }

    @javax.persistence.Column(name = "Accepte", nullable = false, insertable = true, updatable = true, length = 0, precision = 0)
    @Basic
    public boolean isAccepte() {
        return accepte;
    }

    public void setAccepte(boolean accepte) {
        this.accepte = accepte;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Don don = (Don) o;

        if (accepte != don.accepte) return false;
        if (idDon != don.idDon) return false;
        if (utilisateurOffrant.getIdUtilisateur() != don.utilisateurOffrant.getIdUtilisateur()) return false;
        if (utilisateurRecevant.getIdUtilisateur() != don.utilisateurRecevant.getIdUtilisateur()) return false;
        if (dateDon != null ? !dateDon.equals(don.dateDon) : don.dateDon != null) return false;
        if (dateFinDon != null ? !dateFinDon.equals(don.dateFinDon) : don.dateFinDon != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idDon;
        result = 31 * result + (int)(utilisateurOffrant.getIdUtilisateur()^(utilisateurOffrant.getIdUtilisateur()>>>32));
        result = 31 * result + (int)(utilisateurRecevant.getIdUtilisateur()^(utilisateurRecevant.getIdUtilisateur()>>>32));
        result = 31 * result + (dateFinDon != null ? dateFinDon.hashCode() : 0);
        result = 31 * result + (dateDon != null ? dateDon.hashCode() : 0);
        result = 31 * result + (accepte ? 1 : 0);
        return result;
    }
}
