/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2012/01/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/


package fr.univtours.gpod.persistance.entities;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author geoffroy.vibrac
 */
@Entity
@Table(name = "fournisseur")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Fournisseur.findAll", query = "SELECT f FROM Fournisseur f"),
    @NamedQuery(name = "Fournisseur.findByIdFournisseur", query = "SELECT f FROM Fournisseur f WHERE f.idFournisseur = :idFournisseur"),
    @NamedQuery(name = "Fournisseur.findByReferenceSIFAC", query = "SELECT f FROM Fournisseur f WHERE f.referenceSIFAC = :referenceSIFAC"),
    @NamedQuery(name = "Fournisseur.findByNom", query = "SELECT f FROM Fournisseur f WHERE f.nom = :nom"),
    @NamedQuery(name = "Fournisseur.findByAdresse1", query = "SELECT f FROM Fournisseur f WHERE f.adresse1 = :adresse1"),
    @NamedQuery(name = "Fournisseur.findByAdresse2", query = "SELECT f FROM Fournisseur f WHERE f.adresse2 = :adresse2"),
    @NamedQuery(name = "Fournisseur.findByCodePostal", query = "SELECT f FROM Fournisseur f WHERE f.codePostal = :codePostal"),
    @NamedQuery(name = "Fournisseur.findByVille", query = "SELECT f FROM Fournisseur f WHERE f.ville = :ville"),
    @NamedQuery(name = "Fournisseur.findByNomContact", query = "SELECT f FROM Fournisseur f WHERE f.nomContact = :nomContact"),
    @NamedQuery(name = "Fournisseur.findByTelephone", query = "SELECT f FROM Fournisseur f WHERE f.telephone = :telephone"),
    @NamedQuery(name = "Fournisseur.findByFax", query = "SELECT f FROM Fournisseur f WHERE f.fax = :fax")})
public class Fournisseur implements Serializable {
        @OneToMany(mappedBy = "fournisseur")
    private List<Produit> produitList;
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @NotNull
    @Column(name = "idFournisseur")
    private Integer idFournisseur;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "referenceSIFAC")
    private String referenceSIFAC;
    @Size(max = 45)
    @Column(name = "nom")
    private String nom;
    @Size(max = 255)
    @Column(name = "adresse1")
    private String adresse1;
    @Size(max = 255)
    @Column(name = "adresse2")
    private String adresse2;
    @Size(max = 6)
    @Column(name = "codePostal")
    private String codePostal;
    @Size(max = 255)
    @Column(name = "ville")
    private String ville;
    @Size(max = 45)
    @Column(name = "nomContact")
    private String nomContact;
    @Size(max = 12)
    @Column(name = "telephone")
    private String telephone;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 12)
    @Column(name = "fax")
    private String fax;

    public Fournisseur() {
    }

    public Fournisseur(Integer idFournisseur) {
        this.idFournisseur = idFournisseur;
    }

    public Fournisseur(String referenceSIFAC, String nom, String adresse1, String adresse2, String codePostal, String ville, String nomContact, String telephone, String fax) {
        this.referenceSIFAC = referenceSIFAC;
        this.nom = nom;
        this.adresse1 = adresse1;
        this.adresse2 = adresse2;
        this.codePostal = codePostal;
        this.ville = ville;
        this.nomContact = nomContact;
        this.telephone = telephone;
        this.fax = fax;
    }


    public Integer getIdFournisseur() {
        return idFournisseur;
    }

    public void setIdFournisseur(Integer idFournisseur) {
        this.idFournisseur = idFournisseur;
    }

    public String getReferenceSIFAC() {
        return referenceSIFAC;
    }

    public void setReferenceSIFAC(String referenceSIFAC) {
        this.referenceSIFAC = referenceSIFAC;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getAdresse1() {
        return adresse1;
    }

    public void setAdresse1(String adresse1) {
        this.adresse1 = adresse1;
    }

    public String getAdresse2() {
        return adresse2;
    }

    public void setAdresse2(String adresse2) {
        this.adresse2 = adresse2;
    }

    public String getCodePostal() {
        return codePostal;
    }

    public void setCodePostal(String codePostal) {
        this.codePostal = codePostal;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public String getNomContact() {
        return nomContact;
    }

    public void setNomContact(String nomContact) {
        this.nomContact = nomContact;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public Map<String, String> getDataForVerticalTable(){
        Map<String, String> editedData = new LinkedHashMap<String, String>();        
        editedData.put("Reference Sifac :",  this.referenceSIFAC);          
        editedData.put("Nom : ", this.nom);
        editedData.put("Adresse :", this.adresse1);
        editedData.put("Complement :", this.adresse2);
        editedData.put("Code Postal :", this.codePostal);
        editedData.put("Ville :", this.ville);
        editedData.put("Telephone :", this.telephone);
        editedData.put("Fax :", this.fax);
        editedData.put("Contact : ", this.nomContact);        
   
        
        return editedData;        
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idFournisseur != null ? idFournisseur.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Fournisseur)) {
            return false;
        }
        Fournisseur other = (Fournisseur) object;
        if ((this.idFournisseur == null && other.idFournisseur != null) || (this.idFournisseur != null && !this.idFournisseur.equals(other.idFournisseur))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "fr.univtours.Gpod.persistance.entities.Fournisseur[ idFournisseur=" + idFournisseur + " ]";
    }

    @XmlTransient
    public List<Produit> getProduitList() {
        return produitList;
    }

    public void setProduitList(List<Produit> produitList) {
        this.produitList = produitList;
    }

}
