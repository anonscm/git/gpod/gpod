/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2012/01/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/


package fr.univtours.gpod.persistance.entities;

import fr.univtours.gpod.Controller.ActionController;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Query;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author geoffroy.vibrac
 */
@Entity
@Table(name = "utilisateurprofil")
@NamedQueries({
    @NamedQuery(name = "UtilisateurProfil.findAll", query = "SELECT u FROM UtilisateurProfil u"),
    @NamedQuery(name = "UtilisateurProfil.findByIdUtilisateurProfil", query = "SELECT u FROM UtilisateurProfil u WHERE u.idUtilisateurProfil = :idUtilisateurProfil"),
    @NamedQuery(name = "UtilisateurProfil.findByNom", query = "SELECT u FROM UtilisateurProfil u WHERE u.nom = :nom"),
    @NamedQuery(name = "UtilisateurProfil.findByCode", query = "SELECT u FROM UtilisateurProfil u WHERE u.code = :code")})	
@XmlRootElement
public class UtilisateurProfil implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @NotNull
    private Integer idUtilisateurProfil;
    @Size(max = 45)
    private String nom;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    private String code;

    public UtilisateurProfil() {
    }

    public UtilisateurProfil(Integer idUtilisateurProfil) {
        this.idUtilisateurProfil = idUtilisateurProfil;
    }

    public UtilisateurProfil(Integer idUtilisateurProfil, String code) {
        this.idUtilisateurProfil = idUtilisateurProfil;
        this.code = code;
    }

    public UtilisateurProfil findByCode(String code, EntityManager em){
        Query query = em.createNamedQuery("findByCode");
        query.setParameter("code", code);
        return (UtilisateurProfil) query.getSingleResult();
    }
    
    
    public Integer getIdUtilisateurProfil() {
        return idUtilisateurProfil;
    }

    public void setIdUtilisateurProfil(Integer idUtilisateurProfil) {
        this.idUtilisateurProfil = idUtilisateurProfil;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
       

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idUtilisateurProfil != null ? idUtilisateurProfil.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof UtilisateurProfil)) {
            return false;
        }
        UtilisateurProfil other = (UtilisateurProfil) object;
        if ((this.idUtilisateurProfil == null && other.idUtilisateurProfil != null) || (this.idUtilisateurProfil != null && !this.idUtilisateurProfil.equals(other.idUtilisateurProfil))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "fr.univtours.Gpod.persistance.Utilisateurprofil[ idUtilisateurProfil=" + idUtilisateurProfil + " ]";
    }

}
