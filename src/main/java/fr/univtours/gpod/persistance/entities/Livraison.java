/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2012/01/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/


package fr.univtours.gpod.persistance.entities;

import fr.univtours.gpod.utils.Utilities;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author geoffroy.vibrac
 */
@Entity
@Table(name = "livraison")
@NamedQueries({
    @NamedQuery(name = "Livraison.findAll", query = "SELECT l FROM Livraison l"),
    @NamedQuery(name = "Livraison.findByIdLivraison", query = "SELECT l FROM Livraison l WHERE l.idLivraison = :idLivraison"),
    @NamedQuery(name = "Livraison.findByDateLivraison", query = "SELECT l FROM Livraison l WHERE l.dateLivraison = :dateLivraison"),
    @NamedQuery(name = "Livraison.findByNumeroBonLivraison", query = "SELECT l FROM Livraison l WHERE l.numeroBonLivraison = :numeroBonLivraison"),
     @NamedQuery(name = "Livraison.findWithoutCommande", query = "SELECT l FROM Livraison l WHERE l.commande IS NULL"),
    @NamedQuery(name = "Livraison.findWithoutCommandeByIdUtilisateur", query = "SELECT l FROM Livraison l WHERE l.commande IS NULL AND l.utilisateur = :utilisateur"),
    @NamedQuery(name = "Livraison.countWithoutCommandeByIdUtilisateur", query = "SELECT count(l) FROM Livraison l WHERE l.commande IS NULL AND l.utilisateur = :utilisateur"),
    @NamedQuery(name = "Livraison.findByIdUtilisateur", query = "SELECT l FROM Livraison l WHERE l.utilisateur = :utilisateur")})
@XmlRootElement
public class Livraison implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idLivraison")
    private Integer idLivraison;
    @Basic(optional = false)
    @Column(name = "dateLivraison")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateLivraison;
    @Column(name = "numeroBonLivraison")
    private String numeroBonLivraison;

    @JoinColumn(name = "idCommande", referencedColumnName = "idCommande")
    @ManyToOne(optional = false)
    private Commande commande;   

    @JoinColumn(name = "idUtilisateur", referencedColumnName = "idUtilisateur")
    @ManyToOne(optional = false)
    private Utilisateur utilisateur;

    @OneToMany(mappedBy = "livraison", cascade= CascadeType.ALL)
    @JoinColumn(name = "idLivraison", referencedColumnName = "idLivraison")
    private List<LivraisonLigne> livraisonLigneList;
    
    @OneToMany(mappedBy = "livraison", cascade=CascadeType.ALL)
    @JoinColumn(name = "idLivraison", referencedColumnName = "idLivraison")
    private List<Article> articleList;


    public Livraison() {
    }

    public Livraison(Integer idLivraison) {
        this.idLivraison = idLivraison;
    }

    public Livraison(Date dateLivraison, String numeroBonLivraison, Commande commande, Utilisateur utilisateur,
                          List<LivraisonLigne> livraisonLigneList, List<Article> articleList) {
        this.dateLivraison = dateLivraison;
        this.numeroBonLivraison = numeroBonLivraison;
        this.commande = commande;
        this.utilisateur = utilisateur;
        this.livraisonLigneList = livraisonLigneList;
        this.articleList = articleList;
    }


    public Integer getIdLivraison() {
        return idLivraison;
    }

    public void setIdLivraison(Integer idLivraison) {
        this.idLivraison = idLivraison;
    }

    public Date getDateLivraison() {
        return dateLivraison;
    }

    public void setDateLivraison(Date dateLivraison) {
        this.dateLivraison = dateLivraison;
    }

    public String getNumeroBonLivraison() {
        return numeroBonLivraison;
    }

    public void setNumeroBonLivraison(String numeroBonLivraison) {
        this.numeroBonLivraison = numeroBonLivraison;
    }

    @XmlTransient
    public Commande getCommande() {
        return commande;
    }

    public void setCommande(Commande commande) {
        this.commande = commande;
    }


    public Utilisateur getUtilisateur() {
        return utilisateur;
    }

    public void setUtilisateur(Utilisateur utilisateur) {
        this.utilisateur = utilisateur;
    }

    public List<LivraisonLigne> getLivraisonLigneList() {
        return livraisonLigneList;
    }

    public void setLivraisonLigneList(List<LivraisonLigne> livraisonLigneList) {
        this.livraisonLigneList = livraisonLigneList;
    }

    public List<Article> getArticleList() {
        return articleList;
    }

    public void setArticleList(List<Article> articleList) {
        this.articleList = articleList;
    }
    
      // Pour qu'une livraison soit "supprimable"
    // il faut qu'elle ai été faite par l'utilisateur
    public boolean isDeletable(Utilisateur user){
        return this.getUtilisateur().equals(user);
    }  
    

    public static List<String[]> getDataForHorizontalTable(List<Livraison> livraisons){
        List <String[]> editedData = new ArrayList<String[]>();
        if (livraisons!=null){
            editedData.add(new String[]{"Date de livraison", "Numero bon de livraison"});
            for (Livraison livraison : livraisons){
                String dateLivraison = Utilities.javaDateToString(livraison.getDateLivraison());
                editedData.add(new String[]{dateLivraison,
                                            livraison.getNumeroBonLivraison()
                });                                             
            }
        }
        return editedData;
    } 
    
    
    public Map<String, String> getDataForVerticalTable(){
        Map<String, String> editedData = new LinkedHashMap<String, String>();        
        editedData.put("Date de livraison :", Utilities.javaDateToString(this.dateLivraison));
        editedData.put("Numero bon de livraison :",  this.numeroBonLivraison);    
          
        return editedData;        
    }
    
   

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idLivraison != null ? idLivraison.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Livraison)) {
            return false;
        }
        Livraison other = (Livraison) object;
        if ((this.idLivraison == null && other.idLivraison != null) || (this.idLivraison != null && !this.idLivraison.equals(other.idLivraison))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "fr.univtours.Gpod.persistance.entities.Livraison[idLivraison=" + idLivraison + "]";
    }

}
