/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2012/01/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/


package fr.univtours.gpod.persistance.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author geoffroy.vibrac
 */
@Entity
@Table(name = "etatphysique")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "EtatPhysique.findAll", query = "SELECT e FROM EtatPhysique e"),
    @NamedQuery(name = "EtatPhysique.findByIdEtatPhysique", query = "SELECT e FROM EtatPhysique e WHERE e.idEtatPhysique = :idEtatPhysique"),
    @NamedQuery(name = "EtatPhysique.findByLibelle", query = "SELECT e FROM EtatPhysique e WHERE e.libelle = :libelle")})
public class EtatPhysique implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @NotNull
    @Column(name = "idEtatPhysique")
    private Integer idEtatPhysique;
    @Size(max = 45)
    @Column(name = "Libelle")
    private String libelle;

    public EtatPhysique() {
    }

    public EtatPhysique(Integer idEtatPhysique) {
        this.idEtatPhysique = idEtatPhysique;
    }

    public Integer getIdEtatPhysique() {
        return idEtatPhysique;
    }

    public void setIdEtatPhysique(Integer idEtatPhysique) {
        this.idEtatPhysique = idEtatPhysique;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEtatPhysique != null ? idEtatPhysique.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof EtatPhysique)) {
            return false;
        }
        EtatPhysique other = (EtatPhysique) object;
        if ((this.idEtatPhysique == null && other.idEtatPhysique != null) || (this.idEtatPhysique != null && !this.idEtatPhysique.equals(other.idEtatPhysique))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "fr.univtours.Gpod.persistance.entities.Etatphysique[ idEtatPhysique=" + idEtatPhysique + " ]";
    }

}
