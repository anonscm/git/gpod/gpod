/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2012/01/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/


package fr.univtours.gpod.persistance.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author geoffroy.vibrac
 */
@Entity
@Table(name = "mentiondanger")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MentionDanger.findAll", query = "SELECT m FROM MentionDanger m"),
    @NamedQuery(name = "MentionDanger.findByIdmention", query = "SELECT m FROM MentionDanger m WHERE m.idMention = :idMention"),
    @NamedQuery(name = "MentionDanger.findByCode", query = "SELECT m FROM MentionDanger m WHERE m.code = :code")})
public class MentionDanger implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @NotNull
    @Column(name = "idmention")
    private Integer idMention;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 7)
    @Column(name = "code")
    private String code;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "libelle")
    private String libelle;

    public MentionDanger() {
    }

    public MentionDanger(Integer idmention) {
        this.idMention = idmention;
    }

    public MentionDanger(Integer idmention, String code, String libelle) {
        this.idMention = idmention;
        this.code = code;
        this.libelle = libelle;
    }

    public Integer getIdMention() {
        return idMention;
    }

    public void setIdMention(Integer idMention) {
        this.idMention = idMention;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }
    
    public Map<String, String> getDataForVerticalTable(){
        Map<String, String> editedData = new LinkedHashMap<String, String>();    
        editedData.put("Code :", this.getCode());
        editedData.put("Libelle :", this.getLibelle());   

        return editedData;        
    }
    
    public static List<String[]> getDataForHorizontalTable(List<MentionDanger> mentionsDanger){
        List <String[]> editedData = new ArrayList<String[]>();
        if (mentionsDanger!=null){
            editedData.add(new String[]{"Code", "Mention"});

            for (MentionDanger md : mentionsDanger){
                editedData.add(new String[]{md.getCode(), md.getLibelle()});
            }
        }
        return editedData;
    }       

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idMention != null ? idMention.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof MentionDanger)) {
            return false;
        }
        MentionDanger other = (MentionDanger) object;
        if ((this.idMention == null && other.idMention != null) || (this.idMention != null && !this.idMention.equals(other.idMention))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "fr.univtours.Gpod.persistance.entities.MentionDanger[ idmention=" + idMention + " ]";
    }

}
