package fr.univtours.gpod.persistance.entities;

import fr.univtours.gpod.persistance.DAO.ImportExcelDAO;

import javax.persistence.Basic;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.sql.Timestamp;

/**
 * Created with IntelliJ IDEA.
 * User: geoffroy.vibrac
 * Date: 03/04/13
 * Time: 14:24
 */
@Entity
@Table(name = "importexcel")
public class ImportExcel {

    public ImportExcel(){

    }


    public ImportExcel(String cas, Timestamp dateImport, String formuleBrut, String fourniseur, String nom, double quantite, double quantiteUnitaire, String uniteMesure) {
        importExcelPK = new ImportExcelPK(cas, dateImport, formuleBrut, fourniseur, nom, quantite, quantiteUnitaire, uniteMesure);
    }

    public static ImportExcelDAO getDao(){
        return new ImportExcelDAO();
    }

    @EmbeddedId
    ImportExcelPK importExcelPK;






/*    private String cas;

    @javax.persistence.Column(name = "CAS")
    @Basic
    public String getCas() {
        return cas;
    }

    public void setCas(String cas) {
        this.cas = cas;
    }

    private String nom;

    @javax.persistence.Column(name = "Nom")
    @Basic
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    private String fourniseur;

    @javax.persistence.Column(name = "Fourniseur")
    @Basic
    public String getFourniseur() {
        return fourniseur;
    }

    public void setFourniseur(String fourniseur) {
        this.fourniseur = fourniseur;
    }

    private double quantite;

    @javax.persistence.Column(name = "Quantite")
    @Basic
    public double getQuantite() {
        return quantite;
    }

    public void setQuantite(double quantite) {
        this.quantite = quantite;
    }

    private String uniteMesure;

    @javax.persistence.Column(name = "UniteMesure")
    @Basic
    public String getUniteMesure() {
        return uniteMesure;
    }

    public void setUniteMesure(String uniteMesure) {
        this.uniteMesure = uniteMesure;
    }

    private double quantiteUnitaire;

    @javax.persistence.Column(name = "QuantiteUnitaire")
    @Basic
    public double getQuantiteUnitaire() {
        return quantiteUnitaire;
    }

    public void setQuantiteUnitaire(double quantiteUnitaire) {
        this.quantiteUnitaire = quantiteUnitaire;
    }

    private String formuleBrut;

    @javax.persistence.Column(name = "FormuleBrut")
    @Basic
    public String getFormuleBrut() {
        return formuleBrut;
    }

    public void setFormuleBrut(String formuleBrut) {
        this.formuleBrut = formuleBrut;
    }

    private Timestamp dateImport;

    @javax.persistence.Column(name = "DateImport")
    @Basic
    public Timestamp getDateImport() {
        return dateImport;
    }

    public void setDateImport(Timestamp dateImport) {
        this.dateImport = dateImport;
    }
    */
    private int insere;

    @javax.persistence.Column(name = "insere")
    @Basic
    public int getInsere() {
        return insere;
    }

    public void setInsere(int insere) {
        this.insere = insere;
    }

    public ImportExcelPK getImportExcelPK() {
        return importExcelPK;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ImportExcel that = (ImportExcel) o;

        if (importExcelPK != null ? !importExcelPK.equals(that.importExcelPK) : that.importExcelPK != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = importExcelPK.hashCode();
        return result;
    }
}
