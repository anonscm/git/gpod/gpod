/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2012/01/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/


package fr.univtours.gpod.persistance.entities;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author geoffroy.vibrac
 */
@Entity
@Table(name = "properties")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "EmailProperties.findAll", query = "SELECT e FROM Properties e"),
    @NamedQuery(name = "EmailProperties.findByIdEmailProperties", query = "SELECT e FROM Properties e WHERE e.idProperties = :idEmailProperties"),
    @NamedQuery(name = "EmailProperties.findBySmtp", query = "SELECT e FROM Properties e WHERE e.smtp = :smtp"),
    @NamedQuery(name = "EmailProperties.findByPort", query = "SELECT e FROM Properties e WHERE e.port = :port"),
    @NamedQuery(name = "EmailProperties.findByFromEmail", query = "SELECT e FROM Properties e WHERE e.fromEmail = :fromEmail")})
public class Properties implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @NotNull
    @Column(name = "idProperties")
    private Integer idProperties;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "smtp")
    private String smtp;
    @Basic(optional = false)
    @NotNull
    @Column(name = "port")
    private int port;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "fromEmail")
    private String fromEmail;

    @Basic(optional = true)
    @Size(min = 1, max = 255)
    @Column(name = "ldapAddress")
    private String ldapAddress;

    @Basic(optional = true)
    @Column(name = "ldapPort")
    private int ldapPort;

    @Basic(optional = true)
    @Size(min = 1, max = 255)
    @Column(name = "ldapUser")
    private String ldapUser;

    @Basic(optional = true)
    @Size(min = 1, max = 255)
    @Column(name = "ldapPassword")
    private String ldapPassword;







    public Properties() {
    }

    public Properties(Integer idEmailProperties) {
        this.idProperties = idEmailProperties;
    }

    public Properties(Integer idEmailProperties, String smtp, int port, String fromEmail) {
        this.idProperties = idEmailProperties;
        this.smtp = smtp;
        this.port = port;
        this.fromEmail = fromEmail;
    }

    public Integer getIdProperties() {
        return idProperties;
    }

    public void setIdEmailProperties(Integer idProperties) {
        this.idProperties = idProperties;
    }

    public String getSmtp() {
        return smtp;
    }

    public void setSmtp(String smtp) {
        this.smtp = smtp;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getFromEmail() {
        return fromEmail;
    }

    public void setFromEmail(String fromEmail) {
        this.fromEmail = fromEmail;
    }

    public String getLdapAddress() {
        return ldapAddress;
    }

    public int getLdapPort() {
        return ldapPort;
    }

    public String getLdapUser() {
        return ldapUser;
    }

    public String getLdapPassword() {
        return ldapPassword;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idProperties != null ? idProperties.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Properties)) {
            return false;
        }
        Properties other = (Properties) object;
        if ((this.idProperties == null && other.idProperties != null) || (this.idProperties != null && !this.idProperties.equals(other.idProperties))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "fr.univtours.gpod.persistance.entities.Properties[ idEmailProperties=" + idProperties + " ]";
    }
    
}
