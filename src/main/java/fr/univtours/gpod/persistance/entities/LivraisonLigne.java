/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2012/01/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/


package fr.univtours.gpod.persistance.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author geoffroy.vibrac
 */
@Entity
@Table(name = "livraisonligne")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "LivraisonLigne.findAll", query = "SELECT l FROM LivraisonLigne l"),
    @NamedQuery(name = "LivraisonLigne.findByIdLivraisonLigne", query = "SELECT l FROM LivraisonLigne l WHERE l.idLivraisonLigne = :idLivraisonLigne"),
    @NamedQuery(name = "LivraisonLigne.findByQuantite", query = "SELECT l FROM LivraisonLigne l WHERE l.quantite = :quantite"),
    @NamedQuery(name = "LivraisonLigne.findByEnStock", query = "SELECT l FROM LivraisonLigne l WHERE l.enStock = :enStock")})
public class LivraisonLigne implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @NotNull
    @Column(name = "idLivraisonLigne")
    private Integer idLivraisonLigne;
    @Basic(optional = false)
    @NotNull
    @Column(name = "quantite")
    private int quantite;
    @Basic(optional = false)
    @NotNull
    @Column(name = "enStock")
    private boolean enStock;
    
    
    @ManyToOne(optional = false)
    @JoinColumn(name = "idProduitConditionnement", referencedColumnName = "idProduitConditionnement")    
    private ProduitConditionnement produitConditionnement;  
    
    
    @ManyToOne(optional = false)
    @JoinColumn(name = "idLivraison", referencedColumnName = "idLivraison")    
    private Livraison livraison;      
      
    
    public LivraisonLigne() {
    }

    public LivraisonLigne(Integer idLivraisonLigne) {
        this.idLivraisonLigne = idLivraisonLigne;
    }

    public LivraisonLigne(int quantite, boolean enStock, ProduitConditionnement produitConditionnement) {
        this.quantite = quantite;
        this.enStock = enStock;
        this.produitConditionnement = produitConditionnement;
    }

    public Integer getIdLivraisonLigne() {
        return idLivraisonLigne;
    }

    public void setIdLivraisonLigne(Integer idLivraisonLigne) {
        this.idLivraisonLigne = idLivraisonLigne;
    }

    public int getQuantite() {
        return quantite;
    }

    public void setQuantite(int quantite) {
        this.quantite = quantite;
    }

    public boolean getEnStock() {
        return enStock;
    }

    public void setEnStock(boolean enStock) {
        this.enStock = enStock;
    }

    public Livraison getLivraison() {
        return livraison;
    }

    public void setLivraison(Livraison livraison) {
        this.livraison = livraison;
    }

    public ProduitConditionnement getProduitConditionnement() {
        return produitConditionnement;
    }

    public void setProduitConditionnement(ProduitConditionnement produitConditionnement) {
        this.produitConditionnement = produitConditionnement;
    }
    
    

    
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idLivraisonLigne != null ? idLivraisonLigne.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof LivraisonLigne)) {
            return false;
        }
        LivraisonLigne other = (LivraisonLigne) object;
        if ((this.idLivraisonLigne == null && other.idLivraisonLigne != null) || (this.idLivraisonLigne != null && !this.idLivraisonLigne.equals(other.idLivraisonLigne))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "fr.univtours.gpod.persistance.entities.LivraisonLigne[ idLivraisonLigne=" + idLivraisonLigne + " ]";
    }
    
}
