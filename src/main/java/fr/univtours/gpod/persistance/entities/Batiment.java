/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2012/01/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/


package fr.univtours.gpod.persistance.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author geoffroy.vibrac
 */
@Entity
@Table(name = "batiment")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Batiment.findAll", query = "SELECT b FROM Batiment b"),
    @NamedQuery(name = "Batiment.findByIdBatiment", query = "SELECT b FROM Batiment b WHERE b.idBatiment = :idBatiment"),
    @NamedQuery(name = "Batiment.findBySite", query = "SELECT b FROM Batiment b WHERE b.site = :site"),
    @NamedQuery(name = "Batiment.findByCode", query = "SELECT b FROM Batiment b WHERE b.code = :code"),
    @NamedQuery(name = "Batiment.findByNom", query = "SELECT b FROM Batiment b WHERE b.nom = :nom")})
public class Batiment implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idBatiment")
    private Integer idBatiment;
    @JoinColumn(name = "idSite", referencedColumnName = "idSite")
    @ManyToOne(optional = false)
    private Site site;     
    @Size(max = 25)
    @Column(name = "code")
    private String code;
    @Size(max = 45)
    @Column(name = "nom")
    private String nom;
    @Size(max = 255)
    @Column(name = "adresse1")
    private String adresse1;
    @Size(max = 255)
    @Column(name = "adresse2")
    private String adresse2;
    @Size(max = 6)
    @Column(name = "codepostal")
    private String codePostal;
    @Size(max = 255)
    @Column(name = "ville")
    private String ville;
    @Size(max = 12)
    @Column(name = "telephone")
    private String telephone;
    @Size(max = 12)
    @Column(name = "fax")
    private String fax;
    @Column(name = "longitude")
    private double longitude;
    @Column(name = "latitude")
    private double latitude;
    
    @OneToMany(mappedBy = "batiment")
    private List<SalleStockage> salles;
    
    public Batiment() {
    }

    public Batiment(String nom, Site site, String code, String adresse1, String adresse2, String codePostal, String ville, String telephone, String fax, double longitude, double latitude) {
        this.nom = nom;
        this.site = site;
        this.code = code;
        this.adresse1 = adresse1;
        this.adresse2 = adresse2;
        this.codePostal = codePostal;
        this.ville = ville;
        this.telephone = telephone;
        this.fax = fax;
        this.longitude = longitude;
        this.latitude = latitude;
    }

    // permet de récupérer les aires des batiments de ce site
    public List<AireStockage> getAires(){
        List<AireStockage> aires = new ArrayList<AireStockage>();
        for(SalleStockage s : salles){
            aires.addAll(s.getAires());
        }
        return aires;
    }    
    
    public Integer getIdBatiment() {
        return idBatiment;
    }

    public void setIdBatiment(Integer idBatiment) {
        this.idBatiment = idBatiment;
    }

    public Site getSite() {
        return site;
    }

    public void setSite(Site site) {
        this.site = site;
    }

    
    @XmlTransient
    public List<SalleStockage> getSalles() {
        return salles;
    }

    
    public void setSalles(List<SalleStockage> salles) {
        this.salles = salles;
    }
    
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getAdresse1() {
        return adresse1;
    }

    public void setAdresse1(String adresse1) {
        this.adresse1 = adresse1;
    }

    public String getAdresse2() {
        return adresse2;
    }

    public void setAdresse2(String adresse2) {
        this.adresse2 = adresse2;
    }

    public String getCodePostal() {
        return codePostal;
    }

    public void setCodePostal(String codePostal) {
        this.codePostal = codePostal;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public Map<String, String> getDataForVerticalTable(){
        Map<String, String> editedData = new LinkedHashMap<>();   
        editedData.put("Site  :", this.getSite().getNom());
        editedData.put("Nom  :", this.getNom());
        editedData.put("Code :", this.getCode());
        editedData.put("Adresse 1 :", this.getAdresse1()); 
        editedData.put("Adresse 2 :", this.getAdresse1()); 
        editedData.put("Code Postal :", this.getCodePostal()); 
        editedData.put("Ville :", this.getVille()); 

        return editedData;        
    }     
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idBatiment != null ? idBatiment.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Batiment)) {
            return false;
        }
        Batiment other = (Batiment) object;
        return !((this.idBatiment == null && other.idBatiment != null) || (this.idBatiment != null && !this.idBatiment.equals(other.idBatiment)));
    }

    @Override
    public String toString() {
        return "fr.univtours.Gpod.persistance.entities.Batiment[ idBatiment=" + idBatiment + " ]";
    }

}
