/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2012/01/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/


package fr.univtours.gpod.persistance.entities;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import fr.univtours.gpod.utils.Utilities;
import java.util.*;
import javax.persistence.*;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
/**
 *
 * @author geoffroy.vibrac
 */
@Entity
@Table(name = "utilisateur")
@NamedQueries({
    @NamedQuery(name = "Utilisateur.findAll", query = "SELECT u FROM Utilisateur u"),
    @NamedQuery(name = "Utilisateur.findByLogin", query = "SELECT u FROM Utilisateur u WHERE u.login = :login"),
    @NamedQuery(name = "Utilisateur.findByLoginExceptU", query = "SELECT u FROM Utilisateur u WHERE u.login = :login "
        + "AND u.idUtilisateur <>:idUtilisateur")})
@XmlRootElement
public class Utilisateur implements Serializable {
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idUtilisateur")
    private Long idUtilisateur;
    @Size(max = 45)
    private String nom;
    @Size(max = 45)
    private String prenom;
    @Size(max = 45)    
    private String login;
    @Size(max = 45)
    private String password;
     @Size(max = 45)
    private String email;   
    @OneToOne
    @JoinColumn(name="idUtilisateurProfil", nullable=false)
    private UtilisateurProfil utilisateurProfil;
    @ManyToMany
    @JoinTable(name="utilisateursousstructure",
    joinColumns= @JoinColumn(name = "IdUtilisateur"),
    inverseJoinColumns= @JoinColumn(name="idSousStructure"))
    private List<SousStructure> sousStructures;
    
    @Column(name = "dateDerniereConnexion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateDerniereConnexion;
    
    @Column(name = "dateAvantDerniereConnexion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateAvantDerniereConnexion;    
    
    @JoinColumn(name = "idAireFavorite", referencedColumnName = "idAire")
    @ManyToOne(optional = true)
    private AireStockage aireStockage;    
    

    public Utilisateur(){
    }

    public Utilisateur(String nom, String prenom, String login, String password, String email, UtilisateurProfil utilisateurProfil) {
        this.nom = nom;
        this.prenom = prenom;
        this.login = login;
        this.password = Utilities.md5(password);
        this.email = email;
        this.utilisateurProfil = utilisateurProfil;
    }
    
    public Utilisateur(String nom, String prenom, String login, String password, String email, 
                UtilisateurProfil utilisateurProfil, List<SousStructure> sousstructures) {
        this(nom, prenom, login, password, email, utilisateurProfil);
        this.sousStructures = sousstructures;
            
    }
    
        
    public Long getIdUtilisateur() {
        return idUtilisateur;
    }

    public void setIdUtilisateur(Long id) {
        this.idUtilisateur = id;
    }

    public UtilisateurProfil getUtilisateurProfil() {
        return utilisateurProfil;
    }

    public void setUtilisateurProfil(UtilisateurProfil UtilisateurProfil) {
        this.utilisateurProfil = UtilisateurProfil;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
    
    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }  

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = Utilities.md5(password);
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }  

    
    public List<SousStructure> getSousStructure(){
        return sousStructures;
    }
    
    public void setSousStructure(List<SousStructure> sousstructures){
        this.sousStructures = sousstructures;
    }

    public Date getDateAvantDerniereConnexion() {
        return dateAvantDerniereConnexion;
    }

    public void setDateAvantDerniereConnexion(Date dateAvantDerniereConnexion) {
        this.dateAvantDerniereConnexion = dateAvantDerniereConnexion;
    }

    public Date getDateDerniereConnexion() {
        return dateDerniereConnexion;
    }

    public void setDateDerniereConnexion(Date dateDerniereConnexion) {
        this.dateDerniereConnexion = dateDerniereConnexion;
    }

    public AireStockage getAireStockage() {
        return aireStockage;
    }

    public void setAireStockage(AireStockage aireStockage) {
        this.aireStockage = aireStockage;
    }
    
    
    
    
    
    public Map<String, String> getDataForVerticalTable(){
        Map<String, String> editedData = new LinkedHashMap<>();        
        editedData.put("Nom  :", this.getNom());
        editedData.put("Prenom :", this.getPrenom()); 
        editedData.put("Login :", this.getLogin());
        editedData.put("Email :", this.getEmail());  
        editedData.put("Code Profil :", this.getUtilisateurProfil().getCode());  
        editedData.put("Nom Profil :", this.getUtilisateurProfil().getNom());        

        return editedData;        
    }    
           
     
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idUtilisateur != null ? idUtilisateur.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Utilisateur)) {
            return false;
        }
        Utilisateur other = (Utilisateur) object;
        if ((this.idUtilisateur == null && other.idUtilisateur != null) || (this.idUtilisateur != null && !this.idUtilisateur.equals(other.idUtilisateur))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "fr.univtours.Gpod.persistance.Utilisateur[ id=" + idUtilisateur + " ]";
    }

}
