/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2012/01/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/


package fr.univtours.gpod.persistance.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author geoffroy.vibrac
 */
@Entity
@Table(name = "unitemesure")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UniteMesure.findAll", query = "SELECT u FROM UniteMesure u"),
    @NamedQuery(name = "UniteMesure.findByIdUniteMesure", query = "SELECT u FROM UniteMesure u WHERE u.idUniteMesure = :idUniteMesure"),
    @NamedQuery(name = "UniteMesure.findByLibelleLong", query = "SELECT u FROM UniteMesure u WHERE u.libelleLong = :libelleLong"),
    @NamedQuery(name = "UniteMesure.findByAbreviation", query = "SELECT u FROM UniteMesure u WHERE u.abreviation = :abreviation")})
public class UniteMesure implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @NotNull
    @Column(name = "idUniteMesure")
    private Integer idUniteMesure;
    @Size(max = 45)
    @Column(name = "libelleLong")
    private String libelleLong;
    @Size(max = 45)
    @Column(name = "abreviation")
    private String abreviation;


    public UniteMesure() {
    }

    public UniteMesure(Integer idUniteMesure) {
        this.idUniteMesure = idUniteMesure;
    }

    public Integer getIdUniteMesure() {
        return idUniteMesure;
    }

    public void setIdUniteMesure(Integer idUniteMesure) {
        this.idUniteMesure = idUniteMesure;
    }

    public String getLibelleLong() {
        return libelleLong;
    }

    public void setLibelleLong(String libelleLong) {
        this.libelleLong = libelleLong;
    }

    public String getAbreviation() {
        return abreviation;
    }

    public void setAbreviation(String abreviation) {
        this.abreviation = abreviation;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idUniteMesure != null ? idUniteMesure.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof UniteMesure)) {
            return false;
        }
        UniteMesure other = (UniteMesure) object;
        if ((this.idUniteMesure == null && other.idUniteMesure != null) || (this.idUniteMesure != null && !this.idUniteMesure.equals(other.idUniteMesure))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "fr.univtours.Gpod.persistance.entities.UniteMesure[ idUniteMesure=" + idUniteMesure + " ]";
    }

}
