/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2012/01/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/


package fr.univtours.gpod.persistance.entities;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author geoffroy.vibrac
 */
@Entity
@Table(name = "structure")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Structure.findAll", query = "SELECT l FROM Structure l"),
    @NamedQuery(name = "Structure.findByIdStructure", query = "SELECT l FROM Structure l WHERE l.idStructure = :idStructure"),
    @NamedQuery(name = "Structure.findByCode", query = "SELECT l FROM Structure l WHERE l.code = :code"),
    @NamedQuery(name = "Structure.findByNom", query = "SELECT l FROM Structure l WHERE l.nom = :nom")})
public class Structure implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @NotNull
    @Column(name = "idStructure")
    private Integer idStructure;
    @Size(max = 45)
    private String code;    
    @Size(max = 45)
    private String nom;
    @Size(max = 255)
    private String adresse1;   
     @Size(max = 255)
    private String adresse2;      
    @Size(max = 6)
    private String codePostal;       
    @Size(max = 255)
    private String ville;   
    @Size(max = 12)
    private String telephone;   
    @Size(max = 12)
    private String fax;     
    @Size(max = 255)
    private String responsable;  
    @Size(max = 2)
    private String type;


    @OneToMany(mappedBy = "structure")
    private List<SousStructure> sousStructure;

    public Structure() {
    }

    public Structure(String nom, String code, String adresse1, String adresse2, String codePostal, String ville, String telephone, String fax, String responsable, String type) {
        this.nom = nom;
        this.code = code;
        this.adresse1 = adresse1;
        this.adresse2 = adresse2;
        this.codePostal = codePostal;
        this.ville = ville;
        this.telephone = telephone;
        this.fax = fax;
        this.responsable = responsable;
        this.type = type;
    }



    public Integer getIdStructure() {
        return idStructure;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
    
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getAdresse1() {
        return adresse1;
    }

    public void setAdresse1(String adresse1) {
        this.adresse1 = adresse1;
    }

    public String getAdresse2() {
        return adresse2;
    }

    public void setAdresse2(String adresse2) {
        this.adresse2 = adresse2;
    }

    public String getCodePostal() {
        return codePostal;
    }

    public void setCodePostal(String codePostal) {
        this.codePostal = codePostal;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getResponsable() {
        return responsable;
    }

    public void setResponsable(String responsable) {
        this.responsable = responsable;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    
    @XmlTransient
    public List<SousStructure> getSousStructure() {
        return sousStructure;
    }

    public void setSousStructure(List<SousStructure> sousStructure) {
        this.sousStructure = sousStructure;
    }

    public String getType() {
        return type;
    }

     public Map<String, String> getDataForVerticalTable(){
        Map<String, String> editedData = new LinkedHashMap<>();        
        editedData.put("Nom  :", this.getNom());
        editedData.put("Code :", this.getCode()); 
        editedData.put("Adresse :", this.getAdresse1());
        editedData.put("Complement :", this.getAdresse2());  
        editedData.put("Code Postal :", this.getCodePostal());  
        editedData.put("ville :", this.getVille());
        editedData.put("Telephone :", this.getTelephone());
        editedData.put("Fax :", this.getFax());
        editedData.put("Responsable :", this.getResponsable());

        return editedData;        
    }     



    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idStructure != null ? idStructure.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Structure)) {
            return false;
        }
        Structure other = (Structure) object;
        if ((this.idStructure == null && other.idStructure != null) || (this.idStructure != null && !this.idStructure.equals(other.idStructure))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "fr.univtours.Gpod.persistance.Structure[ idStructure=" + idStructure + " ]";
    }

}
