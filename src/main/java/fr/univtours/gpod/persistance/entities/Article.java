/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2012/01/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/


package fr.univtours.gpod.persistance.entities;

import fr.univtours.gpod.persistance.DAO.AireStockageDAO;
import fr.univtours.gpod.persistance.DAO.LivraisonDAO;
import fr.univtours.gpod.persistance.DAO.ProduitConditionnementDAO;
import fr.univtours.gpod.persistance.DAO.SousStructureDAO;
import fr.univtours.gpod.utils.Utilities;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.apache.commons.lang.StringEscapeUtils;

/**
 *
 * @author geoffroy.vibrac
 */
@Entity
@Table(name = "article")
@NamedQueries({
    @NamedQuery(name = "Article.findAll", query = "SELECT a FROM Article a"),
    @NamedQuery(name = "Article.findByIdArticle", query = "SELECT a FROM Article a WHERE a.idArticle = :idArticle"),
    @NamedQuery(name = "Article.findByIdentifiantEtiquette", query = "SELECT a FROM Article a WHERE a.identifiantEtiquette = :identifiantEtiquette")})
@XmlRootElement
public class Article implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idArticle")
    private Integer idArticle;
    @Basic(optional = false)
    @Column(name = "identifiantEtiquette")
    private String identifiantEtiquette;
    @Column(name = "quantite")
    private Double quantite;
    @Basic(optional = true)
    @Column(name = "dateOuverture")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateOuverture;

    @JoinColumn(name = "idProduitConditionnement", referencedColumnName = "idProduitConditionnement")
    @ManyToOne(optional = false)
    private ProduitConditionnement produitConditionnement;

    @JoinColumn(name = "idSousStructure", referencedColumnName = "idSousStructure")
    @ManyToOne(optional = false)
    private SousStructure sousStructure;

    @JoinColumn(name = "idAire", referencedColumnName = "idAire")
    @ManyToOne(optional = false)
    private AireStockage aireStockage;

    @JoinColumn(name = "idLivraison", referencedColumnName = "idLivraison")
    @ManyToOne(optional = false)
    private Livraison livraison;



    public Article() {
    }

    public Article(Integer idArticle) {
        this.idArticle = idArticle;
    }

    // ce constructeur permet d'instancier un article à partir des id et non pas des classes
    public Article(int idProduitConditionnement, int idSousStructure, int idAire, int idLivraison, String identifiantEtiquette) {
        // récupération de produit
        ProduitConditionnementDAO pdao = new ProduitConditionnementDAO();
        ProduitConditionnement produitConditionnement = pdao.findProduitConditionnement(idProduitConditionnement);

        SousStructureDAO ssdao = new SousStructureDAO();
        SousStructure sousStructure = ssdao.findSousStructure(idSousStructure);

        AireStockageDAO asdao = new AireStockageDAO();
        AireStockage aire = asdao.findAireStockage(idAire);

        LivraisonDAO ldao = new LivraisonDAO();
        Livraison livraison = ldao.findLivraison(idLivraison);

        this.aireStockage = aire;
        identifiantEtiquette = Utilities.completeRightZero(identifiantEtiquette,24);
        this.identifiantEtiquette = identifiantEtiquette;
        this.livraison = livraison;
        this.produitConditionnement = produitConditionnement;
        this.sousStructure = sousStructure;
        this.quantite = 1.0;
    }

    public Integer getIdArticle() {
        return idArticle;
    }

    public void setIdArticle(Integer idArticle) {
        this.idArticle = idArticle;
    }

    public String getIdentifiantEtiquette() {
        return identifiantEtiquette;
    }

    public void setIdentifiantEtiquette(String identifiantEtiquette) {
        // si il y a un identifiantEtiquette mais que la longueur est <24, on complete avec des zeros
        identifiantEtiquette = Utilities.completeRightZero(identifiantEtiquette,24);
        this.identifiantEtiquette = identifiantEtiquette;
    }

    public Double getQuantite() {
        return quantite;
    }

    public void setQuantite(Double quantite) {
        this.quantite = quantite;
    }  

    public AireStockage getAireStockage() {
        return aireStockage;
    }

    public void setAireStockage(AireStockage aireStockage) {
        this.aireStockage = aireStockage;
    }
    
    @XmlTransient
    public Livraison getLivraison() {
        return livraison;
    }

    public void setLivraison(Livraison livraison) {
        this.livraison = livraison;
    }

    public ProduitConditionnement getProduitConditionnement() {
        return produitConditionnement;
    }

    public void setProduitConditionnement(ProduitConditionnement produitConditionnement) {
        this.produitConditionnement = produitConditionnement;
    }

    public SousStructure getSousStructure() {
        return sousStructure;
    }

    public void setSousStructure(SousStructure sousStructure) {
        this.sousStructure = sousStructure;
    }

    public Date getDateOuverture() {
        return dateOuverture;
    }

    public void setDateOuverture(Date dateOuverture) {
        this.dateOuverture = dateOuverture;
    }

    
    public static List<String[]> getDataForHorizontalTable(List<Article> articles){
        List <String[]> editedData = new ArrayList<String[]>();
        if (articles!=null){
            editedData.add(new String[]{"id", "Produit", "Etiquette", "Structure", "Local de Stockage", "Qté restante", "Date d'ouverture"});
            for (Article a : articles){
                String dateO = Utilities.javaDateToString(a.getDateOuverture());
                editedData.add(new String[]{Integer.toString(a.getIdArticle()),
                                            StringEscapeUtils.unescapeHtml(a.getProduitConditionnement().getProduit().getNom()),
                                            a.getIdentifiantEtiquette(),
                                            a.getSousStructure().getNom(),
                                            a.getAireStockage().getNom(),
                                            Double.toString(a.getQuantite()),
                                            dateO
                });                                             
            }
        }
        return editedData;
    } 
    
    
    public Map<String, String> getDataForVerticalTable(){
        Map<String, String> editedData = new LinkedHashMap<String, String>();  
        
        editedData.put("id :",  Integer.toString(this.idArticle));
        editedData.put("Produit : ",  this.produitConditionnement.getProduit().getNom());
        editedData.put("Etiquette : ",  this.identifiantEtiquette);
        editedData.put("Structure : ",  this.getSousStructure().getNom());
        editedData.put("Local de Stockage : ",  this.getAireStockage().getNom());
        editedData.put("Qté restante : ",  Double.toString(this.quantite));
        editedData.put("Date d'ouverture :", Utilities.javaDateToString(this.dateOuverture));
   
              
           
        return editedData;        
    }
    

    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idArticle != null ? idArticle.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Article)) {
            return false;
        }
        Article other = (Article) object;
        return !((this.idArticle == null && other.idArticle != null) || (this.idArticle != null && !this.idArticle.equals(other.idArticle)));
    }

    @Override
    public String toString() {
        return "fr.univtours.Gpod.persistance.entities.Article[idArticle=" + idArticle + "]";
    }

}
