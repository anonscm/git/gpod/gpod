/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2012/01/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/

package fr.univtours.gpod.persistance.DAO;

import fr.univtours.gpod.persistance.Manager.PersistenceManager;
import fr.univtours.gpod.persistance.entities.UtilisateurProfil;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;


public class UtilisateurProfilDAO implements Serializable  {

    EntityManagerFactory emf = PersistenceManager.getInstance().getEntityManagerFactory();
    
    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }  
    
     
    public List<UtilisateurProfil> findUtilisateurProfilEntities() {
        return findUtilisateurProfilEntities(true, -1, -1);
    }

    public List<UtilisateurProfil> findUtilisateurProfilEntities(int maxResults, int firstResult) {
        boolean all = false;
        if (maxResults<=0){
            all=true;
        }        
        return findUtilisateurProfilEntities(all, maxResults, firstResult);
    }

    private List<UtilisateurProfil> findUtilisateurProfilEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(UtilisateurProfil.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public UtilisateurProfil findUtilisateurProfil(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(UtilisateurProfil.class, id);
        } finally {
            em.close();
        }
    }

    public int getUtilisateurProfilCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<UtilisateurProfil> rt = cq.from(UtilisateurProfil.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
    
    
}
