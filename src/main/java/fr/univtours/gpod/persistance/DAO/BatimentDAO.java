/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2012/01/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/

package fr.univtours.gpod.persistance.DAO;

import fr.univtours.gpod.exceptions.IllegalOrphanException;
import fr.univtours.gpod.exceptions.NonexistentEntityException;
import fr.univtours.gpod.persistance.Manager.PersistenceManager;
import fr.univtours.gpod.persistance.entities.Batiment;
import fr.univtours.gpod.persistance.entities.SalleStockage;
import fr.univtours.gpod.persistance.entities.Site;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;


public class BatimentDAO implements Serializable{

    EntityManagerFactory emf = PersistenceManager.getInstance().getEntityManagerFactory();
    
     public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }  
     
    public void create(Batiment batiment) {
        if (batiment.getSalles() == null) {
            batiment.setSalles(new ArrayList<SalleStockage>());
        }
        EntityManager em = getEntityManager();
        try {
            em.getTransaction().begin();
            List<SalleStockage> attachedSalles = new ArrayList<SalleStockage>();
            for (SalleStockage sallesSalleStockageToAttach : batiment.getSalles()) {
                sallesSalleStockageToAttach = em.getReference(sallesSalleStockageToAttach.getClass(), sallesSalleStockageToAttach.getIdSalle());
                attachedSalles.add(sallesSalleStockageToAttach);
            }
            batiment.setSalles(attachedSalles);
            em.persist(batiment);
            for (SalleStockage sallesSalleStockage : batiment.getSalles()) {
                Batiment oldBatimentOfSallesSalleStockage = sallesSalleStockage.getBatiment();
                sallesSalleStockage.setBatiment(batiment);
                sallesSalleStockage = em.merge(sallesSalleStockage);
                if (oldBatimentOfSallesSalleStockage != null) {
                    oldBatimentOfSallesSalleStockage.getAires().remove(sallesSalleStockage);
                    oldBatimentOfSallesSalleStockage = em.merge(oldBatimentOfSallesSalleStockage);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Batiment batiment) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = getEntityManager();
        try {
            em.getTransaction().begin();
            Batiment persistentBatiment = em.find(Batiment.class, batiment.getIdBatiment());
            List<SalleStockage> sallesOld = persistentBatiment.getSalles();
            List<SalleStockage> sallesNew = batiment.getSalles();
            List<String> illegalOrphanMessages = null;
            for (SalleStockage sallesOldSalleStockage : sallesOld) {
                if (!sallesNew.contains(sallesOldSalleStockage)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain SalleStockage " + sallesOldSalleStockage + " since its batiment field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<SalleStockage> attachedSallesNew = new ArrayList<SalleStockage>();
            for (SalleStockage sallesNewSalleStockageToAttach : sallesNew) {
                sallesNewSalleStockageToAttach = em.getReference(sallesNewSalleStockageToAttach.getClass(), sallesNewSalleStockageToAttach.getIdSalle());
                attachedSallesNew.add(sallesNewSalleStockageToAttach);
            }
            sallesNew = attachedSallesNew;
            batiment.setSalles(sallesNew);
            batiment = em.merge(batiment);
            for (SalleStockage sallesNewSalleStockage : sallesNew) {
                if (!sallesOld.contains(sallesNewSalleStockage)) {
                    Batiment oldBatimentOfSallesNewSalleStockage = sallesNewSalleStockage.getBatiment();
                    sallesNewSalleStockage.setBatiment(batiment);
                    sallesNewSalleStockage = em.merge(sallesNewSalleStockage);
                    if (oldBatimentOfSallesNewSalleStockage != null && !oldBatimentOfSallesNewSalleStockage.equals(batiment)) {
                        oldBatimentOfSallesNewSalleStockage.getSalles().remove(sallesNewSalleStockage);
                        oldBatimentOfSallesNewSalleStockage = em.merge(oldBatimentOfSallesNewSalleStockage);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = batiment.getIdBatiment();
                if (findBatiment(id) == null) {
                    throw new NonexistentEntityException("The batiment with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = getEntityManager();
        try {
            em.getTransaction().begin();
            Batiment batiment;
            try {
                batiment = em.getReference(Batiment.class, id);
                batiment.getIdBatiment();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The batiment with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<SalleStockage> sallesOrphanCheck = batiment.getSalles();
            for (SalleStockage sallesOrphanCheckSalleStockage : sallesOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Batiment (" + batiment + ") cannot be destroyed since the SalleStockage " + sallesOrphanCheckSalleStockage + " in its salles field has a non-nullable batiment field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(batiment);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Batiment> findBatimentEntities() {
        return findBatimentEntities(true, -1, -1);
    }

    public List<Batiment> findBatimentEntities(int maxResults, int firstResult) {
        boolean all = false;
        if (maxResults<=0){
            all=true;
        }
        return findBatimentEntities(all, maxResults, firstResult);
    }

    private List<Batiment> findBatimentEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Batiment.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }
    
    public int getBatimentCount(String searchValue, String searchColumns){
        if (searchValue == null || searchColumns==null || "".equals(searchValue) || "".equals(searchColumns)){
            return getBatimentCount();
        }
        String sql = returnSqlQuery("count(x)", searchValue, searchColumns);
        
        EntityManager em = getEntityManager();
        Query q = em.createQuery(sql);
        return ((Long) q.getSingleResult()).intValue();                
    }    

    public Batiment findBatiment(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Batiment.class, id);
        } finally {
            em.close();
        }
    }

    public List<Batiment> findBatimentFromSite(Site site){
        EntityManager em = getEntityManager();
        Query query = em.createNamedQuery("Batiment.findBySite");
        query.setParameter("site",site);
        return query.getResultList();
    }

    public List<Batiment> findBatimentWithQuery(String searchValue, String searchColumns, int maxResults, int firstResult){
        if (searchValue == null || searchColumns==null || "".equals(searchValue) || "".equals(searchColumns)){
            return findBatimentEntities(maxResults, firstResult);
        }

                       
        
        EntityManager em = getEntityManager();
        String sql = returnSqlQuery("x", searchValue, searchColumns);
        Query q = em.createQuery(sql);
        if (maxResults>0){
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();    
        
    }
    
    private String returnSqlQuery(String select, String searchValue, String searchColumns){
        // Insertion dans une liste des colonnes de recherche
        searchColumns = searchColumns.replace("[", "");
        searchColumns = searchColumns.replace("\"", "");
        searchColumns = searchColumns.replace("]", "");
        List<String> columns = Arrays.asList(searchColumns.split(","));
        String conditions = "";
        String sql = "SELECT "+select+" FROM Batiment x WHERE 0=0"; 
        for (String c : columns){
            conditions+=" OR x." + c + " LIKE '%"+searchValue+"%'";
        }
        if (conditions.length() > 4){
            conditions = conditions.substring(4);
            sql+=" AND ("+conditions+")";
        }
        return sql;
    }
    
    
    public int getBatimentCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Batiment> rt = cq.from(Batiment.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
