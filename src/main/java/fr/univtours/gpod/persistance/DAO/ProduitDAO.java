/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2012/01/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/

package fr.univtours.gpod.persistance.DAO;

import fr.univtours.gpod.persistance.DAO.exceptions.NonexistentEntityException;
import fr.univtours.gpod.persistance.Manager.PersistenceManager;
import fr.univtours.gpod.persistance.entities.Produit;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import fr.univtours.gpod.persistance.entities.Fournisseur;
import fr.univtours.gpod.persistance.entities.ProduitConditionnement;
import fr.univtours.gpod.utils.Utilities;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import javax.transaction.UserTransaction;

/**
 *
 * @author geoffroy.vibrac
 */
public class ProduitDAO implements Serializable {

    EntityManagerFactory emf= PersistenceManager.getInstance().getEntityManagerFactory();

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Produit produit) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Fournisseur idFournisseur = produit.getFournisseur();
            if (idFournisseur != null) {
                idFournisseur = em.getReference(idFournisseur.getClass(), idFournisseur.getIdFournisseur());
                produit.setFournisseur(idFournisseur);
            }
            em.persist(produit);

            // insertion des conditionnement
            List<ProduitConditionnement> conditionnements = produit.getConditionnements();
            for (ProduitConditionnement conditionnement : conditionnements) {
                conditionnement.setProduit(produit);
                em.persist(conditionnement);
            }
            

            if (idFournisseur != null) {
                idFournisseur.getProduitList().add(produit);
                idFournisseur = em.merge(idFournisseur);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Produit produit) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();

            Produit persistentProduit = em.find(Produit.class, produit.getIdProduit());

            Fournisseur idFournisseurOld = persistentProduit.getFournisseur();
            Fournisseur idFournisseurNew = produit.getFournisseur();
            if (idFournisseurNew != null) {
                idFournisseurNew = em.getReference(idFournisseurNew.getClass(), idFournisseurNew.getIdFournisseur());
                produit.setFournisseur(idFournisseurNew);
            }

            // insertion ou maj des conditionnement

            List<ProduitConditionnement> persistantConditionnements = persistentProduit.getConditionnements();

            List<ProduitConditionnement> newConditionnements = produit.getConditionnements();

            // je parcours les nouveau conditionnement
            for (ProduitConditionnement newConditionnement : newConditionnements) {
                // je cherche dans la liste persistantConditionnements si je trouve le nouveau conditionnement en cours
                int indexPos = persistantConditionnements.indexOf(newConditionnement);
                // je le trouve=> je mets à jour l'ancien condtionnement
                if (indexPos>=0){
                    ProduitConditionnement pc = persistantConditionnements.get(indexPos);
                    pc.setContenance(newConditionnement.getContenance());
                    pc.setContenant(newConditionnement.getContenant());
                    pc.setReferenceFournisseur(newConditionnement.getReferenceFournisseur());
                    pc.setUniteMesure(newConditionnement.getUniteMesure());
                    pc.setProduit(produit);
                    em.merge(pc);
                   // je ne le trouve pas => nouveau, on l'insert
                }else{
                    ProduitConditionnement pc = new ProduitConditionnement(
                            newConditionnement.getReferenceFournisseur(),
                            newConditionnement.getContenance(),
                            newConditionnement.getContenant(),
                            newConditionnement.getUniteMesure(),produit);
                    if (pc!=null){
                        persistantConditionnements.add(pc);
                        em.persist(pc);
                    }
                }
            }

            // je parcous persistantConditionnements
            for(int i=0; i<persistantConditionnements.size();i ++){
                ProduitConditionnement pc = persistantConditionnements.get(i);
                // si on ne trouve pas dans les nouveau conditionnemets => on met à 0
                if (newConditionnements.indexOf(pc)==-1){                    
                    pc.setDeleted(Boolean.TRUE);
                    em.merge(pc);
                }                
            }

            produit.setConditionnements(persistantConditionnements);
            produit = em.merge(produit);




            if (idFournisseurOld != null && !idFournisseurOld.equals(idFournisseurNew)) {
                idFournisseurOld.getProduitList().remove(produit);
                idFournisseurOld = em.merge(idFournisseurOld);
            }
            if (idFournisseurNew != null && !idFournisseurNew.equals(idFournisseurOld)) {
                idFournisseurNew.getProduitList().add(produit);
                idFournisseurNew = em.merge(idFournisseurNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = produit.getIdProduit();
                if (findProduit(id) == null) {
                    throw new NonexistentEntityException("The produit with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Produit produit;
            try {
                produit = em.getReference(Produit.class, id);
                produit.getIdProduit();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The produit with id " + id + " no longer exists.", enfe);
            }
            Fournisseur idFournisseur = produit.getFournisseur();
            if (idFournisseur != null) {
                idFournisseur.getProduitList().remove(produit);
                idFournisseur = em.merge(idFournisseur);
            }
            List<ProduitConditionnement> pcList = produit.getConditionnements();
            for (ProduitConditionnement pc : pcList){
                em.remove(pc);
            }
            
            
            em.remove(produit);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    
     public List<Produit> findProduitEntities(int idFournisseur, String CAS, String nom, int idProduitConditionnement){

        // comme pas de jointure externe en JPA, on fait 2 requetes et on additionne les résultats
        // Insertion dans une liste des colonnes de recherche
        //Set<Produit> listProduits = new ArraySet<Produit>();
        List<Produit> listProduits = new ArrayList<Produit>();
        EntityManager em = getEntityManager();
        String conditions = "";

        String sql  = "SELECT p FROM Produit p "
                    + "JOIN p.conditionnements pc "
                    + "JOIN p.substance s "
                    + "JOIN p.fournisseur f "
                    + "WHERE 0=0 ";
        if (idFournisseur > 0){
            sql+=" AND f.idFournisseur = " + idFournisseur;
        }
        if (CAS!=null && !"".equals(CAS)){
            sql+=" AND s.cas LIKE '%"+CAS+"%'";
        }
        if (nom!= null && !"".equals(nom)){
            sql+=" AND (p.nom LIKE '%"+Utilities.addQuote(nom)+"%' OR p.description LIKE '%"+ Utilities.addQuote(nom)+"%')";
        }
        if (idProduitConditionnement > 0){
            sql+=" AND pc.idProduitConditionnement = " + idProduitConditionnement;
        }
   
        
        Query q = em.createQuery(sql);
        listProduits = q.getResultList();
        
        return listProduits;

    }   
    
    //  prendre modele sur cette DAO pour cascader les methodes findProduitEntities
    public List<Produit> findProduitEntities(String searchValue, String searchColumns, int maxResults, int firstResult){
        if (searchValue == null || searchColumns==null || "".equals(searchValue) || "".equals(searchColumns)){
            return findProduitEntities(maxResults, firstResult);
        }
        // comme pas de jointure externe en JPA, on fait 2 requetes et on additionne les résultats
        // Insertion dans une liste des colonnes de recherche
        //Set<Produit> listProduits = new ArraySet<Produit>();
        List<Produit> listProduits = new ArrayList<Produit>();
        EntityManager em = getEntityManager();

        String sql = returnSqlQuery("distinct p", searchValue, searchColumns);
        
        Query q = em.createQuery(sql);
        if (maxResults>0){
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        
        listProduits = q.getResultList();
        
        return listProduits;

    }
    
    public int getProduitWithQueryCount(String searchValue, String searchColumns){
        if (searchValue == null || searchColumns==null || "".equals(searchValue) || "".equals(searchColumns)){
            return getProduitCount();
        }        
        String sql = returnSqlQuery("count(p)", searchValue, searchColumns);
        EntityManager em = getEntityManager();
        Query q = em.createQuery(sql);
        return ((Long) q.getSingleResult()).intValue();        
    }    
    
    
    private String returnSqlQuery(String select, String searchValue, String searchColumns){
        
        searchColumns = searchColumns.replace("[", "");
        searchColumns = searchColumns.replace("\"", "");
        searchColumns = searchColumns.replace("]", "");
        List<String> columns = Arrays.asList(searchColumns.split(","));


        // 1ère requete sur la table produit
        String conditions = "";
        String sql = "SELECT "+select+" FROM Produit p JOIN p.conditionnements pc JOIN p.substance s WHERE 0=0";
        for (String c : columns){
            conditions+=" OR p." + c + " LIKE '%"+searchValue+"%'";
        }

        conditions+=" OR pc.referenceFournisseur LIKE '%"+searchValue+"%'";
        conditions+=" OR p.description LIKE '%"+searchValue+"%'";
        conditions+=" OR s.cas LIKE '%"+searchValue+"%'";
        conditions+=" OR s.description LIKE '%"+searchValue+"%'";
        conditions+=" OR s.formule LIKE '%"+searchValue+"%'";
        conditions+=" OR s.nom LIKE '%"+searchValue+"%'";
        conditions+=" OR p.idProduit IN (SELECT pr.idProduit FROM Produit pr JOIN pr.tags t WHERE t.libelle LIKE '%"+searchValue+"%')";

        if (conditions.length() > 4){
            conditions = conditions.substring(4);
            sql+=" AND ("+conditions+")";
        }        
        
        return sql;
    }

    public List<Produit> findProduitEntities() {
        return findProduitEntities(true, -1, -1);
    }

    public List<Produit> findProduitEntities(int maxResults, int firstResult) {
        boolean all = false;
        if (maxResults<=0){
            all=true;
        }
        return findProduitEntities(all, maxResults, firstResult);
    }

    private List<Produit> findProduitEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Produit.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Produit findProduit(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Produit.class, id);
        } finally {
            em.close();
        }
    }
  
    
    
    public int getProduitCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Produit> rt = cq.from(Produit.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
