/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2012/01/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/

package fr.univtours.gpod.persistance.DAO;

import fr.univtours.gpod.persistance.DAO.exceptions.NonexistentEntityException;
import fr.univtours.gpod.persistance.DAO.exceptions.PreexistingEntityException;
import fr.univtours.gpod.persistance.Manager.PersistenceManager;
import fr.univtours.gpod.persistance.entities.Don;

import javax.persistence.*;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class DonDAO implements Serializable  {

    EntityManagerFactory emf= PersistenceManager.getInstance().getEntityManagerFactory();

    Date dateJour = Calendar.getInstance().getTime();


    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    } 
    
    public void create(Don don) throws PreexistingEntityException, Exception {
        EntityManager em = getEntityManager();
        try {
            em.getTransaction().begin();
            em.persist(don);
            em.getTransaction().commit();
        } catch (Exception ex) {
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Don don) throws NonexistentEntityException, Exception {
        EntityManager em = getEntityManager();
        try {
            em.getTransaction().begin();
            don = em.merge(don);
            em.getTransaction().commit();
        } catch (Exception ex) {
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(int id) throws NonexistentEntityException {
        EntityManager em = getEntityManager();
        try {
            em.getTransaction().begin();
            Don don;
            try {
                don = em.getReference(Don.class, id);
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The don with id " + id + " no longer exists.", enfe);
            }
            em.remove(don);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Don> findDonEntities() {
        return findDonEntities(true, -1, -1);
    }

    public List<Don> findDonEntities(int maxResults, int firstResult) {
        boolean all = false;
        if (maxResults<=0){
            all=true;
        }           
        return findDonEntities(all, maxResults, firstResult);
    }

    private List<Don> findDonEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Don.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Don findDon(int id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Don.class, id);
        } finally {
            em.close();
        }
    }

    public int getDonCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Don> rt = cq.from(Don.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
    public List<Don> findDonWithQuery(int idArticle, boolean accepte, int maxResults, int firstResult){
        boolean all = false;
        if (maxResults<=0){
            all=true;
        }        
        return findDonWithQuery(all, idArticle, accepte, maxResults, firstResult);
    }    
    
    public List<Don> findDonWithQuery(boolean all, int idArticle, boolean accepte, int maxResults, int firstResult){
        String sql = returnSqlQuery("d", idArticle, accepte);
        EntityManager em = getEntityManager();
        Query q = em.createQuery(sql).setParameter("dateJour", dateJour, TemporalType.DATE);;
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();            
    }

    public int getDonWithQueryCount(int idArticle, boolean accepte){
        String sql = returnSqlQuery("count(d)", idArticle, accepte);
        EntityManager em = getEntityManager();
        Query q = em.createQuery(sql).setParameter("dateJour", dateJour, TemporalType.DATE);
        return ((Long) q.getSingleResult()).intValue();
    }
    
       
    private String returnSqlQuery(String select, int idArticle, boolean accepte){

        String sql = "SELECT " + select + " FROM Don d JOIN d.article a WHERE d.accepte = " + accepte;

        if (idArticle >0 ){
            // obligation de mettre dateFinDon car il est en paramettre de createQuery
            sql += " AND (d.dateFinDon >= :dateJour OR d.dateFinDon < :dateJour) AND a.idArticle = " + idArticle;
        }else{
            // si ID=0 => pas d'article on ne cherche que les don non dépassés
            sql += " AND d.dateFinDon > :dateJour AND d.sousStructure IS NULL";
        }

        System.out.println(sql);
        
        return sql;
    }
       
}