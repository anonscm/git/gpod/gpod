/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2012/01/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/

package fr.univtours.gpod.persistance.DAO;

import fr.univtours.gpod.exceptions.NonexistentEntityException;
import fr.univtours.gpod.persistance.Manager.PersistenceManager;
import fr.univtours.gpod.persistance.entities.SalleStockage;
import fr.univtours.gpod.persistance.entities.Batiment;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;


public class SalleStockageDAO implements Serializable {

    EntityManagerFactory emf = PersistenceManager.getInstance().getEntityManagerFactory();
    
    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }
    
    public void create(SalleStockage salleStockage) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Batiment batiment = salleStockage.getBatiment();
            if (batiment != null) {
                batiment = em.getReference(batiment.getClass(), batiment.getIdBatiment());
                salleStockage.setBatiment(batiment);
            }
            em.persist(salleStockage);
            if (batiment != null) {
                batiment.getSalles().add(salleStockage);
                batiment = em.merge(batiment);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(SalleStockage salleStockage) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            SalleStockage persistentSalleStockage = em.find(SalleStockage.class, salleStockage.getIdSalle());
            Batiment batimentOld = persistentSalleStockage.getBatiment();
            Batiment batimentNew = salleStockage.getBatiment();
            if (batimentNew != null) {
                batimentNew = em.getReference(batimentNew.getClass(), batimentNew.getIdBatiment());
                salleStockage.setBatiment(batimentNew);
            }
            salleStockage = em.merge(salleStockage);
            if (batimentOld != null && !batimentOld.equals(batimentNew)) {
                batimentOld.getSalles().remove(salleStockage);
                batimentOld = em.merge(batimentOld);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = salleStockage.getIdSalle();
                if (findSalleStockage(id) == null) {
                    throw new NonexistentEntityException("The salleStockage with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            SalleStockage salleStockage;
            try {
                salleStockage = em.getReference(SalleStockage.class, id);
                salleStockage.getIdSalle();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The salleStockage with id " + id + " no longer exists.", enfe);
            }
            Batiment batiment = salleStockage.getBatiment();
            if (batiment != null) {
                batiment.getSalles().remove(salleStockage);
                batiment = em.merge(batiment);
            }
            em.remove(salleStockage);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<SalleStockage> findSalleStockageEntities() {
        return findSalleStockageEntities(true, -1, -1);
    }

    public List<SalleStockage> findSalleStockageEntities(int maxResults, int firstResult) {
        boolean all = false;
        if (maxResults<=0){
            all=true;
        }
        return findSalleStockageEntities(all, maxResults, firstResult);
    }

    private List<SalleStockage> findSalleStockageEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(SalleStockage.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public SalleStockage findSalleStockage(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(SalleStockage.class, id);
        } finally {
            em.close();
        }
    }

    public List<SalleStockage> findSalleStockageFromBatiment(Batiment batiment){
        EntityManager em = getEntityManager();
        Query query = em.createNamedQuery("SalleStockage.findByBatiment");
        query.setParameter("batiment",batiment);
        return query.getResultList();
    }


    public List<SalleStockage> findSalleStockageWithQuery(String searchValue, String searchColumns, int maxResults, int firstResult){
        if (searchValue == null || searchColumns==null || "".equals(searchValue) || "".equals(searchColumns)){
            return findSalleStockageEntities(maxResults, firstResult);
        }
        
        EntityManager em = getEntityManager();
        String sql = returnSqlQuery("x", searchValue, searchColumns);
        Query q = em.createQuery(sql);
        if (maxResults>0){
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();    
        
    }
    
    public int getSalleStockageCount(String searchValue, String searchColumns){
        if (searchValue == null || searchColumns==null || "".equals(searchValue) || "".equals(searchColumns)){
            return getSalleStockageCount();
        }
        String sql = returnSqlQuery("count(x)", searchValue, searchColumns);
        
        EntityManager em = getEntityManager();
        Query q = em.createQuery(sql);
        return ((Long) q.getSingleResult()).intValue();                
    }      
    
    private String returnSqlQuery(String select, String searchValue, String searchColumns){
        // Insertion dans une liste des colonnes de recherche
        searchColumns = searchColumns.replace("[", "");
        searchColumns = searchColumns.replace("\"", "");
        searchColumns = searchColumns.replace("]", "");
        List<String> columns = Arrays.asList(searchColumns.split(","));
        String conditions = "";
        String sql = "SELECT "+select+" FROM SalleStockage x WHERE 0=0"; 
        for (String c : columns){
            conditions+=" OR x." + c + " LIKE '%"+searchValue+"%'";
        }
        if (conditions.length() > 4){
            conditions = conditions.substring(4);
            sql+=" AND ("+conditions+")";
        }
        
        return sql;
    }    
    
        
    public int getSalleStockageCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<SalleStockage> rt = cq.from(SalleStockage.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
    
}
