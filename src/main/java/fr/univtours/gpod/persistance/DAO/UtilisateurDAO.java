/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2012/01/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/

package fr.univtours.gpod.persistance.DAO;
import fr.univtours.gpod.exceptions.NonexistentEntityException;
import fr.univtours.gpod.persistance.Manager.PersistenceManager;
import fr.univtours.gpod.persistance.entities.Utilisateur;
import fr.univtours.gpod.utils.Utilities;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.*;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;


public class UtilisateurDAO implements Serializable  {

 
    
    EntityManagerFactory emf = PersistenceManager.getInstance().getEntityManagerFactory();

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }
    
     public void create(Utilisateur utilisateur) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(utilisateur);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Utilisateur utilisateur) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            utilisateur = em.merge(utilisateur);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = utilisateur.getIdUtilisateur();
                if (findUtilisateur(id) == null) {
                    throw new NonexistentEntityException("The utilisateur with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        long id2 = id;
        Long id3 = id2;
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Utilisateur utilisateur;
            try {
                utilisateur = em.getReference(Utilisateur.class, id3);
                utilisateur.getIdUtilisateur();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The utilisateur with id " + id3 + " no longer exists.", enfe);
            }
            em.remove(utilisateur);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }
    
    public Utilisateur identiyUser(String login, String password){
        Utilisateur user = null;
        EntityManager em = getEntityManager();
        password = Utilities.md5(password);
        String sql = "SELECT u FROM Utilisateur u WHERE u.login = '" + login + "' AND u.password = '" + password + "';";
        Logger.getLogger(UtilisateurDAO.class.getName()).log(Level.SEVERE, sql);
        Query query = em.createQuery(sql); 
        try{
            user = (Utilisateur) query.getSingleResult();
        }catch(NoResultException ex){
            Logger.getLogger(UtilisateurDAO.class.getName() + " PAS DE RESULTAT").log(Level.SEVERE, null, ex);
        }finally{
            em.close();
            return user;
        }
        
    }

    public Utilisateur identiyUserByCas(String uid){
        Utilisateur user = null;
        EntityManager em = getEntityManager();
        String sql = "SELECT u FROM Utilisateur u WHERE u.login = '" + uid + "';";
        Logger.getLogger(UtilisateurDAO.class.getName()).log(Level.INFO, sql);
        try{
            Query query = em.createQuery(sql);
            user = (Utilisateur) query.getSingleResult();
            Logger.getLogger(UtilisateurDAO.class.getName()).log(Level.INFO, user.getEmail());
        }catch(NoResultException ex){
            Logger.getLogger(UtilisateurDAO.class.getName() + " PAS DE RESULTAT").log(Level.SEVERE, null, ex);
        }catch(Exception ex){
            Logger.getLogger(UtilisateurDAO.class.getName()).log(Level.SEVERE, ex.getMessage());
        }finally{
            em.close();
            return user;
        }

    }


    
    public List<Utilisateur> findUtilisateurWithQuery(String searchValue, String searchColumns, int maxResults, int firstResult){      
        String sql = returnSqlQuery("u", searchValue, searchColumns);       
        EntityManager em = getEntityManager();
        Query q = em.createQuery(sql);
        q.setMaxResults(maxResults);
        q.setFirstResult(firstResult);
        return q.getResultList();      
    }
    
    public int getUtilisateurWithQueryCount(String searchValue, String searchColumns){      
        String sql = returnSqlQuery("count(u)", searchValue, searchColumns);       
        EntityManager em = getEntityManager();
        Query q = em.createQuery(sql);
        return ((Long) q.getSingleResult()).intValue();
    }    
    
    private String returnSqlQuery(String select, String searchValue, String searchColumns){
                // Insertion dans une liste des colonnes de recherche
        searchColumns = searchColumns.replace("[", "");
        searchColumns = searchColumns.replace("\"", "");
        searchColumns = searchColumns.replace("]", "");
        List<String> columns = Arrays.asList(searchColumns.split(","));
        String conditions = "";
        String sql = "SELECT "+select+" FROM Utilisateur u WHERE 0=0"; 
        for (String c : columns){
            conditions+=" OR u." + c + " LIKE '%"+searchValue+"%'";
        }
        if (conditions.length() > 4){
            conditions = conditions.substring(4);
            sql+=" AND ("+conditions+")";
        }
        return sql;
    }
    
    
    
    public List<Utilisateur> findUtilisateurEntities() {
        return findUtilisateurEntities(true, -1, -1);
    }

    public List<Utilisateur> findUtilisateurEntities(int maxResults, int firstResult) {
        boolean all = false;
        if (maxResults<=0){
            all=true;
        }        
        return findUtilisateurEntities(all, maxResults, firstResult);
    }

    private List<Utilisateur> findUtilisateurEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Utilisateur.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }
    
     public Utilisateur findUtilisateur(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Utilisateur.class, id);
        } finally {
            em.close();
        }
    }

    public int getUtilisateurCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Utilisateur> rt = cq.from(Utilisateur.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }   
    // UTilisé pour savoir si un login est déjà utilisé par un autre utilisateur
    public boolean isLoginExist(String login, Long idUtilisateur){
        EntityManager em = getEntityManager();
        List<Utilisateur> users = null;
        // Nouvel utilisateur on utilise findByLogin
        if (idUtilisateur == 0){
            Query query = em.createNamedQuery("Utilisateur.findByLogin");
            query.setParameter("login", login);
            users = query.getResultList();         
        }else{
            Query query = em.createNamedQuery("Utilisateur.findByLoginExceptU");
            query.setParameter("login", login);
            query.setParameter("idUtilisateur", idUtilisateur);
            users = query.getResultList();           
        }
       
        
        return (!users.isEmpty());
    }
    
    
}
