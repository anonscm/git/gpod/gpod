/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2012/01/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/

package fr.univtours.gpod.persistance.DAO;

import fr.univtours.gpod.persistance.DAO.exceptions.NonexistentEntityException;
import fr.univtours.gpod.persistance.DAO.exceptions.RollbackFailureException;
import fr.univtours.gpod.persistance.Manager.PersistenceManager;
import fr.univtours.gpod.persistance.entities.ClickeyScan;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.UserTransaction;

/**
 *
 * @author geoffroy.vibrac
 */
public class ClickeyScanDAO implements Serializable {

EntityManagerFactory emf = PersistenceManager.getInstance().getEntityManagerFactory();
    
     public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }  

    public void create(ClickeyScan clickeyScan) throws RollbackFailureException, Exception {
        EntityManager em = getEntityManager();
        try {
            em.getTransaction().begin();            
            em.persist(clickeyScan);
            em.getTransaction().commit();
        } catch (Exception ex) {
            try {
                em.getTransaction().rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(ClickeyScan clickeyScan) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = getEntityManager();
        try {
            em.getTransaction().begin();
            clickeyScan = em.merge(clickeyScan);
            em.getTransaction().commit();
        } catch (Exception ex) {
            try {
                em.getTransaction().rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = clickeyScan.getIdClickeyScan();
                if (findClickeyScan(id) == null) {
                    throw new NonexistentEntityException("The clickeyScan with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = getEntityManager();
        try {
            em.getTransaction().begin();
            ClickeyScan clickeyScan;
            try {
                clickeyScan = em.getReference(ClickeyScan.class, id);
                clickeyScan.getIdClickeyScan();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The clickeyScan with id " + id + " no longer exists.", enfe);
            }
            em.remove(clickeyScan);
            em.getTransaction().commit();
        } catch (Exception ex) {
            try {
                em.getTransaction().rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<ClickeyScan> findClickeyScanEntities() {
        return findClickeyScanEntities(true, -1, -1);
    }

    public List<ClickeyScan> findClickeyScanEntities(int maxResults, int firstResult) {
        return findClickeyScanEntities(false, maxResults, firstResult);
    }

    private List<ClickeyScan> findClickeyScanEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(ClickeyScan.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public ClickeyScan findClickeyScan(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(ClickeyScan.class, id);
        } finally {
            em.close();
        }
    }
    
    public int findIfScanExist(ClickeyScan cs){
        EntityManager em = getEntityManager();
        Query query = em.createNamedQuery("ClickeyScan.findByMultiCriteria");
        query.setParameter("uIDClickey",cs.getUIDClickey());
        query.setParameter("article",cs.getArticle());
        query.setParameter("timeStamp",cs.getTimeStamp());
        return query.getResultList().size();
    }
    
    public int findIfTagExist(ClickeyScan cs){
        EntityManager em = getEntityManager();
        Query query = em.createNamedQuery("ClickeyScan.findIfTagExist");
        query.setParameter("article",cs.getArticle());
        query.setParameter("utilisateur",cs.getUtilisateur());
        return query.getResultList().size();
    }     
    
    
    

    public int getClickeyScanCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<ClickeyScan> rt = cq.from(ClickeyScan.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
