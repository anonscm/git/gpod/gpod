/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2012/01/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/

package fr.univtours.gpod.persistance.DAO;

import fr.univtours.gpod.exceptions.NonexistentEntityException;
import fr.univtours.gpod.persistance.Manager.PersistenceManager;
import fr.univtours.gpod.persistance.entities.SousStructure;
import fr.univtours.gpod.persistance.entities.Structure;
import fr.univtours.gpod.persistance.entities.Utilisateur;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author geoffroy.vibrac
 */
public class SousStructureDAO implements Serializable{

    EntityManagerFactory emf = PersistenceManager.getInstance().getEntityManagerFactory();
 
    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

       public List<SousStructure> findSousStructureEntities(String searchValue, String searchColumns, int maxResults, int firstResult, String type){
        if (searchValue == null || searchColumns==null || "".equals(searchValue) || "".equals(searchColumns)){
            return findSousStructureeEntities(maxResults, firstResult, type);
        }

        String sql = returnSqlQuery("e", searchValue, searchColumns, type);

        EntityManager em = getEntityManager();
        Query q = em.createQuery(sql);
        q.setMaxResults(maxResults);
        q.setFirstResult(firstResult);
        return q.getResultList();

    }
    
    public List<SousStructure> findSousStructureEntities() {
        return findSousStructureEntities(true, -1, -1, null);
    }

    public List<SousStructure> findSousStructureeEntities(int maxResults, int firstResult, String type) {
        boolean all = false;
        if (maxResults<=0){
            all=true;
        }
        return findSousStructureEntities(all, maxResults, firstResult, type);
    }

    private List<SousStructure> findSousStructureEntities(boolean all, int maxResults, int firstResult, String type) {
        EntityManager em = getEntityManager();
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery cq = cb.createQuery();
            Root<SousStructure> sousStructure = cq.from(SousStructure.class);
            cq.select(sousStructure);
            if (type!= null){
                cq.where(cb.equal(sousStructure.get("type"), type));
            }
            cq.orderBy(cb.asc(sousStructure.get("type")), cb.asc(sousStructure.get("nom")));

            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }
    
    
    public List<String> getEmailSecretariatList(SousStructure sousStructure){

        String sql="";
        
        sql = "SELECT u.email FROM sousStructures ss JOIN ss.utilisateurs u ";
        
        
        EntityManager em = getEntityManager();
        Query q = em.createQuery(sql);
        return q.getResultList();
        
    }

    public SousStructure findSousStructure(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(SousStructure.class, id);
        } finally {
            em.close();
        }
    }


    public int getSousStructureCount(String searchValue, String searchColumns, String type){
        if (searchValue == null || searchColumns==null || "".equals(searchValue) || "".equals(searchColumns)){
            return getSousStructureCount(type);
        }
        String sql = returnSqlQuery("count(e)", searchValue, searchColumns, type);
        
        EntityManager em = getEntityManager();
        Query q = em.createQuery(sql);
        return ((Long) q.getSingleResult()).intValue();        
        
    }
    
    
   private String returnSqlQuery(String select, String searchValue, String searchColumns, String type){
               // Insertion dans une liste des colonnes de recherche
        searchColumns = searchColumns.replace("[", "");
        searchColumns = searchColumns.replace("\"", "");
        searchColumns = searchColumns.replace("]", "");
        List<String> columns = Arrays.asList(searchColumns.split(","));
        String conditions = "";
        String sql = "SELECT "+select+" FROM SousStructure e WHERE 0=0";
        for (String c : columns){
            conditions+=" OR e." + c + " LIKE '%"+searchValue+"%'";
        }
        if (conditions.length() > 4){
            conditions = conditions.substring(4);
            sql+=" AND ("+conditions+")";
        }

        if (type != null){
            sql+=" AND e.type='"+ type +"'";
        }

        sql+= " ORDER BY e.type, e.nom";
        return sql;
   }    
    
    
    public int getSousStructureCount(String type) {
        EntityManager em = getEntityManager();
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<SousStructure> rt = cq.from(SousStructure.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            cq.where(cb.equal(rt.get("type"), type));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
    public void create(SousStructure sousStructure) {
        EntityManager em = getEntityManager();
        try {
            em.getTransaction().begin();
            Structure structure = sousStructure.getStructure();
            if (structure != null) {
                structure = em.getReference(structure.getClass(), structure.getIdStructure());
                sousStructure.setStructure(structure);
            }
            em.persist(sousStructure);
            if (structure != null) {
                structure.getSousStructure().add(sousStructure);
                structure = em.merge(structure);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(SousStructure sousStructure) throws NonexistentEntityException, Exception {
        EntityManager em = emf.createEntityManager();
        try {
            em.getTransaction().begin();
            SousStructure persistentSousStructure = em.find(SousStructure.class, sousStructure.getIdSousStructure());
            Structure structureOld = persistentSousStructure.getStructure();
            Structure structureNew = sousStructure.getStructure();
            if (structureNew != null) {
                structureNew = em.getReference(structureNew.getClass(), structureNew.getIdStructure());
                sousStructure.setStructure(structureNew);
            }
            sousStructure = em.merge(sousStructure);
            if (structureOld != null && !structureOld.equals(structureNew)) {
                structureOld.getSousStructure().remove(sousStructure);
                structureOld = em.merge(structureOld);
            }
            if (structureNew != null && !structureNew.equals(structureOld)) {
                structureNew.getSousStructure().add(sousStructure);
                structureNew = em.merge(structureNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = sousStructure.getIdSousStructure();
                if (findSousStructure(id) == null) {
                    throw new NonexistentEntityException("The sous structure with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException, Exception {
        EntityManager em = getEntityManager();
        try {
            em.getTransaction().begin();
            SousStructure sousStructure;
            try {
                sousStructure = em.getReference(SousStructure.class, id);
                sousStructure.getIdSousStructure();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The soustructure with id " + id + " no longer exists.", enfe);
            }
            // On test si il y a des utilisateurs
//            List<Utilisateur> users = sousStructure.getUtilisateurs();
//            if (users.isEmpty()){            
                
                Structure structure = sousStructure.getStructure();
                if (structure != null) {
                    structure.getSousStructure().remove(sousStructure);
                    structure = em.merge(structure);
                }
                
                em.remove(sousStructure);
                em.getTransaction().commit();
//            }else{
//                throw new Exception("Au moins un utilisateur est dans cette équipe");
//            }
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }    
    
}
