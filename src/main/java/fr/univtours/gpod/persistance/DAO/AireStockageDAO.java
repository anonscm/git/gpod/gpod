/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2012/01/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/


package fr.univtours.gpod.persistance.DAO;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import fr.univtours.gpod.exceptions.NonexistentEntityException;
import fr.univtours.gpod.persistance.Manager.PersistenceManager;
import fr.univtours.gpod.persistance.entities.AireStockage;
import fr.univtours.gpod.persistance.entities.SalleStockage;
import fr.univtours.gpod.persistance.entities.TypeAire;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;


public class AireStockageDAO implements Serializable {

    EntityManagerFactory emf = PersistenceManager.getInstance().getEntityManagerFactory();
    
    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }
    
    public void create(AireStockage aireStockage) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TypeAire typeAire = aireStockage.getTypeAire();
            if (typeAire != null) {
                typeAire = em.getReference(typeAire.getClass(), typeAire.getIdTypeAire());
                aireStockage.setTypeAire(typeAire);
            }
            SalleStockage salleStockage = aireStockage.getSalleStockage();
            if (salleStockage != null) {
                salleStockage = em.getReference(salleStockage.getClass(), salleStockage.getIdSalle());
                aireStockage.setSalleStockage(salleStockage);
            }
            em.persist(aireStockage);
            if (typeAire != null) {
                typeAire.getAires().add(aireStockage);
                typeAire = em.merge(typeAire);
            }
            if (salleStockage != null) {
                salleStockage.getAires().add(aireStockage);
                salleStockage = em.merge(salleStockage);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(AireStockage aireStockage) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            AireStockage persistentAireStockage = em.find(AireStockage.class, aireStockage.getIdAire());
            TypeAire typeAireOld = persistentAireStockage.getTypeAire();
            TypeAire typeAireNew = aireStockage.getTypeAire();
            SalleStockage salleStockageOld = persistentAireStockage.getSalleStockage();
            SalleStockage salleStockageNew = aireStockage.getSalleStockage();
            if (typeAireNew != null) {
                typeAireNew = em.getReference(typeAireNew.getClass(), typeAireNew.getIdTypeAire());
                aireStockage.setTypeAire(typeAireNew);
            }
            if (salleStockageNew != null) {
                salleStockageNew = em.getReference(salleStockageNew.getClass(), salleStockageNew.getIdSalle());
                aireStockage.setSalleStockage(salleStockageNew);
            }
            aireStockage = em.merge(aireStockage);
            if (typeAireOld != null && !typeAireOld.equals(typeAireNew)) {
                typeAireOld.getAires().remove(aireStockage);
                typeAireOld = em.merge(typeAireOld);
            }
            if (typeAireNew != null && !typeAireNew.equals(typeAireOld)) {
                typeAireNew.getAires().add(aireStockage);
                typeAireNew = em.merge(typeAireNew);
            }
            if (salleStockageOld != null && !salleStockageOld.equals(salleStockageNew)) {
                salleStockageOld.getAires().remove(aireStockage);
                salleStockageOld = em.merge(salleStockageOld);
            }
            if (salleStockageNew != null && !salleStockageNew.equals(salleStockageOld)) {
                salleStockageNew.getAires().add(aireStockage);
                salleStockageNew = em.merge(salleStockageNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = aireStockage.getIdAire();
                if (findAireStockage(id) == null) {
                    throw new NonexistentEntityException("The aireStockage with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            AireStockage aireStockage;
            try {
                aireStockage = em.getReference(AireStockage.class, id);
                aireStockage.getIdAire();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The aireStockage with id " + id + " no longer exists.", enfe);
            }
            TypeAire typeAire = aireStockage.getTypeAire();
            if (typeAire != null) {
                typeAire.getAires().remove(aireStockage);
                typeAire = em.merge(typeAire);
            }
            SalleStockage salleStockage = aireStockage.getSalleStockage();
            if (salleStockage != null) {
                salleStockage.getAires().remove(aireStockage);
                salleStockage = em.merge(salleStockage);
            }
            em.remove(aireStockage);
            em.getTransaction().commit();        
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<AireStockage> findAireStockageEntities() {
        return findAireStockageEntities(true, -1, -1);
    }

    public List<AireStockage> findAireStockageEntities(int maxResults, int firstResult) {
        boolean all = false;
        if (maxResults<=0){
            all=true;
        }
        return findAireStockageEntities(all, maxResults, firstResult);
    }

    private List<AireStockage> findAireStockageEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(AireStockage.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public AireStockage findAireStockage(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(AireStockage.class, id);
        } finally {
            em.close();
        }
    }

    public List<AireStockage> findAireFromSalleStockage(SalleStockage salleStockage){
        EntityManager em = getEntityManager();
        Query query = em.createNamedQuery("AireStockage.findBySalleStockage");
        query.setParameter("salleStockage",salleStockage);
        return query.getResultList();
    }


    public List<AireStockage> findAireStockageWithQuery(String searchValue, String searchColumns, int maxResults, int firstResult){
        if (searchValue == null || searchColumns==null || "".equals(searchValue) || "".equals(searchColumns)){
            return findAireStockageEntities(maxResults, firstResult);
        }
        
        EntityManager em = getEntityManager();
        String sql = returnSqlQuery("x", searchValue, searchColumns);
        Query q = em.createQuery(sql);
        if (maxResults>0){
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();    
        
    }
    
    public int getAireStockageCount(String searchValue, String searchColumns){
        if (searchValue == null || searchColumns==null || "".equals(searchValue) || "".equals(searchColumns)){
            return getAireStockageCount();
        }
        String sql = returnSqlQuery("count(x)", searchValue, searchColumns);
        
        EntityManager em = getEntityManager();
        Query q = em.createQuery(sql);
        return ((Long) q.getSingleResult()).intValue();                
    }      
    
    private String returnSqlQuery(String select, String searchValue, String searchColumns){
        // Insertion dans une liste des colonnes de recherche
        searchColumns = searchColumns.replace("[", "");
        searchColumns = searchColumns.replace("\"", "");
        searchColumns = searchColumns.replace("]", "");
        List<String> columns = Arrays.asList(searchColumns.split(","));
        String conditions = "";
        String sql = "SELECT "+select+" FROM AireStockage x WHERE 0=0"; 
        for (String c : columns){
            conditions+=" OR x." + c + " LIKE '%"+searchValue+"%'";
        }
        if (conditions.length() > 4){
            conditions = conditions.substring(4);
            sql+=" AND ("+conditions+")";
        }
        
        return sql;
    }    
    
        
    public int getAireStockageCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<AireStockage> rt = cq.from(AireStockage.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
    
}
