/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2012/01/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/

package fr.univtours.gpod.persistance.DAO;

import fr.univtours.gpod.exceptions.IllegalOrphanException;
import fr.univtours.gpod.exceptions.NonexistentEntityException;
import fr.univtours.gpod.persistance.Manager.PersistenceManager;
import fr.univtours.gpod.persistance.entities.AireStockage;
import fr.univtours.gpod.persistance.entities.Batiment;
import fr.univtours.gpod.persistance.entities.TypeAire;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

public class TypeAireDAO implements Serializable  {

    EntityManagerFactory emf= PersistenceManager.getInstance().getEntityManagerFactory();

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }    
    
    public void create(TypeAire typeAire) {
        if (typeAire.getAires() == null) {
            typeAire.setAires(new ArrayList<AireStockage>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<AireStockage> attachedAires = new ArrayList<AireStockage>();
            for (AireStockage airesAireStockageToAttach : typeAire.getAires()) {
                airesAireStockageToAttach = em.getReference(airesAireStockageToAttach.getClass(), airesAireStockageToAttach.getIdAire());
                attachedAires.add(airesAireStockageToAttach);
            }
            typeAire.setAires(attachedAires);
            em.persist(typeAire);
            for (AireStockage airesAireStockage : typeAire.getAires()) {
                TypeAire oldTypeAireOfAiresAireStockage = airesAireStockage.getTypeAire();
                airesAireStockage.setTypeAire(typeAire);
                airesAireStockage = em.merge(airesAireStockage);
                if (oldTypeAireOfAiresAireStockage != null) {
                    oldTypeAireOfAiresAireStockage.getAires().remove(airesAireStockage);
                    oldTypeAireOfAiresAireStockage = em.merge(oldTypeAireOfAiresAireStockage);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(TypeAire typeAire) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TypeAire persistentTypeAire = em.find(TypeAire.class, typeAire.getIdTypeAire());
            List<AireStockage> airesOld = persistentTypeAire.getAires();
            List<AireStockage> airesNew = typeAire.getAires();
            List<String> illegalOrphanMessages = null;
            for (AireStockage airesOldAireStockage : airesOld) {
                if (!airesNew.contains(airesOldAireStockage)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain AireStockage " + airesOldAireStockage + " since its typeAire field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<AireStockage> attachedAiresNew = new ArrayList<AireStockage>();
            for (AireStockage airesNewAireStockageToAttach : airesNew) {
                airesNewAireStockageToAttach = em.getReference(airesNewAireStockageToAttach.getClass(), airesNewAireStockageToAttach.getIdAire());
                attachedAiresNew.add(airesNewAireStockageToAttach);
            }
            airesNew = attachedAiresNew;
            typeAire.setAires(airesNew);
            typeAire = em.merge(typeAire);
            for (AireStockage airesNewAireStockage : airesNew) {
                if (!airesOld.contains(airesNewAireStockage)) {
                    TypeAire oldTypeAireOfAiresNewAireStockage = airesNewAireStockage.getTypeAire();
                    airesNewAireStockage.setTypeAire(typeAire);
                    airesNewAireStockage = em.merge(airesNewAireStockage);
                    if (oldTypeAireOfAiresNewAireStockage != null && !oldTypeAireOfAiresNewAireStockage.equals(typeAire)) {
                        oldTypeAireOfAiresNewAireStockage.getAires().remove(airesNewAireStockage);
                        oldTypeAireOfAiresNewAireStockage = em.merge(oldTypeAireOfAiresNewAireStockage);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = typeAire.getIdTypeAire();
                if (findTypeAire(id) == null) {
                    throw new NonexistentEntityException("The typeAire with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
           em = getEntityManager();
            em.getTransaction().begin();
            TypeAire typeAire;
            try {
                typeAire = em.getReference(TypeAire.class, id);
                typeAire.getIdTypeAire();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The typeAire with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<AireStockage> airesOrphanCheck = typeAire.getAires();
            for (AireStockage airesOrphanCheckAireStockage : airesOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This TypeAire (" + typeAire + ") cannot be destroyed since the AireStockage " + airesOrphanCheckAireStockage + " in its aires field has a non-nullable typeAire field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(typeAire);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<TypeAire> findTypeAireEntities() {
        return findTypeAireEntities(true, -1, -1);
    }

    public List<TypeAire> findTypeAireEntities(int maxResults, int firstResult) {
        boolean all = false;
        if (maxResults<=0){
            all=true;
        }        
        return findTypeAireEntities(all, maxResults, firstResult);
    }

    private List<TypeAire> findTypeAireEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(TypeAire.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }
    
    public List<TypeAire> findTypeAireWithQuery(String searchValue, String searchColumns, int maxResults, int firstResult){
        // Insertion dans une liste des colonnes de recherche
        searchColumns = searchColumns.replace("[", "");
        searchColumns = searchColumns.replace("\"", "");
        searchColumns = searchColumns.replace("]", "");
        List<String> columns = Arrays.asList(searchColumns.split(","));
        String conditions = "";
        String sql = "SELECT x FROM TypeAire x WHERE 0=0"; 
        for (String c : columns){
            conditions+=" OR x." + c + " LIKE '%"+searchValue+"%'";
        }
        if (conditions.length() > 4){
            conditions = conditions.substring(4);
            sql+=" AND ("+conditions+")";
        }
        
        EntityManager em = getEntityManager();
        Query q = em.createQuery(sql);
        q.setMaxResults(maxResults);
        q.setFirstResult(firstResult);
        return q.getResultList();    
        
    }
    

    public TypeAire findTypeAire(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(TypeAire.class, id);
        } finally {
            em.close();
        }
    }

    public int getTypeAireCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<TypeAire> rt = cq.from(TypeAire.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
