/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2012/01/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/

package fr.univtours.gpod.persistance.DAO;

import fr.univtours.gpod.exceptions.IllegalOrphanException;
import fr.univtours.gpod.exceptions.RollbackFailureException;
import fr.univtours.gpod.persistance.DAO.exceptions.NonexistentEntityException;
import fr.univtours.gpod.persistance.Manager.PersistenceManager;
import fr.univtours.gpod.persistance.entities.Article;
import fr.univtours.gpod.persistance.entities.Commande;
import fr.univtours.gpod.persistance.entities.CommandeLigne;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import fr.univtours.gpod.persistance.entities.Livraison;
import fr.univtours.gpod.persistance.entities.SousStructure;
import fr.univtours.gpod.utils.Utilities;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Selection;

/**
 *
 * @author geoffroy.vibrac
 */
public class CommandeDAO {

    EntityManagerFactory emf = PersistenceManager.getInstance().getEntityManagerFactory();

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }
  
    
     public void create(Commande commande) throws RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<CommandeLigne> commandeLigneList = commande.getCommandeLigneList();
            commande.setCommandeLigneList(null);
            em.persist(commande);
            for (CommandeLigne ll : commandeLigneList){
                ll.setCommande(commande);
                em.persist(ll);
            }
            commande.setCommandeLigneList(commandeLigneList);
            commande = em.merge(commande);
            em.getTransaction().commit();
        } catch (Exception ex) {
            try {
                em.getTransaction().rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Commande commande) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<CommandeLigne> commandeLigneListOLD = null;
            Commande commandeOLD = em.getReference(Commande.class, commande.getIdCommande());
            if (commandeOLD != null){
                commandeLigneListOLD = commandeOLD.getCommandeLigneList();
            }
            List<CommandeLigne> commandeLigneListNew = commande.getCommandeLigneList();
            commande.setCommandeLigneList(null);                       
            commande = em.merge(commande);
            // je parcours les lignes de commande
            for (CommandeLigne ll : commandeLigneListNew){
                ll.setCommande(commande);
                // si la ligne existe déjà, je merge
                if (commandeLigneListOLD.contains(ll)){
                    em.merge(ll);
                // si elle n'existe pas, je persist
                }else{                    
                    em.persist(ll);
                }
            }              
            commande.setCommandeLigneList(commandeLigneListNew);
            commande = em.merge(commande);            
            
            em.getTransaction().commit();
        } catch (Exception ex) {
            try {
                em.getTransaction().rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = commande.getIdCommande();
                if (findCommande(id) == null) {
                    throw new NonexistentEntityException("The commande with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Commande commande;
            try {
                commande = em.getReference(Commande.class, id);
                commande.getIdCommande();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The commande with id " + id + " no longer exists.", enfe);
            }
            em.remove(commande);
            em.getTransaction().commit();
        } catch (Exception ex) {
            try {
                em.getTransaction().rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }
    
    
    public void destroyCommandeLigne(Integer id) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            CommandeLigne cl;
            try {
                cl = em.getReference(CommandeLigne.class, id);
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The commande with id " + id + " no longer exists.", enfe);
            }
            Commande commande = cl.getCommande();
            List<CommandeLigne> clList = commande.getCommandeLigneList();
            clList.remove(cl);
            commande.setCommandeLigneList(clList);
            
            em.remove(cl);
            em.merge(commande);
            em.getTransaction().commit();
        } catch (Exception ex) {
            try {
                em.getTransaction().rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }
    
    public int getCommandeEntitiesBySousStructureCount(List<SousStructure> sousStructures, int periodInMonth) {
        EntityManager em = getEntityManager();
        try {
          CriteriaQuery cq = returnSqlQuery("count", sousStructures, periodInMonth);
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }        
    }

    public List<Commande> findCommandeEntitiesBySousStructure(int maxResults, int firstResult, List<SousStructure> sousStructures, int periodInMonth) {
        boolean all = false;
        if (maxResults<=0){
            all=true;
        }
        EntityManager em = getEntityManager();
        try {



            CriteriaQuery cq = returnSqlQuery("*", sousStructures, periodInMonth);
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }
    
    private CriteriaQuery returnSqlQuery(String typeSelect, List<SousStructure> sousStructures, int periodInMonth){
            EntityManager em = getEntityManager();
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery cq = cb.createQuery();
            Root<Commande> commande = cq.from(Commande.class);
            if ("count".equals(typeSelect)){
                cq.select(em.getCriteriaBuilder().count(commande));                
            }else{
                cq.select(commande);
            }
            Predicate pre = null;
            if (sousStructures != null && sousStructures.size() > 0) {
                for (SousStructure ss : sousStructures) {
                   if (pre == null) {
                      pre = cb.equal(commande.get("sousStructure"), ss);
                   } else {
                      pre = cb.or(pre, cb.equal(commande.get("sousStructure"), ss));
                   }
                }
            // il faut gérer le cas ou l'utilisateur n'a aucune sous structure !!
            }else{
                   if (pre == null) {
                      pre = cb.equal(commande.get("idCommande"), -1);
                   } else {
                      pre = cb.or(pre, cb.equal(commande.get("idCommande"), -1));
                   }
            }   
            
            
            if (periodInMonth>0){
                   Calendar calendar = Calendar.getInstance();
                   calendar.add(Calendar.MONTH, -1*periodInMonth);
                   Date dateMin = calendar.getTime();
                   if (pre == null) {
                      pre = cb.greaterThan(commande.get("dateCommande").as(Date.class), dateMin);
                   } else {
                      pre = cb.and(pre, cb.greaterThan(commande.get("dateCommande").as(Date.class), dateMin));
                   }                
            }
            
            
            cq.where(pre);
            cq.orderBy(cb.desc(commande.get("dateCommande")));   
            return cq;
        
    }


    public List<Commande> findCommandeEntities() {
        return findCommandeEntities(true, -1, -1);
    }

    public List<Commande> findCommandeEntities(int maxResults, int firstResult) {
        boolean all = false;
        if (maxResults<=0){
            all=true;
        }
        return findCommandeEntities(all, maxResults, firstResult);
    }

    private List<Commande> findCommandeEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Commande.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }
    
    public CommandeLigne findCommandeLigne(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(CommandeLigne.class, id);
        } finally {
            em.close();
        }
    }    

    public Commande findCommande(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Commande.class, id);
        } finally {
            em.close();
        }
    }

    public int getCommandeCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Commande> rt = cq.from(Commande.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
    public int getCommandeQuantiteCommandee(Commande commande){      
        int quantiteCommandee=0;
        EntityManager em = getEntityManager();
        try{
            String sql = "SELECT SUM(cl.quantite) FROM CommandeLigne cl"
                        +" JOIN cl.commande c"
                        +" WHERE c = :commande";
            Query q = em.createQuery(sql);
            q.setParameter("commande", commande);
            Long qte = (Long)q.getSingleResult();
            if (qte !=null){
                quantiteCommandee = qte.intValue();
            }
               
        } finally {
            em.close();
        }  

        return quantiteCommandee;
    }
    
    public int getCommandeQuantiteRecu(Commande commande){      
        int quantiteRecu=0;
        EntityManager em = getEntityManager();
        try{
            String sql = "SELECT SUM(ll.quantite) FROM LivraisonLigne ll"
                        +" JOIN ll.livraison l"
                        +" JOIN l.commande c"
                        +" WHERE c = :commande";
            Query q = em.createQuery(sql);
            q.setParameter("commande", commande);
            Long qte = (Long)q.getSingleResult();
            if(qte!=null){
                quantiteRecu = qte.intValue();
            }
               
        } finally {
            em.close();
        }  

        return quantiteRecu;
    }    
    
    public int getCommandeLigneQuantiteCommandee(CommandeLigne commandeLigne){      
        int quantiteCommandee=0;
        EntityManager em = getEntityManager();
        try{
            String sql = "SELECT SUM(cl.quantite) FROM CommandeLigne cl"
                        +" WHERE cl = :commandeLigne";
            Query q = em.createQuery(sql);
            q.setParameter("commandeLigne", commandeLigne);
            Long qte = (Long)q.getSingleResult();
            quantiteCommandee = qte.intValue();
               
        } finally {
            em.close();
        }  

        return quantiteCommandee;
    }
    
    public int getCommandeLigneQuantiteRecu(CommandeLigne commandeLigne){      
        int quantiteRecu=0;
        EntityManager em = getEntityManager();
        try{
            String sql = "SELECT SUM(ll.quantite) FROM LivraisonLigne ll"
                        +" JOIN ll.produitConditionnement pc"
                        +" JOIN ll.livraison l"
                        +" WHERE pc = :produitConditionnement"
                        +" AND ll.enStock = TRUE"
                        +" AND l.commande = :commande";
            Query q = em.createQuery(sql);
            q.setParameter("produitConditionnement", commandeLigne.getProduitConditionnement());
            q.setParameter("commande", commandeLigne.getCommande());
            Long qte = (Long)q.getSingleResult();
            if(qte!=null){
                quantiteRecu = qte.intValue();
            }
               
        } finally {
            em.close();
        }  

        return quantiteRecu;
    }     
    
    
    
    public List<Object[]> getResteALivrer(Commande commande){
          EntityManager em = getEntityManager();
        try {

            String sql = "SELECT cl, cl.quantite-ll.quantite "
                    + "FROM LivraisonLigne ll "
                    + "JOIN ll.commandeLigne cl "
                    + "JOIN cl.commande c "
                    + "WHERE c.idCommande = " + commande.getIdCommande();



            Logger.getLogger(CommandeDAO.class.getName()).log(Level.INFO, sql, sql);
            Query q = em.createQuery(sql);
            //return q.getHints();
            return q.getResultList();
        } finally {
            em.close();
        }
    }    

}
