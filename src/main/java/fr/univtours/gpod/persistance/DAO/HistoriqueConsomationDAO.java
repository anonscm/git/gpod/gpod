/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2012/01/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/

package fr.univtours.gpod.persistance.DAO;

import fr.univtours.gpod.persistance.DAO.exceptions.NonexistentEntityException;
import fr.univtours.gpod.persistance.Manager.PersistenceManager;
import fr.univtours.gpod.persistance.entities.Article;
import fr.univtours.gpod.persistance.entities.ClickeyScan;
import fr.univtours.gpod.persistance.entities.HistoriqueConsomation;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author geoffroy.vibrac
 */
public class HistoriqueConsomationDAO {

    EntityManagerFactory emf= PersistenceManager.getInstance().getEntityManagerFactory();

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(HistoriqueConsomation historiqueconsomation, ClickeyScan cs) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();

            Article article = historiqueconsomation.getArticle();            
            em.merge(article);
            if (cs!=null){
                em.merge(cs);
            }            

            em.persist(historiqueconsomation);
            
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(HistoriqueConsomation historiqueconsomation) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            historiqueconsomation = em.merge(historiqueconsomation);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = historiqueconsomation.getIdHistoriqueConsommation();
                if (findHistoriqueconsomation(id) == null) {
                    throw new NonexistentEntityException("The historiqueconsomation with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            HistoriqueConsomation historiqueconsomation;
            try {
                historiqueconsomation = em.getReference(HistoriqueConsomation.class, id);
                historiqueconsomation.getIdHistoriqueConsommation();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The historiqueconsomation with id " + id + " no longer exists.", enfe);
            }
            em.remove(historiqueconsomation);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<HistoriqueConsomation> findHistoriqueconsomationEntities() {
        return findHistoriqueconsomationEntities(true, -1, -1);
    }

    public List<HistoriqueConsomation> findHistoriqueconsomationEntities(int maxResults, int firstResult) {
        boolean all = false;
        if (maxResults<=0){
            all=true;
        }        
        return findHistoriqueconsomationEntities(all, maxResults, firstResult);
    }

    private List<HistoriqueConsomation> findHistoriqueconsomationEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(HistoriqueConsomation.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }
    

    
    public List<HistoriqueConsomation> getHistoriqueConsomationByArticle(Integer idArticle, Integer idSousStructure){
        EntityManager em = getEntityManager();
        try{
            
          String sql = "SELECT hc FROM HistoriqueConsomation hc "
                  + " JOIN hc.article a "
                  + " JOIN hc.sousStructure ss "
                  + " WHERE 0=0"
                  + " AND a.idArticle = " + idArticle
                  + " AND ss.idSousStructure = " + idSousStructure
                  + " ORDER BY hc.dateConsommation desc";
          Query q = em.createQuery(sql);

            
            return q.getResultList();
        } finally {
            em.close();
        }   
    }
    

    public HistoriqueConsomation findHistoriqueconsomation(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(HistoriqueConsomation.class, id);
        } finally {
            em.close();
        }
    }

    public int getHistoriqueconsomationCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<HistoriqueConsomation> rt = cq.from(HistoriqueConsomation.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
