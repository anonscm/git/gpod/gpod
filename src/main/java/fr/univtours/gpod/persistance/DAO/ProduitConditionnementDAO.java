/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2012/01/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/

package fr.univtours.gpod.persistance.DAO;

import fr.univtours.gpod.persistance.DAO.exceptions.NonexistentEntityException;
import fr.univtours.gpod.persistance.Manager.PersistenceManager;
import fr.univtours.gpod.persistance.entities.ProduitConditionnement;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import fr.univtours.gpod.persistance.entities.Produit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;

/**
 *
 * @author geoffroy.vibrac
 */
public class ProduitConditionnementDAO {

    EntityManagerFactory emf= PersistenceManager.getInstance().getEntityManagerFactory();

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(ProduitConditionnement produitConditionnement) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Produit produit = produitConditionnement.getProduit();
            if (produit != null) {
                produit = em.getReference(produit.getClass(), produit.getIdProduit());
                produitConditionnement.setProduit(produit);
            }
            em.persist(produitConditionnement);
            if (produit != null) {
                produit.getConditionnements().add(produitConditionnement);
                produit = em.merge(produit);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(ProduitConditionnement produitConditionnement) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            ProduitConditionnement persistentProduitConditionnement = em.find(ProduitConditionnement.class, produitConditionnement.getIdProduitConditionnement());
            Produit produitOld = persistentProduitConditionnement.getProduit();
            Produit produitNew = produitConditionnement.getProduit();
            if (produitNew != null) {
                produitNew = em.getReference(produitNew.getClass(), produitNew.getIdProduit());
                produitConditionnement.setProduit(produitNew);
            }
            produitConditionnement = em.merge(produitConditionnement);
            if (produitOld != null && !produitOld.equals(produitNew)) {
                produitOld.getConditionnements().remove(produitConditionnement);
                produitOld = em.merge(produitOld);
            }
            if (produitNew != null && !produitNew.equals(produitOld)) {
                produitNew.getConditionnements().add(produitConditionnement);
                produitNew = em.merge(produitNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = produitConditionnement.getIdProduitConditionnement();
                if (findProduitConditionnement(id) == null) {
                    throw new NonexistentEntityException("The produitConditionnement with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            ProduitConditionnement produitConditionnement;
            try {
                produitConditionnement = em.getReference(ProduitConditionnement.class, id);
                produitConditionnement.getIdProduitConditionnement();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The produitConditionnement with id " + id + " no longer exists.", enfe);
            }
            Produit produit = produitConditionnement.getProduit();
            if (produit != null) {
                produit.getConditionnements().remove(produitConditionnement);
                produit = em.merge(produit);
            }
            em.remove(produitConditionnement);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<ProduitConditionnement> findProduitConditionnementEntities() {
        return findProduitConditionnementEntities(true, -1, -1);
    }

    public List<ProduitConditionnement> findProduitConditionnementEntities(int maxResults, int firstResult) {
        boolean all = false;
        if (maxResults<=0){
            all=true;
        }
        return findProduitConditionnementEntities(all, maxResults, firstResult);
    }

    private List<ProduitConditionnement> findProduitConditionnementEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(ProduitConditionnement.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public ProduitConditionnement findProduitConditionnement(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(ProduitConditionnement.class, id);
        } finally {
            em.close();
        }
    }


    public List<ProduitConditionnement> findProduitConditionnementByProduit(Produit produit) {
        EntityManager em = getEntityManager();
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery cq = cb.createQuery();
            Root<ProduitConditionnement> produitConditionnement = cq.from(ProduitConditionnement.class);
            cq.select(produitConditionnement);
            Predicate pre = null;
            if (produit!= null){
                pre =  cb.equal(produitConditionnement.get("produit"), produit);
            }

            if (pre != null){
                pre = cb.and(pre, cb.equal(produitConditionnement.get("deleted"), new Integer(0)));
            }


            cq.where(pre);
            cq.orderBy(cb.asc(produitConditionnement.get("idProduitConditionnement")));

            Query q = em.createQuery(cq);
            return q.getResultList();
        } finally {
            em.close();
        }
    }


    public int getProduitConditionnementCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<ProduitConditionnement> rt = cq.from(ProduitConditionnement.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    public List<ProduitConditionnement> findProduitConditionnementEntities(int idProduit, String referenceFournisseur, double contenance, int idUniteMesure, int idContenant, int deleted){


        List<ProduitConditionnement> listProduitCs = new ArrayList<ProduitConditionnement>();
        EntityManager em = getEntityManager();
        String conditions = "";

        String sql  = "SELECT pc FROM ProduitConditionnement pc "
                + "JOIN pc.produit p "
                + "JOIN pc.contenant s "
                + "JOIN pc.uniteMesure um "
                + "WHERE 0=0 AND pc.deleted = " + deleted + " ";
        if (idProduit > 0){
            sql+=" AND p.idProduit = " + idProduit;
        }
        if (referenceFournisseur!=null && !"".equals(referenceFournisseur)){
            sql+=" AND pc.referenceFournisseur LIKE '%"+referenceFournisseur+"%'";
        }
        if (contenance > 0){
            sql+=" AND pc.contenance = " + contenance;
        }
        if (idUniteMesure > 0){
            sql+=" AND um.idUniteMesure = " + idUniteMesure;
        }
        if (idContenant > 0){
            sql+=" AND s.idContenant = " + idContenant;
        }


        Query q = em.createQuery(sql);
        listProduitCs = q.getResultList();

        return listProduitCs;

    }

}
