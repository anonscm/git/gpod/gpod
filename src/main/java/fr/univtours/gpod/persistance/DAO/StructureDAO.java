/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2012/01/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/

package fr.univtours.gpod.persistance.DAO;

import fr.univtours.gpod.exceptions.IllegalOrphanException;
import fr.univtours.gpod.exceptions.NonexistentEntityException;
import fr.univtours.gpod.persistance.Manager.PersistenceManager;
import fr.univtours.gpod.persistance.entities.SousStructure;
import fr.univtours.gpod.persistance.entities.Structure;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Root;

/**
 *
 * @author geoffroy.vibrac
 */
public class StructureDAO  implements Serializable {

    EntityManagerFactory emf = PersistenceManager.getInstance().getEntityManagerFactory();
 
    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }    
    
    public void create(Structure structure) {
        if (structure.getSousStructure() == null) {
            structure.setSousStructure(new ArrayList<SousStructure>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<SousStructure> attachedSousStructures = new ArrayList<SousStructure>();
            for (SousStructure sousStructuresSousStructureToAttach : structure.getSousStructure()) {
                sousStructuresSousStructureToAttach = em.getReference(sousStructuresSousStructureToAttach.getClass(), sousStructuresSousStructureToAttach.getIdSousStructure());
                attachedSousStructures.add(sousStructuresSousStructureToAttach);
            }
            structure.setSousStructure(attachedSousStructures);
            em.persist(structure);
            for (SousStructure sousStructuresSousStructure : structure.getSousStructure()) {
                Structure oldStructureOfSousStructuresSousStructure = sousStructuresSousStructure.getStructure();
                sousStructuresSousStructure.setStructure(structure);
                sousStructuresSousStructure = em.merge(sousStructuresSousStructure);
                if (oldStructureOfSousStructuresSousStructure != null) {
                    oldStructureOfSousStructuresSousStructure.getSousStructure().remove(sousStructuresSousStructure);
                    oldStructureOfSousStructuresSousStructure = em.merge(oldStructureOfSousStructuresSousStructure);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Structure structure) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Structure persistentStructure = em.find(Structure.class, structure.getIdStructure());
            List<SousStructure> sousStructuresOld = persistentStructure.getSousStructure();
            List<SousStructure> sousStructuresNew = structure.getSousStructure();
            List<String> illegalOrphanMessages = null;
            for (SousStructure sousStructuresOldSousStructure : sousStructuresOld) {
                if (!sousStructuresNew.contains(sousStructuresOldSousStructure)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain SousStructure " + sousStructuresOldSousStructure + " since its structure field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<SousStructure> attachedSousStructuresNew = new ArrayList<SousStructure>();
            for (SousStructure sousStructuresNewsousStructureToAttach : sousStructuresNew) {
                sousStructuresNewsousStructureToAttach = em.getReference(sousStructuresNewsousStructureToAttach.getClass(), sousStructuresNewsousStructureToAttach.getIdSousStructure());
                attachedSousStructuresNew.add(sousStructuresNewsousStructureToAttach);
            }
            sousStructuresNew = attachedSousStructuresNew;
            structure.setSousStructure(sousStructuresNew);
            structure = em.merge(structure);
            for (SousStructure sousStructuresNewSousStructure : sousStructuresNew) {
                if (!sousStructuresOld.contains(sousStructuresNewSousStructure)) {
                    Structure oldStructureOfSousStructuresNewSousStructure = sousStructuresNewSousStructure.getStructure();
                    sousStructuresNewSousStructure.setStructure(structure);
                    sousStructuresNewSousStructure = em.merge(sousStructuresNewSousStructure);
                    if (oldStructureOfSousStructuresNewSousStructure != null && !oldStructureOfSousStructuresNewSousStructure.equals(structure)) {
                        oldStructureOfSousStructuresNewSousStructure.getSousStructure().remove(sousStructuresNewSousStructure);
                        oldStructureOfSousStructuresNewSousStructure = em.merge(oldStructureOfSousStructuresNewSousStructure);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = structure.getIdStructure();
                if (findStructure(id) == null) {
                    throw new NonexistentEntityException("The structure with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Structure structure;
            try {
                structure = em.getReference(Structure.class, id);
                structure.getIdStructure();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The structure with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<SousStructure> sousStructuresOrphanCheck = structure.getSousStructure();
            for (SousStructure sousStructuresOrphanCheckSousStructure : sousStructuresOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This structure (" + structure + ") cannot be destroyed since the SousStructure " + sousStructuresOrphanCheckSousStructure + " in its sousStructures field has a non-nullable structure field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(structure);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }
    
   public List<Structure> findStructureEntities(String searchValue, String searchColumns, int maxResults, int firstResult, String type){
        if (searchValue == null || searchColumns==null || "".equals(searchValue) || "".equals(searchColumns)){
            return findStructureEntities(maxResults, firstResult, type);
        }

        String sql = returnSqlQuery("l", searchValue, searchColumns, type);
        
        EntityManager em = getEntityManager();
        Query q = em.createQuery(sql);
        q.setMaxResults(maxResults);
        q.setFirstResult(firstResult);
        return q.getResultList();    
        
    }
   
   
      public int getStructureCount(String searchValue, String searchColumns, String type){
        if (searchValue == null || searchColumns==null || "".equals(searchValue) || "".equals(searchColumns)){
            return getStructureCount(type);
        }

        String sql = returnSqlQuery("count(l)", searchValue, searchColumns, type);
        
        EntityManager em = getEntityManager();
        Query q = em.createQuery(sql);
        return ((Long) q.getSingleResult()).intValue();
        
    }
   
   
   private String returnSqlQuery(String select, String searchValue, String searchColumns, String type){
        // Insertion dans une liste des colonnes de recherche
        searchColumns = searchColumns.replace("[", "");
        searchColumns = searchColumns.replace("\"", "");
        searchColumns = searchColumns.replace("]", "");
        List<String> columns = Arrays.asList(searchColumns.split(","));
        String conditions = "";
        String sql = "SELECT "+select+" FROM Structure l WHERE 0=0";
        for (String c : columns){
            conditions+=" OR l." + c + " LIKE '%"+searchValue+"%'";
        }
        if (conditions.length() > 4){
            conditions = conditions.substring(4);
            sql+=" AND ("+conditions+")";
        }

        if (type != null){
            sql+=" AND l.type='"+ type +"'";
        }

        sql+= " ORDER BY e.type, e.nom";
        
        return sql;
   }


    public List<Structure> findStructureEntities() {
        return findStructureEntities(true, -1, -1, null);
    }

    public List<Structure> findStructureEntities(int maxResults, int firstResult, String type) {
        boolean all = false;
        if (maxResults<=0){
            all=true;
        }
        return findStructureEntities(all, maxResults, firstResult, type);
    }

    private List<Structure> findStructureEntities(boolean all, int maxResults, int firstResult) {
        return findStructureEntities(all, maxResults, firstResult, null);
    }



    private List<Structure> findStructureEntities(boolean all, int maxResults, int firstResult, String type) {
        EntityManager em = getEntityManager();
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery cq = cb.createQuery();
            Root<Structure> structure = cq.from(Structure.class);
            cq.select(structure);
            if (type!= null){
                cq.where(cb.equal(structure.get("type"), type));
            }
            cq.orderBy(cb.asc(structure.get("type")), cb.asc(structure.get("nom")));
            
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Structure findStructure(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Structure.class, id);
        } finally {
            em.close();
        }
    }

    public int getStructureCount(String type) {
        EntityManager em = getEntityManager();
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Structure> rt = cq.from(Structure.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            cq.where(cb.equal(rt.get("type"), type));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
    

    
}
