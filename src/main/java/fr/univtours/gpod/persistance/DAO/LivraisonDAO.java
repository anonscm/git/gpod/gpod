/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2012/01/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/

package fr.univtours.gpod.persistance.DAO;

import fr.univtours.gpod.exceptions.IllegalOrphanException;
import fr.univtours.gpod.exceptions.RollbackFailureException;
import fr.univtours.gpod.persistance.DAO.exceptions.NonexistentEntityException;
import fr.univtours.gpod.persistance.Manager.PersistenceManager;
import fr.univtours.gpod.persistance.entities.Article;
import fr.univtours.gpod.persistance.entities.Livraison;
import java.util.List;
import javax.persistence.*;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import fr.univtours.gpod.persistance.entities.Commande;
import fr.univtours.gpod.persistance.entities.LivraisonLigne;
import fr.univtours.gpod.persistance.entities.SousStructure;
import fr.univtours.gpod.utils.Utilities;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.transaction.Transaction;

/**
 *
 * @author geoffroy.vibrac
 */
public class LivraisonDAO {

    EntityManagerFactory emf= PersistenceManager.getInstance().getEntityManagerFactory();

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

     public void create(Livraison livraison) throws RollbackFailureException, Exception {
        EntityManager em = null;
         EntityTransaction tx = null;
        try {
            em = getEntityManager();
            tx =   em.getTransaction();
            tx.begin();
            Commande commande = livraison.getCommande();
            if (commande != null) {
                commande = em.getReference(commande.getClass(), commande.getIdCommande());
                livraison.setCommande(commande);
            }
            List<Article> articleList = livraison.getArticleList();
            List<LivraisonLigne> livraisonLigneList = livraison.getLivraisonLigneList();
            livraison.setArticleList(null);
            livraison.setLivraisonLigneList(null);
            em.persist(livraison);
            em.flush();
            if (articleList!= null){
                for (Article a : articleList){
                    a.setLivraison(livraison);
                    em.persist(a);
                }
            }
            for (LivraisonLigne ll : livraisonLigneList){
                ll.setLivraison(livraison);
                em.persist(ll);
            }
           
            livraison.setArticleList(articleList);
            livraison.setLivraisonLigneList(livraisonLigneList);
            livraison = em.merge(livraison);
            
            if (commande != null) {
                commande.getLivraisonList().add(livraison);
                commande = em.merge(commande);
            }


            tx.commit();
        } catch (Exception ex) {
            try {
                tx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();                
            }
        }
    }

    public void edit(Livraison livraison) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();  
            livraison = em.merge(livraison);
            em.getTransaction().commit();
        } catch (Exception ex) {
            try {
                em.getTransaction().rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = livraison.getIdLivraison();
                if (findLivraison(id) == null) {
                    throw new NonexistentEntityException("The livraison with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();  
            Livraison livraison;
            try {
                livraison = em.getReference(Livraison.class, id);
                livraison.getIdLivraison();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The livraison with id " + id + " no longer exists.", enfe);
            }
            Commande commande = livraison.getCommande();
            if (commande != null) {
                commande.getLivraisonList().remove(livraison);
                commande = em.merge(commande);
            }
            em.remove(livraison);
            em.getTransaction().commit();
        } catch (Exception ex) {
            try {
                em.getTransaction().rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    /////////////////////////////////////////////////////////////////////////////////
    // Ces méthodes sont utilisées pour afficher les entrée directe dans le stock = livraison sans commande
    // <editor-fold defaultstate="expanded" desc="livraison sans commande">

    public List<Livraison> findLivraisonSansCommandesEntities(int maxResults, int firstResult, int periodInMonth) {
        boolean all = false;
        if (maxResults<=0){
            all=true;
        }
        return findLivraisonSansCommandesEntities(all, maxResults, firstResult, periodInMonth);
    }

    
    // Livraison sans commande (=> entrées directes) => UTILISE PAR LE PROFIL ADMIN
    public List<Livraison> findLivraisonSansCommandesEntities(boolean all, int maxResults, int firstResult, int periodInMonth){

        EntityManager em = getEntityManager();
        Query query;
        String sql = "SELECT l FROM Livraison l WHERE l.commande IS NULL";
        
        if (periodInMonth>0){
           Calendar calendar = Calendar.getInstance();
           calendar.add(Calendar.MONTH, -1*periodInMonth);
           Date dateMin = calendar.getTime();   
           sql +=" AND l.dateLivraison >= :dateMinimum";
           query = em.createQuery(sql).setParameter("dateMinimum", dateMin, TemporalType.DATE);
        }else{
            query = em.createQuery(sql);
        }
        if (!all) {
            query.setMaxResults(maxResults);
            query.setFirstResult(firstResult);
        }
        return query.getResultList();

    }

    // Livraison sans commande PAR SOUs structure (=> entrées directes) => UTILISE PAR AUTRES PROFIL que ADMIN
     public List<Livraison> findLivraisonSansCommandesEntitiesBySousStructure(List<SousStructure> sousStructure, int maxResults, int firstResult, int periodInMonth){
        boolean all = false;
        if (maxResults<=0){
            all=true;
        }
        return findLivraisonSansCommandesEntitiesBySousStructure(sousStructure, all, maxResults, firstResult, periodInMonth);
    }

    public List<Livraison> findLivraisonSansCommandesEntitiesBySousStructure(List<SousStructure> sousStructures, boolean all, int maxResults, int firstResult, int periodInMonth){

        String sql = returnSqlQuery("l", sousStructures, periodInMonth);    
        EntityManager em = getEntityManager();
        
        Query query=null;
        
        if (periodInMonth>0){
           Calendar calendar = Calendar.getInstance();
           calendar.add(Calendar.MONTH, -1*periodInMonth);
           Date dateMin = calendar.getTime();   
           query = em.createQuery(sql).setParameter("dateMinimum", dateMin, TemporalType.DATE);
        }else{
           query = em.createQuery(sql);
        }           
        
        if (!all) {
            query.setMaxResults(maxResults);
            query.setFirstResult(firstResult);
        }
        return query.getResultList();  

    }
    //</editor-fold>
    /////////////////////////////////////////////////////////////////////////////////


    public List<Livraison> findLivraisonEntities() {
        return findLivraisonEntities(true, -1, -1);
    }

    public List<Livraison> findLivraisonEntities(int maxResults, int firstResult) {
        boolean all = false;
        if (maxResults<=0){
            all=true;
        }
        return findLivraisonEntities(all, maxResults, firstResult);
    }

    private List<Livraison> findLivraisonEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Livraison.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }


//    public List<Livraison> findLivraisonEntitiesByUser(Utilisateur utilisateur, int maxResults, int firstResult){
//        boolean all = false;
//        if (maxResults<=0){
//            all=true;
//        }
//        return findLivraisonEntitiesByUser(utilisateur, all, maxResults, firstResult);
//    }
   

//    public List<Livraison> findLivraisonEntitiesByUser(Utilisateur utilisateur, boolean all, int maxResults, int firstResult){
//
//        EntityManager em = getEntityManager();
//        Query query = em.createNamedQuery("Livraison.findByIdUtilisateur");
//        query.setParameter("utilisateur",utilisateur);
//        if (!all) {
//            query.setMaxResults(maxResults);
//            query.setFirstResult(firstResult);
//        }
//        return query.getResultList();
//
//    }


    public Livraison findLivraison(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Livraison.class, id);
        } finally {
            em.close();
        }
    }

    public int getLivraisonCount(int periodInMonth) {
        EntityManager em = getEntityManager();
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery cq = cb.createQuery();
            Root<Livraison> livraison = cq.from(Livraison.class);
            
            Predicate pre = null;
            
            cq.select(em.getCriteriaBuilder().count(livraison));
            
            if (periodInMonth>0){
                   Calendar calendar = Calendar.getInstance();
                   calendar.add(Calendar.MONTH, -1*periodInMonth);
                   Date dateMin = calendar.getTime();
                   pre = cb.greaterThan(livraison.get("dateLivraison").as(Date.class), dateMin);   
                   cq.where(pre);
            }           
            
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
       
    
    public int getLivraisonSansCommandesCount(int periodInMonth) {
        EntityManager em = getEntityManager();
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery cq = cb.createQuery();
            Root<Livraison> livraison = cq.from(Livraison.class);
            
            Predicate pre = null;
            
            cq.select(em.getCriteriaBuilder().count(livraison));
            
            if (periodInMonth>0){
                   Calendar calendar = Calendar.getInstance();
                   calendar.add(Calendar.MONTH, -1*periodInMonth);
                   Date dateMin = calendar.getTime();
                   pre = cb.greaterThan(livraison.get("dateLivraison").as(Date.class), dateMin);   
                   pre = cb.and(pre, cb.isNull(livraison.get("commande")));
                   cq.where(pre);
            }           
            
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }    
    
    public int getLivraisonSansCommandesEntitiesBySousStructureCount(List<SousStructure> sousStructures, int periodInMonth){

        String sql = returnSqlQuery("count(l)", sousStructures, periodInMonth);        
        EntityManager em = getEntityManager();
        Query query=null;
        
        if (periodInMonth>0){
           Calendar calendar = Calendar.getInstance();
           calendar.add(Calendar.MONTH, -1*periodInMonth);
           Date dateMin = calendar.getTime();   
           query = em.createQuery(sql).setParameter("dateMinimum", dateMin, TemporalType.DATE);
        }else{
           query = em.createQuery(sql);
        } 
        return ((Long) query.getSingleResult()).intValue();     

    }    
 
   private String returnSqlQuery(String typeSelect, List<SousStructure> sousStructures, int periodInMonth){
       
       //SELECT * FROM `livraison` 
       
            String sql = null;
            sql = "SELECT "+typeSelect+" FROM Livraison l";
            sql += " WHERE l.commande IS NULL AND l.idLivraison IN (";
            sql += " SELECT li.idLivraison from Article a JOIN a.livraison li JOIN a.sousStructure s WHERE s.idSousStructure IN ("+SousStructure.getIdSousStructureFromListSousStructure(sousStructures) +"))";
            
            if (periodInMonth>0){        
               sql +=" AND l.dateLivraison >= :dateMinimum" ;                
            }
                        
            sql += " ORDER BY l.dateLivraison";
            
            return sql;
        
    }
    
    
}
