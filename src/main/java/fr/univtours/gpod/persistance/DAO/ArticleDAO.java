/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2012/01/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/

package fr.univtours.gpod.persistance.DAO;

import fr.univtours.gpod.persistance.DAO.exceptions.NonexistentEntityException;
import fr.univtours.gpod.persistance.Manager.PersistenceManager;
import fr.univtours.gpod.persistance.entities.*;
import fr.univtours.gpod.utils.Utilities;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.*;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;


/**
 *
 * @author geoffroy.vibrac
 */
public class ArticleDAO {

    EntityManagerFactory emf = PersistenceManager.getInstance().getEntityManagerFactory();

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Article article) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(article);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Article article) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            article = em.merge(article);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = article.getIdArticle();
                if (findArticle(id) == null) {
                    throw new NonexistentEntityException("The article with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Article article;
            try {
                article = em.getReference(Article.class, id);
                article.getIdArticle();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The article with id " + id + " no longer exists.", enfe);
            }
            em.remove(article);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Article> findArticleEntities() {
        return findArticleEntities(true, -1, -1);
    }

    public List<Article> findArticleEntities(int maxResults, int firstResult) {
        boolean all = false;
        if (maxResults<=0){
            all=true;
        }
        return findArticleEntities(all, maxResults, firstResult);
    }

    private List<Article> findArticleEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Article.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public List<Object[]> findStockArticleWithQuery(int maxResults, int firstResult, Site site, Batiment batiment, SalleStockage salleStockage, AireStockage aire,
                                                    ProduitConditionnement produitConditionnement, String nomSigne, Float valeurStock, SousStructure sousStructure,
                                                    List<Pictogramme> pictogrammes, boolean afficheDetailArticle, String etiquette,
                                                    Integer idArticle, String order, List<SousStructure> sousStructureList) {

        return findStockArticleWithQuery(maxResults, firstResult, site, batiment, salleStockage, aire, produitConditionnement, nomSigne, valeurStock,
                sousStructure, pictogrammes, afficheDetailArticle, etiquette, idArticle, order, sousStructureList, "", "");


    }

    public List<Object[]> findStockArticleWithQuery(int maxResults, int firstResult, Site site, Batiment batiment, SalleStockage salleStockage, AireStockage aire,
            ProduitConditionnement produitConditionnement, String nomSigne, Float valeurStock, SousStructure sousStructure,
            List<Pictogramme> pictogrammes, boolean afficheDetailArticle, String etiquette, Integer idArticle, String order,
            List<SousStructure> sousStructureList, String cas, String autre) {
        boolean all = false;
        if (maxResults<=0){
            all=true;
        }
        return findStockArticleWithQuery(all, maxResults, firstResult, site, batiment, salleStockage, aire, produitConditionnement, nomSigne, valeurStock,
                        sousStructure, pictogrammes, afficheDetailArticle, etiquette, idArticle, order, sousStructureList, cas, autre);
    }

    public Article findArticleFromIdentifiantEtiquette(String identifiantEtiquette){
        // on complete avec des zero jusqu'a 24 caractères
        identifiantEtiquette = Utilities.completeRightZero(identifiantEtiquette,24);
        EntityManager em = getEntityManager();
        Query query = em.createNamedQuery("Article.findByIdentifiantEtiquette");
        query.setParameter("identifiantEtiquette",identifiantEtiquette);
        Article a = null;
        try{
            a = (Article)query.getSingleResult();
        }catch(NoResultException e){            
        }catch(javax.persistence.NonUniqueResultException e){
        }

        return a;
    }
    
  

    public List<Article> findArticleEntitiesParCommande(int idCommande){
        EntityManager em = getEntityManager();
        try{
            
          String sql = "select a FROM Article a JOIN a.livraison l JOIN l.commande c WHERE c.idCommande=" + idCommande;
          Query q = em.createQuery(sql);

            
            return q.getResultList();
        } finally {
            em.close();
        }        
        
    }    
    
    public List<Object[]> findStockArticleFromClickey(Utilisateur user){
          String debutSelect = "SELECT a.idArticle, a.produitConditionnement, a.aireStockage, a.sousStructure, "
                  + "a.quantite, a.identifiantEtiquette, a.dateOuverture, cs.idClickeyScan ";
          
          EntityManager em = getEntityManager();
          
          String sql = debutSelect
                    + " FROM ClickeyScan cs "
                    + " JOIN cs.article a "
                    + " JOIN cs.utilisateur u "
                    + " WHERE cs.dejaTraite=FALSE AND u.idUtilisateur="+user.getIdUtilisateur();
          
          
            Logger.getLogger(ArticleDAO.class.getName()).log(Level.INFO, sql, sql);
            Query q = em.createQuery(sql);
            return q.getResultList();
          
          
    }
    

    private List<Object[]> findStockArticleWithQuery(boolean all, int maxResults, int firstResult, Site site, Batiment batiment, SalleStockage salleStockage,AireStockage aire,
            ProduitConditionnement produitConditionnement, String nomSigne, Float valeurStocks, SousStructure sousStructure,
            List<Pictogramme> pictogrammes, boolean afficheDetailArticle, String etiquette, Integer idArticle,
            String order, List<SousStructure> sousStructureList, String cas, String autre){
          EntityManager em = getEntityManager();
        try {

            String debutSelect = "";
            String groupBy="";

            // si le critère étiquette est saisi, on ne retourne que l'article concerné
            if ((etiquette != null && !"".equals(etiquette)) || afficheDetailArticle){
                debutSelect = "SELECT a.idArticle, a.produitConditionnement, a.aireStockage, a.sousStructure, sum(a.quantite), a.identifiantEtiquette, a.dateOuverture ";
                groupBy=" GROUP BY a.idArticle, a.produitConditionnement, a.aireStockage, a.sousStructure, a.identifiantEtiquette, a.dateOuverture";
            }else{
   //         if (regroupeArticle == true){
                debutSelect = "SELECT 0, a.produitConditionnement, a.aireStockage, a.sousStructure, sum(a.quantite) ";
                groupBy = " GROUP BY a.produitConditionnement, a.aireStockage, a.sousStructure";
 //            }
            }
            String sql = debutSelect
                    + "FROM Article a "
                    + "JOIN a.produitConditionnement pc "
                    + "JOIN pc.produit p "
                    + "JOIN p.substance s "
                    + "JOIN a.aireStockage ast "
                    + "JOIN ast.salleStockage sst "
                    + "JOIN sst.batiment b "
                    + "JOIN a.sousStructure ss "
                    + "WHERE a.idArticle > 0";

            // si sousStructureList est renseigné, on restreint la recherche aux soustrusture de l'utilisateur
            if (sousStructureList!=null && sousStructureList.size()>0){
                     sql += " AND ss.idSousStructure IN ( " +  SousStructure.getIdSousStructureFromListSousStructure(sousStructureList) + ")";              
            // Si jamais il n'y a aucune sous structure, on rend la requete infructueuse
            }else{
                     sql += " AND ss.idSousStructure = -1";
            }
            
            // si une aire est renseigné, on retourne le stock de l'air
            if (aire != null){
                sql += " AND ast.idAire = " + aire.getIdAire();
            }else{
                 // si une salle est renseignés (mais pas une aire car l'aire à la priorité) on returne le stock de la salle
                if (salleStockage != null){
                    sql += " AND ast.idAire IN (" + AireStockage.getIdAireFromListAire(salleStockage.getAires()) + ")";                  
                }else{
                    // si un batiment est renseigné (mais pas une aire ou une salle car l'aire à la priorité) on returne le stock du batiment
                    if (batiment != null){
                        sql += " AND ast.idAire IN (" + AireStockage.getIdAireFromListAire(batiment.getAires()) + ")";
                    }else{
                        // si un site est renseigné (mais pas une aire ou un batiment car l'aire à la priorité) on retourne le stock du site
                        if (site != null){
                            sql += " AND ast.idAire IN (" + AireStockage.getIdAireFromListAire(site.getAires()) + ")";
                        }
                    }
                }
            }

            if (idArticle!=null && idArticle > 0){
                sql += " AND a.idArticle = " +  idArticle;
            }

            if (cas != null && !"".equals(cas)){
                sql += " AND s.cas = '" +  cas + "'";
            }

            if (autre != null && !"".equals(autre)){
                sql+=" AND (p.nom LIKE '%"+Utilities.addQuote(autre)+"%' OR p.description LIKE '%"+ Utilities.addQuote(autre)+"%')";
            }


            if (produitConditionnement != null){
                sql += " AND pc.idProduitConditionnement = " +  produitConditionnement.getIdProduitConditionnement();
            }

            if (etiquette != null && !"".equals(etiquette)){
                if (etiquette.contains("*") || etiquette.contains("%")){
                    etiquette = etiquette.replace("*", "%");
                    sql += " AND a.identifiantEtiquette LIKE '" +  etiquette +"'";
                }else{
                    etiquette = Utilities.completeRightZero(etiquette,24);
                    sql += " AND a.identifiantEtiquette = '" +  etiquette +"'";
                }
            }

            if (sousStructure != null){
                sql += " AND ss.idSousStructure = " +  sousStructure.getIdSousStructure();
            }

            if (pictogrammes !=null && pictogrammes.size()>0){
             sql += " AND p.idProduit IN ( SELECT prod.idProduit FROM Produit prod "
                     + "JOIN prod.pictogrammes pi WHERE pi.idPictogramme IN (" +  Pictogramme.ListToStringIdWithComma(pictogrammes) + "))";
            }


            sql +=groupBy;


            if (valeurStocks !=null && nomSigne!=null && !"".equals(nomSigne)){
                sql += " HAVING sum(a.quantite) " + nomSigne + " " + valeurStocks;
            }
            
            if (order==null || "".equals(order)){
                sql +=" ORDER BY s.cas ";
            }else{
                sql += order;
            }
            
           
            Logger.getLogger(ArticleDAO.class.getName()).log(Level.INFO, sql, sql);
            Query q = em.createQuery(sql);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            //return q.getHints();
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Article findArticle(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Article.class, id);
        } finally {
            em.close();
        }
    }
    

    
    public int getArticleCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Article> rt = cq.from(Article.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
}


