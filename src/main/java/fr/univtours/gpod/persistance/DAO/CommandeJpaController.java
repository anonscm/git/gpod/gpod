/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2012/01/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/

package fr.univtours.gpod.persistance.DAO;

import fr.univtours.gpod.persistance.DAO.exceptions.IllegalOrphanException;
import fr.univtours.gpod.persistance.DAO.exceptions.NonexistentEntityException;
import fr.univtours.gpod.persistance.DAO.exceptions.RollbackFailureException;
import fr.univtours.gpod.persistance.entities.Commande;
import java.io.Serializable;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import fr.univtours.gpod.persistance.entities.Livraison;
import java.util.ArrayList;
import java.util.List;
import fr.univtours.gpod.persistance.entities.CommandeLigne;
import javax.transaction.UserTransaction;

/**
 *
 * @author geoffroy.vibrac
 */
public class CommandeJpaController implements Serializable {

    public CommandeJpaController(UserTransaction utx, EntityManagerFactory emf) {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Commande commande) throws RollbackFailureException, Exception {
        if (commande.getLivraisonList() == null) {
            commande.setLivraisonList(new ArrayList<Livraison>());
        }
        if (commande.getCommandeLigneList() == null) {
            commande.setCommandeLigneList(new ArrayList<CommandeLigne>());
        }
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            List<Livraison> attachedLivraisonList = new ArrayList<Livraison>();
            for (Livraison livraisonListLivraisonToAttach : commande.getLivraisonList()) {
                livraisonListLivraisonToAttach = em.getReference(livraisonListLivraisonToAttach.getClass(), livraisonListLivraisonToAttach.getIdLivraison());
                attachedLivraisonList.add(livraisonListLivraisonToAttach);
            }
            commande.setLivraisonList(attachedLivraisonList);
            List<CommandeLigne> attachedCommandeLigneList = new ArrayList<CommandeLigne>();
            for (CommandeLigne commandeLigneListCommandeLigneToAttach : commande.getCommandeLigneList()) {
                commandeLigneListCommandeLigneToAttach = em.getReference(commandeLigneListCommandeLigneToAttach.getClass(), commandeLigneListCommandeLigneToAttach.getIdCommandeLigne());
                attachedCommandeLigneList.add(commandeLigneListCommandeLigneToAttach);
            }
            commande.setCommandeLigneList(attachedCommandeLigneList);
            em.persist(commande);
            for (Livraison livraisonListLivraison : commande.getLivraisonList()) {
                Commande oldCommandeOfLivraisonListLivraison = livraisonListLivraison.getCommande();
                livraisonListLivraison.setCommande(commande);
                livraisonListLivraison = em.merge(livraisonListLivraison);
                if (oldCommandeOfLivraisonListLivraison != null) {
                    oldCommandeOfLivraisonListLivraison.getLivraisonList().remove(livraisonListLivraison);
                    oldCommandeOfLivraisonListLivraison = em.merge(oldCommandeOfLivraisonListLivraison);
                }
            }
            for (CommandeLigne commandeLigneListCommandeLigne : commande.getCommandeLigneList()) {
                Commande oldCommandeOfCommandeLigneListCommandeLigne = commandeLigneListCommandeLigne.getCommande();
                commandeLigneListCommandeLigne.setCommande(commande);
                commandeLigneListCommandeLigne = em.merge(commandeLigneListCommandeLigne);
                if (oldCommandeOfCommandeLigneListCommandeLigne != null) {
                    oldCommandeOfCommandeLigneListCommandeLigne.getCommandeLigneList().remove(commandeLigneListCommandeLigne);
                    oldCommandeOfCommandeLigneListCommandeLigne = em.merge(oldCommandeOfCommandeLigneListCommandeLigne);
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Commande commande) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Commande persistentCommande = em.find(Commande.class, commande.getIdCommande());
            List<Livraison> livraisonListOld = persistentCommande.getLivraisonList();
            List<Livraison> livraisonListNew = commande.getLivraisonList();
            List<CommandeLigne> commandeLigneListOld = persistentCommande.getCommandeLigneList();
            List<CommandeLigne> commandeLigneListNew = commande.getCommandeLigneList();
            List<String> illegalOrphanMessages = null;
            for (Livraison livraisonListOldLivraison : livraisonListOld) {
                if (!livraisonListNew.contains(livraisonListOldLivraison)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Livraison " + livraisonListOldLivraison + " since its commande field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<Livraison> attachedLivraisonListNew = new ArrayList<Livraison>();
            for (Livraison livraisonListNewLivraisonToAttach : livraisonListNew) {
                livraisonListNewLivraisonToAttach = em.getReference(livraisonListNewLivraisonToAttach.getClass(), livraisonListNewLivraisonToAttach.getIdLivraison());
                attachedLivraisonListNew.add(livraisonListNewLivraisonToAttach);
            }
            livraisonListNew = attachedLivraisonListNew;
            commande.setLivraisonList(livraisonListNew);
            List<CommandeLigne> attachedCommandeLigneListNew = new ArrayList<CommandeLigne>();
            for (CommandeLigne commandeLigneListNewCommandeLigneToAttach : commandeLigneListNew) {
                commandeLigneListNewCommandeLigneToAttach = em.getReference(commandeLigneListNewCommandeLigneToAttach.getClass(), commandeLigneListNewCommandeLigneToAttach.getIdCommandeLigne());
                attachedCommandeLigneListNew.add(commandeLigneListNewCommandeLigneToAttach);
            }
            commandeLigneListNew = attachedCommandeLigneListNew;
            commande.setCommandeLigneList(commandeLigneListNew);
            commande = em.merge(commande);
            for (Livraison livraisonListNewLivraison : livraisonListNew) {
                if (!livraisonListOld.contains(livraisonListNewLivraison)) {
                    Commande oldCommandeOfLivraisonListNewLivraison = livraisonListNewLivraison.getCommande();
                    livraisonListNewLivraison.setCommande(commande);
                    livraisonListNewLivraison = em.merge(livraisonListNewLivraison);
                    if (oldCommandeOfLivraisonListNewLivraison != null && !oldCommandeOfLivraisonListNewLivraison.equals(commande)) {
                        oldCommandeOfLivraisonListNewLivraison.getLivraisonList().remove(livraisonListNewLivraison);
                        oldCommandeOfLivraisonListNewLivraison = em.merge(oldCommandeOfLivraisonListNewLivraison);
                    }
                }
            }
            for (CommandeLigne commandeLigneListOldCommandeLigne : commandeLigneListOld) {
                if (!commandeLigneListNew.contains(commandeLigneListOldCommandeLigne)) {
                    commandeLigneListOldCommandeLigne.setCommande(null);
                    commandeLigneListOldCommandeLigne = em.merge(commandeLigneListOldCommandeLigne);
                }
            }
            for (CommandeLigne commandeLigneListNewCommandeLigne : commandeLigneListNew) {
                if (!commandeLigneListOld.contains(commandeLigneListNewCommandeLigne)) {
                    Commande oldCommandeOfCommandeLigneListNewCommandeLigne = commandeLigneListNewCommandeLigne.getCommande();
                    commandeLigneListNewCommandeLigne.setCommande(commande);
                    commandeLigneListNewCommandeLigne = em.merge(commandeLigneListNewCommandeLigne);
                    if (oldCommandeOfCommandeLigneListNewCommandeLigne != null && !oldCommandeOfCommandeLigneListNewCommandeLigne.equals(commande)) {
                        oldCommandeOfCommandeLigneListNewCommandeLigne.getCommandeLigneList().remove(commandeLigneListNewCommandeLigne);
                        oldCommandeOfCommandeLigneListNewCommandeLigne = em.merge(oldCommandeOfCommandeLigneListNewCommandeLigne);
                    }
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = commande.getIdCommande();
                if (findCommande(id) == null) {
                    throw new NonexistentEntityException("The commande with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Commande commande;
            try {
                commande = em.getReference(Commande.class, id);
                commande.getIdCommande();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The commande with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Livraison> livraisonListOrphanCheck = commande.getLivraisonList();
            for (Livraison livraisonListOrphanCheckLivraison : livraisonListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Commande (" + commande + ") cannot be destroyed since the Livraison " + livraisonListOrphanCheckLivraison + " in its livraisonList field has a non-nullable commande field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<CommandeLigne> commandeLigneList = commande.getCommandeLigneList();
            for (CommandeLigne commandeLigneListCommandeLigne : commandeLigneList) {
                commandeLigneListCommandeLigne.setCommande(null);
                commandeLigneListCommandeLigne = em.merge(commandeLigneListCommandeLigne);
            }
            em.remove(commande);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Commande> findCommandeEntities() {
        return findCommandeEntities(true, -1, -1);
    }

    public List<Commande> findCommandeEntities(int maxResults, int firstResult) {
        return findCommandeEntities(false, maxResults, firstResult);
    }

    private List<Commande> findCommandeEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Commande.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Commande findCommande(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Commande.class, id);
        } finally {
            em.close();
        }
    }

    public int getCommandeCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Commande> rt = cq.from(Commande.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
