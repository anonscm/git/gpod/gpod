package fr.univtours.gpod.persistance.DAO;

import fr.univtours.gpod.persistance.DAO.exceptions.NonexistentEntityException;
import fr.univtours.gpod.persistance.Manager.PersistenceManager;
import fr.univtours.gpod.persistance.entities.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: geoffroy.vibrac
 * Date: 03/04/13
 * Time: 14:27
 */
public class ImportExcelDAO implements Serializable {

    EntityManagerFactory emf= PersistenceManager.getInstance().getEntityManagerFactory();

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(ImportExcel importExcel) throws Exception {
        EntityManager em = getEntityManager();
        try{
            em.getTransaction().begin();
            em.persist(importExcel);
            em.getTransaction().commit();
            if (em != null) {
                em.close();
            }
        }catch(Exception e){
            Throwable t =  e.getCause();
            Throwable t2 = null;
            do {
               t2 = t;
               t = t.getCause();
            } while(t != null);
            if (!"Duplicate".equals(t2.getMessage().substring(0,9))){
                throw new Exception(t2.getMessage());
            }

        }
    }

    public void edit(ImportExcel importExcel) throws Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            importExcel = em.merge(importExcel);
            em.getTransaction().commit();
        } catch (Exception ex) {
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroyAllNew() throws fr.univtours.gpod.exceptions.NonexistentEntityException {

        String sql = "DELETE FROM ImportExcel i WHERE i.insere=0";

        EntityManager em = getEntityManager();
        em.getTransaction().begin();
        em.createQuery(sql).executeUpdate();
        em.getTransaction().commit();
    }


    public List<ImportExcel> getImport(Boolean NewImport, boolean all, int maxResults, int firstResult){

        String sql = "SELECT i FROM ImportExcel i";
        if (NewImport){
            sql += " WHERE i.insere=0";
        }
        EntityManager em = getEntityManager();

        Query query=null;

        query = em.createQuery(sql);


        if (!all) {
            query.setMaxResults(maxResults);
            query.setFirstResult(firstResult);
        }
        return query.getResultList();

    }
}
