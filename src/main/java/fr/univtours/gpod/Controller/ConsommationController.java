/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2012/01/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/

package fr.univtours.gpod.Controller;

import fr.univtours.gpod.Controller.stocks.Stock;
import fr.univtours.gpod.HTML.HTML;
import fr.univtours.gpod.persistance.DAO.*;
import fr.univtours.gpod.persistance.entities.*;
import fr.univtours.gpod.utils.AuthorizedProfils;
import fr.univtours.gpod.utils.Utilities;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;
import org.apache.commons.lang.StringEscapeUtils;
import org.joda.time.DateTime;
import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 *
 * @author geoffroy.vibrac
 */
public class ConsommationController  extends ActionController {

        public ConsommationController(HttpServletRequest request, HttpServletResponse response, HttpSession session){
            super(request, response, session);
            authorizedProfils = new AuthorizedProfils(request, response, session);
            authorizedProfils.addReadAuthorisedProfile("AD", "LA");
            authorizedProfils.addWriteAuthorisedProfile("AD", "LA");              
        }

        public String getHtml(){
            //----------------------------------------------------
            //Autorisations         
            String resp = authorizedProfils.getAuthorizedHtmlTicket();
            if (!"".equals(resp)){
                return resp;
            }
            //----------------------------------------------------  


                response.setContentType("text/html;charset=iso-8859-1");

                HTML html = HTML.getInstance();
                html.getHeader().setTitle("Gestion des produits dangereux &gt; Gestion des commandes");
                html.setExtjsHeader();
                html.getHeader().getCss().addLine("lib/multiselect/Multiselect.css");
                html.getHeader().getCss().addLine("lib/search/gridsearch.css");
                html.getHeader().getCss().addLine("lib/msg/msg.css");

                html.getHeader().getJs().addBlock("var B_nameUser = '"+Utilities.addSlashes(utilisateur.getNom())+"'");
                html.getHeader().getJs().addBlock("var B_lastCx = '"+Utilities.javaDateTIMEToString(utilisateur.getDateAvantDerniereConnexion())+"'");                 
                html.getHeader().getJs().addLine("version.js");
                html.getHeader().getJs().addLine("lib/printAction.js");
                html.getHeader().getJs().addLine("lib/help/helpPlugin.js");
                html.getHeader().getJs().addLine("lib/msg/msg.js");
                html.getHeader().getJs().addLine("lib/multiselect/Multiselect.js");
                html.getHeader().getJs().addLine("lib/multiselect/ItemSelector.js");
                html.getHeader().getJs().addLine("lib/search/gridSearch.js");
                html.getHeader().getJs().addLine("ui/search/SousStructure.js");
                html.getHeader().getJs().addLine("ui/search/AireStockage.js");
                html.getHeader().getJs().addLine("ui/search/produit.js");
                html.getHeader().getJs().addLine("ui/container.Consommation.js");
                html.getHeader().getJs().addLine("ui/menus/menu."+ session.getAttribute("profil") +".js");
                html.getHeader().getJs().addLine("ui/squelette.js");
                html.getBody().setBodyElement("<table align='center' class='contenttable' cellpadding='0' cellspacing='0' border='0'><tr><td id='centerPanel'></td></tr></table>");

                return html.getHTMLCode();
        }


        public String saveItem(){
            this.response.setContentType("application/json");
            //----------------------------------------------------
            //Autorisations         
            String resp = authorizedProfils.getWriteAuthorizedJsonTicket();
            if (!"".equals(resp)){
                return resp;
            }
            //----------------------------------------------------               
            String json="{success: false, errors:{reason:\'Enregistrement impossible'}}";
            String idAireS = request.getParameter("idAire");   
            String idSousStructureS = request.getParameter("idSousStructure");           
            String idArticleS = request.getParameter("idArticle");
            String contenuRestantS = request.getParameter("contenuRestant");
            String dateConsoS  = request.getParameter("dateConso");
            String dateOuvertureS = request.getParameter("dateOuverture");
            String quantiteS = request.getParameter("quantite");
            String commentaire = request.getParameter("commentaires");
            String affectationUtilisateur = request.getParameter("affectationUtilisateur");
            String quantitePreciseS = request.getParameter("newQty");  
            String idClickeyScanS = request.getParameter("idClickeyScan");  
   
  
        
            ClickeyScan cs = null;
            Integer idClickeyScan;
            try{
                idClickeyScan = Integer.parseInt(idClickeyScanS);
                if (idClickeyScan > 0 ){
                    ClickeyScanDAO daocs = new ClickeyScanDAO();                     
                    try {                          
                         cs = daocs.findClickeyScan(idClickeyScan);
                         if (cs!=null){
                             cs.setDejaTraite(true);
                         }
                    } catch (Exception ex) {
                        Logger.getLogger(ConsommationController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }catch(NumberFormatException ex){
                Logger.getLogger(ConsommationController.class.getName()).log(Level.INFO, null, ex);
            }            
            
            
            AireStockage aire = null;
            Integer idAire;
            try{
                idAire = Integer.parseInt(idAireS);
                if (idAire > 0 ){
                    try {
                        AireStockageDAO daob = new AireStockageDAO();
                        aire = daob.findAireStockage(idAire);

                    } catch (Exception ex) {
                        Logger.getLogger(ConsommationController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }catch(NumberFormatException ex){
                Logger.getLogger(ConsommationController.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            
            
            SousStructure sStructure = null;
            Integer idSousStructure;
            try{
                idSousStructure = Integer.parseInt(idSousStructureS);
                if (idSousStructure > 0 ){
                    try {
                        SousStructureDAO daob = new SousStructureDAO();
                        sStructure = daob.findSousStructure(idSousStructure);

                    } catch (Exception ex) {
                        Logger.getLogger(ConsommationController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }catch(NumberFormatException ex){
                Logger.getLogger(ConsommationController.class.getName()).log(Level.SEVERE, null, ex);
            }
            

            Article article = null;
            Integer idArticle;
            try{
                idArticle = Integer.parseInt(idArticleS);
                if (idArticle > 0 ){
                    try {
                        ArticleDAO daob = new ArticleDAO();
                        article = daob.findArticle(idArticle);

                    } catch (Exception ex) {
                        Logger.getLogger(ConsommationController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }catch(NumberFormatException ex){
                Logger.getLogger(ConsommationController.class.getName()).log(Level.SEVERE, null, ex);
            }

            DateTime now = new DateTime();
            
            DateTime dateConso = null;
            if (dateConsoS != null && dateConsoS.length() == 10){
                try{
                    int year = Integer.parseInt(dateConsoS.substring(6));
                    int month = Integer.parseInt(dateConsoS.substring(3, 5));
                    int day = Integer.parseInt(dateConsoS.substring(0, 2));
                    dateConso = new DateTime(year, month, day,now.getHourOfDay(),now.getMinuteOfHour(),now.getSecondOfMinute(),now.getMillisOfSecond());
                }catch(NumberFormatException ex){
                   Logger.getLogger(ConsommationController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            DateTime dateOuverture = null;
            if (dateOuvertureS != null && dateOuvertureS.length() == 10){
                try{
                    int year = Integer.parseInt(dateOuvertureS.substring(6));
                    int month = Integer.parseInt(dateOuvertureS.substring(3, 5));
                    int day = Integer.parseInt(dateOuvertureS.substring(0, 2));
                    dateOuverture = new DateTime(year, month, day,now.getHourOfDay(),now.getMinuteOfHour(),now.getSecondOfMinute(),now.getMillisOfSecond());
                }catch(NumberFormatException ex){
                   Logger.getLogger(ConsommationController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            Double quantite = null;
            try{
                quantite = Double.parseDouble(quantiteS.replaceAll(",", "."));                
            }catch(NumberFormatException ex){
                Logger.getLogger(ConsommationController.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            
            // GESTION DE LA QUANTITE EN STOCK
            // Si on saisi une quantite (quantitePrecise) c'est elle qui est pris en compte et pas le slider
            
            //pourcentageRestant = Nouveau pourcentage envoyée par le slider
            Double pourcentageRestantApresConsommation = null;
            try{
                pourcentageRestantApresConsommation = Double.parseDouble(contenuRestantS.replaceAll(",", "."));
            }catch(NumberFormatException ex){
                Logger.getLogger(ConsommationController.class.getName()).log(Level.SEVERE, null, ex);
            }            
            
            //quantiteRestant = Quantite précise est ramené par le champ Qté
            Double quantiteRestantApresConsommation = null;
            try{
                quantiteRestantApresConsommation = Double.parseDouble(quantitePreciseS.replaceAll(",", "."));
            }catch(NumberFormatException ex){
                Logger.getLogger(ConsommationController.class.getName()).log(Level.INFO, null, ex);
            }catch(NullPointerException ex){                  
            }
            

            


            if (article !=null && pourcentageRestantApresConsommation !=null){

                    
                    // si le champ Qté précise n'est pas saisie, on regarde la slider
                    if (quantiteRestantApresConsommation!=null){
                        Double qteInitiale = 100 * article.getProduitConditionnement().getContenance();                        
                        pourcentageRestantApresConsommation = (quantiteRestantApresConsommation * 100)/qteInitiale;
                        pourcentageRestantApresConsommation *=100;
                    }
                    
                    if (pourcentageRestantApresConsommation<=(article.getQuantite()*100)){
              
                            article.setQuantite(pourcentageRestantApresConsommation/100);
                            if (article.getDateOuverture() == null && dateOuverture!=null){
                                article.setDateOuverture(dateOuverture.toDate());
                            }

                            if (aire!= null && article.getAireStockage()!=aire){
                                article.setAireStockage(aire);
                            }
                            if (sStructure!=null && article.getSousStructure()!=sStructure){
                                article.setSousStructure(sStructure);
                            }

                            HistoriqueConsomation hc = new HistoriqueConsomation();
                            hc.setCommentaire(commentaire);
                            hc.setDateConsommation(dateConso.toDate());
                            hc.setArticle(article);
                            hc.setUtilisateur(this.getUtilisateur());
                            hc.setSousStructure(sStructure);
                            hc.setAireStockage(aire);
                            hc.setAffectationUtilisateur(affectationUtilisateur);
                            hc.setQuantiteRestante(pourcentageRestantApresConsommation/100);

                            HistoriqueConsomationDAO dao = new HistoriqueConsomationDAO();
                            dao.create(hc, cs);

                            // On va tester si il reste du produit en stock pour cet structure
                            //  sStructure article
                            Double qteRestante = getQuantiteStock(sStructure, article.getProduitConditionnement());


                            json = "{success: true, " +
                                    "qteRestante:"+qteRestante+", " +
                                    "idSousStructure:"+sStructure.getIdSousStructure()+", " +
                                    "idProduitConditionnement:"+article.getProduitConditionnement().getIdProduitConditionnement()+"}";

                    }else{
                        json = "{success: false, errors:{reason:\'Impossible de saisir une quantité plus elevée que la quantité restante\'}}";
                    }

            }else{
                json = "{success: false, errors:{reason:\'Une erreur s'est produite\'}}";
            }




            return json;
        }

    public Double getQuantiteStock(SousStructure sousStructure, ProduitConditionnement produitConditionnement){
        Double quantite = 0.0;
        List<Stock> stocks = getStock(sousStructure, produitConditionnement);
        for (Stock stock : stocks){
            quantite += stock.getQuantite();
        }

        return quantite;
    }


    public List<Stock> getStock(SousStructure sousStructure, ProduitConditionnement produitConditionnement){
        return getStock(null, null, null, null, produitConditionnement, null, null, sousStructure, null, false, null, 0);
    }


    public List<Stock> getStock(Site site, Batiment batiment, SalleStockage salleStockage, AireStockage aire, ProduitConditionnement produitConditionnement,
                                String nomSigne, Float valeurStock, SousStructure sousStructure, List<Pictogramme> pictogrammes, boolean conso, String etiquette,
                                int idArticle){

        List<Stock> stocks = null;
        List<Object[]> listArticles = null;
        String order = null;

        try {
            ArticleDAO dao = new ArticleDAO();
            listArticles = dao.findStockArticleWithQuery(-1,0, site, batiment, salleStockage, aire, produitConditionnement, nomSigne, valeurStock,
                    sousStructure, pictogrammes, conso, etiquette, idArticle, order, utilisateur.getSousStructure());

            stocks = Stock.getListStock(listArticles);

        } catch (Exception ex) {
            Logger.getLogger(StockController.class.getName()).log(Level.SEVERE, null, ex);

        }

        return stocks;
    }
        
        
        public List<HistoriqueConsomation> getListHistoriqueConsomation(int idArticle, int idSousStructure){
            List<HistoriqueConsomation> hcList = null;
            
            HistoriqueConsomationDAO daoHc = new HistoriqueConsomationDAO();
            hcList = daoHc.getHistoriqueConsomationByArticle(idArticle, idSousStructure);
            
            return hcList;
        }
        
        
        
        
        public String getListHistorique(){
            this.response.setContentType("application/json");
            String json="{success: false, errors:{reason:\'Lecture impossible'}}";
             
            //----------------------------------------------------
            //Autorisations         
            String resp = authorizedProfils.getReadAuthorizedJsonTicket();
            if (!"".equals(resp)){
                return resp;
            }
            //----------------------------------------------------                

            List<HistoriqueConsomation> hcList = null;
            
            
            int idArticle = 0;
            try{
                idArticle = Integer.parseInt(request.getParameter("idArticle"));                                
            }catch(NumberFormatException e){
            }
            
            int idSousStructure = 0;
            try{
                idSousStructure = Integer.parseInt(request.getParameter("idSousStructure"));                                
            }catch(NumberFormatException e){
            }            

       
            int nbItems = 0;
            
            try {

                HistoriqueConsomationDAO daoHc = new HistoriqueConsomationDAO();
                hcList = daoHc.getHistoriqueConsomationByArticle(idArticle, idSousStructure);
                nbItems = hcList.size();                
                
                this.response.setContentType("application/json");
                
                if (hcList !=null && hcList.size()>0){
                    json = "{'totalCount':'"+nbItems+"','data':[";
                    for (HistoriqueConsomation hc : hcList){                       
                        json += " {'idHistoriqueConsommation':'"+hc.getIdHistoriqueConsommation()+"',";
                        json += " 'dateConsommation':'"+Utilities.javaDateToString(hc.getDateConsommation())+"',";
                        json += " 'idUtilisateur':'"+hc.getUtilisateur().getIdUtilisateur()+"',";
                        json += "'nomUtilisateur':'"+Utilities.addSlashes(hc.getUtilisateur().getNom())+"',";
                        json += "'idSousStructure':'"+hc.getSousStructure().getIdSousStructure()+"',";
                        json += "'nomSousStructure':'"+Utilities.addSlashes(hc.getSousStructure().getNom())+"',";
                        json += "'idAire':'"+hc.getAireStockage().getIdAire()+"',";
                        json += "'nomAire':'"+Utilities.addSlashes(hc.getAireStockage().getSalleStockage().getNom() + " &gt; " + hc.getAireStockage().getNom())+"',";
                        json += "'quantiteRestante':'"+hc.getQuantiteRestante()+"',";
                        json += "'commentaire':'"+Utilities.addSlashes(hc.getCommentaire())+"',";
                        json += "'affectationUtilisateur':'"+Utilities.addSlashes(hc.getAffectationUtilisateur())+"'},";                        
                    }                    
                    json += "]}";
                }else{
                    json = "{'totalCount':'0','data':[]}";
                }
            } catch (Exception ex) {
                Logger.getLogger(ConsommationController.class.getName()).log(Level.SEVERE, null, ex);
            }
            return json;            
        }

    private Article getItemFromId(){
        
            Article article = null;
            int idArticle = 0;
            String idArticleS = request.getParameter("id");
            
            if (idArticleS!=null && !"".equals(idArticleS)){
                try{
                    idArticle = Integer.parseInt(idArticleS);
                    if (idArticle > 0 ){
                        try {
                            ArticleDAO dao = new ArticleDAO();
                            article = dao.findArticle(idArticle);
                        } catch (Exception ex) {
                            Logger.getLogger(CommandeController.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }catch(NumberFormatException ex){
                    Logger.getLogger(CommandeController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            return article;
        
    }          
        
        public String getPDF() {
            //----------------------------------------------------
            //Autorisations en lecture
            String resp = authorizedProfils.getReadAuthorizedJsonTicket();
            if (!"".equals(resp)){
                return resp;
            }
            //----------------------------------------------------   
            
            String json="{'success':false}"; 
            Article article = this.getItemFromId();   
            
            
            PdfDocumentFactory pdf = new PdfDocumentFactory("article_" + article.getIdArticle());           
            
            if (article!=null){
                
                pdf.addVerticalTable("Article", 24, article.getDataForVerticalTable(), 0);
                pdf.addHorizontalTable("Consommations", 15, HistoriqueConsomation.getDataForHorizontalTable(
                        this.getListHistoriqueConsomation(article.getIdArticle(), article.getSousStructure().getIdSousStructure())), 1, null);   
                
            }
            
            pdf.getDocument().close();     

            if (!"".equals(pdf.getFileName())){
                return "{'success':true,'fileName':'"+pdf.getFileName()+"'}";
            }else{
                return json;   
            }
            
        }  
        
       public String getXLSList(){

            String fileName = "";
            List<Article> articleList= new ArrayList<Article>();
            articleList.add(this.getItemFromId()); 
             
            try {

                DateTimeFormatter FILE_FORMAT = DateTimeFormat.forPattern("yyyyMMddHmsS");
                DateTimeFormatter USER_FORMAT = DateTimeFormat.forPattern("H:m dd/MM/yyyy");


                LocalDateTime now = new LocalDateTime();
                String dateStr = now.toString(FILE_FORMAT);


                fileName = "Article_"+dateStr+".xls";
                
                WritableWorkbook wrtWorkbook = Workbook.createWorkbook(new File(fileName));

                WritableSheet sheet1;

                sheet1 = wrtWorkbook.createSheet("Article", 0);

                Label titre = new Label(0, 0, "Article, le " + now.toString(USER_FORMAT));
                sheet1.addCell(titre);
           

                sheet1.addCell(new Label(0, 2, "Produit"));
                sheet1.addCell(new Label(1, 2, "Etiquette"));
                sheet1.addCell(new Label(2, 2, "Structure"));
                sheet1.addCell(new Label(3, 2, "Local de Stockage"));
                sheet1.addCell(new Label(4, 2, "Qté restante"));
                sheet1.addCell(new Label(5, 2, "Date d'ouverture"));
                sheet1.addCell(new Label(6, 2, "Date consommation"));
                sheet1.addCell(new Label(7, 2, "Prénom Utilisateur"));
                sheet1.addCell(new Label(8, 2, "Nom Utilisateur"));
                sheet1.addCell(new Label(9, 2, "Aire de Stockage"));
                sheet1.addCell(new Label(10, 2, "Salle de Stockage"));
                sheet1.addCell(new Label(11, 2, "Qté restante")); 
                sheet1.addCell(new Label(12, 2, "Commentaire")); 
                sheet1.addCell(new Label(13, 2, "Affectation"));
                

                int i = 3;
                for (Article a : articleList){
                    
                    sheet1.addCell(new Label(0, i, StringEscapeUtils.unescapeHtml(a.getProduitConditionnement().getProduit().getNom())));
                    sheet1.addCell(new Label(1, i, a.getIdentifiantEtiquette()));
                    sheet1.addCell(new Label(2, i, a.getSousStructure().getNom()));
                    sheet1.addCell(new Label(3, i, a.getAireStockage().getSalleStockage().getNom()+" > "+ a.getAireStockage().getNom()));
                    sheet1.addCell(new Label(4, i, a.getQuantite().toString()));                    
                    sheet1.addCell(new Label(5, i, Utilities.javaDateToString(a.getDateOuverture())));                    
                    for (HistoriqueConsomation hc : this.getListHistoriqueConsomation(a.getIdArticle(), a.getSousStructure().getIdSousStructure())){
                        i++;
                        sheet1.addCell(new Label(6, i, Utilities.javaDateToString(hc.getDateConsommation())));
                        sheet1.addCell(new Label(7, i, hc.getUtilisateur().getPrenom()));
                        sheet1.addCell(new Label(8, i, hc.getUtilisateur().getNom()));
                        sheet1.addCell(new Label(9, i, hc.getAireStockage().getNom()));
                        sheet1.addCell(new Label(10, i, hc.getAireStockage().getSalleStockage().getNom()));
                        sheet1.addCell(new Label(11, i, hc.getQuantiteRestante().toString())); 
                        sheet1.addCell(new Label(12, i, hc.getCommentaire())); 
                        sheet1.addCell(new Label(13, i, hc.getAffectationUtilisateur()));                        
                    }                    
                    i++;
                    

                }
                
                wrtWorkbook.write();
                wrtWorkbook.close();


            } catch (IOException ex) {
                Logger.getLogger(FournisseurController.class.getName()).log(Level.SEVERE, null, ex);
            } catch (RowsExceededException ex) {
                Logger.getLogger(FournisseurController.class.getName()).log(Level.SEVERE, null, ex);
            } catch (WriteException ex) {
                Logger.getLogger(FournisseurController.class.getName()).log(Level.SEVERE, null, ex);
            }

            return "{'success':true,'fileName':'"+fileName+"'}";

        }             

}
