/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2012/01/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/

package fr.univtours.gpod.Controller;

import fr.univtours.gpod.exceptions.DuplicateIdentifiantEtiquetteException;
import fr.univtours.gpod.json.JSONArray;
import fr.univtours.gpod.json.JSONException;
import fr.univtours.gpod.json.JSONObject;
import fr.univtours.gpod.json.JsonConversionUtilities;
import fr.univtours.gpod.persistance.DAO.ArticleDAO;
import fr.univtours.gpod.persistance.DAO.exceptions.NonexistentEntityException;
import fr.univtours.gpod.persistance.entities.Article;
import fr.univtours.gpod.utils.AuthorizedProfils;
import fr.univtours.gpod.utils.Utilities;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author geoffroy.vibrac
 */
public class ArticleController  extends ActionController{
        
        public ArticleController(HttpServletRequest request, HttpServletResponse response, HttpSession session){
            super(request, response, session);
            authorizedProfils = new AuthorizedProfils(request, response, session);
            authorizedProfils.addReadAuthorisedProfile("RA","AD", "LA", "SE");
            authorizedProfils.addWriteAuthorisedProfile("AD", "LA", "SE");             
        } 
        
        public String getListItems(){  
            this.response.setContentType("application/json");
            //----------------------------------------------------
            //Autorisations         
            String resp = authorizedProfils.getReadAuthorizedJsonTicket();
            if (!"".equals(resp)){
                return resp;
            }
            //----------------------------------------------------                
            String json = "";
            List<Article> articleList = null;

            int idCommande = 0;
            try{
                idCommande = Integer.parseInt(request.getParameter("idCommande"));
            }catch(NumberFormatException e){
            }

       
            int nbItems = 0;
            
            try {
                ArticleDAO dao = new ArticleDAO();
                if (idCommande != 0){
                    articleList = dao.findArticleEntitiesParCommande(idCommande);
                }else{
                    articleList = dao.findArticleEntities();
                }
                nbItems = articleList.size();                
                
                this.response.setContentType("application/json");
                
                if (articleList !=null && articleList.size()>0){
                    json = "{'totalCount':'"+nbItems+"','data':[";
                    for (Article u : articleList){                       
                        json += " {'CAS':'"+u.getProduitConditionnement().getProduit().getSubstance().getCas()+"',";
                        json += " 'idArticle':'"+u.getIdArticle()+"',";
                        json += " 'idProduitConditionnement':'"+u.getProduitConditionnement().getIdProduitConditionnement()+"',";
                        json += "'identifiantEtiquette':'"+u.getIdentifiantEtiquette()+"',";
                        json += "'nom':'"+Utilities.addSlashes(u.getProduitConditionnement().getProduit().getNom())+"',";
                        json += "'reference':'"+Utilities.addSlashes(u.getProduitConditionnement().getReferenceFournisseur())+"',";
                        json += "'idSousStructure':'"+u.getSousStructure().getIdSousStructure()+"',";
                        json += "'nomSousStructure':'"+Utilities.addSlashes(u.getSousStructure().getNom())+"',";
                        json += "'idAire':'"+u.getAireStockage().getIdAire()+"',";
                        json += "'nomAire':'"+Utilities.addSlashes(u.getAireStockage().getSalleStockage().getNom() + "&gt;" + u.getAireStockage().getNom())+"',";
                        json += "'quantite':'"+u.getQuantite()+"',";
                        json += "'contenance':'"+u.getProduitConditionnement().getContenance()+"',";
                        json += "'uniteMesure':'"+u.getProduitConditionnement().getUniteMesure().getAbreviation()+"',";
                        json += "'uniteMesureLong':'"+u.getProduitConditionnement().getUniteMesure().getLibelleLong()+"'},";                        
                    }                    
                    json += "]}";
                }else{
                    json = "{'totalCount':'0','data':[]}";
                }
            } catch (Exception ex) {
                Logger.getLogger(ArticleController.class.getName()).log(Level.SEVERE, null, ex);
            }
            return json;       
            
        }
        
        public String  saveItem(){ 
            this.response.setContentType("application/json");
            //----------------------------------------------------
            //Autorisations         
            String resp = authorizedProfils.getWriteAuthorizedJsonTicket();
            if (!"".equals(resp)){
                return resp;
            }
            //----------------------------------------------------              
            String json = "{success: false, errors:{reason:\'Enregistrement impossible\'}}";
            String articlesLivraison = request.getParameter("articleList");
            List<Article> articleList = null;
            
            try{
                articleList = JsonConversionUtilities.jsonArticleArrayToList(articlesLivraison, 0);
            }catch (DuplicateIdentifiantEtiquetteException ex){
                json = "{success: false, errors:{reason:\'"+ex.getMessage()+"'}}";
                return json;
            }
            
            
            if (articleList!=null && articleList.size()>0){
                ArticleDAO daoa = new ArticleDAO();
                for (Article a : articleList){
                    try {
                        daoa.edit(a);
                        json = "{success: true}";
                    } catch (NonexistentEntityException ex) {
                        Logger.getLogger(LivraisonSimpleController.class.getName()).log(Level.SEVERE, null, ex);
                        json = "{success: false, errors:{reason:\'"+ex.getMessage()+"\'}}";
                    } catch (Exception ex) {
                        Logger.getLogger(LivraisonSimpleController.class.getName()).log(Level.SEVERE, null, ex);
                        json = "{success: false, errors:{reason:\'"+ex.getMessage()+"\'}}";
                    }
                }
            }            
            
            return json;
            
//            if (articleList!=null && !"".equals(articleList)){
//                try {
//                    JSONArray articleArray = new JSONArray(articleList);
//                    for (int i = 0; i < articleArray.length(); i++) {                        
//                        JSONObject obj = articleArray.getJSONObject(i);
//                        int idArticle = Integer.parseInt((String) obj.get("idArticle"));
//                        String identifiantEtiquette = (String) obj.get("identifiantEtiquette");
//                        
//                        ArticleDAO dao = new ArticleDAO();
//                        Article a = dao.findArticle(idArticle);
//                        
//                        // si identifiantEtiquette est rempli
//                        if (!"".equals(identifiantEtiquette)){                           
//                            // On cherche un article avec le même identifiant etiquette
//                            Article a2 = dao.findArticleFromIdentifiantEtiquette(identifiantEtiquette);
//                            // si il y a un article avec cet etiquette et que ce n'est pas cet article qu'on modifie
//                            // => On retourne une erreur
//                            if (a2!=null && !a2.equals(a)){                        
//                                return "{success: false, errors:{reason:\'Cette étiquette est déjà utilisée par un autre produit\'}}";
//                            }                            
//                        }
//                        
//                        if (a!=null){
//                            a.setIdentifiantEtiquette(identifiantEtiquette);
//                            dao.edit(a);
//                        }                        
//                        
//                        
//                    }
//                    json = "{success: true}";
//                    
//                } catch (NonexistentEntityException ex) {
//                    Logger.getLogger(ArticleController.class.getName()).log(Level.SEVERE, null, ex);
//                } catch (JSONException ex) {
//                    Logger.getLogger(ArticleController.class.getName()).log(Level.SEVERE, null, ex);
//                } catch (Exception ex) {
//                    Logger.getLogger(ArticleController.class.getName()).log(Level.SEVERE, null, ex);                 
//                }
//                
//                
//            }
                 
        }
        
        
}
