/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2012/01/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/


package fr.univtours.gpod.Controller;

import fr.univtours.gpod.utils.AuthorizedProfils;
import fr.univtours.gpod.exceptions.NonexistentEntityException;
import fr.univtours.gpod.persistance.entities.Fournisseur;
import fr.univtours.gpod.persistance.DAO.FournisseurDAO;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.List;
import fr.univtours.gpod.HTML.HTML;
import fr.univtours.gpod.utils.Utilities;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;
import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
/**
 *
 * @author geoffroy.vibrac
 */
public class FournisseurController extends ActionController {

               
        public FournisseurController(HttpServletRequest request, HttpServletResponse response, HttpSession session){
            super(request, response, session);
            authorizedProfils = new AuthorizedProfils(request, response, session);
            authorizedProfils.addReadAuthorisedProfile("AD","LA", "SE", "RA", "ME", "HY");
            authorizedProfils.addWriteAuthorisedProfile("AD");
        }    
    
  
        public String saveItem(){
            //----------------------------------------------------
            //Autorisations         
            String resp = authorizedProfils.getWriteAuthorizedJsonTicket();
            if (!"".equals(resp)){
                return resp;
            }
            //----------------------------------------------------                  
            String json="";
            Integer idFournisseur=0;
            FournisseurDAO dao = null;
            
            this.response.setContentType("application/json");
                    
            // On récupère l'idUTilisateur, il peut être vide si nouvel utilisateur
            try{
                idFournisseur = Integer.parseInt(request.getParameter("idFournisseur"));
            }catch(NumberFormatException e){
                idFournisseur = 0;
            }
            
            String referenceSIFAC = request.getParameter("referenceSIFAC");
            String nom = request.getParameter("nom");
            String adresse1 = request.getParameter("adresse1");
            String adresse2 = request.getParameter("adresse2");
            String codePostal = request.getParameter("codePostal");
            String ville = request.getParameter("ville");
            String telephone = request.getParameter("telephone");
            String fax = request.getParameter("fax");
            String nomContact = request.getParameter("nomContact");
                      
            
            // Test des champs
            if ("".equals(nom)){
                json = "{success: false, errors:{reason:\'Des champs obligatoires n\\\'ont pas été saisis\'}}";
                return json;          
            }
                      
            try{
                 dao = new FournisseurDAO();              
            } catch (Exception ex) {
                Logger.getLogger(FournisseurController.class.getName()).log(Level.SEVERE, null, ex);
                json = "{success: false, errors:{reason:\'"+ex.getMessage()+"\'}}";
                return json;
            }
            
            
            // Ajout nouveau fournisseur idFournisseur = 0
            if (idFournisseur == 0){

                    Fournisseur fournisseur = new Fournisseur(referenceSIFAC, nom.trim(), adresse1, adresse2, codePostal, ville, nomContact, telephone, fax);
                    if (fournisseur !=null){
                        try{
                            dao.create(fournisseur);
                            json = "{success: true}";
                        } catch (Exception ex) {
                            Logger.getLogger(FournisseurController.class.getName()).log(Level.SEVERE, null, ex);
                            json = "{success: false, errors:{reason:\'"+ex.getMessage()+"\'}}";
                        }
                    }                                  

          
            // UPDATE fournisseur idFournisseur > 0
            }else if (idFournisseur > 0L){
                

                    Fournisseur fournisseur = dao.findFournisseur(idFournisseur);
                 
                    if (fournisseur !=null){
                        
                        fournisseur.setReferenceSIFAC(referenceSIFAC);
                        fournisseur.setNom(nom.trim());
                        fournisseur.setAdresse1(adresse1);
                        fournisseur.setAdresse2(adresse2);
                        fournisseur.setCodePostal(codePostal);
                        fournisseur.setVille(ville);
                        fournisseur.setTelephone(telephone);
                        fournisseur.setFax(fax);
                        fournisseur.setNomContact(nomContact);

                        
                        try{
                            dao.edit(fournisseur);
                            json = "{success: true}"; 
                        } catch (NonexistentEntityException ex) {
                            Logger.getLogger(FournisseurController.class.getName()).log(Level.SEVERE, null, ex);
                            json = "{success: false, errors:{reason:\'"+ex.getMessage()+"\'}}";  
                        } catch (Exception ex) {
                            Logger.getLogger(FournisseurController.class.getName()).log(Level.SEVERE, null, ex);
                            json = "{success: false, errors:{reason:\'"+ex.getMessage()+"\'}}";  
                        }                            
                    }                       
                    
        
            }else{
                json = "{success: false, errors:{reason:\'Impossible de trouver "+idFournisseur+"\'}}";
            }
    
           
            return json;            
            //return "{success: false, errors:{reason:\'"+msg+"\'}};
        }
        
        
    
        public String getHtml(){
                //----------------------------------------------------
                //Autorisations         
                String resp = authorizedProfils.getAuthorizedHtmlTicket();
                if (!"".equals(resp)){
                    return resp;
                }
                //----------------------------------------------------  
                response.setContentType("text/html;charset=iso-8859-1");

                HTML html = HTML.getInstance();
                html.getHeader().setTitle("Gestion des produits dangereux &gt; Gestion des fournisseurs");
                html.setExtjsHeader();
                html.getHeader().getCss().addLine("lib/multiselect/Multiselect.css");
                html.getHeader().getCss().addLine("lib/search/gridsearch.css");
                html.getHeader().getCss().addLine("lib/msg/msg.css");

                html.getHeader().getJs().addBlock("var B_nameUser = '"+Utilities.addSlashes(utilisateur.getNom())+"'");
                html.getHeader().getJs().addBlock("var B_lastCx = '"+Utilities.javaDateTIMEToString(utilisateur.getDateAvantDerniereConnexion())+"'");                 
                html.getHeader().getJs().addLine("version.js");
                html.getHeader().getJs().addLine("lib/printAction.js");
                html.getHeader().getJs().addLine("lib/msg/msg.js");
                html.getHeader().getJs().addLine("lib/multiselect/Multiselect.js");
                html.getHeader().getJs().addLine("lib/multiselect/ItemSelector.js");   
                html.getHeader().getJs().addLine("lib/search/gridSearch.js");
                html.getHeader().getJs().addLine("lib/excelButton.js");
                html.getHeader().getJs().addLine("ui/container.Fournisseurs.js");
                html.getHeader().getJs().addLine("ui/menus/menu."+ session.getAttribute("profil") +".js");
                html.getHeader().getJs().addLine("ui/squelette.js");
                html.getBody().setBodyElement("<table align='center' class='contenttable' cellpadding='0' cellspacing='0' border='0'><tr><td id='centerPanel'></td></tr></table>");

                return html.getHTMLCode();
        }
    
        
        public List<Fournisseur> getListFournisseur(){
            return this.getListFournisseur(-1, 0, "", "");
            
            
        }
        
        private List<Fournisseur> getListFournisseur(int maxResults, int firstResult, String searchValue, String searchColumns){
            List<Fournisseur> fournisseur = null;
            try {
                FournisseurDAO dao = new FournisseurDAO();
                fournisseur = dao.findFournisseurEntities(searchValue, searchColumns, maxResults, firstResult);
            } catch (Exception ex) {
                Logger.getLogger(FournisseurController.class.getName()).log(Level.SEVERE, null, ex);
            }

            return fournisseur;
        }
        
        private int getProduitCount(String searchValue, String searchColumns){
            int nbItems = 0;
            try {
                FournisseurDAO dao = new FournisseurDAO();
                nbItems = dao.getFournisseurWithQueryCount(searchValue, searchColumns);
            } catch (Exception ex) {
                Logger.getLogger(FournisseurController.class.getName()).log(Level.SEVERE, null, ex);
            }

            return nbItems;
        }          
        
        
        public String getListItems(){
                      
            String json = "";
            List<Fournisseur> fournisseurs = null;
            int nbItems = 0;
            // ------------------------------------------------------------------
            int maxResults = -1;
            int firstResult = -1;
            try{
                maxResults = Integer.parseInt(request.getParameter("limit"));
                firstResult = Integer.parseInt(request.getParameter("start"));
                if (maxResults == 0){
                    maxResults=10;
                }
            }catch(NumberFormatException e){
            }
            String searchValue = request.getParameter("query");
            String searchColumns = request.getParameter("fields");
            // ------------------------------------------------------------------  
       
            
            try {

                fournisseurs = this.getListFournisseur(maxResults, firstResult, searchValue, searchColumns);
                nbItems = this.getProduitCount(searchValue, searchColumns);
                
                this.response.setContentType("application/json");
                
                if (fournisseurs !=null && fournisseurs.size()>0){
                    json = "{'totalCount':'"+nbItems+"','data':[";
                    for (Fournisseur u : fournisseurs){                        
                        json += " {'idFournisseur':'"+u.getIdFournisseur()+"',";
                        json += "'referenceSIFAC':'"+Utilities.addSlashes(u.getReferenceSIFAC())+"',";                        
                        json += "'nom':'"+Utilities.addSlashes(u.getNom())+"',";
                        json += "'adresse1':'"+Utilities.addSlashes(u.getAdresse1())+"',";
                        json += "'adresse2':'"+Utilities.addSlashes(u.getAdresse2())+"',";
                        json += "'codePostal':'"+u.getCodePostal()+"',";
                        json += "'ville':'"+Utilities.addSlashes(u.getVille())+"',";
                        json += "'telephone':'"+u.getTelephone()+"',";
                        json += "'fax':'"+u.getFax()+"',";
                        json += "'nomContact':'"+Utilities.addSlashes(u.getNomContact())+"',";
                        json += "'write':'"+AuthorizedProfils.isWriteAllowed(utilisateur.getUtilisateurProfil().getCode(), "AD", "LA", "SE")+"'},";
                    }                    
                    json += "]}";
                }else{
                    json = "{'totalCount':'0','data':[]}";
                }
            } catch (Exception ex) {
                Logger.getLogger(FournisseurController.class.getName()).log(Level.SEVERE, null, ex);
            }
            return json;                   
        }
      
       
        public String getPDF(){
             //----------------------------------------------------
            //Autorisations en lecture
            String resp = authorizedProfils.getReadAuthorizedJsonTicket();
            if (!"".equals(resp)){
                return resp;
            }
            //----------------------------------------------------   
            
            String json="{'success':false}"; 
            Fournisseur fournisseur = this.getItemFromId();
       
        
            PdfDocumentFactory pdf = new PdfDocumentFactory("fournisseur_" + fournisseur.getIdFournisseur());

            pdf.addVerticalTable(fournisseur.getNom(), 24, fournisseur.getDataForVerticalTable(), 0);

            pdf.getDocument().close();     

            if (!"".equals(pdf.getFileName())){
                return "{'success':true,'fileName':'"+pdf.getFileName()+"'}";
            }else{
                return json;   
            }
                
            
                    
        }
    
    // Methode utilisée pour les édition XLS et PDF
        // retourne l'item à partir de l'id. Si l'id n'est pas définit, on a demandé l'impression de la liste (voir méthode getXLSList()
        protected Fournisseur getItemFromId(){

                Fournisseur fournisseur = null;
                Integer idFournisseur = 0;
                String idFournisseurS = request.getParameter("id");

                if (idFournisseurS!=null && !"".equals(idFournisseurS)){
                    try{
                        idFournisseur = Integer.parseInt(idFournisseurS);
                        if (idFournisseur > 0 ){
                            try {
                                FournisseurDAO dao = new FournisseurDAO();
                                fournisseur = dao.findFournisseur(idFournisseur);
                            } catch (Exception ex) {
                                Logger.getLogger(FournisseurController.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }
                    }catch(NumberFormatException ex){
                        Logger.getLogger(FournisseurController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                return fournisseur;

        }         
        
       public String getXLSList(){

            String fileName = "";
            List<Fournisseur> fournisseurList = null;
            
            Fournisseur fournisseur = this.getItemFromId();
            if (fournisseur==null){
                fournisseurList = this.getListFournisseur();
            }else{
                fournisseurList = new ArrayList<>();
                fournisseurList.add(fournisseur);
            }            
            
            
            
            try {

                DateTimeFormatter FILE_FORMAT = DateTimeFormat.forPattern("yyyyMMddHmsS");
                DateTimeFormatter USER_FORMAT = DateTimeFormat.forPattern("H:m dd/MM/yyyy");


                LocalDateTime now = new LocalDateTime();
                String dateStr = now.toString(FILE_FORMAT);


                fileName = "Fournisseurs_"+dateStr+".xls";
                
                WritableWorkbook wrtWorkbook = Workbook.createWorkbook(new File(fileName));

                WritableSheet sheet1;

                sheet1 = wrtWorkbook.createSheet("Fournisseurs", 0);

                Label titre = new Label(0, 0, "Fournisseurs, le " + now.toString(USER_FORMAT));
                sheet1.addCell(titre);

                sheet1.addCell(new Label(0, 2, "Reference Sifac"));
                sheet1.addCell(new Label(1, 2, "Nom"));
                sheet1.addCell(new Label(2, 2, "Adresse"));
                sheet1.addCell(new Label(3, 2, "Complement"));
                sheet1.addCell(new Label(4, 2, "Code Postal"));
                sheet1.addCell(new Label(5, 2, "Ville"));
                sheet1.addCell(new Label(6, 2, "Telephone"));
                sheet1.addCell(new Label(7, 2, "Fax"));
                sheet1.addCell(new Label(8, 2, "Contact"));

                int i = 3;
                for (Fournisseur f : fournisseurList){
                    
                    sheet1.addCell(new Label(0, i, f.getReferenceSIFAC()));
                    sheet1.addCell(new Label(1, i, f.getNom()));
                    sheet1.addCell(new Label(2, i, f.getAdresse1()));
                    sheet1.addCell(new Label(3, i, f.getAdresse2()));
                    sheet1.addCell(new Label(4, i, f.getCodePostal()));                    
                    sheet1.addCell(new Label(5, i, f.getVille())); 
                    sheet1.addCell(new Label(6, i, f.getTelephone())); 
                    sheet1.addCell(new Label(7, i, f.getFax())); 
                    sheet1.addCell(new Label(8, i, f.getNomContact())); 
                    
                    i++;

                }
                
                wrtWorkbook.write();
                wrtWorkbook.close();


            } catch (IOException ex) {
                Logger.getLogger(FournisseurController.class.getName()).log(Level.SEVERE, null, ex);
            } catch (RowsExceededException ex) {
                Logger.getLogger(FournisseurController.class.getName()).log(Level.SEVERE, null, ex);
            } catch (WriteException ex) {
                Logger.getLogger(FournisseurController.class.getName()).log(Level.SEVERE, null, ex);
            }

            return "{'success':true,'fileName':'"+fileName+"'}";

        }                
        
}
