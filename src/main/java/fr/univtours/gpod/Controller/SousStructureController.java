/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2012/01/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/

package fr.univtours.gpod.Controller;

import fr.univtours.gpod.exceptions.NonexistentEntityException;
import fr.univtours.gpod.persistance.DAO.StructureDAO;
import fr.univtours.gpod.persistance.entities.Structure;
import fr.univtours.gpod.persistance.entities.Utilisateur;
import fr.univtours.gpod.persistance.DAO.UtilisateurDAO;
import java.util.logging.Logger;
import java.util.logging.Level;
import fr.univtours.gpod.persistance.entities.SousStructure;
import java.util.List;
import fr.univtours.gpod.persistance.DAO.SousStructureDAO;
import fr.univtours.gpod.utils.AuthorizedProfils;
import fr.univtours.gpod.utils.Utilities;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletRequest;
import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;
import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 *
 * @author geoffroy.vibrac
 */
public class SousStructureController extends ActionController {

          
        public SousStructureController(HttpServletRequest request, HttpServletResponse response, HttpSession session){
            super(request, response, session);        
        }

        public String getListItems(){
            return getListItems(null);
        }


        // Renvoie les structure
        // si le paramètre idUtilisateur est indiqué : renvoie les structure SAUF celle de l'utilisateur
        protected String getListItems(String type){
            this.response.setContentType("application/json");            
            
            String json = ""; 
            Long idUtilisateur = 0L;
            int nbItems = 0;
            List<SousStructure> sousStructuresUtilisateur = null;
            
            List<SousStructure> sousStructures = this.getListSousStructure(type);
            nbItems = this.getListStructureCount(type);
            
            // Récupération de idUtilisateur dans le cas d'appel depuis Utilisateur
            try{
                idUtilisateur = Long.parseLong(request.getParameter("idUtilisateur"));
            }catch(NumberFormatException e){
            }
            
            try {
                // si un utilisateur est selectionné, il faut supprimer les sousStructures de cet utilisateur
                if (idUtilisateur>0){
                    UtilisateurDAO dao = new UtilisateurDAO();               
                    Utilisateur user = dao.findUtilisateur(idUtilisateur);
                    sousStructuresUtilisateur = user.getSousStructure();
                }
                              
                
                if (sousStructures !=null && sousStructures.size()>0){

                    // pour afficher seulement les structure AUTRE que celle de l'utilisateur
                    // On enleve les sousStructures de l'utilisateur
                    // le nbItems est alors décrémenté du nom de sous structure de l'utilisateur
                    if (sousStructuresUtilisateur!=null && sousStructuresUtilisateur.size()>0){
                        sousStructures.removeAll(sousStructuresUtilisateur);
                        nbItems -= sousStructuresUtilisateur.size();
                    }
                    json = "{'totalCount':'"+nbItems+"','data':[";
                    for (SousStructure s : sousStructures){
                        json += " {'idSousStructure':'"+s.getIdSousStructure()+"',";
                        json += "'code':'"+Utilities.addSlashes(s.getCode())+"',";
                        json += "'nom':'"+Utilities.addSlashes(s.getNom())+"',";
                        json += "'adresse1':'"+Utilities.addSlashes(s.getAdresse1())+"',";
                        json += "'adresse2':'"+Utilities.addSlashes(s.getAdresse2())+"',";
                        json += "'codePostal':'"+s.getCodePostal()+"',";
                        json += "'ville':'"+Utilities.addSlashes(s.getVille())+"',";
                        json += "'telephone':'"+s.getTelephone()+"',";
                        json += "'fax':'"+s.getFax()+"',";
                        json += "'responsable':'"+Utilities.addSlashes(s.getResponsable())+"',";
                        json += "'idStructure':'"+s.getStructure().getIdStructure()+"'},";
                    }                    
                    json += "]}";
                }else{
                    json = "{'totalCount':'0','data':[]}";
                }
            } catch (Exception ex) {
                Logger.getLogger(SousStructureController.class.getName()).log(Level.SEVERE, null, ex);
            }
            return json;                   
            
        }

    
        

        
        protected String saveItem(String type){
            this.response.setContentType("application/json");
            
            String json="";
            int idSousStructure=0;
            int idStructure=0;
            Structure structure = null;
            SousStructureDAO dao = null;
            
            try{
                idSousStructure = Integer.parseInt(request.getParameter("idSousStructure"));
            }catch(NumberFormatException e){
                idSousStructure = 0;
            }
            
            
            String nom = request.getParameter("nom");
            String code = request.getParameter("code");
            String adresse1 = request.getParameter("adresse1");
            String adresse2 = request.getParameter("adresse2");
            String codePostal = request.getParameter("codePostal");
            String ville = request.getParameter("ville");
            String telephone = request.getParameter("telephone");
            String fax = request.getParameter("fax");
            String responsable = request.getParameter("responsable");
                    
            String idStructureS = request.getParameter("idStructure");
            try{
                idStructure = Integer.parseInt(idStructureS);
            }catch(NumberFormatException ex){
                idStructure=0;
            }              
            
            // Test des champs
            if ("".equals(nom) || idStructure==0){
                json = "{success: false, errors:{reason:\'Des champs obligatoires n\\\'ont pas été saisis\'}}";
                return json;          
            }
                      
            try{
                 dao = new SousStructureDAO();
            } catch (Exception ex) {
                Logger.getLogger(SousStructureController.class.getName()).log(Level.SEVERE, null, ex);
                json = "{success: false, errors:{reason:\'"+ex.getMessage()+"\'}}";
                return json;
            }
                      

            try{
                if (idStructure >0){
                    StructureDAO daoStructure = new StructureDAO();
                    structure = daoStructure.findStructure(idStructure);
                 }
                 if (structure == null){
                      json = "{success: false, errors:{reason:\'Structure ne peut pas été null\'}}";
                      return json;                  
                 }
                   
                
                             
            } catch (Exception ex) {
                Logger.getLogger(SousStructureController.class.getName()).log(Level.SEVERE, null, ex);
                json = "{success: false, errors:{reason:\'"+ex.getMessage()+"\'}}";
                return json;
            }
            
             // Ajout nouvelle sous structure idSousStructure = 0
            if (idSousStructure == 0){

                    SousStructure sousStructure = new SousStructure(nom, structure, code, adresse1, adresse2, codePostal, ville, telephone, fax, responsable, type);
                    if (sousStructure !=null){
                        try{
                            dao.create(sousStructure);
                            json = "{success: true}";
                        } catch (Exception ex) {
                            Logger.getLogger(SousStructureController.class.getName()).log(Level.SEVERE, null, ex);
                            json = "{success: false, errors:{reason:\'"+ex.getMessage()+"\'}}";
                        }
                    }                                  

          
            // UPDATE id > 0
            }else if (idSousStructure > 0L){
                

                    SousStructure sousStructure = dao.findSousStructure(idSousStructure);
                 
                    if (sousStructure !=null){
                        
                        sousStructure.setNom(nom);
                        sousStructure.setCode(code);
                        sousStructure.setAdresse1(adresse1);
                        sousStructure.setAdresse2(adresse2);
                        sousStructure.setCodePostal(codePostal);
                        sousStructure.setVille(ville);
                        sousStructure.setTelephone(telephone);
                        sousStructure.setFax(fax);
                        sousStructure.setResponsable(responsable);
                        sousStructure.setStructure(structure);

                        
                        try{
                            dao.edit(sousStructure);
                            json = "{success: true}"; 
                        } catch (NonexistentEntityException ex) {
                            Logger.getLogger(SousStructureController.class.getName()).log(Level.SEVERE, null, ex);
                            json = "{success: false, errors:{reason:\'"+ex.getMessage()+"\'}}";  
                        } catch (Exception ex) {
                            Logger.getLogger(SousStructureController.class.getName()).log(Level.SEVERE, null, ex);
                            json = "{success: false, errors:{reason:\'"+ex.getMessage()+"\'}}";  
                        }                            
                    }                       
                    
        
            }else{
                json = "{success: false, errors:{reason:\'Impossible de trouver "+idSousStructure+"\'}}";
            }
                     
            
            return json;            
            //return "{success: false, errors:{reason:\'"+msg+"\'}};
        }
        
        @Override
        public String deleteItem(){

   
            this.response.setContentType("application/json");
            
            String json = "{success: false}";
            String msg = "";
            int idSousStructure = 0;
            boolean success = true;            
            try{
                idSousStructure= Integer.parseInt(request.getParameter("id"));
            }catch(NumberFormatException ex){
                success = false;
            }
                                    
            if (idSousStructure>= 0){
                    try{
                        SousStructureDAO dao = new SousStructureDAO();
                        dao.destroy(idSousStructure);
                    
                    } catch (NonexistentEntityException ex) {
                            Logger.getLogger(SousStructureController.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (Exception ex) {
                        // je gère les erreurs de suppression
                            String message = "";
                            try{
                                message = ex.getCause().getCause().getMessage();
                                String s = "Cannot delete or update a parent row: a foreign key constraint fails (`gpod`.`";
                                message = message.substring(s.length());
                                int apo = message.indexOf("`");
                                message = message.substring(0, apo);   
                            }catch(NullPointerException e2){                                      
                            }
                                                        
                            
                            if (!"".equals(message)){
                                msg = "Impossible de supprimer cette ligne, elle est utilisée par un objet \""+message+"\"";
                            }else{
                                msg = "Impossible de supprimer cette ligne, Erreur logiciel !!";                                
                                Logger.getLogger(ActionController.class.getName()).log(Level.SEVERE, null, ex);
                            }
                            success = false;       
                    }
               
            }else{
                success = false;
                msg = "Sous Structure " + idSousStructure + " inconnu";
            }
            
            
            if (success){
                json = "{success: true}";            
            }else{
                this.response.setStatus(424);
                json = "{success: false, errors:{reason:\'"+msg+"\'}}";
            }
            return json;
        } 
        

        protected List<SousStructure> getListSousStructure(String type){
            List<SousStructure> sousStructureList = null;
            int idStructure=0;
            int idSousStructure=0;
            
            // ------------------------------------------------------------------
            int maxResults = -1;
            int firstResult = -1;
            try{
                maxResults = Integer.parseInt(request.getParameter("limit"));
                firstResult = Integer.parseInt(request.getParameter("start"));
                if (maxResults == 0){
                    maxResults=10;
                }
            }catch(NumberFormatException e){
            }
            String searchValue = request.getParameter("query");
            String searchColumns = request.getParameter("fields");
            // ------------------------------------------------------------------         
            try{
                idStructure = Integer.parseInt(request.getParameter("idStructure"));
            }catch(NumberFormatException e){
                idStructure = 0;
            }

            try{
                idSousStructure = Integer.parseInt(request.getParameter("id"));
            }catch(NumberFormatException e){
                idSousStructure = 0;
            }

            try {

                 if (idSousStructure>0){
                     SousStructure ss =  this.getItemFromId();
                     sousStructureList = new ArrayList<SousStructure>();
                     sousStructureList.add(ss);
                 }else if (idStructure>0){
                    StructureDAO dao = new StructureDAO();
                    Structure structure = dao.findStructure(idStructure);
                    sousStructureList = structure.getSousStructure();

                }else{
                    SousStructureDAO dao = new SousStructureDAO();
                    sousStructureList = dao.findSousStructureEntities(searchValue, searchColumns, maxResults, firstResult, type);
                 }
            } catch (Exception ex) {
                Logger.getLogger(SousStructureController.class.getName()).log(Level.SEVERE, null, ex);
            }            
            
            return sousStructureList;
                
        }
        
        protected int getListStructureCount(String type){
            
            int idStructure=0;      
            // ------------------------------------------------------------------
            String searchValue = request.getParameter("query");
            String searchColumns = request.getParameter("fields");
            // ------------------------------------------------------------------         
            try{
                idStructure = Integer.parseInt(request.getParameter("idStructure"));
            }catch(NumberFormatException e){
                idStructure = 0;
            }
            
            int nbItems = 0;
            try {                
                 if (idStructure>0){
                    StructureDAO dao = new StructureDAO();
                    Structure structure = dao.findStructure(idStructure);
                    if (structure !=null && structure.getSousStructure()!= null){
                        nbItems = structure.getSousStructure().size();
                    }
                }else{
                    SousStructureDAO dao = new SousStructureDAO();
                    nbItems = dao.getSousStructureCount(searchValue, searchColumns, type);
                 }
            } catch (Exception ex) {
                Logger.getLogger(SousStructureController.class.getName()).log(Level.SEVERE, null, ex);
            }            
            
            return nbItems;
                
        }  
        
        // Methode utilisée pour les édition XLS et PDF
        // retourne l'item à partir de l'id. Si l'id n'est pas définit, on a demandé l'impression de la liste (voir méthode getXLSList()
        protected SousStructure getItemFromId(){

                SousStructure sousStructure = null;
                Integer idSousStructure = 0;
                String idSousStructureS = request.getParameter("id");

                if (idSousStructureS!=null && !"".equals(idSousStructureS)){
                    try{
                        idSousStructure = Integer.parseInt(idSousStructureS);
                        if (idSousStructure > 0 ){
                            try {
                                SousStructureDAO dao = new SousStructureDAO();
                                sousStructure = dao.findSousStructure(idSousStructure);
                            } catch (Exception ex) {
                                Logger.getLogger(SousStructureController.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }
                    }catch(NumberFormatException ex){
                        Logger.getLogger(SousStructureController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                return sousStructure;

        }           
        
        
        public String getPDF(){
             //----------------------------------------------------
            //Autorisations en lecture
            String resp = authorizedProfils.getReadAuthorizedJsonTicket();
            if (!"".equals(resp)){
                return resp;
            }
            //----------------------------------------------------   
            
            String json="{'success':false}"; 
            SousStructure sousStructure = this.getItemFromId();

        
            PdfDocumentFactory pdf = new PdfDocumentFactory("sStructure_" + sousStructure.getIdSousStructure());

            pdf.addVerticalTable(sousStructure.getNom(), 24, sousStructure.getDataForVerticalTable(), 0);       

            pdf.getDocument().close();     

            if (!"".equals(pdf.getFileName())){
                return "{'success':true,'fileName':'"+pdf.getFileName()+"'}";
            }else{
                return json;   
            }              
        }
        
       
        
        protected String getXLSList(String type){

            String fileName = "";
            String json="{'success':false}"; 
            
            List<SousStructure> sousStructureList = null;
            
            // on cherche si on demander l'édition d'un seul utilisateur id<>0
            SousStructure sousStructure = this.getItemFromId();
            //si utilisateur = null => c'est que id=0 car on a demander la liste de tous les utilisateurs
            if (sousStructure==null){                
                sousStructureList = this.getListSousStructure(type);
            }else{
                sousStructureList = new ArrayList<SousStructure>();
                sousStructureList.add(sousStructure);
            }
            
             try {

                DateTimeFormatter FILE_FORMAT = DateTimeFormat.forPattern("yyyyMMddHmsS");
                DateTimeFormatter USER_FORMAT = DateTimeFormat.forPattern("H:m dd/MM/yyyy");

                LocalDateTime now = new LocalDateTime();
                String dateStr = now.toString(FILE_FORMAT);


                fileName = "sStructure_"+dateStr+".xls";

                WritableWorkbook wrtWorkbook = Workbook.createWorkbook(new File(fileName));

                WritableSheet sheet1;

                sheet1 = wrtWorkbook.createSheet("sousStructures", 0);

                Label titre = new Label(0, 0, "Equipes/Départements, le " + now.toString(USER_FORMAT));
                sheet1.addCell(titre);

                sheet1.addCell(new Label(0, 2, "Nom"));
                sheet1.addCell(new Label(1, 2, "Unité/Département"));
                sheet1.addCell(new Label(2, 2, "Code"));
                sheet1.addCell(new Label(3, 2, "Adresse"));
                sheet1.addCell(new Label(4, 2, "Complement"));
                sheet1.addCell(new Label(5, 2, "Code Postal"));
                sheet1.addCell(new Label(6, 2, "Ville"));       
                sheet1.addCell(new Label(7, 2, "Tel"));       
                sheet1.addCell(new Label(8, 2, "Fax"));       
                sheet1.addCell(new Label(9, 2, "Responsable"));  
                
                    
                int j=3; // pour la ligne
                for (SousStructure u : sousStructureList){
                    sheet1.addCell(new Label(0, j, u.getNom()));
                    sheet1.addCell(new Label(1, j, u.getStructure().getNom()));
                    sheet1.addCell(new Label(2, j, u.getCode()));
                    sheet1.addCell(new Label(3, j, u.getAdresse1()));
                    sheet1.addCell(new Label(4, j, u.getAdresse2()));
                    sheet1.addCell(new Label(5, j, u.getCodePostal()));
                    sheet1.addCell(new Label(6, j, u.getVille()));
                    sheet1.addCell(new Label(7, j, u.getTelephone()));
                    sheet1.addCell(new Label(8, j, u.getFax()));
                    sheet1.addCell(new Label(9, j, u.getResponsable()));  
                    j++; //on change de ligne
                }
             
                wrtWorkbook.write();
                wrtWorkbook.close();

                json = "{'success':true,'fileName':'"+fileName+"'}";


            } catch (IOException ex) {
                Logger.getLogger(SousStructureController.class.getName()).log(Level.SEVERE, null, ex);
            } catch (RowsExceededException ex) {
                Logger.getLogger(SousStructureController.class.getName()).log(Level.SEVERE, null, ex);
            } catch (WriteException ex) {
                Logger.getLogger(SousStructureController.class.getName()).log(Level.SEVERE, null, ex);
            }  

            return json;

        }         
        
        
}
