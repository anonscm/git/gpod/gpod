/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2012/01/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/

package fr.univtours.gpod.Controller.stocks;

import fr.univtours.gpod.persistance.entities.AireStockage;
import fr.univtours.gpod.persistance.entities.ProduitConditionnement;
import fr.univtours.gpod.persistance.entities.SousStructure;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Cette classe représente le stock et permet de récupérer la liste des articles en stock
 * @author geoffroy.vibrac
 */
public class Stock {
    // Propriétés du stock
    private String identifiantEtiquette;
    private SousStructure sousStructure;
    private AireStockage aire;
    private ProduitConditionnement produitConditionnement;
    private Integer  idArticle;
    private Double quantite;
    private Date dateOuverture;
    private Integer idClickeyScan;

    /**
    * getListStock permet de récupérer une List d'objet Stock à partir d'une list d'Objet recupérer d'une requete JPQL
    */
    public static List<Stock> getListStock(List<Object[]> listArticles){
        List<Stock> stocks = new ArrayList<Stock>();
        if (listArticles!=null && listArticles.size()>0){
              for (Object[] o : listArticles){
                  Stock s = new Stock();
                  if (o[0] instanceof Long){
                      s.idArticle = (Integer)0;
                  }else{
                      s.idArticle = (Integer)o[0];
                  }
                  s.produitConditionnement = (ProduitConditionnement)o[1];
                  s.aire = (AireStockage)o[2];
                  s.sousStructure = (SousStructure)o[3];
                  s.quantite = (Double)o[4];
                  // si on a fait un regroupement des articles par produit
                  // on n'affiche pas les étiquette et la requette ne renvoye que 5 valeurs => ArrayIndexOutOfBoundsException
                  try {
                    s.identifiantEtiquette = (String)o[5];
                  }catch(ArrayIndexOutOfBoundsException e){
                    s.identifiantEtiquette = "";
                  }
                  try {
                    s.dateOuverture = (Date)o[6];
                  }catch(ArrayIndexOutOfBoundsException e){
                    s.dateOuverture = null;
                  }
                  try {
                    s.idClickeyScan = (Integer)o[7];
                  }catch(ArrayIndexOutOfBoundsException | ClassCastException e){
                    s.idClickeyScan = 0;
                  }                    
                  //if (s.quantite > 0){
                    stocks.add(s);
                  //}
              }
        }
        return stocks;
    }

    public AireStockage getAire() {
        return aire;
    }

    public void setAire(AireStockage aire) {
        this.aire = aire;
    }

    public Integer getIdArticle() {
        return idArticle;
    }

    public void setIdArticle(Integer idArticle) {
        this.idArticle = idArticle;
    }

    public String getIdentifiantEtiquette() {
        return identifiantEtiquette;
    }

    public void setIdentifiantEtiquette(String identifiantEtiquette) {
        this.identifiantEtiquette = identifiantEtiquette;
    }

    public ProduitConditionnement getProduitConditionnement() {
        return produitConditionnement;
    }

    public void setProduitConditionnement(ProduitConditionnement produitConditionnement) {
        this.produitConditionnement = produitConditionnement;
    }

    public Double getQuantite() {
        return quantite;
    }

    public void setQuantite(Double quantite) {
        this.quantite = quantite;
    }

    public SousStructure getSousStructure() {
        return sousStructure;
    }

    public void setSousStructure(SousStructure sousStructure) {
        this.sousStructure = sousStructure;
    }

    public Date getDateOuverture() {
        return dateOuverture;
    }

    public void setDateOuverture(Date dateOuverture) {
        this.dateOuverture = dateOuverture;
    }

    public Integer getIdClickeyScan() {
        return idClickeyScan;
    }

    public void setIdClickeyScan(Integer idClickeyScan) {
        this.idClickeyScan = idClickeyScan;
    }


    

}
