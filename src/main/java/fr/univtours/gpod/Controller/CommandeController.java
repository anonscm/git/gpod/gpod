/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2012/01/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/
package fr.univtours.gpod.Controller;

import fr.univtours.gpod.HTML.HTML;
import fr.univtours.gpod.exceptions.DuplicateIdentifiantEtiquetteException;
import fr.univtours.gpod.exceptions.IllegalFournisseurException;
import fr.univtours.gpod.exceptions.IllegalOrphanException;
import fr.univtours.gpod.exceptions.RollbackFailureException;
import fr.univtours.gpod.json.JsonConversionUtilities;
import fr.univtours.gpod.persistance.DAO.CommandeDAO;
import fr.univtours.gpod.persistance.DAO.LivraisonDAO;
import fr.univtours.gpod.persistance.DAO.SousStructureDAO;
import fr.univtours.gpod.persistance.DAO.StatutDAO;
import fr.univtours.gpod.persistance.DAO.exceptions.NonexistentEntityException;
import fr.univtours.gpod.persistance.entities.*;
import fr.univtours.gpod.utils.AuthorizedProfils;
import fr.univtours.gpod.utils.Mail;
import fr.univtours.gpod.utils.Utilities;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;
import org.apache.commons.lang.StringEscapeUtils;
import org.joda.time.DateTime;
import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;


/**
 *
 * @author geoffroy.vibrac
 */
public class CommandeController extends ActionController {



        public CommandeController(HttpServletRequest request, HttpServletResponse response, HttpSession session){
            super(request, response, session);
            authorizedProfils = new AuthorizedProfils(request, response, session);
            authorizedProfils.addReadAuthorisedProfile("RA","AD", "LA", "SE");
            authorizedProfils.addWriteAuthorisedProfile("AD", "LA", "SE");
        }

        public String getHtml(){
            //----------------------------------------------------
            //Autorisations         
            String resp = authorizedProfils.getAuthorizedHtmlTicket();
            if (!"".equals(resp)){
                return resp;
            }
            //----------------------------------------------------  


                response.setContentType("text/html;charset=iso-8859-1");

                HTML html = HTML.getInstance();
                html.getHeader().setTitle("Gestion des produits dangereux &gt; Gestion des commandes");
                html.setExtjsHeader();
                html.getHeader().getCss().addLine("lib/multiselect/Multiselect.css");
                html.getHeader().getCss().addLine("lib/search/gridsearch.css");
                html.getHeader().getCss().addLine("lib/msg/msg.css");
                
                html.getHeader().getJs().addBlock("var B_nameUser = '"+Utilities.addSlashes(utilisateur.getNom())+"'");
                html.getHeader().getJs().addBlock("var B_lastCx = '"+Utilities.javaDateTIMEToString(utilisateur.getDateAvantDerniereConnexion())+"'");
                
                html.getHeader().getJs().addLine("version.js");
                html.getHeader().getJs().addLine("lib/printAction.js");
                html.getHeader().getJs().addLine("lib/periodButtons.js");
                html.getHeader().getJs().addLine("lib/help/helpPlugin.js");
                html.getHeader().getJs().addLine("lib/msg/msg.js");
                html.getHeader().getJs().addLine("lib/multiselect/Multiselect.js");
                html.getHeader().getJs().addLine("lib/multiselect/ItemSelector.js");
                html.getHeader().getJs().addLine("lib/search/gridSearch.js");
                html.getHeader().getJs().addLine("ui/search/SousStructure.js");
                html.getHeader().getJs().addLine("ui/search/AireStockage.js");
                html.getHeader().getJs().addLine("ui/windowEntreeEnStock.js");
                html.getHeader().getJs().addLine("ui/windowApplet.js");
                html.getHeader().getJs().addLine("ui/addProduit.js");
                 html.getHeader().getJs().addLine("lib/excelButton.js");
                html.getHeader().getJs().addLine("ui/container.Commande.js");
                html.getHeader().getJs().addLine("ui/menus/menu."+ session.getAttribute("profil") +".js");
                html.getHeader().getJs().addLine("ui/squelette.js");
                html.getBody().setBodyElement("<table align='center' class='contenttable' cellpadding='0' cellspacing='0' border='0'><tr><td id='centerPanel'></td></tr></table>");

                html.getBody().setBodyElement("<div id='iframediv'>");
                

                return html.getHTMLCode();
        }



       public String supprimeCommandeLigne(){
            this.response.setContentType("application/json");
            String json = "{success: false, errors:{reason:\'Erreur serveur\'}}";
            CommandeLigne cl = null;
            String idCommandeLigneS = request.getParameter("idCommandeLigne");           
            Integer idCommandeLigne=0;
            CommandeDAO dao = new CommandeDAO();    
            try{
                idCommandeLigne = Integer.parseInt(idCommandeLigneS);
            }catch(NumberFormatException e){
                 json = "{success: false, errors:{reason:\'idCommandeLigne incorrect : "+idCommandeLigneS+"\'}}";
                 return json;
            }     
            if (idCommandeLigne > 0){   
                 cl = dao.findCommandeLigne(idCommandeLigne);  
                 if (cl==null){
                    json = "{success: false, errors:{reason:\'Impossible de trouver cette ligne de commande : "+idCommandeLigne+"\'}}";
                    return json;
                 }
            }
            
            // si la commande est ouverte (statut ==1) => on supprime la ligne
            // Sinon, on met à jour le stock (c'est une annulation de livraison)
            if (cl.getCommande().getStatut().getIdStatut()==1){
                try {
                    dao.destroyCommandeLigne(idCommandeLigne);
                    json = "{success: true}";
                } catch (RollbackFailureException | NonexistentEntityException | IllegalOrphanException ex) {
                    Logger.getLogger(CommandeController.class.getName()).log(Level.SEVERE, null, ex);
                    json = "{success: false, errors:{reason:\'"+ex.getMessage()+"\'}}";
                } catch (Exception ex) {
                    Logger.getLogger(CommandeController.class.getName()).log(Level.SEVERE, null, ex);
                    json = "{success: false, errors:{reason:\'"+ex.getMessage()+"\'}}";                  
                }      
            // si la commande est en attente de livraison, on annule la ligne livraison
            }else if (cl.getCommande().getStatut().getIdStatut()==3){
                Commande commande = cl.getCommande();

                int quantiteCommandee = dao.getCommandeLigneQuantiteCommandee(cl);
                int quantiteRecu = dao.getCommandeLigneQuantiteRecu(cl);
                int quantiteResteALivrer = quantiteCommandee - quantiteRecu;

                try{
                    LivraisonLigne ll = new LivraisonLigne(quantiteResteALivrer, false, cl.getProduitConditionnement());
                    List<LivraisonLigne> livraisonLigneList = new ArrayList<>();
                    livraisonLigneList.add(ll);
                
                    Livraison livraison = new Livraison(new Date(), "ANNULE", commande, utilisateur, livraisonLigneList, null);
                    LivraisonDAO daoL = new LivraisonDAO();


                    daoL.create(livraison);
                    commande.getLivraisonList().add(livraison);

                    if (dao.getCommandeQuantiteCommandee(commande)-dao.getCommandeQuantiteRecu(commande)==0){
                        Statut statut = new StatutDAO().findStatut(4);
                        commande.setStatut(statut);
                    }
                    dao.edit(commande);
                    json = "{success: true}";



                } catch (RollbackFailureException | NonexistentEntityException ex) {
                    ex.printStackTrace();
                    json = "{success: false, errors:{reason:\'La commande doit être ouverte\'}}";
                    return json;
                }catch(Exception ex){
                    ex.printStackTrace();
                    json = "{success: false, errors:{reason:\'La commande doit être ouverte\'}}";
                    return json;                          
                }
                
                
                
            }else{
                    json = "{success: false, errors:{reason:\'La commande doit être ouverte\'}}";
                    return json;      
            }
            

            
            
            return json;
        }
        
        
        public String saveItem() {
            this.response.setContentType("application/json");
            //----------------------------------------------------
            //Autorisations         
            String resp = authorizedProfils.getWriteAuthorizedJsonTicket();
            if (!"".equals(resp)){
                return resp;
            }
            //----------------------------------------------------              
            
            
            String json = "{success: false, errors:{reason:\'Enregistrement impossible\'}}";
            CommandeDAO dao = null;

            

            // --------------------------------------------------------------
            // <editor-fold defaultstate="expanded" desc="recup champs formulaire">
            // 1 . RECUPERATION DES CHAMPS DE FORMULAIRE
            //      i. recupération brute (String)
            //      ii. mise en forme (cast) des champs

            // i. Récupération brute des champs
            // onglet 1
            String idCommandeS = request.getParameter("idCommande");
            String idSousStructureS = request.getParameter("idSousStructure");
            String dateCommandeS = request.getParameter("dateCommande");
            boolean cloture;
            try{
                cloture = Boolean.parseBoolean(request.getParameter("cloture"));
            }catch(NumberFormatException e){
                cloture = false;
            }
            String commandeLigne = request.getParameter("commandeLigne");
            List<CommandeLigne> commandeLigneList = null;
            
            // --
            String dateCommandeInterneS = request.getParameter("dateCommandeInterne");           
            String numBonDeCommandeInterne = request.getParameter("numBonDeCommandeInterne");
            boolean commandeEnvoyee;
            try{
                commandeEnvoyee = Boolean.parseBoolean(request.getParameter("commandeEnvoyee"));
            }catch(NumberFormatException e){
                commandeEnvoyee = false;
            }            
            
            // Onglet 2
            String dateLivraisonS = request.getParameter("dateLivraison");
            String numeroBonLivraison = request.getParameter("numeroBonLivraison");
            String livraisonLigne = request.getParameter("livraisonLigne");
            List<LivraisonLigne> livraisonLigneList = null;
            String articles = request.getParameter("stockLigne");
            List<Article> articleList = null;

            // ii. Mise en forme des champs
            Statut statut = null;
            SousStructure sousStructure = null;

            
            // onglet 1
            Integer idCommande;
            try{
                idCommande = Integer.parseInt(idCommandeS);
            }catch(NumberFormatException e){
                idCommande = 0;
            }


            Integer idSousStructure;
            try{
                idSousStructure = Integer.parseInt(idSousStructureS);
                if (idSousStructure > 0 ){
                    try {
                        SousStructureDAO daob = new SousStructureDAO();
                        sousStructure = daob.findSousStructure(idSousStructure);

                    } catch (Exception ex) {
                        Logger.getLogger(CommandeController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }catch(NumberFormatException e){
                idSousStructure = 0;
            }catch(NullPointerException ex){
                idSousStructure=-1;
            }
            
            DateTime dateCommande = null;
            // la date ne doit pas être null et doit faire 10 de longueur = jj/mm/aaaa
            if (dateCommandeS != null && dateCommandeS.length() == 10){
                try{
                    int year = Integer.parseInt(dateCommandeS.substring(6));
                    int month = Integer.parseInt(dateCommandeS.substring(3, 5));
                    int day = Integer.parseInt(dateCommandeS.substring(0, 2));
                    dateCommande = new DateTime(year, month, day,0,0,0,0);
                }catch(NumberFormatException ex){
                   Logger.getLogger(CommandeController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }    
            
            try {
                commandeLigneList =  JsonConversionUtilities.jsonCommandeLigneArrayToList(commandeLigne);
            } catch (IllegalFournisseurException ex) {
                json = "{success: false, errors:{reason:\'"+ex.getMessage()+"'}}";
                return json;
            }
            
            livraisonLigneList = JsonConversionUtilities.jsonLivraisonLigneArrayToList(livraisonLigne);

            try{
                articleList = JsonConversionUtilities.jsonArticleArrayToList(articles, idSousStructure);
            }catch (DuplicateIdentifiantEtiquetteException ex){
                json = "{success: false, errors:{reason:\'"+ex.getMessage()+"'}}";
                return json;
            }

            DateTime dateCommandeInterne = null;
            // la date ne doit pas être null et doit faire 10 de longueur = jj/mm/aaaa
            if (dateCommandeInterneS != null && dateCommandeInterneS.length() == 10){
                try{
                    int year = Integer.parseInt(dateCommandeInterneS.substring(6));
                    int month = Integer.parseInt(dateCommandeInterneS.substring(3, 5));
                    int day = Integer.parseInt(dateCommandeInterneS.substring(0, 2));
                    dateCommandeInterne = new DateTime(year, month, day,0,0,0,0);
                }catch(NumberFormatException ex){
                   Logger.getLogger(CommandeController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }


            // livraison
            DateTime dateLivraison = null;
            // la date ne doit pas être null et doit faire 10 de longueur = jj/mm/aaaa
            if (dateLivraisonS != null && dateLivraisonS.length() == 10){
                try{
                    int year = Integer.parseInt(dateLivraisonS.substring(6));
                    int month = Integer.parseInt(dateLivraisonS.substring(3, 5));
                    int day = Integer.parseInt(dateLivraisonS.substring(0, 2));
                    dateLivraison = new DateTime(year, month, day,0,0,0,0);
                }catch(NumberFormatException ex){
                   Logger.getLogger(CommandeController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }


            
            // Statut 1 : Nouvelle commande
            StatutDAO  daob = null;
            try {
                daob = new StatutDAO();
            } catch (Exception ex) {
                Logger.getLogger(CommandeController.class.getName()).log(Level.SEVERE, null, ex);
            }            
            
            // 1 . FIN RECUPERATION DES CHAMPS DE FORMULAIRE
            // </editor-fold>
            // --------------------------------------------------------------

            // --------------------------------------------------------------
            // <editor-fold defaultstate="expanded" desc="connexion DAO">
            try{
                 dao = new CommandeDAO();
            } catch (Exception ex) {
                Logger.getLogger(CommandeController.class.getName()).log(Level.SEVERE, null, ex);
                json = "{success: false, errors:{reason:\'"+ex.getMessage()+"\'}}";
                return json;
            }
            // </editor-fold>
            // --------------------------------------------------------------


            // --------------------------------------------------------------
            // <editor-fold defaultstate="expanded" desc="traitement commande">
            if (idCommande == 0){
                    // --------------------------------------------------------------
                    // <editor-fold defaultstate="expanded" desc="Nouvelle commande">
                    // idCommande = 0 => nouvelle commande

                
           
                
                    // Test des champs
                    if (commandeLigneList == null || commandeLigneList.size()==0 || sousStructure == null){
                        json = "{success: false, errors:{reason:\'Des champs obligatoires n\\\'ont pas été saisis\'}}";
                        return json;
                    }

                    // Si la case cloturer est cochée => on passe à l'étape 1 : attente de bon de commande
                    if (cloture){
                        statut = daob.findStatut(2);                        
                    }else{
                        statut = daob.findStatut(1);
                    }

                    try{
                        Mail mail = new Mail();
                        String messageHTML = "<p>Vous venez de créer une demande de commande et/ou de devis pour les Produits suivant : </p>";
                        messageHTML+= "<p>Structure : " +sousStructure.getNom() + "</p></br>";
                        messageHTML+="<table border=1 cellpadding=0 cellspacing=0><tr><td>CAS</td><td>Produit</td><td>Fournisseur</td><td>Référence Four.</td><td>Qté</td><td>Contenance</td></tr>";
                        for (CommandeLigne cl : commandeLigneList){
                            messageHTML+="<tr>";
                            messageHTML+="<td>" + cl.getProduitConditionnement().getProduit().getSubstance().getCas() + "</td>";      
                            messageHTML+="<td>" + cl.getProduitConditionnement().getProduit().getNom() + "</td>";
                            messageHTML+="<td>" + cl.getProduitConditionnement().getProduit().getFournisseur().getNom() + "("+
                                    cl.getProduitConditionnement().getProduit().getFournisseur().getReferenceSIFAC()+")</td>";
                            messageHTML+="<td>" + cl.getProduitConditionnement().getReferenceFournisseur() + "</td>";
                            messageHTML+="<td>" + cl.getQuantite() + "</td>";
                            messageHTML+="<td>" + cl.getProduitConditionnement().getContenance() + " " + cl.getProduitConditionnement().getUniteMesure().getAbreviation()  +  "</td>";                                    
                            messageHTML+="</tr>";
                        }
                        messageHTML+="</table>";
                        mail.sendMail(utilisateur.getEmail(), "[gpod] Demande de commande/Devis", messageHTML);
                    }catch(Exception ex){
                            Logger.getLogger(CommandeController.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    Commande commande = new Commande(dateCommande, statut, this.getUtilisateur(), sousStructure, commandeLigneList);
                    if (commande !=null){
                        try{
                            dao.create(commande);
                            json = "{success: true}";
                        } catch (Exception ex) {
                            Logger.getLogger(CommandeController.class.getName()).log(Level.SEVERE, null, ex);
                            json = "{success: false, errors:{reason:\'"+ex.getMessage()+"\'}}";
                        }
                    }
                    // Fin nouvelle commande
                    // </editor-fold>
                    // --------------------------------------------------------------
            }else{
                    // --------------------------------------------------------------
                    // <editor-fold defaultstate="expanded" desc="Mise à jour commande">
                    Commande commande = dao.findCommande(idCommande);
                    if (commande !=null){
                        // je récupère le statut actuel de la commande car en fonction de cet état les étapes seront différentes
                        // rappel : 1. Attente commande
                        // 2. attente livraison
                        // 3. livrée
                        // 4. annulée
                        int statutCourant = commande.getStatut().getIdStatut();
                           
                        
                        switch (statutCourant){
                            // --------------------------------------------------------------
                            // <editor-fold defaultstate="expanded" desc="cas 1 :Commande en attente de bon de commande">
                            // la commande est ouverte
                            // i. on peut modifier les informations: produits / Equipe,département / Date de commande 
                            // ii. on peut aussi la faire passe à l'étape 1 en la cloturant                           
                            case 1:

                                if (dateCommande != null){
                                    commande.setDateCommande(dateCommande.toDate());
                                }
                                if (sousStructure!= null){
                                    commande.setSousStructure(sousStructure);
                                }          
                                if (commandeLigneList!=null){
                                    commande.setCommandeLigneList(commandeLigneList);
                                }
                                
                                // Si la case cloturer est cochée => on passe à l'étape 2 : attente de bon de commande
                                if (cloture){                                    
                                    statut = daob.findStatut(2);
                                }else{
                                    statut = daob.findStatut(1);
                                }  
                                
                                try{
                                    Mail mail = new Mail();
                                    String messageHTML = "<p>Vous venez de créer/modifier une demande de commande et/ou de devis pour les Produits suivant : </p>";
                                    messageHTML+= "<p>Date : " + Utilities.javaDateToString(commande.getDateCommande()) + "</p>";
                                    messageHTML+= "<p>Structure : " +commande.getSousStructure().getNom() + "</p></br>";
                                    messageHTML+="<table border=1 cellpadding=0 cellspacing=0><tr><td>CAS</td><td>Produit</td><td>Fournisseur</td><td>Référence Four.</td><td>Qté</td><td>Contenance</td></tr>";
                                    for (CommandeLigne cl : commande.getCommandeLigneList()){
                                        messageHTML+="<tr>";
                                        messageHTML+="<td>" + cl.getProduitConditionnement().getProduit().getSubstance().getCas() + "</td>";      
                                        messageHTML+="<td>" + cl.getProduitConditionnement().getProduit().getNom() + "</td>";
                                        messageHTML+="<td>" + cl.getProduitConditionnement().getProduit().getFournisseur().getNom() + "("+
                                                cl.getProduitConditionnement().getProduit().getFournisseur().getReferenceSIFAC()+")</td>";
                                        messageHTML+="<td>" + cl.getProduitConditionnement().getReferenceFournisseur() + "</td>";
                                        messageHTML+="<td>" + cl.getQuantite() + "</td>";
                                        messageHTML+="<td>" + cl.getProduitConditionnement().getContenance() + " " + cl.getProduitConditionnement().getUniteMesure().getAbreviation()  +  "</td>";                                    
                                        messageHTML+="</tr>";
                                    }
                                    messageHTML+="</table>";
                                    mail.sendMail(utilisateur.getEmail(), "[gpod] Demande de commande/Devis", messageHTML);                                  
                                }catch(Exception ex){
                                     Logger.getLogger(CommandeController.class.getName()).log(Level.SEVERE, null, ex);
                                }
                                
                                commande.setStatut(statut);
                             
                            
                            break;
                            // </editor-fold>
                            // --------------------------------------------------------------                                
                                
                            // --------------------------------------------------------------
                            // <editor-fold defaultstate="expanded" desc="cas 1 :Commande en attente de bon de commande">
                            // la commande est en attente de bon commande
                            // ii. on peut la faire passe à l'étape 2 en saisissant numero de commande SIFAC / Date commande SIFAC
                            case 2:

                                // Passage à l'étape2 seulement si un BonDeCommande est saisi
                                if (numBonDeCommandeInterne!=null && !"".equals(numBonDeCommandeInterne)){
                                    commande.setNumBonDeCommandeInterne(numBonDeCommandeInterne);
                                    if (dateCommandeInterne != null){
                                        commande.setDateCommandeInterne(dateCommandeInterne.toDate());
                                    }
                                }
                                                               
                                if (commandeEnvoyee){
                                    
                                    
                                    Mail mail = new Mail();
                                    String messageHTML = "<p>Votre commande est maintenant en attente de livraison</p>";
                                    messageHTML+= "<p>Date Commande : " + Utilities.javaDateToString(commande.getDateCommande()) + "</p>";
                                    messageHTML+= "<p>Structure : " +commande.getSousStructure().getNom() + "</p>";                                    
                                    messageHTML+= "<p>No. SIFAC : " + commande.getNumBonDeCommandeInterne() + "</p>";
                                    messageHTML+= "<p>Date Saisie SIFAC : " + Utilities.javaDateToString(commande.getDateCommandeInterne()) + "</p>";
                                    messageHTML+="<table border=1 cellpadding=0 cellspacing=0><tr><td>CAS</td><td>Produit</td><td>Fournisseur</td><td>Référence Four.</td><td>Qté</td><td>Contenance</td></tr>";
                                    for (CommandeLigne cl : commande.getCommandeLigneList()){
                                        messageHTML+="<tr>";
                                        messageHTML+="<td>" + cl.getProduitConditionnement().getProduit().getSubstance().getCas() + "</td>";      
                                        messageHTML+="<td>" + cl.getProduitConditionnement().getProduit().getNom() + "</td>";
                                        messageHTML+="<td>" + cl.getProduitConditionnement().getProduit().getFournisseur().getNom() + "("+
                                                cl.getProduitConditionnement().getProduit().getFournisseur().getReferenceSIFAC()+")</td>";
                                        messageHTML+="<td>" + cl.getProduitConditionnement().getReferenceFournisseur() + "</td>";
                                        messageHTML+="<td>" + cl.getQuantite() + "</td>";
                                        messageHTML+="<td>" + cl.getProduitConditionnement().getContenance() + " " + cl.getProduitConditionnement().getUniteMesure().getAbreviation()  +  "</td>";                                    
                                        messageHTML+="</tr>";
                                    }
                                    messageHTML+="</table>";
                                    mail.addCc(utilisateur.getEmail());
                                    mail.sendMail(commande.getUtilisateur().getEmail(), "[gpod] Validation de commande", messageHTML);                                      
                                    
                                    statut = daob.findStatut(3);
                                }else{
                                    statut = daob.findStatut(2);
                                }   
                                
                                commande.setStatut(statut);
                                

                                break;
                            // </editor-fold>
                            // --------------------------------------------------------------
                                
                            // --------------------------------------------------------------
                            // <editor-fold defaultstate="expanded" desc="cas 2:Commande en attente de livraison">
                            // la commande est en attente de livraison
                            // iii. on peut aussi la faire passe à l'étape 3 en saisissant un numero de bon de livraison et des articles livrés
                            case 3:
                                // iii. on peut aussi la faire passer à l'étape 3 en saisissant un numero de bon de livraison et des articles livrés
                                if (numeroBonLivraison!=null && !"".equals(numeroBonLivraison) && dateLivraison!=null 
                                        && articleList!=null && articleList.size()>0
                                        && livraisonLigneList!=null && livraisonLigneList.size()>0){
                                            Livraison livraison = new Livraison(dateLivraison.toDate(), numeroBonLivraison, 
                                                    commande, this.utilisateur, livraisonLigneList, articleList);    
                                            LivraisonDAO daoL = new LivraisonDAO();
                                            try {
                                                daoL.create(livraison);
                                                commande.getLivraisonList().add(livraison);  
                                                
                                                if (dao.getCommandeQuantiteCommandee(commande)-dao.getCommandeQuantiteRecu(commande)==0){
                                                    statut = daob.findStatut(4);
                                                    commande.setStatut(statut);
                                                }                                                    
                                                
                                            } catch (NonexistentEntityException ex) {
                                                ex.printStackTrace();
                                            } catch (RollbackFailureException ex) {
                                                ex.printStackTrace();
                                            } catch (Exception ex) {
                                                ex.printStackTrace();
                                            }
                                }

                                break;
                            // </editor-fold>
                            // --------------------------------------------------------------
                            case 4:


                                break;
                        }



                        try{
                            dao.edit(commande);
                                                    
                            
                            json = "{success: true}";
                            
                        }catch(Exception ex) {
                            Logger.getLogger(CommandeController.class.getName()).log(Level.SEVERE, null, ex);
                            json = "{success: false, errors:{reason:\'"+ex.getMessage()+"\'}}";
                        }
                    }
                    // Fin mise à jour commande
                    // </editor-fold>
                    // --------------------------------------------------------------
            }
            // FIn traitement commande
            // </editor-fold>
            // --------------------------------------------------------------



             return json;
            //return "{success: false, errors:{reason:\'"+msg+"\'}};
        }
        
        // permet de récupérer la liste des ProduitConditionnement commandés pour une commande
        public List<CommandeLigne> getCommandeLigneList(){
            Commande commande = null;   
            List<CommandeLigne> commandeLigneList = new ArrayList<CommandeLigne>();            
            String idCommandeS = request.getParameter("idCommande");
            Integer idCommande;
            try{
                idCommande = Integer.parseInt(idCommandeS);
            }catch(NumberFormatException e){
                idCommande = 0;
            }
            
            try {
                if (idCommande > 0){
                    CommandeDAO dao = new CommandeDAO();
                    if (dao!=null){
                        commande = dao.findCommande(idCommande);
                    }
                }
                if (commande!=null){
                        commandeLigneList = commande.getCommandeLigneList();
                }
            } catch (Exception ex) {
                Logger.getLogger(CommandeController.class.getName()).log(Level.SEVERE, null, ex);
            }       
                       
            return commandeLigneList;
        }
        
        public String getJsonCommandeLigne(){
            this.response.setContentType("application/json");                 
            CommandeDAO dao = new CommandeDAO();
            
            boolean afficheQteZero=true;
            if (request.getParameter("afficheQteZero")!=null){
                afficheQteZero = Boolean.getBoolean(request.getParameter("afficheQteZero"));
            }
                
                   
            String json = "";
            List<CommandeLigne> commandeLigneList= this.getCommandeLigneList();
            int i=0;
            if (commandeLigneList!=null && commandeLigneList.size() > 0){
                
                for (CommandeLigne cPC : commandeLigneList){
                    
                        int quantiteCommandee = dao.getCommandeLigneQuantiteCommandee(cPC);
                        int quantiteRecu = dao.getCommandeLigneQuantiteRecu(cPC);
                        int quantiteResteALivrer = quantiteCommandee - quantiteRecu;
                        int pourcentageRecu = (quantiteRecu*100)/quantiteCommandee;
                    
                        if (afficheQteZero || quantiteResteALivrer>0){
                             json += " {'idCommandeLigne':'"+cPC.getIdCommandeLigne()+"',";
                             json += " 'idProduitConditionnement':'"+cPC.getProduitConditionnement().getIdProduitConditionnement()+"',";
                             json += " 'CAS':'"+cPC.getProduitConditionnement().getProduit().getSubstance().getCas()+"',";
                             json += " 'reference':'"+cPC.getProduitConditionnement().getReferenceFournisseur()+"',";
                             json += " 'idFournisseur':'"+cPC.getProduitConditionnement().getProduit().getFournisseur().getIdFournisseur()+"',";
                             json += " 'nom':'"+cPC.getProduitConditionnement().getProduit().getNom()+"',";
                             json += " 'contenance':'"+cPC.getProduitConditionnement().getContenance()+" " + cPC.getProduitConditionnement().getUniteMesure().getAbreviation() +"',";
                             json += " 'refrigere':'"+cPC.getProduitConditionnement().getProduit().isRefrigere()+"',";
                             json += " 'quantiteLigne':'"+cPC.getQuantite()+"',";
                             json += " 'quantiteResteALivrer':'"+quantiteResteALivrer+"',";
                             json += " 'quantiteRecue':'"+pourcentageRecu+"',"; 
                             json += " 'supp':'"+cPC.isDeletable(utilisateur)+"',";
                             json += " 'write':'"+AuthorizedProfils.isWriteAllowed(utilisateur.getUtilisateurProfil().getCode(), "AD", "LA", "SE")+"'},";                             
                             i++;
                        }
                         
                }
                if (i>0)
                    json = "{'totalCount':'"+i+"','data':[" + json;
                else
                    json = "{'totalCount':'0','data':[";
                
                json += "]}";
            }else{
                json = "{'totalCount':'0','data':[]}";
            }
            
            return json;
        }
        
        

        public String getListItems(){
            this.response.setContentType("application/json");

            String json = "";
            List<Commande> commandes = null;
            int nbItems = 0;

            try {
                CommandeDAO dao = new CommandeDAO();

                commandes = this.getListCommande();
                nbItems = this.getListCommandeCount();

                
                if (commandes !=null && commandes.size()>0){
                    json = "{'totalCount':'"+nbItems+"','data':[";
                    for (Commande l : commandes){
                        int quantiteCommandee = dao.getCommandeQuantiteCommandee(l);
                        int quantiteRecu = dao.getCommandeQuantiteRecu(l);
                        int quantiteResteALivrer = quantiteCommandee - quantiteRecu;
                        int pourcentageRecu = 0;
                        if (quantiteCommandee != 0){
                            pourcentageRecu = (quantiteRecu*100)/quantiteCommandee;
                        }
                                                

                        json += " {'idCommande':'"+l.getIdCommande()+"',";
                        json += "'dateCommande':'"+Utilities.javaDateToString(l.getDateCommande())+"',";
                        json += "'idSousStructure':'"+l.getSousStructure().getIdSousStructure()+"',";
                        json += "'nomSousStructure':'"+l.getSousStructure().getNom()+"',";
                        json += "'statut':'"+l.getStatut().getLibelle() +"',";
                        json += "'idstatut':'"+l.getStatut().getIdStatut() +"',";
                        json += "'numBonDeCommandeInterne':'"+Utilities.nullToEmpty(l.getNumBonDeCommandeInterne()) +"',";
                        json += "'dateCommandeInterne':'"+Utilities.javaDateToString(l.getDateCommandeInterne()) +"',";
                         json += " 'quantiteCommandee':'"+quantiteCommandee+"',";
                         json += " 'quantiteResteALivrer':'"+quantiteResteALivrer+"',";
                         json += " 'quantiteRecue':'"+pourcentageRecu+"',";                     
                         json += " 'quantiteLivraison':'0',"; 
                        json += "'supp':'"+l.isDeletable(utilisateur)+"',";
                        json += "'write':'"+AuthorizedProfils.isWriteAllowed(utilisateur.getUtilisateurProfil().getCode(), "AD", "LA", "SE")+"'},";
                    }
                    json += "]}";
                }else{
                    json = "{'totalCount':'0','data':[]}";
                }
            } catch (Exception ex) {
                Logger.getLogger(CommandeController.class.getName()).log(Level.SEVERE, null, ex);
                json = "{'totalCount':'0','data':[]}";
            }
            return json;
        }

        public String annuleCommande(){
            this.response.setContentType("application/json");
            //----------------------------------------------------
            //Autorisations         
            String resp = authorizedProfils.getWriteAuthorizedJsonTicket();
            if (!"".equals(resp)){
                return resp;
            }
            //----------------------------------------------------     
            String json="{'success':false}"; 
            Commande commande = this.getItemFromId();  
            // condition pour pouvoir annuler une commande :
            // 1. Elle doit être faite par l'utilisateur
            // 2. Elle doit avoir un statut 1 (ouverte), 2 (en attente de bon de commande) ou 3 (en attente de livraison)
            // 3. sans livraison
            if (commande.getUtilisateur().equals(utilisateur) && commande.getStatut().getIdStatut()<=3 && commande.getLivraisonList().size()==0){
                StatutDAO daoS = new StatutDAO();
                CommandeDAO daoC = new CommandeDAO();
                // statut 5 annulée
                commande.setStatut(daoS.findStatut(5));
                try {
                    daoC.edit(commande);
                    json = "{success: true}";
                } catch (IllegalOrphanException ex) {
                    Logger.getLogger(CommandeController.class.getName()).log(Level.SEVERE, null, ex);
                } catch (NonexistentEntityException ex) {
                    Logger.getLogger(CommandeController.class.getName()).log(Level.SEVERE, null, ex);
                } catch (RollbackFailureException ex) {
                    Logger.getLogger(CommandeController.class.getName()).log(Level.SEVERE, null, ex);
                } catch (Exception ex) {
                    Logger.getLogger(CommandeController.class.getName()).log(Level.SEVERE, null, ex);
                }
            } 
            
            return json;
            
            
        }


        private List<Article> getListArticlesLivraison(){
            this.response.setContentType("application/json");
            Commande commande = null;
            List<Article> articleList = null;

            String idCommandeS = request.getParameter("idCommande");
            if (idCommandeS==null || "".equals(idCommandeS)){
                idCommandeS = request.getParameter("id");
            }
            
            
            Integer idCommande = 0;
            try{
                idCommande = Integer.parseInt(idCommandeS);
            }catch(NumberFormatException e){
            }


            try {
                if (idCommande > 0){
                    CommandeDAO dao = new CommandeDAO();
                    if (dao!=null){
                        commande = dao.findCommande(idCommande);
                    }
                }
                if (commande!=null){
                    articleList = new ArrayList<Article>();
                    List<Livraison> livraisonList = commande.getLivraisonList();
                    for (Livraison l : livraisonList){
                        articleList.addAll(l.getArticleList());
                    }
                }


            } catch (Exception ex) {
                Logger.getLogger(CommandeController.class.getName()).log(Level.SEVERE, null, ex);
            }

            return articleList;
        }

       public List<Commande> getListCommande(){
           
            List<Commande> commandeList;

            // ------------------------------------------------------------------
            int maxResults = -1;
            int firstResult = -1;
            int periodInMonth = -1;
            try{
                maxResults = Integer.parseInt(request.getParameter("limit"));
                firstResult = Integer.parseInt(request.getParameter("start"));
                if (maxResults == 0){
                    maxResults=10;
                }
            }catch(NumberFormatException e){
            }
            
            try{
                periodInMonth = Integer.parseInt(request.getParameter("period"));
            }catch(Exception e){
            }
            // ------------------------------------------------------------------  



            CommandeDAO dao = new CommandeDAO();

            commandeList = dao.findCommandeEntitiesBySousStructure(maxResults, firstResult, utilisateur.getSousStructure(), periodInMonth);
            
            return commandeList;
            
            
           
       }

       public int getListCommandeCount(){
            int nbItems = 0;
           
            // ------------------------------------------------------------------
            int periodInMonth = -1;          
            try{
                periodInMonth = Integer.parseInt(request.getParameter("period"));
            }catch(Exception e){
            }
            // ------------------------------------------------------------------  

           CommandeDAO dao = new CommandeDAO();
            
           nbItems = dao.getCommandeEntitiesBySousStructureCount(utilisateur.getSousStructure(), periodInMonth);
           
           return nbItems;
           
       }       
        
       public String getXLSList(){

            String fileName = "";
            List<Commande> commandeList = null;
            
            Commande commande = this.getItemFromId();
            if (commande==null){
                commandeList = this.getListCommande();
            }else{
                commandeList = new ArrayList<>();
                commandeList.add(commande);
            }            
            
            
            
            try {

                DateTimeFormatter FILE_FORMAT = DateTimeFormat.forPattern("yyyyMMddHmsS");
                DateTimeFormatter USER_FORMAT = DateTimeFormat.forPattern("H:m dd/MM/yyyy");


                LocalDateTime now = new LocalDateTime();
                String dateStr = now.toString(FILE_FORMAT);


                fileName = "Commandes_"+dateStr+".xls";
                
                WritableWorkbook wrtWorkbook = Workbook.createWorkbook(new File(fileName));

                WritableSheet sheet1;

                sheet1 = wrtWorkbook.createSheet("Commandes", 0);

                Label titre = new Label(0, 0, "Commandes, le " + now.toString(USER_FORMAT));
                sheet1.addCell(titre);
                sheet1.addCell(new Label(0, 2, "Date de commande"));
                sheet1.addCell(new Label(1, 2, "Date de commande Sifac"));
                sheet1.addCell(new Label(2, 2, "Numero bon de commande"));
                sheet1.addCell(new Label(3, 2, "Statut commande"));
                sheet1.addCell(new Label(4, 2, "Commanditaire"));
                sheet1.addCell(new Label(5, 2, "Structure"));
                sheet1.addCell(new Label(6, 2, "Date de livraison"));
                sheet1.addCell(new Label(7, 2, "Numero Bon de livraison"));   
                sheet1.addCell(new Label(8, 2, "CAS"));
                sheet1.addCell(new Label(9, 2, "Reference Fournisseur"));
                sheet1.addCell(new Label(10, 2, "Produit"));
                sheet1.addCell(new Label(11, 2, "Local de Stockage"));
                sheet1.addCell(new Label(12, 2, "Etiquette"));                

                int i = 3;
                for (Commande c : commandeList){
                    
                    sheet1.addCell(new Label(0, i, Utilities.javaDateToString(c.getDateCommande())));
                    sheet1.addCell(new Label(1, i, Utilities.javaDateToString(c.getDateCommandeInterne())));
                    sheet1.addCell(new Label(2, i, c.getNumBonDeCommandeInterne()));
                    sheet1.addCell(new Label(3, i, c.getStatut().getLibelle()));
                    sheet1.addCell(new Label(4, i, c.getUtilisateur().getNom()));                    
                    sheet1.addCell(new Label(5, i, c.getSousStructure().getNom())); 
                    for (Livraison l : c.getLivraisonList()){
                        i++;
                        sheet1.addCell(new Label(6, i, Utilities.javaDateToString(l.getDateLivraison()))); 
                        sheet1.addCell(new Label(7, i, l.getNumeroBonLivraison())); 
                        for (Article s : l.getArticleList()){
                            i++;
                            sheet1.addCell(new Label(8, i, s.getProduitConditionnement().getProduit().getSubstance().getCas()));
                            sheet1.addCell(new Label(9, i, s.getProduitConditionnement().getReferenceFournisseur()));
                            sheet1.addCell(new Label(10, i, StringEscapeUtils.unescapeHtml(s.getProduitConditionnement().getProduit().getNom())));
                            sheet1.addCell(new Label(11, i, s.getAireStockage().getNom()));
                            sheet1.addCell(new Label(12, i, s.getIdentifiantEtiquette()));                            
                        }
                    }

                    
                    i++;

                }
                
                wrtWorkbook.write();
                wrtWorkbook.close();


            } catch (IOException ex) {
                Logger.getLogger(FournisseurController.class.getName()).log(Level.SEVERE, null, ex);
            } catch (RowsExceededException ex) {
                Logger.getLogger(FournisseurController.class.getName()).log(Level.SEVERE, null, ex);
            } catch (WriteException ex) {
                Logger.getLogger(FournisseurController.class.getName()).log(Level.SEVERE, null, ex);
            }

            return "{'success':true,'fileName':'"+fileName+"'}";

        }        
        
        
        
        public String getArticleXLS(){
            //----------------------------------------------------
            //Autorisations en lecture
            String resp = authorizedProfils.getReadAuthorizedJsonTicket();
            if (!"".equals(resp)){
                return resp;
            }
            //----------------------------------------------------     
            String fileName = "";

            try {

                DateTimeFormatter FILE_FORMAT = DateTimeFormat.forPattern("yyyyMMddHmsS");
                DateTimeFormatter USER_FORMAT = DateTimeFormat.forPattern("H:m dd/MM/yyyy");

                List<Article> articles = this.getListArticlesLivraison();

                LocalDateTime now = new LocalDateTime();
                String dateStr = now.toString(FILE_FORMAT);


                fileName = "articles_"+dateStr+".xls";

                WritableWorkbook wrtWorkbook = Workbook.createWorkbook(new File(fileName));

                WritableSheet sheet1;

                sheet1 = wrtWorkbook.createSheet("Articles", 0);

                Label titre = new Label(0, 0, "Articles, le " + now.toString(USER_FORMAT));
                Label sousTitre = new Label(0, 1, "Livraison n°"+articles.get(0).getLivraison().getNumeroBonLivraison());
                sheet1.addCell(titre);
                sheet1.addCell(sousTitre);

                sheet1.addCell(new Label(0, 3, "CAS"));
                sheet1.addCell(new Label(1, 3, "Reference Fournisseur"));
                sheet1.addCell(new Label(2, 3, "Produit"));
                sheet1.addCell(new Label(3, 3, "Local de Stockage"));
                sheet1.addCell(new Label(4, 3, "Equipe/departement"));
                sheet1.addCell(new Label(5, 3, "Etiquette"));

                int i = 4;
                for (Article s : articles){

                    sheet1.addCell(new Label(0, i, s.getProduitConditionnement().getProduit().getSubstance().getCas()));
                    sheet1.addCell(new Label(1, i, s.getProduitConditionnement().getReferenceFournisseur()));
                    sheet1.addCell(new Label(2, i, StringEscapeUtils.unescapeHtml(s.getProduitConditionnement().getProduit().getNom())));
                    sheet1.addCell(new Label(3, i, s.getAireStockage().getNom()));
                    sheet1.addCell(new Label(4, i, s.getSousStructure().getNom()));
                    sheet1.addCell(new Label(5, i, s.getIdentifiantEtiquette()));
                    i++;

                }

                wrtWorkbook.write();
                wrtWorkbook.close();

                return "{'success':true,'fileName':'"+fileName+"'}";


            } catch (IOException ex) {
                Logger.getLogger(CommandeController.class.getName()).log(Level.SEVERE, null, ex);
                 return "{'success':false}";
            } catch (RowsExceededException ex) {
                Logger.getLogger(CommandeController.class.getName()).log(Level.SEVERE, null, ex);
                return "{'success':false}";
            } catch (WriteException ex) {
                Logger.getLogger(CommandeController.class.getName()).log(Level.SEVERE, null, ex);
                return "{'success':false}";
            }


        }

       
        
        public String getPDF() {
            this.response.setContentType("application/json");
            //----------------------------------------------------
            //Autorisations en lecture
            String resp = authorizedProfils.getReadAuthorizedJsonTicket();
            if (!"".equals(resp)){
                return resp;
            }
            //----------------------------------------------------   
            
            String json="{'success':false}"; 
            Commande commande = this.getItemFromId();   
            
            
            PdfDocumentFactory pdf = new PdfDocumentFactory("commande_" + commande.getIdCommande());           
            
            if (commande!=null){
                
                pdf.addVerticalTable("Commande", 24, commande.getDataForVerticalTable(), 0);
                pdf.addHorizontalTable("Livraisons", 15, Livraison.getDataForHorizontalTable(commande.getLivraisonList()), 1, null);   
                
                List<Article> articleList = new ArrayList<Article>();
                List<Livraison> livraisonList = commande.getLivraisonList();
                for (Livraison l : livraisonList){
                    articleList.addAll(l.getArticleList());
                }
                pdf.addHorizontalTable("Articles", 15, Article.getDataForHorizontalTable(articleList), 1, null);   
                
            }
            
            pdf.getDocument().close();     

            if (!"".equals(pdf.getFileName())){
                return "{'success':true,'fileName':'"+pdf.getFileName()+"'}";
            }else{
                return json;   
            }
            
        }
        
    // Methode utilisée pour les édition XLS et PDF
    // retourne l'utilisateur à partir de l'id. Si l'id n'est pas définit, on a demandé l'impression de la liste (voir méthode getXLSList()
    private Commande getItemFromId(){
        
            Commande commande = null;
            int idCommande = 0;
            String idCommandeS = request.getParameter("id");
            
            if (idCommandeS!=null && !"".equals(idCommandeS)){
                try{
                    idCommande = Integer.parseInt(idCommandeS);
                    if (idCommande > 0 ){
                        try {
                            CommandeDAO dao = new CommandeDAO();
                            commande = dao.findCommande(idCommande);
                        } catch (Exception ex) {
                            Logger.getLogger(CommandeController.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }catch(NumberFormatException ex){
                    Logger.getLogger(CommandeController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            return commande;
        
    }        


}
