/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2012/01/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/
package fr.univtours.gpod.Controller;

import fr.univtours.gpod.HTML.HTML;
import fr.univtours.gpod.utils.AuthorizedProfils;
import fr.univtours.gpod.utils.Utilities;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author geoffroy.vibrac
 */
public class LaboratoireController extends StructureController {

        public LaboratoireController(HttpServletRequest request, HttpServletResponse response, HttpSession session){
            super(request, response, session);
            authorizedProfils = new AuthorizedProfils(request, response, session);
            authorizedProfils.addReadAuthorisedProfile("AD");
            authorizedProfils.addWriteAuthorisedProfile("AD");                 
        }    

        public String saveItem(){
            //----------------------------------------------------
            //Autorisations         
            String resp = authorizedProfils.getWriteAuthorizedJsonTicket();
            if (!"".equals(resp)){
                return resp;
            }
            //----------------------------------------------------                  
            return this.saveItem("A");
        }

        public String getListItems(){
            return this.getListItems("A");
        }
        
           

        public String getHtml(){
            //----------------------------------------------------
            //Autorisations         
            String resp = authorizedProfils.getAuthorizedHtmlTicket();
            if (!"".equals(resp)){
                return resp;
            }
            //----------------------------------------------------  
                response.setContentType("text/html;charset=iso-8859-1");

                HTML html = HTML.getInstance();
                html.getHeader().setTitle("Gestion des produits dangereux &gt; Gestion des laboratoires");
                html.setExtjsHeader();
                html.getHeader().getCss().addLine("lib/multiselect/Multiselect.css");
                html.getHeader().getCss().addLine("lib/search/gridsearch.css");
                html.getHeader().getCss().addLine("lib/msg/msg.css");

                html.getHeader().getJs().addBlock("var B_nameUser = '"+Utilities.addSlashes(utilisateur.getNom())+"'");
                html.getHeader().getJs().addBlock("var B_lastCx = '"+Utilities.javaDateTIMEToString(utilisateur.getDateAvantDerniereConnexion())+"'");                 
                html.getHeader().getJs().addLine("version.js");
                html.getHeader().getJs().addLine("lib/printAction.js");
                html.getHeader().getJs().addLine("lib/msg/msg.js");
                html.getHeader().getJs().addLine("lib/multiselect/Multiselect.js");
                html.getHeader().getJs().addLine("lib/multiselect/ItemSelector.js");
                html.getHeader().getJs().addLine("lib/search/gridSearch.js");
                html.getHeader().getJs().addLine("lib/excelButton.js");
                html.getHeader().getJs().addLine("ui/container.Laboratoires.js");
                html.getHeader().getJs().addLine("ui/menus/menu."+ session.getAttribute("profil") +".js");
                html.getHeader().getJs().addLine("ui/squelette.js");
                html.getBody().setBodyElement("<table align='center' class='contenttable' cellpadding='0' cellspacing='0' border='0'><tr><td id='centerPanel'></td></tr></table>");

                return html.getHTMLCode();
        }

        public String getXLSList(){
            return getXLSList("A");
        }

       

}
