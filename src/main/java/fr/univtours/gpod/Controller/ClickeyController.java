/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2012/01/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/

package fr.univtours.gpod.Controller;


import fr.univtours.front.Clickey;
import fr.univtours.gpod.Controller.stocks.Stock;
import fr.univtours.gpod.HTML.HTML;
import fr.univtours.gpod.persistance.DAO.ArticleDAO;
import fr.univtours.gpod.persistance.DAO.ClickeyScanDAO;
import fr.univtours.gpod.persistance.DAO.exceptions.RollbackFailureException;
import fr.univtours.gpod.persistance.entities.Article;
import fr.univtours.gpod.persistance.entities.ClickeyScan;
import fr.univtours.gpod.utils.AuthorizedProfils;
import fr.univtours.gpod.utils.Utilities;
import java.io.IOException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class ClickeyController extends ActionController{
        
        public ClickeyController(HttpServletRequest request, HttpServletResponse response, HttpSession session){
            super(request, response, session);
            authorizedProfils = new AuthorizedProfils(request, response, session);
            authorizedProfils.addReadAuthorisedProfile("RA","AD", "LA", "SE");
            authorizedProfils.addWriteAuthorisedProfile("AD", "LA", "SE");             
        } 
    
        
        
       public String proceedClickeyInsert(){
        
            try {

                    // récupération de l'uid du clickey : parametre u du formulaire
                    String clickeyP = request.getParameter("u");            
                    // split de la chaine car sous la forme UIDClickey|1111|1212|1111|1212|01|02
                    String[] clickeyPTab = clickeyP.split("\\|");
                    //  decodage de la chaine base64=> Hexa en string
                    String clickeyUID = Utilities.changeClikeyEncodageToHex(clickeyPTab[0]);
                    Logger.getLogger(ClickeyController.class.getName()).log(Level.INFO, "clickeyUID : {0}", clickeyUID);           

                    //récupération des tag
                    String tags = request.getParameter("c");
                    // split de la chaine car sous la forme tag1@timestant|tag2@timestant|tag3@timestant|...
                    String[] tagsTab = tags.split("\\|");
                    // initialisation d'une liste pour contenir les scans
                    List<ClickeyScan> csList = new ArrayList();
                    // initialisation de la date
                    Date now = new Date();
                    // parcours du tableau des tag
                    for (String s : tagsTab){
                        // split de chaque chaine de tag sous la forme tag1@timestant
                        String[] tagTab = s.split("@");
                        try {
                            //  decodage de la chaine base64=> Hexa en string
                            String tagString = Utilities.changeClikeyEncodageToHex(tagTab[0]);
                            // ajout du tag dans la map 
                            if (tagTab.length==2 && tagTab[1]!=null && !"".equals(tagTab[1]) &&  tagString!=null && !"".equals(tagString)){
                                ClickeyScan cs = new ClickeyScan(clickeyUID, tagString, tagTab[1], now, utilisateur, false);
                                if (cs!=null){
                                    csList.add(cs);
                                }
                                
                            }
                        } catch (MessagingException ex) {
                            Logger.getLogger(ClickeyController.class.getName()).log(Level.SEVERE, null, ex);
                        }

                    }
                    Logger.getLogger(ClickeyController.class.getName()).log(Level.INFO, "Tags : {0}", csList.size());
                    
                    ClickeyScanDAO dao = new ClickeyScanDAO();
                    
                    // si il y a bien un uid clickey et des tags
                    if (clickeyUID!=null && !"".equals(clickeyUID) && csList.size() > 0){
                        for (ClickeyScan cs : csList){
                            // si ce scan n'existe pas, on l'ajoute
                            if (dao.findIfScanExist(cs)==0){
                                // il faut tester si il y a un scan avec le même produit qui n'a pas été traité
                                // si c'est le cas, il est inutile d'avoir 2 tag identique a traiter, et donc on 
                                // met dejatraité de celui ci à true
                                if (dao.findIfTagExist(cs)>0){
                                    cs.setDejaTraite(true);
                                }
                                
                                try {
                                    dao.create(cs);
                                } catch (RollbackFailureException ex) {
                                    Logger.getLogger(ClickeyController.class.getName()).log(Level.SEVERE, null, ex);
                                } catch (Exception ex) {
                                    Logger.getLogger(ClickeyController.class.getName()).log(Level.SEVERE, null, ex);
                                }
                            }                            
                        }
                    }
                

                } catch (MessagingException | IOException ex ) {
                    Logger.getLogger(ClickeyController.class.getName()).log(Level.SEVERE, null, ex);
                }catch (Exception ex){
                    Logger.getLogger(ClickeyController.class.getName()).log(Level.SEVERE, null, ex);
                }
            
            return getHtml();
       
       }
       
       public String getHtml(){
            //----------------------------------------------------
            //Autorisations         
            String resp = authorizedProfils.getAuthorizedHtmlTicket();
            if (!"".equals(resp)){
                return resp;
            }
            //----------------------------------------------------  


                response.setContentType("text/html;charset=iso-8859-1");

                HTML html = HTML.getInstance();
                html.getHeader().setTitle("Gestion des produits dangereux &gt; Gestion des commandes");
                html.setExtjsHeader();
                html.getHeader().getCss().addLine("lib/multiselect/Multiselect.css");
                html.getHeader().getCss().addLine("lib/search/gridsearch.css");
                html.getHeader().getCss().addLine("lib/msg/msg.css");
                
                html.getHeader().getJs().addBlock("var B_nameUser = '"+Utilities.addSlashes(utilisateur.getNom())+"'");
                html.getHeader().getJs().addBlock("var B_lastCx = '"+Utilities.javaDateTIMEToString(utilisateur.getDateAvantDerniereConnexion())+"'");                 
                html.getHeader().getJs().addLine("version.js");
                html.getHeader().getJs().addLine("lib/printAction.js");
                html.getHeader().getJs().addLine("lib/periodButtons.js");
                html.getHeader().getJs().addLine("lib/help/helpPlugin.js");
                html.getHeader().getJs().addLine("lib/msg/msg.js");
                html.getHeader().getJs().addLine("lib/multiselect/Multiselect.js");
                html.getHeader().getJs().addLine("lib/multiselect/ItemSelector.js");
                html.getHeader().getJs().addLine("lib/search/gridSearch.js");
                html.getHeader().getJs().addLine("ui/search/SousStructure.js");
                html.getHeader().getJs().addLine("ui/search/AireStockage.js");
                html.getHeader().getJs().addLine("ui/windowEntreeEnStock.js");
                html.getHeader().getJs().addLine("ui/addProduit.js");
                html.getHeader().getJs().addLine("ui/container.Clickey.js");
                html.getHeader().getJs().addLine("ui/menus/menu."+ session.getAttribute("profil") +".js");
                html.getHeader().getJs().addLine("ui/squelette.js");
                html.getBody().setBodyElement("<table align='center' class='contenttable' cellpadding='0' cellspacing='0' border='0'><tr><td id='centerPanel'></td></tr></table>");


                return html.getHTMLCode();
        }    
       
       
       
       
       
       
        public String getListItems(){
            this.response.setContentType("application/json");

            
            ArticleDAO dao = new ArticleDAO();
            List<Object[]> listArticles = dao.findStockArticleFromClickey(utilisateur);

            List<Stock> stocks = Stock.getListStock(listArticles);            
            
            
            String json = "{'totalCount':'0','data':[]}";


            if (stocks !=null && stocks.size()>0){
                    json = "{'totalCount':'"+stocks.size()+"','data':[";
                    for (Stock u : stocks){
                        json += " {'CAS':'"+u.getProduitConditionnement().getProduit().getSubstance().getCas()+"',";
                        json += " 'idArticle':'"+u.getIdArticle()+"',";
                        json += " 'idProduitConditionnement':'"+u.getProduitConditionnement().getIdProduitConditionnement()+"',";
                        json += "'identifiantEtiquette':'"+u.getIdentifiantEtiquette()+"',";
                        json += "'nomProduit':'"+Utilities.addSlashes(u.getProduitConditionnement().getProduit().getNom())+"',";
                        json += "'idSousStructure':'"+u.getSousStructure().getIdSousStructure()+"',";
                        json += "'nomSousStructure':'"+Utilities.addSlashes(u.getSousStructure().getNom())+"',";
                        json += "'idAire':'"+u.getAire().getIdAire()+"',";
                        json += "'nomAire':'"+Utilities.addSlashes(u.getAire().getSalleStockage().getBatiment().getNom())+ " &gt; " + 
                                    Utilities.addSlashes(u.getAire().getSalleStockage().getNom())+ " &gt; " + 
                                    Utilities.addSlashes(u.getAire().getNom())+"',";
                        json += "'quantite':'"+u.getQuantite()+"',";
                        json += "'contenance':'"+u.getProduitConditionnement().getContenance()+"',";
                        json += "'uniteMesure':'"+u.getProduitConditionnement().getUniteMesure().getAbreviation()+"',";
                        json += "'uniteMesureLong':'"+u.getProduitConditionnement().getUniteMesure().getLibelleLong()+"',";
                        json += "'dateOuverture':'"+Utilities.javaDateToString(u.getDateOuverture())+"',";
                        json += "'idClickeyScan':'"+u.getIdClickeyScan()+"',";
                        json += "'write':'"+AuthorizedProfils.isWriteAllowed(utilisateur.getUtilisateurProfil().getCode(), "AD", "LA")+"'},";
                    }
                    json += "]}";
            }else{
                json = "{'totalCount':'0','data':[]}";
            }

           return json;

        }       
       
       
}
