/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2012/01/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/

package fr.univtours.gpod.Controller;

import fr.univtours.gpod.persistance.DAO.PictogrammeDAO;
import fr.univtours.gpod.persistance.entities.Pictogramme;
import fr.univtours.gpod.utils.Utilities;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author geoffroy.vibrac
 */
public class PictogrammeController extends ActionController {

        public PictogrammeController(HttpServletRequest request, HttpServletResponse response, HttpSession session){
            super(request, response, session);
        }

    
            public String getListItems(){
            
            String json = "";           
            
            try {
               
                
                String path = request.getParameter("path");
                
                
                PictogrammeDAO dao = new PictogrammeDAO();
                
                List<Pictogramme> pictogrammes = dao.findPictogrammeEntities();


                this.response.setContentType("application/json");
                
                if (pictogrammes !=null && pictogrammes.size()>0){

                    json = "{\"success\":\"true\",\"totalCount\":\""+pictogrammes.size()+"\",\"data\":[";
                    for (Pictogramme p : pictogrammes){
                        
                        String imgPath = "<img src=\\\""+path+p.getIcone()+"_30.jpg\\\" qtip=\\\""+p.getCode() + " - " + p.getNom()+"\\\""
                                
                          + " onmouseover=\\\"myTimer=setTimeout('openImgWindow(\\\\\'"+path+p.getIcone()+"\\\\\',\\\\\'"+p.getCode()+"\\\\\')',500);\\\" onmouseout=\\\"javascript:closeImgWindow();clearTimeout(myTimer);\\\"  />&nbsp;&nbsp;";
                        
                        
                        json += " {\"boxLabel\":\""+imgPath+"\",";
                        json += "\"inputValue\":\""+p.getIdPictogramme()+"\",";
                        json += "\"id\":\"picto_"+p.getIdPictogramme()+"\",";
                        json += "\"name\":\"idPictogramme\"},";
                    }                    
                    json += "]}";
                }
            } catch (Exception ex) {
                Logger.getLogger(PictogrammeController.class.getName()).log(Level.SEVERE, null, ex);
            }
            return json;                   
            
        }

            
    
}
