/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2012/01/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/

package fr.univtours.gpod.Controller;


import fr.univtours.gpod.HTML.HTML;
import fr.univtours.gpod.persistance.DAO.PictogrammeDAO;
import fr.univtours.gpod.persistance.DAO.SubstanceDAO;
import fr.univtours.gpod.persistance.DAO.exceptions.NonexistentEntityException;
import fr.univtours.gpod.persistance.DAO.exceptions.PreexistingEntityException;
import fr.univtours.gpod.persistance.entities.Pictogramme;
import fr.univtours.gpod.persistance.entities.Substance;
import fr.univtours.gpod.utils.AuthorizedProfils;
import fr.univtours.gpod.utils.ESISSearch;
import fr.univtours.gpod.utils.Utilities;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;
import org.apache.commons.lang.StringEscapeUtils;
import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;


public class SubstanceController extends ActionController {
     
        
        public SubstanceController(HttpServletRequest request, HttpServletResponse response, HttpSession session){
            super(request, response, session);
            authorizedProfils = new AuthorizedProfils(request, response, session);
            authorizedProfils.addReadAuthorisedProfile("AD","LA","HY","ME");
            authorizedProfils.addWriteAuthorisedProfile("AD","LA");               
        }
        
        public String getHtml(){
            //----------------------------------------------------
            //Autorisations         
            String resp = authorizedProfils.getAuthorizedHtmlTicket();
            if (!"".equals(resp)){
                return resp;
            }
            //----------------------------------------------------    
                response.setContentType("text/html;charset=iso-8859-1");
            
                HTML html = HTML.getInstance();
                html.getHeader().setTitle("Gestion des produits dangereux &gt; Gestion des substances");
                html.setExtjsHeader();
                html.getHeader().getCss().addLine("lib/multiselect/Multiselect.css");
                html.getHeader().getCss().addLine("lib/search/gridsearch.css");
                html.getHeader().getCss().addLine("lib/msg/msg.css");
                html.getHeader().getCss().addLine("lib/keyboard/virtualkeyboard.css");

                html.getHeader().getJs().addBlock("var B_nameUser = '"+Utilities.addSlashes(utilisateur.getNom())+"'");
                html.getHeader().getJs().addBlock("var B_lastCx = '"+Utilities.javaDateTIMEToString(utilisateur.getDateAvantDerniereConnexion())+"'"); 
                html.getHeader().getJs().addLine("version.js");
                html.getHeader().getJs().addLine("lib/keyboard/Ext.ux.VirtualKeyboard.js");
                html.getHeader().getJs().addLine("lib/keyboard/Ext.ux.plugins.VirtualKeyboard.js");
                html.getHeader().getJs().addLine("lib/encoder.js");
                html.getHeader().getJs().addLine("lib/printAction.js");
                html.getHeader().getJs().addLine("lib/msg/msg.js");                 
                html.getHeader().getJs().addLine("lib/checkbox/remotecheckboxgroup.js");
                html.getHeader().getJs().addLine("lib/multiselect/Multiselect.js");
                html.getHeader().getJs().addLine("lib/multiselect/ItemSelector.js");   
                html.getHeader().getJs().addLine("lib/search/gridSearch.js");
                html.getHeader().getJs().addLine("lib/excelButton.js");
                html.getHeader().getJs().addLine("ui/container.Substances.js");
                html.getHeader().getJs().addLine("ui/menus/menu."+ session.getAttribute("profil") +".js");
                html.getHeader().getJs().addLine("ui/squelette.js");
                html.getBody().setBodyElement("<table align='center' class='contenttable' cellpadding='0' cellspacing='0' border='0'><tr><td id='centerPanel'></td></tr></table>");
                html.getBody().setBodyElement("<div id='iframediv'>");

                return html.getHTMLCode();
        }        
        
        public String saveItem(){
            //----------------------------------------------------
            //Autorisations         
            String resp = authorizedProfils.getWriteAuthorizedJsonTicket();
            if (!"".equals(resp)){
                return resp;
            }
            //----------------------------------------------------                  
            String json="";
            SubstanceDAO dao = null;
            List<Pictogramme> pictogrammes = new ArrayList<Pictogramme>(); 
            
            this.response.setContentType("application/json");
                    
            
            String CAS = request.getParameter("CAS").trim();
            String CASOrigine = request.getParameter("CASorigine");
            String ec = request.getParameter("EC");            
            String nom = request.getParameter("nom");
            String formule = request.getParameter("formule");
            String description = request.getParameter("description");
            String[] idPictogrammes = request.getParameterValues("idPictogramme");

                              
            // Test des champs
            if ("".equals(nom) || "".equals(CAS)){
                json = "{success: false, errors:{reason:\'Des champs obligatoires n\\\'ont pas été saisis\'}}";
                return json;          
            }
                      
            try{
                dao = new SubstanceDAO();                
                if (idPictogrammes!= null && idPictogrammes.length>0){                    
                    PictogrammeDAO daoPicto = new PictogrammeDAO();
                    for (String ids : idPictogrammes){
                        int id = Integer.parseInt(ids);
                        Pictogramme p = daoPicto.findPictogramme(id);
                        if (p!=null){
                            pictogrammes.add(p);
                        }
                    }
                }
                 
            } catch (Exception ex) {
                Logger.getLogger(SubstanceController.class.getName()).log(Level.SEVERE, null, ex);
                json = "{success: false, errors:{reason:\'"+ex.getMessage()+"\'}}";
                return json;
            }
            
            // Si CASOrigine !=null on est en modification => 
            // on cherche si la substance est déjà dans la base
            Substance substance = null;
            if (CASOrigine != null && !"".equals(CASOrigine)){
                substance = dao.findSubstance(CAS);
            }
                        
            // Ajout nouvelle substance substance = null
            if (substance == null){
                substance = new Substance(CAS, nom, formule, description, ec, pictogrammes);
                if (substance !=null){
                    try {
                        dao.create(substance);
                        json = "{success: true}";
                    } catch (PreexistingEntityException ex) {
                        Logger.getLogger(SubstanceController.class.getName()).log(Level.SEVERE, null, ex);
                        json = "{success: false, errors:{reason:\'La substance existe déjà\'}}";
                    } catch (Exception ex) {
                        Logger.getLogger(SubstanceController.class.getName()).log(Level.SEVERE, null, ex);
                        json = "{success: false, errors:{reason:\'"+ex.getMessage()+"\'}}";
                    }
                }                                
          
            // UPDATE utilisateur idUtilisateur > 0
            }else if (substance !=null){
                try {
                    substance.setNom(nom);
                    substance.setEc(ec);
                    substance.setFormule(formule);
                    substance.setDescription(description);
                    substance.setPictogramme(pictogrammes);

                    dao.edit(substance);
                    json = "{success: true}";
                } catch (NonexistentEntityException ex) {
                    Logger.getLogger(SubstanceController.class.getName()).log(Level.SEVERE, null, ex);
                    json = "{success: false, errors:{reason:\'"+ex.getMessage()+"\'}}";  
                } catch (Exception ex) {
                    Logger.getLogger(SubstanceController.class.getName()).log(Level.SEVERE, null, ex);
                    json = "{success: false, errors:{reason:\'"+ex.getMessage()+"\'}}";  
                }
        
            }else{
                json = "{success: false, errors:{reason:\'Impossible de trouver "+CAS+"\'}}";
            }
                     
            
            return json;            
            //return "{success: false, errors:{reason:\'"+msg+"\'}};
        }
   

        
        public List<Substance> getListSubstance(){
            return this.getListSubstance(-1, 0, "", "");
        }
        
        public List<Substance> getListSubstance(int maxResults, int firstResult, String searchValue, String searchColumns){

            List<Substance> substances = null;
            try {
                SubstanceDAO dao = new SubstanceDAO();               
                // Si pas de recherche => On affiche tout
                if (searchValue == null || searchColumns==null || "".equals(searchValue) || "".equals(searchColumns)){
                    substances = dao.findSubstanceEntities(maxResults, firstResult);
                }else{
                    substances = dao.findSubstanceWithQuery(searchValue, searchColumns, maxResults, firstResult);
                }        
      
            } catch (Exception ex) {
                Logger.getLogger(SubstanceController.class.getName()).log(Level.SEVERE, null, ex);
            }
            return substances;    
        }
        
        private int getSubstanceCount(String searchValue, String searchColumns){
            int nbItems = 0;
            try {
                SubstanceDAO dao = new SubstanceDAO();
                nbItems = dao.getSubstanceWithQueryCount(searchValue, searchColumns);
            } catch (Exception ex) {
                Logger.getLogger(SubstanceController.class.getName()).log(Level.SEVERE, null, ex);
            }

            return nbItems;
        }     

        
        public String ESISsearch(){
            this.response.setContentType("application/json");
            String json = "";
            String searchValue = request.getParameter("CAS");
            
            ESISSearch esis = new ESISSearch(searchValue);
            Substance searchSub = esis.search();
            
            if (searchSub!=null && searchSub.getCas()!=null && !"".equals(searchSub.getCas()) && searchSub.getNom()!=null && !"".equals(searchSub.getNom())){
                    json = "{'totalCount':'1','data':[";                      
                    json += " {'CAS':'"+Utilities.addSlashes(searchSub.getCas())+"',";
                    json += "'EC':'"+Utilities.addSlashes(searchSub.getEc())+"',";
                    json += "'nom':'"+Utilities.addSlashes(searchSub.getNom())+"',";
                    json += "'formule':'"+Utilities.addSlashes(searchSub.getFormule())+"',";
                    json += "'description':'"+Utilities.addSlashes(searchSub.getDescription())+"',";
                    json += "'pictogrammes':'"+Utilities.addSlashes(searchSub.getListPictogramme())+"'}";        
                    json += "]}";
            }else{
                json = "{'totalCount':'0','data':[]}";
            }                                

            
            
            return json;
        }
        
        
        public String getListItems(){
            
            String json = "";
            int nbItems = 0;
              
            // ------------------------------------------------------------------
            int maxResults = -1;
            int firstResult = -1;
            try{
                maxResults = Integer.parseInt(request.getParameter("limit"));
                firstResult = Integer.parseInt(request.getParameter("start"));
                if (maxResults == 0){
                    maxResults=10;
                }
            }catch(NumberFormatException e){
            }
            String searchValue = request.getParameter("query");
            String searchColumns = request.getParameter("fields");
            // ------------------------------------------------------------------              
                
            List<Substance> substances = this.getListSubstance(maxResults, firstResult, searchValue, searchColumns);
            nbItems = this.getSubstanceCount(searchValue, searchColumns);
            
            this.response.setContentType("application/json");

            if (substances !=null && substances.size()>0){
                json = "{'totalCount':'"+nbItems+"','data':[";
                for (Substance u : substances){                        
                    json += " {'CAS':'"+Utilities.addSlashes(u.getCas())+"',";
                    json += "'EC':'"+Utilities.addSlashes(u.getEc())+"',";
                    json += "'nom':'"+Utilities.addSlashes(u.getNom())+"',";
                    json += "'formule':'"+Utilities.addSlashes(u.getFormule())+"',";
                    json += "'description':'"+Utilities.addSlashes(u.getDescription())+"',";
                    json += "'pictogrammes':'"+Utilities.addSlashes(u.getListPictogramme())+"',";
                    json += "'write':'"+AuthorizedProfils.isWriteAllowed(utilisateur.getUtilisateurProfil().getCode(), "AD", "LA")+"'},";
                }                    
                json += "]}";
            }else{
                json = "{'totalCount':'0','data':[]}";
            }

            return json;                   
        }
        
        
       public String deleteItem(){
            //----------------------------------------------------
            //Autorisations         
            String resp = authorizedProfils.getWriteAuthorizedJsonTicket();
            if (!"".equals(resp)){
                return resp;
            }
            //----------------------------------------------------       
   
            this.response.setContentType("application/json");
            
            String json = "";
            String msg = "";
            String CAS = request.getParameter("id");
            boolean success = true;
            
            
            if (!"".equals(CAS)){
                try{
                    SubstanceDAO dao = new SubstanceDAO();
                    dao.destroy(CAS);
                    
                } catch (NonexistentEntityException ex) {
                        Logger.getLogger(SubstanceController.class.getName()).log(Level.SEVERE, null, ex);
                } catch (Exception ex) {
                        // je gère les erreurs de suppression
                            String message = "";
                            try{
                                message = ex.getCause().getCause().getMessage();
                                String s = "Cannot delete or update a parent row: a foreign key constraint fails (`gpod`.`";
                                message = message.substring(s.length());
                                int apo = message.indexOf("`");
                                message = message.substring(0, apo);   
                            }catch(NullPointerException e2){                                      
                            }
                                                        
                            
                            if (!"".equals(message)){
                                msg = "Impossible de supprimer cette ligne, elle est utilisée par un objet \""+message+"\"";
                            }else{
                                msg = "Impossible de supprimer cette ligne, Erreur logiciel !!";                                
                                Logger.getLogger(ActionController.class.getName()).log(Level.SEVERE, null, ex);
                            }
                            success = false;         
                }
                
            }else{
                success = false;
                msg = "CAS inconnu";
            }
            
            
            if (success){
                json = "{success: true}";            
            }else{
                this.response.setStatus(424);
                json = "{success: false, errors:{reason:\'"+msg+"\'}}";
            }
            return json;
        }
        
       
    // Methode utilisée pour les édition XLS et PDF
    // retourne l'utilisateur à partir de l'id. Si l'id n'est pas définit, on a demandé l'impression de la liste (voir méthode getXLSList()
    private Substance getItemFromId(){
        
            Substance substance = null;
            String CAS = request.getParameter("id");
            
            if (CAS!=null && !"".equals(CAS)){
                try{
                    try {
                        SubstanceDAO dao = new SubstanceDAO();
                        substance = dao.findSubstance(CAS);
                    } catch (Exception ex) {
                        Logger.getLogger(SubstanceController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }catch(NumberFormatException ex){
                    Logger.getLogger(SubstanceController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            return substance;
        
    }       
        
   
        
        public String getPDF(){
             //----------------------------------------------------
            //Autorisations en lecture
            String resp = authorizedProfils.getReadAuthorizedJsonTicket();
            if (!"".equals(resp)){
                return resp;
            }
            //----------------------------------------------------   
            
            String json="{'success':false}"; 
            List<String> pdfPictoDanger = null;

            Substance substance = this.getItemFromId();        
            
            try {
             
                
                if (substance.getPictogrammes()!=null){
                    pdfPictoDanger = new ArrayList<String>();                     
                    for (Pictogramme pic : substance.getPictogrammes()){
                        pdfPictoDanger.add(pic.getIcone());
                    }                       
                }                
                                
            } catch (Exception ex) {
                Logger.getLogger(SubstanceController.class.getName()).log(Level.SEVERE, null, ex);
            }            
       
        
            PdfDocumentFactory pdf = new PdfDocumentFactory("substance_" + substance.getCas());

            pdf.addVerticalTable(StringEscapeUtils.unescapeHtml(substance.getNom()), 24, substance.getDataForVerticalTable(), 0);
            pdf.addHorizontalTableImg("Dangers", 15, Pictogramme.getDataForHorizontalTableImg(substance.getPictogrammes()), 1, null);                

            pdf.getDocument().close();     

            if (!"".equals(pdf.getFileName())){
                return "{'success':true,'fileName':'"+pdf.getFileName()+"'}";
            }else{
                return json;   
            }
                
            
                    
        }
        
        
        public String getXLSList(){

            String fileName = "";
            List<Substance> substanceList = null;
           
            
            // on cherche si on demander l'édition d'un seul utilisateur id<>0
            Substance substance = this.getItemFromId();
            //si utilisateur = null => c'est que id=0 car on a demander la liste de tous les utilisateurs
            if (substance==null){                
                substanceList = this.getListSubstance();
            }else{
                substanceList = new ArrayList<Substance>();
                substanceList.add(substance);
            }            
            
            
            try {

                DateTimeFormatter FILE_FORMAT = DateTimeFormat.forPattern("yyyyMMddHmsS");
                DateTimeFormatter USER_FORMAT = DateTimeFormat.forPattern("H:m dd/MM/yyyy");


                LocalDateTime now = new LocalDateTime();
                String dateStr = now.toString(FILE_FORMAT);


                fileName = "ListSubstances_"+dateStr+".xls";
                
                WritableWorkbook wrtWorkbook = Workbook.createWorkbook(new File(fileName));

                WritableSheet sheet1;

                sheet1 = wrtWorkbook.createSheet("Substances", 0);

                Label titre = new Label(0, 0, "Substances, le " + now.toString(USER_FORMAT));
                sheet1.addCell(titre);

                sheet1.addCell(new Label(0, 2, "CAS"));
                sheet1.addCell(new Label(1, 2, "EC"));
                sheet1.addCell(new Label(2, 2, "Nom"));
                sheet1.addCell(new Label(3, 2, "Formule"));
                sheet1.addCell(new Label(4, 2, "Description"));

                int i = 3;
                for (Substance s : substanceList){
                    
                    sheet1.addCell(new Label(0, i, s.getCas()));
                    sheet1.addCell(new Label(1, i, s.getEc()));
                    sheet1.addCell(new Label(2, i, StringEscapeUtils.unescapeHtml((s.getNom()))));
                    sheet1.addCell(new Label(3, i, s.getFormule()));
                    sheet1.addCell(new Label(4, i, StringEscapeUtils.unescapeHtml(s.getDescription())));
                    int j=5;
                    int k=1;
                    for (Pictogramme pic : s.getPictogrammes()){
                        sheet1.addCell(new Label(j, 2, "Dangers"+k));
                        sheet1.addCell(new Label(j, i, pic.getNom()));
                        j++;
                        k++;
                    }
                    i++;

                }
                
                wrtWorkbook.write();
                wrtWorkbook.close();


            } catch (IOException ex) {
                Logger.getLogger(SubstanceController.class.getName()).log(Level.SEVERE, null, ex);
            } catch (RowsExceededException ex) {
                Logger.getLogger(SubstanceController.class.getName()).log(Level.SEVERE, null, ex);
            } catch (WriteException ex) {
                Logger.getLogger(SubstanceController.class.getName()).log(Level.SEVERE, null, ex);
            }

            return "{'success':true,'fileName':'"+fileName+"'}";

        }
        
        
        
}
