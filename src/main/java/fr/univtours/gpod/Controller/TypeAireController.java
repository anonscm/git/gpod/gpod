/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2012/01/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/

package fr.univtours.gpod.Controller;

import fr.univtours.gpod.persistance.DAO.AireStockageDAO;
import fr.univtours.gpod.persistance.DAO.TypeAireDAO;
import fr.univtours.gpod.persistance.entities.TypeAire;
import fr.univtours.gpod.utils.Utilities;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author geoffroy.vibrac
 */
public class TypeAireController extends ActionController {

        public TypeAireController(HttpServletRequest request, HttpServletResponse response, HttpSession session){
            super(request, response, session);
        }

        
        public String getListItems(){
            
           
            
            String json = "";
            List<TypeAire> typeAires = null;
            // ------------------------------------------------------------------
            int maxResults = -1;
            int firstResult = -1;
            try{
                maxResults = Integer.parseInt(request.getParameter("limit"));
                firstResult = Integer.parseInt(request.getParameter("start"));
                if (maxResults == 0){
                    maxResults=10;
                }
            }catch(NumberFormatException e){
            }
            String searchValue = request.getParameter("query");
            String searchColumns = request.getParameter("fields");
            // ------------------------------------------------------------------     
       
            
            try {
                TypeAireDAO dao = new TypeAireDAO();               
                // Si pas de recherche => On affiche tout
                if (searchValue == null || searchColumns==null || "".equals(searchValue) || "".equals(searchColumns)){
                    typeAires = dao.findTypeAireEntities(maxResults, firstResult);
                }else{
                    typeAires = dao.findTypeAireWithQuery(searchValue, searchColumns, maxResults, firstResult);
                }        
                
                
                
                this.response.setContentType("application/json");
                
                if (typeAires !=null && typeAires.size()>0){
                    json = "{'totalCount':'"+typeAires.size()+"','data':[";
                    for (TypeAire b : typeAires){                                                
                        json += " {'idTypeAire':'"+b.getIdTypeAire()+"',";                    
                        json += "'nom':'"+Utilities.addSlashes(b.getNom())+"'},";
                    }                    
                    json += "]}";
                }else{
                    json = "{'totalCount':'0','data':[]}";
                }
            } catch (Exception ex) {
                Logger.getLogger(TypeAireController.class.getName()).log(Level.SEVERE, null, ex);
            }
            return json;       
            
        }        
        
}
