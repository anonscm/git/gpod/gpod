/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2012/01/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/

package fr.univtours.gpod.Controller;

import java.util.ArrayList;
import fr.univtours.gpod.persistance.DAO.SousStructureDAO;
import fr.univtours.gpod.persistance.entities.UtilisateurProfil;
import fr.univtours.gpod.persistance.entities.SousStructure;
import fr.univtours.gpod.HTML.HTML;
import fr.univtours.gpod.exceptions.NonexistentEntityException;
import fr.univtours.gpod.persistance.DAO.*;
import fr.univtours.gpod.utils.Utilities;
import java.util.List;
import fr.univtours.gpod.persistance.entities.Utilisateur;
import fr.univtours.gpod.persistance.entities.*;
import fr.univtours.gpod.utils.AuthorizedProfils;
import fr.univtours.gpod.utils.LdapSearch;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;
import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;


public class UtilisateurController extends ActionController{
          


        public UtilisateurController(HttpServletRequest request, HttpServletResponse response, HttpSession session){
            super(request, response, session);
            authorizedProfils = new AuthorizedProfils(request, response, session);
            authorizedProfils.addReadAuthorisedProfile("AD");
            authorizedProfils.addWriteAuthorisedProfile("AD");              
        }
        
        public String getHtml(){
            //----------------------------------------------------
            //Autorisations         
            String resp = authorizedProfils.getAuthorizedHtmlTicket();
            if (!"".equals(resp)){
                return resp;
            }
            //----------------------------------------------------  
                response.setContentType("text/html;charset=iso-8859-1");
            
                HTML html = HTML.getInstance();
                html.getHeader().setTitle("Gestion des produits dangereux");
                html.setExtjsHeader();
                html.getHeader().getCss().addLine("lib/multiselect/Multiselect.css");
                html.getHeader().getCss().addLine("lib/search/gridsearch.css");
                html.getHeader().getCss().addLine("lib/msg/msg.css");
                
                html.getHeader().getJs().addBlock("var B_nameUser = '"+Utilities.addSlashes(utilisateur.getNom())+"'");
                html.getHeader().getJs().addBlock("var B_lastCx = '"+Utilities.javaDateTIMEToString(utilisateur.getDateAvantDerniereConnexion())+"'"); 
                html.getHeader().getJs().addLine("version.js");
                html.getHeader().getJs().addLine("lib/printAction.js");
                 html.getHeader().getJs().addLine("lib/msg/msg.js");
                html.getHeader().getJs().addLine("lib/multiselect/Multiselect.js");
                html.getHeader().getJs().addLine("lib/multiselect/ItemSelector.js");   
                html.getHeader().getJs().addLine("lib/search/gridSearch.js");
                html.getHeader().getJs().addLine("ui/search/ldap.js");
                html.getHeader().getJs().addLine("lib/excelButton.js");
                html.getHeader().getJs().addLine("ui/container.Utilisateurs.js");
                html.getHeader().getJs().addLine("ui/menus/menu."+ session.getAttribute("profil") +".js");
                html.getHeader().getJs().addLine("ui/squelette.js");
                html.getBody().setBodyElement("<table align='center' class='contenttable' cellpadding='0' cellspacing='0' border='0'><tr><td id='centerPanel'></td></tr></table>");

                return html.getHTMLCode();
        }        
        
        public String savePref(){
            this.response.setContentType("application/json");
            String json = "";
            
            Integer idAire = 0;
            // On récupère l'idUTilisateur, il peut être vide si nouvel utilisateur
            try{
                idAire = Integer.parseInt(request.getParameter("idAire"));
            }catch(NumberFormatException e){
            }            
            UtilisateurDAO daoUser = new UtilisateurDAO();
            
            AireStockageDAO daoAire = new AireStockageDAO();
            AireStockage aireFavorite = daoAire.findAireStockage(idAire);
            
            
            Utilisateur user = utilisateur;
            user.setAireStockage(aireFavorite);
            
            try {
                daoUser.edit(user);
                json = "{success: true}";
            } catch (NonexistentEntityException ex) {
                Logger.getLogger(UtilisateurController.class.getName()).log(Level.SEVERE, null, ex);
                json = "{success: false, errors:{reason:\'"+ex.getMessage()+"\'}}";
            } catch (Exception ex) {
                Logger.getLogger(UtilisateurController.class.getName()).log(Level.SEVERE, null, ex);
                json = "{success: false, errors:{reason:\'"+ex.getMessage()+"\'}}";
            }
            
            return json;
        }
        
        public String saveItem(){
            //----------------------------------------------------
            //Autorisations         
            String resp = authorizedProfils.getWriteAuthorizedJsonTicket();
            if (!"".equals(resp)){
                return resp;
            }
            //----------------------------------------------------                  
            String json="";
            Long idUtilisateur=0L;
            List<SousStructure> sousStructures = new ArrayList<SousStructure>();
            UtilisateurProfil utilisateurProfil = null;
            Integer idUtilisateurProfil = 0;
            UtilisateurDAO daoUser = null;
            String[] idSousStructures = null;
            
            this.response.setContentType("application/json");
                    
            // On récupère l'idUTilisateur, il peut être vide si nouvel utilisateur
            try{
                idUtilisateur = Long.parseLong(request.getParameter("idUtilisateur"));
            }catch(NumberFormatException e){
                idUtilisateur = 0L;
            }
            
            
            String nom = request.getParameter("nom");
            String prenom = request.getParameter("prenom");
            String email = request.getParameter("email");
            String login = request.getParameter("login");
            String password = Utilities.nullToEmpty(request.getParameter("password"));
            String idSousStructureS = request.getParameter("idSousStructures");
            if (!"".equals(idSousStructureS)){
                idSousStructures = idSousStructureS.split(",");
            }
            
            try{
                idUtilisateurProfil = Integer.parseInt(request.getParameter("idUtilisateurProfil"));
            }catch(NumberFormatException e){
                //=> le profil est obligatoire, il y a un problème
                json = "{success: false, errors:{reason:\'Problème de profil utilisateur\'}}";
                return json;
            }       
            
            
            // Test des champs
            if ("".equals(nom) || "".equals(email) || "".equals(login)){
                json = "{success: false, errors:{reason:\'Des champs obligatoires n\\\'ont pas été saisis\'}}";
                return json;          
            }
                      
            try{
                // --------------------------------------------------
                // sousStructures
                // il y a des sousStructures
                if (idSousStructures!= null && idSousStructures.length>0){
                    SousStructureDAO daoSousStructure = new SousStructureDAO();
                    for (String ids : idSousStructures){
                        int id = Integer.parseInt(ids);
                        SousStructure s = daoSousStructure.findSousStructure(id);
                        if (s!=null){
                            sousStructures.add(s);
                        }
                    }
                }
                // --------------------------------------------------

                // --------------------------------------------------
                // PROFIL DAO
                UtilisateurProfilDAO daoUP = new UtilisateurProfilDAO();
                utilisateurProfil = daoUP.findUtilisateurProfil(idUtilisateurProfil);                        
                // --------------------------------------------------
 
                // --------------------------------------------------
                // Utilisateur DAO
                 daoUser = new UtilisateurDAO();
                  // --------------------------------------------------

                
            } catch (Exception ex) {
                Logger.getLogger(UtilisateurController.class.getName()).log(Level.SEVERE, null, ex);
                json = "{success: false, errors:{reason:\'"+ex.getMessage()+"\'}}";
                return json;
            }
            
            // => a ce point, les données sont validés
            // Il faut vérifier que le login n'existe pas déjà !
            if (daoUser.isLoginExist(login, idUtilisateur)){
                json = "{success: false, errors:{reason:\'Ce Login est déjà utilisé\'}}";
                return json;       
            }
            
            // Ajout nouvel utilisateur idUtilisateur = 0
            if (idUtilisateur == 0L){

                    // --------------------------------------------------
                    // Utilisateur
                    Utilisateur user = new Utilisateur(nom, prenom, login, password, email, utilisateurProfil, sousStructures);

                    if (user !=null){
                        try{
                            daoUser.create(user);
                            json = "{success: true}";
                        } catch (Exception ex) {
                            Logger.getLogger(UtilisateurController.class.getName()).log(Level.SEVERE, null, ex);
                            json = "{success: false, errors:{reason:\'"+ex.getMessage()+"\'}}";
                        }     
                    }                                  

          
            // UPDATE utilisateur idUtilisateur > 0
            }else if (idUtilisateur > 0L){
                

                    Utilisateur user = daoUser.findUtilisateur(idUtilisateur);
                    
                    if (user !=null){
                        
                        user.setNom(nom);
                        user.setPrenom(prenom);
                        user.setLogin(login);
                        user.setEmail(email);
                        user.setSousStructure(sousStructures);
                        if (!"".equals(password)){
                            user.setPassword(password);
                        }
                        user.setUtilisateurProfil(utilisateurProfil);   

                        try{
                            daoUser.edit(user);
                            json = "{success: true}"; 
                        } catch (NonexistentEntityException ex) {
                            Logger.getLogger(UtilisateurController.class.getName()).log(Level.SEVERE, null, ex);
                            json = "{success: false, errors:{reason:\'"+ex.getMessage()+"\'}}";  
                        } catch (Exception ex) {
                            Logger.getLogger(UtilisateurController.class.getName()).log(Level.SEVERE, null, ex);
                            json = "{success: false, errors:{reason:\'"+ex.getMessage()+"\'}}";  
                        }  
                    }   
                
                    
        
            }else{
                json = "{success: false, errors:{reason:\'Impossible de trouver "+idUtilisateur+"\'}}";
            }
     
            return json;            
            //return "{success: false, errors:{reason:\'"+msg+"\'}};
        }


        public String getListFromLdap(){
             String json = "";
             String type = request.getParameter("type"); // 1  =personnel, 2 = etudiant
             String name = request.getParameter("name");
             String uid = request.getParameter("uid");
             String firstName = request.getParameter("firstName");

            if ("".equals(name) && "".equals(uid) && "".equals(firstName)){
                json = "{'totalCount':'0','data':[]}";
            }else{

                LdapSearch ls = new LdapSearch();
                List<LdapSearch.LdapUser> users = ls.getListUsers(name, firstName, uid, type);


                if (users !=null && users.size()>0){
                    json = "{'totalCount':'"+users.size()+"','data':[";
                    for (LdapSearch.LdapUser u : users){
                        json += " {'uid':'"+Utilities.addSlashes(u.getUid())+"',";
                        json += "'prenom':'"+Utilities.addSlashes(u.getFirsname())+"',";
                        json += "'email':'"+Utilities.addSlashes(u.getMail())+"',";
                        json += "'nom':'"+Utilities.addSlashes(u.getName())+"',";
                        json += "'composante':'"+Utilities.addSlashes(u.getComposante())+"',";
                        json += "'codeComposante':'"+Utilities.addSlashes(u.getCodeComposante())+"'},";
                    }
                    json += "]}";
                }else{
                    json = "{'totalCount':'0','data':[]}";
                }
            }
            this.response.setContentType("application/json");
            return json;

        }
        
        
        // Cette méthode retourne le nombre d'utilisateur total ou correspondant à des critères
        public int getUtilisateurCount(){
            
            int utilisateurCount = 0;
            // ------------------------------------------------------------------
            String searchValue = request.getParameter("query");
            String searchColumns = request.getParameter("fields");
            // ------------------------------------------------------------------  
       
            try{
                UtilisateurDAO dao = new UtilisateurDAO();               
                // Si pas de recherche => On affiche tout
                if (searchValue == null || searchColumns==null || "".equals(searchValue) || "".equals(searchColumns)){
                    utilisateurCount = dao.getUtilisateurCount();
                }else{
                    utilisateurCount = dao.getUtilisateurWithQueryCount(searchValue, searchColumns);
                }        
            } catch (Exception ex) {
                Logger.getLogger(UtilisateurController.class.getName()).log(Level.SEVERE, null, ex);
            }    
            
            return utilisateurCount;                        
        }
        
        // Cette méthode retourne tours les utilisateurs ou ceux correspondants à des critères
        public List<Utilisateur> getUtilisateurList(){
            
            List<Utilisateur> utilisateurList = null;
            // ------------------------------------------------------------------
            int maxResults = -1;
            int firstResult = -1;
            try{
                maxResults = Integer.parseInt(request.getParameter("limit"));
                firstResult = Integer.parseInt(request.getParameter("start"));
                if (maxResults == 0){
                    maxResults=10;
                }
            }catch(NumberFormatException e){
            }
            String searchValue = request.getParameter("query");
            String searchColumns = request.getParameter("fields");
            // ------------------------------------------------------------------  
       
            try{
                UtilisateurDAO dao = new UtilisateurDAO();               
                // Si pas de recherche => On affiche tout
                if (searchValue == null || searchColumns==null || "".equals(searchValue) || "".equals(searchColumns)){
                    utilisateurList = dao.findUtilisateurEntities(maxResults, firstResult);
                }else{
                    utilisateurList = dao.findUtilisateurWithQuery(searchValue, searchColumns, maxResults, firstResult);
                }        
            } catch (Exception ex) {
                Logger.getLogger(UtilisateurController.class.getName()).log(Level.SEVERE, null, ex);
            }    
            
            return utilisateurList;
            
            
        }

        
        public String getListItems(){
            this.response.setContentType("application/json");            
            String json = "{'totalCount':'0','data':[]}";
             
            List<Utilisateur> utilisateurList = this.getUtilisateurList();             
            if (utilisateurList !=null && utilisateurList.size()>0){
                json = "{'totalCount':'"+this.getUtilisateurCount()+"','data':[";
                for (Utilisateur u : utilisateurList){
                    json += " {'idUtilisateur':'"+u.getIdUtilisateur()+"',";
                    json += "'nom':'"+Utilities.addSlashes(u.getNom())+"',";
                    json += "'prenom':'"+Utilities.addSlashes(u.getPrenom())+"',";
                    json += "'login':'"+Utilities.addSlashes(u.getLogin())+"',";
                    json += "'email':'"+Utilities.addSlashes(u.getEmail())+"',";
                    json += "'idUtilisateurProfil':'"+u.getUtilisateurProfil().getIdUtilisateurProfil()+"',";
                    json += "'nomProfil':'"+Utilities.addSlashes(u.getUtilisateurProfil().getNom())+"'},";
                }                    
                json += "]}";
            }
            return json;       
            
        }


        // Renvoie les SousStructure de l'utilisateur passé en paramètre
        // si pas d'utilisateur passé en parametre : envoie les sousStructure de l'utilisateur
        public String getListSousStructureUser(){
            String json = "";
            Long idUtilisateur=0L;
            List<SousStructure> sousStructures = null;
            List<SousStructure> sousStructuresSelect = null;
            int idStructure = 0;

            // si pas d'utilisateur passé en parametre : envoie les sousStructure de l'utilisateur
            try{
                idUtilisateur = Long.parseLong(request.getParameter("idUtilisateur"));
                UtilisateurDAO dao = new UtilisateurDAO();
                Utilisateur user = dao.findUtilisateur(idUtilisateur);
                sousStructures = user.getSousStructure();
            }catch(NumberFormatException e){
                sousStructures = utilisateur.getSousStructure();
            }catch (Exception ex) {
                Logger.getLogger(UtilisateurController.class.getName()).log(Level.SEVERE, null, ex);
                return "";
            }

            // Selection d'une structure => On affiche que les sous structure de cette sutructure
            try{
                idStructure = Integer.parseInt(request.getParameter("idStructure"));
                if (idStructure>0){
                    StructureDAO dao = new StructureDAO();
                    Structure ss = dao.findStructure(idStructure);
                    sousStructuresSelect = ss.getSousStructure();
                }
                // On enleve les sousStructures de l'utilisateur
                if (sousStructuresSelect!=null && sousStructuresSelect.size()>0){
                    sousStructures.retainAll(sousStructuresSelect);
                }

            }catch(NumberFormatException e){
                idStructure = 0;
            }



            this.response.setContentType("application/json");

            if (sousStructures !=null){
               json = "{'totalCount':'"+sousStructures.size()+"','data':[";
                for (SousStructure s : sousStructures){
                    json += " {'idSousStructure':'"+s.getIdSousStructure()+"',";
                    json += "'code':'"+Utilities.addSlashes(s.getCode())+"',";
                    json += "'nom':'"+Utilities.addSlashes(s.getNom())+"',";
                    json += "'adresse1':'"+Utilities.addSlashes(s.getAdresse1())+"',";
                    json += "'adresse2':'"+Utilities.addSlashes(s.getAdresse2())+"',";
                    json += "'codePostal':'"+s.getCodePostal()+"',";
                    json += "'ville':'"+Utilities.addSlashes(s.getVille())+"',";
                    json += "'telephone':'"+s.getTelephone()+"',";
                    json += "'fax':'"+s.getFax()+"',";
                    json += "'responsable':'"+Utilities.addSlashes(s.getResponsable())+"',";
                    json += "'idStructure':'"+s.getStructure().getIdStructure()+"'},";
                }
                json += "]}";
            }else{
                json = "{'totalCount':'0','data':[]}";
            }
            
            return json;   
            
            
        }
        
    // Methode utilisée pour les édition XLS et PDF
    // retourne l'utilisateur à partir de l'id. Si l'id n'est pas définit, on a demandé l'impression de la liste (voir méthode getXLSList()
    private Utilisateur getItemFromId(){
        
            Utilisateur utilisateur = null;
            Long idUtilisateur = 0L;
            String idUtilisateurS = request.getParameter("id");
            
            if (idUtilisateurS!=null && !"".equals(idUtilisateurS)){
                try{
                    idUtilisateur = Long.parseLong(idUtilisateurS);
                    if (idUtilisateur > 0 ){
                        try {
                            UtilisateurDAO dao = new UtilisateurDAO();
                            utilisateur = dao.findUtilisateur(idUtilisateur);
                        } catch (Exception ex) {
                            Logger.getLogger(UtilisateurController.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }catch(NumberFormatException ex){
                    Logger.getLogger(UtilisateurController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            return utilisateur;
        
    }
    
    
    public String getAireStockageFavorite(){
        
        return "{'success':true," +
                "'idAireStockage':'"+utilisateur.getAireStockage().getIdAire()+"'," +
                "'libelleAireFavorite':'"+utilisateur.getAireStockage().getNom()+"'}";
        
        
    }
    
          
        
        public String getPDF(){
             //----------------------------------------------------
            //Autorisations en lecture
            String resp = authorizedProfils.getReadAuthorizedJsonTicket();
            if (!"".equals(resp)){
                return resp;
            }
            //----------------------------------------------------   
            
            String json="{'success':false}"; 
            List<String> pdfPictoDanger = null;
            Utilisateur utilisateur = this.getItemFromId();

        
            PdfDocumentFactory pdf = new PdfDocumentFactory("utilisateur_" + utilisateur.getIdUtilisateur());

            pdf.addVerticalTable(utilisateur.getNom(), 24, utilisateur.getDataForVerticalTable(), 0);
            pdf.addHorizontalTable("Structures", 15, SousStructure.getDataForHorizontalTable(utilisateur.getSousStructure()), 1, null);                

            pdf.getDocument().close();     

            if (!"".equals(pdf.getFileName())){
                return "{'success':true,'fileName':'"+pdf.getFileName()+"'}";
            }else{
                return json;   
            }              
        }
        
       
        
        // Cette méthode est appelée quand on veux la liste des utilisateurs (id=0)
        // OU quand on veux éditer 1 utilisateur (id <>0)
        public String getXLSList(){

            String fileName = "";
            String json="{'success':false}"; 
            
            List<Utilisateur> utilisateurList = null;
            
            // on cherche si on demander l'édition d'un seul utilisateur id<>0
            Utilisateur utilisateur = this.getItemFromId();
            //si utilisateur = null => c'est que id=0 car on a demander la liste de tous les utilisateurs
            if (utilisateur==null){                
                utilisateurList = this.getUtilisateurList();
            }else{
                utilisateurList = new ArrayList<Utilisateur>();
                utilisateurList.add(utilisateur);
            }
            
             try {

                DateTimeFormatter FILE_FORMAT = DateTimeFormat.forPattern("yyyyMMddHmsS");
                DateTimeFormatter USER_FORMAT = DateTimeFormat.forPattern("H:m dd/MM/yyyy");


                LocalDateTime now = new LocalDateTime();
                String dateStr = now.toString(FILE_FORMAT);


                fileName = "Utilisateur_"+dateStr+".xls";

                WritableWorkbook wrtWorkbook = Workbook.createWorkbook(new File(fileName));

                WritableSheet sheet1;

                sheet1 = wrtWorkbook.createSheet("Utilisateur", 0);

                Label titre = new Label(0, 0, "Utilisateur, le " + now.toString(USER_FORMAT));
                sheet1.addCell(titre);

                sheet1.addCell(new Label(0, 2, "Nom"));
                sheet1.addCell(new Label(1, 2, "Prenom"));
                sheet1.addCell(new Label(2, 2, "Login"));
                sheet1.addCell(new Label(3, 2, "Email"));
                sheet1.addCell(new Label(4, 2, "Code Profil"));
                sheet1.addCell(new Label(5, 2, "Nom Profil"));       
                    
                int j=3; // pour la ligne
                for (Utilisateur u : utilisateurList){
                    sheet1.addCell(new Label(0, j, u.getNom()));
                    sheet1.addCell(new Label(1, j, u.getPrenom()));
                    sheet1.addCell(new Label(2, j, u.getLogin()));
                    sheet1.addCell(new Label(3, j, u.getEmail()));
                    sheet1.addCell(new Label(4, j, u.getUtilisateurProfil().getCode()));
                    sheet1.addCell(new Label(5, j, u.getUtilisateurProfil().getNom()));
                    int i=6; // colonne
                    int k=1;
                    for (SousStructure ss : u.getSousStructure()){
                        sheet1.addCell(new Label(i, 2, "Code Structure "+k));
                        sheet1.addCell(new Label(i, j, ss.getCode()));
                        i++;
                        sheet1.addCell(new Label(i, 2, "Nom Structure "+k));                        
                        sheet1.addCell(new Label(i, j, ss.getNom()));
                        i++;
                        k++;
                    } 
                    j++; //on change de ligne
                }
             
                wrtWorkbook.write();
                wrtWorkbook.close();

                json = "{'success':true,'fileName':'"+fileName+"'}";


            } catch (IOException ex) {
                Logger.getLogger(UtilisateurController.class.getName()).log(Level.SEVERE, null, ex);
                return "{'success':false}";
            } catch (RowsExceededException ex) {
                Logger.getLogger(UtilisateurController.class.getName()).log(Level.SEVERE, null, ex);
                return "{'success':false}";
            } catch (WriteException ex) {
                Logger.getLogger(UtilisateurController.class.getName()).log(Level.SEVERE, null, ex);
                return "{'success':false}";
            }  

            return json;

        }
                
        

           
    
}
