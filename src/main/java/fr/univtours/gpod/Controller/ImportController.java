package fr.univtours.gpod.Controller;

import fr.univtours.gpod.persistance.DAO.*;
import fr.univtours.gpod.persistance.DAO.exceptions.PreexistingEntityException;
import fr.univtours.gpod.persistance.entities.*;
import fr.univtours.gpod.utils.AuthorizedProfils;
import fr.univtours.gpod.utils.Mail;
import fr.univtours.gpod.utils.Utilities;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.joda.time.DateTime;
import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.io.*;
import java.sql.Timestamp;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * Created with IntelliJ IDEA.
 * User: geoffroy.vibrac
 * Date: 26/03/13
 * Time: 16:04
 */
public class ImportController extends ActionController {

    private Workbook wb;

    public ImportController(HttpServletRequest request, HttpServletResponse response, HttpSession session){
        super(request, response, session);
        authorizedProfils = new AuthorizedProfils(request, response, session);
        authorizedProfils.addReadAuthorisedProfile("AD");
        authorizedProfils.addWriteAuthorisedProfile("AD");
    }

    public String importFile(String filePath, SousStructure sousStructure, AireStockage aireStockage) throws Exception {
        try {
            InputStream inputStream = new FileInputStream(filePath);



            this.wb = WorkbookFactory.create(inputStream);
            Sheet sheet1 = wb.getSheetAt(0);
            int i=0;
            for (Row row : sheet1) {
                if (i>0){ // necessaire pour ne pas traiter les entêtes


                    try{
                        String CAS = row.getCell(0).getStringCellValue().trim();
                        String nom = row.getCell(1).getStringCellValue().trim();
                        String fournisseur = row.getCell(2).getStringCellValue().trim();
                        Double quantite = row.getCell(3).getNumericCellValue();
                        String uniteMesure = row.getCell(4).getStringCellValue().trim().toLowerCase();
                        Double quantiteUnitaire;
                        try{
                            quantiteUnitaire = row.getCell(5).getNumericCellValue();
                        }catch(IllegalStateException e){
                            quantiteUnitaire =  Double.parseDouble(row.getCell(5).getStringCellValue().trim());
                        }
                        if (quantiteUnitaire ==0){
                            quantiteUnitaire = 1.0;
                        }
                        String formule = "";
                        if (!(row.getCell(6) == null)){
                            formule = row.getCell(6).getStringCellValue();
                        }
                        Date dateExcel =   row.getCell(7).getDateCellValue();
                        Timestamp timestampExcel = new Timestamp(dateExcel.getTime());

                        ImportExcel ie = new ImportExcel(CAS, timestampExcel, formule, fournisseur, nom, quantite, quantiteUnitaire, uniteMesure);


                        ImportExcel.getDao().create(ie);
                    }catch(Exception e){

                    }
                }
                i++;
            }

            this.addData(sousStructure, aireStockage);










        } catch (IOException e) {
            return "{success:false, errors:{reason:'"+ Utilities.addSlashes(e.getMessage().substring(0,50)) + "'}}";
        } catch (InvalidFormatException e) {
            return "{success:false, errors:{reason:'"+Utilities.addSlashes(e.getMessage().substring(0,50))+"'}}";
        } catch (Exception e) {
            Throwable t =  e;
            Throwable t2 = null;
            do {
                t2 = t;
                t = t.getCause();
            } while(t != null);

            // si problème, on supprime les ligne non inseree
            ImportExcel.getDao().destroyAllNew();

            return "{success:false, errors:{reason:'"+Utilities.addSlashes(t2.getMessage())+"'}}";
        }
        return "";


    }


    public void addData(SousStructure sousStructure, AireStockage aireStockage) throws Exception {

        SubstanceDAO substanceDao = new SubstanceDAO();
        FournisseurDAO fournisseurDao = new FournisseurDAO();
        UniteMesureDAO uniteMesureDao = new UniteMesureDAO();
        ContenantDAO contenantDAO = new ContenantDAO();
        ProduitDAO produitDao = new ProduitDAO();
        ProduitConditionnementDAO produitConditionnementDAO = new ProduitConditionnementDAO();
        LivraisonDAO livraisonDao = new LivraisonDAO();

        List<ImportExcel> importExcelList = ImportExcel.getDao().getImport(true,  true, 0,0);

        LocalDateTime yo = new LocalDateTime();
        String dateStr = yo.toString(DateTimeFormat.forPattern("yyyyMMddHmsS"));
        String fileName =  "logImport_"+dateStr+".csv";
        PrintWriter writer = new PrintWriter(session.getAttribute("absolutePath") +  "/" + fileName, "UTF-8");

        writer.println("CAS;idProduit;idProduitConditionnement;idLivraison;idArticle");

            for (ImportExcel ie : importExcelList){
                   String csvLine = "";



                    // Création / récupération de la substance
                    Substance s = new Substance(ie.getImportExcelPK().getCas(), ie.getImportExcelPK().getNom(), ie.getImportExcelPK().getFormuleBrut(), "","");
                    Substance sub =  substanceDao.findSubstance(s.getCas());
                    if (sub == null){
                          substanceDao.create(s);
                    }else{
                        s = sub;
                    }
                    csvLine += s.getCas()+";";

                    // récupération du fournisseur
                    Fournisseur f;
                    List<Fournisseur> fournisseurs = fournisseurDao.findFournisseurEntitiesStrictEqual(true, ie.getImportExcelPK().getFourniseur().trim(), "nom", 0,0);
                    if (fournisseurs.size() == 1){
                        f = fournisseurs.get(0);
                    }else{
                        // on récupère le fournisseur inconnu
                        fournisseurs = fournisseurDao.findFournisseurEntities(true, "inconnu", "nom", 0, 0);
                        if (fournisseurs.size() == 1){
                            f = fournisseurs.get(0);
                        }else{
                            throw new ImportException("Impossible de trouver le fournisseur inconnu ou " + ie.getImportExcelPK().getFourniseur());
                        }
                    }


                    // PRODUIT CONDITIONNEMENT
                    // Unité de Mesure
                    UniteMesure um = null;
                    List<UniteMesure> unitesMesure = uniteMesureDao.findUniteMesureEntitiesByLibelle(true, 0, 0, ie.getImportExcelPK().getUniteMesure());
                    if (unitesMesure.size() == 1){
                        um =   unitesMesure.get(0);
                    }else{
                        throw new ImportException("Impossible de trouver unité de mesure : " + ie.getImportExcelPK().getUniteMesure());
                    }
                    // Contenant : Toujours le verre
                    Contenant c =   contenantDAO.findContenant(1);

                    // création d'un nouveau conditionnement
                    ProduitConditionnement produitC = new ProduitConditionnement("", ie.getImportExcelPK().getQuantite(), c, um, null);
                    // il faut mettre ce conditionnement ans une liste
                    List<ProduitConditionnement> produitConditionnements = new ArrayList();
                    produitConditionnements.add(produitC);

                    // on recherche si le produit existe déjà
                    List<Produit> searchProduits = produitDao.findProduitEntities(f.getIdFournisseur(),s.getCas(),ie.getImportExcelPK().getNom(),0);
                    Produit produit = null;

                    // si le produit existe déjà
                    if (searchProduits.size()>0){
                        produit = searchProduits.get(0);
                        // on cherche si le produitConditionnement n'existe pas déjà
                        List<ProduitConditionnement> lcs = produitConditionnementDAO.findProduitConditionnementEntities(produit.getIdProduit(), "", ie.getImportExcelPK().getQuantite(),um.getIdUniteMesure(),c.getIdContenant(),0);
                        // si liste vide, on créé le conditionnement
                        if (lcs.size()==0){
                            produitC.setProduit(produit);
                            produitConditionnementDAO.create(produitC);
                        //sinon, le conditionnement
                        }else{
                            produitC = lcs.get(0);
                        }
                    }else{
                        produit = new Produit();
                        produit.setNom(ie.getImportExcelPK().getNom());
                        produit.setSubstance(s);
                        produit.setPictogrammes(s.getPictogrammes());
                        produit.setDensite(0.0);
                        produit.setMasseMolaire(0.0);
                        produit.setPointDeFusion(0.0);
                        produit.setPointdEbullition(0.0);
                        produit.setFournisseur(f);
                        produit.setDescription("Importé le " + ie.getImportExcelPK().getDateImport());
                        produit.setConditionnements(produitConditionnements);
                        produitDao.create(produit);
                    }

                    csvLine += produit.getIdProduit() + ";" + produitC.getIdProduitConditionnement() + ";";


                    DateTime now = new DateTime();
                    int qte =   (int)ie.getImportExcelPK().getQuantiteUnitaire();
                    LivraisonLigne livraisonLigne = new LivraisonLigne(qte, true, produitC);
                    ArrayList<LivraisonLigne> livraisonLigneList = new ArrayList<LivraisonLigne>();
                    livraisonLigneList.add(livraisonLigne);
                    List<Article> articles = new ArrayList();

                    for(int i=0;i<qte; i++){
                        articles.add(new Article(produitC.getIdProduitConditionnement(), sousStructure.getIdSousStructure(), aireStockage.getIdAire(), 0, ""));
                    }
                    Livraison livraison = new Livraison(now.toDate(), "GENAUTO_"+ie.getImportExcelPK().getDateImport(), null, this.utilisateur, livraisonLigneList, articles);




                    livraisonDao.create(livraison);

                    csvLine +=livraison.getIdLivraison() + ";";
                    for (int i=0; i<qte; i++){
                        writer.println(csvLine+livraison.getArticleList().get(i).getIdArticle() + ";");
                    }

                    ie.setInsere(1);
                    ImportExcel.getDao().edit(ie);

            }
        writer.close();

        try{
            Mail m = new Mail();
            m.addSenderToCc();
            System.out.println(request.getServerName());
            m.sendMail(utilisateur.getEmail(), "[gpod]["+request.getServerName()+"] import Excel gpod","",""+session.getAttribute("absolutePath"), fileName);
        }catch (Exception e ){
            // pas d'erreur si le mail ne passe pas
        }

    }





 class ImportException extends Exception{

     ImportException() {
     }

     ImportException(Throwable cause) {
         super(cause);
     }

     ImportException(String message) {
         super(message);
     }

     ImportException(String message, Throwable cause) {
         super(message, cause);
     }

     ImportException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
         super(message, cause, enableSuppression, writableStackTrace);
     }
 }


}
