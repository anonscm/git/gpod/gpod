/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2012/01/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/

package fr.univtours.gpod.Controller;

import fr.univtours.gpod.persistance.entities.Pictogramme;
import fr.univtours.gpod.persistance.DAO.PictogrammeDAO;
import fr.univtours.gpod.persistance.DAO.EtatPhysiqueDAO;
import fr.univtours.gpod.persistance.entities.EtatPhysique;
import fr.univtours.gpod.persistance.DAO.FournisseurDAO;
import fr.univtours.gpod.persistance.entities.Substance;
import fr.univtours.gpod.persistance.DAO.SubstanceDAO;
import fr.univtours.gpod.exceptions.NonexistentEntityException;
import fr.univtours.gpod.persistance.entities.Produit;
import fr.univtours.gpod.persistance.entities.Fournisseur;
import fr.univtours.gpod.persistance.DAO.TagDAO;
import fr.univtours.gpod.persistance.entities.Tag;
import fr.univtours.gpod.persistance.entities.MentionDanger;
import fr.univtours.gpod.persistance.DAO.MentionDangerDAO;
import fr.univtours.gpod.persistance.DAO.ProduitDAO;
import fr.univtours.gpod.HTML.HTML;
import fr.univtours.gpod.json.JsonConversionUtilities;
import fr.univtours.gpod.persistance.DAO.ProduitConditionnementDAO;
import fr.univtours.gpod.persistance.entities.ProduitConditionnement;
import fr.univtours.gpod.utils.AuthorizedProfils;
import fr.univtours.gpod.utils.ESISSearch;
import fr.univtours.gpod.utils.Utilities;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.lang.StringEscapeUtils;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.nodes.TextNode;


public class ProduitController extends ActionController {

     
        
        public ProduitController(HttpServletRequest request, HttpServletResponse response, HttpSession session){
            super(request, response, session);
            authorizedProfils = new AuthorizedProfils(request, response, session);
            authorizedProfils.addReadAuthorisedProfile("AD","LA","HY","ME");
            authorizedProfils.addWriteAuthorisedProfile("AD","LA");                 
        }


        public List<Produit> getListProduit(){
            return this.getListProduit(-1, 0, "", "");
        }
        
        private List<Produit> getListProduit(int maxResults, int firstResult, String searchValue, String searchColumns){
            List<Produit> produits = null;
            try {
                ProduitDAO dao = new ProduitDAO();
                produits = dao.findProduitEntities(searchValue, searchColumns, maxResults, firstResult);
            } catch (Exception ex) {
                Logger.getLogger(ProduitController.class.getName()).log(Level.SEVERE, null, ex);
            }

            return produits;
        }
        
        private int getProduitCount(String searchValue, String searchColumns){
            int nbItems = 0;
            try {
                ProduitDAO dao = new ProduitDAO();
                nbItems = dao.getProduitWithQueryCount(searchValue, searchColumns);
            } catch (Exception ex) {
                Logger.getLogger(ProduitController.class.getName()).log(Level.SEVERE, null, ex);
            }

            return nbItems;
        }     
        
        private List<Produit> getListProduitSearch(int idFournisseur, String CAS, String nom, int idProduitConditionnement){
            List<Produit> produits = null;
            try {
                ProduitDAO dao = new ProduitDAO();
                produits = dao.findProduitEntities(idFournisseur,  CAS, nom, idProduitConditionnement);
            } catch (Exception ex) {
                Logger.getLogger(ProduitController.class.getName()).log(Level.SEVERE, null, ex);
            }

            return produits;
        }    

        
        public String getListItems(){
   
            
            this.response.setContentType("application/json");
            
            String json = "";
            List<Produit> produits = null;
            // ------------------------------------------------------------------
            int maxResults = -1;
            int firstResult = -1;
            try{
                maxResults = Integer.parseInt(request.getParameter("limit"));
                firstResult = Integer.parseInt(request.getParameter("start"));
                if (maxResults == 0){
                    maxResults=10;
                }
            }catch(NumberFormatException e){
            }
            String searchValue = request.getParameter("query");
            String searchColumns = request.getParameter("fields");
            // ------------------------------------------------------------------  

            
       
            produits = getListProduit(maxResults, firstResult, searchValue, searchColumns);
            int nbItems = getProduitCount(searchValue, searchColumns);

            this.response.setContentType("application/json");

            if (produits !=null && produits.size()>0){
                json = "{'totalCount':'"+nbItems+"','data':[";
                for (Produit u : produits){
                    json += " {'idProduit':'"+u.getIdProduit()+"',";
                    json += " 'substance':'"+Utilities.addSlashes(u.getSubstance().getCas())+"',";
                    json += "'nom':'"+Utilities.addSlashes(u.getNom())+"',";
                    json += "'description':'"+Utilities.addSlashes(u.getDescription())+"',";
                    json += "'nomFournisseur':'"+Utilities.addSlashes(u.getFournisseur().getNom())+"',";
                    json += "'refrigere':'"+u.isRefrigere()+"',";
                    json += "'write':'"+AuthorizedProfils.isWriteAllowed(utilisateur.getUtilisateurProfil().getCode(), "AD", "LA")+"'},";
                }
                json += "]}";
            }else{
                json = "{'totalCount':'0','data':[]}";
            }

            return json;
        }

        
       // --------------------------------------------------------------
        // retourne la liste des conditionnement POUR UN PRODUIT
        public String getListProduitAvecConditionnement(){
            this.response.setContentType("application/json");
            String json = "{'success':'false'}";
            List<Produit> produits = null;
            List<ProduitConditionnement> conditionnements = new ArrayList<ProduitConditionnement>();
            int maxResults = -1;
            int firstResult = -1;

            try{
                maxResults = Integer.parseInt(request.getParameter("limit"));
                firstResult = Integer.parseInt(request.getParameter("start"));
                if (maxResults == 0){
                    maxResults=10;
                }
            }catch(NumberFormatException e){
            }

            String searchValue = request.getParameter("query");
            String searchColumns = request.getParameter("fields");
            
            String idFournisseurS = request.getParameter("idFournisseur");
            int idFournisseur = 0;
            try{
                idFournisseur = Integer.parseInt(idFournisseurS);
            }catch(NumberFormatException e){
                idFournisseur = 0;
            }
            String CAS = request.getParameter("CAS");
            String nom = request.getParameter("nom");
            int idProduitConditionnement;
            String idProduitConditionnementS = request.getParameter("idProduitConditionnement");
            try{
                idProduitConditionnement = Integer.parseInt(idProduitConditionnementS);
            }catch(NumberFormatException e){
                idProduitConditionnement = 0;
            }


            // si on a saisi un fournisseur ou un CAS ou un nom
            if (idProduitConditionnement > 0 || idFournisseur > 0 || (CAS!=null && !"".equals(CAS)) || (nom!=null && !"".equals(nom))){
                produits = getListProduitSearch(idFournisseur, CAS, nom, idProduitConditionnement);
            }else{  
                produits =  getListProduit(maxResults, firstResult, searchValue, searchColumns);
            }
            if (produits !=null && produits.size()>0){
                for (Produit u : produits){
                    conditionnements.addAll(u.getConditionnements());
                }
            }

            this.response.setContentType("application/json");

            if (conditionnements !=null && conditionnements.size()>0){
                json = "{'totalCount':'"+conditionnements.size()+"','data':[";
                for (ProduitConditionnement pc : conditionnements){
                    if (!pc.getDeleted()){
                        json += " {'idProduitConditionnement':'"+pc.getIdProduitConditionnement()+"',";
                        json += " 'idProduit':'"+pc.getProduit().getIdProduit()+"',";
                        json += " 'substance':'"+Utilities.addSlashes(pc.getProduit().getSubstance().getCas())+"',";
                        json += "'nom':'"+Utilities.addSlashes(pc.getProduit().getNom())+"',";
                        json += "'nomFournisseur':'"+Utilities.addSlashes(pc.getProduit().getFournisseur().getNom())+"',";
                        json += "'referenceFournisseur':'"+Utilities.addSlashes(pc.getReferenceFournisseur())+"',";
                        json += "'idFournisseur':'"+pc.getProduit().getFournisseur().getIdFournisseur()+"',";
                        json += "'contenance':'"+pc.getContenance()+" "+pc.getUniteMesure().getAbreviation()+"',";
                        json += "'refrigere':'"+pc.getProduit().isRefrigere()+"'},";
                    }
                }
                json += "]}";
            }else{
                json = "{'totalCount':'0','data':[]}";
            }

            return json; 
        }
        // --------------------------------------------------------------

        
        public String getMolecule(){
            this.response.setContentType("application/text");
            String json = "{'success':'false'}";
            String sdf = "";
            Substance s = null;
            String CAS = request.getParameter("CAS");
            SubstanceDAO dao = new SubstanceDAO();
            if (!"".equals(CAS)){                
                s = dao.findSubstance(CAS);
                if (s!=null){
                    sdf = s.getSdf();
                }                
            } 
            if (sdf == null || "".equals(sdf)){
                String url = "http://cactus.nci.nih.gov/chemical/structure/" + CAS + "/file?format=sdf";
                
                try{
                    
                        Connection connection = Jsoup.connect(url);
                        connection.header("Accept-Language", "fr");

                        Document doc = connection.get();
                        Element body =  doc.body();
                        

                        if (body != null){                    

                            TextNode tn = (TextNode)body.childNodes().get(0);
                            
                            sdf = tn.getWholeText().replace("\n","\r\n");
                            if (!"".equals(sdf)){
                                s.setSdf(sdf);
                                try {
                                    dao.edit(s);
                                    sdf = s.getSdf();
                                } catch (fr.univtours.gpod.persistance.DAO.exceptions.NonexistentEntityException ex) {
                                    Logger.getLogger(ProduitController.class.getName()).log(Level.SEVERE, null, ex);
                                } catch (Exception ex) {
                                    Logger.getLogger(ProduitController.class.getName()).log(Level.SEVERE, null, ex);
                                }
                            }


                        }
                }catch(IOException ex){
                    Logger.getLogger(ProduitController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            
           if (sdf == null || "".equals(sdf)){
               this.response.setContentType("application/json");
               json = "{'success':'false'}";
               return json;
           }
            
            return sdf;
            
        }


        public String getListMention(){
            
            
            String json = "";
            int idProduit = 0;
            List<MentionDanger> mentions = null;
                                
            this.response.setContentType("application/json");
       
            // Récupération de idUtilisateur dans le cas d'appel depuis Utilisateur
            try{
                idProduit = Integer.parseInt(request.getParameter("idProduit"));
            }catch(NumberFormatException e){
                idProduit = 0;
            }            
                 
            if (idProduit == 0){
                json = "{'totalCount':'0','data':[]}";
                return json;
            }
            
            try {
                
                
                ProduitDAO dao = new ProduitDAO();               
                Produit produit = dao.findProduit(idProduit);
                if (produit == null){
                    json = "{'totalCount':'0','data':[]}";
                    return json;    
                }
                mentions = produit.getMentions();


                
                if (mentions !=null && mentions.size()>0){

                    json = "{'totalCount':'"+mentions.size()+"','data':[";
                    for (MentionDanger u : mentions){                        
                        
                        
                        json += " {'idMention':'"+u.getIdMention()+"',";
                        json += "'code':'"+Utilities.addSlashes(u.getCode())+"',";
                        json += "'libelle':'"+Utilities.addSlashes(u.getLibelle())+"'},";
                  
                        
                    }                    
                    json += "]}";
                    
                }else{
                    json = "{'totalCount':'0','data':[]}";
                }
            } catch (Exception ex) {
                Logger.getLogger(ProduitController.class.getName()).log(Level.SEVERE, null, ex);
                json = "{'totalCount':'0','data':[]}";
            }
            return json;             
        }
        
        
        
        public String getListTag(){
            
            
            String json = "";

            List<Tag> tags = null;
            int idProduit = 0;
            this.response.setContentType("application/json");
       
            // Récupération de idUtilisateur dans le cas d'appel depuis Utilisateur
            try{
                idProduit = Integer.parseInt(request.getParameter("idProduit"));
            }catch(NumberFormatException e){
                idProduit = 0;
            }   
       
            if (idProduit == 0){
                json = "{'totalCount':'0','data':[]}";
                return json;
            }
            
            try {
                ProduitDAO dao = new ProduitDAO();               
                Produit produit = dao.findProduit(idProduit);
                if (produit == null){
                    json = "{'totalCount':'0','data':[]}";
                    return json;    
                }                
                tags = produit.getTags();
    
                

                
                if (tags !=null && tags.size()>0){
                   
                    
                    json = "{'totalCount':'"+tags.size()+"','data':[";
                    for (Tag u : tags){                        
                        
                        
                        json += " {'idtag':'"+u.getIdtag()+"',";
                        json += "'libelle':'"+Utilities.addSlashes(u.getLibelle())+"'},";
                  
                        
                    }                    
                    json += "]}";
                    
                }else{
                    json = "{'totalCount':'0','data':[]}";
                }
            } catch (Exception ex) {
                Logger.getLogger(ProduitController.class.getName()).log(Level.SEVERE, null, ex);
                json = "{'totalCount':'0','data':[]}";
            }
            return json;                
        }
        
        public String getProduit(){
   
            
            this.response.setContentType("application/json");            
            String json = "";
            Produit produit = null;
            int idProduit = Integer.parseInt(request.getParameter("idProduit"));  
            
            
            try {
            
                ProduitDAO dao = new ProduitDAO();                  
                produit = dao.findProduit(idProduit);
                
                if (produit !=null){
                    json = "{'success':'true','data':";
                    json += " {'idProduit':'"+produit.getIdProduit()+"',";
                    json += " 'nomSubstance':'"+Utilities.addSlashes(produit.getSubstance().getNom())+"',";
                    json += " 'substance':'"+Utilities.addSlashes(produit.getSubstance().getCas())+"',";
                    json += "'nom':'"+Utilities.addSlashes(produit.getNom())+"',";
                    json += "'idFournisseur':'"+produit.getFournisseur().getIdFournisseur()+"',";   
                    json += "'description':'"+Utilities.addSlashes(produit.getDescription())+"',";
                    // Onglet 2
                    json += "'purete':'"+produit.getPurete()+"',";
                    json += "'densite':'"+produit.getDensite()+"',";
                    json += "'pointDeFusion':'"+produit.getPointDeFusion()+"',";
                    json += "'pointdEbullition':'"+produit.getPointdEbullition()+"',";
                    json += "'masseMolaire':'"+produit.getMasseMolaire()+"',";
                    String idEtatPhysique = "";
                    if (produit.getEtatPhysique()!=null){
                        idEtatPhysique = String.valueOf(produit.getEtatPhysique().getIdEtatPhysique());
                    }                     
                    json += "'idEtatPhysique':'"+idEtatPhysique+"',";
                    json += "'refrigere':'"+produit.isRefrigere()+"',";
                    // Onglet 3
                    json += "'pictogrammes':'"+produit.getListPictogrammes()+"',";            
                    json += "'fds':'"+produit.getFicheSecurite()+"',";
                     
                    
                    
                    json += "}}";
                }else{
                    json = "{'success':'false'}";
                }
                           
            
            } catch (Exception ex) {
                Logger.getLogger(ProduitController.class.getName()).log(Level.SEVERE, null, ex);
                json = "{'success':'false'}";
            }
            
            
            return json; 
        }
        
        
        
        public String saveItem(){
            //----------------------------------------------------
            //Autorisations         
            String resp = authorizedProfils.getWriteAuthorizedJsonTicket();
            if (!"".equals(resp)){
                return resp;
            }
            //----------------------------------------------------                    
            
            String json="";
            Integer idProduit=0;
            ProduitDAO dao = null;
            Substance substancemere = null;
            Fournisseur fournisseur = null;
            EtatPhysique etatPhysique = null;
            List<Pictogramme> pictogrammes = new ArrayList<Pictogramme>(); 
            String[] idMentions = null;
            List<MentionDanger> mentions = new ArrayList<MentionDanger>();   
            String[] idTags = null;
            List<Tag> tags = new ArrayList<Tag>();
            List<ProduitConditionnement> produitsConditionnement= null;
            
            
            this.response.setContentType("application/json");

// ONGLET 1            
            
            try{
                idProduit = Integer.parseInt(request.getParameter("idProduit"));
            }catch(NumberFormatException e){
                idProduit = 0;
            }
            
            String CAS = request.getParameter("substance");
            if (CAS!= null && CAS.length()>0){                    
                SubstanceDAO daosub = new SubstanceDAO();
                substancemere = daosub.findSubstance(CAS);
            }
            
            
            
            String nom = request.getParameter("nom");
            
            int idFournisseur = 0;
            try{
                idFournisseur = Integer.parseInt(request.getParameter("idFournisseur"));
                if (idFournisseur>0){                    
                   FournisseurDAO daofour = new FournisseurDAO();
                   fournisseur = daofour.findFournisseur(idFournisseur);
                }
                            
            }catch(NumberFormatException e){
            }
            String description = request.getParameter("description");
            
// FIN ONGLET 1
// ONGLET 2            
              
            double purete =0;
            try{            
                  purete  = Double.parseDouble(request.getParameter("purete").replaceAll(",", "."));
            }catch(NumberFormatException e){
            }  
            
            double densite =0;
            try{            
                  densite  = Double.parseDouble(request.getParameter("densite").replaceAll(",", "."));
            }catch(NumberFormatException e){
            }  
            
            double pointDeFusion =0;
            try{            
                  pointDeFusion  = Double.parseDouble(request.getParameter("pointDeFusion").replaceAll(",", "."));
            }catch(NumberFormatException e){
            }          
            
            double pointdEbullition =0;
            try{            
                  pointdEbullition  = Double.parseDouble(request.getParameter("pointdEbullition").replaceAll(",", "."));
            }catch(NumberFormatException e){
            }   
            
            double masseMolaire =0;
            try{            
                  masseMolaire  = Double.parseDouble(request.getParameter("masseMolaire").replaceAll(",", "."));
            }catch(NumberFormatException e){
            }               
            
   
            int idEtatPhysique =0;
            try{            
                  idEtatPhysique  = Integer.parseInt(request.getParameter("idEtatPhysique"));
                  if (idEtatPhysique>0){                    
                    EtatPhysiqueDAO daoep = new EtatPhysiqueDAO();
                    etatPhysique = daoep.findEtatphysique(idEtatPhysique);
                  }    
            }catch(NumberFormatException e){
            }
            
            boolean refrigere;
            try{
                refrigere = Boolean.parseBoolean(request.getParameter("refrigere"));
            }catch(NumberFormatException e){
                refrigere = false;
            }
            
            
// FIN ONGLET 2
// ONGLET 3
            String condtionnement = request.getParameter("conditionnement");
            produitsConditionnement = JsonConversionUtilities.jsonProduitConditionnemenArrayToList(condtionnement);


// FIN ONGLET 3
// ONGLET 4
            
            String[] idPictogrammes = request.getParameterValues("idPictogramme");
            if (idPictogrammes!= null && idPictogrammes.length>0){                    
                PictogrammeDAO daoPicto = new PictogrammeDAO();
                for (String ids : idPictogrammes){
                    int id = Integer.parseInt(ids);
                    Pictogramme p = daoPicto.findPictogramme(id);
                    if (p!=null){
                        pictogrammes.add(p);
                    }
                }
            }
            
            
            String idmentionsString = request.getParameter("idmentions");
            if (!"".equals(idmentionsString)){
                idMentions = idmentionsString.split(",");
            }
            if (idMentions!= null && idMentions.length>0){                    
                MentionDangerDAO daoMEntion = new MentionDangerDAO();
                for (String ids : idMentions){
                    try{
                        int id = Integer.parseInt(ids);
                        MentionDanger m = daoMEntion.findMentionDanger(id);
                        if (m!=null){
                            mentions.add(m);
                        }                        
                    }catch(NumberFormatException e){
                    }
                }
            }                                   
            String fds = request.getParameter("fds");

// FIN ONGLET 4
// ONGLET 5
            
            String idtagstring = request.getParameter("idtags");
            if (!"".equals(idtagstring)){
                idTags = idtagstring.split(",");
            }
            if (idTags!= null && idTags.length>0){                    
                TagDAO daotag = new TagDAO();
                for (String ids : idTags){
                    try{
                        int id = Integer.parseInt(ids);
                        Tag t = daotag.findTag(id);
                        if (t!=null){
                            tags.add(t);
                        }                        
                    }catch(NumberFormatException e){
                    }
                }
            } 
// FIN ONGLET 5
            
            // Test des champs
            if ("".equals(nom) || substancemere==null || fournisseur == null){
                json = "{success: false, errors:{reason:\'Des champs obligatoires n\\\'ont pas été saisis\'}}";
                return json;          
            }
                      
            try{
                 dao = new ProduitDAO();              
            } catch (Exception ex) {
                Logger.getLogger(ProduitController.class.getName()).log(Level.SEVERE, null, ex);
                json = "{success: false, errors:{reason:\'"+ex.getMessage()+"\'}}";
                return json;
            }
            
            
            // Ajout nouveau produit idProduit = 0
            if (idProduit == 0){

                    Produit produit = new Produit(nom);
                    produit.setSubstance(substancemere);
                    produit.setFournisseur(fournisseur);
                    produit.setDescription(description);
                    produit.setPurete(purete);
                    produit.setDensite(densite);
                    produit.setPointDeFusion(pointDeFusion);
                    produit.setPointdEbullition(pointdEbullition);
                    produit.setMasseMolaire(masseMolaire);
                    produit.setEtatPhysique(etatPhysique);
                    produit.setPictogrammes(pictogrammes);
                    produit.setMentions(mentions);
                    produit.setFicheSecurite(fds);
                    produit.setTags(tags);
                    produit.setRefrigere(refrigere);
                    produit.setConditionnements(produitsConditionnement);
                    
                    
                    
                    if (produit !=null){
                        try{
                            dao.create(produit);
                            json = "{success: true}";
                        } catch (Exception ex) {
                            Logger.getLogger(ProduitController.class.getName()).log(Level.SEVERE, null, ex);
                            json = "{success: false, errors:{reason:\'"+ex.getMessage()+"\'}}";
                        }
                    }                                  

          
            // UPDATE fournisseur idFournisseur > 0
            }else if (idProduit > 0L){
                

                    Produit produit = dao.findProduit(idProduit);
                 
                    if (produit !=null){
                    
                    produit.setNom(nom);
                    produit.setSubstance(substancemere);
                    produit.setFournisseur(fournisseur);
                    produit.setDescription(description);
                    produit.setPurete(purete);
                    produit.setDensite(densite);
                    produit.setPointDeFusion(pointDeFusion);
                    produit.setPointdEbullition(pointdEbullition);
                    produit.setMasseMolaire(masseMolaire);                    
                    produit.setEtatPhysique(etatPhysique);
                    produit.setPictogrammes(pictogrammes);
                    produit.setMentions(mentions);
                    produit.setFicheSecurite(fds);
                    produit.setTags(tags);
                    produit.setRefrigere(refrigere);
                    produit.setConditionnements(produitsConditionnement);

                        
                        try{
                            dao.edit(produit);
                            json = "{success: true}"; 
                        } catch (NonexistentEntityException ex) {
                            Logger.getLogger(ProduitController.class.getName()).log(Level.SEVERE, null, ex);
                            json = "{success: false, errors:{reason:\'"+ex.getMessage()+"\'}}";  
                        } catch (Exception ex) {
                            Logger.getLogger(ProduitController.class.getName()).log(Level.SEVERE, null, ex);
                            json = "{success: false, errors:{reason:\'"+ex.getMessage()+"\'}}";  
                        }                            
                    }                       
                    
        
            }else{
                json = "{success: false, errors:{reason:\'Impossible de trouver "+idFournisseur+"\'}}";
            }
    
           
            return json;            
            //return "{success: false, errors:{reason:\'"+msg+"\'}};
        }

 

        // --------------------------------------------------------------
        // retourne la liste des conditionnement POUR UN PRODUIT
        public String getListConditionnement(){
            this.response.setContentType("application/json");
            String json = "{'success':'false'}";
            List<ProduitConditionnement> produitConditionnement = null;
            int idProduit = Integer.parseInt(request.getParameter("idProduit"));

            try {
                ProduitConditionnementDAO dao = new ProduitConditionnementDAO();
                if (dao==null){
                    return "{'success':'false'}";
                }
                ProduitDAO pdao = new ProduitDAO();
                if (pdao==null){
                    return "{'success':'false'}";
                }
                Produit prod = pdao.findProduit(idProduit);
                if (prod==null){
                    return "{'success':'false'}";
                }
                produitConditionnement = dao.findProduitConditionnementByProduit(prod);

                if (produitConditionnement != null && produitConditionnement.size()>0){                 
                    json = "{'totalCount':'"+produitConditionnement.size()+"','data':[";
                    int i =0;
                    for (ProduitConditionnement c : produitConditionnement){
                        json += "{'ligneNum':'"+(++i)+"',";
                        json += "'idProduitConditionnement':'"+c.getIdProduitConditionnement()+"',";
                        json += "'reference':'"+c.getReferenceFournisseur()+"',";
                        json += "'contenance':'"+c.getContenance()+"',";
                        json += "'idUniteMesure':'"+c.getUniteMesure().getIdUniteMesure()+"',";
                        json += "'idContenant':'"+c.getContenant().getIdContenant()+"'},";
                    }
                    json += "]}";
                }else{
                    json = "{'totalCount':'0','data':[]}";
                }
            } catch (Exception ex) {
                Logger.getLogger(ProduitController.class.getName()).log(Level.SEVERE, null, ex);
                json = "{'success':'false'}";
            }


            return json; 
        }
        // --------------------------------------------------------------
        
        
        public String getHtml(){
            //----------------------------------------------------
            //Autorisations         
            String resp = authorizedProfils.getAuthorizedHtmlTicket();
            if (!"".equals(resp)){
                return resp;
            }
            //----------------------------------------------------  
            
            
                response.setContentType("text/html;charset=iso-8859-1");
   
                HTML html = HTML.getInstance();
                html.getHeader().setTitle("Gestion des produits dangereux &gt; Gestion des produits");
                html.setExtjsHeader();
                html.getHeader().getCss().addLine("lib/multiselect/Multiselect.css");
                html.getHeader().getCss().addLine("lib/search/gridsearch.css");
                html.getHeader().getCss().addLine("lib/msg/msg.css");
                html.getHeader().getCss().addLine("lib/chemdoodle/ChemDoodleWeb.css");
                html.getHeader().getCss().addLine("lib/keyboard/virtualkeyboard.css");                

                html.getHeader().getJs().addBlock("var B_nameUser = '"+Utilities.addSlashes(utilisateur.getNom())+"'");
                html.getHeader().getJs().addBlock("var B_lastCx = '"+Utilities.javaDateTIMEToString(utilisateur.getDateAvantDerniereConnexion())+"'");                 
                html.getHeader().getJs().addLine("version.js");
                html.getHeader().getJs().addLine("lib/keyboard/Ext.ux.VirtualKeyboard.js");
                html.getHeader().getJs().addLine("lib/keyboard/Ext.ux.plugins.VirtualKeyboard.js");  
                html.getHeader().getJs().addLine("lib/encoder.js");                
                html.getHeader().getJs().addLine("lib/printAction.js");
                html.getHeader().getJs().addLine("lib/msg/msg.js");
                html.getHeader().getJs().addLine("lib/checkbox/remotecheckboxgroup.js");
                html.getHeader().getJs().addLine("lib/upload/FileUploadField.js");
                html.getHeader().getJs().addLine("lib/upload/uploadWindow.js");
                html.getHeader().getJs().addLine("lib/multiselect/Multiselect.js");
                html.getHeader().getJs().addLine("lib/multiselect/ItemSelector.js");   
                html.getHeader().getJs().addLine("lib/search/gridSearch.js");  
                html.getHeader().getJs().addLine("lib/excelButton.js");
                html.getHeader().getJs().addLine("ui/search/substance.js");  
                html.getHeader().getJs().addLine("ui/search/produit.js");  
                
                html.getHeader().getJs().addLine("lib/chemdoodle/ChemDoodleWeb-libs.js");
                html.getHeader().getJs().addLine("lib/chemdoodle/ChemDoodleWeb.js");
                
                html.getHeader().getJs().addLine("ui/container.Produit.js");
                html.getHeader().getJs().addLine("ui/menus/menu."+ session.getAttribute("profil") +".js");
                html.getHeader().getJs().addLine("ui/squelette.js");
                html.getBody().setBodyElement("<table align='center' class='contenttable' cellpadding='0' cellspacing='0' border='0'><tr><td id='centerPanel'></td></tr></table>");
                html.getBody().setBodyElement("<form method='post' enctype='multipart/form-data' id='formUp'></form>");
                html.getBody().setBodyElement("<div id='iframediv'>");

                return html.getHTMLCode();
        }
        
        // Methode utilisée pour les édition XLS et PDF
        // retourne l'utilisateur à partir de l'id. Si l'id n'est pas définit, on a demandé l'impression de la liste (voir méthode getXLSList()
        private Produit getItemFromId(){

                Produit produit = null;
                int idProduit= 0;
                String idProduitS = request.getParameter("id");

                if (idProduitS!=null && !"".equals(idProduitS)){
                    try{
                        idProduit = Integer.parseInt(idProduitS);
                        if (idProduit > 0 ){
                            try {
                                ProduitDAO dao = new ProduitDAO();
                                produit = dao.findProduit(idProduit);
                            } catch (Exception ex) {
                                Logger.getLogger(ProduitController.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }
                    }catch(NumberFormatException ex){
                        Logger.getLogger(ProduitController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                return produit;

        }              
        
        public String getPDF(){
             //----------------------------------------------------
            //Autorisations en lecture
            String resp = authorizedProfils.getReadAuthorizedJsonTicket();
            if (!"".equals(resp)){
                return resp;
            }
            //----------------------------------------------------   
            
            String json="{'success':false}"; 
            Produit produit = null;
            List<String> pdfPictoDanger = null;
            String idProduitS = request.getParameter("id");
            Integer idProduit;
            try{
                idProduit = Integer.parseInt(idProduitS);
            }catch(NumberFormatException e){
                idProduit = 0;
            }

            try {
                if (idProduit > 0){
                    ProduitDAO dao = new ProduitDAO();
                    if (dao!=null){
                        produit = dao.findProduit(idProduit);
                    }
                }               
                
                if (produit.getPictogrammes()!=null){
                    pdfPictoDanger = new ArrayList<String>();                     
                    for (Pictogramme pic : produit.getPictogrammes()){
                        pdfPictoDanger.add(pic.getIcone());
                    }                       
                }                
                                
            } catch (Exception ex) {
                Logger.getLogger(ProduitController.class.getName()).log(Level.SEVERE, null, ex);
            }            
       
        
            PdfDocumentFactory pdf = new PdfDocumentFactory("produit_" + idProduit);

            pdf.addVerticalTable(StringEscapeUtils.unescapeHtml(produit.getNom()), 24, produit.getDataForVerticalTable(), 0);
            pdf.addHorizontalTable("Conditionnements", 15, ProduitConditionnement.getDataForHorizontalTable(produit.getConditionnements()), 1, null);
            pdf.addHorizontalTable("Mentions de danger", 15, MentionDanger.getDataForHorizontalTable(produit.getMentions()), 1, new int[]{1,4});
            pdf.addHorizontalTableImg("Dangers", 15, Pictogramme.getDataForHorizontalTableImg(produit.getPictogrammes()), 1, null);                
            pdf.addHorizontalTable("Tags", 15, Tag.getDataForHorizontalTable(produit.getTags()), 1, null);

            pdf.getDocument().close();     

            if (!"".equals(pdf.getFileName())){
                return "{'success':true,'fileName':'"+pdf.getFileName()+"'}";
            }else{
                return json;   
            }
                
            
                    
        }
        
        
        public String getXLSList(){

            String fileName = "";
            List<Produit> produitList = null;
            
           // on cherche si on demander l'édition d'un seul utilisateur id<>0
            Produit produit = this.getItemFromId();
            //si utilisateur = null => c'est que id=0 car on a demander la liste de tous les utilisateurs
            if (produit==null){                
                produitList = this.getListProduit();
            }else{
                produitList = new ArrayList<Produit>();
                produitList.add(produit);
            }            
            
            try {

                DateTimeFormatter FILE_FORMAT = DateTimeFormat.forPattern("yyyyMMddHmsS");
                DateTimeFormatter USER_FORMAT = DateTimeFormat.forPattern("H:m dd/MM/yyyy");


                LocalDateTime now = new LocalDateTime();
                String dateStr = now.toString(FILE_FORMAT);


                fileName = "Produit_"+dateStr+".xls";
                
                WritableWorkbook wrtWorkbook = Workbook.createWorkbook(new File(fileName));

                WritableSheet sheet1;

                sheet1 = wrtWorkbook.createSheet("Produits", 0);

                Label titre = new Label(0, 0, "Produits, le " + now.toString(USER_FORMAT));
                sheet1.addCell(titre);

                sheet1.addCell(new Label(0, 1, "Substance"));
                sheet1.addCell(new Label(1, 1, "CAS"));
                sheet1.addCell(new Label(2, 1, "Nom"));
                sheet1.addCell(new Label(3, 1, "Fournisseur"));
                sheet1.addCell(new Label(4, 1, "Description"));
                sheet1.addCell(new Label(5, 1, "Pureté"));
                sheet1.addCell(new Label(6, 1, "Densite"));
                sheet1.addCell(new Label(7, 1, "Point de fusion"));
                sheet1.addCell(new Label(8, 1, "Point d'ébullition"));
                sheet1.addCell(new Label(9, 1, "Masse Molaire"));
                sheet1.addCell(new Label(10, 1, "Etat Physique"));
                sheet1.addCell(new Label(11, 1, "Refrigéré"));
                sheet1.addCell(new Label(12, 1, "Conditionnements"));
                sheet1.addCell(new Label(13, 1, "Danger"));
                sheet1.addCell(new Label(14, 1, "Mentions de danger"));
                sheet1.addCell(new Label(15, 1, "Fiche de sécurité"));
                sheet1.addCell(new Label(16, 1, "Tags"));

                int i = 2;
                for (Produit pr : produitList){
                    
                    sheet1.addCell(new Label(0, i, StringEscapeUtils.unescapeHtml(pr.getSubstance().getNom())));
                    sheet1.addCell(new Label(1, i, pr.getSubstance().getCas()));
                    sheet1.addCell(new Label(2, i, StringEscapeUtils.unescapeHtml(pr.getNom())));
                    sheet1.addCell(new Label(3, i, pr.getFournisseur().getNom()));
                    sheet1.addCell(new Label(4, i, StringEscapeUtils.unescapeHtml(pr.getDescription())));
                    sheet1.addCell(new Label(5, i, Double.toString(pr.getPurete())));
                    sheet1.addCell(new Label(6, i, Double.toString(pr.getDensite())));
                    sheet1.addCell(new Label(7, i, Double.toString(pr.getPointDeFusion())));
                    sheet1.addCell(new Label(8, i, Double.toString(pr.getPointdEbullition())));
                    sheet1.addCell(new Label(9, i, Double.toString(pr.getMasseMolaire())));
                    if (pr.getEtatPhysique()!=null)
                        sheet1.addCell(new Label(9, i, pr.getEtatPhysique().getLibelle()));
                   
                    sheet1.addCell(new Label(10, i, Utilities.booleanToOuiNon(pr.isRefrigere())));
             
                    String c = "";
                    for (ProduitConditionnement pc : pr.getConditionnements()){
                        c += "; " + pc.getReferenceFournisseur()+","+Double.toString(pc.getContenance())+","+pc.getUniteMesure().getLibelleLong()+","+pc.getContenant().getLibelle();
                    }
                    if (c.length()>0){
                        c = c.substring(2);
                    }
                    sheet1.addCell(new Label(11, i, c));
                    
                    
                    String p = "";
                    for (Pictogramme pic : pr.getPictogrammes()){
                        p += "; " + pic.getNom();
                    }
                    if (p.length()>0){
                        p = p.substring(2);
                    }
                    sheet1.addCell(new Label(12, i, p));
                    
                    String m = "";
                    for (MentionDanger md : pr.getMentions()){
                        m += "; " + md.getLibelle();
                    }
                    if (m.length()>0){
                        m = m.substring(2);
                    }
                    sheet1.addCell(new Label(13, i, m));                 
                    
                    sheet1.addCell(new Label(14, i, pr.getFicheSecurite()));
                    
                    String tags = "";
                    for (Tag tg : pr.getTags()){
                        tags += "; " + tg.getLibelle();
                    }
                    if (tags.length()>0){
                        tags = tags.substring(2);
                    }
                    sheet1.addCell(new Label(15, i, tags)); 
                    
                    i++;

                }
                
                wrtWorkbook.write();
                wrtWorkbook.close();


            } catch (IOException ex) {
                Logger.getLogger(ProduitController.class.getName()).log(Level.SEVERE, null, ex);
            } catch (RowsExceededException ex) {
                Logger.getLogger(ProduitController.class.getName()).log(Level.SEVERE, null, ex);
            } catch (WriteException ex) {
                Logger.getLogger(ProduitController.class.getName()).log(Level.SEVERE, null, ex);
            }

            return "{'success':true,'fileName':'"+fileName+"'}";

        }        
        
        
}
