/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2012/01/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/

package fr.univtours.gpod.Controller;


import fr.univtours.gpod.HTML.HTML;
import fr.univtours.gpod.exceptions.DuplicateIdentifiantEtiquetteException;
import fr.univtours.gpod.exceptions.RollbackFailureException;
import fr.univtours.gpod.json.JSONArray;
import fr.univtours.gpod.json.JSONException;
import fr.univtours.gpod.json.JSONObject;
import fr.univtours.gpod.json.JsonConversionUtilities;
import fr.univtours.gpod.persistance.DAO.ArticleDAO;
import fr.univtours.gpod.persistance.DAO.LivraisonDAO;
import fr.univtours.gpod.persistance.DAO.ProduitConditionnementDAO;
import fr.univtours.gpod.persistance.DAO.SousStructureDAO;
import fr.univtours.gpod.persistance.DAO.exceptions.NonexistentEntityException;
import fr.univtours.gpod.persistance.entities.*;
import fr.univtours.gpod.utils.AuthorizedProfils;
import fr.univtours.gpod.utils.Utilities;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;
import org.apache.commons.lang.StringEscapeUtils;
import org.joda.time.DateTime;
import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class LivraisonSimpleController extends LivraisonController {

        static DateTimeFormatter DATE_FORMAT = DateTimeFormat.forPattern("dd/MM/yyyy");

        public LivraisonSimpleController(HttpServletRequest request, HttpServletResponse response, HttpSession session){
            super(request, response, session);
            authorizedProfils = new AuthorizedProfils(request, response, session);
            authorizedProfils.addReadAuthorisedProfile("AD","LA", "RA");
            authorizedProfils.addWriteAuthorisedProfile("AD", "LA");               
        }

        public String getHtml(){
            //----------------------------------------------------
            //Autorisations         
            String resp = authorizedProfils.getAuthorizedHtmlTicket();
            if (!"".equals(resp)){
                return resp;
            }
            //----------------------------------------------------  


                response.setContentType("text/html;charset=iso-8859-1");

                HTML html = HTML.getInstance();
                html.getHeader().setTitle("Gestion des produits dangereux &gt; Gestion des livraisons");
                html.setExtjsHeader();
                html.getHeader().getCss().addLine("lib/multiselect/Multiselect.css");
                html.getHeader().getCss().addLine("lib/search/gridsearch.css");
                html.getHeader().getCss().addLine("lib/msg/msg.css");

                html.getHeader().getJs().addBlock("var B_nameUser = '"+Utilities.addSlashes(utilisateur.getNom())+"'");
                html.getHeader().getJs().addBlock("var B_lastCx = '"+Utilities.javaDateTIMEToString(utilisateur.getDateAvantDerniereConnexion())+"'");                 
                html.getHeader().getJs().addLine("version.js");
                html.getHeader().getJs().addLine("lib/printAction.js");
                html.getHeader().getJs().addLine("lib/periodButtons.js");
                 html.getHeader().getJs().addLine("lib/msg/msg.js");
                html.getHeader().getJs().addLine("lib/multiselect/Multiselect.js");
                html.getHeader().getJs().addLine("lib/multiselect/ItemSelector.js");
                html.getHeader().getJs().addLine("lib/search/gridSearch.js");
                html.getHeader().getJs().addLine("ui/search/AireStockage.js");
                html.getHeader().getJs().addLine("ui/search/SousStructure.js");
                html.getHeader().getJs().addLine("ui/windowApplet.js");
                html.getHeader().getJs().addLine("ui/addProduit.js");
                html.getHeader().getJs().addLine("lib/excelButton.js");
                html.getHeader().getJs().addLine("ui/container.LivraisonSimple.js");
                html.getHeader().getJs().addLine("ui/menus/menu."+ session.getAttribute("profil") +".js");
                html.getHeader().getJs().addLine("ui/squelette.js");
                html.getBody().setBodyElement("<table align='center' class='contenttable' cellpadding='0' cellspacing='0' border='0'><tr><td id='centerPanel'></td></tr></table>");
                html.getBody().setBodyElement("<div id='iframediv'>");

                return html.getHTMLCode();
        }


        public String saveItem(){
            //----------------------------------------------------
            //Autorisations         
            String resp = authorizedProfils.getWriteAuthorizedJsonTicket();
            if (!"".equals(resp)){
                return resp;
            }
            //----------------------------------------------------                  
            String json="";
            LivraisonDAO dao = null;

            this.response.setContentType("application/json");


            // --------------------------------------------------------------
            // <editor-fold defaultstate="expanded" desc="recup champs formulaire">

            String idLivraisonS = request.getParameter("idLivraison");
            String idSousStructureS = request.getParameter("idSousStructure");
            String idProduitConditionnementS = request.getParameter("idProduitConditionnement");
            String dateLivraisonS = request.getParameter("dateLivraison");
            String quantiteS = request.getParameter("quantite");
            ProduitConditionnement produitConditionnement = null;
            SousStructure sousStructure = null;
            String articlesLivraison = request.getParameter("articlesLivraison");
            List<Article> articleList = null;

            Integer idLivraison;
            try{
                idLivraison = Integer.parseInt(idLivraisonS);
            }catch(NumberFormatException e){
                idLivraison = 0;
            }            
            
            Integer idSousStructure;
            try{
                idSousStructure = Integer.parseInt(idSousStructureS);
                if (idSousStructure > 0 ){
                    try {
                        SousStructureDAO daob = new SousStructureDAO();
                        sousStructure = daob.findSousStructure(idSousStructure);

                    } catch (Exception ex) {
                        Logger.getLogger(LivraisonSimpleController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }catch(NumberFormatException e){
                idSousStructure = 0;
            }


            Integer idProduitConditionnement=0;
            try{
                idProduitConditionnement = Integer.parseInt(idProduitConditionnementS);
                if (idProduitConditionnement > 0 ){
                    try {
                        ProduitConditionnementDAO daob = new ProduitConditionnementDAO();
                        produitConditionnement = daob.findProduitConditionnement(idProduitConditionnement);

                    } catch (Exception ex) {
                        Logger.getLogger(LivraisonSimpleController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }catch(NumberFormatException e){
                idProduitConditionnement = 0;
            }

            int quantite;
            try{
                quantite = Integer.parseInt(quantiteS);
            }catch(NumberFormatException ex){
                quantite=0;
            }

            DateTime dateLivraison = null;
            // la date ne doit pas être null et doit faire 10 de longueur = jj/mm/aaaa
            if (dateLivraisonS != null && dateLivraisonS.length() == 10){
                try{
                    int year = Integer.parseInt(dateLivraisonS.substring(6));
                    int month = Integer.parseInt(dateLivraisonS.substring(3, 5));
                    int day = Integer.parseInt(dateLivraisonS.substring(0, 2));
                    dateLivraison = new DateTime(year, month, day,0,0,0,0);
                }catch(NumberFormatException ex){
                   Logger.getLogger(LivraisonSimpleController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            try{
                articleList = JsonConversionUtilities.jsonArticleArrayToList(articlesLivraison, idSousStructure);
            }catch (DuplicateIdentifiantEtiquetteException ex){
                json = "{success: false, errors:{reason:\'"+ex.getMessage()+"'}}";
                return json;
            }

            // </editor-fold>
            // --------------------------------------------------------------

            // --------------------------------------------------------------
            // <editor-fold defaultstate="expanded" desc="Test des champs">
            if (produitConditionnement == null || sousStructure == null|| quantite<1 || articleList==null || articleList.size()==0){
                json = "{success: false, errors:{reason:\'Des champs obligatoires n\\\'ont pas été saisis\'}}";
                return json;
            }
            // </editor-fold>
            // --------------------------------------------------------------


            try{
                 dao = new LivraisonDAO();
            } catch (Exception ex) {
                Logger.getLogger(LivraisonSimpleController.class.getName()).log(Level.SEVERE, null, ex);
                json = "{success: false, errors:{reason:\'"+ex.getMessage()+"\'}}";
                return json;
            }


            Livraison livraison = null;
            // si idlivraison = 0, c'est une nouvelle livraison
            if (idLivraison==0){
                LivraisonLigne livraisonLigne = new LivraisonLigne(quantite, true, produitConditionnement);
                ArrayList<LivraisonLigne> livraisonLigneList = new ArrayList<LivraisonLigne>();
                livraisonLigneList.add(livraisonLigne);
                livraison = new Livraison(dateLivraison.toDate(), "", null, this.utilisateur, livraisonLigneList, articleList);
                if (livraison !=null){
                    try{
                        dao.create(livraison);
                        json = "{success: true}";
                    } catch (Exception ex) {
                        Logger.getLogger(LivraisonSimpleController.class.getName()).log(Level.SEVERE, null, ex);
                        json = "{success: false, errors:{reason:\'"+ex.getMessage()+"\'}}";
                    }
                }
            // sinon, on met à jour les articles
            }else{
                
                if (articleList!=null && articleList.size()>0){
                    ArticleDAO daoa = new ArticleDAO();
                    for (Article a : articleList){
                        try {
                            daoa.edit(a);
                            json = "{success: true}";
                        } catch (NonexistentEntityException ex) {
                            Logger.getLogger(LivraisonSimpleController.class.getName()).log(Level.SEVERE, null, ex);
                            json = "{success: false, errors:{reason:\'"+ex.getMessage()+"\'}}";
                        } catch (Exception ex) {
                            Logger.getLogger(LivraisonSimpleController.class.getName()).log(Level.SEVERE, null, ex);
                            json = "{success: false, errors:{reason:\'"+ex.getMessage()+"\'}}";
                        }
                    }
                }
                

//                if (articlesLivraison!=null && !"".equals(articlesLivraison)){
//                    try {
//                        JSONArray articleArray = new JSONArray(articlesLivraison);
//                        for (int i = 0; i < articleArray.length(); i++) {                        
//                            JSONObject obj = articleArray.getJSONObject(i);
//                            int idArticle = Integer.parseInt((String) obj.get("idArticle"));
//                            String identifiantEtiquette = (String) obj.get("identifiantEtiquette");
//
//                            ArticleDAO daoa = new ArticleDAO();
//                            Article a = daoa.findArticle(idArticle);
//
//                            // si identifiantEtiquette est rempli
//                            if (!"".equals(identifiantEtiquette)){                           
//                                // On cherche un article avec le même identifiant etiquette
//                                Article a2 = daoa.findArticleFromIdentifiantEtiquette(identifiantEtiquette);
//                                // si il y a un article avec cet etiquette et que ce n'est pas cet article qu'on modifie
//                                // => On retourne une erreur
//                                if (a2!=null && !a2.equals(a)){                        
//                                    return "{success: false, errors:{reason:\'Cette étiquette est déjà utilisée par un autre produit\'}}";
//                                }                            
//                            }
//
//                            if (a!=null){
//                                a.setIdentifiantEtiquette(identifiantEtiquette);
//                                daoa.edit(a);
//                            }                        
//
//
//                        }
//                        json = "{success: true}";
//
//                    } catch (NonexistentEntityException | JSONException ex) {
//                        Logger.getLogger(LivraisonSimpleController.class.getName()).log(Level.SEVERE, null, ex);
//                        json = "{success: false, errors:{reason:\'"+ex.getMessage()+"\'}}";
//                    } catch (Exception ex) {
//                        Logger.getLogger(LivraisonSimpleController.class.getName()).log(Level.SEVERE, null, ex);   
//                        json = "{success: false, errors:{reason:\'"+ex.getMessage()+"\'}}";
//                    }
//
//
//                }
                
                
            }




             return json;
            //return "{success: false, errors:{reason:\'"+msg+"\'}};
        }



        public List<Livraison> getListLivraison(){
            List<Livraison> livraisonList = null;
            // ------------------------------------------------------------------
            int maxResults = -1;
            int firstResult = -1;
            int periodInMonth = -1;
            try{
                maxResults = Integer.parseInt(request.getParameter("limit"));
                firstResult = Integer.parseInt(request.getParameter("start"));
                if (maxResults == 0){
                    maxResults=10;
                }
            }catch(NumberFormatException e){
            }
            try{
                periodInMonth = Integer.parseInt(request.getParameter("period"));
            }catch(Exception e){
            }
            // ------------------------------------------------------------------  

                LivraisonDAO dao = new LivraisonDAO();

                //si l'utilisateur est administrateur, on affiche tous les résultats... sinon seulemence ceux de l'utilisateur
                if ("AD".equals(utilisateur.getUtilisateurProfil().getCode())){
                    livraisonList = dao.findLivraisonSansCommandesEntities(maxResults, firstResult, periodInMonth);
                }else{
                    livraisonList = dao.findLivraisonSansCommandesEntitiesBySousStructure(utilisateur.getSousStructure(), maxResults, firstResult, periodInMonth);
                }
               return livraisonList;

        }
        
        public int getListLivraisonCount(){
            // ------------------------------------------------------------------
            int periodInMonth = -1;
            try{
                periodInMonth = Integer.parseInt(request.getParameter("period"));
            }catch(Exception e){
            }
            // ------------------------------------------------------------------  

            int nbItems = 0;


                LivraisonDAO dao = new LivraisonDAO();

                //si l'utilisateur est administrateur, on affiche tous les résultats... sinon seulemence ceux de l'utilisateur
                if ("AD".equals(utilisateur.getUtilisateurProfil().getCode())){
                    nbItems = dao.getLivraisonSansCommandesCount(periodInMonth);
                }else{
                    nbItems = dao.getLivraisonSansCommandesEntitiesBySousStructureCount(utilisateur.getSousStructure(), periodInMonth);
                }
                
                
                return nbItems;

        }        
        


        public String getListItems(){

            this.response.setContentType("application/json");
            String json = "";
            List<Livraison> livraisonList = this.getListLivraison();

            try {

                if (livraisonList !=null && livraisonList.size()>0){
                    json = "{'totalCount':'"+this.getListLivraisonCount()+"','data':[";
                    for (Livraison l : livraisonList){
                        DateTime dateLivraison = new DateTime(l.getDateLivraison());
                        String dateLivraisonS = dateLivraison.toString(DATE_FORMAT);

                        json += " {'idLivraison':'"+l.getIdLivraison()+"',";
                        json += " 'CAS':'"+l.getArticleList().get(0).getProduitConditionnement().getProduit().getSubstance().getCas()+"',";
                        json += " 'idProduitConditionnement':'"+l.getArticleList().get(0).getProduitConditionnement().getIdProduitConditionnement()+"',";
                        json += "'nomProduit':'"+Utilities.addSlashes(l.getArticleList().get(0).getProduitConditionnement().getProduit().getNom())+"',";
                        json += "'dateLivraison':'"+dateLivraisonS+"',";
                        json += "'idSousStructure':'"+l.getArticleList().get(0).getSousStructure().getIdSousStructure()+"',";
                        json += "'nomSousStructure':'"+l.getArticleList().get(0).getSousStructure().getNom()+"',";
                        json += "'quantite':'"+l.getArticleList().size()+"',";
                         json += "'supp':'"+l.isDeletable(utilisateur)+"',";
                         json += "'write':'"+AuthorizedProfils.isWriteAllowed(utilisateur.getUtilisateurProfil().getCode(), "AD", "LA")+"'},";
                    }
                    json += "]}";
                }else{
                    json = "{'totalCount':'0','data':[]}";
                }
            } catch (Exception ex) {
                Logger.getLogger(LivraisonSimpleController.class.getName()).log(Level.SEVERE, null, ex);
                json = "{'totalCount':'0','data':[]}";
            }
            return json;
        }

     
        
        // Methode utilisée pour les édition XLS et PDF
        // retourne l'utilisateur à partir de l'id. Si l'id n'est pas définit, on a demandé l'impression de la liste (voir méthode getXLSList()
        private Livraison getItemFromId(){
                Livraison livraison = null;
                int idLivraison= 0;
                String idLivraisonS = request.getParameter("id");
                
                if (idLivraisonS!=null || !"".equals(idLivraisonS)){
                    try{
                        idLivraison = Integer.parseInt(idLivraisonS);
                        if (idLivraison > 0 ){
                            try {
                                LivraisonDAO dao = new LivraisonDAO();
                                livraison = dao.findLivraison(idLivraison);
                            } catch (Exception ex) {
                                Logger.getLogger(LivraisonSimpleController.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }
                    }catch(NumberFormatException ex){
                        Logger.getLogger(LivraisonSimpleController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                return livraison;
        }          
        
        public String getXLSList(){

            String fileName = "";

            try {

                DateTimeFormatter FILE_FORMAT = DateTimeFormat.forPattern("yyyyMMddHmsS");
                DateTimeFormatter USER_FORMAT = DateTimeFormat.forPattern("H:m dd/MM/yyyy");

                List<Livraison> livraisonList = null;
                                            
                Livraison livraison = this.getItemFromId();
                if (livraison==null){
                    livraisonList = this.getListLivraison();
                }else{
                    livraisonList = new ArrayList<Livraison>();
                    livraisonList.add(livraison);
                }
                
                
              
                LocalDateTime now = new LocalDateTime();
                String dateStr = now.toString(FILE_FORMAT);
                fileName = "livraison_"+dateStr+".xls";
                WritableWorkbook wrtWorkbook = Workbook.createWorkbook(new File(fileName));
                WritableSheet sheet1;
                sheet1 = wrtWorkbook.createSheet("Livraisons", 0);

                Label titre = new Label(0, 0, "Livraisons, le " + now.toString(USER_FORMAT));
                sheet1.addCell(titre);

                sheet1.addCell(new Label(0, 2, "Livraison"));
                sheet1.addCell(new Label(1, 2, "Date de livraison"));
                sheet1.addCell(new Label(2, 2, "CAS"));
                sheet1.addCell(new Label(3, 2, "Reference Fournisseur"));
                sheet1.addCell(new Label(4, 2, "Produit"));
                sheet1.addCell(new Label(5, 2, "Local de Stockage"));
                sheet1.addCell(new Label(6, 2, "Equipe/departement"));
                sheet1.addCell(new Label(7, 2, "Code Barre"));

                int i = 3;
                for (Livraison l : livraisonList){
                    sheet1.addCell(new Label(0, i, l.getNumeroBonLivraison()));
                    sheet1.addCell(new Label(1, i, Utilities.javaDateToString(l.getDateLivraison())));
                    for (Article s : l.getArticleList()){
                        i++;
                        sheet1.addCell(new Label(2, i, s.getProduitConditionnement().getProduit().getSubstance().getCas()));
                        sheet1.addCell(new Label(3, i, s.getProduitConditionnement().getReferenceFournisseur()));
                        sheet1.addCell(new Label(4, i, StringEscapeUtils.unescapeHtml(s.getProduitConditionnement().getProduit().getNom())));
                        sheet1.addCell(new Label(5, i, s.getAireStockage().getNom()));
                        sheet1.addCell(new Label(6, i, s.getSousStructure().getNom()));
                        sheet1.addCell(new Label(7, i, s.getIdentifiantEtiquette()));
                    }
                    i++;

                }

                wrtWorkbook.write();
                wrtWorkbook.close();

                return "{'success':true,'fileName':'"+fileName+"'}";


            } catch (IOException ex) {
                Logger.getLogger(LivraisonSimpleController.class.getName()).log(Level.SEVERE, null, ex);
                 return "{'success':false}";
            } catch (RowsExceededException ex) {
                Logger.getLogger(LivraisonSimpleController.class.getName()).log(Level.SEVERE, null, ex);
                return "{'success':false}";
            } catch (WriteException ex) {
                Logger.getLogger(LivraisonSimpleController.class.getName()).log(Level.SEVERE, null, ex);
                return "{'success':false}";
            }


        }
}
