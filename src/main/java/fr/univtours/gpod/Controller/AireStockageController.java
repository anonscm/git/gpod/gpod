/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2012/01/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/

package fr.univtours.gpod.Controller;

import fr.univtours.gpod.HTML.HTML;
import fr.univtours.gpod.exceptions.NonexistentEntityException;
import fr.univtours.gpod.persistance.DAO.AireStockageDAO;
import fr.univtours.gpod.persistance.DAO.SalleStockageDAO;
import fr.univtours.gpod.persistance.DAO.TypeAireDAO;
import fr.univtours.gpod.persistance.entities.AireStockage;
import fr.univtours.gpod.utils.Utilities;
import java.util.List;
import fr.univtours.gpod.persistance.entities.SalleStockage;
import fr.univtours.gpod.persistance.entities.TypeAire;
import fr.univtours.gpod.utils.AuthorizedProfils;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class AireStockageController extends ActionController {

        public AireStockageController(HttpServletRequest request, HttpServletResponse response, HttpSession session){
            super(request, response, session);
            authorizedProfils = new AuthorizedProfils(request, response, session);
            authorizedProfils.addReadAuthorisedProfile("AD");
            authorizedProfils.addWriteAuthorisedProfile("AD");       
        }
    
        public String getHtml(){
            //----------------------------------------------------
            //Autorisations         
            String resp = authorizedProfils.getAuthorizedHtmlTicket();
            if (!"".equals(resp)){
                return resp;
            }
            //----------------------------------------------------    
                response.setContentType("text/html;charset=iso-8859-1");
            
                HTML html = HTML.getInstance();
                html.getHeader().setTitle("Gestion des produits dangereux &gt; Gestion des aires de stockage");
                html.setExtjsHeader();
                html.getHeader().getCss().addLine("lib/multiselect/Multiselect.css");
                html.getHeader().getCss().addLine("lib/search/gridsearch.css");
                html.getHeader().getCss().addLine("lib/msg/msg.css");

                html.getHeader().getJs().addBlock("var B_nameUser = '"+Utilities.addSlashes(utilisateur.getNom())+"'");
                html.getHeader().getJs().addBlock("var B_lastCx = '"+Utilities.javaDateTIMEToString(utilisateur.getDateAvantDerniereConnexion())+"'");                
                html.getHeader().getJs().addLine("version.js");
                html.getHeader().getJs().addLine("lib/printAction.js");
                html.getHeader().getJs().addLine("lib/msg/msg.js");
                html.getHeader().getJs().addLine("http://maps.google.com/maps/api/js?sensor=false");
                html.getHeader().getJs().addLine("lib/gmaps/GMapPanel3.js");
                html.getHeader().getJs().addLine("lib/multiselect/Multiselect.js");
                html.getHeader().getJs().addLine("lib/multiselect/ItemSelector.js");   
                html.getHeader().getJs().addLine("lib/search/gridSearch.js");
                html.getHeader().getJs().addLine("ui/container.AireStockage.js");
                html.getHeader().getJs().addLine("ui/menus/menu."+ session.getAttribute("profil") +".js");
                html.getHeader().getJs().addLine("ui/squelette.js");

                html.getBody().setBodyElement("<table align='center' class='contenttable' cellpadding='0' cellspacing='0' border='0'><tr><td id='centerPanel'></td></tr></table>");
            


                return html.getHTMLCode();
        }  
        
 
       public String getListItems(){
            
            
            String json = "";
            List<AireStockage> aires = null;
            int idSalleStockage = 0;

            // ------------------------------------------------------------------
            int maxResults = -1;
            int firstResult = -1;
            try{
                maxResults = Integer.parseInt(request.getParameter("limit"));
                firstResult = Integer.parseInt(request.getParameter("start"));
                if (maxResults == 0){
                    maxResults=10;
                }
            }catch(NumberFormatException e){
            }
            String searchValue = request.getParameter("query");
            String searchColumns = request.getParameter("fields");
            // ------------------------------------------------------------------  


            // récupération de conditions sur la liste à récupérer
            try{
                idSalleStockage = Integer.parseInt(request.getParameter("idSalleStockage"));
            }catch(NumberFormatException e){
            }
            
            int nbItems = 0;
            
            try {
                AireStockageDAO dao = new AireStockageDAO();
                if (idSalleStockage != 0){
                    SalleStockageDAO siteDao = new SalleStockageDAO();
                    SalleStockage salleStockage = siteDao.findSalleStockage(idSalleStockage);
                    if (salleStockage != null){
                        aires = dao.findAireFromSalleStockage(salleStockage);
                        nbItems = aires.size();
                    }
                }else{
                    aires = dao.findAireStockageWithQuery(searchValue, searchColumns, maxResults, firstResult);
                    nbItems = dao.getAireStockageCount(searchValue, searchColumns);
                }
                
                
                this.response.setContentType("application/json");
                
                if (aires !=null && aires.size()>0){
                    json = "{'totalCount':'"+nbItems+"','data':[";
                    for (AireStockage b : aires){                                                
                        json += " {'idAire':'"+b.getIdAire()+"',";
                        json += "'idSalle':'"+b.getSalleStockage().getIdSalle()+"',";  
                        json += "'nomSalle':'"+Utilities.addSlashes(b.getSalleStockage().getNom())+"',";                        
                        json += "'idTypeAire':'"+b.getTypeAire().getIdTypeAire()+"',";  
                        json += "'nomTypeAire':'"+Utilities.addSlashes(b.getTypeAire().getNom())+"',"; 
                        json += "'nom':'"+Utilities.addSlashes(b.getNom())+"',";
                        json += "'nomLong':'"+Utilities.addSlashes(b.getSalleStockage().getBatiment().getNom())+ " > " + 
                                    Utilities.addSlashes(b.getSalleStockage().getNom())+ " > " + 
                                    Utilities.addSlashes(b.getNom())+"',";                         
                        json += "'code':'"+Utilities.addSlashes(b.getCode())+"',";
                        json += "'description':'"+Utilities.addSlashes(b.getDescription())+"',";
                        json += "'refrigere':'"+b.isRefrigere()+"'},";
                    }                    
                    json += "]}";
                }else{
                    json = "{'totalCount':'0','data':[]}";
                }
            } catch (Exception ex) {
                Logger.getLogger(AireStockageController.class.getName()).log(Level.SEVERE, null, ex);
            }
            return json;       
            
        }        
        
       
        public String saveItem(){
            //----------------------------------------------------
            //Autorisations         
            String resp = authorizedProfils.getWriteAuthorizedJsonTicket();
            if (!"".equals(resp)){
                return resp;
            }
            //----------------------------------------------------               
            String json="";
            int idAire=0;
            TypeAire typeAire = null;
            SalleStockage salleStockage = null;
            AireStockageDAO dao = null;
            
            this.response.setContentType("application/json");
                                                 
            try{
                idAire = Integer.parseInt(request.getParameter("idAire"));
            }catch(NumberFormatException e){
                idAire = 0;
            }

            boolean refrigere;
            try{
                refrigere = Boolean.parseBoolean(request.getParameter("refrigere"));
            }catch(NumberFormatException e){
                refrigere = false;
            }
            
            int idSalleStockage;
            try{
                idSalleStockage = Integer.parseInt(request.getParameter("idSalleStockage"));
                if (idSalleStockage > 0 ){
                    try {
                        SalleStockageDAO daob = new SalleStockageDAO();
                        salleStockage = daob.findSalleStockage(idSalleStockage);
                        
                    } catch (Exception ex) {
                        Logger.getLogger(AireStockageController.class.getName()).log(Level.SEVERE, null, ex);
                    }                    
                }                
            }catch(NumberFormatException e){
                idSalleStockage = 0;
            }
            
            int idTypeAire;
            try{
                idTypeAire = Integer.parseInt(request.getParameter("idTypeAire"));
                if (idTypeAire > 0 ){
                    try {
                        TypeAireDAO daotp = new TypeAireDAO();
                        typeAire = daotp.findTypeAire(idTypeAire);
                        
                    } catch (Exception ex) {
                        Logger.getLogger(AireStockageController.class.getName()).log(Level.SEVERE, null, ex);
                    }                    
                }
            }catch(NumberFormatException e){
                idTypeAire = 0;
            }
            
            
            String nom = request.getParameter("nom");
            String code = request.getParameter("code");
            String description = request.getParameter("description");
           
                              
            
            // Test des champs
            if ("".equals(nom) || typeAire ==null || salleStockage == null){
                json = "{success: false, errors:{reason:\'Des champs obligatoires n\\\'ont pas été saisis\'}}";
                return json;          
            }
                      
            try{
                dao = new AireStockageDAO();              
            } catch (Exception ex) {
                Logger.getLogger(AireStockageController.class.getName()).log(Level.SEVERE, null, ex);
                json = "{success: false, errors:{reason:\'"+ex.getMessage()+"\'}}";
                return json;
            }
                      

            
             // Ajout nouveau id = 0
            if (idAire == 0){

                    AireStockage aire = new AireStockage(typeAire, salleStockage, nom, code, description, refrigere);
                    if (aire !=null){
                        try{
                            dao.create(aire);
                            json = "{success: true}";
                        } catch (Exception ex) {
                            Logger.getLogger(AireStockageController.class.getName()).log(Level.SEVERE, null, ex);
                            json = "{success: false, errors:{reason:\'"+ex.getMessage()+"\'}}";
                        }
                    }                                  

          
            // UPDATE id > 0
            }else if (idAire > 0L){
                

                    AireStockage aire = dao.findAireStockage(idAire);
                 
                    if (aire !=null){
                        
                        aire.setTypeAire(typeAire);
                        aire.setSalleStockage(salleStockage);
                        aire.setNom(nom);
                        aire.setCode(code);
                        aire.setDescription(description);
                        aire.setRefrigere(refrigere);
                        
                        try{
                            dao.edit(aire);
                            json = "{success: true}"; 
                        } catch (NonexistentEntityException ex) {
                            Logger.getLogger(AireStockageController.class.getName()).log(Level.SEVERE, null, ex);
                            json = "{success: false, errors:{reason:\'"+ex.getMessage()+"\'}}";  
                        } catch (Exception ex) {
                            Logger.getLogger(AireStockageController.class.getName()).log(Level.SEVERE, null, ex);
                            json = "{success: false, errors:{reason:\'"+ex.getMessage()+"\'}}";  
                        }                            
                    }                       
                    
        
            }else{
                json = "{success: false, errors:{reason:\'Impossible de trouver "+idAire+"\'}}";
            }
                     
            
            return json;            
            //return "{success: false, errors:{reason:\'"+msg+"\'}};
        }        
        
        
        public String getPDF() {
            //----------------------------------------------------
            //Autorisations en lecture
            String resp = authorizedProfils.getReadAuthorizedJsonTicket();
            if (!"".equals(resp)){
                return resp;
            }
            //----------------------------------------------------   
            
            String json="{'success':false}"; 
            AireStockage aire = null;

            String idS = request.getParameter("id");
            Integer id;
            try{
                id = Integer.parseInt(idS);
            }catch(NumberFormatException e){
                id = 0;
            }


            try {
                if (id > 0){
                    AireStockageDAO dao = new AireStockageDAO();
                    if (dao!=null){
                        aire = dao.findAireStockage(id);
                    }
                }


            } catch (Exception ex) {
                Logger.getLogger(AireStockageController.class.getName()).log(Level.SEVERE, null, ex);
            }    
            
            
            PdfDocumentFactory pdf = new PdfDocumentFactory("aireStockage" + id);           
            
            if (aire!=null){
                
                
                pdf.addVerticalTable("Aire de Stockage", 24, aire.getDataForVerticalTable(), 0);
                
               
                
                
            }
            
            pdf.getDocument().close();     

            if (!"".equals(pdf.getFileName())){
                return "{'success':true,'fileName':'"+pdf.getFileName()+"'}";
            }else{
                return json;   
            }
            
        }        
        
}
