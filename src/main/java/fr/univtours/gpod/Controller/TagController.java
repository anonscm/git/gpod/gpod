/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2012/01/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/

package fr.univtours.gpod.Controller;

import fr.univtours.gpod.persistance.DAO.MentionDangerDAO;
import fr.univtours.gpod.persistance.DAO.ProduitDAO;
import fr.univtours.gpod.persistance.DAO.TagDAO;
import fr.univtours.gpod.persistance.entities.MentionDanger;
import fr.univtours.gpod.persistance.entities.Produit;
import fr.univtours.gpod.persistance.entities.Tag;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.List;
import fr.univtours.gpod.utils.Utilities;
import javax.persistence.NoResultException;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author geoffroy.vibrac
 */
public class TagController   extends ActionController {

        public TagController(HttpServletRequest request, HttpServletResponse response, HttpSession session){
            super(request, response, session);
        }

    
        public String getListItems(){
            
            
            String json = "";

            List<Tag> tags = null;
            int idProduit = 0;
            List<Tag> tagsProduit = null;

       
            // Récupération de idUtilisateur dans le cas d'appel depuis Utilisateur
            try{
                idProduit = Integer.parseInt(request.getParameter("idProduit"));
            }catch(NumberFormatException e){
                idProduit = 0;
            }   
       
            
            try {
                // si un produit est selectionné, il faut supprimer les tags de ce produit
                if (idProduit>0){
                    ProduitDAO dao = new ProduitDAO();               
                    Produit produit = dao.findProduit(idProduit);
                    tagsProduit = produit.getTags();
                }  
                
                TagDAO dao = new TagDAO();   
                tags = dao.findTagEntities();
                
          
                
                this.response.setContentType("application/json");
                
                if (tags !=null && tags.size()>0){
                    // On enleve les tags du produits
                    if (tagsProduit!=null && tagsProduit.size()>0){
                        tags.removeAll(tagsProduit);
                    }                       
                    
                    json = "{'totalCount':'"+tags.size()+"','data':[";
                    for (Tag u : tags){                        
                        
                        
                        json += " {'idtag':'"+u.getIdtag()+"',";
                        json += "'libelle':'"+Utilities.addSlashes(u.getLibelle())+"'},";
                  
                        
                    }                    
                    json += "]}";
                    
                }else{
                    json = "{'totalCount':'0','data':[]}";
                }
            } catch (Exception ex) {
                Logger.getLogger(TagController.class.getName()).log(Level.SEVERE, null, ex);
                json = "{'totalCount':'0','data':[]}";
            }
            return json;                   
        }

        
        public String addItem(){
            
            String json = "";
            String tagLibelle = request.getParameter("tagLibelle");
            Tag tag = null;
            if ("".equals(tagLibelle)){   
                this.response.setStatus(424);
                json = "{success: false, errors:{reason:\'Des champs obligatoires n\\\'ont pas été saisis\'}}";
                return json;                 
            }
            
            try{
            
                TagDAO dao = new TagDAO();  
                try{
                    tag = dao.findTagByLibelle(tagLibelle);
                }catch(NoResultException ex){
                    tag = null;
                }
                    

                if (tag == null){
                    tag = new Tag(tagLibelle);
                    if (tag !=null){
                        dao.create(tag);
                        json = "{success: true}";
                    }else{
                        this.response.setStatus(424);
                        json = "{success: false, errors:{reason:\'Ajout impossible\'}}";
                    }
                }else{
                    this.response.setStatus(424);
                    json = "{success: false, errors:{reason:\'Ce tag existe\'}}";
                }
            
            } catch (Exception ex) {
                this.response.setStatus(424);
                Logger.getLogger(TagController.class.getName()).log(Level.SEVERE, null, ex);
                json = "{success: false, errors:{reason:\'"+ex.getMessage()+"\'}}";
            }     
            
            return json; 
            
            
        }
        
        
    @Override
    public String deleteItem(){
             
            int idtag = 0;
            Tag tag = null;
            String json = "";
            
            try{
                idtag = Integer.parseInt(request.getParameter("idtag"));
            }catch(NumberFormatException e){
            }                
             
            
            if (idtag > 0){
                
                 try{
            
                        TagDAO dao = new TagDAO();  
                        tag = dao.findTag(idtag);

                        if (tag != null){
                                dao.destroy(idtag);
                                json = "{success: true}";
                        }else{
                            this.response.setStatus(424);
                            json = "{success: false, errors:{reason:\'Ce tag n\\\'existe pas\'}}";
                        }
                          
                 
                }catch (Exception ex) {
                    this.response.setStatus(424);
                    Logger.getLogger(TagController.class.getName()).log(Level.SEVERE, null, ex);
                    json = "{success: false, errors:{reason:\'Impossible de supprimer ce tag\'}}";
                }                  
                
            }else{
                 this.response.setStatus(424);
                json = "{success: false, errors:{reason:\'Ce tag n\\\'existe pas\'}}";
            }
            
           return json;                 
                             
           
            
            
         }
        
}
