/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2012/01/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/

package fr.univtours.gpod.Controller;

import fr.univtours.gpod.Controller.stocks.Stock;
import fr.univtours.gpod.persistance.entities.Produit;
import fr.univtours.gpod.persistance.DAO.ProduitDAO;
import fr.univtours.gpod.persistance.DAO.AireStockageDAO;
import fr.univtours.gpod.persistance.DAO.ArticleDAO;
import fr.univtours.gpod.persistance.DAO.BatimentDAO;
import fr.univtours.gpod.persistance.DAO.PictogrammeDAO;
import fr.univtours.gpod.persistance.DAO.ProduitConditionnementDAO;
import fr.univtours.gpod.persistance.DAO.SalleStockageDAO;
import fr.univtours.gpod.persistance.DAO.SiteDAO;
import fr.univtours.gpod.persistance.DAO.SousStructureDAO;
import fr.univtours.gpod.persistance.entities.AireStockage;
import fr.univtours.gpod.persistance.entities.Batiment;
import fr.univtours.gpod.persistance.entities.Pictogramme;
import fr.univtours.gpod.persistance.entities.ProduitConditionnement;
import fr.univtours.gpod.persistance.entities.SalleStockage;
import fr.univtours.gpod.persistance.entities.Site;
import fr.univtours.gpod.persistance.entities.SousStructure;
import fr.univtours.gpod.utils.AuthorizedProfils;
import fr.univtours.gpod.utils.Utilities;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


public class StockController extends ActionController {

        public StockController(HttpServletRequest request, HttpServletResponse response, HttpSession session){
            super(request, response, session);
            
        }



        
        protected  List<Stock> getListStock(String order){


            List<Stock> stocks = null;
            List<Object[]> listArticles = null;
            int maxResults = -1;
            int firstResult = -1;
            int idSite=0;
            Site site=null;
            int idBatiment = 0;
            Batiment batiment=null;
            int idSalleStockage = 0;
            SalleStockage salleStockage=null;            
            int idAire = 0;
            AireStockage aire = null;
            int idProduitConditionnement = 0;
            ProduitConditionnement produitConditionnement = null;
            int idSousStructure = 0;
            SousStructure sousStructure = null;
            Float valeurStock = null;
            List<Pictogramme> pictogrammes = new ArrayList<Pictogramme>(); 

            try{
                maxResults = Integer.parseInt(request.getParameter("limit"));
                firstResult = Integer.parseInt(request.getParameter("start"));
                if (maxResults == 0){
                    maxResults=10;
                }
            }catch(NumberFormatException e){
            }

            Integer idArticle = 0;
            try{
                idArticle = Integer.parseInt(request.getParameter("idArticle"));
            }catch(NumberFormatException e){
                idArticle = 0;
            }

            // -------------
            // PARAMETRES
            String idSiteS = request.getParameter("idSite");
            try{
                idSite = Integer.parseInt(idSiteS);
                if (idSite > 0 ){
                    try {
                        SiteDAO daob = new SiteDAO();
                        site = daob.findSite(idSite);
                    } catch (Exception ex) {
                        Logger.getLogger(StockController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }catch(NumberFormatException e){
                idSite = 0;
            }

            String idSousStructureS = request.getParameter("idSousStructure");
            try{
                idSousStructure = Integer.parseInt(idSousStructureS);
                if (idSousStructure > 0 ){
                    try {
                        SousStructureDAO daob = new SousStructureDAO();
                        sousStructure = daob.findSousStructure(idSousStructure);
                    } catch (Exception ex) {
                        Logger.getLogger(StockController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }catch(NumberFormatException e){
                idSousStructure = 0;
            }


            String idBatimentS = request.getParameter("idBatiment");
            try{
                idBatiment = Integer.parseInt(idBatimentS);
                if (idBatiment > 0 ){
                    try {
                        BatimentDAO daob = new BatimentDAO();
                        batiment = daob.findBatiment(idBatiment);

                    } catch (Exception ex) {
                        Logger.getLogger(StockController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }catch(NumberFormatException e){
                idBatiment = 0;
            }
            
            String idSalleStockageS = request.getParameter("idSalleStockage");
            try{
                idSalleStockage = Integer.parseInt(idSalleStockageS);
                if (idSalleStockage > 0 ){
                    try {
                        SalleStockageDAO daob = new SalleStockageDAO();
                        salleStockage = daob.findSalleStockage(idSalleStockage);

                    } catch (Exception ex) {
                        Logger.getLogger(StockController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }catch(NumberFormatException e){
                idSalleStockage = 0;
            }            
            

            String idAireS = request.getParameter("idAire");
            try{
                idAire = Integer.parseInt(idAireS);
                if (idAire > 0 ){
                    try {
                        AireStockageDAO daob = new AireStockageDAO();
                        aire = daob.findAireStockage(idAire);

                    } catch (Exception ex) {
                        Logger.getLogger(StockController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }catch(NumberFormatException e){
                idAire = 0;
            }

            String produitConditionnementS = request.getParameter("idProduitConditionnement");
            try{
                idProduitConditionnement = Integer.parseInt(produitConditionnementS);
                if (idProduitConditionnement > 0 ){
                    try {
                        ProduitConditionnementDAO daob = new ProduitConditionnementDAO();
                        produitConditionnement = daob.findProduitConditionnement(idProduitConditionnement);

                    } catch (Exception ex) {
                        Logger.getLogger(StockController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }catch(NumberFormatException e){
                idProduitConditionnement = 0;
            }

            String nomSigne = "";
            nomSigne = request.getParameter("nomSigne");

            String valeurStocks = request.getParameter("valeurStock");
            try{
                valeurStock = Float.parseFloat(valeurStocks.replace(",","."));
            }catch(NumberFormatException | NullPointerException e){
            }

            String etiquette = request.getParameter("etiquette");



            String picto = request.getParameter("pictogrammes");
            String[] idPictogrammes = null;
            if (picto!=null){
                idPictogrammes = picto.split( ",\\s*" );

                if (idPictogrammes!= null && idPictogrammes.length>0){
                    PictogrammeDAO daoPicto = new PictogrammeDAO();
                    for (String ids : idPictogrammes){
                        try{
                            int id = Integer.parseInt(ids);
                            Pictogramme p = daoPicto.findPictogramme(id);
                            if (p!=null){
                                pictogrammes.add(p);
                            }
                        }catch(NumberFormatException e){
                        }
                    }
                }
            }

            String cas = request.getParameter("cas");
            String autre = request.getParameter("autre");
            
            boolean conso;
            try{
                conso = Boolean.parseBoolean(request.getParameter("conso"));
            }catch(NumberFormatException e){
                conso = false;
            }

            try {
                ArticleDAO dao = new ArticleDAO();
                listArticles = dao.findStockArticleWithQuery(maxResults, firstResult, site, batiment, salleStockage, aire, produitConditionnement, nomSigne, valeurStock,
                        sousStructure, pictogrammes, conso, etiquette, idArticle, order, utilisateur.getSousStructure(), cas, autre);


                stocks = Stock.getListStock(listArticles);

                this.response.setContentType("application/json");

                
            } catch (Exception ex) {
                Logger.getLogger(StockController.class.getName()).log(Level.SEVERE, null, ex);
                
            }
            return stocks;
        }


        public String getListItems(){
            this.response.setContentType("application/json");

            String json = "{'totalCount':'0','data':[]}";
             List<Stock> stocks = this.getListStock(null);


            if (stocks !=null && stocks.size()>0){
                    json = "{'totalCount':'"+stocks.size()+"','data':[";
                    for (Stock u : stocks){
                                            
                        
                        json += " {'CAS':'"+u.getProduitConditionnement().getProduit().getSubstance().getCas()+"',";
                        json += " 'idArticle':'"+u.getIdArticle()+"',";
                        json += " 'idProduitConditionnement':'"+u.getProduitConditionnement().getIdProduitConditionnement()+"',";
                        json += "'identifiantEtiquette':'"+u.getIdentifiantEtiquette()+"',";
                        json += "'nomProduit':'"+Utilities.addSlashes(u.getProduitConditionnement().getProduit().getNom())+"',";
                        json += "'idSousStructure':'"+u.getSousStructure().getIdSousStructure()+"',";
                        json += "'nomSousStructure':'"+Utilities.addSlashes(u.getSousStructure().getNom())+"',";
                        json += "'idAire':'"+u.getAire().getIdAire()+"',";
                        json += "'nomAire':'"+Utilities.addSlashes(u.getAire().getSalleStockage().getBatiment().getNom())+ " &gt; " + 
                                    Utilities.addSlashes(u.getAire().getSalleStockage().getNom())+ " &gt; " + 
                                    Utilities.addSlashes(u.getAire().getNom())+"',";
                        json += "'quantite':'"+u.getQuantite()+"',";
                        json += "'contenance':'"+u.getProduitConditionnement().getContenance()+"',";
                        json += "'uniteMesure':'"+u.getProduitConditionnement().getUniteMesure().getAbreviation()+"',";
                        json += "'uniteMesureLong':'"+u.getProduitConditionnement().getUniteMesure().getLibelleLong()+"',";
                        json += "'dateOuverture':'"+Utilities.javaDateToString(u.getDateOuverture())+"',";
                        json += "'write':'"+AuthorizedProfils.isWriteAllowed(utilisateur.getUtilisateurProfil().getCode(), "AD", "LA")+"'},";
                    }
                    json += "]}";
            }else{
                json = "{'totalCount':'0','data':[]}";
            }

           return json;

        }
        
        
        public String getQteItems(){
            this.response.setContentType("application/json");
            int quantite = 0;
            String json = "{'quantite':'0'}";
             List<Stock> stocks = this.getListStock(null);

             if (stocks !=null && stocks.size()>0){
                    json = "{'quantite':'";
                    for (Stock u : stocks){
                        quantite += u.getQuantite();
                    }                    
                    json += quantite +"'}";
            }

           return json;

        }        




        
        
}
