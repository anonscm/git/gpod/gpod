/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2012/01/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/

package fr.univtours.gpod.Controller;

import fr.univtours.gpod.exceptions.NonexistentEntityException;
import fr.univtours.gpod.persistance.DAO.StructureDAO;
import fr.univtours.gpod.persistance.entities.SousStructure;
import fr.univtours.gpod.persistance.entities.Structure;
import fr.univtours.gpod.utils.Utilities;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;
import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;


public class StructureController extends ActionController {

    
          
        public StructureController(HttpServletRequest request, HttpServletResponse response, HttpSession session){
            super(request, response, session);     
        }    

        public String getListItems(){
            return this.getListItems(null);
        }
  
        public String saveItem(String type){
            String json="";
            Integer idStructure=0;
            StructureDAO dao = null;
            
            this.response.setContentType("application/json");
                    
            try{
                idStructure = Integer.parseInt(request.getParameter("idStructure"));
            }catch(NumberFormatException e){
                idStructure = 0;
            }
            
            
            String nom = request.getParameter("nom");
            String code = request.getParameter("code");
            String adresse1 = request.getParameter("adresse1");
            String adresse2 = request.getParameter("adresse2");
            String codePostal = request.getParameter("codePostal");
            String ville = request.getParameter("ville");
            String telephone = request.getParameter("telephone");
            String fax = request.getParameter("fax");
            String responsable = request.getParameter("responsable");
                   
            
            // Test des champs
            if ("".equals(nom)){
                json = "{success: false, errors:{reason:\'Des champs obligatoires n\\\'ont pas été saisis\'}}";
                return json;          
            }
                      
            try{
                 dao = new StructureDAO();
            } catch (Exception ex) {
                Logger.getLogger(StructureController.class.getName()).log(Level.SEVERE, null, ex);
                json = "{success: false, errors:{reason:\'"+ex.getMessage()+"\'}}";
                return json;
            }
            
            
            // Ajout nouvelle structure idStructure = 0
            if (idStructure == 0){

                    Structure structure = new Structure(nom, code, adresse1, adresse2, codePostal, ville, telephone, fax, responsable, type);
                    if (structure !=null){
                        try{
                            dao.create(structure);
                            json = "{success: true}";
                        } catch (Exception ex) {
                            Logger.getLogger(StructureController.class.getName()).log(Level.SEVERE, null, ex);
                            json = "{success: false, errors:{reason:\'"+ex.getMessage()+"\'}}";
                        }                        
                    }                                   

          
            // UPDATE Structure idstructure > 0
            }else if (idStructure > 0L){
                

                    Structure structure = dao.findStructure(idStructure);
                 
                    if (structure !=null){
                        
                        structure.setNom(nom);
                        structure.setCode(code);
                        structure.setAdresse1(adresse1);
                        structure.setAdresse2(adresse2);
                        structure.setCodePostal(codePostal);
                        structure.setVille(ville);
                        structure.setTelephone(telephone);
                        structure.setFax(fax);
                        structure.setResponsable(responsable);

                       
                        try{
                            dao.edit(structure);
                            json = "{success: true}"; 
                        } catch (NonexistentEntityException ex) {
                            Logger.getLogger(StructureController.class.getName()).log(Level.SEVERE, null, ex);
                            json = "{success: false, errors:{reason:\'"+ex.getMessage()+"\'}}";  
                        } catch (Exception ex) {
                            Logger.getLogger(StructureController.class.getName()).log(Level.SEVERE, null, ex);
                            json = "{success: false, errors:{reason:\'"+ex.getMessage()+"\'}}";  
                        }  
                    }                       
                    
        
            }else{
                json = "{success: false, errors:{reason:\'Impossible de trouver "+idStructure+"\'}}";
            }
             return json;            
            //return "{success: false, errors:{reason:\'"+msg+"\'}};
        }
        
        protected List<Structure> getListStructure(String type){
            List<Structure> structureList = null;
            
            // ------------------------------------------------------------------
            int maxResults = -1;
            int firstResult = -1;
            try{
                maxResults = Integer.parseInt(request.getParameter("limit"));
                firstResult = Integer.parseInt(request.getParameter("start"));
                if (maxResults == 0){
                    maxResults=10;
                }
            }catch(NumberFormatException e){
            }
            String searchValue = request.getParameter("query");
            String searchColumns = request.getParameter("fields");
            // ------------------------------------------------------------------

   
            
            try {
                StructureDAO dao = new StructureDAO();
                structureList = dao.findStructureEntities(searchValue, searchColumns, maxResults, firstResult, type);
            } catch (Exception ex) {
                Logger.getLogger(StructureController.class.getName()).log(Level.SEVERE, null, ex);
            }  
            return structureList;
                
        }
        
        protected int getListStructureCount(String type){
            
            // ------------------------------------------------------------------
            String searchValue = request.getParameter("query");
            String searchColumns = request.getParameter("fields");
            // ------------------------------------------------------------------
            int nbItems = 0;
   
            
            try {
                StructureDAO dao = new StructureDAO();
                nbItems = dao.getStructureCount(searchValue, searchColumns, type);
            } catch (Exception ex) {
                Logger.getLogger(StructureController.class.getName()).log(Level.SEVERE, null, ex);
            }  
            return nbItems;
                
        }        
        
    // Methode utilisée pour les édition XLS et PDF
        // retourne l'item à partir de l'id. Si l'id n'est pas définit, on a demandé l'impression de la liste (voir méthode getXLSList()
        protected Structure getItemFromId(){

                Structure structure = null;
                Integer idStructure = 0;
                String idStructureS = request.getParameter("id");

                if (idStructureS!=null && !"".equals(idStructureS)){
                    try{
                        idStructure = Integer.parseInt(idStructureS);
                        if (idStructure > 0 ){
                            try {
                                StructureDAO dao = new StructureDAO();
                                structure = dao.findStructure(idStructure);
                            } catch (Exception ex) {
                                Logger.getLogger(StructureController.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }
                    }catch(NumberFormatException ex){
                        Logger.getLogger(StructureController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                return structure;

        }        
                
        public String getListItems(String type){

          
            String json = "";
            List<Structure> structures = this.getListStructure(type);
             
            try {
                
                this.response.setContentType("application/json");
                
                if (structures !=null && structures.size()>0){
                    json = "{'totalCount':'"+this.getListStructureCount(type) +"','data':[";
                    for (Structure u : structures){
                        json += " {'idStructure':'"+u.getIdStructure()+"',";
                        json += "'code':'"+Utilities.addSlashes(u.getCode())+"',";
                        json += "'nom':'"+Utilities.addSlashes(u.getNom())+"',";
                        json += "'adresse1':'"+Utilities.addSlashes(u.getAdresse1())+"',";
                        json += "'adresse2':'"+Utilities.addSlashes(u.getAdresse2())+"',";
                        json += "'codePostal':'"+u.getCodePostal()+"',";
                        json += "'ville':'"+Utilities.addSlashes(u.getVille())+"',";
                        json += "'telephone':'"+u.getTelephone()+"',";
                        json += "'fax':'"+u.getFax()+"',";
                        json += "'responsable':'"+Utilities.addSlashes(u.getResponsable())+"'},";
                    }                    
                    json += "]}";
                }else{
                    json = "{'totalCount':'0','data':[]}";
                }
            } catch (Exception ex) {
                Logger.getLogger(StructureController.class.getName()).log(Level.SEVERE, null, ex);
            }
            return json;                   
        }      
       
        
        @Override
        public String deleteItem(){
   
            this.response.setContentType("application/json");
            
            String json = "{success: false}";
            String msg = "";
            int idStructure = 0;
            boolean success = true;            
            try{
                idStructure= Integer.parseInt(request.getParameter("id"));
            }catch(NumberFormatException ex){
                success = false;
            }
                                    
            if (idStructure>= 0){
                    try{
                        StructureDAO dao = new StructureDAO();
                        dao.destroy(idStructure);
                    
                    } catch (NonexistentEntityException ex) {
                            Logger.getLogger(StructureController.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (Exception ex) {
                        
                        // je gère les erreurs de suppression
                            String message = "";
                            try{
                                message = ex.getMessage();
                                int deb = message.indexOf("since the ")+10;

                                message = message.substring(deb);
                                int apo = message.indexOf(" ");
                                message = message.substring(0, apo); 
                            }catch(NullPointerException e2){                                      
                            }
                                                        
                            
                            if (!"".equals(message)){
                                msg = "Impossible de supprimer cette ligne, elle est utilisée par un objet \""+message+"\"";
                            }else{
                                msg = "Impossible de supprimer cette ligne, Erreur logiciel !!";                                
                                Logger.getLogger(ActionController.class.getName()).log(Level.SEVERE, null, ex);
                            }
                            success = false;                                                        
                    }
               
            }else{
                success = false;
                msg = "Structure " + idStructure + " inconnue";
            }
            
            
            if (success){
                json = "{success: true}";            
            }else{
                this.response.setStatus(424);
                json = "{success: false, errors:{reason:\'"+msg+"\'}}";
            }
            return json;
        } 
 
              
        
        
        public String getPDF(){
             //----------------------------------------------------
            //Autorisations en lecture
            String resp = authorizedProfils.getReadAuthorizedJsonTicket();
            if (!"".equals(resp)){
                return resp;
            }
            //----------------------------------------------------   
            
            String json="{'success':false}"; 
            Structure structure = this.getItemFromId();

        
            PdfDocumentFactory pdf = new PdfDocumentFactory("Structure_" + structure.getIdStructure());

            pdf.addVerticalTable(structure.getNom(), 24, structure.getDataForVerticalTable(), 0);
            pdf.addHorizontalTable("Equipes/Départements", 15, SousStructure.getDataForHorizontalTable(structure.getSousStructure()), 1, null);                

            pdf.getDocument().close();     

            if (!"".equals(pdf.getFileName())){
                return "{'success':true,'fileName':'"+pdf.getFileName()+"'}";
            }else{
                return json;   
            }              
        }
        
       
        
        protected String getXLSList(String type){

            String fileName = "";
            String json="{'success':false}"; 
            
            List<Structure> structureList = null;
            
            // on cherche si on demander l'édition d'un seul utilisateur id<>0
            Structure structure = this.getItemFromId();
            //si utilisateur = null => c'est que id=0 car on a demander la liste de tous les utilisateurs
            if (structure==null){                
                structureList = this.getListStructure(type);
            }else{
                structureList = new ArrayList<Structure>();
                structureList.add(structure);
            }
            
             try {

                DateTimeFormatter FILE_FORMAT = DateTimeFormat.forPattern("yyyyMMddHmsS");
                DateTimeFormatter USER_FORMAT = DateTimeFormat.forPattern("H:m dd/MM/yyyy");


                LocalDateTime now = new LocalDateTime();
                String dateStr = now.toString(FILE_FORMAT);


                fileName = "Structure_"+dateStr+".xls";

                WritableWorkbook wrtWorkbook = Workbook.createWorkbook(new File(fileName));

                WritableSheet sheet1;

                sheet1 = wrtWorkbook.createSheet("Laboratoires", 0);

                Label titre = new Label(0, 0, "Unités de recherche, le " + now.toString(USER_FORMAT));
                sheet1.addCell(titre);

                sheet1.addCell(new Label(0, 2, "Nom"));
                sheet1.addCell(new Label(1, 2, "Code"));
                sheet1.addCell(new Label(2, 2, "Adresse"));
                sheet1.addCell(new Label(3, 2, "Complement"));
                sheet1.addCell(new Label(4, 2, "Code Postal"));
                sheet1.addCell(new Label(5, 2, "Ville"));       
                sheet1.addCell(new Label(6, 2, "Tel"));       
                sheet1.addCell(new Label(7, 2, "Fax"));       
                sheet1.addCell(new Label(8, 2, "Responsable"));  
                
                sheet1.addCell(new Label(9, 2, "Nom Equipe")); 
                sheet1.addCell(new Label(10, 2, "code Equipe")); 
                sheet1.addCell(new Label(11, 2, "Responsable Equipe")); 
                    
                int j=3; // pour la ligne
                for (Structure u : structureList){
                    sheet1.addCell(new Label(0, j, u.getNom()));
                    sheet1.addCell(new Label(1, j, u.getCode()));
                    sheet1.addCell(new Label(2, j, u.getAdresse1()));
                    sheet1.addCell(new Label(3, j, u.getAdresse2()));
                    sheet1.addCell(new Label(4, j, u.getCodePostal()));
                    sheet1.addCell(new Label(5, j, u.getVille()));
                    sheet1.addCell(new Label(6, j, u.getTelephone()));
                    sheet1.addCell(new Label(7, j, u.getFax()));
                    sheet1.addCell(new Label(8, j, u.getResponsable()));  
                    for (SousStructure ss :u.getSousStructure()){
                        j++;
                        sheet1.addCell(new Label(9, j, ss.getNom()));
                        sheet1.addCell(new Label(10, j, ss.getCode()));
                        sheet1.addCell(new Label(11, j, ss.getResponsable()));                          
                    }

                    j++; //on change de ligne
                }
             
                wrtWorkbook.write();
                wrtWorkbook.close();

                json = "{'success':true,'fileName':'"+fileName+"'}";


            } catch (IOException ex) {
                Logger.getLogger(StructureController.class.getName()).log(Level.SEVERE, null, ex);
                return "{'success':false}";
            } catch (RowsExceededException ex) {
                Logger.getLogger(StructureController.class.getName()).log(Level.SEVERE, null, ex);
                return "{'success':false}";
            } catch (WriteException ex) {
                Logger.getLogger(StructureController.class.getName()).log(Level.SEVERE, null, ex);
                return "{'success':false}";
            }  

            return json;

        }         
        
        
}
