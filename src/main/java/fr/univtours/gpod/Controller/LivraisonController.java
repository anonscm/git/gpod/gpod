/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2012/01/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/

package fr.univtours.gpod.Controller;


import fr.univtours.gpod.HTML.HTML;
import fr.univtours.gpod.persistance.DAO.LivraisonDAO;
import fr.univtours.gpod.persistance.entities.Article;
import fr.univtours.gpod.persistance.entities.Livraison;
import fr.univtours.gpod.utils.AuthorizedProfils;
import fr.univtours.gpod.utils.Utilities;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 *
 * @author geoffroy.vibrac
 */
public class LivraisonController  extends ActionController {

        static DateTimeFormatter DATE_FORMAT = DateTimeFormat.forPattern("dd/MM/yyyy");

        public LivraisonController(HttpServletRequest request, HttpServletResponse response, HttpSession session){
            super(request, response, session);
            authorizedProfils = new AuthorizedProfils(request, response, session);
            authorizedProfils.addReadAuthorisedProfile("AD","LA", "RA");
            authorizedProfils.addWriteAuthorisedProfile("AD", "LA");                 
        }

        public String getHtml(){
            //----------------------------------------------------
            //Autorisations         
            String resp = authorizedProfils.getAuthorizedHtmlTicket();
            if (!"".equals(resp)){
                return resp;
            }
            //----------------------------------------------------  


                response.setContentType("text/html;charset=iso-8859-1");

                HTML html = HTML.getInstance();
                html.getHeader().setTitle("Gestion des produits dangereux &gt; Gestion des livraisons");
                html.setExtjsHeader();
                html.getHeader().getCss().addLine("lib/multiselect/Multiselect.css");
                html.getHeader().getCss().addLine("lib/search/gridsearch.css");
                html.getHeader().getCss().addLine("lib/msg/msg.css");

                html.getHeader().getJs().addBlock("var B_nameUser = '"+Utilities.addSlashes(utilisateur.getNom())+"'");
                html.getHeader().getJs().addBlock("var B_lastCx = '"+Utilities.javaDateTIMEToString(utilisateur.getDateAvantDerniereConnexion())+"'");                 
                html.getHeader().getJs().addLine("version.js");
                html.getHeader().getJs().addLine("lib/printAction.js");
                 html.getHeader().getJs().addLine("lib/msg/msg.js");
                html.getHeader().getJs().addLine("lib/multiselect/Multiselect.js");
                html.getHeader().getJs().addLine("lib/multiselect/ItemSelector.js");
                html.getHeader().getJs().addLine("lib/search/gridSearch.js");
                html.getHeader().getJs().addLine("ui/search/AireStockage.js");
                html.getHeader().getJs().addLine("ui/search/SousStructure.js");
                html.getHeader().getJs().addLine("ui/search/produit.js");
                html.getHeader().getJs().addLine("lib/excelButton.js");
                html.getHeader().getJs().addLine("ui/container.LivraisonSimple.js");
                html.getHeader().getJs().addLine("ui/menus/menu."+ session.getAttribute("profil") +".js");
                html.getHeader().getJs().addLine("ui/squelette.js");
                html.getBody().setBodyElement("<table align='center' class='contenttable' cellpadding='0' cellspacing='0' border='0'><tr><td id='centerPanel'></td></tr></table>");


                return html.getHTMLCode();
        }

        public String getPDF(){
             //----------------------------------------------------
            //Autorisations en lecture
            String resp = authorizedProfils.getReadAuthorizedJsonTicket();
            if (!"".equals(resp)){
                return resp;
            }
            //----------------------------------------------------   
            
            String json="{'success':false}"; 
            Livraison livraison = this.getItemFromIdLivraison();
            
        
            PdfDocumentFactory pdf = new PdfDocumentFactory("livraison_" + livraison.getIdLivraison());

            pdf.addVerticalTable(livraison.getNumeroBonLivraison(), 24, livraison.getDataForVerticalTable(), 0);
            pdf.addHorizontalTable("Articles", 15, Article.getDataForHorizontalTable(livraison.getArticleList()), 1, null);
            

            pdf.getDocument().close();     

            if (!"".equals(pdf.getFileName())){
                return "{'success':true,'fileName':'"+pdf.getFileName()+"'}";
            }else{
                return json;   
            }
                
            
                    
        }

//        public String saveItem(){
//            //----------------------------------------------------
//            //Autorisations         
//            String resp = authorizedProfils.getWriteAuthorizedJsonTicket();
//            if (!"".equals(resp)){
//                return resp;
//            }
//            //----------------------------------------------------                  
//            String json="";
//            LivraisonDAO dao = null;
//
//            this.response.setContentType("application/json");
//
//
//            // --------------------------------------------------------------
//            // <editor-fold defaultstate="expanded" desc="recup champs formulaire">
//
//            String idAireS = request.getParameter("idAire");
//            String idSousStructureS = request.getParameter("idSousStructure");
//            String idProduitConditionnementS = request.getParameter("idProduitConditionnement");
//            String dateEntreeS = request.getParameter("dateEntree");
//            String quantiteS = request.getParameter("quantite");
//            ProduitConditionnement produitConditionnement = null;
//            AireStockage aire = null;
//            SousStructure sousStructure = null;
//            String articlesLivraison = request.getParameter("articlesLivraison");
//            List<Article> articles = null;
//
//            Integer idAire;
//            try{
//                idAire = Integer.parseInt(idAireS);
//                if (idAire > 0 ){
//                    try {
//                        AireStockageDAO daob = new AireStockageDAO();
//                        aire = daob.findAireStockage(idAire);
//
//                    } catch (Exception ex) {
//                        Logger.getLogger(LivraisonController.class.getName()).log(Level.SEVERE, null, ex);
//                    }
//                }
//            }catch(NumberFormatException e){
//                idAire = 0;
//            }
//
//            Integer idSousStructure;
//            try{
//                idSousStructure = Integer.parseInt(idSousStructureS);
//                if (idSousStructure > 0 ){
//                    try {
//                        SousStructureDAO daob = new SousStructureDAO();
//                        sousStructure = daob.findSousStructure(idSousStructure);
//
//                    } catch (Exception ex) {
//                        Logger.getLogger(LivraisonController.class.getName()).log(Level.SEVERE, null, ex);
//                    }
//                }
//            }catch(NumberFormatException e){
//                idSousStructure = 0;
//            }
//
//
//            Integer idProduitConditionnement;
//            try{
//                idProduitConditionnement = Integer.parseInt(idProduitConditionnementS);
//                if (idProduitConditionnement > 0 ){
//                    try {
//                        ProduitConditionnementDAO daob = new ProduitConditionnementDAO();
//                        produitConditionnement = daob.findProduitConditionnement(idProduitConditionnement);
//
//                    } catch (Exception ex) {
//                        Logger.getLogger(LivraisonController.class.getName()).log(Level.SEVERE, null, ex);
//                    }
//                }
//            }catch(NumberFormatException e){
//                idProduitConditionnement = 0;
//            }
//
//            Float quantite;
//            try{
//                quantite = Float.parseFloat(quantiteS);
//            }catch(NumberFormatException ex){
//                quantite=0.0F;
//            }
//
//            DateTime dateLivraison = null;
//            // la date ne doit pas être null et doit faire 10 de longueur = jj/mm/aaaa
//            if (dateEntreeS != null && dateEntreeS.length() == 10){
//                try{
//                    int year = Integer.parseInt(dateEntreeS.substring(6));
//                    int month = Integer.parseInt(dateEntreeS.substring(3, 5));
//                    int day = Integer.parseInt(dateEntreeS.substring(0, 2));
//                    dateLivraison = new DateTime(year, month, day,0,0,0,0);
//                }catch(NumberFormatException ex){
//                   Logger.getLogger(LivraisonController.class.getName()).log(Level.SEVERE, null, ex);
//                }
//            }
//
//             articles = JsonConversionUtilities.jsonArticleArrayToList(articlesLivraison, idSousStructure);
//
//            // </editor-fold>
//            // --------------------------------------------------------------
//
//            // --------------------------------------------------------------
//            // <editor-fold defaultstate="expanded" desc="Test des champs">
//            if (produitConditionnement == null || aire == null || sousStructure == null|| quantite<1 || articles==null || articles.isEmpty()){
//                json = "{success: false, errors:{reason:\'Des champs obligatoires n\\\'ont pas été saisis\'}}";
//                return json;
//            }
//            // </editor-fold>
//            // --------------------------------------------------------------
//
//
//            try{
//                 dao = new LivraisonDAO();
//            } catch (Exception ex) {
//                Logger.getLogger(LivraisonController.class.getName()).log(Level.SEVERE, null, ex);
//                json = "{success: false, errors:{reason:\'"+ex.getMessage()+"\'}}";
//                return json;
//            }
//
//
//            Livraison livraison = null;
//            ArrayList<LivraisonLigne> ll =  null;
//            //livraison = new Livraison(dateLivraison.toDate(), "", null, this.utilisateur, ll);
//            if (livraison !=null){
//                try{
//                    dao.create(livraison);
//                    json = "{success: true}";
//                } catch (Exception ex) {
//                    Logger.getLogger(LivraisonController.class.getName()).log(Level.SEVERE, null, ex);
//                    json = "{success: false, errors:{reason:\'"+ex.getMessage()+"\'}}";
//                }
//            }
//
//
//
//
//             return json;
//            //return "{success: false, errors:{reason:\'"+msg+"\'}};
//        }

      

        
        


//        public String getListItems(){
//
//            this.response.setContentType("application/json");
//
//            String json = "";
//            List<Livraison> livraisons = null;
//            // ------------------------------------------------------------------
//            int maxResults = -1;
//            int firstResult = -1;
//            try{
//                maxResults = Integer.parseInt(request.getParameter("limit"));
//                firstResult = Integer.parseInt(request.getParameter("start"));
//                if (maxResults == 0){
//                    maxResults=10;
//                }
//            }catch(NumberFormatException e){
//            }
//            String searchValue = request.getParameter("query");
//            String searchColumns = request.getParameter("fields");
//            // ------------------------------------------------------------------  
//
//
//            try {
//                LivraisonDAO dao = new LivraisonDAO();
//
//                //si l'utilisateur est administrateur, on affiche tous les résultats... sinon seulemence ceux de l'utilisateur
//                if ("AD".equals(utilisateur.getUtilisateurProfil().getCode())){
//                    livraisons = dao.findLivraisonEntities(maxResults, firstResult);
//                }else{
//                    livraisons = dao.findLivraisonEntitiesByUser(utilisateur, maxResults, firstResult);
//                }
//
//                this.response.setContentType("application/json");
//
//                if (livraisons !=null && livraisons.size()>0){
//                    json = "{'totalCount':'"+livraisons.size()+"','data':[";
//                    for (Livraison l : livraisons){
//                        DateTime dateLivraison = new DateTime(l.getDateLivraison());
//                        String dateLivraisonS = dateLivraison.toString(DATE_FORMAT);
//
//                        json += " {'idLivraison':'"+l.getIdLivraison()+"',";
//                        json += " 'CAS':'"+l.getArticles().get(0).getProduitConditionnement().getProduit().getSubstance().getCas()+"',";
//                        json += " 'idProduitConditionnement':'"+l.getArticles().get(0).getProduitConditionnement().getIdProduitConditionnement()+"',";
//                        json += "'nomProduit':'"+Utilities.addSlashes(l.getArticles().get(0).getProduitConditionnement().getProduit().getNom())+"',";
//                        json += "'dateLivraison':'"+dateLivraisonS+"',";
//                        json += "'idSousStructure':'"+l.getArticles().get(0).getSousStructure().getIdSousStructure()+"',";
//                        json += "'nomSousStructure':'"+l.getArticles().get(0).getSousStructure().getNom()+"',";
//                        json += "'quantite':'"+l.getArticles().size()+"'},";
//                    }
//                    json += "]}";
//                }else{
//                    json = "{'totalCount':'0','data':[]}";
//                }
//            } catch (Exception ex) {
//                Logger.getLogger(LivraisonController.class.getName()).log(Level.SEVERE, null, ex);
//                json = "{'totalCount':'0','data':[]}";
//            }
//            return json;
//        }

       
         

        public String getListArticles(){

            this.response.setContentType("application/json");

            String json = "{'totalCount':'0','data':[]}";

            List<Article> articles = this.getItemFromIdLivraison().getArticleList();
            
            if (articles !=null && articles.size()>0){
                json = "{'totalCount':'"+articles.size()+"','data':[";
                for (Article a : articles){
                    json += " {'idArticle':'"+a.getIdArticle()+"',";
                    json += " 'CAS':'"+a.getProduitConditionnement().getProduit().getSubstance().getCas()+"',";
                    json += " 'reference':'"+a.getProduitConditionnement().getReferenceFournisseur()+"',";
                    json += " 'idProduitConditionnement':'"+a.getProduitConditionnement().getIdProduitConditionnement()+"',";
                    json += "'nom':'"+Utilities.addSlashes(a.getProduitConditionnement().getProduit().getNom())+"',";
                    json += "'identifiantEtiquette':'"+Utilities.addSlashes(a.getIdentifiantEtiquette())+"',";
                    json += "'refrigere':'"+a.getProduitConditionnement().getProduit().isRefrigere()+"',";
                    json += "'idAire':'"+a.getAireStockage().getIdAire()+"'},";
                }
                json += "]}";
            }else{
                json = "{'totalCount':'0','data':[]}";
            }
                
            
            return json;
        }


        // Methode utilisée pour les édition XLS et PDF
        // retourne l'utilisateur à partir de l'id. Si l'id n'est pas définit, on a demandé l'impression de la liste (voir méthode getXLSList()
        private Livraison getItemFromIdLivraison(){
                Livraison livraison = null;
                int idLivraison= 0;
                String idLivraisonS = request.getParameter("id");
                if (idLivraisonS==null || "".equals(idLivraisonS)){
                    idLivraisonS = request.getParameter("idLivraison");                    
                }
                
                if (idLivraisonS!=null || !"".equals(idLivraisonS)){
                    try{
                        idLivraison = Integer.parseInt(idLivraisonS);
                        if (idLivraison > 0 ){
                            try {
                                LivraisonDAO dao = new LivraisonDAO();
                                livraison = dao.findLivraison(idLivraison);
                            } catch (Exception ex) {
                                Logger.getLogger(LivraisonController.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }
                    }catch(NumberFormatException ex){
                        Logger.getLogger(LivraisonController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                return livraison;
        }  
        
             
        







}
