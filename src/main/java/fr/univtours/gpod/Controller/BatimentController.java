/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2012/01/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/

package fr.univtours.gpod.Controller;
import fr.univtours.gpod.HTML.HTML;
import fr.univtours.gpod.exceptions.NonexistentEntityException;
import fr.univtours.gpod.persistance.DAO.BatimentDAO;
import fr.univtours.gpod.persistance.DAO.SiteDAO;
import fr.univtours.gpod.persistance.entities.*;
import fr.univtours.gpod.utils.Utilities;
import java.sql.SQLException;
import java.util.List;
import fr.univtours.gpod.utils.AuthorizedProfils;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;
import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class BatimentController extends ActionController{
          
           
        public BatimentController(HttpServletRequest request, HttpServletResponse response, HttpSession session){
            super(request, response, session);
            authorizedProfils = new AuthorizedProfils(request, response, session);
            authorizedProfils.addReadAuthorisedProfile("AD");
            authorizedProfils.addWriteAuthorisedProfile("AD");              
        }

        public String getHtml(){
            //----------------------------------------------------
            //Autorisations         
            String resp = authorizedProfils.getAuthorizedHtmlTicket();
            if (!"".equals(resp)){
                return resp;
            }
            //----------------------------------------------------   
                response.setContentType("text/html;charset=iso-8859-1");
            
                HTML html = HTML.getInstance();
                html.getHeader().setTitle("Gestion des produits dangereux &gt; Gestion des batiments");
                html.setExtjsHeader();
                html.getHeader().getCss().addLine("lib/multiselect/Multiselect.css");
                html.getHeader().getCss().addLine("lib/search/gridsearch.css");
                html.getHeader().getCss().addLine("lib/msg/msg.css");
                
                html.getHeader().getJs().addBlock("var B_nameUser = '"+Utilities.addSlashes(utilisateur.getNom())+"'");
                html.getHeader().getJs().addBlock("var B_lastCx = '"+Utilities.javaDateTIMEToString(utilisateur.getDateAvantDerniereConnexion())+"'");                 
                html.getHeader().getJs().addLine("version.js");
                html.getHeader().getJs().addLine("lib/printAction.js");
                html.getHeader().getJs().addLine("lib/msg/msg.js");
                html.getHeader().getJs().addLine("http://maps.google.com/maps/api/js?sensor=false");
                html.getHeader().getJs().addLine("lib/gmaps/GMapPanel3.js");
                html.getHeader().getJs().addLine("lib/multiselect/Multiselect.js");
                html.getHeader().getJs().addLine("lib/multiselect/ItemSelector.js");   
                html.getHeader().getJs().addLine("lib/search/gridSearch.js");
                html.getHeader().getJs().addLine("lib/excelButton.js");
                html.getHeader().getJs().addLine("ui/container.Batiments.js");
                html.getHeader().getJs().addLine("ui/menus/menu."+ session.getAttribute("profil") +".js");
                html.getHeader().getJs().addLine("ui/squelette.js");
                html.getBody().setBodyElement("<table align='center' class='contenttable' cellpadding='0' cellspacing='0' border='0'><tr><td id='centerPanel'></td></tr></table>");

                return html.getHTMLCode();
        }      
        
        public List<Batiment> getBatimentList(){
            
            List<Batiment> batimentList = null;
            int idSite = 0;

            // ------------------------------------------------------------------
            int maxResults = -1;
            int firstResult = -1;
            try{
                maxResults = Integer.parseInt(request.getParameter("limit"));
                firstResult = Integer.parseInt(request.getParameter("start"));
                if (maxResults == 0){
                    maxResults=10;
                }
            }catch(NumberFormatException e){
            }
            String searchValue = request.getParameter("query");
            String searchColumns = request.getParameter("fields");
            // ------------------------------------------------------------------  

            // récupération de conditions sur la liste à récupérer
            try{
                idSite = Integer.parseInt(request.getParameter("idSite"));
            }catch(NumberFormatException e){
            }

       
            int nbItems = 0;
            
            try {
                BatimentDAO dao = new BatimentDAO();
                if (idSite != 0){
                    SiteDAO siteDao = new SiteDAO();
                    Site site = siteDao.findSite(idSite);
                    if (site != null){
                        batimentList = dao.findBatimentFromSite(site);
                        nbItems = batimentList.size();
                    }
                }else{
                    batimentList = dao.findBatimentWithQuery(searchValue, searchColumns, maxResults, firstResult);
                    nbItems = dao.getBatimentCount(searchValue, searchColumns);
                }
            }catch(Exception ex){
                
            }
            
            return batimentList;
            
        }
        
        public int getBatimentCount(){
            int idSite = 0;

            // ------------------------------------------------------------------
            String searchValue = request.getParameter("query");
            String searchColumns = request.getParameter("fields");
            // ------------------------------------------------------------------  

            // récupération de conditions sur la liste à récupérer
            try{
                idSite = Integer.parseInt(request.getParameter("idSite"));
            }catch(NumberFormatException e){
            }

       
            int batimentCount = 0;
            
            try {
                BatimentDAO dao = new BatimentDAO();
                if (idSite != 0){
                    SiteDAO siteDao = new SiteDAO();
                    Site site = siteDao.findSite(idSite);
                    if (site != null){
                        batimentCount = dao.findBatimentFromSite(site).size();
                    }
                }else{
                    batimentCount = dao.getBatimentCount(searchValue, searchColumns);
                }
            }catch(Exception ex){                
            }
            
            return batimentCount;            
        }
        
        
        public String getListItems(){                       
                
            this.response.setContentType("application/json");
            String json = "";
            List<Batiment> batiments = this.getBatimentList();
               
            if (batiments !=null && batiments.size()>0){
                json = "{'totalCount':'"+this.getBatimentCount()+"','data':[";
                for (Batiment b : batiments){
                    json += " {'idBatiment':'"+b.getIdBatiment()+"',";
                    json += "'idSite':'"+b.getSite().getIdSite()+"',";                        
                    json += "'nomSite':'"+Utilities.addSlashes(b.getSite().getNom())+"',";  
                    json += "'code':'"+Utilities.addSlashes(b.getCode())+"',";
                    json += "'nom':'"+Utilities.addSlashes(b.getNom())+"',";
                    json += "'adresse1':'"+Utilities.addSlashes(b.getAdresse1())+"',";
                    json += "'adresse2':'"+Utilities.addSlashes(b.getAdresse2())+"',";
                    json += "'codePostal':'"+b.getCodePostal()+"',";
                    json += "'ville':'"+Utilities.addSlashes(b.getVille())+"',";
                    json += "'telephone':'"+b.getTelephone()+"',";
                    json += "'fax':'"+b.getFax()+"',";
                    json += "'longitude':'"+b.getLongitude()+"',";
                    json += "'latitude':'"+b.getLatitude()+"'},";
                }                    
                json += "]}";
            }else{
                json = "{'totalCount':'0','data':[]}";
            }

            return json;       
            
        }

        public String saveItem(){
            //----------------------------------------------------
            //Autorisations         
            String resp = authorizedProfils.getWriteAuthorizedJsonTicket();
            if (!"".equals(resp)){
                return resp;
            }
            //----------------------------------------------------               
            String json="";
            int idBatiment=0;
            BatimentDAO dao = null;
            
            this.response.setContentType("application/json");
                                                 
            try{
                idBatiment = Integer.parseInt(request.getParameter("idBatiment"));
            }catch(NumberFormatException e){
                idBatiment = 0;
            }
            
            int idSite;
            Site site=null;
            try{
                idSite = Integer.parseInt(request.getParameter("idSite"));
                if (idSite > 0){
                try {
                    SiteDAO daos = new SiteDAO();
                    site = daos.findSite(idSite);
                } catch (Exception ex) {
                    Logger.getLogger(BatimentController.class.getName()).log(Level.SEVERE, null, ex);
                }
                    
                    
                }
            }catch(NumberFormatException e){
                idSite = 0;
            }
            
            String nom = request.getParameter("nom");
            String code = request.getParameter("code");
            String adresse1 = request.getParameter("adresse1");
            String adresse2 = request.getParameter("adresse2");
            String codePostal = request.getParameter("codePostal");
            String ville = request.getParameter("ville");
            String telephone = request.getParameter("telephone");
            String fax = request.getParameter("fax");                             
            
            
            String longitudeS = request.getParameter("longitude").replaceAll(",", ".");
            Double longitude;
            try{
                longitude = Double.parseDouble(longitudeS);
            }catch(NumberFormatException ex){
                longitude=0.0;
            }
            
            String latitudeS = request.getParameter("latitude").replaceAll(",", ".");
            Double latitude;
            try{
                latitude = Double.parseDouble(latitudeS);
            }catch(NumberFormatException ex){
                latitude=0.0;
            }                        
            
            // Test des champs
            if ("".equals(nom) || site == null){
                json = "{success: false, errors:{reason:\'Des champs obligatoires n\\\'ont pas été saisis\'}}";
                return json;          
            }
                      
            try{
                 dao = new BatimentDAO();              
            } catch (Exception ex) {
                Logger.getLogger(BatimentController.class.getName()).log(Level.SEVERE, null, ex);
                json = "{success: false, errors:{reason:\'"+ex.getMessage()+"\'}}";
                return json;
            }
                      

            
             // INSERT id = 0
            if (idBatiment == 0){

                    Batiment batiment = new Batiment(nom, site, code, adresse1, adresse2, codePostal, ville, telephone, fax, longitude, latitude);
                    if (batiment !=null){
                        try{
                            dao.create(batiment);
                            json = "{success: true}";
                        } catch (Exception ex) {
                            Logger.getLogger(BatimentController.class.getName()).log(Level.SEVERE, null, ex);
                            json = "{success: false, errors:{reason:\'"+ex.getMessage()+"\'}}";
                        }
                    }                                  

          
            // UPDATE id > 0
            }else if (idBatiment > 0L){
                

                    Batiment batiment = dao.findBatiment(idBatiment);
                 
                    if (batiment !=null){
                        
                        batiment.setNom(nom);
                        batiment.setSite(site);
                        batiment.setCode(code);
                        batiment.setAdresse1(adresse1);
                        batiment.setAdresse2(adresse2);
                        batiment.setCodePostal(codePostal);
                        batiment.setVille(ville);
                        batiment.setTelephone(telephone);
                        batiment.setFax(fax);
                        batiment.setLatitude(latitude);
                        batiment.setLongitude(longitude);

                        
                        try{
                            dao.edit(batiment);
                            json = "{success: true}"; 
                        } catch (NonexistentEntityException ex) {
                            Logger.getLogger(BatimentController.class.getName()).log(Level.SEVERE, null, ex);
                            json = "{success: false, errors:{reason:\'"+ex.getMessage()+"\'}}";  
                        } catch (Exception ex) {
                            Logger.getLogger(BatimentController.class.getName()).log(Level.SEVERE, null, ex);
                            json = "{success: false, errors:{reason:\'"+ex.getMessage()+"\'}}";  
                        }                            
                    }                       
                    
        
            }else{
                json = "{success: false, errors:{reason:\'Impossible de trouver "+idBatiment+"\'}}";
            }
                     
            
            return json;            
            //return "{success: false, errors:{reason:\'"+msg+"\'}};
        }        
        

    // Methode utilisée pour les édition XLS et PDF
    // retourne l'utilisateur à partir de l'id. Si l'id n'est pas définit, on a demandé l'impression de la liste (voir méthode getXLSList()
    private Batiment getItemFromId(){
        
            Batiment batiment = null;
            int idBatiment= 0;
            String idBatimentS = request.getParameter("id");
            
            if (idBatimentS!=null && !"".equals(idBatimentS)){
                try{
                    idBatiment = Integer.parseInt(idBatimentS);
                    if (idBatiment > 0 ){
                        try {
                            BatimentDAO dao = new BatimentDAO();
                            batiment = dao.findBatiment(idBatiment);
                        } catch (Exception ex) {
                            Logger.getLogger(BatimentController.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }catch(NumberFormatException ex){
                    Logger.getLogger(BatimentController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            return batiment;
        
    }        
        
       public String getPDF(){
             //----------------------------------------------------
            //Autorisations en lecture
            String resp = authorizedProfils.getReadAuthorizedJsonTicket();
            if (!"".equals(resp)){
                return resp;
            }
            //----------------------------------------------------   
            
            String json="{'success':false}"; 

            Batiment batiment = this.getItemFromId();

        
            PdfDocumentFactory pdf = new PdfDocumentFactory("batiment_" + batiment.getIdBatiment());

            pdf.addVerticalTable(batiment.getNom(), 24, batiment.getDataForVerticalTable(), 0);
            pdf.addHorizontalTable("Salles de Stockage", 15, SalleStockage.getDataForHorizontalTable(batiment.getSalles()), 1, null);
            // je récupère toutes les aire de stockage de toutes les salles du Batiments
            List<AireStockage> aireList = new ArrayList<>();
            for (SalleStockage ss : batiment.getSalles()){
                aireList.addAll(ss.getAires());
            }
            pdf.addHorizontalTable("Aires de Stockage", 15, AireStockage.getDataForHorizontalTable(aireList), 1, null);
            
            if (batiment.getLatitude()!=0 && batiment.getLongitude()!=0){

                String urlImage = "http://maps.google.com/maps/api/staticmap?center="
                    + batiment.getLatitude()+"," + batiment.getLongitude()
                    + "&zoom=17&size=390x280&maptype=roadmap&markers=color:red|"
                    + batiment.getLatitude()+"," + batiment.getLongitude()
                    + "&sensor=false";                    

                pdf.addImageFromUrl(urlImage, 390, 280, 2);  


            }            

            pdf.getDocument().close();     

            if (!"".equals(pdf.getFileName())){
                return "{'success':true,'fileName':'"+pdf.getFileName()+"'}";
            }else{
                return json;   
            }              
        }
        
       
        
        // Cette méthode est appelée quand on veux la liste des utilisateurs (id=0)
        // OU quand on veux éditer 1 utilisateur (id <>0)
        public String getXLSList(){

            String fileName = "";
            String json="{'success':false}"; 
            
            List<Batiment> batimentList = null;
            
            // on cherche si on demander l'édition d'un seul utilisateur id<>0
            Batiment batiment = this.getItemFromId();
            //si utilisateur = null => c'est que id=0 car on a demander la liste de tous les utilisateurs
            if (batiment==null){                
                batimentList = this.getBatimentList();
            }else{
                batimentList = new ArrayList<Batiment>();
                batimentList.add(batiment);
            }
            
             try {

                DateTimeFormatter FILE_FORMAT = DateTimeFormat.forPattern("yyyyMMddHmsS");
                DateTimeFormatter USER_FORMAT = DateTimeFormat.forPattern("H:m dd/MM/yyyy");


                LocalDateTime now = new LocalDateTime();
                String dateStr = now.toString(FILE_FORMAT);


                fileName = "Batiment_"+dateStr+".xls";

                WritableWorkbook wrtWorkbook = Workbook.createWorkbook(new File(fileName));

                WritableSheet sheet1;

                sheet1 = wrtWorkbook.createSheet("Batiment", 0);

                Label titre = new Label(0, 0, "Batiment, le " + now.toString(USER_FORMAT));
                sheet1.addCell(titre);

                sheet1.addCell(new Label(0, 2, "Nom"));
                sheet1.addCell(new Label(1, 2, "Code"));
                sheet1.addCell(new Label(2, 2, "Adresse 1"));
                sheet1.addCell(new Label(3, 2, "Adresse 2"));
                sheet1.addCell(new Label(4, 2, "Code Postal"));
                sheet1.addCell(new Label(5, 2, "Ville")); 
                
                sheet1.addCell(new Label(6, 2, "Nom Salle Stockage "));
                sheet1.addCell(new Label(7, 2, "Code Salle Stockage "));
                sheet1.addCell(new Label(8, 2, "Description Salle Stockage "));
   
                sheet1.addCell(new Label(9, 2, "Nom Aire Stockage "));
                sheet1.addCell(new Label(10, 2, "Code Aire Stockage "));
                sheet1.addCell(new Label(11, 2, "Type Aire Stockage "));
                sheet1.addCell(new Label(12, 2, "Refrigéré"));
                sheet1.addCell(new Label(13, 2, "Description Aire Stockage "));                
                
                    
                int j=3; // pour la ligne
                for (Batiment u : batimentList){
                    sheet1.addCell(new Label(0, j, u.getNom()));
                    sheet1.addCell(new Label(1, j, u.getCode()));
                    sheet1.addCell(new Label(2, j, u.getAdresse1()));
                    sheet1.addCell(new Label(3, j, u.getAdresse2()));
                    sheet1.addCell(new Label(4, j, u.getCodePostal()));
                    sheet1.addCell(new Label(5, j, u.getVille()));
                    for (SalleStockage ss : u.getSalles()){
                        j++;
                        sheet1.addCell(new Label(6, j, ss.getNom()));
                        sheet1.addCell(new Label(7, j, ss.getCode()));                        
                        sheet1.addCell(new Label(8, j, ss.getDescription()));
                        
                        for (AireStockage as : ss.getAires()){
                            j++;
                            sheet1.addCell(new Label(9, j, as.getNom()));
                            sheet1.addCell(new Label(10, j, as.getCode()));                        
                            sheet1.addCell(new Label(11, j, as.getTypeAire().getNom()));                            
                            sheet1.addCell(new Label(12, j, Utilities.booleanToOuiNon(as.isRefrigere())));                        
                            sheet1.addCell(new Label(13, j, as.getDescription()));                                                        
                        }                        
                    } 
                    j++; //on change de ligne
                }
             
                wrtWorkbook.write();
                wrtWorkbook.close();

                json = "{'success':true,'fileName':'"+fileName+"'}";


            } catch (IOException ex) {
                Logger.getLogger(BatimentController.class.getName()).log(Level.SEVERE, null, ex);
                return "{'success':false}";
            } catch (RowsExceededException ex) {
                Logger.getLogger(BatimentController.class.getName()).log(Level.SEVERE, null, ex);
                return "{'success':false}";
            } catch (WriteException ex) {
                Logger.getLogger(BatimentController.class.getName()).log(Level.SEVERE, null, ex);
                return "{'success':false}";
            }  

            return json;

        }        

}
