/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2012/01/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/

package fr.univtours.gpod.Controller;

import fr.univtours.gpod.HTML.HTML;
import fr.univtours.gpod.exceptions.NonexistentEntityException;
import fr.univtours.gpod.persistance.DAO.AireStockageDAO;
import fr.univtours.gpod.persistance.DAO.BatimentDAO;
import fr.univtours.gpod.persistance.DAO.SalleStockageDAO;
import fr.univtours.gpod.persistance.DAO.TypeAireDAO;
import fr.univtours.gpod.persistance.entities.AireStockage;
import fr.univtours.gpod.utils.Utilities;
import java.util.List;
import fr.univtours.gpod.persistance.entities.Batiment;
import fr.univtours.gpod.persistance.entities.SalleStockage;
import fr.univtours.gpod.persistance.entities.TypeAire;
import fr.univtours.gpod.utils.AuthorizedProfils;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class SalleStockageController extends ActionController {

        public SalleStockageController(HttpServletRequest request, HttpServletResponse response, HttpSession session){
            super(request, response, session);
            authorizedProfils = new AuthorizedProfils(request, response, session);
            authorizedProfils.addReadAuthorisedProfile("AD");
            authorizedProfils.addWriteAuthorisedProfile("AD");            
        }
    
        public String getHtml(){
            //----------------------------------------------------
            //Autorisations         
            String resp = authorizedProfils.getAuthorizedHtmlTicket();
            if (!"".equals(resp)){
                return resp;
            }
            //----------------------------------------------------    
                response.setContentType("text/html;charset=iso-8859-1");
            
                HTML html = HTML.getInstance();
                html.getHeader().setTitle("Gestion des produits dangereux &gt; Gestion des aires de stockage");
                html.setExtjsHeader();
                html.getHeader().getCss().addLine("lib/multiselect/Multiselect.css");
                html.getHeader().getCss().addLine("lib/search/gridsearch.css");
                html.getHeader().getCss().addLine("lib/msg/msg.css");

                html.getHeader().getJs().addBlock("var B_nameUser = '"+Utilities.addSlashes(utilisateur.getNom())+"'");
                html.getHeader().getJs().addBlock("var B_lastCx = '"+Utilities.javaDateTIMEToString(utilisateur.getDateAvantDerniereConnexion())+"'"); 
                html.getHeader().getJs().addLine("version.js");
                html.getHeader().getJs().addLine("lib/printAction.js");
                html.getHeader().getJs().addLine("lib/msg/msg.js");
                html.getHeader().getJs().addLine("http://maps.google.com/maps/api/js?sensor=false");
                html.getHeader().getJs().addLine("lib/gmaps/GMapPanel3.js");
                html.getHeader().getJs().addLine("lib/multiselect/Multiselect.js");
                html.getHeader().getJs().addLine("lib/multiselect/ItemSelector.js");   
                html.getHeader().getJs().addLine("lib/search/gridSearch.js");
                html.getHeader().getJs().addLine("lib/excelButton.js");
                html.getHeader().getJs().addLine("ui/container.SalleStockage.js");
                html.getHeader().getJs().addLine("ui/menus/menu."+ session.getAttribute("profil") +".js");
                html.getHeader().getJs().addLine("ui/squelette.js");
                html.getBody().setBodyElement("<table align='center' class='contenttable' cellpadding='0' cellspacing='0' border='0'><tr><td id='centerPanel'></td></tr></table>");

                return html.getHTMLCode();
        }  
        
 
       public String getListItems(){
            
            
            String json = "";
            List<SalleStockage> salles = null;
            int idBatiment = 0;

            // ------------------------------------------------------------------
            int maxResults = -1;
            int firstResult = -1;
            try{
                maxResults = Integer.parseInt(request.getParameter("limit"));
                firstResult = Integer.parseInt(request.getParameter("start"));
                if (maxResults == 0){
                    maxResults=10;
                }
            }catch(NumberFormatException e){
            }
            String searchValue = request.getParameter("query");
            String searchColumns = request.getParameter("fields");
            // ------------------------------------------------------------------  


            // récupération de conditions sur la liste à récupérer
            try{
                idBatiment = Integer.parseInt(request.getParameter("idBatiment"));
            }catch(NumberFormatException e){
            }
            
            int nbItems = 0;
            
            try {
                SalleStockageDAO dao = new SalleStockageDAO();
                if (idBatiment != 0){
                    BatimentDAO batDao = new BatimentDAO();
                    Batiment batiment = batDao.findBatiment(idBatiment);
                    if (batiment != null){
                        salles = dao.findSalleStockageFromBatiment(batiment);
                        nbItems = salles.size();
                    }
                }else{
                    salles = dao.findSalleStockageWithQuery(searchValue, searchColumns, maxResults, firstResult);
                    nbItems = dao.getSalleStockageCount(searchValue, searchColumns);
                }
                
                
                this.response.setContentType("application/json");
                
                if (salles !=null && salles.size()>0){
                    json = "{'totalCount':'"+nbItems+"','data':[";
                    for (SalleStockage b : salles){                                                
                        json += " {'idSalleStockage':'"+b.getIdSalle()+"',";
                        json += "'idBatiment':'"+b.getBatiment().getIdBatiment()+"',";  
                        json += "'nomBatiment':'"+Utilities.addSlashes(b.getBatiment().getNom())+"',";                        
                        json += "'nom':'"+Utilities.addSlashes(b.getNom())+"',";
                        json += "'code':'"+Utilities.addSlashes(b.getCode())+"',";
                        json += "'description':'"+b.getDescription()+"'},";
                    }                    
                    json += "]}";
                }else{
                    json = "{'totalCount':'0','data':[]}";
                }
            } catch (Exception ex) {
                Logger.getLogger(SalleStockageController.class.getName()).log(Level.SEVERE, null, ex);
            }
            return json;       
            
        }        
        
       
        public String saveItem(){
            //----------------------------------------------------
            //Autorisations         
            String resp = authorizedProfils.getWriteAuthorizedJsonTicket();
            if (!"".equals(resp)){
                return resp;
            }
            //----------------------------------------------------               
            String json="";
            int idSalleStockage=0;
            Batiment batiment = null;
            SalleStockageDAO dao = null;
            
            this.response.setContentType("application/json");
                                                 
            try{
                idSalleStockage = Integer.parseInt(request.getParameter("idSalleStockage"));
            }catch(NumberFormatException e){
                idSalleStockage = 0;
            }

            
            int idBatiment;
            try{
                idBatiment = Integer.parseInt(request.getParameter("idBatiment"));
                if (idBatiment > 0 ){
                    try {
                        BatimentDAO daob = new BatimentDAO();
                        batiment = daob.findBatiment(idBatiment);
                        
                    } catch (Exception ex) {
                        Logger.getLogger(SalleStockageController.class.getName()).log(Level.SEVERE, null, ex);
                    }                    
                }                
            }catch(NumberFormatException e){
                idBatiment = 0;
            }
                        
            
            String nom = request.getParameter("nom");
            String code = request.getParameter("code");
            String description = request.getParameter("description");
            
            
                      
            try{
                dao = new SalleStockageDAO();              
            } catch (Exception ex) {
                Logger.getLogger(SalleStockageController.class.getName()).log(Level.SEVERE, null, ex);
                json = "{success: false, errors:{reason:\'"+ex.getMessage()+"\'}}";
                return json;
            }
                      

            
             // Ajout nouveau id = 0
            if (idSalleStockage == 0){

                    SalleStockage salle = new SalleStockage(batiment, nom, code, description);
                    if (salle !=null){
                        try{
                            dao.create(salle);
                            json = "{success: true}";
                        } catch (Exception ex) {
                            Logger.getLogger(SalleStockageController.class.getName()).log(Level.SEVERE, null, ex);
                            json = "{success: false, errors:{reason:\'"+ex.getMessage()+"\'}}";
                        }
                    }                                  

          
            // UPDATE id > 0
            }else if (idSalleStockage > 0L){
                

                    SalleStockage salle = dao.findSalleStockage(idSalleStockage);
                 
                    if (salle !=null){
                        
                        salle.setNom(nom);
                        salle.setCode(code);
                        salle.setDescription(description);
                        
                        try{
                            dao.edit(salle);
                            json = "{success: true}"; 
                        } catch (NonexistentEntityException ex) {
                            Logger.getLogger(SalleStockageController.class.getName()).log(Level.SEVERE, null, ex);
                            json = "{success: false, errors:{reason:\'"+ex.getMessage()+"\'}}";  
                        } catch (Exception ex) {
                            Logger.getLogger(SalleStockageController.class.getName()).log(Level.SEVERE, null, ex);
                            json = "{success: false, errors:{reason:\'"+ex.getMessage()+"\'}}";  
                        }                            
                    }                       
                    
        
            }else{
                json = "{success: false, errors:{reason:\'Impossible de trouver "+idSalleStockage+"\'}}";
            }
                     
            
            return json;            
            //return "{success: false, errors:{reason:\'"+msg+"\'}};
        }        
        
        
        public String getPDF() {
            //----------------------------------------------------
            //Autorisations en lecture
            String resp = authorizedProfils.getReadAuthorizedJsonTicket();
            if (!"".equals(resp)){
                return resp;
            }
            //----------------------------------------------------   
            
            String json="{'success':false}"; 
            SalleStockage salleStockage = null;

            String idS = request.getParameter("id");
            Integer id;
            try{
                id = Integer.parseInt(idS);
            }catch(NumberFormatException e){
                id = 0;
            }


            try {
                if (id > 0){
                    SalleStockageDAO dao = new SalleStockageDAO();
                    if (dao!=null){
                        salleStockage = dao.findSalleStockage(id);
                    }
                }


            } catch (Exception ex) {
                Logger.getLogger(SalleStockageController.class.getName()).log(Level.SEVERE, null, ex);
            }    
            
            
            PdfDocumentFactory pdf = new PdfDocumentFactory("salleStockage" + id);           
            
            if (salleStockage!=null){
                
                
                pdf.addVerticalTable("Salle de Stockage", 24, salleStockage.getDataForVerticalTable(), 0);
                
                
                if (salleStockage.getBatiment().getLatitude()!=0 && salleStockage.getBatiment().getLongitude()!=0){
                
                    String urlImage = "http://maps.google.com/maps/api/staticmap?center="
                        + salleStockage.getBatiment().getLatitude()+"," + salleStockage.getBatiment().getLongitude()
                        + "&zoom=17&size=390x280&maptype=roadmap&markers=color:red|"
                        + salleStockage.getBatiment().getLatitude()+"," + salleStockage.getBatiment().getLongitude()
                        + "&sensor=false";                    
                
                    pdf.addImageFromUrl(urlImage, 390, 280, 2);  
                    
                    
                }
                
                List<String[]> las = AireStockage.getDataForHorizontalTable(salleStockage.getAires());
                pdf.addHorizontalTable("Armoires/locals", 15, las, 1, null);              
                
            }
            
            pdf.getDocument().close();     

            if (!"".equals(pdf.getFileName())){
                return "{'success':true,'fileName':'"+pdf.getFileName()+"'}";
            }else{
                return json;   
            }
            
        }        
        
}
