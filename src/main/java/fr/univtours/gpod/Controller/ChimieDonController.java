/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2012/01/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/

package fr.univtours.gpod.Controller;


import fr.univtours.gpod.HTML.HTML;
import fr.univtours.gpod.persistance.DAO.*;
import fr.univtours.gpod.persistance.DAO.exceptions.NonexistentEntityException;
import fr.univtours.gpod.persistance.DAO.exceptions.PreexistingEntityException;
import fr.univtours.gpod.persistance.entities.*;
import fr.univtours.gpod.utils.AuthorizedProfils;
import fr.univtours.gpod.utils.Mail;
import fr.univtours.gpod.utils.Utilities;
import org.joda.time.DateTime;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


public class ChimieDonController extends ActionController {


        public ChimieDonController(HttpServletRequest request, HttpServletResponse response, HttpSession session){
            super(request, response, session);
            authorizedProfils = new AuthorizedProfils(request, response, session);
            authorizedProfils.addReadAuthorisedProfile("AD","LA","HY","ME");
            authorizedProfils.addWriteAuthorisedProfile("AD","LA");               
        }
        
        public String getHtml(){
            //----------------------------------------------------
            //Autorisations         
            String resp = authorizedProfils.getAuthorizedHtmlTicket();
            if (!"".equals(resp)){
                return resp;
            }
            //----------------------------------------------------    
                response.setContentType("text/html;charset=iso-8859-1");
            
                HTML html = HTML.getInstance();
                html.getHeader().setTitle("Gestion des produits dangereux &gt; Gestion des substances");
                html.setExtjsHeader();
                html.getHeader().getCss().addLine("lib/multiselect/Multiselect.css");
                html.getHeader().getCss().addLine("lib/search/gridsearch.css");
                html.getHeader().getCss().addLine("lib/msg/msg.css");
                html.getHeader().getCss().addLine("lib/keyboard/virtualkeyboard.css");

                html.getHeader().getJs().addBlock("var B_nameUser = '"+Utilities.addSlashes(utilisateur.getNom())+"'");
                html.getHeader().getJs().addBlock("var B_lastCx = '"+Utilities.javaDateTIMEToString(utilisateur.getDateAvantDerniereConnexion())+"'"); 
                html.getHeader().getJs().addLine("version.js");
                html.getHeader().getJs().addLine("lib/keyboard/Ext.ux.VirtualKeyboard.js");
                html.getHeader().getJs().addLine("lib/keyboard/Ext.ux.plugins.VirtualKeyboard.js");
                html.getHeader().getJs().addLine("lib/encoder.js");
                html.getHeader().getJs().addLine("lib/printAction.js");
                html.getHeader().getJs().addLine("lib/msg/msg.js");                 
                html.getHeader().getJs().addLine("lib/checkbox/remotecheckboxgroup.js");
                html.getHeader().getJs().addLine("lib/multiselect/Multiselect.js");
                html.getHeader().getJs().addLine("lib/multiselect/ItemSelector.js");   
                html.getHeader().getJs().addLine("lib/search/gridSearch.js");
                html.getHeader().getJs().addLine("ui/search/AireStockage.js");
                html.getHeader().getJs().addLine("ui/search/SousStructure.js");
                html.getHeader().getJs().addLine("lib/excelButton.js");
                html.getHeader().getJs().addLine("ui/container.ChimieDon.js");
                html.getHeader().getJs().addLine("ui/menus/menu."+ session.getAttribute("profil") +".js");
                html.getHeader().getJs().addLine("ui/squelette.js");
                html.getBody().setBodyElement("<table align='center' class='contenttable' cellpadding='0' cellspacing='0' border='0'><tr><td id='centerPanel'></td></tr></table>");
                html.getBody().setBodyElement("<div id='iframediv'>");

                return html.getHTMLCode();
        }


        public String saveItem(){
            //----------------------------------------------------
            //Autorisations         
            String resp = authorizedProfils.getWriteAuthorizedJsonTicket();
            if (!"".equals(resp)){
                return resp;
            }
            //----------------------------------------------------                  
            String json="";
            DonDAO dao = new DonDAO();
            ArticleDAO articleDao = new ArticleDAO();
            Don don = null;
            
            this.response.setContentType("application/json");


            int idDon;
            boolean donOuiNon;
            boolean proposition;
            boolean accepteDon;



            String dateFinDonS = request.getParameter("dateFinDon");
            System.out.println("dateFinDons : " + dateFinDonS);
            Article article = null;
            SousStructure ss = null;
            AireStockage as = null;

            try{
                int idArticle = Integer.parseInt(request.getParameter("idArticle"));
                ArticleDAO daoArticle = new ArticleDAO();
                article = daoArticle.findArticle(idArticle);
            }catch(NumberFormatException e){
            }

            try{
                int idSousStructure = Integer.parseInt(request.getParameter("idSousStructure"));
                SousStructureDAO daoSS = new SousStructureDAO();
                ss = daoSS.findSousStructure(idSousStructure);
            }catch(NumberFormatException e){
            }

            try{
                int idAire = Integer.parseInt(request.getParameter("idAire"));
                AireStockageDAO daoAS = new AireStockageDAO();
                as = daoAS.findAireStockage(idAire);
            }catch(NumberFormatException e){
            }

            try{
                idDon = Integer.parseInt(request.getParameter("idDon"));
            }catch(NumberFormatException e){
                idDon = 0;
            }

            try{
                donOuiNon = Boolean.parseBoolean(request.getParameter("donOuiNon"));
            }catch(Exception e){
                donOuiNon = false;
            }

            try{
                accepteDon = Boolean.parseBoolean(request.getParameter("accepteDon"));
            }catch(Exception e){
                accepteDon = false;
            }

            try{
                proposition = Boolean.parseBoolean(request.getParameter("proposition"));
            }catch(Exception e){
                proposition = false;
            }

            DateTime now = new DateTime();
            DateTime dateFinDon = null;
            // le format de la date est différent, ici :  AAAA-MM-DDT00:00:00
            if (dateFinDonS != null && dateFinDonS.length() == 19){
                try{
                    int year = Integer.parseInt(dateFinDonS.substring(0,4));
                    int month = Integer.parseInt(dateFinDonS.substring(5, 7));
                    int day = Integer.parseInt(dateFinDonS.substring(8, 10));
                    dateFinDon = new DateTime(year, month, day,now.getHourOfDay(),now.getMinuteOfHour(),now.getSecondOfMinute(),now.getMillisOfSecond());
                    System.out.println("dateFinDon : " + dateFinDon.toString());
                }catch(NumberFormatException ex){
                    Logger.getLogger(ChimieDonController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }


            // Ajout nouveau don
            if (idDon == 0){
                don = new Don();

                don.setDateFinDon(dateFinDon.toDate());
                don.setArticle(article);
                don.setUtilisateurOffrant(utilisateur);


                if (don !=null){
                    try {
                        dao.create(don);
                        json = "{success: true}";
                    } catch (PreexistingEntityException ex) {
                        Logger.getLogger(ChimieDonController.class.getName()).log(Level.SEVERE, null, ex);
                        json = "{success: false, errors:{reason:\'La substance existe déjà\'}}";
                    } catch (Exception ex) {
                        Logger.getLogger(ChimieDonController.class.getName()).log(Level.SEVERE, null, ex);
                        json = "{success: false, errors:{reason:\'"+ex.getMessage()+"\'}}";
                    }
                }                                
          
            // UPDATE utilisateur idUtilisateur > 0
            }else{

                don = dao.findDon(idDon);

                // On supprime le don
                if (!donOuiNon && !proposition){
                    try {
                        dao.destroy(idDon);
                    } catch (NonexistentEntityException e) {
                        Logger.getLogger(ChimieDonController.class.getName()).log(Level.INFO, null, e.getMessage());
                        json = "{success: false, errors:{reason:\'"+e.getMessage()+"\'}}";
                    }
                }else{

                    try {
                        // on e
                        if (!proposition){
                            don.setDateFinDon(dateFinDon.toDate());
                            don.setArticle(article);
                            don.setUtilisateurOffrant(utilisateur);



                            if (don.getAireStockage()!=null && don.getSousStructure()!= null){
                                // si le don est accepté : on change le produit de propriétaire et d'aire
                                if (accepteDon){
                                    don.setAccepte(accepteDon);
                                    article.setSousStructure(don.getSousStructure());
                                    article.setAireStockage(don.getAireStockage());
                                    articleDao.edit(article);

                                    Mail mail = new Mail();
                                    mail.sendMail(don.getUtilisateurRecevant().getEmail(), "Don accepte",
                                            don.getUtilisateurOffrant().getPrenom() + " " + don.getUtilisateurOffrant().getNom()
                                                    + "(" + don.getUtilisateurOffrant().getEmail()+ ") a accepté le don de l'article " + don.getArticle().getProduitConditionnement().getProduit().getNom());

                                }else{
                                    don.setAccepte(accepteDon);
                                    don.setAireStockage(null);
                                    don.setSousStructure(null);

                                    Mail mail = new Mail();
                                    mail.sendMail(don.getUtilisateurRecevant().getEmail(), "Don refusé",
                                            don.getUtilisateurOffrant().getPrenom() + " " + don.getUtilisateurOffrant().getNom()
                                                    + "(" + don.getUtilisateurOffrant().getEmail()+ ") a refusé le don de l'article " + don.getArticle().getProduitConditionnement().getProduit().getNom());
                                }

                            }
                        }else{
                            don.setAireStockage(as);
                            don.setSousStructure(ss);
                            don.setUtilisateurRecevant(utilisateur);
                        }



                        dao.edit(don);
                        json = "{success: true}";
                    } catch (NonexistentEntityException ex) {
                        Logger.getLogger(ChimieDonController.class.getName()).log(Level.SEVERE, null, ex);
                        json = "{success: false, errors:{reason:\'"+ex.getMessage()+"\'}}";
                    } catch (Exception ex) {
                        Logger.getLogger(ChimieDonController.class.getName()).log(Level.SEVERE, null, ex);
                        json = "{success: false, errors:{reason:\'"+ex.getMessage()+"\'}}";
                    }
                }
            }
                     
            
            return json;            
            //return "{success: false, errors:{reason:\'"+msg+"\'}};
        }
   

        
        public List<Don> getListDon(){
            return this.getListDon(-1, 0, "", "");
        }

        public List<Don> getListDon(int maxResults, int firstResult, String searchValue, String searchColumns){



            List<Don> dons = null;
            int idArticle = 0;
            boolean accepte = false;
            try {

                idArticle = Integer.parseInt(request.getParameter("idArticle"));
                accepte = Boolean.parseBoolean(request.getParameter("accepte"));

            } catch (Exception ex) {
            }

            DonDAO dao = new DonDAO();

            dons = dao.findDonWithQuery(idArticle, accepte, maxResults, firstResult);



            return dons;
        }

        private int getDonCount(String searchValue, String searchColumns){
            int nbItems = 0;
            int idArticle = 0;
            boolean accepte = false;
            try {

                idArticle = Integer.parseInt(request.getParameter("idArticle"));
                Boolean.parseBoolean(request.getParameter("accepte"));
            } catch (Exception ex) {
            }


                DonDAO dao = new DonDAO();
                nbItems = dao.getDonWithQueryCount(idArticle, accepte);


            return nbItems;
        }



        
        public String getListItems(){
            
            String json = "";
            int nbItems = 0;
            int maxResults = -1;
            int firstResult = -1;

            try {

                try{
                    maxResults = Integer.parseInt(request.getParameter("limit"));
                    firstResult = Integer.parseInt(request.getParameter("start"));
                    if (maxResults == 0){
                        maxResults=10;
                    }
                }catch(NumberFormatException e){
                }
                String searchValue = request.getParameter("query");
                String searchColumns = request.getParameter("fields");

                List<Don> dons = this.getListDon(maxResults, firstResult, searchValue, searchColumns);
                nbItems = this.getDonCount(searchValue, searchColumns);

                this.response.setContentType("application/json");

                if (dons !=null && dons.size()>0){
                    json = "{'totalCount':'"+nbItems+"','data':[";
                    for (Don u : dons){


                        json += " {'idDon':'"+u.getIdDon()+"',";
                        json += "'idUtilisateur':'"+u.getUtilisateurOffrant().getIdUtilisateur()+"',";
                        json += "'emplacement':'"+u.getArticle().getAireStockage().getSalleStockage().getBatiment().getNom()+">"
                                +u.getArticle().getAireStockage().getSalleStockage().getNom()+">"
                                +u.getArticle().getAireStockage().getNom()+"',";
                        json += "'proprietaire':'"+u.getArticle().getSousStructure().getNom()+"',";
                        json += "'idArticle':'"+u.getArticle().getIdArticle()+"',";
                        json += "'articleNom':'"+u.getArticle().getProduitConditionnement().getProduit().getNom()+"',";
                        json += "'articleQte':'"+u.getArticle().getQuantite() * u.getArticle().getProduitConditionnement().getContenance()+" " + u.getArticle().getProduitConditionnement().getUniteMesure().getAbreviation() +"',";
                        json += "'dateFinDon':'"+Utilities.javaDateToString(u.getDateFinDon())+"',";
                        json += "'dateDon':'"+Utilities.javaDateToString(u.getDateDon())+"',";
                        json += "'accepte':'"+u.isAccepte()+"',";
                        if (u.getSousStructure()!= null){
                            json += "'idSousStructure':'"+u.getSousStructure().getIdSousStructure()+"',";
                            json += "'nomSousStructure':'"+u.getSousStructure().getNom()+"',";
                        }else{
                            json += "'idSousStructure':'',";
                            json += "'nomSousStructure':'',";
                        }
                        if (u.getAireStockage()!= null){
                            json += "'idAire':'"+u.getAireStockage().getIdAire()+"',";
                            json += "'nomAire':'"+u.getAireStockage().getNom()+"',";
                        }else{
                            json += "'idAire':'',";
                            json += "'idAire':'',";
                        }

                        json += "'write':'"+AuthorizedProfils.isWriteAllowed(utilisateur.getUtilisateurProfil().getCode(), "AD", "LA")+"'},";
                    }
                    json += "]}";
                }else{
                    json = "{'totalCount':'0','data':[]}";
                }
            }catch(ClassCastException e){
                json = "{'totalCount':'0','data':[]}";
            }

            return json;                   
        }


    // Methode utilisée pour les édition XLS et PDF
    // retourne l'utilisateur à partir de l'id. Si l'id n'est pas définit, on a demandé l'impression de la liste (voir méthode getXLSList()
    private Don getItemFromId(){
        
        Don don = null;

        try{
            int id = Integer.parseInt(request.getParameter("id"));

            if (id!=0){
                    try {
                        DonDAO dao = new DonDAO();
                        don = dao.findDon(id);
                    } catch (Exception ex) {
                        Logger.getLogger(ChimieDonController.class.getName()).log(Level.SEVERE, null, ex);
                    }
            }
        }catch (ClassCastException ex){
            Logger.getLogger(ChimieDonController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return don;
        
    }       
        
   
        

        
        
        
}
