/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2012/01/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/

package fr.univtours.gpod.Controller;

import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import com.itextpdf.text.Font;
import fr.univtours.gpod.HTML.HTML;
import fr.univtours.gpod.persistance.DAO.UtilisateurDAO;
import fr.univtours.gpod.persistance.entities.Utilisateur;
import fr.univtours.gpod.utils.AuthorizedProfils;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 *
 * @author geoffroy.vibrac
 */
public class ActionController {

    protected HttpSession session;
    protected HttpServletRequest request;
    protected HttpServletResponse response;
    protected Utilisateur utilisateur;
    protected AuthorizedProfils authorizedProfils;
         
    public ActionController(HttpServletRequest request, HttpServletResponse response, HttpSession session){
        this.request = request;
        this.response = response;
        this.session = session;
        this.setUtilisateur();
    } 
        
    private void setUtilisateur(){
            Long idutilisateur;
            try{
                idutilisateur = (Long)session.getAttribute("idUtilisateur");
                if (idutilisateur > 0 ){
                    try {
                        UtilisateurDAO daob = new UtilisateurDAO();
                        utilisateur = daob.findUtilisateur(idutilisateur);

                    } catch (Exception ex) {
                        Logger.getLogger(ActionController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }catch(NumberFormatException ex){
                Logger.getLogger(ActionController.class.getName()).log(Level.SEVERE, null, ex);
            }
    }

    public Utilisateur getUtilisateur(){
        return utilisateur;
    }
    
    public String getHtml(){
        //----------------------------------------------------
        //Autorisations         
        String resp = authorizedProfils.getAuthorizedHtmlTicket();
        if (!"".equals(resp)){
            return resp;
        }
        //----------------------------------------------------    
                    
        response.setContentType("text/html;charset=iso-8859-1");

        HTML html = HTML.getInstance();
        html.getHeader().setTitle("Gestion des produits dangereux");
        html.getBody().setBodyElement("<h1 style=\"color: black; text-align: center;\">Accès impossible</h1>");

        return html.getHTMLCode();
    }
      
    
    public String deleteItem(){
            //----------------------------------------------------
            //Autorisations en ecrite
            String resp = authorizedProfils.getWriteAuthorizedJsonTicket();
            if (!"".equals(resp)){
                return resp;
            }
            //----------------------------------------------------    
   
            this.response.setContentType("application/json");
            
            String json = "";
            String msg = "";
            int idItem = 0;
            boolean success = true;     
            String DAOPath = "fr.univtours.gpod.persistance.DAO."; 
            String controller = request.getParameter("controller");
              
            try{
                idItem = Integer.parseInt(request.getParameter("id"));
            }catch(NumberFormatException ex){
                success = false;
            }
                                    
            if (idItem>= 0){
                    try{
                                             
                        
                        // Construction de la Classe avec reflexivité
                        Class c = Class.forName(DAOPath + controller + "DAO");
                        // récupération du constructeur Controller(HttpServletRequest, HttpServletResponse, HttpSession)
                        Constructor cons = c.getConstructor();
                        // Instanciation du constructeur
                        Object o = cons.newInstance();                      

                        // récupration de la methode
                        Method m = c.getMethod("destroy", Integer.class);
                        String result = (String) m.invoke(o, idItem);                        
                    } catch (ClassNotFoundException ex) {
                            Logger.getLogger(ActionController.class.getName()).log(Level.SEVERE, null, ex);
                            msg = "Impossible de supprimer cette ligne, Erreur logiciel !!";
                            success = false;              
                    } catch (Exception ex) {
                            // je gère les erreurs de suppression
                            //1er CAS, c'est un MySQLIntegrityConstraintViolationException
                            String message = "";
                            try{
                                message = ex.getCause().getCause().getCause().getMessage();
                                String s = "Cannot delete or update a parent row: a foreign key constraint fails (`gpod`.`";
                                message = message.substring(s.length());
                                int apo = message.indexOf("`");
                                message = message.substring(0, apo);                                
                            }catch(NullPointerException e){  
                                // 2nd cas, autre message d'erreur pour InvocationTargetException
                                try{
                                    message = ex.getCause().getMessage();
                                    int deb = message.indexOf("since the ")+10;
                                    
                                    message = message.substring(deb);
                                    int apo = message.indexOf(" ");
                                    message = message.substring(0, apo); 
                                }catch(NullPointerException e2){                                      
                                }
                            }
                            
                            
                            
                            if (!"".equals(message)){
                                msg = "Impossible de supprimer cette ligne, elle est utilisée par un objet \""+message+"\"";
                            }else{
                                msg = "Impossible de supprimer cette ligne, Erreur logiciel !!";                                
                                Logger.getLogger(ActionController.class.getName()).log(Level.SEVERE, null, ex);
                            }
                            success = false;
                    }                 
                    
               
            }else{
                success = false;
                msg = idItem + " inconnu";
            }
            
            
            if (success){
                json = "{success: true}";            
            }else{
                this.response.setStatus(424);
                json = "{success: false, errors:{reason:\'"+msg+"\'}}";
            }
            return json;
        }   
    
    
        public String getRight(){
            String json="{success: false}";
                                       
            this.response.setContentType("application/json");
            
            if (authorizedProfils.isWriteAuthorisedProfile()){
                json="{success: true}";
            }else{
                this.response.setStatus(424);
                json="{success: false}";
            }
            
            
            return json;
            
        }    
    
        
        
        public class PdfDocumentFactory{
        
            Document document = null;
            String fileName = "";
            public static final String RESOURCE = "/public/ui/images/picto/%s.jpg";
             
            
            public PdfDocumentFactory(Document document){
                this.document = document;
            }
            
            public PdfDocumentFactory(String shortFileName){
                try {
                    DateTimeFormatter FILE_FORMAT = DateTimeFormat.forPattern("yyyyMMddHmsS");
                    LocalDateTime now = new LocalDateTime();
                    String dateStr = now.toString(FILE_FORMAT);
                    this.fileName = shortFileName + "_"+ dateStr + ".pdf";

                    this.document = new Document(PageSize.A4,50,20,20,50);
                    // step 2
                    PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(this.fileName));
                    // step 3
                    document.open();
                    // step 4
                    PdfContentByte cb = writer.getDirectContent();
                } catch (DocumentException ex) {
                    Logger.getLogger(ActionController.class.getName()).log(Level.SEVERE, null, ex);
                } catch (FileNotFoundException ex) {
                    Logger.getLogger(ActionController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }            
            
            
            // addVerticalTable
            // ajout d'une table vertical : Table avec 2 colonne, labels à gauche et valeurs à droite
            public void addVerticalTable(String title, int titleSize, Map<String, String> tableContent, int borderWidth) {
                try {
                    BaseFont bf = BaseFont.createFont(session.getAttribute("absolutePath")+"/public/lib/keyboard/arial.ttf", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
                    Font titleF = new Font(bf,titleSize, Font.BOLD,new BaseColor(0, 0, 0));         
                    Font labelF = new Font(bf, 11, Font.NORMAL, new BaseColor(0, 0, 0));
                    Font valueF = new Font(bf, 11, Font.NORMAL, new BaseColor(0, 0, 0));

                    PdfPTable table = new PdfPTable(2);
                    table.getDefaultCell().setPadding(5);
                    table.getDefaultCell().setBorderWidth(borderWidth);
                    table.setWidthPercentage(100);
                    table.setWidths(new int[]{1, 3});                
                    table.setSpacingAfter(10);

                    PdfPCell cell = new PdfPCell(new Phrase(1, title, titleF));
                    cell.setPadding(5);
                    cell.setColspan(2);
                    cell.setBorderWidth(borderWidth);
                    table.addCell(cell);
                    table.setHeaderRows(1);                

                    table.addCell(" ");
                    table.addCell(" ");

                    for(Entry<String, String> entry : tableContent.entrySet()) {
                        if (entry.getValue()!=null && !"".equals(entry.getValue())){
                            table.addCell(new Phrase(2, entry.getKey(), labelF));
                            table.addCell(new Phrase(2, entry.getValue(), valueF));                           
                        }
                    }




                    document.add(table);
                } catch (IOException | DocumentException ex) {
                    Logger.getLogger(ActionController.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                
                
            }
            
            public void addHorizontalTableImg(String title, int titleSize, List<String[]> tableContent, int borderWidth, int[] columnWidth) {
                
                Font titleF = new Font(FontFamily.HELVETICA, titleSize, Font.BOLD, new BaseColor(0, 0, 0));  
                
                int nbColumns = 5; 
                // la premiere chaine de la liste représente le nom de la colonne dont on ne se sert pas.
                int nbImg = tableContent.size()-1;
                
                if (nbColumns > 0){
                    try {
                        Image[] img = new Image[nbImg];
                  
                        //System.out.println(session.getAttribute("absolutePath") + RESOURCE);
    
                        // on prend démarre à 1 car on ne prend pas la premiere chaine de la liste
                        for (int i=1;i<tableContent.size();i++){                     
                            img[i-1] = Image.getInstance(String.format(session.getAttribute("absolutePath") + RESOURCE, tableContent.get(i)[0]));
                        }



                        PdfPTable table = new PdfPTable(nbColumns);
                        table.getDefaultCell().setPadding(5);
                        table.getDefaultCell().setBorderWidth(0);
                        table.setWidthPercentage(100);             
                        table.setSpacingAfter(10);
                        table.setHeaderRows(1); 
                        if (columnWidth!=null && columnWidth.length == nbColumns){
                            table.setWidths(columnWidth);                          
                        }

                        PdfPCell cell = new PdfPCell(new Phrase(1, title, titleF));
                        cell.setColspan(nbColumns);
                        cell.setPadding(5);
                        cell.setBorderWidth(0);
                        table.addCell(cell);              


                        for (int i=0;i<nbImg;i++){
                            img[i].setWidthPercentage(75);
                            cell = new PdfPCell();
                            cell.setBorderWidth(0);
                            cell.addElement(img[i]);
                            table.addCell(cell);  

                        }
                        table.completeRow();


                        document.add(table);
                    } catch (BadElementException ex) {
                        Logger.getLogger(ActionController.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (MalformedURLException ex) {
                        Logger.getLogger(ActionController.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (FileNotFoundException ex) {
                        Logger.getLogger(ActionController.class.getName()).log(Level.SEVERE, null, ex);            
                    } catch (IOException ex) {
                        Logger.getLogger(ActionController.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (DocumentException ex) {
                        Logger.getLogger(ActionController.class.getName()).log(Level.SEVERE, null, ex);

                    }
                
                }
            }
            
            
            public void addImageFromUrl(String imgUrl, float width, float height, int borderWidth){
                
                try {
                    Image image = Image.getInstance(new URL(imgUrl));
                    image.scalePercent(100);
                    image.setBorder(borderWidth);
                    document.add(image);
                } catch (BadElementException ex) {
                    Logger.getLogger(ActionController.class.getName()).log(Level.SEVERE, null, ex);    
                } catch (DocumentException ex) {
                    Logger.getLogger(ActionController.class.getName()).log(Level.SEVERE, null, ex);
                } catch (MalformedURLException ex) {
                    Logger.getLogger(ActionController.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(ActionController.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                
            }
            
            // addHorizontalTable
            // ajout d'une table horizontale avec entête de colonne et valeur en dessous
            // le nombre de colonne dépend de la table du tableau de chaine
            public void addHorizontalTable(String title, int titleSize, List<String[]> tableContent, int borderWidth, int[] columnWidth){
            try {
                BaseFont bf = BaseFont.createFont(session.getAttribute("absolutePath")+"/public/lib/keyboard/arial.ttf", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
                Font titleF = new Font(bf,titleSize, Font.BOLD,new BaseColor(0, 0, 0));         
                Font labelF = new Font(bf, 11, Font.NORMAL, new BaseColor(0, 0, 0));
                Font valueF = new Font(bf, 11, Font.NORMAL, new BaseColor(0, 0, 0));            
            
                if (tableContent!=null){
                    try {
                        String[] headers = tableContent.get(0);
                        int nbColumns = headers.length;

                        PdfPTable table = new PdfPTable(nbColumns);
                        table.getDefaultCell().setPadding(5);
                        table.getDefaultCell().setBorderWidth(borderWidth);
                        table.setWidthPercentage(100);             
                        table.setSpacingAfter(10);
                        table.setHeaderRows(1); 
                        if (columnWidth!=null && columnWidth.length == nbColumns){
                            table.setWidths(columnWidth);                          
                        }

                        PdfPCell cell = new PdfPCell(new Phrase(1, title, titleF));
                        cell.setColspan(nbColumns);
                        cell.setPadding(5);
                        cell.setBorderWidth(borderWidth);
                        table.addCell(cell);



                        for (String s : headers){
                            table.addCell(new Phrase(2, s, labelF));
                        }


                        for (int i=1; i<tableContent.size();i++){
                            for (String s : tableContent.get(i)){
                                table.addCell(new Phrase(2, s, valueF));
                            }                        
                        }


                        document.add(table);
                    } catch (DocumentException ex) {
                        Logger.getLogger(ActionController.class.getName()).log(Level.SEVERE, null, ex);
                    }



                }
            } catch (DocumentException ex) {
                Logger.getLogger(ActionController.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(ActionController.class.getName()).log(Level.SEVERE, null, ex);
            }
            }

            public Document getDocument() {
                return document;
            }

            public String getFileName() {
                return fileName;
            }
            
            
            
            
        }
             
        
    
}
