/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2012/01/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/


package fr.univtours.gpod.utils;

import fr.univtours.gpod.persistance.DAO.PropertiesDAO;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.*;

public class LdapSearch {


  public void addGroupe (String userid){

      userid  ="vibrac";

      Hashtable env = new Hashtable();
      String sp = "com.sun.jndi.ldap.LdapCtxFactory";
      env.put(Context.INITIAL_CONTEXT_FACTORY, sp);
      String ldapUrl = "ldap://ldap.univ-tours.fr:389/dc=univ-tours,dc=fr";
      env.put(Context.PROVIDER_URL, ldapUrl);
      String baseDn = "ou=people,dc=univ-tours,dc=fr";

      try {
          DirContext ctx = new InitialDirContext(env);
          Attributes attrs = new BasicAttributes();

          BasicAttribute UFRGroupe = new BasicAttribute("UFRGroupe");
          UFRGroupe.add("cn=scanner-xerox,ou=groups,dc=univ-tours,dc=fr");


          attrs.put("UFRGroupe", UFRGroupe);

          ctx.bind("uid=" + userid + "," + baseDn, null, attrs);


      } catch (NamingException e) {
          e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
      }


  }

  // TODO : Mettre les valeurs des variables en BDD
  public List<LdapUser> getListUsers(String name, String firstname, String userid, String type)  {

        PropertiesDAO dao = new PropertiesDAO();
        List<fr.univtours.gpod.persistance.entities.Properties> propertiesList = dao.findEmailPropertiesEntities();

            String filter = "";
            String subFilter="";
            if (!"".equals(name)){
                subFilter += "(sn=" + name+ "*)";
            }
            if (!"".equals(firstname)){
                subFilter += "(givenName=" + firstname+ "*)";
            }
            if (!"".equals(userid)){
                subFilter += "(uid=" + userid+ "*)";
            }

            if ("2".equals(type)){
                filter = "(&"+subFilter+"(eduPersonAffiliation=student))";
            }else{
                filter = "(&"+subFilter+"(!(eduPersonAffiliation=student)))";
            }


            Properties env = new Properties();

            ArrayList<LdapUser> listUsers= new ArrayList<LdapUser>();
            String sp = "com.sun.jndi.ldap.LdapCtxFactory";
            env.put(Context.INITIAL_CONTEXT_FACTORY, sp);
            String ldapUrl = "ldap://"+propertiesList.get(0).getLdapAddress()+":"+propertiesList.get(0).getLdapPort()+"/dc=univ-tours,dc=fr";
            env.put(Context.PROVIDER_URL, ldapUrl);

            env.setProperty(Context.SECURITY_PRINCIPAL, propertiesList.get(0).getLdapUser());
            env.setProperty(Context.SECURITY_CREDENTIALS, propertiesList.get(0).getLdapPassword());


            try {
                DirContext dctx = new InitialDirContext(env);
                String base = "ou=People";
                SearchControls sc = new SearchControls();
                String[] attributeFilter = {"sn", "givenName", "mail", "uid", "UFRComposante", "UFRcodeComposante"};
                sc.setReturningAttributes(attributeFilter);
                sc.setSearchScope(SearchControls.SUBTREE_SCOPE);
                NamingEnumeration results = dctx.search(base, filter, sc);
                while (results.hasMore()) {
                    SearchResult sr = (SearchResult) results.next();
                    Attributes attrs = sr.getAttributes();

                    try{
                        System.out.println((String)attrs.get("sn").get());
                        listUsers.add(new LdapUser(getAttributeValue(attrs, "sn"), getAttributeValue(attrs, "givenName"),
                                getAttributeValue(attrs, "mail"), getAttributeValue(attrs, "uid"),
                                getAttributeValue(attrs, "UFRComposante"), getAttributeValue(attrs, "UFRcodeComposante")));
                    } catch (java.lang.NullPointerException ex) {
                    }

                }
                dctx.close();
            } catch (NamingException ex) {
                Logger.getLogger(LdapSearch.class.getName()).log(Level.SEVERE, null, ex);
            }
            return listUsers;
  }

  private String getAttributeValue(Attributes attrs, String attributeName) throws NamingException{
        String attValue = "";
        try {
            attValue = (String)attrs.get(attributeName).get();
        } catch (NullPointerException ex) {
        }
        return attValue;
  }



  public class LdapUser {

       public LdapUser(String name, String firsname, String mail, String uid, String composante, String codeComposante) {
            this.name = name;
            this.firsname = firsname;
            this.mail = mail;
            this.uid = uid;
            this.composante = composante;
            this.codeComposante = codeComposante;
        }



      private String name;
      private String firsname;
      private String mail;
      private String uid;
      private String composante;
      private String codeComposante;

        public String getFirsname() {
            return firsname;
        }

        public String getMail() {
            return mail;
        }

        public String getName() {
            return name;
        }

        public String getUid() {
            return uid;
        }

        public String getCodeComposante() {
            return codeComposante;
        }

        public String getComposante() {
            return composante;
        }




  }


}