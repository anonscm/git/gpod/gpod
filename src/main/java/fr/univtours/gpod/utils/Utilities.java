/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2012/01/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/


package fr.univtours.gpod.utils;

import fr.univtours.gpod.Controller.ActionController;
import fr.univtours.gpod.Controller.CommandeController;
import fr.univtours.gpod.Controller.StockListeController;
import fr.univtours.gpod.HTML.HTML;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.MessagingException;
import javax.mail.internet.MimeUtility;
import javax.servlet.ServletContext;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 *
 * @author geoffroy.vibrac
 */
public class Utilities {

    static DateTimeFormatter DATE_FORMAT = DateTimeFormat.forPattern("dd/MM/yyyy");
    static DateTimeFormatter DATETIME_FORMAT = DateTimeFormat.forPattern("dd/MM/yyyy à HH:mm:ss");


    public static boolean isAuth(HttpSession session){
        if (session == null)
            return false;

        if (session.getAttribute("auth")==null)
            return false;

        return (boolean)session.getAttribute("auth");

    }


    public static String getCompleteUrl(HttpServletRequest request){
        String url = "";
        if (request.isSecure()){
            url += "https://";
        }else{
            url +="http://";
        }
        url +=request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
        
        
        return url;
    }
    
    
    public static String md5(String password) {
        MessageDigest md = null;
        StringBuffer hexString = null;
        try {
            md = MessageDigest.getInstance("MD5");
            md.update(password.getBytes()); 
            byte[] md5 = md.digest(); 
            hexString = new StringBuffer();
            for (int i=0;i<md5.length;i++) {
                int value = md5[i] & 0xFF;
		hexString.append(Integer.toHexString(value));
            }
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(Utilities.class.getName()).log(Level.SEVERE, null, ex);
        }

	return hexString.toString();
   }
   
   public static String changeClikeyEncodageToHex(String clickeyUID) throws MessagingException, IOException{
        clickeyUID = clickeyUID.replace("-", "+");
        
        byte[] b = clickeyUID.getBytes();
        ByteArrayInputStream bais = new ByteArrayInputStream(b);
        InputStream b64is = MimeUtility.decode(bais, "base64");
        byte[] tmp = new byte[b.length];
        int n = b64is.read(tmp);
        byte[] res = new byte[n];
        System.arraycopy(tmp, 0, res, 0, n);
        
        return byteArrayToHexString(res);
   }
    
   
    public static String byteArrayToHexString(byte in[]) {
        byte ch = 0x00;
        int i = 0;
        if (in == null || in.length <= 0)
            return null;

        String pseudo[] = {"0", "1", "2","3", "4", "5", "6", "7", "8","9", "A", "B", "C", "D", "E","F"};
        StringBuffer out = new StringBuffer(in.length * 2);
        while (i < in.length) {
            ch = (byte) (in[i] & 0xF0);
            ch = (byte) (ch >>> 4);
            ch = (byte) (ch & 0x0F);
            out.append(pseudo[ (int) ch]);
            ch = (byte) (in[i] & 0x0F);
            out.append(pseudo[ (int) ch]);
            i++;
        }
        String rslt = new String(out);
        return rslt;
    }     
   
    
   public static boolean isLogged(HttpSession session, String codeProfilUtilisateur) {
        if (session == null || (Long)session.getAttribute("idUtilisateur")==0 || !codeProfilUtilisateur.equals((String)session.getAttribute("profil"))){
            return false;
            //response.sendRedirect("index.html");
        }
        return true;
    }

    
    // Cette methode fonctionne avec un Set car certaine pages peuvent être autorisée pour plusieurs profils
    public static boolean isLogged(HttpSession session, Set<String> codesProfilUtilisateur) {
        if (session == null || (Long)session.getAttribute("idUtilisateur")==0 || !codesProfilUtilisateur.contains((String)session.getAttribute("profil"))){
            return false;
            //response.sendRedirect("index.html");
        }
        return true;
    }

    public static String addSlashes(String string){
        if (string != null){
            string = string.replace("\n\r", "\n");
            string = string.replace("\r\n", "\n");
            string = string.replace("\r", "\n");
            string = string.replace("\n", "\\n");
            return string.replace("'", "\\\'");
        }else{
            return "";
        }
    }

    public static String addQuote(String string){
        if (string != null){
            return string.replace("'", "''");
        }else{
            return "";
        }
    }


    public static String nullToEmpty(String string){
        if (string == null){
            return "";
        }else{
            return string;
        }
    }


    public static String listToStringWithComma(List<String> list){
        String stringWithComma = "";
        if (list.size() > 0){
            for(String s : list){
                stringWithComma += ","+s;
            }
            stringWithComma = stringWithComma.substring(1);
        }
        return stringWithComma;
    }

    public static String listIntToStringWithComma(List<Integer> list){
        String stringWithComma = "";
        if (list.size() > 0){
            for(Integer s : list){
                stringWithComma += ","+s;
            }
            stringWithComma = stringWithComma.substring(1);
        }
        return stringWithComma;
    }

    public static String javaDateToString(Date date){
        String returnValue = "";
        DateTime dateFormatee = null;
        if (date!=null){
            dateFormatee = new DateTime(date);
            if (dateFormatee != null){
                returnValue = dateFormatee.toString(DATE_FORMAT);
            }
        }
        return returnValue;

    }

    public static String javaDateTIMEToString(Date date){
        String returnValue = "";
        DateTime dateFormatee = null;
        if (date!=null){
            dateFormatee = new DateTime(date);
            if (dateFormatee != null){
                returnValue = dateFormatee.toString(DATETIME_FORMAT);
            }
        }
        return returnValue;

    }    

    //sert à completer la chaine avec des zero à gauche
    // exemple : je veux que la chaine "12" devienne "0012" de longueur 4
    // 4-2 = 2 => je dois rajouter 2 zero
    public static String completeLeftZero(String stringToComplete, int stringLength){
        String returnValue = stringToComplete;
        stringLength = stringLength - stringToComplete.length();
        for (int i=0; i<stringLength; i++){
            returnValue = "0" + returnValue;
        }
        return returnValue;
    }
    
    //sert à completer la chaine avec des zero à gauche
    // exemple : je veux que la chaine "12" devienne "0012" de longueur 4
    // 4-2 = 2 => je dois rajouter 2 zero
    public static String completeRightZero(String stringToComplete, int stringLength){
        String returnValue = stringToComplete;
        stringLength = stringLength - stringToComplete.length();
        for (int i=0; i<stringLength; i++){
            returnValue = returnValue + "0";
        }
        return returnValue;
    }    
    

    public static ActionController getDefaultProfilActionController(HttpServletRequest request, HttpServletResponse response, HttpSession session){
        ActionController defaultController = null;
        
        if ("SE".equals(session.getAttribute("profil"))){
            defaultController = new CommandeController(request, response, session);
        }else{
            defaultController = new StockListeController(request, response, session);
        }
        
        return defaultController;               
    }
    
  
    
    public static String booleanToOuiNon(boolean value){
        if (value){
            return "oui";
        }else{
            return "non";
        }
    }
    
  /*  public static void InibeAcces(HttpServletResponse response) throws IOException{
        PrintWriter out = response.getWriter();
        HTML html = HTML.getInstance();
        html.getHeader().setTitle("Gestion des produits dangereux");
        html.getBody().setBodyElement("Accès interdit");
        out.println(html.getHTMLCode());
        out.close();
    }*/
    
    
    
}
