/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2012/01/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/


package fr.univtours.gpod.utils;


import fr.univtours.gpod.Controller.CommandeController;
import fr.univtours.gpod.persistance.DAO.PropertiesDAO;
import fr.univtours.gpod.persistance.entities.Properties;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.*;
import javax.mail.internet.*;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;

/**
 *
 * @author geoffroy.vibrac
 */
public class Mail {
    
    String smtp;
    int port;
    String fromEmail;
    List<InternetAddress> cc = new ArrayList<>();
    
    public Mail(){
        PropertiesDAO dao = new PropertiesDAO();
        List<Properties> propertiesList = dao.findEmailPropertiesEntities();
        if (propertiesList.size() > 0){
            this.smtp = propertiesList.get(0).getSmtp();
            this.port = propertiesList.get(0).getPort();
            if (this.port==0){
                this.port=25;
            }            
            this.fromEmail = propertiesList.get(0).getFromEmail();
        }       
    }

    public void addSenderToCc(){
         this.addCc(fromEmail);
    }
    
    
    public void addCc(String email){
        try {
            cc.add(new InternetAddress(email));
        } catch (AddressException ex) {
            Logger.getLogger(Mail.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
            
    
    
    
    public String sendMail(String adressTO, String subject, String message){
        
        String sendMail = "{success: true}";
        
        if (!"".equals(this.smtp) && !"".equals(this.fromEmail)){
        
            java.util.Properties props = new java.util.Properties();
            props.put("mail.smtp.host", this.smtp);
            props.put("mail.smtp.port", this.port);            
            props.put("mail.from", this.fromEmail);  
            Session session =  Session.getInstance(props, null);

            try {
                MimeMessage msg = new MimeMessage(session);
                msg.setFrom();
                msg.setRecipients(Message.RecipientType.TO, adressTO);
                msg.setSubject(subject);
                msg.setSentDate(new Date());
                msg.setContent(message, "text/html");
                msg.addRecipient(Message.RecipientType.BCC, new InternetAddress(this.fromEmail));
                for (InternetAddress ia : this.cc){
                    msg.addRecipient(Message.RecipientType.CC, ia);
                }
                
                Transport.send(msg);
            } catch (MessagingException ex) {
                Logger.getLogger(CommandeController.class.getName()).log(Level.SEVERE, null, ex);
                sendMail = "{success: false, errors:{reason:\'send failed, exception: " + ex + "\'}}";
            }        
        }else{
            sendMail = "{success: false, errors:{reason:\'Problème : Aucun SMTP et/ou email\'}}";
        }
        return sendMail;
        
    }

    public String sendMail(String adressTO, String subject, String message, String pathFile,String fileName){

        String sendMail = "{success: true}";

        if (!"".equals(this.smtp) && !"".equals(this.fromEmail)){

            java.util.Properties props = new java.util.Properties();
            props.put("mail.smtp.host", this.smtp);
            props.put("mail.smtp.port", this.port);
            props.put("mail.from", this.fromEmail);
            Session session =  Session.getInstance(props, null);

            try {
                MimeMessage msg = new MimeMessage(session);
                msg.setFrom();
                msg.setRecipients(Message.RecipientType.TO, adressTO);


                msg.setSubject(subject);
                msg.setSentDate(new Date());
                for (InternetAddress ia : this.cc){
                    msg.addRecipient(Message.RecipientType.CC, ia);
                }

                Multipart multipart = new MimeMultipart();


                BodyPart messageBodyPart = new MimeBodyPart();
                messageBodyPart.setText(message);
                multipart.addBodyPart(messageBodyPart);

                // creation et ajout de la piece jointe
                messageBodyPart = new MimeBodyPart();
                DataSource source = new FileDataSource(pathFile+"/"+fileName);
                messageBodyPart.setDataHandler(new DataHandler(source));
                messageBodyPart.setFileName(fileName);
                multipart.addBodyPart(messageBodyPart);


                // ajout des éléments au mail
                msg.setContent(multipart);

                Transport.send(msg);

            } catch (MessagingException ex) {
                Logger.getLogger(Mail.class.getName()).log(Level.SEVERE, null, ex);
                sendMail = "{success: false, errors:{reason:\'send failed, exception: " + ex + "\'}}";
            }
        }else{
            sendMail = "{success: false, errors:{reason:\'Problème : Aucun SMTP et/ou email\'}}";
        }
        return sendMail;

    }
    
}
