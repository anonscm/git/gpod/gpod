/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2012/01/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/


package fr.univtours.gpod.utils;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.*;
import fr.univtours.gpod.persistance.DAO.PictogrammeDAO;
import fr.univtours.gpod.persistance.entities.Pictogramme;
import fr.univtours.gpod.persistance.entities.Substance;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author geoffroy.vibrac
 */
public class ESISSearch {
    
    String CAS;
    
    public ESISSearch(String CAS){
        this.CAS=CAS;
    }
    
    public Substance search() {
        Substance sub = new Substance();
        
        try {
                     
            String ec = "";
            String casHtml = "";
            String name = "";
            String molecularFormula = "";
            String description = "";
            ArrayList<Pictogramme> pictoList  = new ArrayList();
            PictogrammeDAO daoPic = new PictogrammeDAO(); 
            
            final WebClient webClient = new WebClient(BrowserVersion.FIREFOX_3_6);
            webClient.addRequestHeader("Accept-Language", "fr");
            
            final HtmlPage page = webClient.getPage("http://esis.jrc.ec.europa.eu/");
            
            final HtmlPage formFrame = (HtmlPage) page.getFrameByName("esispgm").getEnclosedPage();    
            
            final HtmlPage formPage = (HtmlPage) formFrame.getFrameByName("esisquestion").getEnclosedPage();  
            
            final HtmlForm form = formPage.getFormByName("ESISISForm");
            
            
            final HtmlSubmitInput button = form.getInputByValue("SEARCH");
            final HtmlTextInput textField = form.getInputByName("ENTREE");
            final HtmlSelect selectField = form.getSelectByName("GENRE");
            HtmlOption option = selectField.getOptionByValue("CASNO");
            
            selectField.setSelectedAttribute(option, true);
            textField.setValueAttribute(this.CAS);

            // Now submit the form by clicking the button and get back the second page.
            final HtmlPage page2 = button.click();    
            
            HtmlElement body = page2.getBody();
            ArrayList<String> dataProduct = new ArrayList();
            HtmlElement tabcas =  body.getElementsByTagName("table").get(1);
            if (tabcas != null) {
                tabcas = tabcas.getChildElements().iterator().next(); // tbody
                Iterator<HtmlElement> i = tabcas.getChildElements().iterator();
                tabcas = i.next();
                tabcas = i.next();
                tabcas = i.next();
                tabcas = tabcas.getChildElements().iterator().next(); // td                
                tabcas = tabcas.getChildElements().iterator().next(); // table
                tabcas = tabcas.getChildElements().iterator().next(); // tbody
                Iterator<HtmlElement> trEC = tabcas.getChildElements().iterator();
                int index=0;
                    while (trEC.hasNext()){
                        HtmlElement elementTR = trEC.next();
                        Iterator<HtmlElement> elementTD = elementTR.getChildElements().iterator();
                        if (index>0){
                            elementTD.next();
                            elementTD.next();
                            dataProduct.add(elementTD.next().asText());
                        }
                        index++;
                    }
                   
                    try{
                        ec = dataProduct.get(0);
                        casHtml = dataProduct.get(1);
                        name = dataProduct.get(3);
                        molecularFormula = dataProduct.get(4);
                        description = dataProduct.get(5);
                        if ("Not available".equals(description)){
                            description = "";
                        }
                    }catch(Exception e){

                    }
                        
                        
                  //DomNodeList<DomNode> Linklist = body.querySelectorAll("[src~=\"/image/logoF.gif\"]");
                    DomNodeList<DomNode> Linklist = body.querySelectorAll("img");
                    for (DomNode dn : Linklist){
                         HtmlImage img = (HtmlImage)dn;
                         System.out.println(img.getAttribute("src"));
                         if (img.getAttribute("src").substring(0, 11).equals("/image/logo")){
                                Pictogramme p = daoPic.findPictoFromCode(img.getAttribute("src").substring(11, 12));
                                if (p!=null){
                                    pictoList.add(p);
                                }                             
                         }
                    }            
            
//            
//            
//            
//            
//            
//            
//            
//            
//            
//            
//            
//            
//            
//            
//            
//            
//            
//            Connection connection = Jsoup.connect(url);
//            connection.header("Accept-Language", "fr");
//            
//            Document doc = connection.get();
////            String ec = "";
////            String casHtml = "";
////            String name = "";
////            String molecularFormula = "";
////            String description = "";
////            ArrayList<Pictogramme> pictoList  = new ArrayList();
////            ArrayList<MentionDanger> riskList  = new ArrayList();
////            PictogrammeDAO daoPic = new PictogrammeDAO(); 
//            MentionDangerDAO daoMD = new MentionDangerDAO();     
//            
//            if (doc != null){
//                Element tabcas =  doc.select("table").get(1);
//                if (tabcas != null) {
//                    tabcas = tabcas.child(0); // tbody
//                    if (tabcas != null) {
//                        tabcas = tabcas.child(0); // tr
//                        if (tabcas != null) {
//                            tabcas = tabcas.nextElementSibling(); // tr
//                            if (tabcas != null) {
//                                tabcas = tabcas.nextElementSibling(); // tr
//                                if (tabcas != null) {
//                                    tabcas = tabcas.child(0); // td                                
//                                    if (tabcas != null) {
//                                        tabcas = tabcas.child(0); // table                           
//                                        if (tabcas != null) {
//                                            if (tabcas != null) {
//                                                tabcas = tabcas.child(0); // tbody
//                                                if (tabcas != null) {
//                                                    tabcas = tabcas.child(0); // tr
//                                                    if (tabcas != null) {
//                                                        tabcas = tabcas.nextElementSibling(); // tr EC
//                                                        if (tabcas != null) {
//                                                            // Récupération du numéro EC
//                                                            Element tdEc = tabcas.child(0); // td 
//                                                            if (tdEc != null) {
//                                                                tdEc = tdEc.nextElementSibling(); // td       
//                                                                if (tdEc != null) {
//                                                                    tdEc = tdEc.nextElementSibling(); // td 
//                                                                    if (tdEc != null) {
//                                                                        ec = tdEc.text();
//                                                                    }                                                                    
//                                                                }
//                                                            }
//                                                            
//                                                            // récupération du cas
//                                                            tabcas = tabcas.nextElementSibling(); // tr CAS
//                                                            if (tabcas != null) {
//                                                                Element tdCas = tabcas.child(0); // td 
//                                                                if (tdCas != null) {
//                                                                    tdCas = tdCas.nextElementSibling(); // td       
//                                                                    if (tdCas != null) {
//                                                                        tdCas = tdCas.nextElementSibling(); // td 
//                                                                        if (tdCas != null) {
//                                                                            casHtml = tdCas.text();
//                                                                        }                                                                    
//                                                                    }
//                                                                }
//                                                                
//                                                                // récupération du nom 
//                                                                tabcas = tabcas.nextElementSibling(); // tr nom anglais
//                                                                if (tabcas != null) {
//                                                                    tabcas = tabcas.nextElementSibling(); // tr nom français
//                                                                    if (tabcas != null) {
//                                                                        Element tdname = tabcas.child(0); // td 
//                                                                        if (tdname != null) {
//                                                                            tdname = tdname.nextElementSibling(); // td       
//                                                                            if (tdname != null) {
//                                                                                tdname = tdname.nextElementSibling(); // td 
//                                                                                if (tdname != null) {
//                                                                                    name = tdname.text();
//                                                                                }                                                                    
//                                                                            }
//                                                                        }                                                            
//
//                                                                        // récupération de la Formule
//                                                                        tabcas = tabcas.nextElementSibling(); // tr CAS
//                                                                        if (tabcas != null) {
//                                                                            Element tdFormula = tabcas.child(0); // td 
//                                                                            if (tdFormula != null) {
//                                                                                tdFormula = tdFormula.nextElementSibling(); // td       
//                                                                                if (tdFormula != null) {
//                                                                                    tdFormula = tdFormula.nextElementSibling(); // td 
//                                                                                    if (tdFormula != null) {
//                                                                                        molecularFormula = tdFormula.text();
//                                                                                    }                                                                    
//                                                                                }
//                                                                            }      
//                                                                            // récupération de la Description
//                                                                            tabcas = tabcas.nextElementSibling(); // tr CAS
//                                                                            if (tabcas != null) {
//                                                                                Element tdDesc = tabcas.child(0); // td 
//                                                                                if (tdDesc != null) {
//                                                                                    tdDesc = tdDesc.nextElementSibling(); // td       
//                                                                                    if (tdDesc != null) {
//                                                                                        tdDesc = tdDesc.nextElementSibling(); // td 
//                                                                                        if (tdDesc != null) {
//                                                                                            description = tdDesc.text();
//                                                                                            if ("Not available".equals(description)){
//                                                                                                description = "";
//                                                                                            }
//                                                                                        }                                                                    
//                                                                                    }
//                                                                                } 
//                                                                            }  
//                                                                        }
//                                                                    }
//                                                                }                                                            
//                                                            }                                                            
//                                                        }
//                                                    }
//                                                }
//                                           }
//                                        }
//                                    }
//                                }
//                            }
//                        }
//                    }                                                  
//                }     
//                
//                Elements links = doc.select("[src^=/image/logo]");
//                
//                for (Element l : links){                  
//                    l = l.nextElementSibling();
//                    String danger = "";
//                    if (l!=null){
//                        danger = l.text();
//                        danger = danger.trim().substring(2,danger.length()-3).trim();
//                        danger = danger.replace(" ", "");
//                        Pictogramme p = daoPic.findPictoFromCode(danger);
//                        if (p!=null){
//                            pictoList.add(p);
//                        }                        
//                    }                   
//                }
                
                

                if (!"".equals(CAS) && CAS.equals(casHtml) && !"".equals(name)){
                    sub = new Substance(CAS, name, molecularFormula, description, ec, pictoList);

                }
            }
            
        
        }catch(IOException ex){
            Logger.getLogger(ESISSearch.class.getName()).log(Level.SEVERE, null, ex);
        }
       
        
        return sub;        
    }
    
   
}
