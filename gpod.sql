-- phpMyAdmin SQL Dump
-- version 3.3.7
-- http://www.phpmyadmin.net
--
-- Serveur: arnold.univ-tours.local
-- Généré le : Lun 09 Décembre 2013 à 15:45
-- Version du serveur: 5.0.77
-- Version de PHP: 5.3.2

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `gpod`
--

-- --------------------------------------------------------

--
-- Structure de la table `airestockage`
--

CREATE TABLE IF NOT EXISTS `airestockage` (
  `idAire` int(11) NOT NULL auto_increment,
  `idTypeAire` int(11) default NULL,
  `idSalleStockage` int(11) default NULL,
  `code` varchar(255) NOT NULL,
  `nom` varchar(255) default NULL,
  `refrigere` bit(1) default NULL,
  `description` text NOT NULL,
  PRIMARY KEY  (`idAire`),
  KEY `FK_type_piece` (`idTypeAire`),
  KEY `idSalleStockage` (`idSalleStockage`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=614 ;

-- --------------------------------------------------------

--
-- Structure de la table `article`
--

CREATE TABLE IF NOT EXISTS `article` (
  `idArticle` int(11) NOT NULL auto_increment,
  `idProduitConditionnement` int(11) NOT NULL,
  `idLivraison` int(11) default NULL,
  `idAire` int(11) NOT NULL,
  `idSousStructure` int(11) NOT NULL,
  `identifiantEtiquette` varchar(50) NOT NULL,
  `quantite` double NOT NULL default '1',
  `dateOuverture` timestamp NULL default NULL,
  PRIMARY KEY  (`idArticle`),
  KEY `idLivraison` (`idLivraison`),
  KEY `idAire` (`idAire`),
  KEY `idSousStructure` (`idSousStructure`),
  KEY `idProduitConditionnement` (`idProduitConditionnement`),
  KEY `identifiantEtiquette` (`identifiantEtiquette`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7236 ;

-- --------------------------------------------------------

--
-- Structure de la table `batiment`
--

CREATE TABLE IF NOT EXISTS `batiment` (
  `idBatiment` int(11) NOT NULL auto_increment,
  `idSite` int(11) NOT NULL,
  `code` varchar(25) default NULL,
  `nom` varchar(45) default NULL,
  `adresse1` varchar(255) default NULL,
  `adresse2` varchar(255) default NULL,
  `codepostal` varchar(6) default NULL,
  `ville` varchar(255) default NULL,
  `telephone` varchar(12) default NULL,
  `fax` varchar(12) default NULL,
  `longitude` double default NULL,
  `latitude` double default NULL,
  PRIMARY KEY  (`idBatiment`),
  KEY `idSite` (`idSite`),
  KEY `idSite_2` (`idSite`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=33 ;

-- --------------------------------------------------------

--
-- Structure de la table `clickeyscan`
--

CREATE TABLE IF NOT EXISTS `clickeyscan` (
  `idClickeyScan` int(11) NOT NULL auto_increment,
  `UIDClickey` varchar(24) NOT NULL,
  `UIDTag` varchar(50) NOT NULL,
  `TimeStamp` varchar(20) NOT NULL,
  `DateSynchro` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `idUtilisateur` int(11) NOT NULL,
  `DejaTraite` tinyint(1) NOT NULL,
  PRIMARY KEY  (`idClickeyScan`),
  KEY `idUtilisateur` (`idUtilisateur`),
  KEY `UIDTag` (`UIDTag`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `commande`
--

CREATE TABLE IF NOT EXISTS `commande` (
  `idCommande` int(11) NOT NULL auto_increment,
  `dateCommande` datetime default NULL,
  `idUtilisateur` int(11) default NULL,
  `idStatut` int(11) default NULL,
  `idSousStructure` int(11) default NULL,
  `numBonDeCommandeInterne` varchar(50) default NULL,
  `dateCommandeInterne` datetime default NULL,
  PRIMARY KEY  (`idCommande`),
  KEY `fk_C_utilisateur` (`idUtilisateur`),
  KEY `fk_C_statut` (`idStatut`),
  KEY `fk_C_SousStructure` (`idSousStructure`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Structure de la table `commandeligne`
--

CREATE TABLE IF NOT EXISTS `commandeligne` (
  `idCommandeLigne` int(11) NOT NULL auto_increment,
  `idCommande` int(11) NOT NULL,
  `idProduitConditionnement` int(11) NOT NULL,
  `quantite` int(11) NOT NULL,
  PRIMARY KEY  (`idCommandeLigne`),
  KEY `idProduitConditionnement` (`idProduitConditionnement`),
  KEY `idCommande` (`idCommande`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Structure de la table `contenant`
--

CREATE TABLE IF NOT EXISTS `contenant` (
  `idContenant` int(11) NOT NULL auto_increment,
  `Libelle` varchar(45) default NULL,
  PRIMARY KEY  (`idContenant`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Structure de la table `emailproperties`
--

CREATE TABLE IF NOT EXISTS `emailproperties` (
  `idEmailProperties` int(11) NOT NULL auto_increment,
  `smtp` varchar(255) NOT NULL,
  `port` int(11) NOT NULL,
  `fromEmail` varchar(255) NOT NULL,
  PRIMARY KEY  (`idEmailProperties`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Structure de la table `equipe`
--

CREATE TABLE IF NOT EXISTS `equipe` (
  `idEquipe` int(11) NOT NULL auto_increment,
  `idLabo` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `code` varchar(25) NOT NULL,
  `adresse1` varchar(255) NOT NULL,
  `adresse2` varchar(255) NOT NULL,
  `codepostal` varchar(6) NOT NULL,
  `ville` varchar(255) NOT NULL,
  `telephone` varchar(12) NOT NULL,
  `fax` varchar(12) NOT NULL,
  `responsable` varchar(255) NOT NULL,
  `longitude` double NOT NULL,
  `latitude` double NOT NULL,
  PRIMARY KEY  (`idEquipe`),
  KEY `FK_labo_ID` (`idLabo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `etatphysique`
--

CREATE TABLE IF NOT EXISTS `etatphysique` (
  `idEtatPhysique` int(11) NOT NULL auto_increment,
  `Libelle` varchar(45) default NULL,
  PRIMARY KEY  (`idEtatPhysique`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Structure de la table `fournisseur`
--

CREATE TABLE IF NOT EXISTS `fournisseur` (
  `idFournisseur` int(11) NOT NULL auto_increment,
  `referenceSIFAC` varchar(45) NOT NULL,
  `nom` varchar(45) default NULL,
  `adresse1` varchar(255) default NULL,
  `adresse2` varchar(255) default NULL,
  `codePostal` varchar(6) default NULL,
  `ville` varchar(255) default NULL,
  `nomContact` varchar(45) default NULL,
  `telephone` varchar(12) default NULL,
  `fax` varchar(12) NOT NULL,
  PRIMARY KEY  (`idFournisseur`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=131 ;

-- --------------------------------------------------------

--
-- Structure de la table `historiquecommande`
--

CREATE TABLE IF NOT EXISTS `historiquecommande` (
  `idHistoriqueCommande` int(11) NOT NULL auto_increment,
  `dateHistorique` varchar(45) default NULL,
  `idCommande` int(11) default NULL,
  `idUtilisateur` int(11) default NULL,
  `action` varchar(45) default NULL,
  `statut` varchar(45) default NULL,
  PRIMARY KEY  (`idHistoriqueCommande`),
  KEY `fk_HC_commande` (`idCommande`),
  KEY `fk_HC_utilisateur` (`idUtilisateur`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `historiqueconsomation`
--

CREATE TABLE IF NOT EXISTS `historiqueconsomation` (
  `idHistoriqueConsommation` int(11) NOT NULL auto_increment,
  `dateConsommation` timestamp NULL default NULL,
  `idArticle` int(11) default NULL,
  `idUtilisateur` int(11) default NULL,
  `idSousStructure` int(11) NOT NULL,
  `idAire` int(11) NOT NULL,
  `quantiteRestante` double default NULL,
  `commentaire` text,
  `affectationutilisateur` varchar(255) default NULL,
  PRIMARY KEY  (`idHistoriqueConsommation`),
  KEY `fk_HCO_produit` (`idArticle`),
  KEY `fk_HCO_utilisateur` (`idUtilisateur`),
  KEY `idSousStructure` (`idSousStructure`,`idAire`),
  KEY `idAireStockage` (`idAire`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=644 ;

-- --------------------------------------------------------

--
-- Structure de la table `importexcel`
--

CREATE TABLE IF NOT EXISTS `importexcel` (
  `CAS` varchar(50) NOT NULL,
  `Nom` varchar(255) NOT NULL,
  `Fourniseur` varchar(255) NOT NULL,
  `Quantite` double NOT NULL,
  `UniteMesure` varchar(5) NOT NULL,
  `QuantiteUnitaire` double NOT NULL,
  `FormuleBrut` varchar(255) NOT NULL,
  `DateImport` datetime NOT NULL,
  `insere` int(11) NOT NULL default '0',
  PRIMARY KEY  (`CAS`,`Nom`,`Fourniseur`,`Quantite`,`UniteMesure`,`QuantiteUnitaire`,`FormuleBrut`,`DateImport`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `laboratoire`
--

CREATE TABLE IF NOT EXISTS `laboratoire` (
  `idLabo` int(11) NOT NULL auto_increment,
  `code` varchar(25) NOT NULL,
  `nom` varchar(45) default NULL,
  `adresse1` varchar(255) NOT NULL,
  `adresse2` varchar(255) NOT NULL,
  `codepostal` varchar(6) NOT NULL,
  `ville` varchar(255) NOT NULL,
  `telephone` varchar(12) NOT NULL,
  `fax` varchar(12) NOT NULL,
  `responsable` varchar(255) NOT NULL,
  `longitude` double NOT NULL,
  `latitude` double NOT NULL,
  PRIMARY KEY  (`idLabo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `livraison`
--

CREATE TABLE IF NOT EXISTS `livraison` (
  `idLivraison` int(11) NOT NULL auto_increment,
  `dateLivraison` datetime NOT NULL,
  `numeroBonLivraison` varchar(45) default NULL,
  `idCommande` int(11) default '0',
  `idUtilisateur` int(11) NOT NULL,
  PRIMARY KEY  (`idLivraison`),
  KEY `fk_commande` (`idCommande`),
  KEY `fk_idutilisateur` (`idUtilisateur`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5587 ;

-- --------------------------------------------------------

--
-- Structure de la table `livraisonligne`
--

CREATE TABLE IF NOT EXISTS `livraisonligne` (
  `idLivraisonLigne` int(11) NOT NULL auto_increment,
  `idLivraison` int(11) default NULL,
  `idProduitConditionnement` int(11) NOT NULL,
  `quantite` int(11) NOT NULL,
  `enStock` tinyint(1) NOT NULL,
  PRIMARY KEY  (`idLivraisonLigne`),
  KEY `idLivraison` (`idLivraison`),
  KEY `idProduitConditionnement` (`idProduitConditionnement`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5569 ;

-- --------------------------------------------------------

--
-- Structure de la table `mentiondanger`
--

CREATE TABLE IF NOT EXISTS `mentiondanger` (
  `idmention` int(11) NOT NULL auto_increment,
  `code` varchar(7) NOT NULL,
  `libelle` text NOT NULL,
  PRIMARY KEY  (`idmention`),
  UNIQUE KEY `code` (`code`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=73 ;

-- --------------------------------------------------------

--
-- Structure de la table `pictogramme`
--

CREATE TABLE IF NOT EXISTS `pictogramme` (
  `idPictogramme` int(11) NOT NULL auto_increment,
  `nom` varchar(100) default NULL,
  `description` varchar(45) default NULL,
  `icone` varchar(45) default NULL,
  `code` varchar(10) default NULL,
  `sgh` bit(1) default NULL,
  PRIMARY KEY  (`idPictogramme`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

-- --------------------------------------------------------

--
-- Structure de la table `produit`
--

CREATE TABLE IF NOT EXISTS `produit` (
  `idProduit` int(11) NOT NULL auto_increment,
  `CAS` varchar(15) NOT NULL,
  `nom` varchar(255) default NULL,
  `description` text,
  `idFournisseur` int(11) default NULL,
  `idEtatPhysique` int(11) default NULL,
  `purete` double default NULL,
  `densite` double NOT NULL,
  `pointDeFusion` double NOT NULL,
  `pointdEbullition` double NOT NULL,
  `masseMolaire` double NOT NULL,
  `ficheSecurite` varchar(255) default NULL,
  `refrigere` bit(1) default NULL,
  PRIMARY KEY  (`idProduit`),
  KEY `fk_P_fournisseur` (`idFournisseur`),
  KEY `fk_P_etatphysique` (`idEtatPhysique`),
  KEY `CAS` (`CAS`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5002 ;

-- --------------------------------------------------------

--
-- Structure de la table `produitconditionnement`
--

CREATE TABLE IF NOT EXISTS `produitconditionnement` (
  `idProduitConditionnement` int(11) NOT NULL auto_increment,
  `idProduit` int(11) NOT NULL,
  `referenceFournisseur` varchar(50) NOT NULL,
  `contenance` double NOT NULL,
  `idUniteMesure` int(11) NOT NULL,
  `idContenant` int(11) NOT NULL,
  `deleted` bit(1) NOT NULL default b'0',
  PRIMARY KEY  (`idProduitConditionnement`),
  KEY `idUniteMesure` (`idUniteMesure`),
  KEY `idProduit` (`idProduit`),
  KEY `idContenant` (`idContenant`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5398 ;

-- --------------------------------------------------------

--
-- Structure de la table `produitmention`
--

CREATE TABLE IF NOT EXISTS `produitmention` (
  `idmention` int(11) NOT NULL,
  `idproduit` int(11) NOT NULL,
  PRIMARY KEY  (`idmention`,`idproduit`),
  KEY `idmention` (`idmention`),
  KEY `idproduit` (`idproduit`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `produitpictogramme`
--

CREATE TABLE IF NOT EXISTS `produitpictogramme` (
  `idProduit` int(11) NOT NULL,
  `idPictogramme` int(11) NOT NULL,
  PRIMARY KEY  (`idProduit`,`idPictogramme`),
  KEY `fk_PP_produit` (`idProduit`),
  KEY `fk_PP_pictop` (`idPictogramme`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `produittag`
--

CREATE TABLE IF NOT EXISTS `produittag` (
  `idproduit` int(11) NOT NULL,
  `idtag` int(11) NOT NULL,
  PRIMARY KEY  (`idproduit`,`idtag`),
  KEY `idproduit` (`idproduit`),
  KEY `idtag` (`idtag`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `properties`
--

CREATE TABLE IF NOT EXISTS `properties` (
  `idProperties` int(11) NOT NULL auto_increment,
  `smtp` varchar(255) NOT NULL,
  `port` int(11) NOT NULL,
  `fromEmail` varchar(255) NOT NULL,
  `ldapAddress` varchar(255) default NULL,
  `ldapPort` int(11) default NULL,
  `ldapUser` varchar(255) default NULL,
  `ldapPassword` varchar(255) default NULL,
  PRIMARY KEY  (`idProperties`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Structure de la table `sallestockage`
--

CREATE TABLE IF NOT EXISTS `sallestockage` (
  `idSalleStockage` int(11) NOT NULL auto_increment,
  `idBatiment` int(11) default NULL,
  `nom` varchar(255) default NULL,
  `code` varchar(25) default NULL,
  `Description` text NOT NULL,
  PRIMARY KEY  (`idSalleStockage`),
  KEY `idBatiment` (`idBatiment`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=175 ;

-- --------------------------------------------------------

--
-- Structure de la table `site`
--

CREATE TABLE IF NOT EXISTS `site` (
  `idSite` int(11) NOT NULL auto_increment,
  `nom` varchar(255) default NULL,
  PRIMARY KEY  (`idSite`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

-- --------------------------------------------------------

--
-- Structure de la table `sousstructure`
--

CREATE TABLE IF NOT EXISTS `sousstructure` (
  `idSousStructure` int(11) NOT NULL auto_increment,
  `idStructure` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `code` varchar(25) NOT NULL,
  `adresse1` varchar(255) NOT NULL,
  `adresse2` varchar(255) NOT NULL,
  `codepostal` varchar(6) NOT NULL,
  `ville` varchar(255) NOT NULL,
  `telephone` varchar(12) NOT NULL,
  `fax` varchar(12) NOT NULL,
  `responsable` varchar(255) NOT NULL,
  `type` varchar(2) NOT NULL default 'A',
  PRIMARY KEY  (`idSousStructure`),
  KEY `FK_Structure_ID` (`idStructure`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=67 ;

-- --------------------------------------------------------

--
-- Structure de la table `statut`
--

CREATE TABLE IF NOT EXISTS `statut` (
  `idStatut` int(11) NOT NULL,
  `libelle` varchar(45) default NULL,
  PRIMARY KEY  (`idStatut`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `stock`
--

CREATE TABLE IF NOT EXISTS `stock` (
  `idAire` int(11) NOT NULL,
  `idProduit` int(11) NOT NULL,
  `idSousStructure` int(11) NOT NULL,
  `Quantite` float default NULL,
  PRIMARY KEY  (`idAire`,`idProduit`,`idSousStructure`),
  KEY `fk_ST_piece` (`idAire`),
  KEY `fk_ST_produit` (`idProduit`),
  KEY `fk_idSousStructure` (`idSousStructure`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `structure`
--

CREATE TABLE IF NOT EXISTS `structure` (
  `idStructure` int(11) NOT NULL auto_increment,
  `code` varchar(25) NOT NULL,
  `nom` varchar(255) default NULL,
  `adresse1` varchar(255) NOT NULL,
  `adresse2` varchar(255) NOT NULL,
  `codepostal` varchar(6) NOT NULL,
  `ville` varchar(255) NOT NULL,
  `telephone` varchar(12) NOT NULL,
  `fax` varchar(12) NOT NULL,
  `responsable` varchar(255) NOT NULL,
  `type` varchar(2) NOT NULL default 'A',
  PRIMARY KEY  (`idStructure`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=31 ;

-- --------------------------------------------------------

--
-- Structure de la table `substance`
--

CREATE TABLE IF NOT EXISTS `substance` (
  `CAS` varchar(15) NOT NULL,
  `nom` varchar(255) default NULL,
  `formule` varchar(45) default NULL,
  `description` text,
  `EC` varchar(10) NOT NULL,
  `sdf` text,
  PRIMARY KEY  (`CAS`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `substancepictogramme`
--

CREATE TABLE IF NOT EXISTS `substancepictogramme` (
  `CAS` varchar(15) NOT NULL,
  `idPictogramme` int(11) NOT NULL,
  PRIMARY KEY  (`CAS`,`idPictogramme`),
  KEY `fk_SP_cas` (`CAS`),
  KEY `fk_SP_picto` (`idPictogramme`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `tag`
--

CREATE TABLE IF NOT EXISTS `tag` (
  `idtag` int(11) NOT NULL auto_increment,
  `libelle` varchar(100) default NULL,
  PRIMARY KEY  (`idtag`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=47 ;

-- --------------------------------------------------------

--
-- Structure de la table `typeaire`
--

CREATE TABLE IF NOT EXISTS `typeaire` (
  `idTypeAire` int(11) NOT NULL auto_increment,
  `nom` varchar(45) default NULL,
  PRIMARY KEY  (`idTypeAire`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

-- --------------------------------------------------------

--
-- Structure de la table `unitemesure`
--

CREATE TABLE IF NOT EXISTS `unitemesure` (
  `idUniteMesure` int(11) NOT NULL auto_increment,
  `libelleLong` varchar(45) default NULL,
  `abreviation` varchar(45) default NULL,
  PRIMARY KEY  (`idUniteMesure`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

CREATE TABLE IF NOT EXISTS `utilisateur` (
  `IdUtilisateur` int(11) NOT NULL auto_increment,
  `nom` varchar(45) default NULL,
  `prenom` varchar(45) NOT NULL,
  `login` varchar(45) default NULL,
  `password` varchar(45) default NULL,
  `email` varchar(45) NOT NULL,
  `idUtilisateurProfil` int(11) NOT NULL,
  `dateDerniereConnexion` timestamp NULL default NULL,
  `dateAvantDerniereConnexion` timestamp NULL default NULL,
  `idAireFavorite` int(11) default '0',
  PRIMARY KEY  (`IdUtilisateur`),
  KEY `fk_Utilisateur_Profil_utilisateurs` (`idUtilisateurProfil`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=119 ;

-- --------------------------------------------------------

--
-- Structure de la table `utilisateurequipe`
--

CREATE TABLE IF NOT EXISTS `utilisateurequipe` (
  `idUtilisateur` int(11) NOT NULL,
  `idEquipe` int(11) NOT NULL,
  PRIMARY KEY  (`idUtilisateur`,`idEquipe`),
  KEY `FK_US_utilisateur` (`idUtilisateur`),
  KEY `FK_US_site` (`idEquipe`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `utilisateurprofil`
--

CREATE TABLE IF NOT EXISTS `utilisateurprofil` (
  `idUtilisateurProfil` int(11) NOT NULL auto_increment,
  `nom` varchar(45) default NULL,
  `code` varchar(2) NOT NULL,
  PRIMARY KEY  (`idUtilisateurProfil`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

-- --------------------------------------------------------

--
-- Structure de la table `utilisateursousstructure`
--

CREATE TABLE IF NOT EXISTS `utilisateursousstructure` (
  `idUtilisateur` int(11) NOT NULL,
  `idSousStructure` int(11) NOT NULL,
  PRIMARY KEY  (`idUtilisateur`,`idSousStructure`),
  KEY `FK_US_util` (`idUtilisateur`),
  KEY `FK_US_SStruc` (`idSousStructure`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `airestockage`
--
ALTER TABLE `airestockage`
  ADD CONSTRAINT `airestockage_ibfk_1` FOREIGN KEY (`idSalleStockage`) REFERENCES `sallestockage` (`idSalleStockage`);

--
-- Contraintes pour la table `article`
--
ALTER TABLE `article`
  ADD CONSTRAINT `article_ibfk_2` FOREIGN KEY (`idLivraison`) REFERENCES `livraison` (`idLivraison`),
  ADD CONSTRAINT `article_ibfk_3` FOREIGN KEY (`idAire`) REFERENCES `airestockage` (`idAire`),
  ADD CONSTRAINT `article_ibfk_4` FOREIGN KEY (`idSousStructure`) REFERENCES `sousstructure` (`idSousStructure`),
  ADD CONSTRAINT `article_ibfk_5` FOREIGN KEY (`idProduitConditionnement`) REFERENCES `produitconditionnement` (`idProduitConditionnement`);

--
-- Contraintes pour la table `batiment`
--
ALTER TABLE `batiment`
  ADD CONSTRAINT `batiment_ibfk_1` FOREIGN KEY (`idSite`) REFERENCES `site` (`idSite`);

--
-- Contraintes pour la table `clickeyscan`
--
ALTER TABLE `clickeyscan`
  ADD CONSTRAINT `clickeyscan_ibfk_1` FOREIGN KEY (`idUtilisateur`) REFERENCES `utilisateur` (`IdUtilisateur`);

--
-- Contraintes pour la table `commande`
--
ALTER TABLE `commande`
  ADD CONSTRAINT `commande_ibfk_1` FOREIGN KEY (`idSousStructure`) REFERENCES `sousstructure` (`idSousStructure`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_C_statut` FOREIGN KEY (`idStatut`) REFERENCES `statut` (`idStatut`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_C_utilisateur` FOREIGN KEY (`idUtilisateur`) REFERENCES `utilisateur` (`IdUtilisateur`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `commandeligne`
--
ALTER TABLE `commandeligne`
  ADD CONSTRAINT `commandeligne_ibfk_2` FOREIGN KEY (`idProduitConditionnement`) REFERENCES `produitconditionnement` (`idProduitConditionnement`),
  ADD CONSTRAINT `commandeligne_ibfk_3` FOREIGN KEY (`idCommande`) REFERENCES `commande` (`idCommande`);

--
-- Contraintes pour la table `equipe`
--
ALTER TABLE `equipe`
  ADD CONSTRAINT `FK_labo_ID` FOREIGN KEY (`idLabo`) REFERENCES `laboratoire` (`idLabo`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `historiquecommande`
--
ALTER TABLE `historiquecommande`
  ADD CONSTRAINT `fk_HC_commande` FOREIGN KEY (`idCommande`) REFERENCES `commande` (`idCommande`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_HC_utilisateur` FOREIGN KEY (`idUtilisateur`) REFERENCES `utilisateur` (`IdUtilisateur`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `historiqueconsomation`
--
ALTER TABLE `historiqueconsomation`
  ADD CONSTRAINT `fk_HCO_utilisateur` FOREIGN KEY (`idUtilisateur`) REFERENCES `utilisateur` (`IdUtilisateur`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `historiqueconsomation_ibfk_1` FOREIGN KEY (`idArticle`) REFERENCES `article` (`idArticle`),
  ADD CONSTRAINT `historiqueconsomation_ibfk_2` FOREIGN KEY (`idSousStructure`) REFERENCES `sousstructure` (`idSousStructure`),
  ADD CONSTRAINT `historiqueconsomation_ibfk_3` FOREIGN KEY (`idAire`) REFERENCES `airestockage` (`idAire`);

--
-- Contraintes pour la table `livraison`
--
ALTER TABLE `livraison`
  ADD CONSTRAINT `fk_commande` FOREIGN KEY (`idCommande`) REFERENCES `commande` (`idCommande`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `livraison_ibfk_7` FOREIGN KEY (`idUtilisateur`) REFERENCES `utilisateur` (`IdUtilisateur`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `livraisonligne`
--
ALTER TABLE `livraisonligne`
  ADD CONSTRAINT `livraisonligne_ibfk_2` FOREIGN KEY (`idLivraison`) REFERENCES `livraison` (`idLivraison`),
  ADD CONSTRAINT `livraisonligne_ibfk_3` FOREIGN KEY (`idProduitConditionnement`) REFERENCES `produitconditionnement` (`idProduitConditionnement`);

--
-- Contraintes pour la table `produit`
--
ALTER TABLE `produit`
  ADD CONSTRAINT `fk_P_etatphysique` FOREIGN KEY (`idEtatPhysique`) REFERENCES `etatphysique` (`idEtatPhysique`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_P_fournisseur` FOREIGN KEY (`idFournisseur`) REFERENCES `fournisseur` (`idFournisseur`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `produit_ibfk_1` FOREIGN KEY (`CAS`) REFERENCES `substance` (`CAS`);

--
-- Contraintes pour la table `produitconditionnement`
--
ALTER TABLE `produitconditionnement`
  ADD CONSTRAINT `produitconditionnement_ibfk_1` FOREIGN KEY (`idUniteMesure`) REFERENCES `unitemesure` (`idUniteMesure`),
  ADD CONSTRAINT `produitconditionnement_ibfk_4` FOREIGN KEY (`idContenant`) REFERENCES `contenant` (`idContenant`),
  ADD CONSTRAINT `produitconditionnement_ibfk_5` FOREIGN KEY (`idProduit`) REFERENCES `produit` (`idProduit`);

--
-- Contraintes pour la table `produitmention`
--
ALTER TABLE `produitmention`
  ADD CONSTRAINT `produitmention_ibfk_1` FOREIGN KEY (`idmention`) REFERENCES `mentiondanger` (`idmention`),
  ADD CONSTRAINT `produitmention_ibfk_2` FOREIGN KEY (`idproduit`) REFERENCES `produit` (`idProduit`);

--
-- Contraintes pour la table `produitpictogramme`
--
ALTER TABLE `produitpictogramme`
  ADD CONSTRAINT `fk_PP_pictop` FOREIGN KEY (`idPictogramme`) REFERENCES `pictogramme` (`idPictogramme`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_PP_produit` FOREIGN KEY (`idProduit`) REFERENCES `produit` (`idProduit`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `produittag`
--
ALTER TABLE `produittag`
  ADD CONSTRAINT `produittag_ibfk_1` FOREIGN KEY (`idproduit`) REFERENCES `produit` (`idProduit`),
  ADD CONSTRAINT `produittag_ibfk_2` FOREIGN KEY (`idtag`) REFERENCES `tag` (`idtag`);

--
-- Contraintes pour la table `sallestockage`
--
ALTER TABLE `sallestockage`
  ADD CONSTRAINT `sallestockage_ibfk_1` FOREIGN KEY (`idBatiment`) REFERENCES `batiment` (`idBatiment`);

--
-- Contraintes pour la table `stock`
--
ALTER TABLE `stock`
  ADD CONSTRAINT `fk_ST_produit` FOREIGN KEY (`idProduit`) REFERENCES `produit` (`idProduit`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `stock_ibfk_3` FOREIGN KEY (`idAire`) REFERENCES `airestockage` (`idAire`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `substancepictogramme`
--
ALTER TABLE `substancepictogramme`
  ADD CONSTRAINT `fk_SP_cas` FOREIGN KEY (`CAS`) REFERENCES `substance` (`CAS`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_SP_picto` FOREIGN KEY (`idPictogramme`) REFERENCES `pictogramme` (`idPictogramme`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  ADD CONSTRAINT `utilisateur_ibfk_1` FOREIGN KEY (`idUtilisateurProfil`) REFERENCES `utilisateurprofil` (`idUtilisateurProfil`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `utilisateurequipe`
--
ALTER TABLE `utilisateurequipe`
  ADD CONSTRAINT `utilisateurequipe_ibfk_1` FOREIGN KEY (`idUtilisateur`) REFERENCES `utilisateur` (`IdUtilisateur`),
  ADD CONSTRAINT `utilisateurequipe_ibfk_2` FOREIGN KEY (`idEquipe`) REFERENCES `equipe` (`idEquipe`);
